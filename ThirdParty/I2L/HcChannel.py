from enum import Enum
import time

from .HilWrap import HilWrap as hil
from .Ltm4680 import *
from .HbiDpsFpga import *
from .Utility import Sim, Log

class HcChannel:

    RESOURCE_TYPE = "Hc"
    NUMBER_OF_CHANNELS = 10
    MIN_VOLTAGE, MAX_VOLTAGE, MAX_CLAMP_VOLTAGE = 0.5, 3.3, 3.6
    MIN_CURRENT, MAX_CURRENT = 0.0, 30.0
    MIN_FREEDRIVE_TIME_MS, MAX_FREEDRIVE_TIME_MS = 0.0, 1300.0
    MIN_FREEFALL_TIME_MS, MAX_FREEFALL_TIME_MS = 0.25, 1300.0
    DEFAULT_FREEDRIVE_MS = 1.0
    DEFAULT_FREEFALL_MS = 3.0
    DEFAULT_VFORCE = 1.0
    FREEDRIVE_TIME_LIMIT_MS = 2000.0
    RUN_PIN_OFF, RUN_PIN_ON = 0, 1
    BAR1 = 1
    LOW_VOLTAGE_MODE_MAX_VOLTAGE = 2.0 #This was based on testing, not on a datasheet.

    ##### USER CONFIGURABLE SECTION ###########
    DEFAULT_SWITCH_FREQUENCY = Ltm4680SwitchingFrequencies.F_650_KHZ
    DEFAULT_FAULT_RESPONSES = {
        #Command                                |  IsImmediate | RetryIndefinitely | DelayTime (us for VOUT, ms for IOUT) |      
        Ltm4680Commands.VOUT_OV_FAULT_RESPONSE  : [ False,          False,              70],
        Ltm4680Commands.VOUT_UV_FAULT_RESPONSE  : [ False,          False,              70],
        Ltm4680Commands.IOUT_OC_FAULT_RESPONSE  : [ False,          False,              112],
        Ltm4680Commands.TON_MAX_FAULT_RESPONSE  : [ True,           False,              0],
        Ltm4680Commands.OT_FAULT_RESPONSE       : [ True,           False,              0]
    }
    DEFAULT_OT_FAULT_LIMIT = 80 #degrees Celsius
    ##### END USER CONFIGURABLE SECTION ##########

    #Members of this class have a ".name" member variable to retrieve string representation
    class Attributes(Enum):
        FreeDriveCurrentHi = 0
        FreeDriveTime = 1
        FreeDriveRampTime = 2
        IClampHi = 3
        UnderVoltageLimit = 4
        OverVoltageLimit = 5
        PowerSequence = 6
        VForce = 7
        #StartMeasurement = 8
        #SampleInfo = 9

    ATTRIBUTE_LIMITS_DEFAULT = {
        # ATTRIBUTE                     | MIN, MAX, DEFAULT                                                                     | Notes   
        #------------------------------------------------------------------------------------------------------------------------------------------------------
        Attributes.FreeDriveCurrentHi   : [MIN_CURRENT, MAX_CURRENT, MIN_CURRENT],
        Attributes.FreeDriveTime        : [MIN_FREEDRIVE_TIME_MS/1000, MAX_FREEDRIVE_TIME_MS/1000, MAX_FREEDRIVE_TIME_MS/1000],     #Seconds
        Attributes.FreeDriveRampTime    : [MIN_FREEDRIVE_TIME_MS/1000, MAX_FREEDRIVE_TIME_MS/1000, DEFAULT_FREEDRIVE_MS/1000],
        Attributes.IClampHi             : [MIN_CURRENT, MAX_CURRENT, MAX_CURRENT],                                                  #Amperes
        Attributes.UnderVoltageLimit    : [0.0, MAX_VOLTAGE, 0.0],
        Attributes.OverVoltageLimit     : [MIN_VOLTAGE, MAX_CLAMP_VOLTAGE, MAX_CLAMP_VOLTAGE],
        Attributes.PowerSequence        : [False, True, False],                                                                     #Set to True to turn on the rail
        Attributes.VForce               : [MIN_VOLTAGE, MAX_VOLTAGE, DEFAULT_VFORCE]
        #Attributes.StartMeasurement     : [False, True, False],                                                                    #Set to True to turn on measurements. Not yet supported
        #SampleInfo not supported
    }

    RANGE_ATTRIBUTES = [Attributes.FreeDriveCurrentHi, Attributes.FreeDriveTime, Attributes.FreeDriveRampTime, Attributes.IClampHi, Attributes.UnderVoltageLimit, Attributes.OverVoltageLimit, Attributes.VForce]
    BOOL_ATTRIBUTES = [Attributes.PowerSequence]

    ATTR_MIN_INDEX, ATTR_MAX_INDEX, ATTR_DEFAULT = 0, 1, 2
    
    @staticmethod
    def SafestateAllChannels(slot):
        allChannels = list(range(HcChannel.NUMBER_OF_CHANNELS))
        #Turn off all rails
        HbiDpsFpga.HcRailGroupControl(slot, allChannels, False)
        #Disconnect all ganging relays
        HbiDpsFpga.HcGangControl(slot, allChannels, False, False, False)

    def __init__(self, channelInstrument, hcGangController, channel):
        self.slot = channelInstrument.GetSlotId()
        self.channel = channel
        self.chip = channel // Ltm4680.CHANNELS_PER_MODULE
        self.page = channel % Ltm4680.CHANNELS_PER_MODULE
        self.hcGangController = hcGangController
        self.ClearGangConfiguration() #Initializes self.gangConfig
        self._EnableGangConfigCheck() #Initializes self.checkGangConfig
        self.attributes = {}
        self.ResetAttributes()
        self.L16Mode = 0x14 #This is hard-coded in the Ltm4680, no need to read it from HW. Should return a representation of 2**-12

    def Initialize(self):
        self.ClearGangConfiguration()
        self._EnableGangConfigCheck()
        self._Ltm4680Write(Ltm4680Commands.ON_OFF_CONFIG, bytes([Ltm4680OnOffConfig.RUN_PIN_ONLY_NO_TOFF.value]))
        self.Safestate()
        
        #Input current measure shunt is 3mOhm
        self._Ltm4680Write(Ltm4680Commands.MFR_IIN_CAL_GAIN, hil.hilToL11(3.0))        
        
        #Set to high voltage mode (3.3V range). TODO: Hopefully this doesn't need to be set dynamically...
        bits = (1 << Ltm4680MfrPwmModeBits.VOUT_RANGE.value)
        mask = 0xFF
        self._Ltm4680ReadModifyWrite(Ltm4680Commands.MFR_PWM_MODE, Ltm4680ByteCount.SINGLE_BYTE.value, bits, mask, False) #Clear this bit (0=high voltage mode, 1=low voltage mode)
        #TODO: Could clear faults when the RUN pin rises? Would that be a good thing?
        # Set the SYNC signal to be generated internally without outputting the signal
        self._SetSyncIndependent()

        #Set fault responses correctly here.
        self._Ltm4680Write(Ltm4680Commands.OT_FAULT_LIMIT, hil.hilToL11(HcChannel.DEFAULT_OT_FAULT_LIMIT))
        self._Ltm4680Write(Ltm4680Commands.OT_WARN_LIMIT, hil.hilToL11(HcChannel.DEFAULT_OT_FAULT_LIMIT)) #TODO: Need a separate warn limit?
        for faultResponseCommand, defaultResponse in HcChannel.DEFAULT_FAULT_RESPONSES.items():
            isImmediate = defaultResponse[0]
            retryIndefinitely = defaultResponse[1]
            delayTime = defaultResponse[2]
            self._ConfigureFaultResponse(faultResponseCommand, isImmediate, retryIndefinitely, delayTime)
        
    def GetChannelId(self):
        return "{}.{}".format(self.slot, self.channel)

    def GetChannelNumber(self):
        #TODO: What does this mean?
        return self.channel

    def Apply(self):
        if self.checkGangConfig and self.gangConfig == HcChannel.GangConfiguration.Slave:
            raise RuntimeError('Cannot set attributes on a ganged rail that is configured as a slave.')
        elif self.checkGangConfig and self.gangConfig == HcChannel.GangConfiguration.Master:
            self.hcGangController.SequenceGang()    #masterChannel.Apply() is just an alias for HcGang.SequenceGang()
            return

        for attribute, value in self.attributes.items():
            if attribute != HcChannel.Attributes.PowerSequence:
                self._ApplySingleAttribute(attribute, value)
                #self.CheckRailStatus()
        powerSequenceValue = self.attributes[HcChannel.Attributes.PowerSequence]
        self._ApplySingleAttribute(HcChannel.Attributes.PowerSequence, powerSequenceValue)

        #For ganging, the toggling of the RUN pins is managed by HcGang. It needs to happen all at once.
        if self.gangConfig == HcChannel.GangConfiguration.Independent:
            self._SetRunPin(powerSequenceValue)

    def GetResults(self):
        rawCurrent = self._Ltm4680Read(Ltm4680Commands.READ_IOUT, Ltm4680ByteCount.L11_READ.value)
        current = hil.hilFromL11(rawCurrent)
        rawVoltage = self._Ltm4680Read(Ltm4680Commands.READ_VOUT, Ltm4680ByteCount.L16_READ.value)
        voltage = hil.hilFromL16(rawVoltage, self.L16Mode)
        rawInputCurrent = self._Ltm4680Read(Ltm4680Commands.READ_IIN, Ltm4680ByteCount.L11_READ.value)
        inputCurrent = hil.hilFromL11(rawInputCurrent)
        rawInputVoltage = self._Ltm4680Read(Ltm4680Commands.READ_VIN, Ltm4680ByteCount.L16_READ.value)
        inputVoltage = hil.hilFromL16(rawInputVoltage, self.L16Mode)
        return {'RailOutputCurrent' : current, 'RailOutputVoltage' : voltage, 'ModuleInputCurrent' : inputCurrent, 'ModuleInputVoltage' : inputVoltage}

    def GetName(self):
        return "{}Rail_Slot{}_Channel{}".format(HcChannel.RESOURCE_TYPE, self.slot, self.channel)

    def GetResourceType(self):
        return HcChannel.RESOURCE_TYPE

    def SetAttribute(self, attributeName, attributeValue):
        validAttribute = None
        for attribute in HcChannel.Attributes:
            if attributeName == attribute.name:
                validAttribute = attribute
        if validAttribute is None:
            raise RuntimeError("Attribute \"{}\" is invalid".format(attributeName))

        self._ValidateUserAttribute(validAttribute, attributeValue)

        #Putting the check here allows for checking if the attribute is valid or not so that it fails sooner rather than later
        if self.checkGangConfig and self.gangConfig != HcChannel.GangConfiguration.Independent:
            if self.gangConfig == HcChannel.GangConfiguration.Master:
                self.hcGangController.SetAttribute(validAttribute.name, attributeValue) #Use as an alias to set attribute on the HC gang, not the channel (for backwards compatibility reasons)
            else:
                raise RuntimeError('Cannot set attributes on a ganged slave rail.')
        else: #Either checking is bypassed or this is configured as independent
            self.attributes[validAttribute] = attributeValue

    def SetAttributes(self, attributeMap):
        for attributeName, attributeValue in attributeMap.items():
            self.SetAttribute(attributeName, attributeValue)

    def GetAttributes(self):
        return {attribute.name : self.attributes[attribute] for attribute in self.attributes.keys()} #Returning a new dict so that users can't modify a direct reference

    def GetAttributesFromHw(self):
        attributes = {}
        for attribute in self.Attributes:
            attributes[attribute] = self._GetUserAttributeValueFromHw(attribute)
        return attributes

    def ResetAttributes(self):
        self.SetAttributes({attribute.name : HcChannel.ATTRIBUTE_LIMITS_DEFAULT[attribute][HcChannel.ATTR_DEFAULT] for attribute in HcChannel.ATTRIBUTE_LIMITS_DEFAULT.keys()})

    #Functions that should be exposed?
    def Safestate(self):
        self.SetAttribute(HcChannel.Attributes.PowerSequence.name, False)
        self.Apply()

    def CheckStatus(self):
        wordStatus = self._Ltm4680Read(Ltm4680Commands.STATUS_WORD, Ltm4680ByteCount.WORD.value)
        intStatus = wordStatus[1] << 8 | wordStatus[0]
        generalErrors, allErrors = [], []
        generalErrorsIgnoreList = [Ltm4680StatusBits.NONE_OF_THE_ABOVE]

        for statusBit in Ltm4680StatusBits:
            if intStatus & (1 << statusBit.value) and statusBit not in generalErrorsIgnoreList:
                generalErrors.append(statusBit.name)
        allErrors = allErrors + generalErrors

        cmlErrorOccurred = intStatus & (1 << Ltm4680StatusBits.CML_FAULT.value)
        cmlErrors = []
        if cmlErrorOccurred:
            cmlStatus = self._Ltm4680Read(Ltm4680Commands.STATUS_CML, Ltm4680ByteCount.SINGLE_BYTE.value, False)[0]
            for cmlStatusBit in Ltm4680CmlStatusBits:
                if cmlStatus & (1 << cmlStatusBit.value):
                    cmlErrors.append(cmlStatusBit.name)
        allErrors = allErrors + cmlErrors

        mfrErrorOccurred = intStatus & (1 << Ltm4680StatusBits.MFR_SPECIFIC_FAULT.value)
        mfrErrors = []
        if mfrErrorOccurred:
            mfrStatus = self._Ltm4680Read(Ltm4680Commands.STATUS_MFR_SPECIFIC, Ltm4680ByteCount.SINGLE_BYTE.value)[0]
            for mfrStatusBit in Ltm4680MfrStatusBits:
                if mfrStatus & (1 << mfrStatusBit.value):
                    mfrErrors.append(mfrStatusBit.name)
        allErrors = allErrors + mfrErrors

        voutErrorOccurred = intStatus & (1 << Ltm4680StatusBits.VOUT_FAULT.value)
        voutErrors = []
        if voutErrorOccurred:
            voutStatus = self._Ltm4680Read(Ltm4680Commands.STATUS_VOUT, Ltm4680ByteCount.SINGLE_BYTE.value)[0]
            for voutStatusBit in Ltm4680VoutStatusBits:
                if voutStatus & (1 << voutStatusBit.value):
                    voutErrors.append(voutStatusBit.name)
        allErrors = allErrors + voutErrors

        if Log.Enabled:
            print('General Errors:')
            for error in generalErrors:
                print('\tError on HC channel {}: {}'.format(self.channel, error))
            print('Communication Errors:')
            for error in cmlErrors:
                print('\tError on HC channel {}: {}'.format(self.channel, error))
            print('VOUT Errors:')
            for error in voutErrors:
                print('\tError on HC channel {}: {}'.format(self.channel, error))
            print('LTM4680 Specific Errors:')
            for error in mfrErrors:
                print('\tError on HC channel {}: {}'.format(self.channel, error))

        self._Ltm4680Write(Ltm4680Commands.CLEAR_FAULTS, bytes([]), False) #No command data for this command
        return allErrors

    #Ganging interface functions
    class GangConfiguration(Enum):
        Independent = 0
        Slave = 1
        Master = 2

    def SetAsMaster(self):
        self.gangConfig = HcChannel.GangConfiguration.Master

    def SetAsSlave(self):
        self.gangConfig = HcChannel.GangConfiguration.Slave

    def ClearGangConfiguration(self):
        self.gangConfig = HcChannel.GangConfiguration.Independent

    def _EnableGangConfigCheck(self):
        self.checkGangConfig = True

    def _DisableGangConfigCheck(self):
        self.checkGangConfig = False

    def _SetSyncMaster(self):
        self._EnableSyncOutput()
        self._SetSyncClockSource(False) #This generates Sync, and outputs its internal clock on the SYNC pin

    def _SetSyncSlave(self):
        self._DisableSyncOutput()
        self._SetSyncClockSource(True) #Use an externally generated oscillator

    def _SetSyncIndependent(self):
        self._DisableSyncOutput()
        self._SetSyncClockSource(False) #Rely on internally generated clock

    def _SetSyncClockSource(self, isExternal):
        if isExternal:
            self._Ltm4680Write(Ltm4680Commands.FREQUENCY_SWITCH, hil.ConvertToBytes(Ltm4680SwitchingFrequencies.EXTERNAL_OSC.value), False) 
        else:
            self._Ltm4680Write(Ltm4680Commands.FREQUENCY_SWITCH, hil.ConvertToBytes(HcChannel.DEFAULT_SWITCH_FREQUENCY.value), False)

    def _EnableSyncOutput(self):
        bits = (1 << Ltm4680MfrConfigAllBits.DISABLE_SYNC_OUTPUT.value)
        mask = 0xFF
        self._Ltm4680ReadModifyWrite(Ltm4680Commands.MFR_CONFIG_ALL, Ltm4680ByteCount.SINGLE_BYTE.value, bits, mask, False, False) #Clear the "Disable sync output" bit, non-paged command

    def _DisableSyncOutput(self):
        bits = (1 << Ltm4680MfrConfigAllBits.DISABLE_SYNC_OUTPUT.value)
        mask = 0xFF
        self._Ltm4680ReadModifyWrite(Ltm4680Commands.MFR_CONFIG_ALL, Ltm4680ByteCount.SINGLE_BYTE.value, bits, mask, True, False) #Set the "Disable sync output" bit, non-paged command

    #Internal functions
    def _ConfigureFaultResponse(self, command, isImmediate, retryIndefinitely, delayTime=0):
        registerValue = 0
        if command == Ltm4680Commands.VOUT_OV_FAULT_RESPONSE or command == Ltm4680Commands.VOUT_UV_FAULT_RESPONSE:
            if isImmediate:
                registerValue = registerValue | (1 << 7) #Writing 0b10 into bits [7:6] of the FAULT_RESPONSE register
            else:
                if delayTime < 0 or delayTime > 70:
                    raise RuntimeError('Value of delay must be between 0 and 70us')
                encodedDelayTime = delayTime // 10 #Each step represents 10us
                registerValue = registerValue | (1 << 6) #Writing 0b01 into bits [7:6] of the FAULT_RESPONSE register
                registerValue = registerValue | (encodedDelayTime) #Write encoded delay time into bits [2:0] of the FAULT_RESPONSE register
            if retryIndefinitely: #Otherwise (no retry), value of bits [5:3] is 0b000, no work to do
                registerValue = registerValue | (0x7 << 3) #Writing 0b111 into bits [5:3] of the FAULT_REPONSE register
        elif command == Ltm4680Commands.OT_FAULT_RESPONSE or command == Ltm4680Commands.TON_MAX_FAULT_RESPONSE:
            registerValue = 0x80 #Set the most significant bit, indicating that operation depends on retry settings
            if retryIndefinitely: #Otherwise, not modified since 0b000 means "shut off immediately; don't retry"
                registerValue = registerValue | (0x7 << 3) #Writing 0b111 into bits [5:3] of the FAULT_REPONSE register
        elif command == Ltm4680Commands.IOUT_OC_FAULT_RESPONSE:
            if isImmediate:
                registerValue = registerValue | (0x3 << 6) #Writing 0b11 into bits [7:6] of the FAULT_RESPONSE register
            else:
                if delayTime < 0 or delayTime > 112:
                    raise RuntimeError('Value of delay must be between 0 and 112ms')
                encodedDelayTime = delayTime // 16 #Each step represents 16ms
                registerValue = registerValue | (0b10 << 6) #Writing 0b10 into bits [7:6] of the FAULT_RESPONSE register
                registerValue = registerValue | (encodedDelayTime) #Write encoded delay time into bits [2:0] of the FAULT_RESPONSE register
            if retryIndefinitely: #Otherwise (no retry), value of bits [5:3] is 0b000, no work to do
                registerValue = registerValue | (0x7 << 3) #Writing 0b111 into bits [5:3] of the FAULT_REPONSE register
        self._Ltm4680Write(command, bytes([registerValue]))

    def _ValidateUserAttribute(self, attribute, value):
        if attribute in HcChannel.RANGE_ATTRIBUTES:
            minValue = HcChannel.ATTRIBUTE_LIMITS_DEFAULT[attribute][HcChannel.ATTR_MIN_INDEX]
            maxValue = HcChannel.ATTRIBUTE_LIMITS_DEFAULT[attribute][HcChannel.ATTR_MAX_INDEX]
            if value > maxValue or value < minValue:
                raise RuntimeError("{}'s value ({}) cannot be set; it is outside the legal range of values. Min: {}, Max: {}".format(attribute.name, value, minValue, maxValue))
        elif attribute in HcChannel.BOOL_ATTRIBUTES:
            if type(value) is not bool:
                raise RuntimeError("{}'s value ({}) cannot be set; it must be either True or False.".format(attribute.name, value))

    def _ApplySingleAttribute(self, attribute, value):
        internalAttributes = self._UserToInternalAttribute(attribute, value)
        for command, encodedValue in internalAttributes:
            self._Ltm4680Write(command, encodedValue)

    def _SetRunPin(self, turnOn):
        HbiDpsFpga.HcRailControl(self.slot, self.channel, turnOn)

    def _UserToInternalAttribute(self, attribute, value):
        internalAttributes = []
        if attribute == HcChannel.Attributes.FreeDriveCurrentHi or attribute == HcChannel.Attributes.IClampHi:
            internalAttributes.append([Ltm4680Commands.IOUT_OC_FAULT_LIMIT, hil.hilToL11(value)])
            internalAttributes.append([Ltm4680Commands.IOUT_OC_WARN_LIMIT, hil.hilToL11(value)]) #TODO: Need a separate warn limit?
        elif attribute == HcChannel.Attributes.FreeDriveTime:
            internalAttributes.append([Ltm4680Commands.TON_MAX_FAULT_LIMIT, hil.hilToL11(value * 1000)])         #Convert from seconds to milliseconds
        elif attribute == HcChannel.Attributes.FreeDriveRampTime:
            internalAttributes.append([Ltm4680Commands.TON_RISE, hil.hilToL11(value * 1000)]) #TODO: Is faulting after a hard-coded freedrive time good?
        elif attribute == HcChannel.Attributes.UnderVoltageLimit:
            internalAttributes.append([Ltm4680Commands.VOUT_UV_FAULT_LIMIT, hil.hilToL16(value, self.L16Mode)])
            internalAttributes.append([Ltm4680Commands.VOUT_UV_WARN_LIMIT, hil.hilToL16(value, self.L16Mode)]) #TODO: Need a separate warn limit?
        elif attribute == HcChannel.Attributes.OverVoltageLimit:
            internalAttributes.append([Ltm4680Commands.VOUT_MAX, hil.hilToL16(HcChannel.MAX_CLAMP_VOLTAGE, self.L16Mode)])
            internalAttributes.append([Ltm4680Commands.VOUT_OV_FAULT_LIMIT, hil.hilToL16(value, self.L16Mode)])
            internalAttributes.append([Ltm4680Commands.VOUT_OV_WARN_LIMIT, hil.hilToL16(value, self.L16Mode)]) #TODO: Need a separate warn limit?
        elif attribute == HcChannel.Attributes.VForce:
            internalAttributes.append([Ltm4680Commands.VOUT_COMMAND, hil.hilToL16(value, self.L16Mode)])
        elif attribute == HcChannel.Attributes.PowerSequence:
            pass    #PowerSequence is not controlled by a PMBus command at the moment... currently using Run PIN
        return internalAttributes
            
    def _GetUserAttributeValueFromHw(self, attribute):
        if attribute == HcChannel.Attributes.FreeDriveCurrentHi or attribute == HcChannel.Attributes.IClampHi:
            rawL11Value = self._Ltm4680Read(Ltm4680Commands.IOUT_OC_FAULT_LIMIT, Ltm4680ByteCount.L11_READ.value)
            return hil.hilFromL11(rawL11Value)
        elif attribute == HcChannel.Attributes.FreeDriveTime:
            rawL11Value = self._Ltm4680Read(Ltm4680Commands.TON_MAX_FAULT_LIMIT, Ltm4680ByteCount.L11_READ.value)
            return hil.hilFromL11(rawL11Value) / 1000 #Value represents milliseconds, need to convert back
        elif attribute == HcChannel.Attributes.FreeDriveRampTime:
            rawL11Value = self._Ltm4680Read(Ltm4680Commands.TON_RISE, Ltm4680ByteCount.L11_READ.value)
            return hil.hilFromL11(rawL11Value) / 1000 #Value represents milliseconds, need to convert back
        elif attribute == HcChannel.Attributes.UnderVoltageLimit:
            rawL16Value = self._Ltm4680Read(Ltm4680Commands.VOUT_UV_FAULT_LIMIT, Ltm4680ByteCount.L16_READ.value)
            return hil.hilFromL16(rawL16Value, self.L16Mode)
        elif attribute == HcChannel.Attributes.OverVoltageLimit:
            rawL16Value = self._Ltm4680Read(Ltm4680Commands.VOUT_OV_FAULT_LIMIT, Ltm4680ByteCount.L16_READ.value)
            return hil.hilFromL16(rawL16Value, self.L16Mode)
        elif attribute == HcChannel.Attributes.VForce:
            rawL16Value = self._Ltm4680Read(Ltm4680Commands.VOUT_COMMAND, Ltm4680ByteCount.L16_READ.value)
            return hil.hilFromL16(rawL16Value, self.L16Mode)
        elif attribute == HcChannel.Attributes.PowerSequence:
            return HbiDpsFpga.HcRailStatus(self.slot, self.channel)
        else:
            raise RuntimeError("Error: attribute {} not supported".format(attribute.name))

    def _GetNewPwmModeRegisterValue(self, isHighVoltageMode):
        oldMfrPwmMode = self._Ltm4680Read(Ltm4680Commands.MFR_PWM_MODE, 1)[0]
        LOW_VOLTAGE_MODE_BIT_INDEX = 1 #Bit 1 in the MFR_PWM_MODE register = 0 for high voltage, 1 for low voltage
        mask = 1 << LOW_VOLTAGE_MODE_BIT_INDEX
        if isHighVoltageMode:
            return oldMfrPwmMode & (mask ^ 0xFF)  #zero out the bit for high voltage mode
        else:
            return oldMfrPwmMode | mask           #set the bit to 1 for low voltage mode

    def _IsLtm4680Busy(self):
        status = self._Ltm4680Read(Ltm4680Commands.MFR_COMMON, 1, False)
        expectedValue = Ltm4680MfrCommonBitmask.NOT_BUSY.value | Ltm4680MfrCommonBitmask.CALCULATIONS_NOT_PENDING.value | Ltm4680MfrCommonBitmask.NVM_INITIALIZED.value | Ltm4680MfrCommonBitmask.OUTPUTS_NOT_IN_TRANSITION.value
        return (status[0] & expectedValue) != expectedValue

    def _Ltm4680Write(self, command, encodedData, isPaged=True, checkIfBusy=True, timeout=1.0):
        if Log.Enabled:
            if len(encodedData) == 2: #2 bytes needed in bytestring to decode
                fromL11, fromL16 = hil.hilFromL11(encodedData), hil.hilFromL16(encodedData, self.L16Mode)
            else:
                fromL11, fromL16 = 'N/A', 'N/A'
            print("HC PMBUS WRITE SLOT{}_CH{}: {}, {}, {}, {}, {}".format(self.slot, self.channel, command.name, hex(command.value), encodedData, fromL11, fromL16))
        if checkIfBusy:
            busy = self._IsLtm4680Busy()
            startTime = time.time()
            while busy and (time.time() - startTime < timeout):
                time.sleep(0.01)
                busy = self._IsLtm4680Busy()
        page = self.page if isPaged else -1 #-1 indicates "don't bother changing the page"
        hil.hbiDpsLtm4680PmbusPagedWrite(self.slot, self.chip, page, command.value, encodedData)

    def _Ltm4680Read(self, command, readCount, isPaged=True, retryCount=1):
        if retryCount < 0:
            raise RuntimeError('Retry count cannot be negative')
        attempts = 0
        readData = bytes([])
        while True:
            try:
                page = self.page if isPaged else -1 #-1 indicates "don't bother changing the page"
                readData = hil.hbiDpsLtm4680PmbusPagedRead(self.slot, self.chip, page, command.value, readCount)
                break
            except RuntimeError as e:
                attempts = attempts + 1
                if attempts > retryCount:
                    raise RuntimeError('{} Attempted the operation {} times'.format(e, retryCount + 1))
        if Log.Enabled:
            if readCount == 2: #2 bytes needed in bytestring to decode
                fromL11, fromL16 = hil.hilFromL11(readData), hil.hilFromL16(readData, self.L16Mode)
            else:
                fromL11, fromL16 = 'N/A', 'N/A'
            print("HC PMBUS READ SLOT{}_CH{}: {}, {}, {}, {}, {}".format(self.slot, self.channel, command.name, hex(command.value), readData, fromL11, fromL16))
        return readData


    #ByteCount can be up to 2 bytes; this should accommodate all "normal" registers in the Ltm4680
    def _Ltm4680ReadModifyWrite(self, command, byteCount, bits, mask, setBits, paged=True):
        byteStringValue = self._Ltm4680Read(command, byteCount, paged)
        oldRegisterValue = 0
        for byteIndex in range(byteCount):
            oldRegisterValue = oldRegisterValue | (byteStringValue[byteIndex] << (8 * byteIndex))
        if setBits:
            newRegisterValue = oldRegisterValue | bits
        else:
            #This is where the byteCount limitation comes in... cut off any high bits in mask
            inverseAndMaskedBits = bits ^ (0xFFFF & mask)
            newRegisterValue = oldRegisterValue & inverseAndMaskedBits
        byteArray = [(newRegisterValue >> (8 * byteIndex)) & 0xFF for byteIndex in range(byteCount)]
        newRegByteString = bytes(byteArray)
        self._Ltm4680Write(command, newRegByteString, paged)
                
            

if __name__ == "__main__":
    print("This module is a helper module that should not be used directly, even in a script. Please use the main module instead.")
    exit()