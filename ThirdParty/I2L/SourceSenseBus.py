from enum import Enum

from .HilWrap import HilWrap as hil
from .Utility import Sim, Log

class SourceSenseBus:

    class Load(Enum):
        Ohm100k = 0
        Ohm51_1k = 1
        Ohm20k = 2
        Ohm10k = 3
        Ohm4_7k = 4
        Open = 5

    def __init__(self, channelInstrument):
        self.slot = channelInstrument.GetSlotId()

    def SelectSelfTestToVMeasure0(self):
        #TODO: Implement
        raise NotImplementedError()

    def SelectLoad(self, load):
        #TODO: Implement
        raise NotImplementedError()

    def SelectChannel(self, channel):
        #TODO: Implement
        raise NotImplementedError()
    
    def SelectLc(self, channel):
        #TODO: Implement
        raise NotImplementedError()
    
    def SelectHc(self, channel):
        #TODO: Implement
        raise NotImplementedError()

    def ClearAll(self):
        #TODO: Implement
        raise NotImplementedError()

    def ApplyToHw(self):
        #TODO: Implement
        raise NotImplementedError()
    
if __name__ == "__main__":
    print("This module is a helper module that should not be used directly, even in a script. Please use the main module instead.")
    exit()