from .HilWrap import HilWrap as hil
from .Utility import Sim, Log
from .LcChannel import LcChannel
from .HbiDpsFpga import *
from .Ltc2975 import *

class LcGang:
    def __init__(self, channelInstrument):
        self.slot = channelInstrument.GetSlotId()
        self.channelInstrument = channelInstrument
        self.masterChannel = None
        self.slaveChannels = []
        self.dutId = 0 #TODO: Fix for mDUT
        #TODO: Introduce a flag "isGangInProgress" and prevent changes to gang until DisableGang is called?

    def Initialize(self):
        self.Reset()

    def Reset(self):
        channelsToReset = self.slaveChannels.copy()
        if self.masterChannel is not None:
            channelsToReset.append(self.masterChannel)
        for channel in channelsToReset:
            channel = self.channelInstrument.GetChannel(self.dutId, self.masterChannel, LcChannel.RESOURCE_TYPE)
            channel.ClearGangConfiguration()
        self.masterChannel = None
        self.slaveChannels = []

    def ConfigureAsMaster(self, channel):
        if self.masterChannel is not None:
            raise RuntimeError('Cannot configure a second channel as a master. The current master channel is {}; please configure it as a slave before setting a new master channel.'.format(self.masterChannel))
        lcChannel = self.channelInstrument.GetChannel(self.dutId, channel, LcChannel.RESOURCE_TYPE)
        lcChannel.SetAsMaster()
        self.masterChannel = channel

    def ConfigureAsSlave(self, channel):
        if channel in self.slaveChannels:
            return
        lcChannel = self.channelInstrument.GetChannel(self.dutId, channel, LcChannel.RESOURCE_TYPE)
        lcChannel.SetAsSlave()
        self.slaveChannels.append(channel)
        
        if self.masterChannel == channel:
            self.masterChannel = None

    def SequenceGang(self):
        self.ValidateGangConfiguration()
        self._TurnOffRailsInGang()
        self._DisconnectGangSignals()
        self._ApplyAttributesToMasterChannel()
        self._ApplyAttributesToSlaveChannels()
        #Note: There is no functional difference between master and slaves. They all attempt to raise/lower the collective
        #   voltage of the gang together, not a follower-leader relationship.
        self._ConnectGangSignals()
        self._TurnOnRailsInGang()

    def DisableGang(self):
        self._TurnOffRailsInGang()
        self._DisconnectGangSignals()
        self._ConfigureAllAsIndependent()
        
    def ValidateGangConfiguration(self):
        #TODO: This probably breaks crossboard gang... a board doesn't necessarily need a master. It could all be slaves.
        #       The logic needs to be changed to check if crossboard gang input is enabled, and somehow need to get attributes
        #       in between instrument objects in the case of an instrument with all slave rails?
        #       Also need to check that, when crossboard gang output is enabled, that either a master exists or that the input is enabled.

        #First check that all of the channel numbers are valid
        validChannels = range(LcChannel.NUMBER_OF_CHANNELS)
        if self.masterChannel is None:
            raise RuntimeError('A master channel must be set in order to create a gang.')
        elif self.masterChannel not in validChannels:
            raise RuntimeError('Internal error: invalid master channel {}'.format(self.masterChannel))

        for channel in self.slaveChannels:
            if channel not in validChannels:
                raise RuntimeError('Internal error: invalid slave channel {}'.format(self.masterChannel))

        #Now check that they form a contiguous sequence (channels must be immediately adjacent to each other without gaps)
        allChannels = self.slaveChannels.copy()
        allChannels.append(self.masterChannel)
        allChannels.sort()

        if len(allChannels) == 0 or len(allChannels) > HbiDpsFpga.LC_CHANNELS_PER_MODULE:
            raise RuntimeError('Must have at least 1 channel and {} or fewer channels in a gang.'.format(HbiDpsFpga.LC_CHANNELS_PER_MODULE))

        modulesInvolved = set([channel // HbiDpsFpga.LC_CHANNELS_PER_MODULE for channel in allChannels])
        if len(modulesInvolved) > 1:
            raise RuntimeError('Cannot gang across multiple LC Channel Modules.')

        lastChannel = None
        missingRails = []
        for channel in allChannels:
            if lastChannel is not None and channel != lastChannel + 1:
                missingRails = missingRails + [chan for chan in range(lastChannel, channel)]
            lastChannel = channel

        if len(missingRails) > 0:
            raise RuntimeError('Illegal gang - collection of channels are not contiguous. Ganged channels = {}, Missing channels= {}'.format(allChannels, missingRails))

    def _TurnOffRailsInGang(self):
        allChannels = self.slaveChannels.copy()
        if self.masterChannel is not None:
            allChannels.append(self.masterChannel)
        #By this point it has been validated that there is only one module involved in the gang.
        #Figure out which one, and the localized channel numbers for a global page write to turn all on at the same time.
        moduleInGang = self.masterChannel // HbiDpsFpga.LC_CHANNELS_PER_MODULE
        localChannels = [channel % HbiDpsFpga.LC_CHANNELS_PER_MODULE for channel in allChannels]
        Ltc2975.GlobalOperationControl(self.slot, moduleInGang, localChannels, False)

    def _TurnOnRailsInGang(self):
        allChannels = self.slaveChannels.copy()
        if self.masterChannel is not None:
            allChannels.append(self.masterChannel)
        #By this point it has been validated that there is only one module involved in the gang.
        #Figure out which one, and the localized channel numbers for a global page write to turn all on at the same time.
        moduleInGang = self.masterChannel // HbiDpsFpga.LC_CHANNELS_PER_MODULE
        localChannels = [channel % HbiDpsFpga.LC_CHANNELS_PER_MODULE for channel in allChannels]
        Ltc2975.GlobalOperationControl(self.slot, moduleInGang, localChannels, True)
        
        """ Backup
        for channel in allChannels:
            lcChannel = self.channelInstrument.GetChannel(self.dutId, channel, LcChannel.RESOURCE_TYPE)
            lcChannel._DisableGangConfigCheck()
            lcChannel.SetAttribute(LcChannel.Attributes.PowerSequence, True)
            lcChannel.Apply()
            lcChannel._EnableGangConfigCheck()
        """

    def _ApplyAttributesToMasterChannel(self):
        #TODO: This probably breaks crossboard gang... a board doesn't necessarily need a master. It could all be slaves.
        #       The logic needs to be changed to check if crossboard gang input is enabled, and somehow need to get attributes
        #       in between instrument objects in the case of an instrument with all slave rails?
        #       Also need to check that, when crossboard gang output is enabled, that either a master exists or that the input is enabled.
        if self.masterChannel is None:
            raise RuntimeError('Internal error: masterChannel was None, not parameter checked correctly')
        masterChannel = self.channelInstrument.GetChannel(self.dutId, self.masterChannel, LcChannel.RESOURCE_TYPE)
        masterChannel._DisableGangConfigCheck()
        masterChannel.Apply()
        masterChannel._EnableGangConfigCheck()

    def _ApplyAttributesToSlaveChannels(self):
        #TODO: This probably breaks crossboard gang... a board doesn't necessarily need a master. It could all be slaves.
        #       The logic needs to be changed to check if crossboard gang input is enabled, and somehow need to get attributes
        #       in between instrument objects in the case of an instrument with all slave rails?
        #       Also need to check that, when crossboard gang output is enabled, that either a master exists or that the input is enabled.
        if self.masterChannel is None:
            raise RuntimeError('Internal error: masterChannel was None, not parameter checked correctly')
        masterChannel = self.channelInstrument.GetChannel(self.dutId, self.masterChannel, LcChannel.RESOURCE_TYPE)
        masterAttributes = masterChannel.GetAttributes()

        for channel in self.slaveChannels:
            slaveChannel = self.channelInstrument.GetChannel(self.dutId, channel, LcChannel.RESOURCE_TYPE)
            slaveChannel._DisableGangConfigCheck()
            slaveChannel.SetAttributes(masterAttributes)
            slaveChannel.Apply()
            slaveChannel._EnableGangConfigCheck()

    def _ConfigureAllAsIndependent(self):
        allChannels = self.slaveChannels.copy()
        if self.masterChannel is not None:
            allChannels.append(self.masterChannel)
        for channelNumber in allChannels:
            channel = self.channelInstrument.GetChannel(self.dutId, channelNumber, LcChannel.RESOURCE_TYPE)
            channel.ClearGangConfiguration()

    def _ConnectGangSignals(self):
        allChannels = self.slaveChannels.copy()
        if self.masterChannel is not None:
            allChannels.append(self.masterChannel)
        allChannels.sort()
        HbiDpsFpga.LcGangGroupControl(self.slot, allChannels)

    def _DisconnectGangSignals(self):
        allChannels = [] #Empty list indicates that no rails are ganged/all rails are independent
        #Indicate which channels should be modified, including slave in/master out, and the final "False" means "Clear all"
        HbiDpsFpga.LcGangGroupControl(self.slot, allChannels, False) #Force update all switch state


if __name__ == "__main__":
    print("This module is a helper module that should not be used directly, even in a script. Please use the main module instead.")
    exit()