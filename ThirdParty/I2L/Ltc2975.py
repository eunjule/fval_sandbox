from enum import Enum
import time

from .Utility import *
from .HilWrap import HilWrap as hil

class Ltc2975Commands(Enum):             # Description, Permissions, Data Length, Paged? Y/N, Command type
    PAGE = 0x00                          # Channel or page currently selected for any command that supports paging. R/W Byte N Reg 0x00 29
    OPERATION = 0x01                     # Operating mode control. On/Off, Margin High and Margin Low. R/W Byte Y Reg Y 0x00 34
    ON_OFF_CONFIG = 0x02                 # CONTROL pin and PMBus on/off command setting. R/W Byte Y Reg Y 0x1E 35
    CLEAR_FAULTS = 0x03                  # Clear any fault bits that have been set. Send Byte Y NA 64
    WRITE_PROTECT = 0x10                 # Level of protection provided by the device against accidental changes. R/W Byte N Reg Y 0x00 30
    STORE_USER_ALL = 0x15                # Store entire operating memory to EEPROM. Send Byte N NA 45
    RESTORE_USER_ALL = 0x16              # Restore entire operating memory from EEPROM. Send Byte N NA 45
    CAPABILITY = 0x19                    # Summary of PMBus optional communication protocols supported by this device. R Byte N Reg 0xB0 83
    VOUT_MODE = 0x20                     # Output voltage data format and mantissa exponent (2–13). R Byte Y Reg 0x13 51
    VOUT_COMMAND = 0x21                  # Servo target. Nominal DC/DC converter output voltage setpoint. R/W Word Y L16 V Y 1.0 0x2000 51
    VOUT_MAX = 0x24                      # Upper limit on the output voltage the unit can command regardless of any other commands. R/W Word Y L16 V Y 4.0 0x8000 51
    VOUT_MARGIN_HIGH = 0x25              # Margin high DC/DC converter output voltage setting. R/W Word Y L16 V Y 1.05 0x219A 51
    VOUT_MARGIN_LOW = 0x26               # Margin low DC/DC converter output voltage setting. R/W Word Y L16 V Y 0.95 0x1E66 51
    VIN_ON = 0x35                        # Input voltage above which power conversion can be enabled. R/W Word N L11 V Y 10.0 0xD280 47
    VIN_OFF = 0x36                       # Input voltage below which power conversion is disabled. All VOUT_EN pins go off immediately or sequence off after TOFF_DELAY (See Mfr_config_track_enn). R/W Word N L11 V Y 9.0 0xD240 47
    IOUT_CAL_GAIN = 0x38                 # The nominal resistance of the current sense element in mΩ. R/W Word Y L11 mΩ Y 1.0 0xBA00 52
    VOUT_OV_FAULT_LIMIT = 0x40           # Output overvoltage fault limit. R/W Word Y L16 V Y 1.1 0x2333 51
    VOUT_OV_FAULT_RESPONSE = 0x41        # Action to be taken by the device when an output overvoltage fault is detected. R/W Byte Y Reg Y 0x80 59
    VOUT_OV_WARN_LIMIT = 0x42            # Output overvoltage warning limit. R/W Word Y L16 V Y 1.075 0x2266 51
    VOUT_UV_WARN_LIMIT = 0x43            # Output undervoltage warning limit. R/W Word Y L16 V Y 0.925 0x1D9A 51
    VOUT_UV_FAULT_LIMIT = 0x44           # Output undervoltage fault limit. Used for Ton_max_fault and power good deassertion. R/W Word Y L16 V Y 0.9 0x1CCD 51
    VOUT_UV_FAULT_RESPONSE = 0x45        # Action to be taken by the device when an output undervoltage fault is detected. R/W Byte Y Reg Y 0x7F 59
    IOUT_OC_FAULT_LIMIT = 0x46           # Output overcurrent fault limit. R/W Word Y L11 A Y 10.0 0xD280 60
    IOUT_OC_FAULT_RESPONSE = 0x47        # Action to be taken by the device when an output overcurrent fault is detected. R/W Byte Y Reg Y 0x00 60
    IOUT_OC_WARN_LIMIT = 0x4A            # Output overcurrent warning limit. R/W Word Y L11 A Y 5.0 0xCA80 52
    IOUT_UC_FAULT_LIMIT = 0x4B           # Output undercurrent fault limit. Used to detect a reverse current and must be a negative value. R/W Word Y L11 A Y –1.0 0xB400 52
    IOUT_UC_FAULT_RESPONSE = 0x4C        # Action to be taken by the device when an output undercurrent fault is detected. R/W Byte Y Reg Y 0x00 60
    OT_FAULT_LIMIT = 0x4F                # Overtemperature fault limit for the external temperature sensor. R/W Word Y L11 °C Y 65.0 0xEA08 54
    OT_FAULT_RESPONSE = 0x50             # Action to be taken by the device when an overtemperature fault is detected on the external temperature sensor. R/W Byte Y Reg Y 0xB8 61
    OT_WARN_LIMIT = 0x51                 # Overtemperature warning limit for the external temperature sensor R/W Word Y L11 °C Y 60.0 0xE3C0 54
    UT_WARN_LIMIT = 0x52                 # Undertemperature warning limit for the external temperature sensor. R/W Word Y L11 °C Y 0 0x8000 54
    UT_FAULT_LIMIT = 0x53                # Undertemperature fault limit for the external temperature sensor. R/W Word Y L11 °C Y –5.0 0xCD80 54
    UT_FAULT_RESPONSE = 0x54             # Action to be taken by the device when an undertemperature fault is detected on the external temperature sensor. R/W Byte Y Reg Y 0xB8 61
    VIN_OV_FAULT_LIMIT = 0x55            # Input overvoltage fault limit measured at VIN_SNS pin. R/W Word N L11 V Y 15.0 0xD3C0 47
    VIN_OV_FAULT_RESPONSE = 0x56         # Action to be taken by the device when an input overvoltage fault is detected. R/W Byte N Reg Y 0x80 61
    VIN_OV_WARN_LIMIT = 0x57             # Input overvoltage warning limit measured at VIN_SNS pin. R/W Word N L11 V Y 14.0 0xD380 47
    VIN_UV_WARN_LIMIT = 0x58             # Input undervoltage warning limit measured at VIN_SNS pin. R/W Word N L11 V Y 0 0x8000 47
    VIN_UV_FAULT_LIMIT = 0x59            # Input undervoltage fault limit measured at VIN_SNS pin. R/W Word N L11 V Y 0 0x8000 47
    VIN_UV_FAULT_RESPONSE = 0x5A         # Action to be taken by the device when an input undervoltage fault is detected. R/W Byte N Reg Y 0x00 61
    POWER_GOOD_ON = 0x5E                 # Output voltage at or above which a power good should be asserted. R/W Word Y L16 V Y 0.96 0x1EB8 51
    POWER_GOOD_OFF = 0x5F                # Output voltage at or below which a power good should be de-asserted when Mfr_config_all_pwrgd_off_uses_uv is clear. R/W Word Y L16 V Y 0.94 0x1E14 51
    TON_DELAY = 0x60                     # Time from CONTROL pin and/or OPERATION command = ON to VOUT_EN pin = ON. R/W Word Y L11 mS Y 1.0 0xBA00 56
    TON_RISE = 0x61                      # Time from when the VOUT_ENn pin goes high until the LTC2975 optionally soft-connects its DAC and begins to servo the output voltage to the desired value. R/W Word Y L11 mS Y 10.0 0xD280 56
    TON_MAX_FAULT_LIMIT = 0x62           # Maximum time from VOUT_EN pin on assertion that an UV condition will be tolerated before a TON_MAX_FAULT condition results. R/W Word Y L11 mS Y 15.0 0xD3C0 56
    TON_MAX_FAULT_RESPONSE = 0x63        # Action to be taken by the device when a TON_MAX_FAULT event is detected. R/W Byte Y Reg Y 0xB8 62 
    TOFF_DELAY = 0x64                    #Time from CONTROL pin and/or OPERATION command = OFF to VOUT_EN pin = OFF. R/W Word Y L11 mS Y 1.0 0xBA00 56
    STATUS_BYTE = 0x78                   # One byte summary of the unit’s fault condition. R Byte Y Reg NA 65 STATUS_WORD 0x79 Two byte summary of the unit’s fault condition. R Word Y Reg NA 65
    STATUS_WORD = 0x79                   # 
    STATUS_VOUT = 0x7A                   # Output voltage fault and warning status. R Byte Y Reg NA 66
    STATUS_IOUT = 0x7B                   # Output current fault and warning status. R Byte Y Reg NA 66
    STATUS_INPUT = 0x7C                  # Input supply fault and warning status. R Byte N Reg NA 66
    STATUS_TEMPERATURE = 0x7D            # External temperature fault and warning status for READ_TEMPERATURE_1. R Byte Y Reg NA 67
    STATUS_CML = 0x7E                    # Communication and memory fault and warning status. R Byte N Reg NA 67
    STATUS_MFR_SPECIFIC = 0x80           # Manufacturer specific fault and state information. R Byte Y Reg NA 68
    READ_VIN = 0x88                      # Input supply voltage. R Word N L11 V NA 70
    READ_IIN = 0x89                      # DC/DC converter input current. R Word Y L11 A NA 70
    READ_VOUT = 0x8B                     # DC/DC converter output voltage. R Word Y L16 V NA 70
    READ_IOUT = 0x8C                     # DC/DC converter output current. R Word Y L11 A NA 71
    READ_TEMPERATURE_1 = 0x8D            # External diode junction temperature. This is the value used for all temperature related processing, including IOUT_CAL_GAIN. R Word Y L11 °C NA 71
    READ_TEMPERATURE_2 = 0x8E            # Internal junction temperature. R Word N L11 °C NA 71
    READ_POUT = 0x96                     # DC/DC converter output power. R Word Y L11 W NA 71
    READ_PIN = 0x97                      # DC/DC converter input power. R Word Y L11 W NA 70
    PMBUS_REVISION = 0x98                # PMBus revision supported by this device. Current revision is 1.1. R Byte N Reg 0x11 83
    USER_DATA_00 = 0xB0                  # Manufacturer reserved for LTpowerPlay. R/W Word N Reg Y NA 83
    USER_DATA_01 = 0xB1                  # Manufacturer reserved for LTpowerPlay. R/W Word Y Reg Y NA 83
    USER_DATA_02 = 0xB2                  # OEM Reserved. R/W Word N Reg Y NA 83
    USER_DATA_03 = 0xB3                  # Scratchpad location. R/W Word Y Reg Y 0x0000 83
    USER_DATA_04 = 0xB4                  # Scratchpad location. R/W Word N Reg Y 0x0000 83
    MFR_LTC_RESERVED_1 = 0xB5            # Manufacturer reserved. R/W Word Y Reg Y NA 83
    MFR_T_SELF_HEAT = 0xB8               # Calculated temperature rise due to selfheating of output current sense device above value measured by external temperature sensor. R Word Y L11 °C NA 54
    MFR_IOUT_CAL_GAIN_TAU_INV = 0xB9     # Inverse of time constant for Mfr_t_self_heat changes scaled by 4 • tCONV_SENSE. R/W Word Y L11 Y 0.0 0x8000 54
    MFR_IOUT_CAL_GAIN_THETA = 0xBA       # Thermal resistance from inductor core to point measured by external temperature sensor. R/W Word Y L11 °C/W Y 0.0 0x8000 54
    MFR_READ_IOUT = 0xBB                 # Alternate data format for READ_IOUT. One LSB = 2.5mA. R Word Y CF 2.5mA NA 72
    MFR_LTC_RESERVED_2 = 0xBC            # Manufacturer reserved. R/W Word Y Reg NA 83
    MFR_EE_UNLOCK = 0xBD                 # Unlock user EEPROM for access by MFR_EE_ERASE and MFR_EE_DATA commands. R/W Byte N Reg NA 46
    MFR_EE_ERASE = 0xBE                  # Initialize user EEPROM for bulk programming by MFR_EE_DATA. R/W Byte N Reg NA 46
    MFR_EE_DATA = 0xBF                   # Data transferred to and from EEPROM using sequential PMBus word reads or writes. Supports bulk programming. R/W Word N Reg NA 46
    MFR_EIN = 0xC0                       # Input Energy data bytes. R Block N Reg NA 48
    MFR_EIN_CONFIG = 0xC1                # Configuration register for energy and input current. R/W Byte N Reg Y 0x00 49
    MFR_SPECIAL_LOT = 0xC2               # Customer dependent codes that identify the factory programmed user configuration stored in EEPROM. Contact factory for default value. R Byte Y Reg Y NA 83
    MFR_IIN_CAL_GAIN_TC = 0xC3           # Temperature coefficient applied to IIN_CAL_GAIN. R/W Word N CF ppm Y 0x0000 50
    MFR_IIN_PEAK = 0xC4                  # Maximum measured value of READ_IIN R Word Y L11 A NA 71
    MFR_IIN_MIN = 0xC5                   # Minimum measured value of READ_IIN. R Word Y L11 A NA 71
    MFR_PIN_PEAK = 0xC6                  # Maximum measured value of READ_PIN. R Word Y L11 W NA 71
    MFR_PIN_MIN = 0xC7                   # Minimum measured value of READ_PIN. R Word Y L11 W NA 71
    MFR_COMMAND_PLUS = 0xC8              # Alternate access to block read and other data. Commands for all additional hosts. R/W Word N Reg 31
    MFR_DATA_PLUS0 = 0xC9                # Alternate access to block read and other data. Data for additional host 0. R/W Word N Reg 31
    MFR_DATA_PLUS1 = 0xCA                # Alternate access to block read and other data. Data for additional host 1. R/W Word N Reg 31
    MFR_CONFIG_LTC2975 = 0xD0            # Configuration bits that are channel specific. R/W Word Y Reg Y 0x0080 36
    MFR_CONFIG_ALL_LTC2975 = 0xD1        # Configuration bits that are common to all pages. R/W Word N Reg Y 0x0F7B 43
    MFR_FAULTB0_PROPAGATE = 0xD2         # Configuration that determines if a channel’s faulted off state is propagated to the FAULTB0 pin. R/W Byte Y Reg Y 0x00 63
    MFR_FAULTB1_PROPAGATE = 0xD3         # Configuration that determines if a channel’s faulted off state is propagated to the FAULTB1 pin. R/W Byte Y Reg Y 0x00 63
    MFR_PWRGD_EN = 0xD4                  # Configuration that maps WDI/RESETB status and individual channel power good to the PWRGD pin. R/W Word N Reg Y 0x0000 57
    MFR_FAULTB0_RESPONSE = 0xD5          # Action to be taken by the device when the FAULTB0 pin is asserted low. R/W Byte N Reg Y 0x00 63
    MFR_FAULTB1_RESPONSE = 0xD6          # Action to be taken by the device when the FAULTB1 pin is asserted low. R/W Byte N Reg Y 0x00 63
    MFR_IOUT_PEAK = 0xD7                 # Maximum measured value of READ_IOUT. R Word Y L11 A NA 73
    MFR_IOUT_MIN = 0xD8                  # Minimum measured value of READ_IOUT. R Word Y L11 A NA 73
    MFR_CONFIG2_LTC2975 = 0xD9           # Configuration bits that are channel specific R/W Byte N Reg Y 0x00 38
    MFR_CONFIG3_LTC2975 = 0xDA           # Configuration bits that are channel specific R/W Byte N Reg Y 0x00 39
    MFR_RETRY_DELAY = 0xDB               # Retry interval during FAULT retry mode. R/W Word N L11 mS Y 200 0xF320 62
    MFR_RESTART_DELAY = 0xDC             # Delay from actual CONTROL active edge to virtual CONTROL active edge. R/W Word N L11 mS Y 400 0xFB20 57
    MFR_VOUT_PEAK = 0xDD                 # Maximum measured value of READ_VOUT. R Word Y L16 V NA 73
    MFR_VIN_PEAK = 0xDE                  # Maximum measured value of READ_VIN. R Word N L11 V NA 73
    MFR_TEMPERATURE_1_PEAK = 0xDF        # Maximum measured value of READ_TEMPERATURE_1. R Word Y L11 °C NA 73
    MFR_DAC = 0xE0                       # Manufacturer register that contains the code of the 10-bit DAC. R/W Word Y Reg 0x0000 51
    MFR_POWERGOOD_ASSERTION_DELAY = 0xE1 # Power-good output assertion delay. R/W Word N L11 mS Y 100 0xEB20 58
    MFR_WATCHDOG_T_FIRST = 0xE2          # First watchdog timer interval. R/W Word N L11 mS Y 0 0x8000 58
    MFR_WATCHDOG_T = 0xE3                # Watchdog timer interval. R/W Word N L11 mS Y 0 0x8000 58
    MFR_PAGE_FF_MASK = 0xE4              # Configuration defining which channels respond to global page commands (PAGE=0xFF). R/W Byte N Reg Y 0x0F 30
    MFR_PADS = 0xE5                      # Current state of selected digital I/O pads. R/W Word N Reg NA 68
    MFR_I2C_BASE_ADDRESS = 0xE6          # Base value of the I2C/SMBus address byte. R/W Byte N Reg Y 0x5C 31
    MFR_SPECIAL_ID = 0xE7                # Manufacturer code for identifying the LTC2975. R Word N Reg Y 547 0x0223 83
    MFR_IIN_CAL_GAIN = 0xE8              # The nominal resistance of the input current sense element in mΩ. R/W Word N L11 mΩ Y 1.0 0xBA00 49
    MFR_VOUT_DISCHARGE_THRESHOLD = 0xE9  # Coefficient used to multiply VOUT_COMMAND in order to determine VOUT off threshold voltage. R/W Word Y L11 Y 2.0 0xC200 51
    MFR_FAULT_LOG_STORE = 0xEA           # Command a transfer of the fault log from RAM to EEPROM. Send Byte N NA 74
    MFR_FAULT_LOG_RESTORE = 0xEB         # Command a transfer of the fault log previously stored in EEPROM back to RAM. Send Byte N NA 74
    MFR_FAULT_LOG_CLEAR = 0xEC           # Initialize the EEPROM block reserved for fault logging and clear any previous fault logging locks. Send Byte N NA 75
    MFR_FAULT_LOG_STATUS = 0xED          # Fault logging status. R Byte N Reg Y NA 75
    MFR_FAULT_LOG = 0xEE                 # Fault log data bytes. This sequentially retrieved data is used to assemble a complete fault log. R Block N Reg Y NA 75
    MFR_COMMON = 0xEF                    # Manufacturer status bits that are common across multiple LTC chips. R Byte N Reg NA 69
    MFR_IOUT_CAL_GAIN_TC = 0xF6          # Temperature coefficient applied to IOUT_CAL_GAIN. R/W Word Y CF ppm Y 0x0000 53
    MFR_RETRY_COUNT = 0xF7               # Retry count for all faulted off conditions that enable retry. R/W Byte N Reg Y 0x07 62
    MFR_TEMP_1_GAIN = 0xF8               # Inverse of external diode temperature non ideality factor. One LSB = 2–14. R/W Word Y CF Y 1 0x4000 54
    MFR_TEMP_1_OFFSET = 0xF9             # Offset value for the external temperature. R/W Word Y L11 °C Y 0 0x8000 54
    MFR_IOUT_SENSE_VOLTAGE = 0xFA        # Absolute value of VISENSEP – VISENSEM. One LSB = 3.05µV. R Word Y CF 3.05µV NA 73
    MFR_VOUT_MIN = 0xFB                  # Minimum measured value of READ_VOUT. R Word Y L16 V NA 73
    MFR_VIN_MIN = 0xFC                   # Minimum measured value of READ_VIN. R Word N L11 V NA 73
    MFR_TEMPERATURE_1_MIN = 0xFD         # Minimum measured value of READ_TEMPERATURE_1. R Word Y L11 °C NA 74

class Ltc2975MfrCommonBitmask(Enum):
    MFR_COMON_ALERTB = 0x80
    MFR_COMMON_BUSYB = 0x40
    RESERVED = 0x3C
    MFR_COMMON_SHARE_CLK = 0x02
    MFR_COMMON_WRITE_PROTECT = 0x01

class Ltc2975OnOffConfigBits(Enum):
    ON_OFF_CONFIG_CONTROLLED_ON = 4
    ON_OFF_CONFIG_USE_PMBUS = 3
    ON_OFF_CONFIG_USE_CONTROL = 2
    ON_OFF_CONFIG_CONTROL_FAST_OFF = 0

class Ltc2975ByteCount(Enum):
    L11_READ = 2
    L16_READ = 2
    SINGLE_BYTE = 1
    WORD = 2

class Ltc2975OperationMode(Enum):
    #Note: These are only valid when using PMBUS commands to control on/off state (which is POR)
    OFF = 0x00
    SEQUENCE_ON = 0x80
    MARGIN_LOW_IGNORE_FAULTS = 0x94
    MARGIN_LOW = 0x98
    MARGIN_HIGH_IGNORE_FAULTS = 0xA4
    MARGIN_HIGH = 0xA8
    SEQUENCE_OFF_MARGIN_NOMINAL = 0x40
    SEQUENCE_OFF_MARGIN_LOW_IGNORE_FAULTS = 0x54
    SEQUENCE_OFF_MARGIN_LOW = 0x58
    SEQUENCE_OFF_MARGIN_HIGH_IGNORE_FAULTS = 0x64
    SEQUENCE_OFF_MARGIN_HIGH = 0x68

class Ltc2975StatusBits(Enum):
    NONE_OF_THE_ABOVE = 0
    CML_FAULT = 1
    TEMPERATURE_FAULT = 2
    VIN_UV = 3 #Datasheet says this isn't supported
    IOUT_OC = 4
    VOUT_OV = 5
    OFF = 6
    BUSY_FAULT = 7
    UNKNOWN_FAULT = 8
    OTHER_FAULT = 9
    FANS_FAULT = 10
    POWER_NOT_GOOD = 11
    MFR_SPECIFIC_FAULT = 12
    INPUT_FAULT = 13
    IOUT_FAULT = 14
    VOUT_FAULT = 15

class Ltc2975CmlStatusBits(Enum):
    UNKNOWN_FAULT = 0
    PMBUS_FAULT = 1
    #Reserved = 2
    PROCESSOR_FAULT = 3
    MEMORY_FAULT = 4
    PEC_FAILED = 5
    INVALID_DATA = 6
    INVALID_COMMAND = 7

class Ltc2975MfrStatusBits(Enum):
    WATCHDOG_FAULT = 0
    AUXFAULTB_FAULTED_OFF = 1
    DAC_SATURATED = 2
    DAC_CONNECTED = 3
    SERVO_TARGET_REACHED = 4
    FAULT0_IN = 5
    FAULT1_IN = 6
    DISCHARGE_FAULT = 7

class Ltc2975VoutStatusBits(Enum):
    TRACKING_ERROR = 0 #Datasheet says it's not supported and will always return 0
    TOFF_MAX_WARN = 1
    TON_MAX_FAULT = 2
    VOUT_MAX_WARNING = 3
    VOUT_UV_FAULT = 4
    VOUT_UV_WARNING = 5
    VOUT_OV_WARNING = 6
    VOUT_OV_FAULT = 7

class Ltc2975IoutStatusBits(Enum):
    UC_FAULT = 4
    OC_WARN = 5
    OC_FAULT = 7
    #Everything else is not supported and always returns 0

class Ltc2975:
    L16_MODE = 0x13 #Corresponds to mean 2**-13, hardcoded in LTC2975 part
    ON_OPERATION = Ltc2975OperationMode.SEQUENCE_ON
    OFF_OPERATION = Ltc2975OperationMode.OFF
    GLOBAL_PAGE = 0xFF

    #This function turns rails within a module on or off via the operation command and the global page
    @staticmethod
    def GlobalOperationControl(slot, chipNumber, localChannels, turnOn):
        globalPageMask = 0x00
        for channel in localChannels:
            globalPageMask |= (1 << channel)
        #Module level command to change which channels respond to Page=0xFF commands
        Ltc2975.Write(slot, chipNumber, Ltc2975Commands.MFR_PAGE_FF_MASK, bytes([globalPageMask]))
        operationValue = Ltc2975.ON_OPERATION.value if turnOn else Ltc2975.OFF_OPERATION.value
        Ltc2975.Write(slot, chipNumber, Ltc2975Commands.OPERATION, bytes([operationValue]), Ltc2975.GLOBAL_PAGE)

    @staticmethod
    def Write(slot, chipNumber, command, encodedData, page=None, checkIfBusy=True, timeout=1.0):
        if Log.Enabled:
            if len(encodedData) == 2: #2 bytes needed in bytestring to decode
                fromL11, fromL16 = hil.hilFromL11(encodedData), hil.hilFromL16(encodedData, Ltc2975.L16_MODE)
            else:
                fromL11, fromL16 = 'N/A', 'N/A'
            print("LC PMBUS WRITE SLOT{}_CHIP{}_PAGE{}: {}, {}, {}, {}, {}".format(slot, chipNumber, page, command.name, hex(command.value), encodedData, fromL11, fromL16))
        if checkIfBusy:
            busy = Ltc2975._IsBusy(slot, chipNumber)
            startTime = time.time()
            currentTime = startTime
            while busy and (currentTime - startTime < timeout):
                time.sleep(0.01)
                busy = Ltc2975._IsBusy(slot, chipNumber)
                currentTime = time.time()
            if currentTime - startTime > timeout:
                raise RuntimeError('Timeout({} seconds): LC PMBUS WRITE SLOT{}_CHIP{}_PAGE{}: {}, {}, {}'.format(timeout, slot, chipNumber, page, command.name, hex(command.value), encodedData))
        internalPage = page if page is not None else -1
        hil.hbiDpsLtc2975PmbusPagedWrite(slot, chipNumber, internalPage, command.value, encodedData)

    @staticmethod
    def Read(slot, chipNumber, command, byteReadCount, page=None, retryCount=1):
        if retryCount < 0:
            raise RuntimeError('Retry count cannot be negative')
        attempts = 0
        readData = bytes([])
        while True:
            try:
                internalPage = page if page is not None else -1 #Translate to HIL's conventions
                readData = hil.hbiDpsLtc2975PmbusPagedRead(slot, chipNumber, internalPage, command.value, byteReadCount)
                break
            except RuntimeError as e:
                attempts = attempts + 1
                if attempts > retryCount:
                    raise RuntimeError('{} Attempted the operation {} times'.format(e, retryCount + 1))
        if Log.Enabled:
            if byteReadCount == 2: #2 bytes needed in bytestring to decode
                fromL11, fromL16 = hil.hilFromL11(readData), hil.hilFromL16(readData, Ltc2975.L16_MODE)
            else:
                fromL11, fromL16 = 'N/A', 'N/A'
            print("LC PMBUS READ SLOT{}_CHIP{}_PAGE{}: {}, {}, {}, {}, {}".format(slot, chipNumber, page, command.name, hex(command.value), readData, fromL11, fromL16))
        return readData

    @staticmethod
    def _IsBusy(slot, chipNumber):
        status = Ltc2975.Read(slot, chipNumber, Ltc2975Commands.MFR_COMMON, 1, None)[0]
        expectedValue = Ltc2975MfrCommonBitmask.MFR_COMMON_BUSYB.value #This bit is cleared if the part is busy; set if it can receive new commands
        #Since this function is called "IsBusy", we return true if the bit is not set.
        return (status & expectedValue) != expectedValue