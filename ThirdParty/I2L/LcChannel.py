import time

from .HilWrap import HilWrap as hil
from .Utility import Sim, Log
from .HbiDpsFpga import HbiDpsFpga
from .Ltc2975 import *

class LcChannel:
    RESOURCE_TYPE = "Lc"
    NUMBER_OF_CHANNELS = HbiDpsFpga.LC_NUMBER_OF_MODULES * HbiDpsFpga.LC_CHANNELS_PER_MODULE
    IOUT_SENSE_RESISTOR_MOHM = 10
    POWER_GOOD_ON_THRESHOLD_V = 0.6
    POWER_GOOD_OFF_THRESHOLD_V = 0.55
    MIN_VOLTAGE, MAX_VOLTAGE, MIN_CLAMP_VOLTAGE, MAX_CLAMP_VOLTAGE = 0.5, 5.5, 0.0, 6.0
    MIN_CURRENT, MIN_CURRENT_CLAMP_LO, MAX_CURRENT_CLAMP_HI = 0.0, -2.0, 5.0 #For clamp definitions. Without the soft-start capacitor rework, this could be too low due to start-up overshoot behavior.
    MIN_FREEDRIVE_TIME_MS, MAX_FREEDRIVE_TIME_MS = 0.01, 655 #A value of 0 would disable Freedrive Faults, limiting to 10us (minimum)
    DEFAULT_FREEDRIVE_MS = 0.0 #Turn on as fast as possible
    DEFAULT_VFORCE = 1.0

    #Members of this class have a ".name" member variable to retrieve string representation
    class Attributes(Enum):
        FreeDriveCurrentHi = 0
        FreeDriveCurrentLo = 1
        FreeDriveTime = 2
        IClampHi = 3
        IClampLo = 4
        UnderVoltageLimit = 5
        OverVoltageLimit = 6
        PowerSequence = 7
        VForce = 8
        #StartMeasurement = 8
        #SampleInfo = 9

    ATTRIBUTE_LIMITS_DEFAULT = {
        # ATTRIBUTE                     | MIN, MAX, DEFAULT                                                                     | Notes   
        #------------------------------------------------------------------------------------------------------------------------------------------------------
        Attributes.FreeDriveCurrentHi   : [MIN_CURRENT, MAX_CURRENT_CLAMP_HI, MAX_CURRENT_CLAMP_HI],
        Attributes.FreeDriveCurrentLo   : [MIN_CURRENT_CLAMP_LO, MIN_CURRENT, MIN_CURRENT_CLAMP_LO],
        Attributes.FreeDriveTime        : [MIN_FREEDRIVE_TIME_MS/1000, MAX_FREEDRIVE_TIME_MS/1000, MAX_FREEDRIVE_TIME_MS/1000],     #Seconds
        Attributes.IClampHi             : [MIN_CURRENT, MAX_CURRENT_CLAMP_HI, MAX_CURRENT_CLAMP_HI],                                #Amperes
        Attributes.IClampLo             : [MIN_CURRENT_CLAMP_LO, MIN_CURRENT, MIN_CURRENT_CLAMP_LO],
        Attributes.UnderVoltageLimit    : [0.0, MAX_VOLTAGE, 0.0],
        Attributes.OverVoltageLimit     : [MIN_VOLTAGE, MAX_CLAMP_VOLTAGE, MAX_CLAMP_VOLTAGE],
        Attributes.PowerSequence        : [False, True, False],                                                                     #Set to True to turn on the rail
        Attributes.VForce               : [MIN_VOLTAGE, MAX_VOLTAGE, DEFAULT_VFORCE]
        #Attributes.StartMeasurement     : [False, True, False],                                                                    #Set to True to turn on measurements. Not yet supported
        #SampleInfo not supported
    }

    RANGE_ATTRIBUTES = [Attributes.FreeDriveCurrentHi, Attributes.FreeDriveCurrentLo, Attributes.FreeDriveTime, Attributes.IClampHi, Attributes.IClampLo, Attributes.UnderVoltageLimit, Attributes.OverVoltageLimit, Attributes.VForce]
    BOOL_ATTRIBUTES = [Attributes.PowerSequence]

    ATTR_MIN_INDEX, ATTR_MAX_INDEX, ATTR_DEFAULT = 0, 1, 2

    @staticmethod
    def InitAllLcModules(slot):
        allModules = HbiDpsFpga.ALL_LC_MODULES 
        HbiDpsFpga.LcModuleGroupControl(slot, allModules, False)    #Disable control lines of all LC modules
        HbiDpsFpga.LcGangSwitchGroupControl(slot, allModules, True) #Enable serial communication of switches that control LC gang
        time.sleep(0.001)   #Testing if delay between disabling shutdown pins and writing to the switches fixes FVAL issues
        HbiDpsFpga.LcGangGroupControl(slot, [], False)              #Tell this function that nothing is ganged in any fashion to disable all ganging, and force update for all rails

        #TODO: Refactor needed for HC and LC such that hierarcy is instrument->modules->channels instead of instrument->channels?
        for moduleNumber in HbiDpsFpga.ALL_LC_MODULES:
            Ltc2975.Write(slot, moduleNumber, Ltc2975Commands.WRITE_PROTECT, bytes([0x00]))                           # Enable writes to all commands
            Ltc2975.Write(slot, moduleNumber, Ltc2975Commands.VIN_ON, hil.hilToL11(9))                                # Set Vin_ON Threshold to 9v (0xd240 l11)
            Ltc2975.Write(slot, moduleNumber, Ltc2975Commands.VIN_OFF, hil.hilToL11(8))                               # Set Vin_Off Threshold to 8v (0xd200 l11)
            Ltc2975.Write(slot, moduleNumber, Ltc2975Commands.VIN_OV_FAULT_LIMIT, hil.hilToL11(15))                   # Set Vin_OV_Fault Threshold to 15v (0xd3c0 l11)
            Ltc2975.Write(slot, moduleNumber, Ltc2975Commands.VIN_OV_FAULT_RESPONSE, bytes([0x00]))                   #
            Ltc2975.Write(slot, moduleNumber, Ltc2975Commands.VIN_OV_WARN_LIMIT, hil.hilToL11(14))                    # (0xd380 l11)
            Ltc2975.Write(slot, moduleNumber, Ltc2975Commands.VIN_UV_WARN_LIMIT, hil.hilToL11(0))                     # (0x8000 l11)
            Ltc2975.Write(slot, moduleNumber, Ltc2975Commands.VIN_UV_FAULT_LIMIT, hil.hilToL11(0))                    # (0x8000 l11)
            Ltc2975.Write(slot, moduleNumber, Ltc2975Commands.VIN_UV_FAULT_RESPONSE, bytes([0x00]))                   # 
            Ltc2975.Write(slot, moduleNumber, Ltc2975Commands.USER_DATA_00, hil.ConvertToBytes(0x0000))               # TODO: Remove these USER_DATA writes?
            Ltc2975.Write(slot, moduleNumber, Ltc2975Commands.USER_DATA_02, hil.ConvertToBytes(0x0000))               # 
            Ltc2975.Write(slot, moduleNumber, Ltc2975Commands.USER_DATA_04, hil.ConvertToBytes(0xC3F5))               # 
            Ltc2975.Write(slot, moduleNumber, Ltc2975Commands.MFR_EIN_CONFIG, bytes([0x00]))                          # high range, 100mV
            Ltc2975.Write(slot, moduleNumber, Ltc2975Commands.MFR_IIN_CAL_GAIN_TC, hil.ConvertToBytes(0x0000))        # 
            Ltc2975.Write(slot, moduleNumber, Ltc2975Commands.MFR_CONFIG_ALL_LTC2975, hil.ConvertToBytes(0x0F7B))     # 
            Ltc2975.Write(slot, moduleNumber, Ltc2975Commands.MFR_PWRGD_EN, hil.ConvertToBytes(0x0000))               # 
            Ltc2975.Write(slot, moduleNumber, Ltc2975Commands.MFR_FAULTB0_RESPONSE, bytes([0x00]))                    #
            Ltc2975.Write(slot, moduleNumber, Ltc2975Commands.MFR_FAULTB1_RESPONSE, bytes([0x00]))                    #
            Ltc2975.Write(slot, moduleNumber, Ltc2975Commands.MFR_CONFIG2_LTC2975, bytes([0x00]))                     #
            Ltc2975.Write(slot, moduleNumber, Ltc2975Commands.MFR_CONFIG3_LTC2975, bytes([0x00]))                     #
            Ltc2975.Write(slot, moduleNumber, Ltc2975Commands.MFR_RETRY_DELAY, hil.hilToL11(200))                     # 200mS, LT tools sets to 0x0A58 (279nS?)
            Ltc2975.Write(slot, moduleNumber, Ltc2975Commands.MFR_RESTART_DELAY, hil.hilToL11(400))                   # 400mS
            Ltc2975.Write(slot, moduleNumber, Ltc2975Commands.MFR_POWERGOOD_ASSERTION_DELAY, hil.hilToL11(100))       # 100mS, LT tools sets to 0x8000 (0mS) 
            Ltc2975.Write(slot, moduleNumber, Ltc2975Commands.MFR_WATCHDOG_T_FIRST, hil.hilToL11(0))                  # 0mS
            Ltc2975.Write(slot, moduleNumber, Ltc2975Commands.MFR_WATCHDOG_T, hil.hilToL11(0))                        # 0mS
            Ltc2975.Write(slot, moduleNumber, Ltc2975Commands.MFR_PAGE_FF_MASK, bytes([0x0F]))                        # 
            Ltc2975.Write(slot, moduleNumber, Ltc2975Commands.MFR_IIN_CAL_GAIN, hil.hilToL11(10))                     # 10mOhm (0xD280) default is 1mOhm
            Ltc2975.Write(slot, moduleNumber, Ltc2975Commands.MFR_RETRY_COUNT, bytes([0x07]))                         # 

    @staticmethod
    def EnableAllChannels(slot):
        allModules = HbiDpsFpga.ALL_LC_MODULES 
        HbiDpsFpga.LcModuleGroupControl(slot, allModules, True)    #Enable control lines of all LC modules

    def __init__(self, channelInstrument, lcGang, channel):
        self.slot = channelInstrument.GetSlotId()
        self.channel = channel
        self.chip = channel // HbiDpsFpga.LC_CHANNELS_PER_MODULE
        self.page = channel % HbiDpsFpga.LC_CHANNELS_PER_MODULE
        self.lcGangController = lcGang

    def Initialize(self):
        self.ClearGangConfiguration()
        self._EnableGangConfigCheck()
        self.ResetAttributes()

        self.L16Mode = self._Ltc2975Read(Ltc2975Commands.VOUT_MODE, 1)[0] #Should always return 0x13 (corresponds to 2**-13)
        #ON_OFF_CONFIG setting requires that the control pin be logic high, and the OPERATION register is used to turn the rail on and off
        onOffConfig = (1 << Ltc2975OnOffConfigBits.ON_OFF_CONFIG_CONTROLLED_ON.value) | (1 << Ltc2975OnOffConfigBits.ON_OFF_CONFIG_USE_CONTROL.value) | (1 << Ltc2975OnOffConfigBits.ON_OFF_CONFIG_USE_PMBUS.value)
        self._Ltc2975Write(Ltc2975Commands.ON_OFF_CONFIG, bytes([onOffConfig]))
        self.Safestate()
        self._Ltc2975Write(Ltc2975Commands.IOUT_CAL_GAIN, hil.hilToL11(LcChannel.IOUT_SENSE_RESISTOR_MOHM))                   # Iout sense resistor val in mOhm: 10mOhm (0xD280) default is 1mOhm
        self._Ltc2975Write(Ltc2975Commands.POWER_GOOD_ON, hil.hilToL16(LcChannel.POWER_GOOD_ON_THRESHOLD_V, self.L16Mode))    # POWER_GOOD signal on if output voltage > 0.6v
        self._Ltc2975Write(Ltc2975Commands.POWER_GOOD_OFF, hil.hilToL16(LcChannel.POWER_GOOD_OFF_THRESHOLD_V, self.L16Mode))  # POWER_GOOD signal off if output voltage < 0.55
        self._Ltc2975Write(Ltc2975Commands.MFR_IOUT_CAL_GAIN_TAU_INV, hil.hilToL11(0))                              # TBD: NEEDED? default & LT tool
        self._Ltc2975Write(Ltc2975Commands.MFR_IOUT_CAL_GAIN_THETA, hil.hilToL11(1))                                # TBD: NEEDED? LT tool = 1, default = 0
        
        #TODO: Travis is experimenting with flipping this one bit, controls some aspects of servo mode and turning the rail on
        #self._Ltc2975Write(Ltc2975Commands.MFR_CONFIG_LTC2975,hil.ConvertToBytes(0x0402))                          # CONTROL0, 8mV resolution (0 to 6v), Do not continuously servo VOUT after reaching initial target.
        self._Ltc2975Write(Ltc2975Commands.MFR_CONFIG_LTC2975,hil.ConvertToBytes(0x0420))                           # CONTROL0, 8mV resolution (0 to 6v), Do not continuously servo VOUT after reaching initial target, hard connect DAC, DAC_gain_0.

        #TODO: Move to a "SetFaultResponses" function
        self._Ltc2975Write(Ltc2975Commands.MFR_FAULTB0_PROPAGATE, bytes([0x01]))      # assert FAULTB0 low on channel fault
        self._Ltc2975Write(Ltc2975Commands.MFR_FAULTB1_PROPAGATE, bytes([0x01]))      # assert FAULTB1 low on channel fault
        self._Ltc2975Write(Ltc2975Commands.UT_FAULT_RESPONSE, bytes([0x80]))          # shutdown, no retry
        self._Ltc2975Write(Ltc2975Commands.OT_FAULT_RESPONSE, bytes([0x80]))          # shutdown, no retry
        self._Ltc2975Write(Ltc2975Commands.IOUT_UC_FAULT_RESPONSE, bytes([0x84]))     # Allow 10mS glitch, then fold. TODO: Re-evaluate after everyone has soft-start cap rework 
        self._Ltc2975Write(Ltc2975Commands.VOUT_UV_FAULT_RESPONSE, bytes([0x7F]))     # continue, Retry X times (default)
        self._Ltc2975Write(Ltc2975Commands.IOUT_OC_FAULT_RESPONSE, bytes([0x84]))     # Allow 10mS glitch, then fold. TODO: Re-evaluate after everyone has soft-start cap rework 
        self._Ltc2975Write(Ltc2975Commands.VOUT_OV_FAULT_RESPONSE, bytes([0x80]))     # shut down immediately (default)
        self._Ltc2975Write(Ltc2975Commands.TON_MAX_FAULT_RESPONSE, bytes([0x80]))     # shutdown, no retry

        self._Ltc2975Write(Ltc2975Commands.OT_FAULT_LIMIT, hil.hilToL11(85))                 # 85 per LT tool, default is 65
        self._Ltc2975Write(Ltc2975Commands.OT_WARN_LIMIT, hil.hilToL11(70))                  # 70 per LT tool, default is 60
        self._Ltc2975Write(Ltc2975Commands.UT_WARN_LIMIT, hil.hilToL11(0))                   # 0C
        self._Ltc2975Write(Ltc2975Commands.UT_FAULT_LIMIT, hil.hilToL11(-5))                 # -5C

        self._Ltc2975Write(Ltc2975Commands.TOFF_DELAY, hil.hilToL11(0))                      # No turn-off delay
        self._Ltc2975Write(Ltc2975Commands.TON_DELAY, hil.hilToL11(0))
        self._Ltc2975Write(Ltc2975Commands.TON_RISE, hil.hilToL11(0))

        #Effectively disable warnings with out-of-range values
        #The practical limit of L16Mode = 0x13 is the magnitude of values is 0 <= value < 8
        self._Ltc2975Write(Ltc2975Commands.VOUT_UV_WARN_LIMIT, hil.hilToL16(0, self.L16Mode))
        self._Ltc2975Write(Ltc2975Commands.IOUT_OC_WARN_LIMIT, hil.hilToL11(7))
        self._Ltc2975Write(Ltc2975Commands.VOUT_OV_WARN_LIMIT, hil.hilToL16(7, self.L16Mode))

        self._Ltc2975Write(Ltc2975Commands.VOUT_MAX, hil.hilToL16(LcChannel.MAX_CLAMP_VOLTAGE, self.L16Mode))

        # Setting to 5V because of MFR_DISCHARGE_FAULT behavior
        # 1. MFR_DAC is used to directly set output voltage instead of VOUT_COMMAND
        # 2. VOUT_COMMAND defaults to 1.0V, MFR_VOUT_DISCHARGE_THRESHOLD defaults to 2.0
        # 3. This means that a MFR_DISCHARGE_FAULT may occur if output voltage is 1.0V * 2.0 = 2.0V
        #       and an OPERATION command is issued to the device while at this level.
        # 4. Setting VOUT_COMMAND to 5.0V (and never changing it) results in 5.0V * 2.0 = 10.V
        #       which should be higher than the part would ever be able to read.
        self._Ltc2975Write(Ltc2975Commands.VOUT_COMMAND, hil.hilToL16(5, self.L16Mode))

        #TODO: Other things Travis was playing with:
        #self._Ltc2975Write(Ltc2975Commands.MFR_VOUT_DISCHARGE_THRESHOLD, hil.hilToL11(2))    # 0xC200/default
        #self._Ltc2975Write(Ltc2975Commands.MFR_IOUT_CAL_GAIN_TC,hil.ConvertToBytes(0x0000))        # 0x0000/default
        #self._Ltc2975Write(Ltc2975Commands.MFR_TEMP_1_GAIN,hil.ConvertToBytes(0x4000))             # 0x4000/default
        #self._Ltc2975Write(Ltc2975Commands.MFR_TEMP_1_OFFSET,hil.ConvertToBytes(0x8000))           # 0x8000/default

        #self._Ltc2975Write(Ltc2975Commands.USER_DATA_01,hil.ConvertToBytes(0x0000))               # reserved for LTpowerPlay
        #self._Ltc2975Write(Ltc2975Commands.USER_DATA_03,hil.ConvertToBytes(0x0000))               # scratchpad

    def GetChannelId(self):
        return self.slot << 16 | self.channel

    def GetChannelNumber(self):
        return "{}.{}".format(self.slot, self.channel)

    def Apply(self):
        if self.checkGangConfig and self.gangConfig == LcChannel.GangConfiguration.Slave:
            raise RuntimeError('Cannot set attributes on a ganged rail that is configured as a slave.')
        elif self.checkGangConfig and self.gangConfig == LcChannel.GangConfiguration.Master:
            self.lcGangController.SequenceGang()    #masterChannel.Apply() is just an alias for HcGang.SequenceGang()
            return

        for attribute, value in self.attributes.items():
            if attribute != LcChannel.Attributes.PowerSequence:
                self._ApplySingleAttribute(attribute, value)
                #self.CheckRailStatus()
        if self.gangConfig == LcChannel.GangConfiguration.Independent:
            powerSequenceValue = self.attributes[LcChannel.Attributes.PowerSequence]
            self._ApplySingleAttribute(LcChannel.Attributes.PowerSequence, powerSequenceValue)

    def GetResults(self):
        rawCurrent = self._Ltc2975Read(Ltc2975Commands.READ_IOUT, Ltc2975ByteCount.L11_READ.value)
        current = hil.hilFromL11(rawCurrent)
        rawVoltage = self._Ltc2975Read(Ltc2975Commands.READ_VOUT, Ltc2975ByteCount.L16_READ.value)
        voltage = hil.hilFromL16(rawVoltage, self.L16Mode)
        rawInputCurrent = self._Ltc2975Read(Ltc2975Commands.READ_IIN, Ltc2975ByteCount.L11_READ.value)
        inputCurrent = hil.hilFromL11(rawInputCurrent)
        rawInputVoltage = self._Ltc2975Read(Ltc2975Commands.READ_VIN, Ltc2975ByteCount.L16_READ.value)
        inputVoltage = hil.hilFromL16(rawInputVoltage, self.L16Mode)
        return {'RailOutputCurrent' : current, 'RailOutputVoltage' : voltage, 'ModuleInputCurrent' : inputCurrent, 'ModuleInputVoltage' : inputVoltage}

    def GetName(self):
        return "{}Rail_Slot{}_Channel{}".format(LcChannel.RESOURCE_TYPE, self.slot, self.channel)

    def GetResourceType(self):
        return LcChannel.RESOURCE_TYPE

    def SetAttribute(self, attributeName, attributeValue):
        if self.checkGangConfig and self.gangConfig == LcChannel.GangConfiguration.Slave:
            raise RuntimeError('Cannot set attributes on a ganged rail that is configured as a slave.')

        validAttribute = None
        for attribute in LcChannel.Attributes:
            if attributeName == attribute.name:
                validAttribute = attribute
        if validAttribute is None:
            raise RuntimeError("Attribute \"{}\" is invalid".format(attributeName))

        self._ValidateUserAttribute(validAttribute, attributeValue)
        self.attributes[validAttribute] = attributeValue

    def SetAttributes(self, attributeMap):
        for attributeName, attributeValue in attributeMap.items():
            self.SetAttribute(attributeName, attributeValue)

    def GetAttributes(self):
        return {attribute.name : self.attributes[attribute] for attribute in self.attributes.keys()}

    def GetAttributesFromHw(self):
        attributes = {}
        for attribute in self.Attributes:
            attributes[attribute] = self._GetUserAttributeValueFromHw(attribute)
        return attributes

    def ResetAttributes(self):
        if self.checkGangConfig and self.gangConfig == LcChannel.GangConfiguration.Slave:
            raise RuntimeError('Cannot set attributes on a ganged rail that is configured as a slave.')
        self.attributes = {attribute : LcChannel.ATTRIBUTE_LIMITS_DEFAULT[attribute][LcChannel.ATTR_DEFAULT] for attribute in LcChannel.ATTRIBUTE_LIMITS_DEFAULT.keys()}

    def Safestate(self):
        self.SetAttribute(LcChannel.Attributes.PowerSequence.name, False)
        self.Apply()
        
    def CheckStatus(self):
        wordStatus = self._Ltc2975Read(Ltc2975Commands.STATUS_WORD, Ltc2975ByteCount.WORD.value)
        intStatus = wordStatus[1] << 8 | wordStatus[0]
        generalErrors, allErrors = [], []
        generalErrorsIgnoreList = [Ltc2975StatusBits.NONE_OF_THE_ABOVE]

        for statusBit in Ltc2975StatusBits:
            if intStatus & (1 << statusBit.value) and statusBit not in generalErrorsIgnoreList:
                generalErrors.append(statusBit.name)
        #allErrors = allErrors + generalErrors #Don't but the general errors in the list because they are duplicates and its confusing to users

        #TODO: Add smarts for more types of errors. The most common ones have been done above (and below)

        cmlErrorOccurred = intStatus & (1 << Ltc2975StatusBits.CML_FAULT.value)
        cmlErrors = []
        if cmlErrorOccurred:
            cmlStatus = self._Ltc2975Read(Ltc2975Commands.STATUS_CML, Ltc2975ByteCount.SINGLE_BYTE.value, False)[0]
            for cmlStatusBit in Ltc2975CmlStatusBits:
                if cmlStatus & (1 << cmlStatusBit.value):
                    cmlErrors.append(cmlStatusBit.name)
        allErrors = allErrors + cmlErrors

        mfrErrorIgnoreList = [Ltc2975MfrStatusBits.DAC_CONNECTED]
        mfrErrorOccurred = intStatus & (1 << Ltc2975StatusBits.MFR_SPECIFIC_FAULT.value)
        mfrErrors = []
        if mfrErrorOccurred:
            mfrStatus = self._Ltc2975Read(Ltc2975Commands.STATUS_MFR_SPECIFIC, Ltc2975ByteCount.SINGLE_BYTE.value)[0]
            for mfrStatusBit in Ltc2975MfrStatusBits:
                if mfrStatusBit not in mfrErrorIgnoreList and mfrStatus & (1 << mfrStatusBit.value):
                    mfrErrors.append(mfrStatusBit.name)
        allErrors = allErrors + mfrErrors

        voutErrorOccurred = intStatus & (1 << Ltc2975StatusBits.VOUT_FAULT.value)
        voutErrors = []
        if voutErrorOccurred:
            voutStatus = self._Ltc2975Read(Ltc2975Commands.STATUS_VOUT, Ltc2975ByteCount.SINGLE_BYTE.value)[0]
            for voutStatusBit in Ltc2975VoutStatusBits:
                if voutStatus & (1 << voutStatusBit.value):
                    voutErrors.append(voutStatusBit.name)
        allErrors = allErrors + voutErrors

        ioutErrorOccurred = intStatus & (1 << Ltc2975StatusBits.IOUT_FAULT.value)
        ioutErrors = []
        if ioutErrorOccurred:
            ioutStatus = self._Ltc2975Read(Ltc2975Commands.STATUS_IOUT, Ltc2975ByteCount.SINGLE_BYTE.value)[0]
            for ioutStatusBit in Ltc2975IoutStatusBits:
                if ioutStatus & (1 << ioutStatusBit.value):
                    ioutErrors.append(ioutStatusBit.name)
        allErrors = allErrors + ioutErrors

        if Log.Enabled:
            print('General Errors:')
            for error in generalErrors:
                print('\tError on {} channel {}: {}'.format(LcChannel.RESOURCE_TYPE, self.channel, error))
            print('Communication Errors:')
            for error in cmlErrors:
                print('\tError on {} channel {}: {}'.format(LcChannel.RESOURCE_TYPE, self.channel, error))
            print('VOUT Errors:')
            for error in voutErrors:
                print('\tError on {} channel {}: {}'.format(LcChannel.RESOURCE_TYPE, self.channel, error))
            print('IOUT Errors:')
            for error in ioutErrors:
                print('\tError on {} channel {}: {}'.format(LcChannel.RESOURCE_TYPE, self.channel, error))
            print('LTM2975 Specific Errors:')
            for error in mfrErrors:
                print('\tError on {} channel {}: {}'.format(LcChannel.RESOURCE_TYPE, self.channel, error))

        self._Ltc2975Write(Ltc2975Commands.CLEAR_FAULTS, bytes([]), False) #No command data for this command
        return allErrors

    #Ganging functions
    #Ganging interface functions
    class GangConfiguration(Enum):
        Independent = 0
        Slave = 1
        Master = 2

    def SetAsMaster(self):
        self.gangConfig = LcChannel.GangConfiguration.Master

    def SetAsSlave(self):
        self.gangConfig = LcChannel.GangConfiguration.Slave

    def ClearGangConfiguration(self):
        self.gangConfig = LcChannel.GangConfiguration.Independent

    def _EnableGangConfigCheck(self):
        self.checkGangConfig = True

    def _DisableGangConfigCheck(self):
        self.checkGangConfig = False

    #Internal functions
    def _VoltageToDAC(self, voltage):
        VdcMax = 5.57273659
        VdcMin = 0.537827499 #Max/Min voltages calculated from resistor values and formulas in LTC2975 data sheet
        MaxDACCount = 0x3FF
        #The total DAC count is distributed evenly across the usable voltage range of the channel
        DACCountPerVolt = MaxDACCount/(VdcMax - VdcMin) #Travis calculated as 203.181424238047 # 1023(10 bit DAC 0x3FF)/(5.57273659(VdcMax) - 0.537827499 (VdcMin))
        DACCount = round((voltage-VdcMin)*DACCountPerVolt)
        return hil.ConvertToBytes(DACCount)

    def _DACToVoltage(self, dacCount):
        VdcMax = 5.57273659
        VdcMin = 0.537827499 #Max/Min voltages calculated from resistor values and formulas in LTC2975 data sheet
        MaxDACCount = 0x3FF
        VoltsPerDACCount = (VdcMax - VdcMin) / MaxDACCount
        voltage = dacCount * VoltsPerDACCount + VdcMin #Scale the DAC count into a voltage based on the range, then use the offset to put it into the correct range
        return voltage

    def _ValidateUserAttribute(self, attribute, value):
        if attribute in LcChannel.RANGE_ATTRIBUTES:
            minValue = LcChannel.ATTRIBUTE_LIMITS_DEFAULT[attribute][LcChannel.ATTR_MIN_INDEX]
            maxValue = LcChannel.ATTRIBUTE_LIMITS_DEFAULT[attribute][LcChannel.ATTR_MAX_INDEX]
            if value > maxValue or value < minValue:
                raise RuntimeError("{}'s value ({}) cannot be set; it is outside the legal range of values. Min: {}, Max: {}".format(attribute.name, value, minValue, maxValue))
        elif attribute in LcChannel.BOOL_ATTRIBUTES:
            if type(value) is not bool:
                raise RuntimeError("{}'s value ({}) cannot be set; it must be either True or False.".format(attribute.name, value))

    def _ApplySingleAttribute(self, attribute, value):
        internalAttributes = self._UserToInternalAttribute(attribute, value)
        for command, encodedValue in internalAttributes:
            self._Ltc2975Write(command, encodedValue)

    def _UserToInternalAttribute(self, attribute, value):
        internalAttributes = []
        if attribute == LcChannel.Attributes.FreeDriveCurrentHi or attribute == LcChannel.Attributes.IClampHi:
            internalAttributes.append([Ltc2975Commands.IOUT_OC_FAULT_LIMIT, hil.hilToL11(value)])
        elif attribute == LcChannel.Attributes.FreeDriveCurrentLo or attribute == LcChannel.Attributes.IClampLo:
            internalAttributes.append([Ltc2975Commands.IOUT_UC_FAULT_LIMIT, hil.hilToL11(value)])
        elif attribute == LcChannel.Attributes.FreeDriveTime:
            internalAttributes.append([Ltc2975Commands.TON_MAX_FAULT_LIMIT, hil.hilToL11(value * 1000)])         #Convert from seconds to milliseconds
        elif attribute == LcChannel.Attributes.UnderVoltageLimit:
            internalAttributes.append([Ltc2975Commands.VOUT_UV_FAULT_LIMIT, hil.hilToL16(value, self.L16Mode)])
        elif attribute == LcChannel.Attributes.OverVoltageLimit:
            internalAttributes.append([Ltc2975Commands.VOUT_OV_FAULT_LIMIT, hil.hilToL16(value, self.L16Mode)])
        elif attribute == LcChannel.Attributes.VForce:
            internalAttributes.append([Ltc2975Commands.MFR_DAC, self._VoltageToDAC(value)])
        elif attribute == LcChannel.Attributes.PowerSequence:
            value = Ltc2975.ON_OPERATION.value if value else Ltc2975.OFF_OPERATION.value
            internalAttributes.append([Ltc2975Commands.OPERATION, bytes([value])])
        return internalAttributes
            
    def _GetUserAttributeValueFromHw(self, attribute):
        if attribute == LcChannel.Attributes.FreeDriveCurrentHi or attribute == LcChannel.Attributes.IClampHi:
            rawL11Value = self._Ltc2975Read(Ltc2975Commands.IOUT_OC_FAULT_LIMIT, Ltc2975ByteCount.L11_READ.value)
            return hil.hilFromL11(rawL11Value)
        elif attribute == LcChannel.Attributes.FreeDriveCurrentLo or attribute == LcChannel.Attributes.IClampLo:
            rawL11Value = self._Ltc2975Read(Ltc2975Commands.IOUT_UC_FAULT_LIMIT, Ltc2975ByteCount.L11_READ.value)
            return hil.hilFromL11(rawL11Value)
        elif attribute == LcChannel.Attributes.FreeDriveTime:
            rawL11Value = self._Ltc2975Read(Ltc2975Commands.TON_MAX_FAULT_LIMIT, Ltc2975ByteCount.L11_READ.value)
            return hil.hilFromL11(rawL11Value) / 1000 #Value represents milliseconds, need to convert back
        elif attribute == LcChannel.Attributes.UnderVoltageLimit:
            rawL16Value = self._Ltc2975Read(Ltc2975Commands.VOUT_UV_FAULT_LIMIT, Ltc2975ByteCount.L16_READ.value)
            return hil.hilFromL16(rawL16Value, self.L16Mode)
        elif attribute == LcChannel.Attributes.OverVoltageLimit:
            rawL16Value = self._Ltc2975Read(Ltc2975Commands.VOUT_OV_FAULT_LIMIT, Ltc2975ByteCount.L16_READ.value)
            return hil.hilFromL16(rawL16Value, self.L16Mode)
        elif attribute == LcChannel.Attributes.VForce:
            rawDacValue = self._Ltc2975Read(Ltc2975Commands.MFR_DAC, Ltc2975ByteCount.WORD.value)
            return self._DACToVoltage(hil.ConvertToInt(rawDacValue))
        elif attribute == LcChannel.Attributes.PowerSequence:
            rawRegister = self._Ltc2975Read(Ltc2975Commands.OPERATION, Ltc2975ByteCount.SINGLE_BYTE.value)
            return True if rawRegister[0] in [Ltc2975OperationMode.SEQUENCE_ON.value, Ltc2975OperationMode.MARGIN_HIGH.value, Ltc2975OperationMode.MARGIN_LOW.value, Ltc2975OperationMode.MARGIN_HIGH_IGNORE_FAULTS.value, Ltc2975OperationMode.MARGIN_LOW_IGNORE_FAULTS.value] else False
        else:
            raise RuntimeError("Error: attribute {} not supported".format(attribute.name))

    def _TurnOn(self):
        self.attributes[LcChannel.Attributes.PowerSequence] = True
        self._ApplySingleAttribute(LcChannel.Attributes.PowerSequence, True)

    def _TurnOff(self):
        self.attributes[LcChannel.Attributes.PowerSequence] = False
        self._ApplySingleAttribute(LcChannel.Attributes.PowerSequence, False)

    def _Ltc2975Write(self, command, encodedData, isPaged=True, checkIfBusy=True, timeout=1.0):
        page = self.page if isPaged else None
        Ltc2975.Write(self.slot, self.chip, command, encodedData, page, checkIfBusy, timeout)

    def _Ltc2975Read(self, command, readCount, isPaged=True, retryCount=1):
        page = self.page if isPaged else None
        return Ltc2975.Read(self.slot, self.chip, command, readCount, page, retryCount)

    #ByteCount can be up to 2 bytes; this should accommodate all "normal" registers in the Ltc2975
    def _Ltc2975ReadModifyWrite(self, command, byteCount, bits, mask, setBits, paged=True):
        byteStringValue = self._Ltc2975Read(command, byteCount, paged)
        oldRegisterValue = 0
        for byteIndex in range(byteCount):
            oldRegisterValue = oldRegisterValue | (byteStringValue[byteIndex] << (8 * byteIndex))
        if setBits:
            newRegisterValue = oldRegisterValue | bits
        else:
            #This is where the byteCount limitation comes in... cut off any high bits in mask
            inverseAndMaskedBits = bits ^ (0xFFFF & mask)
            newRegisterValue = oldRegisterValue & inverseAndMaskedBits
        byteArray = [(newRegisterValue >> (8 * byteIndex)) & 0xFF for byteIndex in range(byteCount)]
        newRegByteString = bytes(byteArray)
        self._Ltc2975Write(command, newRegByteString, paged)

if __name__ == "__main__":
    print("This module is a helper module that should not be used directly, even in a script. Please use the main module instead.")
    exit()