#coding:utf8
import struct

from Common import hilmon as hil

#
# Functions made for the following commands except for the commented out ones, which need a PMBus block-mode function.
#

PAGE = 0x00                   # Provides integration with multi-page PMBus devices. R/W Byte N Reg 0x00 76
OPERATION = 0x01              # Operating mode control. On/off, margin high and margin low. R/W Byte Y Reg Y 0x80 80
ON_OFF_CONFIG = 0x02          # RUN pin and PMBus bus on/off command configuration. R/W Byte Y Reg Y 0x1E 80
CLEAR_FAULTS = 0x03           # Clear any fault bits that have been set. Send Byte N NA 105
# PAGE_PLUS_WRITE = 0x05        # Write a command directly to a specified page. W Block N 76
# PAGE_PLUS_READ = 0x06         # Read a command directly from a specified page. Block R/W N 76
WRITE_PROTECT = 0x10          # Level of protection provided by the device against accidental changes. R/W Byte N Reg Y 0x00 77
STORE_USER_ALL = 0x15         # Store user operating memory to EEPROM. Send Byte N NA 115
RESTORE_USER_ALL = 0x16       # Restore user operating memory from EEPROM. Send Byte N NA 115
CAPABILITY = 0x19             # Summary of PMBus optional communication protocols supported by this device. R Byte N Reg 0xB0 104
# SMBALERT_MASK = 0x1B          # Mask ALERT activity Block R/W Y Reg Y See CMD 105
VOUT_MODE = 0x20              # Output voltage format and exponent (2–12). R Byte Y Reg 2–12 0x14 86
VOUT_COMMAND = 0x21           # Nominal output voltage set point. R/W Word Y L16 V Y 1.0 0x1000 87
VOUT_MAX = 0x24               # Upper limit on the commanded output voltage including VOUT_MARGIN_HI. R/W Word Y L16 V Y 3.6 0xC399 86
VOUT_MARGIN_HIGH = 0x25       # Margin high output voltage set point. Must be greater than VOUT_COMMAND. R/W Word Y L16 V Y 1.05 0x10CD 87
VOUT_MARGIN_LOW = 0x26        # Margin low output voltage set point. Must be less than VOUT_COMMAND. R/W Word Y L16 V Y 0.95 0x0F33 87
VOUT_TRANSITION_RATE = 0X27   # Rate the output changes when VOUT commanded to a new value. R/W Word Y L11 V/ms Y 0.001 0x8042 93
FREQUENCY_SWITCH = 0x33       # Switching frequency of the controller. R/W Word N L11 kHz Y 350kHz 0xFABC 84
VIN_ON = 0x35                 # Input voltage at which the unit should start power conversion. R/W Word N L11 V Y 6.5 0xCB40 85
VIN_OFF = 0x36                # Input voltage at which the unit should stop power conversion. R/W Word N L11 V Y 6.0 0xCB00 85
VOUT_OV_FAULT_LIMIT = 0x40    # Output overvoltage fault limit. R/W Word Y L16 V Y 1.1 0x119A 86
VOUT_OV_FAULT_RESPONSE = 0x41 # Action to be taken by the device when an output overvoltage fault is detected. R/W Byte Y Reg Y 0xB8 95
VOUT_OV_WARN_LIMIT = 0x42     # Output overvoltage warning limit. R/W Word Y L16 V Y 1.075 0x1133 86
VOUT_UV_WARN_LIMIT = 0x43     # Output undervoltage warning limit. R/W Word Y L16 V Y 0.925 0x0ECD 87
VOUT_UV_FAULT_LIMIT = 0x44    # Output undervoltage fault limit. R/W Word Y L16 V Y 0.9 0x0E66 87
VOUT_UV_FAULT_RESPONSE = 0x45 # Action to be taken by the device when an output undervoltage fault is detected. R/W Byte Y Reg Y 0xB8 96
IOUT_OC_FAULT_LIMIT = 0x46    # Output overcurrent fault limit. R/W Word Y L11 A Y 45.00 0xE280 89
IOUT_OC_FAULT_RESPONSE = 0x47 # Action to be taken by the device when an output overcurrent fault is detected. R/W Byte Y Reg Y 0x00 98
IOUT_OC_WARN_LIMIT = 0x4A     # Output overcurrent warning limit. R/W Word Y L11 A Y 30.0 0xDBC0 90
OT_FAULT_LIMIT = 0x4F         # External overtemperature fault limit. R/W Word Y L11 C Y 128.0 0xF200 91
OT_FAULT_RESPONSE = 0x50      # Action to be taken by the device when an external overtemperature fault is detected, R/W Byte Y Reg Y 0xB8 100
OT_WARN_LIMIT = 0x51          # External overtemperature warning limit. R/W Word Y L11 C Y 125.0 0xEBE8 91
UT_FAULT_LIMIT = 0x53         # External undertemperature fault limit. R/W Word Y L11 C Y –45.0 0xE530 92
UT_FAULT_RESPONSE = 0x54      # Action to be taken by the device when an external undertemperature fault is detected. R/W Byte Y Reg Y 0xB8 100
VIN_OV_FAULT_LIMIT = 0x55     # Input supply overvoltage fault limit. R/W Word N L11 V Y 15.5 0xD3E0 86
VIN_OV_FAULT_RESPONSE = 0x56  # Action to be taken by the device when an input overvoltage fault is detected. R/W Byte Y Reg Y 0x80 95
VIN_UV_WARN_LIMIT = 0x58      # Input supply undervoltage warning limit. R/W Word N L11 V Y 6.3 0xCB26 85
IIN_OC_WARN_LIMIT = 0x5D      # Input supply overcurrent warning limit. R/W Word N L11 A Y 10.0 0xD280 90
TON_DELAY = 0x60              # Time from RUN and/or Operation on to output rail turn-on. R/W Word Y L11 ms Y 0.0 0x8000 92
TON_RISE = 0x61               # Time from when the output starts to rise until the output voltage reaches the VOUT commanded value. R/W Word Y L11 ms Y 3.0 0xC300 92
TON_MAX_FAULT_LIMIT = 0x62    # Maximum time from the start of TON_RISE for VOUT to cross the VOUT_UV_FAULT_LIMIT. R/W Word Y L11 ms Y 5.0 0xCA80 93
TON_MAX_FAULT_RESPONSE = 0x63 # Action to be taken by the device when a TON_ MAX_FAULT event is detected. R/W Byte Y Reg Y 0xB8 98
TOFF_DELAY = 0x64             # Time from RUN and/or Operation off to the start of TOFF_FALL ramp. R/W Word Y L11 ms Y 0.0 0x8000 93
TOFF_FALL = 0x65              # Time from when the output starts to fall until the output reaches zero volts. R/W Word Y L11 ms Y 3.0 0xC300 93
TOFF_MAX_WARN_LIMIT = 0x66    # Maximum allowed time, after TOFF_FALL completed, for the unit to decay below 12.5%. R/W Word Y L11 ms Y 0 0x8000 94
STATUS_BYTE = 0x78            # One byte summary of the unit’s fault condition. R/W Byte Y Reg NA 106
STATUS_WORD = 0x79            # Two byte summary of the unit’s fault condition. R/W Word Y Reg NA 107
STATUS_VOUT = 0x7A            # Output voltage fault and warning status. R/W Byte Y Reg NA 107
STATUS_IOUT = 0x7B            # Output current fault and warning status. R/W Byte Y Reg NA 108
STATUS_INPUT = 0x7C           # Input supply fault and warning status. R/W Byte N Reg NA 108
STATUS_TEMPERATURE = 0x7D     # External temperature fault and warning status for READ_TEMERATURE_1. R/W Byte Y Reg NA 109
STATUS_CML = 0x7E             # Communication and memory fault and warning status.R/W Byte N Reg NA 109
STATUS_MFR_SPECIFIC = 0x80    # Manufacturer specific fault and state information. R/W Byte Y Reg NA 110
READ_VIN = 0x88               # Measured input supply voltage. R Word N L11 V NA 112
READ_IIN = 0x89               # Measured input supply current. R Word N L11 A NA 112
READ_VOUT = 0x8B              # Measured output voltage. R Word Y L16 V NA 112
READ_IOUT = 0x8C              # Measured output current. R Word Y L11 A NA 112
READ_TEMPERATURE_1 = 0x8D     # External temperature sensor temperature. This is the value used for all temperature related processing, including IOUT_CAL_GAIN. R Word Y L11 C NA 112
READ_TEMPERATURE_2 = 0x8E     # Internal die junction temperature. Does not affect any other commands. R Word N L11 C NA 112
READ_FREQUENCY = 0x95         # Measured PWM switching frequency. R Word Y L11 Hz NA 112
READ_POUT = 0x96              # Measured output power R Word Y L11 W N/A 112
READ_PIN = 0x97               # Calculated input power R Word Y L11 W N/A 113
PMBUS_REVISION = 0x98         # PMBus revision supported by this device. Current revision is 1.2. R Byte N Reg 0x22 104
# MFR_ID = 0x99                 # The manufacturer ID of the LTM4680 in ASCII. R String N ASC LTC 104
# MFR_MODEL = 0x9A              # Manufacturer part number in ASCII. R String N ASC 104
MFR_VOUT_MAX = 0xA5           # Maximum allowed output voltage including VOUT_OV_FAULT_LIMIT. R Word Y L16 V 3.6 0x0399 88

#
# No functions made for the following commands yet...
#

MFR_PIN_ACCURACY = 0xAC       # Returns the accuracy of the READ_PIN command R Byte N % 5.0% 113
USER_DATA_00 = 0xB0           # OEM RESERVED. Typically used for part serialization. R/W Word N Reg Y NA 104
USER_DATA_01 = 0xB1           # Manufacturer reserved for LTpowerPlay. R/W Word Y Reg Y NA 104
USER_DATA_02 = 0xB2           # OEM RESERVED. Typically used for part serialization R/W Word N Reg Y NA 104
USER_DATA_03 = 0xB3           # An NVM word available for the user. R/W Word Y Reg Y 0x0000 104
USER_DATA_04 = 0xB4           # An NVM word available for the user. R/W Word N Reg Y 0x0000 104
# MFR_EE_UNLOCK = 0xBD          # Contact factory. 120
# MFR_EE_ERASE = 0xBE           # Contact factory. 120
# MFR_EE_DATA = 0xBF            # Contact factory. 120
MFR_CHAN_CONFIG = 0xD0        # Configuration bits that are channel specific. R/W Byte Y Reg Y 0x1D 78
MFR_CONFIG_ALL = 0xD1         # General configuration bits. R/W Byte N Reg Y 0x21 79
MFR_FAULT_PROPAGATE = 0xD2    # Configuration that determines which faults are propagated to the FAULT pin. R/W Word Y Reg Y 0x6993 101
MFR_PWM_COMP = 0xD3           # PWM loop compensation configuration R/W Byte Y Reg Y 0x28 82
MFR_PWM_MODE = 0xD4           # Configuration for the PWM engine. R/W Byte Y Reg Y 0xC7 81
MFR_FAULT_RESPONSE = 0xD5     # Action to be taken by the device when the FAULT pin is externally asserted low. R/W Byte Y Reg Y 0xC0 99
MFR_OT_FAULT_RESPONSE = 0xD6  # Action to be taken by the device when an internal overtemperature fault is detected. R Byte N Reg 0xC0 99
MFR_IOUT_PEAK = 0xD7          # Report the maximum measured value of READ_ IOUT since last MFR_CLEAR_PEAKS. R Word Y L11 A NA 113
MFR_ADC_CONTROL = 0xD8        # ADC telemetry parameter selected for repeated fast ADC read back R/W Byte N Reg 0x00 114
MFR_RETRY_DELAY = 0xDB        # Retry interval during FAULT retry mode. R/W Word Y L11 ms Y 250.0 0xF3E8 94
MFR_RESTART_DELAY = 0xDC      # Minimum time the RUN pin is held low by the LTM4680. R/W Word Y L11 ms Y 150.0 0xF258 94
MFR_VOUT_PEAK = 0xDD          # Maximum measured value of READ_VOUT since last MFR_CLEAR_PEAKS. R Word Y L16 V NA 113
MFR_VIN_PEAK = 0xDE           # Maximum measured value of READ_VIN since last MFR_CLEAR_PEAKS. R Word N L11 V NA 113
MFR_TEMPERATURE_1_PEAK = 0xDF # Maximum measured value of external Temperature (READ_TEMPERATURE_1) since last MFR_CLEAR_PEAKS. R Word Y L11 C NA 113
MFR_READ_IIN_PEAK = 0xE1      # Maximum measured value of READ_IIN command since last MFR_CLEAR_PEAKS R Word N L11 A NA 113
MFR_CLEAR_PEAKS = 0xE3        # Clears all peak values. Send Byte N NA 106
MFR_READ_ICHIP = 0xE4         # Measured supply current of the SVIN pin R Word N L11 A NA 114
MFR_PADS = 0xE5               # Digital status of the I/O pads.
MFR_ADDRESS = 0xE6            # Sets the 7-bit I2C address byte. R/W Byte N Reg Y 0x4F 78
MFR_SPECIAL_ID = 0xE7         # Manufacturer code representing the LTM4680 and revision R Word N Reg 0x4100 104
MFR_IIN_CAL_GAIN = 0xE8       # The resistance value of the input current sense element in mΩ. R/W Word N L11 mΩ Y 2.0 0xC200 90
MFR_FAULT_LOG_STORE = 0xEA    # Command a transfer of the fault log from RAM to EEPROM. Send Byte N NA 116
# MFR_INFO = 0x                 # Contact factory. 120
# MFR_IOUT_CAL_GAIN = 0x        # SET AT FACTORY 120
MFR_FAULT_LOG_CLEAR = 0xEC    # Initialize the EEPROM block reserved for fault logging. Send Byte N NA 120
MFR_FAULT_LOG = 0xEE          # Fault log data bytes. R Block N Reg Y NA 116
MFR_COMMON = 0xEF             # Manufacturer status bits that are common across multiple ADI chips. R Byte N Reg NA 111
MFR_COMPARE_USER_ALL = 0xF0   # Compares current command contents with NVM. Send Byte N NA 115
MFR_TEMPERATURE_2_PEAK = 0xF4 # Peak internal die temperature since last MFR_ CLEAR_PEAKS. R Word N L11 C NA 114
MFR_PWM_CONFIG = 0xF5         # Set numerous parameters for the DC/DC controller including phasing. R/W Byte N Reg Y 0x10 83
MFR_IOUT_CAL_GAIN_TC = 0xF6   # Temperature coefficient of the current sensing element. R/W Word Y CF ppm/˚C Y 3900 0x0F3C 88
MFR_ICHIP_CAL_GAIN = 0xF7     # The resistance value of the VIN pin filter element in mΩ. R/W Word N L11 mΩ Y 1000 0x03E8 85
MFR_TEMP_1_GAIN = 0xF8        # Sets the slope of the external temperature sensor. R/W Word Y CF Y 0.995 0x3FAE 91
MFR_TEMP_1_OFFSET = 0xF9      # Sets the offset of the external temperature sensor with respect to –273.1°C R/W Word Y L11 C Y 0.0 0x8000 91
MFR_RAIL_ADDRESS = 0xFA       # Common address for PolyPhase outputs to adjust common parameters. R/W Byte Y Reg Y 0x80 78
MFR_REAL_TIME = 0xFB          # 48-bit share-clock counter value. R Block N CF NA 104
MFR_RESET = 0xFD              # Commanded reset without requiring a power down. Send Byte N NA 80

def fromL11(b):
    return hil.hilFromL11(b)

def toL11(n):
    return hil.hilToL11(n)

def fromL16(slot,idx,pg,b):
    return hil.hilFromL16(b,vout_mode(slot,idx,pg))

def toL16(slot,idx,pg,n):
    return hil.hilToL16(n,vout_mode(slot,idx,pg))

def pmw(slot,idx,pg,cmd,data):
    hil.hbiDpsLtm4680PmbusWrite(slot,idx,pg,cmd,data)

def pmr(slot,idx,pg,cmd,length):
    return hil.hbiDpsLtm4680PmbusRead(slot,idx,pg,cmd,length)

def page(slot,idx,which=None):
    '''Provides integration with multi-page PMBus devices.
    '''
    if which is None:
        return pmr(slot,idx,-1,PAGE,1)[0]
    if which < 0 or which > 1:
        return ValueError('which must be 0-1')
    return pmw(slot,idx,-1,PAGE,bytes([which]))

def operation(slot,idx,pg,value=None):
    '''Operating mode control. On/off, margin high and margin low.
    '''
    if value is None:
        return pmr(slot,idx,pg,OPERATION,1)[0]
    if value not in (0x00,0x40,0x80,0x98,0xA8):
        return ValueError('value must be one of 0x00,0x40,0x80,0x98,0xA8')
    return pmw(slot,idx,pg,OPERATION,bytes([value]))

def on_off_config(slot,idx,pg,value=None):
    '''RUN pin and PMBus bus on/off command configuration.
    '''
    if value is None:
        return pmr(slot,idx,pg,ON_OFF_CONFIG,1)[0]
    if value not in (0x16,0x17,0x1E,0x1F):
        return ValueError('value must be one of 0x16,0x17,0x1E,0x1F')
    return pmw(slot,idx,pg,ON_OFF_CONFIG,bytes([value]))

def clear_faults(slot,idx,pg):
    '''Clear any fault bits that have been set.
    '''
    pmw(slot,idx,pg,CLEAR_FAULTS,b'')

def write_protect(slot,idx,pg,value=None):
    '''Level of protection provided by the device against accidental changes.
    '''
    if value is None:
        return pmr(slot,idx,pg,WRITE_PROTECT,1)[0]
    if value not in (0x00,0x20,0x40,0x80):
        return ValueError('value must be one of 0x00,0x20,0x40,0x80')
    return pmw(slot,idx,pg,WRITE_PROTECT,bytes([value]))

def store_user_all(slot,idx,pg):
    '''Store user operating memory to EEPROM.
    '''
    pmw(slot,idx,pg,STORE_USER_ALL,b'')

def restore_user_all(slot,idx,pg):
    '''Restore user operating memory from EEPROM.
    '''
    pmw(slot,idx,pg,RESTORE_USER_ALL,b'')

def capability(slot,idx,pg):
    '''Summary of PMBus optional communication protocols supported by this device.
    '''
    return pmr(slot,idx,pg,CAPABILITY,1)[0]

def vout_mode(slot,idx,pg):
    '''Output voltage format and exponent (2–12).
    '''
    return pmr(slot,idx,pg,VOUT_MODE,1)[0]

def vout_command(slot,idx,pg,value=None):
    '''Nominal output voltage set point.
    '''
    if value is None:
        return fromL16(slot,idx,pg,pmr(slot,idx,pg,VOUT_COMMAND,2))
    return pmw(slot,idx,pg,VOUT_COMMAND,toL16(slot,idx,pg,value))
    
def vout_max(slot,idx,pg,value=None):
    '''Nominal output voltage set point.
    '''
    if value is None:
        return fromL16(slot,idx,pg,pmr(slot,idx,pg,VOUT_MAX,2))
    return pmw(slot,idx,pg,VOUT_MAX,toL16(slot,idx,pg,value))
    
def vout_margin_high(slot,idx,pg,value=None):
    '''Margin high output voltage set point. Must be greater than VOUT_COMMAND.
    '''
    if value is None:
        return fromL16(slot,idx,pg,pmr(slot,idx,pg,VOUT_MARGIN_HIGH,2))
    return pmw(slot,idx,pg,VOUT_MARGIN_HIGH,toL16(slot,idx,pg,value))
    
def vout_margin_low(slot,idx,pg,value=None):
    '''Margin low output voltage set point. Must be less than VOUT_COMMAND.
    '''
    if value is None:
        return fromL16(slot,idx,pg,pmr(slot,idx,pg,VOUT_MARGIN_LOW,2))
    return pmw(slot,idx,pg,VOUT_MARGIN_LOW,toL16(slot,idx,pg,value))

def vout_transition_rate(slot,idx,pg,value=None):
    '''Rate the output changes when VOUT commanded to a new value.
    '''
    if value is None:
        return fromL11(pmr(slot,idx,pg,VOUT_TRANSITION_RATE,2))
    return pmw(slot,idx,pg,VOUT_TRANSITION_RATE,toL11(value))
   
def frequency_switch(slot,idx,pg,value=None):
    '''Switching frequency of the controller.
    '''
    if value is None:
        return fromL11(pmr(slot,idx,pg,FREQUENCY_SWITCH,2))
    if value not in (0,250,350,425,500,575,650,750,1000):
        return ValueError('value must be one of 0,250,350,425,500,575,650,750,1000.')
    return pmw(slot,idx,pg,FREQUENCY_SWITCH,toL11(value))
    
def vin_on(slot,idx,pg,value=None):
    '''Input voltage at which the unit should start power conversion.
    '''
    if value is None:
        return fromL11(pmr(slot,idx,pg,VIN_ON,2))
    return pmw(slot,idx,pg,VIN_ON,toL11(value))
   
def vin_off(slot,idx,pg,value=None):
    '''Input voltage at which the unit should stop power conversion.
    '''
    if value is None:
        return fromL11(pmr(slot,idx,pg,VIN_OFF,2))
    return pmw(slot,idx,pg,VIN_OFF,toL11(value))

def vout_ov_fault_limit(slot,idx,pg,value=None):
    '''Output overvoltage fault limit.
    '''
    if value is None:
        return fromL16(slot,idx,pg,pmr(slot,idx,pg,VOUT_OV_FAULT_LIMIT,2))
    return pmw(slot,idx,pg,VOUT_OV_FAULT_LIMIT,toL16(slot,idx,pg,value))

def vout_ov_fault_response(slot,idx,pg,value=None):
    '''Action to be taken by the device when an output overvoltage fault is detected.
    '''
    if value is None:
        return pmr(slot,idx,pg,VOUT_OV_FAULT_RESPONSE,1)[0]
    return pmw(slot,idx,pg,VOUT_OV_FAULT_RESPONSE,bytes([value]))

def vout_ov_warn_limit(slot,idx,pg,value=None):
    '''Output overvoltage warning limit.
    '''
    if value is None:
        return fromL16(slot,idx,pg,pmr(slot,idx,pg,VOUT_OV_WARN_LIMIT,2))
    return pmw(slot,idx,pg,VOUT_OV_WARN_LIMIT,toL16(slot,idx,pg,value))

def vout_uv_warn_limit(slot,idx,pg,value=None):
    '''Output undervoltage warning limit.
    '''
    if value is None:
        return fromL16(slot,idx,pg,pmr(slot,idx,pg,VOUT_UV_WARN_LIMIT,2))
    return pmw(slot,idx,pg,VOUT_UV_WARN_LIMIT,toL16(slot,idx,pg,value))

def vout_uv_fault_limit(slot,idx,pg,value=None):
    '''Output undervoltage fault limit.
    '''
    if value is None:
        return fromL16(slot,idx,pg,pmr(slot,idx,pg,VOUT_UV_FAULT_LIMIT,2))
    return pmw(slot,idx,pg,VOUT_UV_FAULT_LIMIT,toL16(slot,idx,pg,value))

def vout_uv_fault_response(slot,idx,pg,value=None):
    '''Action to be taken by the device when an output undervoltage fault is detected.
    '''
    if value is None:
        return pmr(slot,idx,pg,VOUT_UV_FAULT_RESPONSE,1)[0]
    return pmw(slot,idx,pg,VOUT_UV_FAULT_RESPONSE,bytes([value]))

def iout_oc_fault_limit(slot,idx,pg,value=None):
    '''Output overcurrent fault limit.
    '''
    if value is None:
        return fromL11(pmr(slot,idx,pg,IOUT_OC_FAULT_LIMIT,2))
    return pmw(slot,idx,pg,IOUT_OC_FAULT_LIMIT,toL11(value))

def iout_oc_fault_response(slot,idx,pg,value=None):
    '''Action to be taken by the device when an output overcurrent fault is detected.
    '''
    if value is None:
        return pmr(slot,idx,pg,IOUT_OC_FAULT_RESPONSE,1)[0]
    return pmw(slot,idx,pg,IOUT_OC_FAULT_RESPONSE,bytes([value]))

def iout_oc_warn_limit(slot,idx,pg,value=None):
    '''Output overcurrent warning limit.
    '''
    if value is None:
        return fromL11(pmr(slot,idx,pg,IOUT_OC_WARN_LIMIT,2))
    return pmw(slot,idx,pg,IOUT_OC_WARN_LIMIT,toL11(value))

def ot_fault_limit(slot,idx,pg,value=None):
    '''External overtemperature fault limit.
    '''
    if value is None:
        return fromL11(pmr(slot,idx,pg,OT_FAULT_LIMIT,2))
    return pmw(slot,idx,pg,OT_FAULT_LIMIT,toL11(value))

def ot_fault_response(slot,idx,pg,value=None):
    '''Action to be taken by the device when an output overcurrent fault is detected.
    '''
    if value is None:
        return pmr(slot,idx,pg,OT_FAULT_RESPONSE,1)[0]
    return pmw(slot,idx,pg,OT_FAULT_RESPONSE,bytes([value]))

def ot_warn_limit(slot,idx,pg,value=None):
    '''External overtemperature warning limit.
    '''
    if value is None:
        return fromL11(pmr(slot,idx,pg,OT_WARN_LIMIT,2))
    return pmw(slot,idx,pg,OT_WARN_LIMIT,toL11(value))

def ut_fault_limit(slot,idx,pg,value=None):
    '''External undertemperature fault limit.
    '''
    if value is None:
        return fromL11(pmr(slot,idx,pg,UT_FAULT_LIMIT,2))
    return pmw(slot,idx,pg,UT_FAULT_LIMIT,toL11(value))

def ut_fault_response(slot,idx,pg,value=None):
    '''Action to be taken by the device when an external undertemperature fault is detected.
    '''
    if value is None:
        return pmr(slot,idx,pg,UT_FAULT_RESPONSE,1)[0]
    return pmw(slot,idx,pg,UT_FAULT_RESPONSE,bytes([value]))

def vin_ov_fault_limit(slot,idx,pg,value=None):
    '''Input supply overvoltage fault limit.
    '''
    if value is None:
        return fromL11(pmr(slot,idx,pg,VIN_OV_FAULT_LIMIT,2))
    return pmw(slot,idx,pg,VIN_OV_FAULT_LIMIT,toL11(value))

def vin_ov_fault_response(slot,idx,pg,value=None):
    '''Action to be taken by the device when an input overvoltage fault is detected.
    '''
    if value is None:
        return pmr(slot,idx,pg,VIN_OV_FAULT_RESPONSE,1)[0]
    return pmw(slot,idx,pg,VIN_OV_FAULT_RESPONSE,bytes([value]))

def vin_uv_warn_limit(slot,idx,pg,value=None):
    '''Input supply undervoltage warning limit.
    '''
    if value is None:
        return fromL11(pmr(slot,idx,pg,VIN_UV_WARN_LIMIT,2))
    return pmw(slot,idx,pg,VIN_UV_WARN_LIMIT,toL11(value))

def iin_oc_warn_limit(slot,idx,pg,value=None):
    '''Input supply overcurrent warning limit.
    '''
    if value is None:
        return fromL11(pmr(slot,idx,pg,IIN_OC_WARN_LIMIT,2))
    return pmw(slot,idx,pg,IIN_OC_WARN_LIMIT,toL11(value))

def ton_delay(slot,idx,pg,value=None):
    '''Time from RUN and/or Operation on to output rail turn-on.
    '''
    if value is None:
        return fromL11(pmr(slot,idx,pg,TON_DELAY,2))
    return pmw(slot,idx,pg,TON_DELAY,toL11(value))

def ton_rise(slot,idx,pg,value=None):
    '''Time from when the output starts to rise until the output voltage reaches the VOUT commanded value.
    '''
    if value is None:
        return fromL11(pmr(slot,idx,pg,TON_RISE,2))
    return pmw(slot,idx,pg,TON_RISE,toL11(value))

def ton_max_fault_limit(slot,idx,pg,value=None):
    '''Maximum time from the start of TON_RISE for VOUT to cross the VOUT_UV_FAULT_LIMIT.
    '''
    if value is None:
        return fromL11(pmr(slot,idx,pg,TON_MAX_FAULT_LIMIT,2))
    return pmw(slot,idx,pg,TON_MAX_FAULT_LIMIT,toL11(value))

def ton_max_fault_response(slot,idx,pg,value=None):
    '''Action to be taken by the device when a TON_ MAX_FAULT event is detected.
    '''
    if value is None:
        return pmr(slot,idx,pg,TON_MAX_FAULT_RESPONSE,1)[0]
    return pmw(slot,idx,pg,TON_MAX_FAULT_RESPONSE,bytes([value]))

def toff_delay(slot,idx,pg,value=None):
    '''Time from RUN and/or Operation off to the start of TOFF_FALL ramp.
    '''
    if value is None:
        return fromL11(pmr(slot,idx,pg,TOFF_DELAY,2))
    return pmw(slot,idx,pg,TOFF_DELAY,toL11(value))

def toff_fall(slot,idx,pg,value=None):
    '''Time from when the output starts to fall until the output reaches zero volts.
    '''
    if value is None:
        return fromL11(pmr(slot,idx,pg,TOFF_FALL,2))
    return pmw(slot,idx,pg,TOFF_FALL,toL11(value))

def toff_max_warn_limit(slot,idx,pg,value=None):
    '''Maximum allowed time, after TOFF_FALL completed, for the unit to decay below 12.5%.
    '''
    if value is None:
        return fromL11(pmr(slot,idx,pg,TOFF_MAX_WARN_LIMIT,2))
    return pmw(slot,idx,pg,TOFF_MAX_WARN_LIMIT,toL11(value))

def status_byte(slot,idx,pg,value=None):
    '''One byte summary of the unit’s fault condition.
    '''
    if value is None:
        return pmr(slot,idx,pg,STATUS_BYTE,1)[0]
    return pmw(slot,idx,pg,STATUS_BYTE,bytes([value]))

def status_word(slot,idx,pg,value=None):
    '''Two byte summary of the unit’s fault condition.
    '''
    if value is None:
        return struct.unpack('<H',pmr(slot,idx,pg,STATUS_WORD,2))[0]
    return pmw(slot,idx,pg,STATUS_WORD,struct.pack('<H',value))

def status_vout(slot,idx,pg,value=None):
    '''Output voltage fault and warning status.
    '''
    if value is None:
        return pmr(slot,idx,pg,STATUS_VOUT,1)[0]
    return pmw(slot,idx,pg,STATUS_VOUT,bytes([value]))

def status_iout(slot,idx,pg,value=None):
    '''Output current fault and warning status.
    '''
    if value is None:
        return pmr(slot,idx,pg,STATUS_IOUT,1)[0]
    return pmw(slot,idx,pg,STATUS_IOUT,bytes([value]))

def status_input(slot,idx,pg,value=None):
    '''Input supply fault and warning status.
    '''
    if value is None:
        return pmr(slot,idx,pg,STATUS_INPUT,1)[0]
    return pmw(slot,idx,pg,STATUS_INPUT,bytes([value]))

def status_temperature(slot,idx,pg,value=None):
    '''External temperature fault and warning status for READ_TEMERATURE_1.
    '''
    if value is None:
        return pmr(slot,idx,pg,STATUS_TEMPERATURE,1)[0]
    return pmw(slot,idx,pg,STATUS_TEMPERATURE,bytes([value]))

def status_cml(slot,idx,pg,value=None):
    '''Communication and memory fault and warning status.
    '''
    if value is None:
        return pmr(slot,idx,pg,STATUS_CML,1)[0]
    return pmw(slot,idx,pg,STATUS_CML,bytes([value]))

def status_mfr_specific(slot,idx,pg,value=None):
    '''Manufacturer specific fault and state information.
    '''
    if value is None:
        return pmr(slot,idx,pg,STATUS_MFR_SPECIFIC,1)[0]
    return pmw(slot,idx,pg,STATUS_MFR_SPECIFIC,bytes([value]))

def read_vin(slot,idx,pg):
    '''Measured input supply voltage.
    '''
    return fromL11(pmr(slot,idx,pg,READ_VIN,2))

def read_iin(slot,idx,pg):
    '''Measured input supply current.
    '''
    return fromL11(pmr(slot,idx,pg,READ_IIN,2))

def read_vout(slot,idx,pg):
    '''Measured output voltage.
    '''
    return fromL16(slot,idx,pg,pmr(slot,idx,pg,READ_VOUT,2))

def read_iout(slot,idx,pg):
    '''Measured output current.
    '''
    return fromL11(pmr(slot,idx,pg,READ_IOUT,2))

def read_temperature_1(slot,idx,pg):
    '''External temperature sensor temperature. This is the value used for all temperature related processing, including IOUT_CAL_GAIN.
    '''
    return fromL11(pmr(slot,idx,pg,READ_TEMPERATURE_1,2))

def read_temperature_2(slot,idx,pg):
    '''Internal die junction temperature. Does not affect any other commands.
    '''
    return fromL11(pmr(slot,idx,pg,READ_TEMPERATURE_2,2))

def read_frequency(slot,idx,pg):
    '''Measured PWM switching frequency.
    '''
    return fromL11(pmr(slot,idx,pg,READ_FREQUENCY,2))

def read_pout(slot,idx,pg):
    '''Measured output power.
    '''
    return fromL11(pmr(slot,idx,pg,READ_POUT,2))

def read_pin(slot,idx,pg):
    '''Calculated input power.
    '''
    return fromL11(pmr(slot,idx,pg,READ_PIN,2))

def pmbus_revision(slot,idx,pg):
    '''PMBus revision supported by this device. Current revision is 1.2.
    '''
    return pmr(slot,idx,pg,PMBUS_REVISION,1)[0]

def mfr_vout_max(slot,idx,pg):
    '''Maximum allowed output voltage including VOUT_OV_FAULT_LIMIT.
    '''
    return fromL16(slot,idx,pg,pmr(slot,idx,pg,MFR_VOUT_MAX,2))

def display(slot,idx,pg):
    print('{:>22s} {}'.format('page',pg))
    print('{:>22s} {:02X}h'.format('operation',operation(slot,idx,pg)))
    print('{:>22s} {:02X}h'.format('on_off_config',on_off_config(slot,idx,pg)))
    print('{:>22s} {:02X}h'.format('write_protect',write_protect(slot,idx,pg)))
    print('{:>22s} {:02X}h'.format('capability',capability(slot,idx,pg)))
    print('{:>22s} {:02X}h'.format('vout_mode',vout_mode(slot,idx,pg)))
    print('{:>22s} {:8.4f} V'.format('vout_command',vout_command(slot,idx,pg)))
    print('{:>22s} {:8.4f} V'.format('vout_max',vout_max(slot,idx,pg)))
    print('{:>22s} {:8.4f} V'.format('vout_margin_high',vout_margin_high(slot,idx,pg)))
    print('{:>22s} {:8.4f} V'.format('vout_margin_low',vout_margin_low(slot,idx,pg)))
    print('{:>22s} {:8.4f} V'.format('vout_transition_rate',vout_transition_rate(slot,idx,pg)))
    print('{:>22s} {:8.4f} kHz'.format('frequency_switch',frequency_switch(slot,idx,pg)))
    print('{:>22s} {:8.4f} V'.format('vin_on',vin_on(slot,idx,pg)))
    print('{:>22s} {:8.4f} V'.format('vin_off',vin_off(slot,idx,pg)))
    print('{:>22s} {:8.4f} V'.format('vout_ov_fault_limit',vout_ov_fault_limit(slot,idx,pg)))
    print('{:>22s} {:02X}h'.format('vout_ov_fault_response',vout_ov_fault_response(slot,idx,pg)))
    print('{:>22s} {:8.4f} V'.format('vout_ov_warn_limit',vout_ov_warn_limit(slot,idx,pg)))
    print('{:>22s} {:8.4f} V'.format('vout_uv_warn_limit',vout_uv_warn_limit(slot,idx,pg)))
    print('{:>22s} {:8.4f} V'.format('vout_uv_fault_limit',vout_uv_fault_limit(slot,idx,pg)))
    print('{:>22s} {:02X}h'.format('vout_uv_fault_response',vout_uv_fault_response(slot,idx,pg)))
    print('{:>22s} {:8.4f} A'.format('iout_oc_fault_limit',iout_oc_fault_limit(slot,idx,pg)))
    print('{:>22s} {:02X}h'.format('iout_oc_fault_response',iout_oc_fault_response(slot,idx,pg)))
    print('{:>22s} {:8.4f} A'.format('iout_oc_warn_limit',iout_oc_warn_limit(slot,idx,pg)))
    print('{:>22s} {:8.4f} °C'.format('ot_fault_limit',ot_fault_limit(slot,idx,pg)))
    print('{:>22s} {:02X}h'.format('ot_fault_response',ot_fault_response(slot,idx,pg)))
    print('{:>22s} {:8.4f} °C'.format('ot_warn_limit',ot_warn_limit(slot,idx,pg)))
    print('{:>22s} {:8.4f} °C'.format('ut_fault_limit',ut_fault_limit(slot,idx,pg)))
    print('{:>22s} {:02X}h'.format('ut_fault_response',ut_fault_response(slot,idx,pg)))
    print('{:>22s} {:8.4f} V'.format('vin_ov_fault_limit',vin_ov_fault_limit(slot,idx,pg)))
    print('{:>22s} {:02X}h'.format('vin_ov_fault_response',vin_ov_fault_response(slot,idx,pg)))
    print('{:>22s} {:8.4f} V'.format('vin_uv_warn_limit',vin_uv_warn_limit(slot,idx,pg)))
    print('{:>22s} {:8.4f} A'.format('iin_oc_warn_limit',iin_oc_warn_limit(slot,idx,pg)))
    print('{:>22s} {:8.4f} ms'.format('ton_delay',ton_delay(slot,idx,pg)))
    print('{:>22s} {:8.4f} ms'.format('ton_rise',ton_rise(slot,idx,pg)))
    print('{:>22s} {:8.4f} ms'.format('ton_max_fault_limit',ton_max_fault_limit(slot,idx,pg)))
    print('{:>22s} {:02X}h'.format('ton_max_fault_response',ton_max_fault_response(slot,idx,pg)))
    print('{:>22s} {:8.4f} ms'.format('toff_delay',toff_delay(slot,idx,pg)))
    print('{:>22s} {:8.4f} ms'.format('toff_fall',toff_fall(slot,idx,pg)))
    print('{:>22s} {:8.4f} ms'.format('toff_max_warn_limit',toff_max_warn_limit(slot,idx,pg)))
    print('{:>22s} {:02X}h'.format('status_byte',status_byte(slot,idx,pg)))
    print('{:>22s} {:04X}h'.format('status_word',status_word(slot,idx,pg)))
    print('{:>22s} {:02X}h'.format('status_vout',status_vout(slot,idx,pg)))
    print('{:>22s} {:02X}h'.format('status_iout',status_iout(slot,idx,pg)))
    print('{:>22s} {:02X}h'.format('status_input',status_input(slot,idx,pg)))
    print('{:>22s} {:02X}h'.format('status_temperature',status_temperature(slot,idx,pg)))
    print('{:>22s} {:02X}h'.format('status_cml',status_cml(slot,idx,pg)))
    print('{:>22s} {:02X}h'.format('status_mfr_specific',status_mfr_specific(slot,idx,pg)))
    print('{:>22s} {:8.4f} V'.format('read_vin',read_vin(slot,idx,pg)))
    print('{:>22s} {:8.4f} A'.format('read_iin',read_iin(slot,idx,pg)))
    print('{:>22s} {:8.4f} V'.format('read_vout',read_vout(slot,idx,pg)))
    print('{:>22s} {:8.4f} A'.format('read_iout',read_iout(slot,idx,pg)))
    print('{:>22s} {:8.4f} °C'.format('read_temperature_1',read_temperature_1(slot,idx,pg)))
    print('{:>22s} {:8.4f} °C'.format('read_temperature_2',read_temperature_2(slot,idx,pg)))
    print('{:>22s} {:8.4f} Hz'.format('read_frequency',read_frequency(slot,idx,pg)))
    print('{:>22s} {:8.4f} W'.format('read_pout',read_pout(slot,idx,pg)))
    print('{:>22s} {:8.4f} W'.format('read_pin',read_pin(slot,idx,pg)))
    print('{:>22s} {:02X}h'.format('pmbus_revision',pmbus_revision(slot,idx,pg)))
    print('{:>22s} {:8.4f} V'.format('mfr_vout_max',mfr_vout_max(slot,idx,pg)))
    print()

if __name__ == '__main__':
    slot = int(input('Slot? '))
    idx = int(input('Index? '))
    display(slot,idx,0)
    display(slot,idx,1)
