# Copyright 2018-2020 Intel Corporation. All rights reserved.
#
# This software is supplied under the terms of an executed license agreement with Intel Corporation and
# may not be copied, transferred or used except in accordance with that agreement. All disclosures to third
# parties are expressly limited and defined by an executed non-disclosure agreement with Intel Corporation.

import time
import multiprocessing
import struct
import re
import sys

# Allow hil_utils to be imported as a package where hil.py/hil_utils.py reside
# in a directory other than the current one.
try:
    import hil
except ImportError:
    from . import hil

def _rc3IsFabB():
    try:
        r = hil.rc3EepromRead(0,1)
        return True
    except RuntimeError as e:
        if e.args[0] == 'Function not supported.':
            return False
        else:
            raise

def _rcTriggerWaiter(q,b):
    '''Internal helper function for rcOutOfProcessTriggerWait.
    This function is run under multiprocessing.Process().
    '''
    try:
        b.wait()
        value = hil.rcTriggerWait()
        q.put(value)
    except RuntimeError:
        pass

def rcOutOfProcessTriggerWait(timeout):
    '''This is a simple wrapper to run hil.rcTriggerWait(slot) in another
    process.  If no trigger is returned by the process within the timeout
    period, the trigger wait is canceled, the process exits, and None is returned;
    otherwise, the trigger data is returned and the process still exits.
    '''
    q = multiprocessing.Queue()
    b = multiprocessing.Barrier(2)
    p = multiprocessing.Process(target=_rcTriggerWaiter,args=(q,b))
    p.start()
    b.wait()
    try:
        p.join(timeout)
    finally:
        if p.is_alive():
            hil.rcTriggerWaitCancel()
            p.join()
    if q.empty():
        return None
    return q.get()

def _rcAlarmWaiter(q,b):
    '''Internal helper function for rcOutOfProcessAlarmWait.
    This function is run under multiprocessing.Process().
    '''
    try:
        b.wait()
        value = hil.rcAlarmWait()
        q.put(value)
    except RuntimeError:
        pass

def rcOutOfProcessAlarmWait(timeout):
    '''This is a simple wrapper to run hil.rcAlarmWait() in another
    process.  If no alarm is returned by the process within the timeout
    period, the alarm is canceled, the process exits, and None is returned;
    otherwise, the alarm value is returned and the process still exits.
    '''
    # A queue is passed to the process to support passing the return value back.
    q = multiprocessing.Queue()
    b = multiprocessing.Barrier(2)
    p = multiprocessing.Process(target=_rcAlarmWaiter,args=(q,b))
    p.start()
    b.wait()
    try:
        p.join(timeout)
    finally:
        if p.is_alive():
            hil.rcAlarmWaitCancel()
            p.join()
    if q.empty():
        return None
    return q.get()

def _rc3AlarmWaiter(q,b):
    '''Internal helper function for rc3OutOfProcessAlarmWait.
    This function is run under multiprocessing.Process().
    '''
    try:
        b.wait()
        value = hil.rc3AlarmWait()
        q.put(value)
    except RuntimeError:
        pass

def rc3OutOfProcessAlarmWait(timeout):
    '''This is a simple wrapper to run hil.rc3AlarmWait() in another
    process.  If no alarm is returned by the process within the timeout
    period, the alarm is canceled, the process exits, and None is returned;
    otherwise, the alarm value is returned and the process still exits.
    '''
    # A queue is passed to the process to support passing the return value back.
    q = multiprocessing.Queue()
    b = multiprocessing.Barrier(2)
    p = multiprocessing.Process(target=_rc3AlarmWaiter,args=(q,b))
    p.start()
    b.wait()
    try:
        p.join(timeout)
    finally:
        if p.is_alive():
            hil.rc3AlarmWaitCancel()
            p.join()
    if q.empty():
        return None
    return q.get()

####################################################################
# RC HIL Utilities functions

regArray = [
    0x0000A018, #0   CLKout00_01_PD=0,                        , CLKout01_ADLY_SEL=0, CLKout00_ADLY_SEL=0, CLKout00_01_DDLY=0x005, RESET=0, CLKout01_02_HS=0, CLKout00_01_DIV=0x018
    0x0000A018, #1   CLKout02_03_PD=0,                        , CLKout03_ADLY_SEL=0, CLKout02_ADLY_SEL=0, CLKout02_03_DDLY=0x005, PWRDN=0, CLKout02_03_HS=0, CLKout02_03_DIV=0x018
    0x0000A018, #2   CLKout04_05_PD=0,                        , CLKout05_ADLY_SEL=0, CLKout04_ADLY_SEL=0, CLKout04_05_DDLY=0x005,        , CLKout04_05_HS=0, CLKout04_05_DIV=0x018
    0x0000A060, #3   CLKout06_07_PD=0, CLKout06_07_OSCin_Sel=0, CLKout07_ADLY_SEL=0, CLKout06_ADLY_SEL=0, CLKout06_07_DDLY=0x005,        , CLKout06_07_HS=0, CLKout06_07_DIV=0x060
    0x0000A01E, #4   CLKout08_09_PD=0, CLKout08_09_OSCin_Sel=0, CLKout09_ADLY_SEL=0, CLKout08_ADLY_SEL=0, CLKout08_09_DDLY=0x005,        , CLKout08_09_HS=0, CLKout08_09_DIV=0x01E
    0x0000A12C, #5   CLKout10_11_PD=0,                        , CLKout11_ADLY_SEL=0, CLKout10_ADLY_SEL=0, CLKout10_11_DDLY=0x005,        , CLKout10_11_HS=0, CLKout10_11_DIV=0x12C
    0x00A20800, #6   CLKout03_TYPE=0x01, CLKout02_TYPE=0x04, CLKout01_TYPE=0x04, CLKout00_TYPE=0x01, CLKout02_03_ADLY=0x00, 0, CLKout00_01_ADLY=0x00
    0x02208800, #7   CLKout07_TYPE=0x04, CLKout06_TYPE=0x04, CLKout05_TYPE=0x01, CLKout04_TYPE=0x01, CLKout06_07_ADLY=0x00, 0, CLKout04_05_ADLY=0x00
    0x02222000, #8   CLKout11_TYPE=0x04, CLKout10_TYPE=0x04, CLKout09_TYPE=0x04, CLKout08_TYPE=0x04, CLKout10_11_ADLY=0x00, 0, CLKout08_09_ADLY=0x00
    0x02AAAAAA, #9   010_1010_1010_1010_1010_1010_1010 - Fixed bit values specified by the data sheet. - Must be written
    0x00880A4C, #10  OSCout1_LVPECL_AMP=0x0, 0, 1, OSCout0_TYPE=0x01, EN_OSCout1=0, EN_OSCout0=0, OSCout1_MUX=0, OSCout0_MUX=0, PD_OSCin=0, OSCout_DIV=0x1, 0, 1, 0 VCO_MUX=0, EN_FEEDBACK_MUX=1, VCO_DIV=0x1, FEEDBACK_MUX=0x4
    0x00200180, #11  MODE=0x00, EN_SYNC=0, NO_SYNC_CLKout10_11=0, NO_SYNC_CLKout8_9=0, NO_SYNC_CLKout6_7=0, NO_SYNC_CLKout4_5=0, NO_SYNC_CLKout2_3=0, NO_SYNC_CLKout0_1=0, SYNC_MUX=0x0, SYNC_QUAL=0, SYNC_POL_INV=0, SYNC_EN_AUTO=0, SYNC_TYPE=0x3, 0,0,0,0,0,0, EN_PLL2_XTAL=0x0
    0x00D96003, #12  LD_MUX=0x03, LD_TYPE=0x3, SYNC_PLL2_DLD=0, SYNCPLL1_DLD=0, 1,0,1,1,0,0,0,0,0,0,0,0,0, EN_TRACK=0, HOLDOVER_MODE=0x1, 1
    0x01D81403, #13  HOLDOVER_MUX=0x7, HOLDOVER_TYPE=0x3, 0, Status_CLKin1_MUX=0x0, 0, Status_CLKin0_TYPE=0x2, DISABLE_DLD1_DET=1, Status_CLKin0_MUX=0x0, CLKin_Select_MODE=0x0, CLKin_Sel_INV=0, 0, EN_CLKin1=1, EN_CLKin0=1
    0x00100000, #14  LOS_TIMEOUT=0x0, 0, EN_LOS=0, 0, Status_CLKin1_TYPE=0x4, 0, 0, CLKin1_BUF_TYPE=0, CLKin0_BUF_TYPE=0, DAC_HIGH_TRIP=0x00, 0, 0, DAC_LOW_TRIP=0x00, EN_VTUNE_RAIL_DET=0
    0x04000400, #15  MAN_DAC=0x200, 0, EN_MAN_DAC=0, HOLDOVER_DLD_CNT=0x0200, FORCE_HOLDOVER=0
    0x060AA820, #16  XTAL_LVL=0x3, 0_0000_1010_1010_1000_0010_0000
    0x00000000, #17  this value is ignored within the Lmk04808FullWrite
    0x00000000, #18  this value is ignored within the Lmk04808FullWrite
    0x00000000, #19  this value is ignored within the Lmk04808FullWrite
    0x00000000, #20  this value is ignored within the Lmk04808FullWrite
    0x00000000, #21  this value is ignored within the Lmk04808FullWrite
    0x00000000, #22  this value is ignored within the Lmk04808FullWrite
    0x00000000, #23  this value is ignored within the Lmk04808FullWrite
    0x00000002, #24  PLL2_CF_LF=0x0, PLL2_C3_LF=0x0, 0, PLL2_R4_LF=0x0, 0, PLL2_R3_LF=0x0, 0, PLL1_N_DLY=0x0, 0, PLL1_R_DLY=0x0, PLL1_WND_SIZE=0x1, 0
    0x00164E20, #25  DAC_CLK_DIV=0x00B, 0, 0, PLL1_DLD_CNT=0x4E20
    0x047D4000, #26  PLL2_WND_SIZE=0x2, EN_PLL2_REF_2X=0, PLL2_CP_POL=0, PLL2_CP_GAIN=0x3, 1, 1, 1, 0, 1, 0, PLL2_DLD_CNT=0x2000, PLL2_CP_TRI=0
    0x00800014, #27  0,0,0, PLL1_CP_POL=1, PLL1_CP_GAIN0x0, 0,0, CLKin1_PreR_DIV=0x0, CLKin0_PreR_DIV=0x0, PLL1_R=10, PLL1_CP_TRI=0
    0x000080FA, #28  PLL2_R=0x0001, PLL1_N=125, 0
    0x000C000C, #29  0,0,0,0,0, OSCin_FREQ=0x1, PLL2_FAST_PDF=1, PLL2_N_CAL=0x0000C
    0x0010000C, #30  0,0,0,0,0, PLL2_P=0x2, 0, PLL2_N=0x0000C
    0x0000F800  #31  0,0,0,0,0,0,0,0,0,0, READBACK_LE=0, READBACK_ADDR=0x1F, 0,0,0,0,0,0,0,0,0,0, uWire_LOCK=0
]

def rcClocksInit():
    hil.rcLmk04808FullWrite(regArray)

def rc3ClocksInit(force=False):
    if _rc3IsFabB():
        _rc3ClocksInitB(force)
    else:
        _rc3ClocksInitA(force)

def _rc3ClocksInitB(force):
    if not force and hil.rc3Lmk04832Read(0x148) == 0x33:
        return # Already programmed
    hil.rc3Lmk04832Write(0x0000,0x90)
    time.sleep(.5)
    hil.rc3Lmk04832Write(0x0000,0x10)
    hil.rc3Lmk04832Write(0x0002,0x00)
    hil.rc3Lmk04832Write(0x0003,0x06)
    hil.rc3Lmk04832Write(0x0004,0xD1)
    hil.rc3Lmk04832Write(0x0005,0x63)
    hil.rc3Lmk04832Write(0x0006,0x50)
    hil.rc3Lmk04832Write(0x000C,0x51)
    hil.rc3Lmk04832Write(0x000D,0x04)
    hil.rc3Lmk04832Write(0x0100,0x18)  # 7:0 = DCLK0_1_DIV[7:0] (0x018 = 125MHz)
    hil.rc3Lmk04832Write(0x0101,0x0A)
    hil.rc3Lmk04832Write(0x0102,0x60)  # 1:0 = DCLK0_1_DIV[9:8]
    hil.rc3Lmk04832Write(0x0103,0x40)
    hil.rc3Lmk04832Write(0x0104,0x1C)
    hil.rc3Lmk04832Write(0x0105,0x00)
    hil.rc3Lmk04832Write(0x0106,0x01)
    hil.rc3Lmk04832Write(0x0107,0x05)
    hil.rc3Lmk04832Write(0x0108,0x60)  # 7:0 = DCLK2_3_DIV[7:0] (0x060 = 31.25MHz)
    hil.rc3Lmk04832Write(0x0109,0x0A)
    hil.rc3Lmk04832Write(0x010A,0x60)  # 1:0 = DCLK2_3_DIV[9:8]
    hil.rc3Lmk04832Write(0x010B,0x40)
    hil.rc3Lmk04832Write(0x010C,0x10)
    hil.rc3Lmk04832Write(0x010D,0x00)
    hil.rc3Lmk04832Write(0x010E,0x01)
    hil.rc3Lmk04832Write(0x010F,0x55)
    hil.rc3Lmk04832Write(0x0110,0x18)  # 7:0 = DCLK4_5_DIV[7:0] (0x018 = 125MHz)
    hil.rc3Lmk04832Write(0x0111,0x0A)
    hil.rc3Lmk04832Write(0x0112,0x60)  # 1:0 = DCLK4_5_DIV[9:8]
    hil.rc3Lmk04832Write(0x0113,0x44)
    hil.rc3Lmk04832Write(0x0114,0x10)
    hil.rc3Lmk04832Write(0x0115,0x00)
    hil.rc3Lmk04832Write(0x0116,0x01)
    hil.rc3Lmk04832Write(0x0117,0x55)
    hil.rc3Lmk04832Write(0x0118,0x18)  # 7:0 = DCLK6_7_DIV[7:0] (0x018 = 125MHz)
    hil.rc3Lmk04832Write(0x0119,0x0A)
    hil.rc3Lmk04832Write(0x011A,0x60)  # 1:0 = DCLK6_7_DIV[9:8]
    hil.rc3Lmk04832Write(0x011B,0x40)
    hil.rc3Lmk04832Write(0x011C,0x1C)
    hil.rc3Lmk04832Write(0x011D,0x00)
    hil.rc3Lmk04832Write(0x011E,0x01)
    hil.rc3Lmk04832Write(0x011F,0x05)
    hil.rc3Lmk04832Write(0x0120,0x2C)  # 7:0 = DCLK8_9_DIV[7:0] (0x12C = 10MHz)
    hil.rc3Lmk04832Write(0x0121,0x0A)
    hil.rc3Lmk04832Write(0x0122,0x61)  # 1:0 = DCLK8_9_DIV[9:8]
    hil.rc3Lmk04832Write(0x0123,0x40)
    hil.rc3Lmk04832Write(0x0124,0x10)
    hil.rc3Lmk04832Write(0x0125,0x00)
    hil.rc3Lmk04832Write(0x0126,0x01)
    hil.rc3Lmk04832Write(0x0127,0x55)
    hil.rc3Lmk04832Write(0x0128,0x58)  # 7:0 = DCLK10_11_DIV[7:0] (0x258 = 5MHz)
    hil.rc3Lmk04832Write(0x0129,0x0A)
    hil.rc3Lmk04832Write(0x012A,0xE2)  # 1:0 = DCLK10_11_DIV[9:8]
    hil.rc3Lmk04832Write(0x012B,0x40)
    hil.rc3Lmk04832Write(0x012C,0x10)
    hil.rc3Lmk04832Write(0x012D,0x00)
    hil.rc3Lmk04832Write(0x012E,0x01)
    hil.rc3Lmk04832Write(0x012F,0x50)
    hil.rc3Lmk04832Write(0x0130,0x1E)  # 7:0 = DCLK12_13_DIV[7:0] (0x01E = 100MHz)
    hil.rc3Lmk04832Write(0x0131,0x0A)
    hil.rc3Lmk04832Write(0x0132,0x60)  # 1:0 = DCLK12_13_DIV[9:8]
    hil.rc3Lmk04832Write(0x0133,0x44)
    hil.rc3Lmk04832Write(0x0134,0x10)
    hil.rc3Lmk04832Write(0x0135,0x00)
    hil.rc3Lmk04832Write(0x0136,0x01)
    hil.rc3Lmk04832Write(0x0137,0x55)
    hil.rc3Lmk04832Write(0x0138,0x30)
    hil.rc3Lmk04832Write(0x0139,0x00)
    hil.rc3Lmk04832Write(0x013A,0x0C)
    hil.rc3Lmk04832Write(0x013B,0x00)
    hil.rc3Lmk04832Write(0x013C,0x00)
    hil.rc3Lmk04832Write(0x013D,0x08)
    hil.rc3Lmk04832Write(0x013E,0x03)
    hil.rc3Lmk04832Write(0x013F,0x09)
    hil.rc3Lmk04832Write(0x0140,0x0F)
    hil.rc3Lmk04832Write(0x0141,0x00)
    hil.rc3Lmk04832Write(0x0142,0x00)
    hil.rc3Lmk04832Write(0x0143,0x11)
    hil.rc3Lmk04832Write(0x0144,0x00)
    hil.rc3Lmk04832Write(0x0145,0x00)
    hil.rc3Lmk04832Write(0x0146,0x38)
    hil.rc3Lmk04832Write(0x0147,0x06)  # 0x06 for CLKIN0 (10MHZ), 0x26 for CLKIN2 (5MHz HIPI)
    hil.rc3Lmk04832Write(0x0148,0x33)  # 0x33 selects READBACK
    hil.rc3Lmk04832Write(0x0149,0x42)
    hil.rc3Lmk04832Write(0x014A,0x03)
    hil.rc3Lmk04832Write(0x014B,0x06)
    hil.rc3Lmk04832Write(0x014C,0x00)
    hil.rc3Lmk04832Write(0x014D,0x00)
    hil.rc3Lmk04832Write(0x014E,0xC0)
    hil.rc3Lmk04832Write(0x014F,0x7F)
    hil.rc3Lmk04832Write(0x0150,0x01)
    hil.rc3Lmk04832Write(0x0151,0x02)
    hil.rc3Lmk04832Write(0x0152,0x00)
    hil.rc3Lmk04832Write(0x0153,0x00)
    hil.rc3Lmk04832Write(0x0154,0x02)
    hil.rc3Lmk04832Write(0x0155,0x00)
    hil.rc3Lmk04832Write(0x0156,0x78)
    hil.rc3Lmk04832Write(0x0157,0x00)
    hil.rc3Lmk04832Write(0x0158,0x96)
    hil.rc3Lmk04832Write(0x0159,0x00)
    hil.rc3Lmk04832Write(0x015A,0x19)
    hil.rc3Lmk04832Write(0x015B,0xD4)
    hil.rc3Lmk04832Write(0x015C,0x20)
    hil.rc3Lmk04832Write(0x015D,0x00)
    hil.rc3Lmk04832Write(0x015E,0x1E)
    hil.rc3Lmk04832Write(0x015F,0x0B)
    hil.rc3Lmk04832Write(0x0160,0x00)
    hil.rc3Lmk04832Write(0x0161,0x01)
    hil.rc3Lmk04832Write(0x0162,0x4D)
    hil.rc3Lmk04832Write(0x0163,0x00)
    hil.rc3Lmk04832Write(0x0164,0x00)
    hil.rc3Lmk04832Write(0x0165,0x06)
    hil.rc3Lmk04832Write(0x0169,0x58)
    hil.rc3Lmk04832Write(0x016A,0x20)
    hil.rc3Lmk04832Write(0x016B,0x00)
    hil.rc3Lmk04832Write(0x016C,0x00)
    hil.rc3Lmk04832Write(0x016D,0x00)
    hil.rc3Lmk04832Write(0x016E,0x13)
    hil.rc3Lmk04832Write(0x0173,0x10)
    hil.rc3Lmk04832Write(0x0177,0x00)
    hil.rc3Lmk04832Write(0x0182,0x00)
    hil.rc3Lmk04832Write(0x0183,0x00)
    hil.rc3Lmk04832Write(0x0166,0x00)
    hil.rc3Lmk04832Write(0x0167,0x00)
    hil.rc3Lmk04832Write(0x0168,0x06)
    hil.rc3Lmk04832Write(0x0555,0x00)

def _rc3ClocksInitA(force):
    if not force and hil.rc3Lmk04832Read(0x148) == 0x33:
        return # Already programmed
    hil.rc3Lmk04832Write(0x0000,0x90)
    time.sleep(.5)
    hil.rc3Lmk04832Write(0x0000,0x10)
    hil.rc3Lmk04832Write(0x0002,0x00)
    hil.rc3Lmk04832Write(0x0003,0x06)
    hil.rc3Lmk04832Write(0x0004,0xD1)
    hil.rc3Lmk04832Write(0x0005,0x63)
    hil.rc3Lmk04832Write(0x0006,0x50)
    hil.rc3Lmk04832Write(0x000C,0x51)
    hil.rc3Lmk04832Write(0x000D,0x04)
    hil.rc3Lmk04832Write(0x0100,0x18)  # 7:0 = DCLK0_1_DIV[7:0] (0x018 = 125MHz)
    hil.rc3Lmk04832Write(0x0101,0x0A)
    hil.rc3Lmk04832Write(0x0102,0x00)  # 1:0 = DCLK0_1_DIV[9:8]
    hil.rc3Lmk04832Write(0x0103,0x40)
    hil.rc3Lmk04832Write(0x0104,0x10)
    hil.rc3Lmk04832Write(0x0105,0x00)
    hil.rc3Lmk04832Write(0x0106,0x01)
    hil.rc3Lmk04832Write(0x0107,0x55)
    hil.rc3Lmk04832Write(0x0108,0x2C)  # 7:0 = DCLK2_3_DIV[7:0] (0x12C = 10MHz)
    hil.rc3Lmk04832Write(0x0109,0x0A)
    hil.rc3Lmk04832Write(0x010A,0x01)  # 1:0 = DCLK2_3_DIV[9:8]
    hil.rc3Lmk04832Write(0x010B,0x40)
    hil.rc3Lmk04832Write(0x010C,0x10)
    hil.rc3Lmk04832Write(0x010D,0x00)
    hil.rc3Lmk04832Write(0x010E,0x01)
    hil.rc3Lmk04832Write(0x010F,0x55)
    hil.rc3Lmk04832Write(0x0110,0x1E)  # 7:0 = DCLK4_5_DIV[7:0] (0x01E = 100MHz)
    hil.rc3Lmk04832Write(0x0111,0x0A)
    hil.rc3Lmk04832Write(0x0112,0x00)  # 1:0 = DCLK4_5_DIV[9:8]
    hil.rc3Lmk04832Write(0x0113,0x40)
    hil.rc3Lmk04832Write(0x0114,0x10)
    hil.rc3Lmk04832Write(0x0115,0x00)
    hil.rc3Lmk04832Write(0x0116,0x01)
    hil.rc3Lmk04832Write(0x0117,0x55)
    hil.rc3Lmk04832Write(0x0118,0x18)  # 7:0 = DCLK6_7_DIV[7:0] (0x018 = 125MHz)
    hil.rc3Lmk04832Write(0x0119,0x0A)
    hil.rc3Lmk04832Write(0x011A,0x00)  # 1:0 = DCLK6_7_DIV[9:8]
    hil.rc3Lmk04832Write(0x011B,0x40)
    hil.rc3Lmk04832Write(0x011C,0x10)
    hil.rc3Lmk04832Write(0x011D,0x00)
    hil.rc3Lmk04832Write(0x011E,0x01)
    hil.rc3Lmk04832Write(0x011F,0x55)
    hil.rc3Lmk04832Write(0x0120,0x60)  # 7:0 = DCLK8_9_DIV[7:0] (0x060 = 31.25MHz)
    hil.rc3Lmk04832Write(0x0121,0x0A)
    hil.rc3Lmk04832Write(0x0122,0x00)  # 1:0 = DCLK8_9_DIV[9:8]
    hil.rc3Lmk04832Write(0x0123,0x40)
    hil.rc3Lmk04832Write(0x0124,0x10)
    hil.rc3Lmk04832Write(0x0125,0x00)
    hil.rc3Lmk04832Write(0x0126,0x01)
    hil.rc3Lmk04832Write(0x0127,0x55)
    hil.rc3Lmk04832Write(0x0128,0x58)  # 7:0 = DCLK10_11_DIV[7:0] (0x258 = 5MHz)
    hil.rc3Lmk04832Write(0x0129,0x0A)
    hil.rc3Lmk04832Write(0x012A,0x02)  # 1:0 = DCLK10_11_DIV[9:8]
    hil.rc3Lmk04832Write(0x012B,0x40)
    hil.rc3Lmk04832Write(0x012C,0x10)
    hil.rc3Lmk04832Write(0x012D,0x00)
    hil.rc3Lmk04832Write(0x012E,0x01)
    hil.rc3Lmk04832Write(0x012F,0x55)
    hil.rc3Lmk04832Write(0x0130,0x1E)  # 7:0 = DCLK12_13_DIV[7:0] (0x01E = 100MHz)
    hil.rc3Lmk04832Write(0x0131,0x0A)
    hil.rc3Lmk04832Write(0x0132,0x00)  # 1:0 = DCLK12_13_DIV[9:8]
    hil.rc3Lmk04832Write(0x0133,0x40)
    hil.rc3Lmk04832Write(0x0134,0x10)
    hil.rc3Lmk04832Write(0x0135,0x00)
    hil.rc3Lmk04832Write(0x0136,0x01)
    hil.rc3Lmk04832Write(0x0137,0x55)
    hil.rc3Lmk04832Write(0x0138,0x30)
    hil.rc3Lmk04832Write(0x0139,0x00)
    hil.rc3Lmk04832Write(0x013A,0x0C)
    hil.rc3Lmk04832Write(0x013B,0x00)
    hil.rc3Lmk04832Write(0x013C,0x00)
    hil.rc3Lmk04832Write(0x013D,0x08)
    hil.rc3Lmk04832Write(0x013E,0x03)
    hil.rc3Lmk04832Write(0x013F,0x0F)
    hil.rc3Lmk04832Write(0x0140,0x0F)
    hil.rc3Lmk04832Write(0x0141,0x00)
    hil.rc3Lmk04832Write(0x0142,0x00)
    hil.rc3Lmk04832Write(0x0143,0x11)
    hil.rc3Lmk04832Write(0x0144,0x00)
    hil.rc3Lmk04832Write(0x0145,0x00)
    hil.rc3Lmk04832Write(0x0146,0x38)
    hil.rc3Lmk04832Write(0x0147,0x06)  # 0x06 for CLKIN0 (10MHZ), 0x26 for CLKIN2 (5MHz HIPI)
    hil.rc3Lmk04832Write(0x0148,0x33)  # 0x33 selects READBACK
    hil.rc3Lmk04832Write(0x0149,0x42)
    hil.rc3Lmk04832Write(0x014A,0x03)
    hil.rc3Lmk04832Write(0x014B,0x06)
    hil.rc3Lmk04832Write(0x014C,0x00)
    hil.rc3Lmk04832Write(0x014D,0x00)
    hil.rc3Lmk04832Write(0x014E,0xC0)
    hil.rc3Lmk04832Write(0x014F,0x7F)
    hil.rc3Lmk04832Write(0x0150,0x01)
    hil.rc3Lmk04832Write(0x0151,0x02)
    hil.rc3Lmk04832Write(0x0152,0x00)
    hil.rc3Lmk04832Write(0x0153,0x00)
    hil.rc3Lmk04832Write(0x0154,0x02)
    hil.rc3Lmk04832Write(0x0155,0x00)
    hil.rc3Lmk04832Write(0x0156,0x78)
    hil.rc3Lmk04832Write(0x0157,0x00)
    hil.rc3Lmk04832Write(0x0158,0x01)
    hil.rc3Lmk04832Write(0x0159,0x00)
    hil.rc3Lmk04832Write(0x015A,0x19)
    hil.rc3Lmk04832Write(0x015B,0xD4)
    hil.rc3Lmk04832Write(0x015C,0x20)
    hil.rc3Lmk04832Write(0x015D,0x00)
    hil.rc3Lmk04832Write(0x015E,0x1E)
    hil.rc3Lmk04832Write(0x015F,0x0B)
    hil.rc3Lmk04832Write(0x0160,0x00)
    hil.rc3Lmk04832Write(0x0161,0x01)
    hil.rc3Lmk04832Write(0x0162,0x4C)
    hil.rc3Lmk04832Write(0x0163,0x00)
    hil.rc3Lmk04832Write(0x0164,0x00)
    hil.rc3Lmk04832Write(0x0165,0x0C)
    hil.rc3Lmk04832Write(0x0169,0x58)
    hil.rc3Lmk04832Write(0x016A,0x20)
    hil.rc3Lmk04832Write(0x016B,0x00)
    hil.rc3Lmk04832Write(0x016C,0x00)
    hil.rc3Lmk04832Write(0x016D,0x00)
    hil.rc3Lmk04832Write(0x016E,0x13)
    hil.rc3Lmk04832Write(0x0173,0x10)
    hil.rc3Lmk04832Write(0x0177,0x00)
    hil.rc3Lmk04832Write(0x0182,0x00)
    hil.rc3Lmk04832Write(0x0183,0x00)
    hil.rc3Lmk04832Write(0x0166,0x00)
    hil.rc3Lmk04832Write(0x0167,0x00)
    hil.rc3Lmk04832Write(0x0168,0x0C)
    hil.rc3Lmk04832Write(0x0555,0x00)

####################################################################
# TiuCalBase HIL Utilities functions

tiuRegArray = [
    ( 0,0x0000A018), # CLKout00_01_PD = 0, , CLKout01_ADLY_SEL = 0, CLKout00_ADLY_SEL = 0, CLKout00_01_DDLY = 0x005, RESET = 0, CLKout01_02_HS = 0, CLKout00_01_DIV = 0x018
    ( 1,0x0000A258), # CLKout02_03_PD = 0, , CLKout03_ADLY_SEL = 0, CLKout02_ADLY_SEL = 0, CLKout02_03_DDLY = 0x005, PWRDN = 0, CLKout02_03_HS = 0, CLKout02_03_DIV = 0x258
    ( 2,0x0000A258), # CLKout04_05_PD = 0, , CLKout05_ADLY_SEL = 0, CLKout04_ADLY_SEL = 0, CLKout04_05_DDLY = 0x005, , CLKout04_05_HS = 0, CLKout04_05_DIV = 0x258
    ( 3,0x0000A018), # CLKout06_07_PD = 0, CLKout06_07_OSCin_Sel = 0, CLKout07_ADLY_SEL = 0, CLKout06_ADLY_SEL = 0, CLKout06_07_DDLY = 0x005, , CLKout06_07_HS = 0, CLKout06_07_DIV = 0x018
    ( 4,0x0000A060), # CLKout08_09_PD = 0, CLKout08_09_OSCin_Sel = 0, CLKout09_ADLY_SEL = 0, CLKout08_ADLY_SEL = 0, CLKout08_09_DDLY = 0x005, , CLKout08_09_HS = 0, CLKout08_09_DIV = 0x060
    ( 5,0x0000A258), # CLKout10_11_PD = 0, , CLKout11_ADLY_SEL = 0, CLKout10_ADLY_SEL = 0, CLKout10_11_DDLY = 0x005, , CLKout10_11_HS = 0, CLKout10_11_DIV = 0x258
    ( 6,0x02222000), # CLKout03_TYPE = 0x04, CLKout02_TYPE = 0x04, CLKout01_TYPE = 0x04, CLKout00_TYPE = 0x04, CLKout02_03_ADLY = 0x00, 0, CLKout00_01_ADLY = 0x00
    ( 7,0x02222000), # CLKout07_TYPE = 0x04, CLKout06_TYPE = 0x04, CLKout05_TYPE = 0x04, CLKout04_TYPE = 0x04, CLKout06_07_ADLY = 0x00, 0, CLKout04_05_ADLY = 0x00
    ( 8,0x02222000), # CLKout11_TYPE = 0x04, CLKout10_TYPE = 0x04, CLKout09_TYPE = 0x04, CLKout08_TYPE = 0x04, CLKout10_11_ADLY = 0x00, 0, CLKout08_09_ADLY = 0x00
    ( 9,0x02AAAAAA), # 010_1010_1010_1010_1010_1010_1010 - Fixed bit values specified by the data sheet. - Must be written
    (10,0x04A6124C), # OSCout1_LVPECL_AMP = 0x3, 0, 1, OSCout0_TYPE = 0x01, EN_OSCout1 = 1, EN_OSCout0 = 1, OSCout1_MUX = 0, OSCout0_MUX = 0, PD_OSCin = 0, OSCout_DIV = 0x2, 0, 1, 0 VCO_MUX = 0, EN_FEEDBACK_MUX = 1, VCO_DIV = 0x0, FEEDBACK_MUX = 0x4
    (11,0x00A00580), # MODE = 0x02, EN_SYNC = 1, NO_SYNC_CLKx = 0, SYNC_MUX = 0x0, SYNC_QUAL = 0, SYNC_POL_INV = 0, SYNC_EN_AUTO = 1, SYNC_TYPE = 0x3, 0, 0, 0, 0, 0, 0, EN_PLL2_XTAL = 0x0
    (12,0x00D86003), # LD_MUX = 0x03, LD_TYPE = 0x3, SYNC_PLL2_DLD = 0, SYNCPLL1_DLD = 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, EN_TRACK = 0, HOLDOVER_MODE = 0x1, 1
    (13,0x01D81403), # HOLDOVER_MUX = 0x7, HOLDOVER_TYPE = 0x3, 0, Status_CLKin1_MUX = 0x0, 0, Status_CLKin0_TYPE = 0x2, DISABLE_DLD1_DET = 1, Status_CLKin0_MUX = 0x0, CLKin_Select_MODE = 0x0, CLKin_Sel_INV = 0, 0, EN_CLKin1 = 1, EN_CLKin0 = 1
    (14,0x00100000), # LOS_TIMEOUT = 0x0, 0, EN_LOS = 0, 0, Status_CLKin1_TYPE = 0x4, 0, 0, CLKin1_BUF_TYPE = 0, CLKin0_BUF_TYPE = 0, DAC_HIGH_TRIP = 0x00, 0, 0, DAC_LOW_TRIP = 0x00, EN_VTUNE_RAIL_DET = 0
    (15,0x04000400), # MAN_DAC = 0x200, 0, EN_MAN_DAC = 0, HOLDOVER_DLD_CNT = 0x0200, FORCE_HOLDOVER = 0
    (16,0x060AA820), # XTAL_LVL = 0x3, 0_0000_1010_1010_1000_0010_0000
    (24,0x00000000), # PLL2_CF_LF = 0x0, PLL2_C3_LF = 0x0, 0, PLL2_R4_LF = 0x0, 0, PLL2_R3_LF = 0x0, 0, PLL1_N_DLY = 0x0, 0, PLL1_R_DLY = 0x0, PLL1_WND_SIZE = 0x0, 0
    (25,0x00164E20), # DAC_CLK_DIV = 0x00B, 0, 0, PLL1_DLD_CNT = 0x4E20
    (26,0x047D4000), # PLL2_WND_SIZE = 0x2, EN_PLL2_REF_2X = 0, PLL2_CP_POL = 0, PLL2_CP_GAIN = 0x3, 1, 1, 1, 0, 1, 0, PLL2_DLD_CNT = 0x2000, PLL2_CP_TRI = 0
    (27,0x00800002), # 0, 0, 0, PLL1_CP_POL = 1, PLL1_CP_GAIN = 0x0, 0, 0, CLKin1_PreR_DIV = 0x0, CLKin0_PreR_DIV = 0x0, PLL1_R = 31, PLL1_CP_TRI = 0
    (28,0x00008002), # PLL2_R = 0x0001, PLL1_N = 31, 0
    (29,0x000C000C), # 0, 0, 0, 0, 0, OSCin_FREQ = 0x1, PLL2_FAST_PDF = 1, PLL2_N_CAL = 0x0000C
    (30,0x0010000C), # 0, 0, 0, 0, 0, PLL2_P = 0x2, 0, PLL2_N = 0x0000C
    (31,0x0000F800)  # 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, READBACK_LE = 0, READBACK_ADDR = 0x1F, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, uWire_LOCK = 0
]

def tiuCalBaseClocksInit(force=False):
    if not force and hil.tiuCalBaseLmk04808Read(0) == tiuRegArray[0][1]:
        return # Already programmed
    hil.tiuCalBaseLmk04808Write(0,1<<12) # RESET
    time.sleep(.1)
    for reg,data in tiuRegArray:
        hil.tiuCalBaseLmk04808Write(reg,data)

def hexdump(data,filename=None):
    '''Dump the hexadecimal data of the byte string 'data' to the terminal or a file.
    '''
    if filename is None:
        file = sys.stdout
    else:
        file = open(filename,'w',encoding='ascii')
    try:
        for i in range(0,len(data),16):
            values = data[i:i + 16]
            dstr = ' '.join('{:02x}'.format(n) for n in values)
            astr = ''.join(chr(c) if c < 128 and chr(c).isprintable() else '.' for c in values)
            print('{:04x}: {:48} {}'.format(i,dstr,astr),file=file)
    finally:
        if filename is not None:
            file.close()

# This statement is required to support using functions in the multiprocessing module.
# Code that executes when running this script directly is protected by this statement
# so it won't run when this script is imported directly, such as by multiprocessing.
if __name__ == '__main__':
    pass
