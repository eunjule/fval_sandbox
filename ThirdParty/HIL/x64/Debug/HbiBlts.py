# Copyright 2018 Intel Corporation. All rights reserved.
#
# This software is supplied under the terms of an executed license agreement with Intel Corporation and
# may not be copied, transferred or used except in accordance with that agreement. All disclosures to third
# parties are expressly limited and defined by an executed non-disclosure agreement with Intel Corporation.

import ctypes
from ctypes import wintypes as w
import hil
import time
import csv
import sys

if hil.hilVersion() < 0x04321700: # TODO: 0x05000000 after release.
    print('Requires HIL 4.50.23 or later')
    sys.exit(-1)

start_time = time.time()

def validate(result,func,args):
    if not result:
        err = ctypes.get_last_error()
        raise RuntimeError('{}() failed({})'.format(func.__name__,err))

blts = []

def displayBlt(blt,slot='NA'):
    vid,did = divmod(blt.Id,0x1000000)
    devname = blt.DeviceName.split('\x00')[0]
    huid = '{}_{}'.format(blt.VendorName,did)
    args = [slot,devname,huid,blt.PartNumberAsBuilt,blt.PartNumberCurrent,
            blt.SerialNumber,blt.ManufactureDate]
    print(' {:2}   {:20} {:14}  {:19}  {:18}  {:17}  {}'.format(*args))
    writer.writerow(args)

def slots(connect,numslots):
    for slot in range(numslots):
        try:
            connect(slot)
        except RuntimeError:
            pass
        else:
            yield slot

# hbiDpsConnect
# hbiMbConnect
# hbiPsConnect
# hbiPsdbConnect

def psConnect():
    for slot in slots(hil.hbiPsConnect,3):
        blt_of_slot(f'HBI BPS{slot}',hil.hbiPsBltBoardRead,slot)

def dpsConnect():
    for slot in slots(hil.hbiDpsConnect,16):
        blt_of_slot('HBI DPS',hil.hbiDpsBltBoardRead,slot)

def psdbConnect():
    for slot in slots(hil.hbiPsdbConnect,2):
        blt_of_slot('HBI PSDB',hil.hbiPsdbBltBoardRead,slot)

def tiuConnect():
    for slot in range(8):
        blt_of_slot('HBI TIU',hil.hbiTiuBltBoardRead,slot,0)

def blt_of(desc,func,*args):
    try:
        blt = func(*args)
    except RuntimeError:
        print (desc,'BLT not available')
    else:
        displayBlt(blt)

def blt_of_slot(desc,func,slot,*args):
    try:
        #raise RuntimeError # for testing
        blt = func(slot,*args)
    except RuntimeError:
        print (' {:2}   {} BLT not available'.format(slot,desc))
    else:
        blts.append((slot,blt))

k32 = ctypes.WinDLL('kernel32',use_last_error=True)
k32.GetComputerNameW.argtypes = w.LPWSTR,w.LPDWORD
k32.GetComputerNameW.restype = w.BOOL
k32.GetComputerNameW.errcheck = validate
buf = ctypes.create_unicode_buffer(50)
dw = w.DWORD(50)
k32.GetComputerNameW(buf,ctypes.byref(dw))
print("\nSystem Name : ",buf.value)

hil.hilComplianceEnable(0)

filename = '{}.blt.csv'.format(buf.value)
with open(filename,'w',encoding='utf-8-sig',newline='') as f:
    writer = csv.writer(f)

    print('Slot  Device Name          HUID            PartNumberAsBuilt    PartNumberCurrent   SerialNumber       MfgDate')
    print('-' * 110)
    writer.writerow('Slot DeviceName HUID PartNumberAsBuilt PartNumberCurrent SerialNumber MfgDate'.split())

    blt_of('HBI Instrument',hil.hbiMbBltInstrumentRead)
    blt_of('HBI Mainboard',hil.hbiMbBltBoardRead)
    blt_of('Site Controller',hil.scBltBoardRead)
    blt_of('HBI Backplane',hil.hbiBpBltBoardRead)

    psConnect()
    psdbConnect()
    dpsConnect()
    tiuConnect()

    for slot,blt in blts: #sorted(blts,key=lambda x: x[0]):
        displayBlt(blt,slot)

    seconds = time.time() - start_time
    print('Completed in {:.1f} seconds.'.format(seconds))
    print('CSV data also written to {}.'.format(filename))
