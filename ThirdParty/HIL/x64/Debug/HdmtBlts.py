# Copyright 2018-2020 Intel Corporation. All rights reserved.
#
# This software is supplied under the terms of an executed license agreement with Intel Corporation and
# may not be copied, transferred or used except in accordance with that agreement. All disclosures to third
# parties are expressly limited and defined by an executed non-disclosure agreement with Intel Corporation.

import ctypes
from ctypes import wintypes as w
import hil
import time
import csv
import sys

if hil.hilVersion() < 0x07010000: # TODO: 0x03000000 after release.
    print('Requires HIL 7.1 or later')
    sys.exit(-1)

start_time = time.time()

def validate(result,func,args):
    if not result:
        err = ctypes.get_last_error()
        raise RuntimeError('{}() failed({})'.format(func.__name__,err))

def is_on():
    state = hil.bpTiuAuxPowerState()
    return state == [1,1]

def on():
    if not is_on():
        hil.bpTiuAuxPowerEnable(1)

def off():
    if is_on():
        hil.bpTiuAuxPowerEnable(0)

def toggle_power():
    if is_on():
        print('Turning off...')
        off()
    else:
        print('Turning on...')
        on()

blts = []

def displayBlt(blt,slot='NA'):
    vid,did = divmod(blt.Id,0x1000000)
    devname = blt.DeviceName.split('\x00')[0]
    huid = '{}_{}'.format(blt.VendorName,did)
    args = [slot,devname,huid,blt.PartNumberAsBuilt,blt.PartNumberCurrent,
            blt.SerialNumber,blt.ManufactureDate]
    print(' {:2}   {:20} {:14}  {:19}  {:18}  {:17}  {}'.format(*args))
    writer.writerow(args)

def slots(connect):
    for slot in range(12):
        try:
            connect(slot)
        except RuntimeError:
            pass
        else:
            yield slot

def hpccConnect():
    for slot in slots(hil.hpccDcConnect):
        blt_of_slot('HPCC Instrument',hil.hpccBltInstrumentRead,slot)
        blt_of_slot('HPCC Mainboard',hil.hpccDcBltBoardRead,slot)
    for slot in slots(hil.hpccAcConnect):
        blt_of_slot('HPCC Daughterboard',hil.hpccAcBltBoardRead,slot)

def calhpccConnect():
    for slot in range(12):
        try:
            hil.calHpccConnect(slot)
        except RuntimeError:
            pass
        else:
            blt_of_slot('HPCC Cal',hil.calHpccBltBoardRead,slot)

def calhddpsConnect():
    for slot in slots(hil.calHddpsConnect):
        blt_of_slot('HDDPS Cal',hil.calHddpsBltBoardRead,slot)

def hddpsConnect():
    for slot in slots(lambda s: hil.hddpsConnect(s,0)):
        blt_of_slot('HDDPS Instrument',hil.dpsBltInstrumentRead,slot)
        blt_of_slot('HDDPS Mainboard',hil.dpsBltBoardRead,slot,0)
    for slot in slots(lambda s: hil.hddpsConnect(s,1)):
        blt_of_slot('HDDPS Daughterboard',hil.dpsBltBoardRead,slot,1)

def calhvdpsConnect():
    for slot in slots(hil.calHvdpsConnect):
        blt_of_slot('HVDPS Cal',hil.calHvdpsBltBoardRead,slot)

def hvdpsConnect():
    for slot in slots(hil.hvdpsConnect):
        blt_of_slot('HVDPS Instrument',hil.dpsBltInstrumentRead,slot)
        blt_of_slot('HVDPS Mainboard',hil.dpsBltBoardRead,slot,0)
    for slot in slots(hil.hvilConnect):
        blt_of_slot('HVDPS Daughterboard',hil.dpsBltBoardRead,slot,1)

def tdbConnect():
    for slot in slots(hil.tdbConnect):
        blt_of_slot('TDAUBANK',hil.tdbBltBoardRead,slot)

def tddConnect():
    for slot in slots(hil.tddConnect):
        blt_of_slot('TDAUDiag',hil.tddBltBoardRead,slot)

def blt_of(desc,func,*args):
    try:
        blt = func(*args)
    except RuntimeError:
        print (desc,'BLT not available')
    else:
        displayBlt(blt)

def blt_of_slot(desc,func,slot,*args):
    try:
        #raise RuntimeError # for testing
        blt = func(slot,*args)
    except RuntimeError:
        print (' {:2}   {} BLT not available'.format(slot,desc))
    else:
        blts.append((slot,blt))

k32 = ctypes.WinDLL('kernel32',use_last_error=True)
k32.GetComputerNameW.argtypes = w.LPWSTR,w.LPDWORD
k32.GetComputerNameW.restype = w.BOOL
k32.GetComputerNameW.errcheck = validate
buf = ctypes.create_unicode_buffer(50)
dw = w.DWORD(50)
k32.GetComputerNameW(buf,ctypes.byref(dw))
print("\nSystem Name : ",buf.value)

# BP Sys ID
on()
hil.hilComplianceEnable(0)

filename = '{}.blt.csv'.format(buf.value)
with open(filename,'w',encoding='utf-8-sig',newline='') as f:
    writer = csv.writer(f)

    print('Slot  Device Name          HUID            PartNumberAsBuilt    PartNumberCurrent   SerialNumber       MfgDate')
    print('-' * 110)
    writer.writerow('Slot DeviceName HUID PartNumberAsBuilt PartNumberCurrent SerialNumber MfgDate'.split())

    blt_of('TIU',hil.tiuBltBoardRead,0)
    blt_of('Front Panel',hil.fpBltBoardRead)
    blt_of('Backplane',hil.bpBltBoardRead)
    blt_of('Site Controller',hil.scBltBoardRead)
    try:
        blt = hil.rc3BltBoardRead()
    except RuntimeError:
        blt_of('Resource Card',hil.rcBltBoardRead)
        blt_of('Thermal Card',hil.tcBltBoardRead)
    else:
        displayBlt(blt)
    blt_of('Primary PSE',hil.psExtBltBoardRead,0)
    blt_of('Primary PS',hil.psBltBoardRead,0)
    blt_of('Secondary PSE',hil.psExtBltBoardRead,1)
    blt_of('Secondary PS',hil.psBltBoardRead,1)

    hpccConnect()
    hddpsConnect()
    hvdpsConnect()
    tdbConnect()
    tddConnect()
    calhpccConnect()
    calhddpsConnect()
    calhvdpsConnect()

    for slot,blt in sorted(blts,key=lambda x: x[0]):
        displayBlt(blt,slot)

    seconds = time.time() - start_time
    print('Completed in {:.1f} seconds.'.format(seconds))
    print('CSV data also written to {}.'.format(filename))
