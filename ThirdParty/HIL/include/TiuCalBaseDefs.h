// INTEL CONFIDENTIAL
// Copyright 2014-2018 Intel Corporation All Rights Reserved.
//
// The source code contained or described herein and all documents related to
// the source code ("Material") are owned by Intel Corporation or its
// suppliers or licensors. Title to the Material remains with Intel
// Corporation or its suppliers and licensors. The Material contains trade
// secrets and proprietary and confidential information of Intel or its
// suppliers and licensors. The Material is protected by worldwide copyright
// and trade secret laws and treaty provisions. No part of the Material may be
// used, copied, reproduced, modified, published, uploaded, posted,
// transmitted, distributed, or disclosed in any way without Intel's prior
// express written permission.
//
// No license under any patent, copyright, trade secret or other intellectual
// property right is granted to or conferred upon you by disclosure or
// delivery of the Materials, either expressly, by implication, inducement,
// estoppel or otherwise. Any license under such intellectual property rights
// must be express and approved by Intel in writing.

//! @file
//! @brief Non-function definitions such as constants and structures used by the HDMT calibration base TIU APIs.

#pragma once

//! @brief Enumeration of the possible calibration base TUI GPIOs that can be passed to functions that
//!        have a ::TIU_CAL_BASE_GPIO argument.  These names coincide with the IO Expander net
//!        names on the calibration base TIU schematic.
typedef enum TIU_CAL_BASE_GPIO
{
    TCBG_U7_IO0_0,                   // I/O Expander U7 - page 45
    TCBG_SCOPE_FQCNTR_SEL,
    TCBG_SCOPE_LB_SEL,
    TCBG_I2C_GPIO0,
    TCBG_I2C_GPIO1,
    TCBG_I2C_GPIO2,
    TCBG_SEL_LEVEL6,
    TCBG_SEL_LEVEL7,
    TCBG_SEL_LEVEL8,
    TCBG_SEL_LEVEL9,
    TCBG_U7_IO1_2,
    TCBG_AUXPWR_3P3V_SEL,
    TCBG_AUXPWR_12V_SEL,
    TCBG_AUXPWR_5V_SEL,
    TCBG_SUPPLY_SELECT_SENSE_SEL,
    TCBG_TP_U7_21,
    TCBG_TDR_PD_SCOPE_SEL,
    TCBG_CH_DATA_CLK_SENSE_SEL,
    TCBG_CH_DATA_CLK_FORCE_SEL,
    TCBG_SEL_P1CLK_LEV,
    TCBG_PMU_FOR_SEN_LOOP_SEL,
    TCBG_SYS_PMU_FORCE_SEL,
    TCBG_SYS_PMU_SEN_REF_SEL,
    TCBG_U7_IO2_7,
    TCBG_SYS_PMU0_FORCE_SEL,
    TCBG_SYS_PMU1_FORCE_SEL,
    TCBG_SYS_PMU2_FORCE_SEL,
    TCBG_SYS_PMU3_FORCE_SEL,
    TCBG_SYS_PMU4_FORCE_SEL,
    TCBG_SYS_PMU5_FORCE_SEL,
    TCBG_SYS_PMU6_FORCE_SEL,
    TCBG_SYS_PMU7_FORCE_SEL,
    TCBG_SYS_PMU0_SENREF_SEL,
    TCBG_SYS_PMU1_SENREF_SEL,
    TCBG_SYS_PMU2_SENREF_SEL,
    TCBG_SYS_PMU3_SENREF_SEL,
    TCBG_SYS_PMU4_SENREF_SEL,
    TCBG_SYS_PMU5_SENREF_SEL,
    TCBG_SYS_PMU6_SENREF_SEL,
    TCBG_SYS_PMU7_SENREF_SEL,

    TCBG_U101_IO0_0,                 // I/O Expander U101 - page 47
    TCBG_CALRES_SENSE14_P_SEL,
    TCBG_CALRES_TREE_SEN_PN_SEL,
    TCBG_CALRES_TREE_FORCE_SEL,
    TCBG_ADC_INPUT_PN_SEL,
    TCBG_TP_U101_9,
    TCBG_PHASE_VO_SEL,
    TCBG_CAL_RES_LEV1_SEL,
    TCBG_CAL_RES_LEV2_SEL,
    TCBG_CAL_RES_LEV3_SEL,
    TCBG_CAL_RES_LEV4_SEL,
    TCBG_CALRES_SENSE1_P_SEL,
    TCBG_CALRES_SENSE2_P_SEL,
    TCBG_CALRES_SENSE3_P_SEL,
    TCBG_CALRES_SENSE4_P_SEL,
    TCBG_CALRES_SENSE5_P_SEL,
    TCBG_CALRES_SENSE6_P_SEL,
    TCBG_CALRES_SENSE7_P_SEL,
    TCBG_CALRES_SENSE8_P_SEL,
    TCBG_CALRES_SENSE9_P_SEL,
    TCBG_CALRES_SENSE10_P_SEL,
    TCBG_CALRES_SENSE11_P_SEL,
    TCBG_CALRES_SENSE12_P_SEL,
    TCBG_CALRES_SENSE13_P_SEL,
    TCBG_TIU_5V_RC_FPGA_SPARE_1,
    TCBG_TIU_5V_RC_FPGA_SPARE_2,
    TCBG_TIU_5V_RC_FPGA_SPARE_3,
    TCBG_TIU_5V_RC_FPGA_SPARE_4,
    TCBG_TIU_5V_RC_FPGA_SPARE_5,
    TCBG_TIU_5V_RC_FPGA_SPARE_6,
    TCBG_TP_5V_I2C_IO07,
    TCBG_TP_5V_I2C_IO08,
    TCBG_PECI_SEL0,
    TCBG_PECI_SEL1,
    TCBG_PECI_SEL2,
    TCBG_TP_5V_I2C_IO012,
    TCBG_TP_5V_I2C_IO013,
    TCBG_TP_5V_I2C_IO014,
    TCBG_TP_5V_I2C_IO015,
    TCBG_TP_5V_I2C_IO016,

    TCBG_GPIO_LEV1_SEL,              // I/O Expander U77 - page 46
    TCBG_GPIO_LEV2_SEL,
    TCBG_GPIO_LEV3_SEL,
    TCBG_GPIO_LEV4_SEL,
    TCBG_GPIO_SEL,
    TCBG_IO_RLY0_19_SEL,
    TCBG_SENSE_SMA_SEL,
    TCBG_FORCE_SMA_SEL,
    TCBG_U77_IO1_0,
    TCBG_FORCE_POST_SEL,
    TCBG_SENSE_POST_SEL,
    TCBG_CARD_FORCE_SEL,
    TCBG_CARD_SENSE_SEL,
    TCBG_U77_IO1_5,
    TCBG_EPA_CAL_MEAS_SEL,
    TCBG_EPA_UP_DOWN_SEL,
    TCBG_SPARE_SEL_1,
    TCBG_SPARE_SEL_2,
    TCBG_SPARE_SEL_3,
    TCBG_SPARE_SEL_4,
    TCBG_SPARE_SEL_5,
    TCBG_SPARE_SEL_6,
    TCBG_SPARE_SEL_7,
    TCBG_SPARE_SEL_8,
    TCBG_LMK_GOE,
    TCBG_LMK_SYNCN,
    TCBG_DAC1_CLRN,
    TCBG_DAC2_CLRN,
    TCBG_U77_41,
    TCBG_U77_42,
    TCBG_U77_43,
    TCBG_U77_44,
    TCBG_SPARE_SEL_9,
    TCBG_SPARE_SEL_10,
    TCBG_SPARE_SEL_11,
    TCBG_SPARE_SEL_12,
    TCBG_SPARE_SEL_13,
    TCBG_SPARE_SEL_14,
    TCBG_SPARE_SEL_15,
    TCBG_SPARE_SEL_16,
    TCBG_LAST_INVALID
} TIU_CAL_BASE_GPIO;
