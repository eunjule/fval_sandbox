// INTEL CONFIDENTIAL
// Copyright 2016-2018 Intel Corporation All Rights Reserved.
//
// The source code contained or described herein and all documents related to
// the source code ("Material") are owned by Intel Corporation or its
// suppliers or licensors. Title to the Material remains with Intel
// Corporation or its suppliers and licensors. The Material contains trade
// secrets and proprietary and confidential information of Intel or its
// suppliers and licensors. The Material is protected by worldwide copyright
// and trade secret laws and treaty provisions. No part of the Material may be
// used, copied, reproduced, modified, published, uploaded, posted,
// transmitted, distributed, or disclosed in any way without Intel's prior
// express written permission.
//
// No license under any patent, copyright, trade secret or other intellectual
// property right is granted to or conferred upon you by disclosure or
// delivery of the Materials, either expressly, by implication, inducement,
// estoppel or otherwise. Any license under such intellectual property rights
// must be express and approved by Intel in writing.

//! @file
//! @brief Non-function definitions such as constants and structures used by the HDMT power supply APIs.
#pragma once

//! @brief Power supply data packet field definitions for a Murata power supply.
typedef struct PS_D1U_INFO
{
    BYTE status;        //!< Status byte as documented in the Murata D1U Communications Protocol
    BYTE fault;         //!< Fault byte as documented in the Murata D1U Communications Protocol
    double mainVoltage; //!< Main Output Voltage byte (converted through transfer equation) as documented in the Murata D1U Communications Protocol
    double mainCurrent; //!< Main Output Current byte (converted through transfer equation) as documented in the Murata D1U Communications Protocol
    double fan1Current; //!< Fan1 Current byte (converted through transfer equation) as documented in the Murata D1U Communications Protocol
    double fan2Current; //!< Fan2 Current byte (converted through transfer equation) as documented in the Murata D1U Communications Protocol
} PS_D1U_INFO;

typedef PS_D1U_INFO* PPS_D1U_INFO; //!< Pointer type for #PS_D1U_INFO structure.
