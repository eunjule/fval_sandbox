// INTEL CONFIDENTIAL
// Copyright 2014-2018 Intel Corporation All Rights Reserved.
//
// The source code contained or described herein and all documents related to
// the source code ("Material") are owned by Intel Corporation or its
// suppliers or licensors. Title to the Material remains with Intel
// Corporation or its suppliers and licensors. The Material contains trade
// secrets and proprietary and confidential information of Intel or its
// suppliers and licensors. The Material is protected by worldwide copyright
// and trade secret laws and treaty provisions. No part of the Material may be
// used, copied, reproduced, modified, published, uploaded, posted,
// transmitted, distributed, or disclosed in any way without Intel's prior
// express written permission.
//
// No license under any patent, copyright, trade secret or other intellectual
// property right is granted to or conferred upon you by disclosure or
// delivery of the Materials, either expressly, by implication, inducement,
// estoppel or otherwise. Any license under such intellectual property rights
// must be express and approved by Intel in writing.

//! @file
//! @brief Non-function definitions such as constants and structures used by the HDMT PCI driver APIs.

#pragma once

//! @brief PCI simulation functions.
//!
//! This structure is used by pciSimOverride() to return the current PCI simulation functions.
//! When used as an input, it sets the current simulation functions.  If the function is NULL on input, the default function is restored.
//! @see hilSimEnable()
//! @see hilSimIsEnabled()
typedef struct PCISIM
{
    //! @brief Default returns HS_SUCCESS and *pHandle is a fake handle based on the \c viddid, \c slot, and \c additional parameters.
    HIL_STATUS(*DeviceOpen)(_In_z_ LPCSTR interfaceGuid, _In_ DWORD viddid, _In_ INT slot, _In_opt_z_ LPCWSTR additional, _Out_ PHANDLE pHandle);
    //! @brief Default returns HS_SUCCESS.
    HIL_STATUS(*DeviceClose)(_In_ HANDLE device);
    //! @brief Default returns HS_SUCCESS.  There is 2MB of simulation memory.  If \c offset + \c length exceeds 2MB return HS_SUCCESS, but \c pData is not modified.
    //! Memory is initialized to zero and otherwise returns the last data written by pciDmaWrite() in simulation mode.
    HIL_STATUS(*DmaRead)(_In_ HANDLE device, _In_ INT64 offset, _Out_writes_bytes_all_(length) LPVOID pData, _In_ DWORD length);
    //! @brief Default returns HS_SUCCESS.  There is 2MB of simulation memory.  If \c offset + \c length exceeds 2MB return HS_SUCCESS, but simulation memory is not modified.
    HIL_STATUS(*DmaWrite)(_In_ HANDLE device, _In_ INT64 offset, _In_reads_bytes_(length) const void* pData, _In_ DWORD length);
    //! @brief Default returns HS_SUCCESS if \c bar and \c offset are valid, else HS_UNSUCCESSFUL.
    //! Memory for three 256KB BARs is supported (0-2).  \c offset must be DWORD-aligned and within 256KB.
    //! Memory is initialized to zero and returns the last value written by pciBarWrite() in simulation mode.
    HIL_STATUS(*BarRead)(_In_ HANDLE device, _In_ UINT bar, _In_ DWORD offset, _Out_ LPDWORD pData);
    //! @brief Default returns HS_SUCCESS if \c bar and \c offset are valid, else HS_UNSUCCESSFUL.
    //! Three 256KB BARs are supported (0-2).  \c offset must be DWORD-aligned and within 256KB.
    //! For legacy reasons, the default also always writes 0 for Bar 0 offset 0x8010 no matter the \c data value.
    HIL_STATUS(*BarWrite)(_In_ HANDLE device, _In_ UINT bar, _In_ DWORD offset, _In_ DWORD data);
} PCISIM;

#define PCI_GUID_RC       "C89BD126-FA2F-4734-9BCC-41CADBA45FA2" //!< Resource card device driver interface GUID.
#define PCI_GUID_HDDPS    "5D107D9B-BEE4-447B-B40F-C44AE42F752C" //!< Gen 2 DUT power supply driver interface GUID.
#define PCI_GUID_TDAUBANK "F6C93906-A67C-48EA-8320-E4A80FE77989" //!< TDAU Bank device driver interface GUID.
#define PCI_GUID_HPCC_AC  "402AF676-00F1-4430-8C65-FE6B7D5FA75F" //!< Gen2 channel card AC device driver interface GUID.
#define PCI_GUID_HPCC_DC  "9754D09B-95EA-48CA-B1C1-C5F9C3CD763F" //!< Gen2 channel card DC device driver interface GUID.
#define PCI_GUID_RCTC3    "A5913586-AB38-4A94-8BA0-AE838DE182E5" //!< Gen3 Resource card device driver interface GUID.

#define PCI_GUID_HBI_PATGEN "2BD22ED2-AAF4-405E-A404-0505111EB1C4" //!< HBI Mainboard Pattern Generator FPGA interface GUID.
#define PCI_GUID_HBI_RCTC   "A76F9F67-6552-415A-B572-7D7E923B480D" //!< HBI Mainboard Resource/Thermal FPGA interface GUID.
#define PCI_GUID_HBI_RING   "4CADCCDB-DEAB-4FEB-9C18-F61C72C2A78A" //!< HBI Ring Multiplier FPGA interface GUID.
#define PCI_GUID_HBI_PIN    "11DDEA72-7F3C-4AD2-9700-751BFFF094CD" //!< HBI Pin Multiplier FPGA interface GUID.
#define PCI_GUID_HBI_DPS    "3193C5B8-CB21-47D2-9E00-EA707314F45F" //!< HBI DUT Power Supply FPGA interface GUID.

#define PCI_VIDDID_HPCC_AC  0x808600ACu //!< Gen 2 channel card AC (daughterboard) vendor/device ID.
#define PCI_VIDDID_HPCC_DC  0x808600DCu //!< Gen 2 channel card DC (mainboard) vendor/device ID.
#define PCI_VIDDID_HDDPS_LC 0x10EE0009u //!< Gen 2 low current digital power supply vendor/device ID.
#define PCI_VIDDID_HDDPS_HC 0x10EE000Au //!< Gen 2 high current digital power supply vendor/device ID.
#define PCI_VIDDID_RC2      0x8086096Du //!< Gen 2 resource card vendor/device ID.
#define PCI_VIDDID_RCTC3    0x80860D92u //!< Gen 3 resource card vendor/device ID.
#define PCI_VIDDID_TDAUBANK 0x8086096Bu //!< TDAU bank vendor/device ID.
#define PCI_VIDDID_HVDPS    0x80860B29u //!< Gen 2 HVDPS card vendor/device ID.
#define PCI_VIDDID_HVIL     0x80860B2Au //!< Gen 2 HVIL card vendor/device ID.

#define PCI_VIDDID_HBI_RC     0x8086B100 //!< HBI Mainboard Resource FPGA vendor/device ID.
#define PCI_VIDDID_HBI_PATGEN 0x8086B101 //!< HBI Mainboard Pattern Generator FPGA vendor/device ID.
#define PCI_VIDDID_HBI_RING   0x8086B102 //!< HBI Mainboard Ring Multiplier FPGA vendor/device ID.
#define PCI_VIDDID_HBI_PIN    0x8086B103 //!< HBI Power/Signal Distribution Board Pin Multiplier FPGA vendor/device ID.
#define PCI_VIDDID_HBI_DPS    0x8086B104 //!< HBI DUT Power Supply FPGA vendor/device ID.
