// INTEL CONFIDENTIAL
// Copyright 2016-2018 Intel Corporation All Rights Reserved.
//
// The source code contained or described herein and all documents related to
// the source code ("Material") are owned by Intel Corporation or its
// suppliers or licensors. Title to the Material remains with Intel
// Corporation or its suppliers and licensors. The Material contains trade
// secrets and proprietary and confidential information of Intel or its
// suppliers and licensors. The Material is protected by worldwide copyright
// and trade secret laws and treaty provisions. No part of the Material may be
// used, copied, reproduced, modified, published, uploaded, posted,
// transmitted, distributed, or disclosed in any way without Intel's prior
// express written permission.
//
// No license under any patent, copyright, trade secret or other intellectual
// property right is granted to or conferred upon you by disclosure or
// delivery of the Materials, either expressly, by implication, inducement,
// estoppel or otherwise. Any license under such intellectual property rights
// must be express and approved by Intel in writing.

//! @file
//! @brief APIs to control the HDMT Backplane.
#pragma once
#include "HilDefs.h"
#include "PowersupplyDefs.h"

#ifdef __cplusplus
extern "C" {
#endif

//! @brief Verifies a power supply is present.
//!
//! This function verifies an HDMT bulk power supply is present.  It connects to and caches driver resources for use
//! by other \c psXXXXX functions.  psDisconnect() is its complementary function and frees any cached resources.
//!
//! Neither psConnect() or psDisconnect() are required to be called to use the other \c psXXXXX functions.  All
//! \c psXXXXX functions will allocate resources if they are not already allocated and free them if required to
//! perform their function.
//! @param[in] psSlot The index of a power supply according to the table below:
//! | psSlot | Comment                     |
//! | :----: | :-------------------------- |
//! |   0    | Left internal power supply  |
//! |   1    | Right internal power supply |
//! |   2    | Left external power supply  |
//! |   3    | Right external power supply |
//!
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS psConnect(_In_ INT psSlot);

//! @brief Frees any resources cached from using the power supply functions.
//!
//! This function frees any resources associated with using the power supply functions. psConnect() is its
//! complementary function.
//!
//! Neither psConnect() or psDisconnect() are required to be called to use the other \c psXXXXX functions.  All
//! \c psXXXXX functions will allocate resources if they are not already allocated and free them if required to
//! perform their function.
//! @param[in] psSlot The index of a power supply according to the table below:
//! | psSlot | Comment                     |
//! | :----: | :-------------------------- |
//! |   0    | Left internal power supply  |
//! |   1    | Right internal power supply |
//! |   2    | Left external power supply  |
//! |   3    | Right external power supply |
//!
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS psDisconnect(_In_ INT psSlot);

//! @brief Reads information provided by the D1U power supply.
//!
//! This function reads information provided by the Murata D1U power supply.
//! This function also calls rcRootI2cLock() and rcRootI2cUnlock() during its execution since it accesses
//! devices on the root I2C bus.  If additional I2C commands need to be atomically executed on the root I2C
//! bus, then manually call rcRootI2cLock() and rcRootI2cUnlock() around the entire root I2C bus access.
//!
//! @note While the interface providing this information is present in a Delta power supply, it is not compatible
//!       with this function.  A Delta supply will return #HS_UNSUPPORTED.  Use psPmbusRead() and send PMBUS commands
//!       to get equivalent information.
//!
//! \par C Example
//! \code{.c}
//! #include <windows.h>
//! #include <stdio.h>
//! #include "HilApi.h"
//! #include "PowersupplyApi.h"
//!
//! int main()
//! {
//!     PS_D1U_INFO info;
//!     HIL_STATUS result = psD1uRead(0, &info);
//!     if(result != HS_SUCCESS)
//!         fprintf(stderr,"%s",hilStatusDescription(result));
//!     else
//!         printf("Voltage = %.1f\n", info.mainVoltage);
//! }
//! \endcode
//! \par Python Example
//! \code{.py}
//! import hil
//! info = hil.psD1uRead(0)
//! print('Voltage = {:.1f}'.format(info.mainVoltage))
//! # Note Python returns output parameters and raises RuntimeError on failure.
//! \endcode
//!
//! @param[in] psSlot The index of a power supply according to the table below:
//! | psSlot | Comment                     |
//! | :----: | :-------------------------- |
//! |   0    | Left internal power supply  |
//! |   1    | Right internal power supply |
//! |   2    | Left external power supply  |
//! |   3    | Right external power supply |
//!
//! @param[out] pInfo The address of a #PS_D1U_INFO structure to be filled out with the power supply information.
//! @returns \ref HIL_STATUS
//! @see
//!  \ref pyapi\n
HIL_API HIL_STATUS psD1uRead(_In_ INT psSlot, _Out_ PPS_D1U_INFO pInfo);

//! @brief Issues a write command to the PMBUS interface on a Delta power supply.
//!
//! This function issues a write command to the PMBUS interface on a Delta power supply.
//! This function also calls rcRootI2cLock() and rcRootI2cUnlock() during its execution since it accesses
//! devices on the root I2C bus.  If additional I2C commands need to be atomically executed on the root I2C
//! bus, then manually call rcRootI2cLock() and rcRootI2cUnlock() around the entire root I2C bus access.
//!
//! @note The PMBUS I2C interface is not present in a Murata power supply and this function will return #HS_I2C_NOACK.
//!       Use psBltBoardRead() to determine the power supply vendor.
//!
//! @param[in] psSlot The index of a power supply according to the table below:
//! | psSlot | Comment                     |
//! | :----: | :-------------------------- |
//! |   0    | Left internal power supply  |
//! |   1    | Right internal power supply |
//! |   2    | Left external power supply  |
//! |   3    | Right external power supply |
//!
//! @param[in] command   PMBUS command to write.  See the PMBUS Specification Part II online for commands and additional data requirements.
//! @param[in] pCmdData  A data buffer of additional bytes to write for the associated \c command.
//!                      Note that the bytes are sent to the device sequentially starting with the first
//!                      byte pointed to by \c pCmdData.  Also note that the I2C address and the PEC (Packet
//!                      Error Checking) bytes should not be included in \c pCmdData because this is handled
//!                      automatically within the API.  Lastly, the command byte should not be included in this
//!                      buffer as it is already supplied in \c command.  Note that this parameter may be
//!                      NULL for commands that have no data associated with them.
//! @param[in] cmdLength The byte length of the \c pCmdData buffer.  Note that this parameter may be 0 for commands
//!                      that have no data.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS psPmbusWrite(_In_ INT psSlot, _In_ BYTE command, _In_reads_bytes_opt_(cmdLength) const void* pCmdData, _In_ DWORD cmdLength);

//! @brief Issues a read command to the PMBUS interface on a Delta power supply.
//!
//! This function issues a read command to the PMBUS interface on a Delta power supply.
//! This function also calls rcRootI2cLock() and rcRootI2cUnlock() during its execution since it accesses
//! devices on the root I2C bus.  If additional I2C commands need to be atomically executed on the root I2C
//! bus, then manually call rcRootI2cLock() and rcRootI2cUnlock() around the entire root I2C bus access.
//!
//! @note The PMBUS is not present in a Murata power supply and this function will return #HS_I2C_NOACK.
//!       Use psBltBoardRead() to determine the power supply vendor.
//!
//! @param[in] psSlot The index of a power supply according to the table below:
//! | psSlot | Comment                     |
//! | :----: | :-------------------------- |
//! |   0    | Left internal power supply  |
//! |   1    | Right internal power supply |
//! |   2    | Left external power supply  |
//! |   3    | Right external power supply |
//!
//! @param[in] command    PMBUS command to read.
//! @param[out] pReadData A data buffer of bytes to hold the data read from the associated \c command.
//!                       Note that the bytes received from the device are located sequentially
//!                       in the buffer in the order they were received with the first byte being located
//!                       directly at \c pReadData.  Also note that the PEC (Packet Error Checking) byte
//!                       is automatically checked within the API and is not included in \c pReadData.
//!                       Because of this, the PEC byte should not be included in the \c readLength value.
//!                       Lastly, this parameter cannot be NULL since all reads return at least one byte.
//! @param[in] readLength The byte length of the \c pReadData buffer.  Note that this parameter cannot be 0
//!                       since all reads return at least one byte.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS psPmbusRead(_In_ INT psSlot, _In_ BYTE command, _Out_writes_bytes_all_(readLength) LPVOID pReadData, _In_ DWORD readLength);

//! @brief Writes a command within the BMR454 (Intermediate Bus Converter) device.
//!
//! This function writes a command to the BMR454 (Intermediate Bus Converter) device.
//! This function also calls rcRootI2cLock() and rcRootI2cUnlock() during its execution since it accesses
//! devices on the root I2C bus.  If additional I2C commands need to be atomically executed on the root I2C
//! bus, then manually call rcRootI2cLock() and rcRootI2cUnlock() around the entire root I2C bus access.
//!
//! @note The power to the TIU must be enabled by calling bpTiuAuxPowerEnable() before using this function.
//!       If bpTiuAuxPowerEnable() is not called, then the BMR454 is not active and does not respond to commands.
//! @note Some BMR454 command writes cause the device to NACK further commands for some amount of time (sometimes as
//!       long as 500ms).
//! @param[in] psSlot The index of a power supply according to the table below:
//! | psSlot | Comment                     |
//! | :----: | :-------------------------- |
//! |   0    | Left internal power supply  |
//! |   1    | Right internal power supply |
//! |   2    | Left external power supply  |
//! |   3    | Right external power supply |
//!
//! @param[in] command   BMR454 command to write.
//! @param[in] pCmdData  A data buffer of bytes to write to the BMR454 device via the I2C bus for the associated
//!                      \c command.  Note that the bytes are sent to the device sequentially starting with the first
//!                      byte pointed to by \c pCmdData.  Also note that the I2C address and the PEC (Packet
//!                      Error Checking) bytes should not be included in \c pCmdData because this is handled
//!                      automatically within the API.  Lastly, the command byte should not be included in this
//!                      buffer as it is already supplied in \c command.  Note that this parameter may be
//!                      NULL for commands that have no data associated with them.
//! @param[in] cmdLength The byte length of the \c pCmdData buffer.  Note that this parameter may be 0 for commands
//!                      that have no data.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS psExtBmr454Write(_In_ INT psSlot, _In_ BYTE command, _In_reads_bytes_opt_(cmdLength) const void* pCmdData, _In_ DWORD cmdLength);

//! @brief Reads a command within the BMR454 (Intermediate Bus Converter) device.
//!
//! This function reads a command from the BMR454 (Intermediate Bus Converter) device.
//! This function also calls rcRootI2cLock() and rcRootI2cUnlock() during its execution since it accesses
//! devices on the root I2C bus.  If additional I2C commands need to be atomically executed on the root I2C
//! bus, then manually call rcRootI2cLock() and rcRootI2cUnlock() around the entire root I2C bus access.
//!
//! @note The power to the TIU must be enabled by calling bpTiuAuxPowerEnable() before using this function.
//!       If bpTiuAuxPowerEnable() is not called, then the BMR454 is not active and does not respond to commands.
//! @note Some BMR454 command reads cause the device to NACK further commands for some amount of time (sometimes as
//!       long as 500ms).
//! @param[in] psSlot The index of a power supply according to the table below:
//! | psSlot | Comment                     |
//! | :----: | :-------------------------- |
//! |   0    | Left internal power supply  |
//! |   1    | Right internal power supply |
//! |   2    | Left external power supply  |
//! |   3    | Right external power supply |
//!
//! @param[in] command    BMR454 command to read.
//! @param[out] pReadData A data buffer of bytes to hold the data read from the BMR454 device via the
//!                       I2C bus.  Note that the bytes received from the device are located sequentially
//!                       in the buffer in the order they were received with the first byte being located
//!                       directly at \c pReadData.  Also note that the PEC (Packet Error Checking) byte
//!                       is automatically checked within the API and is not included in \c pReadData.
//!                       Because of this, the PEC byte should not be included in the \c readLength value.
//!                       Lastly, this parameter cannot be NULL since all reads return at least one byte.
//! @param[in] readLength The byte length of the \c pReadData buffer.  Note that this parameter cannot be 0
//!                       since all reads return at least one byte.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS psExtBmr454Read(_In_ INT psSlot, _In_ BYTE command, _Out_writes_bytes_all_(readLength) LPVOID pReadData, _In_ DWORD readLength);

//! @brief Reads the board-level traceability values from a power supply extender card.
//!
//! This function reads the board-level traceability values from a power supply extender card.  The values are defined in the #BLT structure.
//!
//! @param[in] psSlot The index of a power supply according to the table below:
//! | psSlot | Comment                     |
//! | :----: | :-------------------------- |
//! |   0    | Left internal power supply  |
//! |   1    | Right internal power supply |
//! |   2    | Left external power supply  |
//! |   3    | Right external power supply |
//!
//! @param[out] pBlt   The address of a #BLT structure that will be populated with the data.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS psExtBltBoardRead(_In_ INT psSlot, _Out_ PBLT pBlt);

//! @brief Writes the board-level traceability values of a power supply extender card.
//!
//! This function writes the board-level traceability values of a power supply extender card.  The values are defined in the #BLT structure.
//!
//! The \c flags field allows this function to update individual values.  The flags can be ORed together.  Only the fields indicated by
//! \c flags need to be populated in the provided \c pBlt parameter.
//!
//! \par Updating the Full BLT - C
//! \code{.c}
//! #include <Windows.h>
//! #include "PowersupplyApi.h"
//! #include "string.h"
//!
//! int main()
//! {
//!     BLT blt;
//!     HIL_STATUS status;
//!     INT slot = 0;
//!     blt.Id = 0x01000000;
//!     strcpy(blt.DeviceName,"DeviceName");
//!     strcpy(blt.VendorName,"INTEL");
//!     strcpy(blt.PartNumberAsBuilt,"AAxxxxxx-xxx");
//!     strcpy(blt.PartNumberCurrent,blt.PartNumberAsBuilt);
//!     strcpy(blt.SerialNumber,"12345678");
//!     strcpy(blt.ManufactureDate,"YYMMDD");
//!     status = psExtBltBoardWrite(slot, &blt, BF_ALL);
//!     if (status != HS_SUCCESS)
//!         return -1;
//!     return 0;
//! }
//! \endcode
//! \par Updating the Current Part Number - Python
//! \code{.py}
//! blt = hil.BLT()
//! slot = 0
//! blt.PartNumberCurrent = 'AAyyyyyy-yyy'
//! hil.psExtBltBoardWrite(slot, blt, hil.BF_PART_NUMBER_CURRENT)
//! # Note Python returns None for HIL_STATUS if successful and raises RuntimeError on failure.
//! \endcode
//!
//! \note HS_BLT_NOT_FOUND can be returned on an update (lack of #BF_ALL flag).
//!
//! @param[in] psSlot The index of a power supply according to the table below:
//! | psSlot | Comment                     |
//! | :----: | :-------------------------- |
//! |   0    | Left internal power supply  |
//! |   1    | Right internal power supply |
//! |   2    | Left external power supply  |
//! |   3    | Right external power supply |
//!
//! @param[in] pBlt   The address of a #BLT structure that contains the data to be written or updated.
//! @param[in] flags  #BLT_FLAGS bits indicating what fields in \c pBlt are valid.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS psExtBltBoardWrite(_In_ INT psSlot, _In_ PCBLT pBlt, _In_ BLT_FLAGS flags);

//! @brief Reads bytes from a power supply extender card's 24LC512 EEPROM.
//!
//! This function reads bytes from a power supply extender card's 24LC512 EEPROM.  It is a 64KB device, accessed using
//! I2C address 0xAE.
//!
//! @param[in] psSlot The index of a power supply according to the table below:
//! | psSlot | Comment                     |
//! | :----: | :-------------------------- |
//! |   0    | Left internal power supply  |
//! |   1    | Right internal power supply |
//! |   2    | Left external power supply  |
//! |   3    | Right external power supply |
//!
//! @param[in]  address The address to begin reading from the EEPROM.  Valid values are 0x0000-0xFFFF.
//! @param[out] pData   The output buffer.  It cannot be NULL.
//! @param[in]  length  The amount of data to read.  \c address plus \c length cannot exceed 0x10000 (64KB).
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS psExtEepromRead(_In_ INT psSlot, _In_ UINT address, _Out_writes_bytes_all_(length) LPVOID pData, _In_ DWORD length);

//! @brief Writes bytes to a power supply extender card's 24LC512 EEPROM.
//!
//! This function writes bytes to a power supply extender card's 24LC512 EEPROM.  It is a 64KB device, accessed using
//! I2C address 0xAE.
//!
//! @param[in] psSlot The index of a power supply according to the table below:
//! | psSlot | Comment                     |
//! | :----: | :-------------------------- |
//! |   0    | Left internal power supply  |
//! |   1    | Right internal power supply |
//! |   2    | Left external power supply  |
//! |   3    | Right external power supply |
//!
//! @param[in] address The address to begin writing to the EEPROM.  Valid values are 0x0000-0xFFFF.
//! @param[in] pData   The input buffer.  It cannot be NULL.
//! @param[in] length  The amount of data to write.  \c address plus \c length cannot exceed 0x10000 (64KB).
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS psExtEepromWrite(_In_ INT psSlot, _In_ UINT address, _In_reads_bytes_(length) const void* pData, _In_ DWORD length);

//! @brief Reads bytes from a Murata or Delta power supply's AT24C02D EEPROM.
//!
//! This function reads bytes from a Murata or Delta power supply's AT24C02D EEPROM.  It is a 256-byte device containing the FRU (Field
//! Replaceable Unit) information of the supply such as the vendor name, part number and product code.
//!
//! @param[in] psSlot The index of a power supply according to the table below:
//! | psSlot | Comment                     |
//! | :----: | :-------------------------- |
//! |   0    | Left internal power supply  |
//! |   1    | Right internal power supply |
//! |   2    | Left external power supply  |
//! |   3    | Right external power supply |
//!
//! @param[in]  address The address to begin reading from the FRU EEPROM.  Valid values are 0x00-0xFF.
//! @param[out] pData   The output buffer.  It cannot be NULL.
//! @param[in]  length  The amount of data to read.  \c address plus \c length cannot exceed 0x100 (256 bytes).
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS psFruEepromRead(_In_ INT psSlot, _In_ UINT address, _Out_writes_bytes_all_(length) LPVOID pData, _In_ DWORD length);

//! @brief Reads the board-level traceability values from a Delta or Murata power supply.
//!
//! This function reads the board-level traceability values from a Delta or Murata power supply.  The values are defined in the #BLT structure.
//! Though the supplies do not store equivalent #BLT fields, a #BLT structure is returned populated with compatible fields from the
//! FRU (Field Replaceable Unit) information of the supplies.
//!
//! @param[in] psSlot The index of a power supply according to the table below:
//! | psSlot | Comment                     |
//! | :----: | :-------------------------- |
//! |   0    | Left internal power supply  |
//! |   1    | Right internal power supply |
//! |   2    | Left external power supply  |
//! |   3    | Right external power supply |
//!
//! @param[out] pBlt   The address of a #BLT structure that will be populated with the data.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS psBltBoardRead(_In_ INT psSlot, _Out_ PBLT pBlt);

//! @brief Updates the Intel part number in a Delta power supply.
//!
//! The third-party Delta and Murata power supplies do not have an Intel compatible #BLT area, but this API simulates the Intel
//! BLT write functionality available on other device APIs.  This function only updates the Intel part number in a Delta supply.
//!
//! @param[in] psSlot The index of a power supply according to the table below:
//! | psSlot | Comment                     |
//! | :----: | :-------------------------- |
//! |   0    | Left internal power supply  |
//! |   1    | Right internal power supply |
//! |   2    | Left external power supply  |
//! |   3    | Right external power supply |
//!
//! @param[in] pBlt  The address of a #BLT structure.  Only the \c PartNumberCurrent field must be populated.  It must begin with
//!                  the two letters "AA" and be no more than 13 characters long, not including the terminating null.  This is
//!                  a limitation of the field width of the Delta supply.
//! @param[in] flags This field must be #BF_PART_NUMBER_CURRENT.  Other #BLT_FLAGS are not supported.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS psBltBoardWrite(_In_ INT psSlot, _In_ PCBLT pBlt, _In_ BLT_FLAGS flags);

//! @brief Reads firmware version numbers of the primary, secondary, and secondary communication (COM) microcontrollers in a Delta power supply.
//!
//! This function reads firmware version numbers of the primary, secondary, and secondary communication (COM) microcontrollers in a Delta power supply.
//! The returned 16-bit values are in the format 0xMMmm where the upper 8-bits are the major version and lower 8-bits are the minor version.
//! For example, 0x0205 is version 2.5.
//! @param[in] psSlot The index of a power supply according to the table below:
//!                   | psSlot | Comment                     |
//!                   | :----: | :-------------------------- |
//!                   |   0    | Left internal power supply  |
//!                   |   1    | Right internal power supply |
//!                   |   2    | Left external power supply  |
//!                   |   3    | Right external power supply |
//! @param[out] pPri  The address of a WORD of memory representing the primary microcontroller firmware version number.  It may not be NULL.
//! @param[out] pSec  The address of a WORD of memory representing the secondary microcontroller firmware version number. It may not be NULL.
//! @param[out] pCom  The address of a WORD of memory representing the secondary communication microcontroller (COM) firmware version number.  It may not be NULL.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS psFirmwareVersions(_In_ INT psSlot, _Out_ LPWORD pPri, _Out_ LPWORD pSec, _Out_ LPWORD pCom);

//! @brief Loads binary firmware image file for the specified microcontroller in a Delta power supply.
//!
//! This function loads binary firmware image file for the specified microcontroller in a Delta power supply.
//! @note The secondary communication (index=2) firmware requires an updated bootloader in the Delta supply and requires part number AAJ33014-004 or later to work.
//! @note A cold reboot is recommended after changing the firmware.
//! @param[in] psSlot   The index of a power supply according to the table below:
//!                     | psSlot | Comment                     |
//!                     | :----: | :-------------------------- |
//!                     |   0    | Left internal power supply  |
//!                     |   1    | Right internal power supply |
//!                     |   2    | Left external power supply  |
//!                     |   3    | Right external power supply |
//! @param[in] index    The index of the microcontroller to program according to the table below:
//!                     | index  | Microcontroller          |
//!                     | :----: | :----------------------- |
//!                     |   0    | Primary                  |
//!                     |   1    | Secondary                |
//!                     |   2    | Secondary Communication  |
//! @param[in] filename An ANSI string containing an absolute or relative (to the current directory) firmware filename.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS psFirmwareLoad(_In_ INT psSlot, _In_ UINT index, _In_ LPCSTR filename);

//! @brief Loads binary firmware image for the specified microcontroller in a Delta power supply.
//!
//! This function loads binary firmware image for the specified microcontroller in a Delta power supply.
//! @note The secondary communication (index=2) firmware requires an updated bootloader in the Delta supply and requires part number AAJ33014-004 or later to work.
//! @note A cold reboot is recommended after changing the firmware.
//! @param[in] psSlot The index of a power supply according to the table below:
//!                   | psSlot | Comment                     |
//!                   | :----: | :-------------------------- |
//!                   |   0    | Left internal power supply  |
//!                   |   1    | Right internal power supply |
//!                   |   2    | Left external power supply  |
//!                   |   3    | Right external power supply |
//! @param[in] index  The index of the microcontroller to program according to the table below:
//!                   | index  | Microcontroller          |
//!                   | :----: | :---------------------- |
//!                   |   0    | Primary                 |
//!                   |   1    | Secondary               |
//!                   |   2    | Secondary Communication |
//! @param[in] pData  The firmware's binary image data.
//! @param[in] length The length of \c pData in bytes.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS psFirmwareBufferLoad(_In_ INT psSlot, _In_ UINT index, _In_reads_bytes_(length) LPCVOID pData, _In_ DWORD length);

#ifdef __cplusplus
}
#endif
