// INTEL CONFIDENTIAL
// Copyright 2017-2018 Intel Corporation All Rights Reserved.
//
// The source code contained or described herein and all documents related to
// the source code ("Material") are owned by Intel Corporation or its
// suppliers or licensors. Title to the Material remains with Intel
// Corporation or its suppliers and licensors. The Material contains trade
// secrets and proprietary and confidential information of Intel or its
// suppliers and licensors. The Material is protected by worldwide copyright
// and trade secret laws and treaty provisions. No part of the Material may be
// used, copied, reproduced, modified, published, uploaded, posted,
// transmitted, distributed, or disclosed in any way without Intel's prior
// express written permission.
//
// No license under any patent, copyright, trade secret or other intellectual
// property right is granted to or conferred upon you by disclosure or
// delivery of the Materials, either expressly, by implication, inducement,
// estoppel or otherwise. Any license under such intellectual property rights
// must be express and approved by Intel in writing.

//! @file
//! @brief Non-function definitions such as constants and structures used by the HDMT DPS APIs.

#pragma once

const UINT HL_R1 = 1u << 8; //!< HVIL 1st resistor load.  See hvilLoadControl() for resistor value details.
const UINT HL_R2 = 1u << 7; //!< HVIL 2nd resistor load.  See hvilLoadControl() for resistor value details.
const UINT HL_R3 = 1u << 6; //!< HVIL 3rd resistor load.  See hvilLoadControl() for resistor value details.
const UINT HL_R4 = 1u << 5; //!< HVIL 4th resistor load.  See hvilLoadControl() for resistor value details.
const UINT HL_R5 = 1u << 4; //!< HVIL 5th resistor load.  See hvilLoadControl() for resistor value details.
const UINT HL_R6 = 1u << 3; //!< HVIL 6th resistor load.  See hvilLoadControl() for resistor value details.
const UINT HL_R7 = 1u << 2; //!< HVIL 7th resistor load.  See hvilLoadControl() for resistor value details.
const UINT HL_R8 = 1u << 1; //!< HVIL 8th resistor load.  See hvilLoadControl() for resistor value details.
const UINT HL_R9 = 1u << 0; //!< HVIL 9th resistor load.  See hvilLoadControl() for resistor value details.
