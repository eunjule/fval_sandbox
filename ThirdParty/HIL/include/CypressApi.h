// INTEL CONFIDENTIAL
// Copyright 2014-2018 Intel Corporation All Rights Reserved.
//
// The source code contained or described herein and all documents related to
// the source code ("Material") are owned by Intel Corporation or its
// suppliers or licensors. Title to the Material remains with Intel
// Corporation or its suppliers and licensors. The Material contains trade
// secrets and proprietary and confidential information of Intel or its
// suppliers and licensors. The Material is protected by worldwide copyright
// and trade secret laws and treaty provisions. No part of the Material may be
// used, copied, reproduced, modified, published, uploaded, posted,
// transmitted, distributed, or disclosed in any way without Intel's prior
// express written permission.
//
// No license under any patent, copyright, trade secret or other intellectual
// property right is granted to or conferred upon you by disclosure or
// delivery of the Materials, either expressly, by implication, inducement,
// estoppel or otherwise. Any license under such intellectual property rights
// must be express and approved by Intel in writing.

//! @file
//! @brief Low-level APIs to directly control Cypress USB devices.
//!
//! The Cypress APIs implement functionality of Cypress USB devices including access to GPIO and FPGA programming.
//! They are exposed to facilitate algorithm development for card-level APIs and should not be used
//! unless a card-level API is not available.
#pragma once
#include "HilDefs.h"
#include "CypressDefs.h"

#ifdef __cplusplus
extern "C" {
#endif

//! @brief Locates a Cypress USB device with a specific vendor and product ID.
//!
//! This function locates a Cypress USB device with a specific vendor and product ID. The search can be optionally
//! constrained by a specific physical slot.  A handle to the Cypress driver device interface is returned.  Be sure
//! to close the handle via cypDeviceClose() when done with it to prevent resource leaks.  Prefer using card-level
//! APIs that used internally-cached handles instead.
//! @param[in] vidpid A DWORD in 0xVVVVPPPP format where VVVV is the vendor ID and PPPP is the product ID of
//!                   the Cypress USB device to locate.  Common \c CYP_VIDPID_xxxx constants are provided.
//! @param[in] slot   Physical slot 0-11 can be specified, or HIL_ANY_SLOT(-1) for slotless devices.
//! @param[in] additional  Additional PCI hardware path to match beyond the slot.  Useful if two identical devices can appear beyond a physical tester slot.  Can be NULL.
//! @param[out] pHandle Pointer to returned handle value.
//! @retval HS_SUCCESS \c pHandle is valid.
//! @retval HS_DEVICE_NOT_FOUND \c pHandle is set to INVALID_HANDLE_VALUE.
HIL_API HIL_STATUS cypDeviceOpen(_In_ DWORD vidpid, _In_ INT slot, _In_opt_z_ LPCWSTR additional, _Out_ PHANDLE pHandle);

//! @brief Closes device handles returned by cypDeviceOpen().
//!
//! This function closes device handles returned by cypDeviceOpen().  Handles must be closed
//! when the user is through with them or there will be a resource leak that can interfere
//! with other APIs using the device.  For example, load FPGA firmware on a PCI device
//! requires all PCI device handles to be closed.  Prefer using card-level APIs that use
//! internally-cached handles instead.
//! @param[in] handle the device handle to close.
//! @retval HS_SUCCESS The handle was closed successfully.
//! @retval HS_INVALID_HANDLE \c handle was not a valid OS handle.
//! @retval HS_UNSUCCESSFUL A non-specific error occurred.
HIL_API HIL_STATUS cypDeviceClose(_In_ HANDLE handle);

//! @brief Retrieves the Cypress USB vendor and product ID for the device.
//!
//! This function returns the Cypress USB vendor and product ID for the device.
//! @param[in] device A Cypress device handle opened by cypDeviceOpen().
//! @param[out] pVendorId The requested vendor ID.
//! @param[out] pProductId The requested product ID.
//! @retval HS_SUCCESS \c pVendorId and \c pProductId are valid.
//! @retval HS_UNSUCCESSFUL \c pVendorId and \c pProductId are set to 0xFFFF.
HIL_API HIL_STATUS cypVidPid(_In_ HANDLE device, _Out_ LPWORD pVendorId, _Out_ LPWORD pProductId);

//! @brief Sets the direction bits for a Cypress GPIO port.
//!
//! This function sets the direction bits for a Cypress GPIO port.
//! @param[in] device A device handle returned from cypDeviceOpen().
//! @param[in] port One of the \ref CYP_PORT enumerations.
//! @param[in] direction Each bit in this byte controls the direction of the corresponding bit on the port (0=input, 1=output).
//! @retval HS_SUCCESS
//! @retval HS_UNSUCCESSFUL
//! @retval HS_INVALID_PARAMETER A read- or write-only port was specified.
HIL_API HIL_STATUS cypPortDirectionWrite(_In_ HANDLE device, _In_ CYP_PORT port, _In_ BYTE direction);

//! @brief Reads the direction bits for a Cypress GPIO port.
//!
//! This function reads the direction bits for a Cypress GPIO port.
//! @param[in] device A device handle returned from cypDeviceOpen().
//! @param[in] port One of the \ref CYP_PORT enumerations.
//! @param[out] pDirection Each bit in this byte represents the direction of the corresponding bit on the port (0=input, 1=output).
//! @retval HS_SUCCESS
//! @retval HS_UNSUCCESSFUL
//! @retval HS_INVALID_PARAMETER A read- or write-only port was specified.
HIL_API HIL_STATUS cypPortDirectionRead(_In_ HANDLE device, _In_ CYP_PORT port, _Out_ PBYTE pDirection);

//! @brief Writes a byte to a Cypress GPIO port.
//!
//! This function writes a byte to a Cypress GPIO port.
//! @param[in] device A device handle returned from cypDeviceOpen().
//! @param[in] port One of the \ref CYP_PORT enumerations.
//! @param[in] data The 8-bit value to write.
//! @retval HS_SUCCESS
//! @retval HS_UNSUCCESSFUL
//! @retval HS_INVALID_PARAMETER A read-only port was specified.
HIL_API HIL_STATUS cypPortWrite(_In_ HANDLE device, _In_ CYP_PORT port, _In_ BYTE data);

//! @brief Reads a byte to a Cypress GPIO port.
//!
//! This function reads a byte from a Cypress GPIO port.
//! @param[in] device A device handle returned from cypDeviceOpen().
//! @param[in] port One of the \ref CYP_PORT enumerations.
//! @param[out] pData The address of a byte to store the 8-bit result.
//! @retval HS_SUCCESS
//! @retval HS_UNSUCCESSFUL
//! @retval HS_INVALID_PARAMETER A write-only port was specified.
HIL_API HIL_STATUS cypPortRead(_In_ HANDLE device, _In_ CYP_PORT port, _Out_ PBYTE pData);

//! @brief Sets a bit on a Cypress GPIO port.
//!
//! This function sets a single bit on a Cypress GPIO port.
//! @param[in] device A device handle returned from cypDeviceOpen().
//! @param[in] port One of the \ref CYP_PORT enumerations.
//! @param[in] bit The bit to set (0-7).
//! @param[in] value \c TRUE to set the bit.  \c FALSE to clear it.
//! @retval HS_SUCCESS
//! @retval HS_UNSUCCESSFUL
//! @retval HS_INVALID_PARAMETER A read-only port was specified.
HIL_API HIL_STATUS cypPortBitWrite(_In_ HANDLE device, _In_ CYP_PORT port, _In_range_(0, 7) UINT bit, _In_ BOOL value);

//! @brief Reads a bit on a Cypress GPIO port.
//!
//! This function reads a single bit from a Cypress GPIO port.
//! @param[in] device A device handle returned from cypDeviceOpen().
//! @param[in] port One of the \ref CYP_PORT enumerations.
//! @param[in] bit The bit to read (0-7).
//! @param[out] pValue \c TRUE if the bit was set.  \c FALSE if not.
//! @retval HS_SUCCESS
//! @retval HS_UNSUCCESSFUL
//! @retval HS_INVALID_PARAMETER A write-only port was specified.
HIL_API HIL_STATUS cypPortBitRead(_In_ HANDLE device, _In_ CYP_PORT port, _In_range_(0, 7) UINT bit, _Out_ PBOOL pValue);

//! @brief Writes a number of bytes to an address on the I2C bus of a Cypress USB device.
//!
//! This function issues a command to write \c length bytes to an I2C address on the I2C bus of a Cypress USB device.
//! Call cypI2cStatus() to verify successful completion of the write.
//! @param[in] device A device handle returned from cypDeviceOpen().
//! @param[in] i2cAddress The I2C address of the device to receive the data (0x00-0xFF).
//! @param[in] pData The starting address of the bytes to write.
//! @param[in] length The number of bytes to write.
//! @retval HS_SUCCESS The command was issued successfully.
//! @retval HS_UNSUCCESSFUL The command was not issued successfully.
//! @retval HS_INVALID_PARAMETER The I2C address was invalid.
HIL_API HIL_STATUS cypI2cWrite(_In_ HANDLE device, _In_ UINT i2cAddress, _In_reads_bytes_(length) const void* pData, _In_ DWORD length);

//! @brief Reads a number of bytes from an address on the I2C bus of a Cypress USB device.
//!
//! This function issues a command to read \c length bytes from an I2C address on the I2C bus of a Cypress USB device.
//! Call cypI2cStatus() to verify successful completion of the read.
//! @param[in] device A device handle returned from cypDeviceOpen().
//! @param[in] i2cAddress The I2C address of the device from which to receive the data (0x00-0xFF).
//! @param[out] pData A buffer at least \c length bytes long to receive the data.
//! @param[in] length The number of bytes to read.
//! @retval HS_SUCCESS The command was issued successfully.
//! @retval HS_UNSUCCESSFUL The command was not issued successfully.
//! @retval HS_INVALID_PARAMETER The I2C address was invalid.
HIL_API HIL_STATUS cypI2cRead(_In_ HANDLE device, _In_ UINT i2cAddress, _Out_writes_bytes_all_(length) LPVOID pData, _In_ DWORD length);

//! @brief Combines a byte write of an address with a read of one or more data bytes from an address on the I2C bus of a Cypress USB device.
//!
//! This function issues a command to write a byte address to an I2C address on the I2C bus of a Cypress USB device, then read \c length bytes back
//! from that device.  It is a shortcut for a cypI2cWrite() of one byte followed by cypI2cRead() of one or more bytes.
//! Call cypI2cStatus() to verify successful completion of the read.
//! @param[in] device A device handle returned from cypDeviceOpen().
//! @param[in] i2cAddress The I2C address of the device from which to receive the data (0x00-0xFF).
//! @param[in] registerAddress The register address to read from (0x00-0xFF).
//! @param[out] pData A buffer at least \c length bytes long to receive the data.
//! @param[in] length The number of bytes to read.
//! @retval HS_SUCCESS The command was issued successfully.
//! @retval HS_UNSUCCESSFUL The command was not issued successfully.
//! @retval HS_INVALID_PARAMETER The I2C address was invalid.
HIL_API HIL_STATUS cypI2cRegisterRead(_In_ HANDLE device, _In_ UINT i2cAddress, _In_ UINT registerAddress, _Out_writes_bytes_all_(length) LPVOID pData, _In_ DWORD length);

//! @brief Returns the status of the last I2C read or write operation.
//!
//! This function must be called after cypI2cRead(), cypI2cWrite(), or cypI2cRegisterRead() to verify the I2C operation completed successfully.
//! @param[in] device A device handle returned from cypDeviceOpen().
//! @param[out] pStatus The address of an \ref CYP_I2C_STATUS data value.  If the function is successful the I2C status of the last operation is returned.
//! @retval HS_SUCCESS \c *pStatus is valid.
//! @retval HS_UNSUCCESSFUL
HIL_API HIL_STATUS cypI2cStatus(_In_ HANDLE device, _Out_ CYP_I2C_STATUS* pStatus);

//! @brief Loads an FPGA image file directly into an FPGA via the GPIF method.
//!
//! This function loads the data from the given filename into an FPGA using the GPIF
//! CPLD mode.  Most FPGAs in HDMT use this mode.  See cypDataIoFpgaLoad()
//! for programming the 400MHz channel card helper FPGA.
//! @param[in] device A device handle returned from cypDeviceOpen().
//! @param[in] filename The data file used to program the FPGA image.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS cypGpifFpgaLoad(_In_ HANDLE device, _In_z_ LPCSTR filename);

//! @brief Loads an FPGA image file directly into an FPGA via the Data I/O method.
//!
//! This function loads the data from the given filename into an FPGA using the Data I/O
//! CPLD mode.  The 400MHz channel card helper FPGA is known to use this method.
//! Most FPGAs in HDMT use cypGpifFpgaLoad().
//! @param[in] device A device handle returned from cypDeviceOpen().
//! @param[in] filename The data file used to program the FPGA image.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS cypDataIoFpgaLoad(_In_ HANDLE device, _In_z_ LPCSTR filename);

//! @brief Programs an FPGA image file into the base SPI FLASH used to boot initialize an FPGA.
//!
//! This function programs the data from the given filename into the base SPI FLASH used to boot initialize an FPGA
//! using the SPI FLASH CPLD mode.  This function can take up to 10 minutes to complete.
//! @param[in] device A device handle returned from cypDeviceOpen().
//! @param[in] filename The data file used to program SPI FLASH.
//! @param[in] callback The callback function to be called with progress updates while programming the SPI FLASH.  Value can be NULL.
//! @param[in] context The context that will be passed back as an argument in the #HIL_BASE_LOAD_CALLBACK callback function.  Value can be NULL.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS cypSpiFpgaBaseLoad(_In_ HANDLE device, _In_z_ LPCSTR filename, _In_opt_ HIL_BASE_LOAD_CALLBACK callback, _In_opt_ void* context);

//! @brief Loads an FPGA image buffer directly into an FPGA via the GPIF method.
//!
//! This function loads the data from the given buffer into an FPGA using the GPIF
//! CPLD mode.  Most FPGAs in HDMT use this mode.  See cypDataIoFpgaLoad()
//! for programming the 400MHz channel card helper FPGA.
//! @param[in] device A device handle returned from cypDeviceOpen().
//! @param[in] pData The FPGA image data.
//! @param[in] length The length of the FPGA image data in bytes.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS cypGpifFpgaBufferLoad(_In_ HANDLE device, _In_reads_bytes_(length) LPCVOID pData, _In_ DWORD length);

//! @brief Loads an FPGA image buffer directly into an FPGA via the Data I/O method.
//!
//! This function loads the data from the given buffer into an FPGA using the Data I/O
//! CPLD mode.  The 400MHz channel card helper FPGA is known to use this method.
//! Most FPGAs in HDMT use cypGpifFpgaLoad().
//! @param[in] device A device handle returned from cypDeviceOpen().
//! @param[in] pData The FPGA image data.
//! @param[in] length The length of the FPGA image data in bytes.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS cypDataIoFpgaBufferLoad(_In_ HANDLE device, _In_reads_bytes_(length) LPCVOID pData, _In_ DWORD length);

//! @brief Programs an FPGA image buffer into the base SPI FLASH used to boot initialize an FPGA.
//!
//! This function programs the data from the given buffer into the base SPI FLASH used to boot initialize an FPGA
//! using the SPI FLASH CPLD mode.  This function can take up to 10 minutes to complete.
//!
//! The callback and context parameters are used to report progress back during a long operation.  See #HIL_BASE_LOAD_CALLBACK
//! for details.
//! @param[in] device A device handle returned from cypDeviceOpen().
//! @param[in] pData The FPGA image data.
//! @param[in] length The length of the FPGA image data in bytes.
//! @param[in] callback The callback function to be called with progress updates while programming the SPI FLASH.  Value can be NULL.
//! @param[in] context The context that will be passed back as an argument in the #HIL_BASE_LOAD_CALLBACK callback function.  Value can be NULL.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS cypSpiFpgaBaseBufferLoad(_In_ HANDLE device, _In_reads_bytes_(length) LPCVOID pData, _In_ DWORD length, _In_opt_ HIL_BASE_LOAD_CALLBACK callback, _In_opt_ void* context);

#ifdef __cplusplus
}
#endif
