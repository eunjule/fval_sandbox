// INTEL CONFIDENTIAL
// Copyright 2019 Intel Corporation All Rights Reserved.
//
// The source code contained or described herein and all documents related to
// the source code ("Material") are owned by Intel Corporation or its
// suppliers or licensors. Title to the Material remains with Intel
// Corporation or its suppliers and licensors. The Material contains trade
// secrets and proprietary and confidential information of Intel or its
// suppliers and licensors. The Material is protected by worldwide copyright
// and trade secret laws and treaty provisions. No part of the Material may be
// used, copied, reproduced, modified, published, uploaded, posted,
// transmitted, distributed, or disclosed in any way without Intel's prior
// express written permission.
//
// No license under any patent, copyright, trade secret or other intellectual
// property right is granted to or conferred upon you by disclosure or
// delivery of the Materials, either expressly, by implication, inducement,
// estoppel or otherwise. Any license under such intellectual property rights
// must be express and approved by Intel in writing.

//! @file
//! @brief APIs to control the HBI DPS AIC MFG test board.

#pragma once
#include "HilDefs.h"

#ifdef __cplusplus
extern "C" {
#endif

//! @brief Verifies an HBI DPS AIC MFG test board is present.
//!
//! This function verifies an HBI DPS AIC MFG test board is present.
//! A call to hbiMtbConnect() is not required for using other \c hbiMtbXXXXX functions.
//!
//! @param[in] slot A physical slot number. Valid values are 0 and 1.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMtbConnect(_In_ INT slot);

//! @brief Programs the vendor and product IDs of all USB devices on an HBI DPS AIC MFG test board.
//!
//! This function programs the vendor and product IDs (VID/PIDs) of FT240X and FT2232H on an HBI DPS AIC MFG test board.
//! Correct VID/PIDs are required to ensure Windows Device Manager displays a proper description of the device,
//! and HIL requires correct VID/PIDs to ensure software can locate and communicate with the correct component.
//! @note FTDI devices have a limited number of updates, but this function is safe to call because it
//! only writes if the component programming is incorrect.
//! @param[in] slot A physical slot number. Valid values are 0 and 1.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMtbVidPidsSet(_In_ INT slot);

//! @brief Reads the board-level traceability values from an HBI DPS AIC MFG test board.
//!
//! This function reads the board-level traceability values from an HBI DPS AIC MFG test board.
//!
//! @param[in] slot A physical slot number. Valid values are 0 and 1.
//! @param[out] pBlt The address of a #BLT structure that will be populated with the data.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMtbBltBoardRead(_In_ INT slot, _Out_ PBLT pBlt);

//! @brief Writes the board-level traceability values of an HBI DPS AIC MFG test board.
//!
//! This function writes the board-level traceability values of an HBI DPS AIC MFG test board.
//!
//! The \c flags field allows this function to update individual values.  The flags can be ORed together.  Only the fields indicated by
//! \c flags need to be populated in the provided \c pBlt parameter.
//!
//! \par Updating the Full BLT - C
//! \code{.c}
//! #include <Windows.h>
//! #include "HbiMtbApi.h"
//! #include "string.h"
//!
//! int main()
//! {
//!     int slot = 0;
//!     BLT blt;
//!     HIL_STATUS status;
//!     blt.Id = 0x100000FF; /* NOT the actual card ID */
//!     strcpy(blt.DeviceName,"DeviceName");
//!     strcpy(blt.VendorName,"INTEL");
//!     strcpy(blt.PartNumberAsBuilt,"AAxxxxxx-xxx");
//!     strcpy(blt.PartNumberCurrent,blt.PartNumberAsBuilt);
//!     strcpy(blt.SerialNumber,"12345678");
//!     strcpy(blt.ManufactureDate,"YYMMDD");
//!     status = hbiMtbBltBoardWrite(slot, &blt, BF_ALL);
//!     if (status != HS_SUCCESS)
//!         return -1;
//!     return 0;
//! }
//! \endcode
//! \par Updating the Current Part Number - Python
//! \code{.py}
//! slot = 0
//! blt = hil.BLT()
//! blt.PartNumberCurrent = 'AAyyyyyy-yyy'
//! hil.hbiMtbBltBoardWrite(slot,blt,hil.BF_PART_NUMBER_CURRENT)
//! # Note Python returns None for HIL_STATUS if successful and raises RuntimeError on failure.
//! \endcode
//!
//! \note HS_BLT_NOT_FOUND can be returned on an update (lack of #BF_ALL flag).
//!
//! @param[in] slot A physical slot number. Valid values are 0 and 1.
//! @param[in] pBlt The address of a #BLT structure that contains the data to be written or updated.
//! @param[in] flags #BLT_FLAGS bits indicating what fields in \c pBlt are valid.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMtbBltBoardWrite(_In_ INT slot, _In_ PCBLT pBlt, _In_ BLT_FLAGS flags);

//! @brief Reads raw data from an HBI DPS AIC MFG test board's BLT EEPROM.
//!
//! This function reads raw data from an HBI DPS AIC MFG test board's BLT EEPROM.
//!
//! @param[in] slot A physical slot number. Valid values are 0 and 1.
//! @param[out] pEepromData The output buffer.
//! @param[in,out] pLength On input, the size of the output buffer referenced by \c pData.  On output, the size of the EEPROM data.
//! @retval HS_INSUFFICIENT_BUFFER The buffer provided was too small.  \c pLength will indicate the minimum buffer size required.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMtbBltEepromRead(_In_ INT slot, _Out_writes_bytes_to_(_Old_(*pLength), *pLength) LPVOID pEepromData, _Inout_ LPDWORD pLength);

//! @brief Reports whether or not each of the four DPSs are connected to an HBI DPS AIC MFG test board.
//!
//! This function reports whether or not each of the four DPSs are connected to an HBI DPS AIC MFG test board.
//!
//! The returned 8-bit value is in the format 0b0000SSSS where the upper 4-bits are to be ignored (always 0) and lower 4-bits
//! indicate presence(1) or absence(0) of a DPS's electrical connection with an MFG test board in the following order DPS3 : DPS2 : DPS1: DPS0.
//!
//! For example 0b00001110 indicates that all DPSs except DPS0 are connected to the HBI DPS AIC MFG test board.
//!
//! @param[in] slot A physical slot number. Valid values are 0 and 1.
//! @param[out] pStatus The DPS present status byte.  It may not be NULL.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMtbDpsPresentStatus(_In_ INT slot, _Out_ PBYTE pStatus);

//! @brief Reports whether or not fuses of each of the four DPSs connected to an HBI DPS AIC MFG test board are functional.
//!
//! This function reports whether or not fuses of each of the four DPSs connected to an HBI DPS AIC MFG test board are functional.
//! Each bit in the returned 8-bit value indicates status of the eight DPS Fuses in the following order DPS7Fuse : DPS0Fuse. (1 = Fuse OK)
//!
//!
//! The returned 8-bit value is in the format 0b0000SSSS where the upper 4-bits are to be ignored (always 0) and lower 4-bits
//! indicate status of a DPS fuse in the following order DPS3 : DPS2 : DPS1: DPS0. (1 = Fuse OK)
//!
//! For example 0b00000111 indicates that all DPS fuses except DPS3 are OK.
//!
//! @param[in] slot A physical slot number. Valid values are 0 and 1.
//! @param[out] pStatus The DPS Fuse OK status byte.  It may not be NULL.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMtbDpsFuseOkStatus(_In_ INT slot, _Out_ PBYTE pStatus);

//! @brief Writes data to a 24LC512 (EEPROM) device on an HBI DPS AIC MFG test board.
//!
//! This function writes data to a 24LC512 (EEPROM) device on an HBI DPS AIC MFG test board.
//!
//! @note Of the 9 24LC512 devices on an HBI DPS AIC MFG test board, only 4 can be controlled through HIL.
//! Other devices viz. U124, U125, U126, U127 and U128 do not have their I2C interfaces exposed and can be
//! accessed using Ring Multiplier Trigger Queue mechanism.
//!
//! @param[in] slot A physical slot number. Valid values are 0 and 1.
//! @param[in] eepromIndex Index of the EEPROM to write.  Consult the following table.
//!                 Note that reference designators may change in new fabs.
//!                 | eepromIndex | Fab A |
//!                 | :---------: | :---: |
//!                 |      0      |  U107 |
//!                 |      1      |  U77  |
//!                 |      2      |  U139 |
//!                 |      3      |  U140 |
//! @param[in] address The location of the first byte in the EEPROM to which the data in the \c pData buffer will be written.
//! @param[in] pData   A data buffer containing data to write to the EEPROM device.
//! @param[in] length  The byte length of the \c pData buffer.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMtbEepromWrite(_In_ INT slot, _In_ UINT eepromIndex, _In_ DWORD address, _In_reads_bytes_(length) const void* pData, _In_ DWORD length);

//! @brief Reads data from a 24LC512 (EEPROM) device on an HBI DPS AIC MFG test board.
//!
//! This function reads data from a 24LC512 (EEPROM) device on an HBI DPS AIC MFG test board.
//!
//! @note Of the 9 24LC512 devices on an HBI DPS AIC MFG test board, only 4 can be controlled through HIL.
//! Other devices viz. U124, U125, U126, U127 and U128 do not have their I2C interfaces exposed and can be
//! accessed using Ring Multiplier Trigger Queue mechanism.
//!
//! @param[in] slot A physical slot number. Valid values are 0 and 1.
//! @param[in] eepromIndex Index of the EEPROM to read from.  Consult the following table.
//!                 Note that reference designators may change in new fabs.
//!                 | eepromIndex | Fab A |
//!                 | :---------: | :---: |
//!                 |      0      |  U107 |
//!                 |      1      |  U77  |
//!                 |      2      |  U139 |
//!                 |      3      |  U140 |
//! @param[in] address The location of the first byte in the EEPROM from which the data will be read.
//! @param[out] pData  A pointer to a data buffer in which the read data will be stored. Note that this buffer must have at least \c length
//!                    bytes allocated in order to complete successfully.
//! @param[in] length  The number of bytes to read from the EEPROM.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMtbEepromRead(_In_ INT slot, _In_ UINT eepromIndex, _In_ DWORD address, _Out_writes_bytes_all_(length) LPVOID pData, _In_ DWORD length);

//! @brief Directly writes an 8-bit register on the ADT7411 (8-channel ADC) device on an HBI DPS AIC MFG test board.
//!
//! This function writes an 8-bit register on the ADT7411 (8-channel ADC) device on an HBI DPS AIC MFG test board.
//!
//! @param[in] slot A physical slot number. Valid values are 0 and 1.
//! @param[in] reg A valid register address for the Analog Devices ADT7411.
//! @param[in] data The data to write.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMtbAdt7411Write(_In_ INT slot, _In_ BYTE reg, _In_ BYTE data);

//! @brief Directly reads an 8-bit register on the ADT7411 (8-channel ADC) device on an HBI DPS AIC MFG test board.
//!
//! This function reads an 8-bit register on the ADT7411 (8-channel ADC) device on an HBI DPS AIC MFG test board.
//!
//! @param[in] slot A physical slot number. Valid values are 0 and 1.
//! @param[in] reg  A valid register address for the Analog Devices ADT7411.
//! @param[out] pData The register contents.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMtbAdt7411Read(_In_ INT slot, _In_ BYTE reg, _Out_ PBYTE pData);

//! @brief Reads one of nine voltage monitor channels on an HBI DPS AIC MFG test board.
//!
//! This function reads one of the nine monitor channels on an HBI DPS AIC MFG test board.
//!
//! @param[in] slot A physical slot number. Valid values are 0 and 1.
//! @param[in] channel Selects the monitoring channel. Valid values are 0 to 8. The channels
//!                    and corresponding voltages monitored are :
//!                    | Channel |  Reference (Fab A)    |
//!                    | :-----: |  :------------------: |
//!                    |    0    |   +3P3V_16A           |
//!                    |    1    |   PEX_VREG_TEMP       |
//!                    |    2    |   LTM4644_3P3V_TEMP   |
//!                    |    3    |   LTM4650_5P0V_TEMP   |
//!                    |    4    |   MON_1P8V            |
//!                    |    5    |   MON_12V_DIV_BY_6    |
//!                    |    6    |   MON_0P9V            |
//!                    |    7    |   MON_5P0V_DIV_BY_3   |
//!                    |    8    |   MON_3P3V_DIV_BY_2   |
//! @param[out] pVoltage The voltage in volts.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMtbVmonRead(_In_ INT slot, _In_ UINT channel, _Out_ double* pVoltage);

//! @brief Writes an 8-bit register of a PCA9505 device on an HBI DPS AIC MFG test board.
//!
//! This function writes an 8-bit register of a PCA9505 device on an HBI DPS AIC MFG test board.
//!
//! @param[in] slot A physical slot number. Valid values are 0 and 1.
//! @param[in] chip A zero-based chip index.  Consult the following table.
//!                 Note that reference designators may change in new fabs.
//!                 | chip |  Fab A |
//!                 | :--: | :----: |
//!                 |  0   |   U22  |
//!                 |  1   |   U24  |
//!                 |  2   |   U78  |
//! @param[in] reg  A valid register address for the PCA9505 device.
//! @param[in] data The value to write.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMtbPca9505Write(_In_ INT slot, _In_ UINT chip, _In_ BYTE reg, _In_ BYTE data);

//! @brief Reads an 8-bit register of a PCA9505 device on an HBI DPS AIC MFG test board.
//!
//! This function reads an 8-bit register of a PCA9505 devices on an HBI DPS AIC MFG test board.
//!
//! @param[in] slot A physical slot number. Valid values are 0 and 1.
//! @param[in] chip A zero-based chip index.  Consult the following table.
//!                 Note that reference designators may change in new fabs.
//!                 | chip |  Fab A |
//!                 | :--: | :----: |
//!                 |  0   |   U22  |
//!                 |  1   |   U24  |
//!                 |  2   |   U78  |
//! @param[in] reg       A valid register address for the PCA9505 device.
//! @param[out] pData    The register contents read from the device.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMtbPca9505Read(_In_ INT slot, _In_ UINT chip, _In_ BYTE reg, _Out_ PBYTE pData);

//! @brief Writes an 8-bit register of a MAX14661 device on an HBI DPS AIC MFG test board.
//!
//! This function writes an 8-bit register of a MAX14661 device on an HBI DPS AIC MFG test board.
//!
//! @param[in] slot A physical slot number. Valid values are 0 and 1.
//! @param[in] chip A zero-based chip index. Consult the following table.
//!                 Note that reference designators may change in new fabs.
//!                 |  chip   | Fab A  |
//!                 | :-----: | :----: |
//!                 |    0    |  EU1   |
//!                 |    1    |  EU2   |
//!                 |    2    |  EU5   |
//!                 |    3    |  EU9   |
//!                 |    4    |  EU7   |
//!                 |    5    |  EU8   |
//!                 |    6    |  EU12  |
//!                 |    7    |  EU14  |
//! @param[in] reg  A valid register address for the MAX14661 device.
//! @param[in] data The value to write.  Valid values are 0-255 (00h-FFh).
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMtbMax14661Write(_In_ INT slot, _In_ UINT chip, _In_ BYTE reg, _In_ BYTE data);

//! @brief Reads an 8-bit register of a MAX14661 device on an HBI DPS AIC MFG test board.
//!
//! This function reads an 8-bit register of a MAX14661 device on an HBI DPS AIC MFG test board.
//!
//! @param[in] slot A physical slot number. Valid values are 0 and 1.
//! @param[in] chip A zero-based chip index. Consult the following table.
//!                 Note that reference designators may change in new fabs.
//!                 |  chip   | Fab A  |
//!                 | :-----: | :----: |
//!                 |    0    |  EU1   |
//!                 |    1    |  EU2   |
//!                 |    2    |  EU5   |
//!                 |    3    |  EU9   |
//!                 |    4    |  EU7   |
//!                 |    5    |  EU8   |
//!                 |    6    |  EU12  |
//!                 |    7    |  EU14  |
//! @param[in] reg    A valid register address for the MAX14661 device.
//! @param[out] pData The register contents read from the device.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMtbMax14661Read(_In_ INT slot, _In_ UINT chip, _In_ BYTE reg, _Out_ PBYTE pData);

//! @brief Writes a register of a LM75A (Temperature Sensor) device on an HBI DPS AIC MFG test board.
//!
//! This function writes a register of a LM75A (Temperature Sensor) device on an HBI DPS AIC MFG test board.
//!
//! @param[in] slot A physical slot number. Valid values are 0 and 1.
//! @param[in] chip A zero-based chip index. Consult the following table.
//!                 Note that reference designators may change in new fabs.
//!                 |  chip   | Fab A |
//!                 | :-----: | :---: |
//!                 |    0    |  U80  |
//!                 |    1    |  U81  |
//!                 |    2    |  U82  |
//!                 |    3    |  U83  |
//!                 |    4    |  U84  |
//!                 |    5    |  U85  |
//! @param[in] reg  Writable LM75A register address.
//!                 | Register | Description    | Bits |
//!                 | :------: | :------------- | :--: |
//!                 |   0x01   | Configuration  |   8  |
//!                 |   0x02   | THYST          |  16  |
//!                 |   0x03   | TOS            |  16  |
//! @param[in] data The value to write.  Valid values are 0-255 (00h-FFh).
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMtbLm75aWrite(_In_ INT slot, _In_ UINT chip, _In_ BYTE reg, _In_ WORD data);

//! @brief Reads a register of a LM75A (Temperature Sensor) device on an HBI DPS AIC MFG test board.
//!
//! This function reads a register of a LM75A (Temperature Sensor) device on an HBI DPS AIC MFG test board.
//!
//! @param[in] slot A physical slot number. Valid values are 0 and 1.
//! @param[in] chip A zero-based chip index. Consult the following table.
//!                 Note that reference designators may change in new fabs.
//!                 |  chip   | Fab A |
//!                 | :-----: | :---: |
//!                 |    0    |  U80  |
//!                 |    1    |  U81  |
//!                 |    2    |  U82  |
//!                 |    3    |  U83  |
//!                 |    4    |  U84  |
//!                 |    5    |  U85  |
//! @param[in] reg LM75A register address.
//!                 | Register | Description         | Bits |
//!                 | :------: | :------------------ | :--: |
//!                 |   0x00   | Temperature         |  16  |
//!                 |   0x01   | Configuration       |   8  |
//!                 |   0x02   | THYST               |  16  |
//!                 |   0x03   | TOS                 |  16  |
//!                 |   0x07   | Product ID Register |   8  |
//! @param[out] pData The register contents read from the device.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMtbLm75aRead(_In_ INT slot, _In_ UINT chip, _In_ BYTE reg, _Out_ LPWORD pData);

//! @brief Reads the specified temperature channel on an HBI DPS AIC MFG test board.
//!
//! This function reads the specified temperature channel on an HBI DPS AIC MFG test board.
//!
//! @param[in] slot A physical slot number. Valid values are 0 and 1.
//! @param[in] channel  Selects a monitoring channel. Consult the following table:
//!                     | channel | Measured Temperature  |
//!                     | :-----: | :-------------------: |
//!                     |    0    |     LM75A (U80)       |
//!                     |    1    |     LM75A (U81)       |
//!                     |    2    |     LM75A (U82)       |
//!                     |    3    |     LM75A (U83)       |
//!                     |    4    |     LM75A (U84)       |
//!                     |    5    |     LM75A (U85)       |
//!                     |    6    |     ADT7411 (U8)      |
//! @param[out] pTemp  The temperature in degrees Celsius.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMtbTmonRead(_In_ INT slot, _In_ UINT channel, _Out_ double* pTemp);

//! @brief Writes a register within the MAX6650 (fan controller) device on an HBI DPS AIC MFG test board.
//!
//! This function writes a register within the MAX6650 (fan controller) device on an HBI DPS AIC MFG test board.
//!
//! @param[in] slot A physical slot number. Valid values are 0 and 1.
//! @param[in] chip A zero-based chip index. Consult the following table.
//!                 Note that reference designators may change in new fabs.
//!                 |  chip   | Fab A |
//!                 | :-----: | :---: |
//!                 |    0    |  U76  |
//!                 |    1    |  U143 |
//!                 |    2    |  U142 |
//!                 |    3    |  U144 |
//!                 |    4    |  U145 |
//! @param[in] reg Writable MAX6650 register address.
//!                 | Register | Name         | Function              |
//!                 | :------: | :----------: | :-------------------- |
//!                 |   0x00   | SPEED        | Fan speed             |
//!                 |   0x02   | CONFIG       | Configuration         |
//!                 |   0x04   | GPIO DEF     | GPIO definition       |
//!                 |   0x06   | DAC          | DAC                   |
//!                 |   0x08   | ALARM ENABLE | Alarm Enable          |
//!                 |   0x16   | COUNT        | Tachometer count time |
//! @param[in] data The value to write.  Valid values are 0-255 (00h-FFh).
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMtbMax6650Write(_In_ INT slot, _In_ UINT chip, _In_ BYTE reg, _In_ BYTE data);

//! @brief Reads a register within the MAX6650 (fan controller) device on an HBI DPS AIC MFG test board.
//!
//! This function reads a register within the MAX6650 (fan controller) device on an HBI DPS AIC MFG test board.
//!
//! @param[in] slot A physical slot number. Valid values are 0 and 1.
//! @param[in] chip A zero-based chip index. Consult the following table.
//!                 Note that reference designators may change in new fabs.
//!                 |  chip   | Fab A |
//!                 | :-----: | :---: |
//!                 |    0    |  U76  |
//!                 |    1    |  U143 |
//!                 |    2    |  U142 |
//!                 |    3    |  U144 |
//!                 |    4    |  U145 |
//! @param[in] reg  MAX6650 register address.
//!                 | Register | Name         | Function              |
//!                 | :------: | :----------: | :-------------------- |
//!                 |   0x00   | SPEED        | Fan speed             |
//!                 |   0x02   | CONFIG       | Configuration         |
//!                 |   0x04   | GPIO DEF     | GPIO definition       |
//!                 |   0x06   | DAC          | DAC                   |
//!                 |   0x08   | ALARM ENABLE | Alarm Enable          |
//!                 |   0x0A   | ALARM        | Alarm Status          |
//!                 |   0x0C   | TACH0        | Tachometer 0 count    |
//!                 |   0x0E   | TACH1        | Tachometer 1 count    |
//!                 |   0x10   | TACH2        | Tachometer 2 count    |
//!                 |   0x12   | TACH3        | Tachometer 3 count    |
//!                 |   0x14   | GPIO STAT    | GPIO Status           |
//!                 |   0x16   | COUNT        | Tachometer count time |
//! @param[out]     pData The register contents.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMtbMax6650Read(_In_ INT slot, _In_ UINT chip, _In_ BYTE reg, _Out_ PBYTE pData);

//! @brief Reads the specified fan tachometer channel on an HBI DPS AIC MFG test board.
//!
//! This function reads the specified fan tachometer channel on an HBI DPS AIC MFG test board.
//!
//! @param[in] slot A physical slot number. Valid values are 0 and 1.
//! @param[in] fan The fan to read. Consult the following table:
//!                Note that reference designators are for Fab A and may change in future fabs.
//!                 |  fan  |     MAX6650     | Connector |
//!                 | :---: | :------------:  | :-------: |
//!                 |   0   |  U76  F0_TACH   |   J97     |
//!                 |   1   |  U143 F1_TACH   |   J105    |
//!                 |   2   |  U142 F2_TACH   |   J107    |
//!                 |   3   |  U144 F3_TACH   |   J108    |
//!                 |   4   |  U145 F4_TACH   |   J109    |
//! @param[out] pTach   The fan speed in revolutions per second (RPS).
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMtbFanRead(_In_ INT slot, _In_ UINT fan, _Out_ double* pTach);

#ifdef __cplusplus
}
#endif
