// INTEL CONFIDENTIAL
// Copyright 2014-2018 Intel Corporation All Rights Reserved.
//
// The source code contained or described herein and all documents related to
// the source code ("Material") are owned by Intel Corporation or its
// suppliers or licensors. Title to the Material remains with Intel
// Corporation or its suppliers and licensors. The Material contains trade
// secrets and proprietary and confidential information of Intel or its
// suppliers and licensors. The Material is protected by worldwide copyright
// and trade secret laws and treaty provisions. No part of the Material may be
// used, copied, reproduced, modified, published, uploaded, posted,
// transmitted, distributed, or disclosed in any way without Intel's prior
// express written permission.
//
// No license under any patent, copyright, trade secret or other intellectual
// property right is granted to or conferred upon you by disclosure or
// delivery of the Materials, either expressly, by implication, inducement,
// estoppel or otherwise. Any license under such intellectual property rights
// must be express and approved by Intel in writing.

//! @file
//! @brief Non-function definitions such as constants and structures used by the HDMT resource card APIs.

#pragma once

// Simulation constants
#define RC_P2P_SIM_VALUE       0x123456789abcdef0LL //!< Value returned by rcPeerToPeerMemoryBase when simulation is enabled.
#define RC_TRIGGER_SIM_VALUE   0x12345678uL         //!< Value returned by rcTriggerWait in pTriggerStatus when simulation is enabled.
#define RC_TRIGGER32_SIM_VALUE 0x89ABCDEFuL         //!< Value returned by rcTriggerWait in pTrigger32 when simulation is enabled.

//! @brief Alarm bit definitions for alarms returned by rcAlarmWait().
typedef enum RC_ALARMS
{
    RCA_TIU_FORCE_SAFETY_MODE = (1 << 0) //!< TIU has requested to force tester into safety mode.
} RC_ALARMS;

//! @brief These are the status bits that are returned by rcTriggerBusStatus().
typedef enum RC_TRIGGER_BUS_STATUS
{
    RC_TBS_HARD_ERROR   = 1uL << 5, //!< Trigger Bus Hard Error occurred
    RC_TBS_SOFT_ERROR   = 1uL << 4, //!< Trigger Bus Soft Error occurred
    RC_TBS_CHANNEL_DOWN = 1uL << 3, //!< Trigger Bus Channel is down
    RC_TBS_LANE_DOWN    = 1uL << 2, //!< Trigger Bus Lane is down
    RC_TBS_PLL_UNLOCKED = 1uL << 1, //!< Trigger Bus Core PLL is unlocked
    RC_TBS_TRX_UNLOCKED = 1uL << 0  //!< Trigger Bus Transceiver is unlocked
} RC_TRIGGER_BUS_STATUS;

