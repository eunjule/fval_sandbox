// INTEL CONFIDENTIAL
// Copyright 2017-2018 Intel Corporation All Rights Reserved.
//
// The source code contained or described herein and all documents related to
// the source code ("Material") are owned by Intel Corporation or its
// suppliers or licensors. Title to the Material remains with Intel
// Corporation or its suppliers and licensors. The Material contains trade
// secrets and proprietary and confidential information of Intel or its
// suppliers and licensors. The Material is protected by worldwide copyright
// and trade secret laws and treaty provisions. No part of the Material may be
// used, copied, reproduced, modified, published, uploaded, posted,
// transmitted, distributed, or disclosed in any way without Intel's prior
// express written permission.
//
// No license under any patent, copyright, trade secret or other intellectual
// property right is granted to or conferred upon you by disclosure or
// delivery of the Materials, either expressly, by implication, inducement,
// estoppel or otherwise. Any license under such intellectual property rights
// must be express and approved by Intel in writing.

//! @file
//! @brief APIs to control an HDMT HVIL card.
#pragma once
#include "HilDefs.h"
#include "HvilDefs.h"

#ifdef __cplusplus
extern "C" {
#endif

//! @brief Verifies an HDMT HVIL card is present.
//!
//! This function verifies an HDMT HVIL card is present.  It connects to and caches driver resources for use
//! by other \c hvilXXXXX functions.  hvilDisconnect() is its complementary function and frees any cached resources.
//!
//! Neither hvilConnect() or hvilDisconnect() are required to be called to use the other \c hvilXXXXX functions.  All
//! \c hvilXXXXX functions will allocate resources if they are not already allocated and free them if required to
//! perform their function.
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hvilConnect(_In_ INT slot);

//! @brief Frees any resources cached from using the HVIL card functions.
//!
//! This function frees any resources associated with using an HVIL card's HIL functions. hvilConnect() is its
//! complementary function.
//!
//! Neither hvilConnect() or hvilDisconnect() are required to be called to use the other \c hvilXXXXX functions.  All
//! \c hvilXXXXX functions will allocate resources if they are not already allocated and free them if required to
//! perform their function.
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hvilDisconnect(_In_ INT slot);

//! @brief Programs the vendor and product IDs of all USB devices on an HDMT High Voltage Internal Load (HVIL) card.
//!
//! This function programs the vendor and product IDs (VID/PIDs) of the FT240X and Cypress
//! USB devices on an HDMT High Voltage Internal Load (HVIL) card.  Correct VID/PIDs are required to ensure Windows Device
//! Manager displays a proper description of the device, and HIL requires correct VID/PIDs to ensure
//! software can locate and communicate with the correct component.
//! @note FTDI devices have a limited number of updates, but this function is safe to call because it
//! only writes if the component programming is incorrect.
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hvilVidPidsSet(_In_ INT slot);

//! @brief Initializes the components of an HDMT High Voltage Internal Load (HVIL) card for operation.
//!
//! This function initializes the components of an HVIL card for operation.  Calling this
//! function is required before using most other \c hvilXXXXXX() functions.
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hvilInit(_In_ INT slot);

//! @brief Sets the calibration measure selection MUX on an HVIL card.
//!
//! This function sets the calibration measure selection MUX to the selected channel and measurement point on an HVIL card.
//! @param[in] slot       A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] channel    The channel selected for measurement.Valid values are 0-7.
//! @param[in] measureMpm TRUE to measure near the middle of the resistor bank (MPM) for the channel, FALSE to measure near the Xcede connector (MPX).
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hvilMeasureMuxSelect(_In_ INT slot, _In_ UINT channel, _In_ BOOL measureMpm);

//! @brief Reads a rail voltage on an HVIL card.
//!
//! This function reads a rail voltage on an HVIL card.
//! @param[in]  slot     A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in]  rail     The rail number to read.  Valid values are 0-7.
//! @param[out] pVoltage A pointer to double that receives the voltage in volts.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hvilRailVoltageRead(_In_ INT slot, _In_ UINT rail, _Out_ double* pVoltage);

//! @brief Reads a rail current on an HVIL card.
//!
//! This function reads a rail current on an HVIL card.
//! @param[in]  slot     A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in]  rail     The rail number to read.  Valid values are 0-7.
//! @param[out] pCurrent A pointer to double that receives the current in amps.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hvilRailCurrentRead(_In_ INT slot, _In_ UINT rail, _Out_ double* pCurrent);

//! @brief Reads a turbo rail current on an HVIL card.
//!
//! This function reads a turbo rail current on an HVIL card.
//! @param[in]  slot     A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in]  rail     The rail number to read.  Valid values are 0-7.
//! @param[out] pCurrent A pointer to double that receives the turbo current in amps.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hvilTurboRailCurrentRead(_In_ INT slot, _In_ UINT rail, _Out_ double* pCurrent);

//! @brief Sets the load resistor configuration on an HVIL card channel.
//!
//! This function prepares the load resistor configuration on an HVIL card channel.  Call hvilLoadActivate() to load
//! the prepared configuration into one or more channels.
//! @note In order to facilitate load switching within 200ns, set the desired loads on all required channels first,
//!       then call hvilLoadActivate() to actually switch to the new loads.
//! @note The channel must also be enabled.  See hvilLoadChannelEnable().
//! @warning The load values change per fab/channel!
//! @param[in] slot    A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] channel The channel on which the loads are to be prepared. Valid values are 0-7.
//! @param[in] loads   Binary-OR the following values to configure the loads.  The values are approximate in ohms and are provided here for convenience but verify with the schematic as they could change.
//!                    | Load     | Fab A All | Fab B CH0 | Fab B CH1 | Fab B CH2 | Fab B CH3 | Fab B CH4 | Fab B CH5 | Fab B CH6 | Fab B CH7 |
//!                    | :------: | :-------: | :-------: | :-------: | :-------: | :-------: | :-------: | :-------: | :-------: | :-------: |
//!                    |  #HL_R1  | 8         | 8         |  .5       | 8         |  .062     |  .5       | 8         |  .5       | 8         |
//!                    |  #HL_R2  | 4         | 4         |  .062     | 4         |  .124     |  .25      | 4         |  .25      | 4         |
//!                    |  #HL_R3  | 2         | 2         |  .124     | 2         |  .25      |  .124     | 2         |  .124     | 2         |
//!                    |  #HL_R4  | 1         | 1         |  .25      | 1         |  .5       |  .062     | 1         |  .062     | 1         |
//!                    |  #HL_R5  |  .5       |  .03      | 8         |  .062     | 1         | 8         |  .5       | 8         |  .25      |
//!                    |  #HL_R6  |  .25      |  .25      | 1         |  .25      | 2         | 4         |  .25      | 4         |  .124     |
//!                    |  #HL_R7  |  .124     |  .124     | 2         |  .124     | 4         | 2         |  .124     | 2         |  .062     |
//!                    |  #HL_R8  |  .062     |  .062     | 4         |  .5       | 8         | 1         |  .062     | 1         |  .03      |
//!                    |  #HL_R9  |  .03      |  .5       |  .03      |  .03      |  .03      |  .03      |  .03      |  .03      |  .5       |
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hvilLoadControl(_In_ INT slot, _In_ UINT channel, _In_ UINT loads);

//! @brief Activates the load resistors configured on an HVIL card channel.
//!
//! This function activates the load resistor configurations for the selected channels on an HVIL card as prepared by hvilLoadControl().
//! @note In order to facilitate load switching within 200ns, set the desired loads on all required channels first via hvilLoadControl(),
//!       then call this function to actually switch to the new loads.
//! @note The channel must also be enabled.  See hvilLoadChannelEnable().
//! @param[in] slot         A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] channelMask  Bits 0-7 of this mask represent channels 0-7 on an HVIL card.  A set bit indicates that the load
//!                         configuration prepared by hvilLoadControl() on that channel is applied to the channel.  Valid values are 0x01 to 0xFF.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hvilLoadActivate(_In_ INT slot, _In_ UINT channelMask);

//! @brief Selects the master FET or bleeder resistor to be enabled on an HVIL card channel.
//!
//! This function selects the master FET or bleeder resistor to be enabled on an HVIL card channel.
//! @param[in] slot         A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] enable       TRUE - enable the channel (select master FET).  FALSE - disable the channel (select the bleeder resistor).
//! @param[in] channelMask  Bits 0-7 of this mask represent channels 0-7 on an HVIL card.  A set bit indicates to apply the \c enable
//!                         instruction to that channel.  Valid values are 0x01 to 0xFF.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hvilLoadChannelEnable(_In_ INT slot, _In_ BOOL enable, _In_ UINT channelMask);

//! @brief Sets the upper and lower temperature limits for the MAX6627 devices on an HVIL card.
//!
//! This function sets the upper and lower temperature limits for the MAX6627 devices on an HVIL card.
//! @param[in]  slot   A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in]  index  A physical index number. Valid values are 0 to 5.
//!                    |  Index  |          FAB A References          |        FAB B References         |
//!                    | :-----: | :--------------------------------: | :-----------------------------: |
//!                    |    0    | U285 monitors Q3489 thermal diode. | U9 Monitors Q29 thermal diode.  |
//!                    |    1    | U286 monitors Q3490 thermal diode. | U10 Monitors Q30 thermal diode. |
//!                    |    2    | U287 monitors Q3491 thermal diode. | U11 Monitors Q31 thermal diode. |
//!                    |    3    | U288 monitors Q3492 thermal diode. | U12 Monitors Q32 thermal diode. |
//!                    |    4    | U289 monitors Q3493 thermal diode. | U13 Monitors Q33 thermal diode. |
//!                    |    5    | U290 monitors Q3494 thermal diode. | U14 Monitors Q34 thermal diode. |
//! @param[in]  lcl    Lower control limit of the MAX6627 Thermal ADC devices. Valid values are -256 to 255 degrees Celsius.
//! @param[in]  ucl    Upper control limit of the MAX6627 Thermal ADC devices. Valid values are -256 to 255 degrees Celsius.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hvilMax6627LimitsSet(_In_ INT slot, _In_ UINT index, _In_ double lcl, _In_ double ucl);

//! @brief Reads the  upper and lower temperature limits for the MAX6627 devices on an HVIL card.
//!
//! This function reads the upper and lower temperature limits for the MAX6627 devices on the HVIL card.
//! @param[in]  slot   A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in]  index  A physical index number. Valid values are 0 to 5.
//!                    |  Index  |          FAB A References          |        FAB B References         |
//!                    | :-----: | :--------------------------------: | :-----------------------------: |
//!                    |    0    | U285 monitors Q3489 thermal diode. | U9 Monitors Q29 thermal diode.  |
//!                    |    1    | U286 monitors Q3490 thermal diode. | U10 Monitors Q30 thermal diode. |
//!                    |    2    | U287 monitors Q3491 thermal diode. | U11 Monitors Q31 thermal diode. |
//!                    |    3    | U288 monitors Q3492 thermal diode. | U12 Monitors Q32 thermal diode. |
//!                    |    4    | U289 monitors Q3493 thermal diode. | U13 Monitors Q33 thermal diode. |
//!                    |    5    | U290 monitors Q3494 thermal diode. | U14 Monitors Q34 thermal diode. |
//! @param[out] pLcl  The returned lower control limit of the selected MAX6627 device.
//! @param[out] pUcl  The returned upper control limit of the selected Max6627 device.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hvilMax6627LimitsGet(_In_ INT slot, _In_ UINT index, _Out_ double* pLcl, _Out_ double* pUcl);

//! @brief Reads one of seven temperature monitor channels on an HVIL card.
//!
//! This function reads one of the seven temperature monitor channels on an HVIL card.
//! @note hvilInit() is required to be called before using this function (ADT7411 requirement).
//! @param[in] slot    A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] channel Selects the monitoring channel. Valid values are 0 to 6.  The channels
//!                    and corresponding voltages monitored are as per the fab revision):
//!                    |  Index  | Device  |          FAB A References          |        FAB B References         |
//!                    | :-----: | :-----: | :--------------------------------: | :-----------------------------: |
//!                    |    0    | MAX6627 | U285 monitors Q3489 thermal diode. | U9 Monitors Q29 thermal diode.  |
//!                    |    1    | MAX6627 | U286 monitors Q3490 thermal diode. | U10 Monitors Q30 thermal diode. |
//!                    |    2    | MAX6627 | U287 monitors Q3491 thermal diode. | U11 Monitors Q31 thermal diode. |
//!                    |    3    | MAX6627 | U288 monitors Q3492 thermal diode. | U12 Monitors Q32 thermal diode. |
//!                    |    4    | MAX6627 | U289 monitors Q3493 thermal diode. | U13 Monitors Q33 thermal diode. |
//!                    |    5    | MAX6627 | U290 monitors Q3494 thermal diode. | U14 Monitors Q34 thermal diode. |
//!                    |    6    | ADT7411 | U307 internal temperature.         | U307 internal temperature.      |
//! @param[out] pTemp The temperature in degrees Celsius.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hvilTmonRead(_In_ INT slot, _In_ INT channel, _Out_ double* pTemp);

//! @brief Reads one of nine voltage monitor channels on an HVIL card.
//!
//! This function reads one of the nine monitor channels on an HVIL card.
//! @note hvilInit() is required to be called before using this function.
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] channel Selects the monitoring channel. Valid values are 0 to 8.  The channels
//! and corresponding voltages monitored are (per HVIL fab A schematic):
//!                    | Channel | Reference     | Notes                                               |
//!                    | :-----: | :-----------: | :-------------------------------------------------- |
//!                    |    0    | +5V_13A       | VDD for U307 and references for channels 1-8.       |
//!                    |    1    | +1.2VCCINT_5A |                                                     |
//!                    |    2    | +1.5V_5A      |                                                     |
//!                    |    3    | +3.3V_12A     |                                                     |
//!                    |    4    | +5V_13A       | Not accurate due to self reference. Read channel 0. |
//!                    |    5    | +11.6V_150MA  |                                                     |
//!                    |    6    | -11.6V_150MA  |                                                     |
//!                    |    7    | +12V_30A      |                                                     |
//!                    |    8    | -12V_30A      |                                                     |
//! @param[out] pVoltage The voltage in volts.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hvilVmonRead(_In_ INT slot, _In_ INT channel, _Out_ double* pVoltage);

//! @brief Directly writes an 8-bit register on the ADT7411 of an HVIL card.
//!
//! This function writes an 8-bit register on the ADT7411 of an HVIL card.
//! @note hvilInit() is required to be called before using this function.
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] reg  A valid register address for the Analog Devices ADT7411.
//! @param[in] data The data to write.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hvilAdt7411Write(_In_ INT slot, _In_ BYTE reg, _In_ BYTE data);

//! @brief Directly reads an 8-bit register on the ADT7411 of an HVIL card.
//!
//! This function reads an 8-bit register on the ADT7411 of an HVIL card.
//! @note hvilInit() is required to be called before using this function.
//! @param[in] slot   A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] reg    A valid register address for the Analog Devices ADT7411.
//! @param[out] pData The register contents.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hvilAdt7411Read(_In_ INT slot, _In_ BYTE reg, _Out_ PBYTE pData);

//! @brief Sets the high and low voltage comparator limits for the LTC2358 device on an HVIL card.
//!
//! This function sets high and low voltage comparator limits for the LTC2358 device on an HVIL card.
//! @param[in]  slot   A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in]  lsl    Lower safety limit of the LTC2358. Valid values are -0.5V to 5.0V.
//! @param[in]  hsl    Higher safety limit of the LTC2358. Valid values are -0.5V to 5.0V.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hvilVoltageComparatorLimitsSet(_In_ INT slot, _In_ double lsl, _In_ double hsl);

//! @brief Reads the high and low voltage comparator limits of the LTC2358 device on an HVIL card.
//!
//! This function reads the high and low voltage comparator limits of the LTC2358 device on an HVIL card.
//! @param[in]  slot   A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[out] pLsl  The returned lower safety limit of the LTC2358 device.
//! @param[out] pHsl  The returned upper safety limit of the LTC2358 device.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hvilVoltageComparatorLimitsGet(_In_ INT slot, _Out_ double* pLsl, _Out_ double* pHsl);

#ifdef __cplusplus
}
#endif
