// INTEL CONFIDENTIAL
// Copyright 2017-2018 Intel Corporation All Rights Reserved.
//
// The source code contained or described herein and all documents related to
// the source code ("Material") are owned by Intel Corporation or its
// suppliers or licensors. Title to the Material remains with Intel
// Corporation or its suppliers and licensors. The Material contains trade
// secrets and proprietary and confidential information of Intel or its
// suppliers and licensors. The Material is protected by worldwide copyright
// and trade secret laws and treaty provisions. No part of the Material may be
// used, copied, reproduced, modified, published, uploaded, posted,
// transmitted, distributed, or disclosed in any way without Intel's prior
// express written permission.
//
// No license under any patent, copyright, trade secret or other intellectual
// property right is granted to or conferred upon you by disclosure or
// delivery of the Materials, either expressly, by implication, inducement,
// estoppel or otherwise. Any license under such intellectual property rights
// must be express and approved by Intel in writing.

//! @file
//! @brief Non-function definitions such as constants and structures used by the HDMT HVDPS calibration card APIs.

#pragma once

//! @brief Enumeration of the possible general purpose I/O lines on the HVDPS calibration card that can be passed to functions that
//!        have a ::CAL_HVDPS_GPIO argument.
typedef enum CAL_HVDPS_GPIO
{
    CHVG_U3_IO0_0,      // I2C IO EXPANDER Chip #0 (U3)
    CHVG_U3_IO0_1,
    CHVG_U3_IO0_2,
    CHVG_U3_IO0_3,
    CHVG_U3_IO0_4,
    CHVG_U3_IO0_5,
    CHVG_U3_IO0_6,
    CHVG_U3_IO0_7,
    CHVG_U3_IO1_0,
    CHVG_U3_IO1_1,
    CHVG_U3_IO1_2,
    CHVG_U3_IO1_3,
    CHVG_U3_IO1_4,
    CHVG_U3_IO1_5,
    CHVG_U3_IO1_6,
    CHVG_U3_IO1_7,
    CHVG_U3_IO2_0,
    CHVG_U3_IO2_1,
    CHVG_U3_IO2_2,
    CHVG_U3_IO2_3,
    CHVG_U3_IO2_4,
    CHVG_U3_IO2_5,
    CHVG_U3_IO2_6,
    CHVG_U3_IO2_7,
    CHVG_U3_IO3_0,
    CHVG_U3_IO3_1,
    CHVG_U3_IO3_2,
    CHVG_U3_IO3_3,
    CHVG_U3_IO3_4,
    CHVG_U3_IO3_5,
    CHVG_U3_IO3_6,
    CHVG_U3_IO3_7,
    CHVG_U3_IO4_0,
    CHVG_U3_IO4_1,
    CHVG_U3_IO4_2,
    CHVG_U3_IO4_3,
    CHVG_U3_IO4_4,
    CHVG_U3_IO4_5,
    CHVG_U3_IO4_6,
    CHVG_U3_IO4_7,
    CHVG_U11_IO0_0,     // I2C IO EXPANDER Chip #1 (U11)
    CHVG_U11_IO0_1,
    CHVG_U11_IO0_2,
    CHVG_U11_IO0_3,
    CHVG_U11_IO0_4,
    CHVG_U11_IO0_5,
    CHVG_U11_IO0_6,
    CHVG_U11_IO0_7,
    CHVG_U11_IO1_0,
    CHVG_U11_IO1_1,
    CHVG_U11_IO1_2,
    CHVG_U11_IO1_3,
    CHVG_U11_IO1_4,
    CHVG_U11_IO1_5,
    CHVG_U11_IO1_6,
    CHVG_U11_IO1_7,
    CHVG_U11_IO2_0,
    CHVG_U11_IO2_1,
    CHVG_U11_IO2_2,
    CHVG_U11_IO2_3,
    CHVG_U11_IO2_4,
    CHVG_U11_IO2_5,
    CHVG_U11_IO2_6,
    CHVG_U11_IO2_7,
    CHVG_U11_IO3_0,
    CHVG_U11_IO3_1,
    CHVG_U11_IO3_2,
    CHVG_U11_IO3_3,
    CHVG_U11_IO3_4,
    CHVG_U11_IO3_5,
    CHVG_U11_IO3_6,
    CHVG_U11_IO3_7,
    CHVG_U11_IO4_0,
    CHVG_U11_IO4_1,
    CHVG_U11_IO4_2,
    CHVG_U11_IO4_3,
    CHVG_U11_IO4_4,
    CHVG_U11_IO4_5,
    CHVG_U11_IO4_6,
    CHVG_U11_IO4_7,
    CHVG_U1_IO0_0,      // I2C IO EXPANDER Chip #2 (U1)
    CHVG_U1_IO0_1,
    CHVG_U1_IO0_2,
    CHVG_U1_IO0_3,
    CHVG_U1_IO0_4,
    CHVG_U1_IO0_5,
    CHVG_U1_IO0_6,
    CHVG_U1_IO0_7,
    CHVG_U1_IO1_0,
    CHVG_U1_IO1_1,
    CHVG_U1_IO1_2,
    CHVG_U1_IO1_3,
    CHVG_U1_IO1_4,
    CHVG_U1_IO1_5,
    CHVG_U1_IO1_6,
    CHVG_U1_IO1_7,
    CHVG_U1_IO2_0,
    CHVG_U1_IO2_1,
    CHVG_U1_IO2_2,
    CHVG_U1_IO2_3,
    CHVG_U1_IO2_4,
    CHVG_U1_IO2_5,
    CHVG_U1_IO2_6,
    CHVG_U1_IO2_7,
    CHVG_U1_IO3_0,
    CHVG_U1_IO3_1,
    CHVG_U1_IO3_2,
    CHVG_U1_IO3_3,
    CHVG_U1_IO3_4,
    CHVG_U1_IO3_5,
    CHVG_U1_IO3_6,
    CHVG_U1_IO3_7,
    CHVG_U1_IO4_0,
    CHVG_U1_IO4_1,
    CHVG_U1_IO4_2,
    CHVG_U1_IO4_3,
    CHVG_U1_IO4_4,
    CHVG_U1_IO4_5,
    CHVG_U1_IO4_6,
    CHVG_U1_IO4_7
} CAL_HVDPS_GPIO;
