// INTEL CONFIDENTIAL
// Copyright 2014-2019 Intel Corporation All Rights Reserved.
//
// The source code contained or described herein and all documents related to
// the source code ("Material") are owned by Intel Corporation or its
// suppliers or licensors. Title to the Material remains with Intel
// Corporation or its suppliers and licensors. The Material contains trade
// secrets and proprietary and confidential information of Intel or its
// suppliers and licensors. The Material is protected by worldwide copyright
// and trade secret laws and treaty provisions. No part of the Material may be
// used, copied, reproduced, modified, published, uploaded, posted,
// transmitted, distributed, or disclosed in any way without Intel's prior
// express written permission.
//
// No license under any patent, copyright, trade secret or other intellectual
// property right is granted to or conferred upon you by disclosure or
// delivery of the Materials, either expressly, by implication, inducement,
// estoppel or otherwise. Any license under such intellectual property rights
// must be express and approved by Intel in writing.

//! @file
//! @brief APIs to control the HDMT resource card.
#pragma once
#include "HilDefs.h"
#include "RcDefs.h"

#ifdef __cplusplus
extern "C" {
#endif

//! @brief Initializes the components of the resource card for operation.
//!
//! This function initializes the components of the resource card for operation.  Calling this
//! function is required before using most other \c rcXXXXXX() functions.
//! This function also calls rcRootI2cLock() and rcRootI2cUnlock() during its execution since it accesses
//! devices on the root I2c bus.  If additional I2C commands need to be atomically executed on the root I2C
//! bus, then manually call rcRootI2cLock() and rcRootI2cUnlock() around the entire root I2C bus access.
//!
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rcInit();

//! @brief Verifies the HDMT resource card is present.
//!
//! This function verifies the HDMT resource card is present.  It connects to and caches driver resources for use
//! by other \c rcXXXXX functions.  rcDisconnect() is its complementary function and frees any cached resources.
//!
//! Neither rcConnect() or rcDisconnect() are required to be called to use the other \c rcXXXXX functions.  All
//! \c rcXXXXX functions will allocate resources if they are not already allocated and free them if required to
//! perform their function.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rcConnect(void);

//! @brief Frees any resources cached from using the resource card functions.
//!
//! This function frees any resources associated with using the resource card HIL functions. rcConnect() is its
//! complementary function.
//!
//! Neither rcConnect() or rcDisconnect() are required to be called to use the other \c rcXXXXX functions.  All
//! \c rcXXXXX functions will allocate resources if they are not already allocated and free them if required to
//! perform their function.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rcDisconnect(void);

//! @brief Programs the vendor and product IDs of all USB devices on the resource card.
//!
//! This function programs the vendor and product IDs (VID/PIDs) of the FT4232 and Cypress
//! USB devices on the resource card.  Correct VID/PIDs are required to ensure Windows Device
//! Manager displays a proper description of the device, and HIL requires correct VID/PIDs to ensure
//! software can locate and communicate with the correct component.
//! @note FTDI devices have a limited number of updates, but this function is safe to call because it
//! only writes if the component programming is incorrect.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rcVidPidsSet(void);

//! @brief Reads the resource card FPGA version number.
//!
//! This function reads the FPGA version number from the resource card.  The returned 32-bit value is in the
//! format \c 0xMMmmpppp where \c MM is the major version, \c mm is the minor version, and \c pppp is a patch number.
//!
//! For example, 0x05010002 is version 5.1.2.
//! @param[out] pVersion The address of a DWORD of memory.  It may not be NULL.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rcFpgaVersion(_Out_ LPDWORD pVersion);

//! @brief Reads the resource card CPLD version number.
//!
//! This function reads the CPLD version number from the resource card.
//! @note The -402 revision of the resource card removed resistors required for JTAG operation for a noise fix and does not work with
//!       this function.  -403 restored the resistors.
//!
//! @param[out] pVersion Returns the version register value.  The pointer must not be NULL.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rcCpldVersion(_Out_ LPDWORD pVersion);

//! @brief Reads the resource card's CPLD version string.
//!
//! This function reads the version register of the resource card's CPLD and converts it to an ASCII string representation.
//! @note The -402 revision of the resource card removed resistors required for JTAG operation for a noise fix and does not work with
//!       this function.  -403 restored the resistors.
//!
//! @param[out] pVersion Returns the version string.  The pointer must not be NULL.
//! @param[in]  length   The length of the \c pVersion buffer.  It should be at least five characters to hold a null-terminated version string.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rcCpldVersionString(_Out_writes_z_(length) LPSTR pVersion, _In_ DWORD length);

//! @brief Disables the PCI device driver for the resource card.
//!
//! This function disables the PCI device driver for the resource card.  It should be called before using functions
//! such as rcFpgaLoad() that affect the hardware used by the drivers.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rcDeviceDisable(void);

//! @brief Enables the PCI device driver for the resource card.
//!
//! This function enables the PCI device driver for the resource card.  It should be called after using functions
//! such as rcFpgaLoad() that affect the hardware used by the drivers.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rcDeviceEnable(void);

//! @brief Loads an FPGA binary image file into the resource card FPGA.
//!
//! This function loads an FPGA binary image file into the resource card FPGA.  This image is volatile and the FPGA
//! will load its original base program from its SPI FLASH after a cold reboot.
//! @warning The resource card device driver is sensitive to FPGA changes.    If present, wrap this call in rcDeviceDisable() and rcDeviceEnable().
//! @param[in] filename An ANSI string containing an absolute or relative (to the current directory) FPGA binary filename.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rcFpgaLoad(_In_z_ LPCSTR filename);

//! @brief Programs an FPGA image file into the base SPI FLASH used to boot initialize the resource card FPGA.
//!
//! This function programs an FPGA image file into the base SPI FLASH used to boot initialize the resource card FPGA.
//!
//! The callback and context parameters are used to report progress back during a long operation.  See #HIL_BASE_LOAD_CALLBACK
//! for details.
//! @param[in] filename An ANSI string containing an absolute or relative (to the current directory) FPGA binary filename.
//! @param[in] callback The callback function to be called with progress updates while programming the SPI FLASH.  Value can be NULL.
//! @param[in] context The context that will be passed back as an argument in the #HIL_BASE_LOAD_CALLBACK callback function.  Value can be NULL.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rcFpgaBaseLoad(_In_z_ LPCSTR filename, _In_opt_ HIL_BASE_LOAD_CALLBACK callback, _In_opt_ void* context);

//! @brief Compares an existing image within the base SPI FLASH on the resource card against an FPGA binary image file.
//!
//! This function compares an existing image within the base SPI FLASH on the resource card against an FPGA binary image file
//! and optionally dumps that image into a separate binary file.
//! @param[in] imageFilename An ANSI string containing an absolute or relative (to the current directory) FPGA binary filename to be compared against.
//! @param[in] dumpFilename An ANSI string containing an absolute or relative (to the current directory) binary dump filename.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rcFpgaBaseVerify(_In_z_ LPCSTR imageFilename, _In_opt_z_ LPCSTR dumpFilename);

//! @brief Executes a Xilinx .XSVF file over the resource card's JTAG interface.
//!
//! This function executes a Xilinx .XSVF file over the resource card's JTAG interface.  A Xilinx CPLD
//! and the FPGA are in the scan chain.  An appropriate .XSVF file can be generated through Xilinx's iMPACT software to perform
//! functions such as programming and/or verifying the content of the CPLD.
//! @warning The resource card device driver is not sensitive to changing the CPLD firmware, but if the FPGAs are targeted and the
//!          driver is present, wrap this call in rcDeviceDisable() and rcDeviceEnable().
//! @note The -402 revision of the resource card removed resistors required for JTAG operation for a noise fix and does not work with
//!       this function.  -403 restored the resistors.
//! @param[in] filename An ANSI string containing an absolute or relative (to the current directory) .XSVF binary filename.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rcJtagExecute(_In_z_ LPCSTR filename);

//! @brief Loads an FPGA binary image buffer into the resource card FPGA.
//!
//! This function loads an FPGA binary image buffer into the resource card FPGA.  This image is volatile and the FPGA
//! will load its original base program from its SPI FLASH after a cold reboot.
//! @warning The resource card device driver is sensitive to FPGA changes.    If present, wrap this call in rcDeviceDisable() and rcDeviceEnable().
//! @param[in] pData The FPGA image data.
//! @param[in] length The length of the FPGA image data in bytes.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rcFpgaBufferLoad(_In_reads_bytes_(length) LPCVOID pData, _In_ DWORD length);

//! @brief Programs an FPGA image buffer into the base SPI FLASH used to boot initialize the resource card FPGA.
//!
//! This function programs an FPGA image buffer into the base SPI FLASH used to boot initialize the resource card FPGA.
//!
//! The callback and context parameters are used to report progress back during a long operation.  See #HIL_BASE_LOAD_CALLBACK
//! for details.
//! @param[in] pData The FPGA image data.
//! @param[in] length The length of the FPGA image data in bytes.
//! @param[in] callback The callback function to be called with progress updates while programming the SPI FLASH.  Value can be NULL.
//! @param[in] context The context that will be passed back as an argument in the #HIL_BASE_LOAD_CALLBACK callback function.  Value can be NULL.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rcFpgaBaseBufferLoad(_In_reads_bytes_(length) LPCVOID pData, _In_ DWORD length, _In_opt_ HIL_BASE_LOAD_CALLBACK callback, _In_opt_ void* context);

//! @brief Execute a buffer containing Xilinx XSVF data over the resource card's JTAG interface.
//!
//! This function executes a Xilinx XSVF data buffer over the resource card's JTAG interface.  A Xilinx CPLD
//! and the FPGA are in the scan chain.  XSVF data can be generated through Xilinx's iMPACT software to perform
//! functions such as programming and/or verifying the content of the CPLD.
//! @warning The resource card device driver is not sensitive to changing the CPLD firmware, but if the FPGAs are targeted and the
//!          driver is present, wrap this call in rcDeviceDisable() and rcDeviceEnable().
//! @note The -402 revision of the resource card removed resistors required for JTAG operation for a noise fix and does not work with
//!       this function.  -403 restored the resistors.
//! @param[in] pData The XSVF image data.
//! @param[in] length The length of the XSVF image data in bytes.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rcJtagBufferExecute(_In_reads_bytes_(length) LPCVOID pData, _In_ DWORD length);

//! @brief Reads a 32-bit register in one of the BAR regions of the resource card.
//!
//! This function reads a 32-bit register in one of the BAR regions of the resource card.
//! @param[in] bar The PCI base address register region to access.  Valid values are 0-1.
//! @param[in] offset The DWORD-aligned offset address in BAR0 memory to read.
//! @param[out] pData The address of a DWORD to contain the result.  It cannot be NULL.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rcBarRead(_In_ UINT bar, _In_ DWORD offset, _Out_ LPDWORD pData);

//! @brief Writes a 32-bit register in one of the BAR regions of the resource card.
//!
//! This function writes a 32-bit register in one of the BAR regions of the resource card.
//! @param[in] bar The PCI base address register region to access.  Valid values are 0-1.
//! @param[in] offset The DWORD-aligned offset address in BAR0 memory to write.
//! @param[in] data The DWORD value to write.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rcBarWrite(_In_ UINT bar, _In_ DWORD offset, _In_ DWORD data);

//! @brief Reads from the 8KB SPI instruction memory region of the resource card.
//!
//! The resource card has a single, small 8KB device memory region used hold instructions used
//! to access various SPI buses on the resource card.  This function is used to read directly from
//! that small instruction area.  Instructions are 64-bit, so the \c address and \c length parameters
//! should be 8-byte-aligned and their sum must not exceed 8KB, or \ref HS_INVALID_PARAMETER will be returned.
//!
//! Prefer to use the higher-level \c rcQueueXXXXX functions instead of rcDmaRead() and rcDmaWrite() directly.
//! @param[in] address The zero-based address within the 8KB region to read.
//! @param[out] pData The address of a buffer to hold the result.
//! @param[in] length The length of the \c pData buffer in bytes.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rcDmaRead(_In_ INT64 address, _Out_writes_bytes_all_(length) LPVOID pData, _In_ DWORD length);

//! @brief Writes to the 8KB SPI instruction memory region of the resource card.
//!
//! The resource card has a single, small 8KB device memory region used hold instructions used
//! to access various SPI buses on the resource card.  This function is used to write directly to
//! that small instruction area.  Instructions are 64-bit, so the \c address and \c length parameters
//! should be 8-byte-aligned and their sum must not exceed 8KB, or \ref HS_INVALID_PARAMETER will be returned.
//! @param[in] address The zero-based address within the 8KB region to write.
//! @param[in] pData The address of a buffer containing the data to write.
//! @param[in] length The length of the \c pData buffer in bytes.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rcDmaWrite(_In_ INT64 address, _In_reads_bytes_(length) const void* pData, _In_ DWORD length);

//! @brief Clears the 8KB SPI instruction queue.
//!
//! This function clears any existing but uncommitted SPI instructions in the resource card's SPI instruction queue.
HIL_API void rcQueueClear(void);

//! @brief Adds an SPI instruction to the 8KB SPI instruction queue.
//!
//! This function adds an SPI instruction to the resource card's SPI instruction queue.  To prevent hand-coding an
//! instruction, use the \c rcEncodeXXXXX set of functions.  For example, rcPlutoInstructionEncode().
//!
//! There is a maximum of 1023 instructions.  Appending more than that without an intervening rcQueueExecute() will
//! return \ref HS_UNSUCCESSFUL.
//! @param[in] instruction A encoded 64-bit SPI instruction.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rcQueueAppend(_In_ UINT64 instruction);

//! @brief Executes the instructions in the resource card's SPI instruction queue.
//!
//! This function executes SPI instructions queued by rcQueueAppend() and clears the queue.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rcQueueExecute(void);  // TODO: Handle feedback from committed reads.

// TODO: "address" can be broken down into "area" (per_pin, register, central), "channel" and "resource" according to Pluto spec.  Would probably be more useful to break out.
//! @brief Returns an encoded SPI instruction accessing the resource card PMU devices.
//!
//! This function encodes an SPI instruction to access the 8-channel Pluto 2 device on the resource card.
//! @param[in] isWrite Specifies whether the SPI access is a read or a write.  TRUE for write, FALSE for read.
//! @param[in] isParallelWrite Specifies whether the SPI access is to one channel or all channels.
//! @param[in] address See the Pluto 2 specification for addressing details.
//! @param[in] data    See the Pluto 2 specification for addressing details.
//! @returns A 64-bit encoded SPI instruction suitable for passing to rcQueueAppend().
HIL_API UINT64 rcPlutoInstructionEncode(_In_ BOOL isWrite, _In_ BOOL isParallelWrite, _In_ WORD address, _In_ WORD data);

//! @brief Enables the hardware interrupt for resource card software triggers.
//!
//! This function enables the hardware interrupt for resource card software triggers.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rcTriggerEnable(void);

//! @brief Disables the hardware interrupt for resource card software triggers.
//!
//! This function disables the hardware interrupt for resource card software triggers.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rcTriggerDisable(void);

//! @brief Waits for an resource card software trigger interrupt.
//!
//! This function waits for an resource card software trigger interrupt and returns with the trigger value.
//!
//! This function does not return until a trigger interrupt is received.  Make sure to enable the trigger interrupt via rcTriggerEnable()
//! before calling this function.  Since the function is blocking, the user should call it from a worker thread.  The wait can be canceled
//! via another thread by calling rcTriggerWaitCancel().
//!
//! @param[out] pTriggerStatus The trigger status register of the resource card.  Bit 31 indicates support for 32-bit triggers.  If 0, 16-bit trigger is in bits 15-0.
//! @param[out] pTrigger32     If non-NULL on input, contains 32-bit trigger if bit 0 of \c *pTriggerStatus is a 1; otherwise, undefined.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rcTriggerWait(_Out_ LPDWORD pTriggerStatus, _Out_opt_ LPDWORD pTrigger32);

//! @brief Cancels a pending rcTriggerWait().
//!
//! This function cancels a pending rcTriggerWait().  Since rcTriggerWait() is blocking, this must be performed from a separate thread of execution.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rcTriggerWaitCancel(void);

//! @brief Retrieves the resource card peer-to-peer base physical memory address.
//!
//! This function retrieves the resource card peer-to-peer base physical memory address required for DPS peer-to-peer capability.
//! It is present for testing only and will likely be removed when DPS peer-to-peer functionality is complete in HIL.
//!
//! @param[out] pPhysicalAddress The physical base address of the resource card peer-to-peer memory area.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rcPeerToPeerMemoryBase(_Out_ PLARGE_INTEGER pPhysicalAddress);

//! @brief Writes a 27-bit data register of the LMK04808 device on the resource card.
//!
//! This function writes a 27-bit data register of the Texas Instruments LMK04808 device on the resource card.
//! @note The recommended procedure is to program register 0 - 31 in sequence.  See rcLmk04808FullWrite().
//!
//! @param[in] address The address of the register to write.  Valid values are 0-31.
//! @param[in] data This is the data value to write.  Valid values are 0-0x7FFFFFF.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rcLmk04808Write(_In_ DWORD address, _In_ DWORD data);

//! @brief Writes all 32 of the LMK04808 device registers on the resource card in sequence.
//!
//! This function writes all 32 of the LMK04808 device registers on the resource card in sequence.
//! \c pData must point to a 32-element DWORD array where the value at each offset is the 27-bit data to program
//! the corresponding device register in the LMK04808.
//!
//! Note that registers 17-23 do not exist and the values in the corresponding \c pData offsets are ignored.
//!
//! @param[in] pData A pointer to a 32-element DWORD array.  Each element contains the 27-bit value
//!                  to write to the associated register in the LMK04808 on the resource card.
//! @param[in] length The length in DWORDs of \c pData.  It is currently required to be 32.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rcLmk04808FullWrite(_In_ const DWORD* pData, _In_ DWORD length);

//! @brief Reads the 27-bit data from a register of the LMK04808 device on the resource card.
//!
//! This function reads the 27-bit data from a register of the LMK04808 device on the resource card.
//!
//! @param[in] address The address of the register to read.  Valid values are 0-31.
//! @param[out] pData  The address of a DWORD that returns the 27-bit data.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rcLmk04808Read(_In_ DWORD address, _Out_ LPDWORD pData);

//! @brief Checks the PLL lock status of the LMK04808 clock device on the resource card.
//!
//! This function checks the PLL lock status of the LMK04808 clock device on the resource card.
//! It should be called after programming the clock to verify the clock is running properly.
//!
//! @param[out] pIsLocked TRUE if the PLL is locked, FALSE otherwise.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rcLmk04808PllIsLocked(_Out_ BOOL* pIsLocked);

//! @brief Performs a core reset on the specified group of Aurora cores that are used to connect the resource card
//!        to the trigger bus.
//!
//! This function performs a core reset on the specified group of Aurora cores that are used to connect the resource card
//! to the trigger bus.  Note that the associated Aurora transceivers should be reset using rcTriggerBusTrxReset()
//! before the Aurora core is reset with this function.
//!
//! | Group ID | Cores Reset          |
//! | :------: | :--------------------|
//! | 0        | Slot 0-3 Core Reset  |
//! | 1        | Slot 4-7 Core Reset  |
//! | 2        | Slot 8-11 Core Reset |
//!
//! @param[in] group Specifies which group of slots to perform the core reset on.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rcTriggerBusCoreReset(_In_ INT group);

//! @brief Writes a 32-bit command to the AD5064 DAC device on the resource card.
//!
//! This function writes a 32-bit command to the AD5064 DAC device on the resource card.
//! See the Analog Devices AD5064 data sheet for the command format.
//!
//! @param[in] data The 32-bit command to write.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rcAd5064Write(_In_ DWORD data);

//! @brief Directly reads an 8-bit register on an ADT7411 on the resource card.
//!
//! This function reads an 8-bit register on an ADT7411 on the resource card.  This function
//! is for debugging only.  Prefer to use other specialized \c rcXXXXX functions instead.
//! @param[in] chip A zero-based chip index.  Consult the following table.
//!                 Note that reference designators may change in new fabs.
//!                 | Chip    | Fab D |
//!                 | :-----: | :---: |
//!                 |    0    | U91   |
//!                 |    1    | U12   |
//! @param[in] reg A valid register address for the Analog Devices ADT7411.
//! @param[out] pData The register contents.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rcAdt7411Read(_In_ INT chip, _In_ BYTE reg, _Out_ PBYTE pData);

//! @brief Reads one of seventeen voltage monitor channels on the resource card.
//!
//! This function reads one of seventeen voltage monitor channels on the resource card.
//! @note rcInit() is required to be called before using this function.
//! @param[in] channel Selects the monitoring channel. Valid values are 0 to 17.  The channels
//! and corresponding voltages monitored are (per resource card fab D schematic):
//!                    | Channel |  Chip  | Reference                | Notes                      |
//!                    | :-----: |  :---: | :----------------------: | :------------------------- |
//!                    | 0       |  U91   | 5.0V_12A                 |                            |
//!                    | 1       |  U91   | 3.3V_12A                 |                            |
//!                    | 2       |  U91   | 3.3V_1A                  |                            |
//!                    | 3       |  U91   | 2.5V_12A                 |                            |
//!                    | 4       |  U91   | 1.8V_300MA               |                            |
//!                    | 5       |  U91   | 1.0V_12A                 |                            |
//!                    | 6       |  U91   | 1.2V_12A                 |                            |
//!                    | 7       |  U91   | 2.5V_12A                 |                            |
//!                    | 8       |  U91   | -3.3V_5A                 |                            |
//!                    | 9       |  U12   | 5.0V_12A                 |                            |
//!                    | 10      |  U12   | PECI1_VREF_R             | DAC6573(U122) VOUTA output |
//!                    | 11      |  U12   | PECI5_VREF_R             | DAC6573(U129) VOUTA output |
//!                    | 12      |  U12   | V_CNTL_Y9_R              | AD5064(U19) VOUT_A output  |
//!                    | 13      |  U12   | +3.3V_LMK04808_VCX0_1A_R |                            |
//!                    | 14      |  U12   | +3.3V_LMK04808_PLL_1A_R  |                            |
//!                    | 15      |  U12   | 3.3V_CLK_R               |                            |
//!                    | 16      |  U12   | +3.3V_PMU_DAC_R          |                            |
//!                    | 17      |  U12   | -5V_20MA_R               |                            |
//! @param[out] pVoltage The voltage in volts.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rcVmonRead(_In_ INT channel, _Out_ double* pVoltage);

//! @brief Reads one of three temperature monitor channels on the resource card.
//!
//! This function reads one of three temperature monitor channels on the resource card.
//! //!
//!
//! @note rcInit() is required to be called before using this function.
//! @param[in] channel Selects the monitoring channel. Valid values are 0 to 2.
//!                    The channels and corresponding temperatures are (per resource card fab D schematic):
//!                    | Channel | Description                       |
//!                    | :-----: | :-------------------------------- |
//!                    | 0       | ADT7411(U91) Internal Temperature |
//!                    | 1       | ADT7411(U12) Internal Temperature |
//!                    | 2       | FPGA(U94) Internal Temperature    |
//! @param[out] pTemp The temperature in degrees Celsius.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rcTmonRead(_In_ INT channel, _Out_ double* pTemp);

//! @brief **[DEPRECATED]** Disables or enables the tester failsafe timer.
//!
//! This function disables or enables the tester failsafe timer.
//! @warning DEPRECATED! Use hilFailsafeDisable().
//! @note rcInit() is required to be called before using this function.
//! @param[in] disable \c TRUE disables the timer.  \c FALSE enables it.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rcFailsafeDisable(_In_ BOOL disable);

//! @brief **[DEPRECATED]** Sets the failsafe timer count value.
//!
//! This function sets the failsafe timer count value.  Once a time is set rcFailsafePing() must be called
//! periodically before the timer expires or the tester will shut down.
//! @warning DEPRECATED! Use hilFailsafeTimerSet().
//! @note rcInit() is required to be called before using this function.
//! @param[in] seconds The count value in seconds.  Valid values are 0 to 1023.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rcFailsafeTimerSet(_In_ WORD seconds);

//! @brief **[DEPRECATED]** Resets the failsafe timer count.
//!
//! This function resets the failsafe timer to the count set by rcFailsafeTimerSet().
//! If the timer expires the tester shuts down, so calling this function periodically keeps the tester powered up.
//! @warning DEPRECATED! Use hilFailsafePing().
//! @note rcInit() is required to be called before using this function.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rcFailsafePing(void);

//! @brief Directly writes an 8-bit register on an ADT7411 on the resource card.
//!
//! This function writes an 8-bit register on an ADT7411 on the resource card.  This function
//! is for debugging only.  Prefer to use other specialized \c rcXXXXX functions instead.
//! @param[in] chip A zero-based chip index.  Consult the following table.
//!                 Note that reference designators may change in new fabs.
//!                 | Chip    | Fab D |
//!                 | :-----: | :---: |
//!                 |    0    | U91   |
//!                 |    1    | U12   |
//! @param[in] reg A valid register address for the Analog Devices ADT7411.
//! @param[in] data The data to write.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rcAdt7411Write(_In_ INT chip, _In_ BYTE reg, _In_ BYTE data);

//! @brief Performs a transceiver reset on the specified Aurora transceiver that is used to connect the resource card
//!        to the trigger bus on a given slot.
//!
//! This function performs a transceiver reset on the specified Aurora transceiver that is used to connect the resource card
//! to the trigger bus on a given slot.  Note that the Aurora cores should also be reset using rcTriggerBusCoreReset()
//! after all the associated transceivers for a give core have been reset.
//!
//! | Transceiver ID | Transceiver Reset         |
//! | :------------: | :------------------------ |
//! | 0              | Slot 0 Transceiver Reset  |
//! | 1              | Slot 1 Transceiver Reset  |
//! | 2              | Slot 2 Transceiver Reset  |
//! | 3              | Slot 3 Transceiver Reset  |
//! | 4              | Slot 4 Transceiver Reset  |
//! | 5              | Slot 5 Transceiver Reset  |
//! | 6              | Slot 6 Transceiver Reset  |
//! | 7              | Slot 7 Transceiver Reset  |
//! | 8              | Slot 8 Transceiver Reset  |
//! | 9              | Slot 9 Transceiver Reset  |
//! | 10             | Slot 10 Transceiver Reset |
//! | 11             | Slot 11 Transceiver Reset |
//!
//! @param[in] transceiver Specifies which Aurora transceiver in the resource card to perform the transceiver reset on.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rcTriggerBusTrxReset(_In_ INT transceiver);

//! @brief Reads the status of the Aurora link that is used to connect the RC at the specified slot to the trigger bus.
//!
//! This function reads the status of the Aurora link that is used to connect the RC at the specified slot to the trigger bus.
//!
//! @param[in] slot A physical slot number.  Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[out] pStatus See the #RC_TRIGGER_BUS_STATUS encoding for all possible status that can be returned.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rcTriggerBusStatus(_In_ INT slot, _Out_ LPDWORD pStatus);

//! @brief Sends a trigger to all the tester slots.
//!
//! This function sends the specified trigger to all the tester slots.  Note that all triggers are broadcasted
//! in the tester, so it is impossible to send a trigger to only one tester slot.
//!
//! @param[in] trigger The trigger value that is sent to all the tester slots.  Currently the trigger must be 16-bits
//!                    long.  Trying to send a trigger with any of the upper 16-bits set returns an error.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rcTriggerSend(_In_ DWORD trigger);

//! @brief Sends a Trigger Sync pulse to the specified tester slots
//!
//! This function sends a Trigger Sync pulse to the specified tester slots.  Note that any combination of tester
//! slots can be specified using the #HIL_SLOT_BITS encoding.  See example below.
//!
//! \par Sending Trigger Sync pulse to slots 0, 1, and 11 - C
//! \code{.c}
//!  WORD slots = HSB_S0 | HSB_S1 | HSB_S11;
//!  rcTriggerSyncSend(slots);
//! \endcode
//!
//! @param[in] slots Bitwise OR of the physical HDMT Tester slots to send the trigger sync pulse to.  This
//!                  parameter should be passed values from the #HIL_SLOT_BITS encoding.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rcTriggerSyncSend(_In_ WORD slots);

//! @brief Enables or disables TIU alarm monitoring.
//!
//! This function enables or disables TIU alarm monitoring.  Supported alarms are defined in the #RC_ALARMS enumeration.
//!
//! @param[in] enable TRUE to enable alarms, FALSE to disable.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rcTiuAlarmEnable(_In_ BOOL enable);

//! @brief Blocks execution until a resource card alarm is received.
//!
//! This function blocks execution until a resource card alarm is received.  Supported alarms are defined in the #RC_ALARMS enumeration.
//! This function can be unblocked by calling rcAlarmWaitCancel() in another thread of execution.
//!
//! @param[out] pAlarms Reported alarms are returned as bits set in this output DWORD.  See #RC_ALARMS for bit definitions.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rcAlarmWait(_Out_ LPDWORD pAlarms);

//! @brief Aborts the blocking rcAlarmWait() function.
//!
//! This function aborts the blocking rcAlarmWait() function.
//!
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rcAlarmWaitCancel();

//! @brief Locks access to the root I2C bus to allow only the current process's thread to send I2C commands on the root I2C bus.
//!
//! This function locks access to the root I2C bus to allow only the current process's thread to send I2C commands
//! on the root I2C bus.  Other threads (including threads in other processes) calling HIL functions that use the root I2C
//! bus will be blocked until rcRootI2cUnlock() is called.
//!
//! The lock is recursive, so the same thread can call this function multiple times without blocking.
//! The lock is not released until a matching number of rcRootI2cUnlock() calls are made.
//!
//! The root I2C bus originates on the RCTC2 (resource card rev 2) and spans out to other core instruments in the
//! tester such as the front panel, backplane, and power supplies.
//!
//! @note While the RCTC2 has a single I2C bus that muxes to four I2C paths (RCTC2, TIU, backplane, and front panel),
//!       the RCTC3 (resource card rev 3) has four individual buses.  For backward compatibility, this API works
//!       with RCTC3 and locks access to all four buses as well.
//!
//! @param[in] msTimeout The number of milliseconds to wait for the lock.  #HS_TIMEOUT is
//!                      returned if the lock is not acquired within the timeout period.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rcRootI2cLock(_In_ DWORD msTimeout);

//! @brief Releases one rcRootI2cLock() call.
//!
//! This function releases one rcRootI2cLock() call and allows other threads (including threads in other
//! processes) to access the root I2C bus when all the calls to rcRootI2cLock() have been released.
//!
//! The lock is recursive, so the same thread can call rcRootI2cLock() multiple times without blocking.
//! The lock is not released until a matching number of calls to this function are made.
//!
//! The root I2C bus originates on the RCTC2 (resource card rev 2) and spans out to other core instruments in the
//! tester such as the front panel, backplane, and power supplies.
//!
//! @note While the RCTC2 has a single I2C bus that muxes to four I2C paths (RCTC2, TIU, backplane, and front panel),
//!       the RCTC3 (resource card rev 3) has four individual buses.  For backward compatibility, this API works
//!       with RCTC3 and unlocks access to all four buses as well.
//!
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rcRootI2cUnlock();

//! @brief Routes the root I2C bus traffic to a selected device path.
//!
//! This function routes the root I2C bus traffic to a selected device path.
//! For efficiency, an internal cache remembers the MUX setting and will only send the command
//! if the cache indicates the requested path is not already set.  It is possible for external programs to
//! modify the hardware without HIL's knowledge.  If this is suspected, call rcRootI2cCacheInvalidate() to
//! invalidate the cache.
//!
//! This function calls rcRootI2cLock() and rcRootI2cUnlock() during its execution.  If additional
//! I2C commands require this MUX selection to be maintained, manually call rcRootI2cLock() and
//! rcRootI2cUnlock() around the entire root I2C bus access.
//!
//! @note While this API is intended to control the RCTC2 (resource card rev 2) I2C mux, for backward
//!       compatibility this API works on RCTC3 (resource card rev 3) as well.  The mux doesn't exist,
//!       but HIL remembers the selection so rcRootI2cRead() and rcRootI2cWrite() target the correct
//!       bus on RCTC3.
//!
//! @param[in] selection The index of the I2C path being requested from the table below.
//! | selection | Destination   |
//! | :-------: | :------------ |
//! |     0     | Front Panel   |
//! |     1     | Backplane     |
//! |     2     | TIU           |
//! |     3     | Resource Card |
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rcRootI2cMuxSelect(_In_ INT selection);

//! @brief Writes raw byte data to an addressed device on the root I2C bus.
//!
//! This function writes raw byte data to an addressed device on the root I2C bus.  It calls rcRootI2cLock()
//! and rcRootI2cUnlock() during its execution.  If the target I2C address is behind an I2C MUX or switch,
//! manually call rcRootI2cLock() and rcRootI2cUnlock() around the entire root I2C bus access to prevent
//! another HIL-based process or thread from interfering with the transaction.
//!
//! @note Along with rcRootI2cLock(), rcRootI2cUnlock() and rcRootI2cMuxSelect(); this function is compatible
//!       with both RCTC2 and RCTC3 (resource card rev 3).  The lock/unlock functions are used to control access to RCTC3's
//!       four I2c buses, and mux select chooses between the four separate I2C buses to which this function will write.
//!
//! @param[in] address The I2C address of the target device.
//! @param[in] pData The address of a buffer containing the data bytes to write.
//! @param[in] length The length of the \c pData buffer in bytes.
//! @returns \ref HIL_STATUS
//! @see rcRootI2cLock\n
//!      rcRootI2cUnlock\n
//!      rcRootI2cMuxSelect\n
HIL_API HIL_STATUS rcRootI2cWrite(_In_ BYTE address, _In_reads_bytes_(length) const void* pData, _In_ DWORD length);

//! @brief Reads raw byte data from an addressed device on the root I2C bus.
//!
//! This function reads raw byte data from an addressed device on the root I2C bus.  It calls rcRootI2cLock()
//! and rcRootI2cUnlock() during its execution.  If the target I2C address is behind an I2C MUX or switch,
//! manually call rcRootI2cLock() and rcRootI2cUnlock() around the entire root I2C bus access to prevent
//! another HIL-based process or thread from interfering with the transaction.
//!
//! @note Along with rcRootI2cLock(), rcRootI2cUnlock() and rcRootI2cMuxSelect(); this function is compatible
//!       with both RCTC2 and RCTC3 (resource card rev 3).  The lock/unlock functions are used to control access to RCTC3's
//!       four I2c buses, and mux select chooses between the four separate I2C buses to which this function will read.
//!
//! @param[in] address The I2C address of the target device.
//! @param[out] pData The address of a buffer to hold the result read from the device.
//! @param[in] length The length of the \c pData buffer in bytes.
//! @returns \ref HIL_STATUS
//! @see rcRootI2cLock\n
//!      rcRootI2cUnlock\n
//!      rcRootI2cMuxSelect\n
HIL_API HIL_STATUS rcRootI2cRead(_In_ BYTE address, _Out_writes_bytes_all_(length) LPVOID pData, _In_ DWORD length);

//! @brief Combines a byte write with a read of one or more data bytes from a device on the root I2C bus.
//!
//! This function combines a byte write with a read of one or more data bytes from a device on the root I2C bus.  Note
//! that this function is required for devices that do not allow an I2C stop to be sent between an I2C write and I2C read.
//! This function calls rcRootI2cLock() and rcRootI2cUnlock() during its execution.  If the target I2C address is behind
//! an I2C MUX or switch, manually call rcRootI2cLock() and rcRootI2cUnlock() around the entire root I2C bus access to prevent
//! another HIL-based process or thread from interfering with the transaction.
//!
//! @note Along with rcRootI2cLock(), rcRootI2cUnlock() and rcRootI2cMuxSelect(); this function is compatible
//!       with both RCTC2 and RCTC3 (resource card rev 3).  The lock/unlock functions are used to control access to RCTC3's
//!       four I2c buses, and mux select chooses between the four separate I2C buses to which this function will access a device register.
//!
//! @param[in] address The I2C address of the target device.
//! @param[in] registerAddress The register address to read from.  Note that this is the data used in the byte write before the read.
//! @param[out] pData The address of a buffer to hold the result read from the device.
//! @param[in] length The length of the \c pData buffer in bytes.
//! @returns \ref HIL_STATUS
//! @see rcRootI2cLock\n
//!      rcRootI2cUnlock\n
//!      rcRootI2cMuxSelect\n
HIL_API HIL_STATUS rcRootI2cRegisterRead(_In_ BYTE address, _In_ BYTE registerAddress, _Out_writes_bytes_all_(length) LPVOID pData, _In_ DWORD length);

//! @brief Initializes the root I2C bus for use.
//!
//! This function initializes the root I2C bus for use.  This initialization includes allocating the resources
//! necessary to acquire the lock that is used within rcRootI2cLock() and rcRootI2cUnlock().  Because of this,
//! rcRootI2cInit() cannot be called within a rcRootI2cLock() / rcRootI2cUnlock() access block.  Also note that
//! this function invalidates the internal root I2C cache (see rcRootI2cCacheInvalidate() for details).
//!
//! The root I2C bus originates on the RCTC2 (resource card rev 2) and spans out to other core instruments in the
//! tester such as the front panel, backplane, and power supplies.
//!
//! @note For compatibility, this function works with an RCTC3 (resource card rev 3) as well.
//!
//! @param[in] msTimeout The number of milliseconds to wait for the lock.  #HS_TIMEOUT is
//!                      returned if the lock is not acquired within the timeout period.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rcRootI2cInit(_In_ DWORD msTimeout);

//! @brief Invalidates the internal root I2C cache.
//!
//! This function invalidates the internal root I2C cache so that the next command sent to a MUX or switch will program
//! the MUX or switch and cache its state correctly.
//!
//! The root I2C bus originates on the RCTC2 (resource card rev 2) and spans out to other core instruments in the
//! tester such as the front panel, backplane, and power supplies.
//!
//! @note For compatibility, this function works with an RCTC3 (resource card rev 3) as well.
//!
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rcRootI2cCacheInvalidate();

//! @brief Reads the board-level traceability values from the resource card.
//!
//! This function reads the board-level traceability values from the resource card.  The values are defined in the #BLT structure.
//!
//! @param[out] pBlt The address of a #BLT structure that will be populated with the data.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rcBltBoardRead(_Out_ PBLT pBlt);

//! @brief Writes the board-level traceability values of the resource card.
//!
//! This function writes the board-level traceability values of the resource card.  The values are defined in the #BLT structure.
//!
//! The \c flags field allows this function to update individual values.  The flags can be ORed together.  Only the fields indicated by
//! \c flags need to be populated in the provided \c pBlt parameter.
//!
//! \par Updating the Full BLT - C
//! \code{.c}
//! #include <Windows.h>
//! #include "RcApi.h"
//! #include "string.h"
//!
//! int main()
//! {
//!     BLT blt;
//!     HIL_STATUS status;
//!     blt.Id = 0x0100001f;
//!     strcpy(blt.DeviceName,"DeviceName");
//!     strcpy(blt.VendorName,"INTEL");
//!     strcpy(blt.PartNumberAsBuilt,"AAxxxxxx-xxx");
//!     strcpy(blt.PartNumberCurrent,blt.PartNumberAsBuilt);
//!     strcpy(blt.SerialNumber,"12345678");
//!     strcpy(blt.ManufactureDate,"YYMMDD");
//!     status = rcBltBoardWrite(&blt, BF_ALL);
//!     if (status != HS_SUCCESS)
//!         return -1;
//!     return 0;
//! }
//! \endcode
//! \par Updating the Current Part Number - Python
//! \code{.py}
//! blt = hil.BLT()
//! blt.PartNumberCurrent = 'AAyyyyyy-yyy'
//! hil.rcBltBoardWrite(blt,hil.BF_PART_NUMBER_CURRENT)
//! # Note Python returns None for HIL_STATUS if successful and raises RuntimeError on failure.
//! \endcode
//!
//! \note HS_BLT_NOT_FOUND can be returned on an update (lack of #BF_ALL flag).
//!
//! @param[in] pBlt The address of a #BLT structure that contains the data to be written or updated.
//! @param[in] flags #BLT_FLAGS bits indicating what fields in \c pBlt are valid.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rcBltBoardWrite(_In_ PCBLT pBlt, _In_ BLT_FLAGS flags);

//! @brief Reads bytes from the resource card 24LC512 EEPROM.
//!
//! This function reads bytes from the resource card 24LC512 EEPROM.  It is a 64KB device, accessed using
//! I2C address 0xAE.
//!
//! @param[in]  address The address to begin reading from the EEPROM.  Valid values are 0x0000-0xFFFF.
//! @param[out] pData   The output buffer.  It cannot be NULL.
//! @param[in]  length  The amount of data to read.  \c address plus \c length cannot exceed 0x10000 (64KB).
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rcEepromRead(_In_ UINT address, _Out_writes_bytes_all_(length) LPVOID pData, _In_ DWORD length);

//! @brief Writes bytes to the resource card 24LC512 EEPROM.
//!
//! This function writes bytes to the resource card 24LC512 EEPROM.  It is a 64KB device, accessed using
//! I2C address 0xAC.
//!
//! @param[in]  address The address to begin writing to the EEPROM.  Valid values are 0x0000-0xFFFF.
//! @param[in]  pData   The input buffer.  It cannot be NULL.
//! @param[in]  length  The amount of data to write.  \c address plus \c length cannot exceed 0x2000 (64KB).
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rcEepromWrite(_In_ UINT address, _In_reads_bytes_(length) const void* pData, _In_ DWORD length);

//! @brief Reads the system identifier from the resource card.
//!
//! This function reads the system identifier from the resource card.
//!
//! @param[out] pSysId A buffer for the returned system ID.  It cannot be NULL.  A buffer of #HIL_MAX_ID_STRING_SIZE is guaranteed
//!                    to be large enough to hold the identifier.
//! @param[in] length  The size of the \c pSysId buffer in bytes.
//! @retval HS_SUCCESS The system ID was returned successfully.
//! @retval HS_INSUFFICIENT_BUFFER The \c length of the \c pSysId buffer was insufficient to hold the result.
HIL_API HIL_STATUS rcSysIdRead(_Out_writes_z_(length) LPSTR pSysId, _In_ DWORD length);

//! @brief Writes a system identifier to the resource card.
//!
//! This function writes a system identifier to the resource card.
//!
//! @param[in] pSysId  A pointer to a null-terminated system ID string no longer than #HIL_MAX_ID_STRING_SIZE including the null.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rcSysIdWrite(_In_z_ LPCSTR pSysId);

//! @brief Reads the material equipment system identifier from the resource card.
//!
//! This function reads the material equipment system(MES) identifier from the resource card.
//!
//! @param[out] pMesId A buffer for the returned MES ID.  It cannot be NULL.  A buffer of #HIL_MAX_ID_STRING_SIZE is guaranteed
//!                    to be large enough to hold the identifier.
//! @param[in] length  The size of the \c pMesId buffer in bytes.
//! @retval HS_SUCCESS The MES ID was returned successfully.
//! @retval HS_INSUFFICIENT_BUFFER The \c length of the \c pMesId buffer was insufficient to hold the result.
HIL_API HIL_STATUS rcMesIdRead(_Out_writes_z_(length) LPSTR pMesId, _In_ DWORD length);

//! @brief Writes a material equipment system identifier to the resource card.
//!
//! This function writes a material equipment system(MES) identifier to the resource card.
//!
//! @param[in] pMesId  A pointer to a null-terminated MES ID string no longer than #HIL_MAX_ID_STRING_SIZE including the null.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rcMesIdWrite(_In_z_ LPCSTR pMesId);

#ifdef __cplusplus
}
#endif
