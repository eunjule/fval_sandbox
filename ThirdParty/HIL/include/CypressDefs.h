// INTEL CONFIDENTIAL
// Copyright 2014-2018 Intel Corporation All Rights Reserved.
//
// The source code contained or described herein and all documents related to
// the source code ("Material") are owned by Intel Corporation or its
// suppliers or licensors. Title to the Material remains with Intel
// Corporation or its suppliers and licensors. The Material contains trade
// secrets and proprietary and confidential information of Intel or its
// suppliers and licensors. The Material is protected by worldwide copyright
// and trade secret laws and treaty provisions. No part of the Material may be
// used, copied, reproduced, modified, published, uploaded, posted,
// transmitted, distributed, or disclosed in any way without Intel's prior
// express written permission.
//
// No license under any patent, copyright, trade secret or other intellectual
// property right is granted to or conferred upon you by disclosure or
// delivery of the Materials, either expressly, by implication, inducement,
// estoppel or otherwise. Any license under such intellectual property rights
// must be express and approved by Intel in writing.

//! @file
//! @brief Non-function definitions such as constants and structures used by the Cypress USB APIs.

#pragma once

//! @brief Allowable ports for \c cypPortXxxxx calls.
typedef enum CYP_PORT
{
    CP_PORTA   = 0,
    CP_PORTB   = 1,
    CP_PORTD   = 2,
    CP_IFCFG   = 3,
    CP_RDYSTAT = 4, //!< read-only
    CP_CTLCFG  = 5  //!< write-only
} CYP_PORT;

//! I2C status values returned from cypI2cStatus().
typedef enum CYP_I2C_STATUS
{
    I2C_SUCCESS   = 0, //!< I2C operation successful.
    I2C_TIMEOUT   = 1, //!< I2C operation timed out.
    I2C_NOACK     = 2, //!< I2C operation has not yet acknowledged completion.
    I2C_BUS_ERROR = 3, //!< There was an I2C bus error reported.
    I2C_NOT_READY = 4  //!< The I2C bus is not ready for an operation.
} CYP_I2C_STATUS;

#define CYP_VIDPID_GENERIC    0x04B48613u //!< Default Cypress
#define CYP_VIDPID_RC         0x8087F026u //!< Resource Card
#define CYP_VIDPID_HDDPS_LC   0x8087F02Au //!< Gen2 Low Current Motherboard DPS
#define CYP_VIDPID_HDDPS_HC   0x8087F02Bu //!< Gen2 High Current Daughterboard DPS
#define CYP_VIDPID_HPCC_DC    0x8087F02Cu //!< Gen2 Channel Card for DC FPGA
#define CYP_VIDPID_HPCC_AC    0x8087F02Du //!< Gen2 Channel Card for AC FPGA
#define CYP_VIDPID_HDDPS_CAL  0x80870A42u //!< Gen2 DPS calibration board.
#define CYP_VIDPID_CALBASE2   0x80870A44u //!< Gen2 calibration TIU baseboard
#define CYP_VIDPID_TDAUBANK   0x80870A6Au //!< TDAU Bank
#define CYP_VIDPID_HVDPS      0x80870B0Eu //!< HVDPS card
#define CYP_VIDPID_HVDPS_CAL  0x80870B12u //!< HVDPS calibration board
#define CYP_VIDPID_HVIL       0x80870B10u //!< HVIL card
