// INTEL CONFIDENTIAL
// Copyright 2019 Intel Corporation All Rights Reserved.
//
// The source code contained or described herein and all documents related to
// the source code ("Material") are owned by Intel Corporation or its
// suppliers or licensors. Title to the Material remains with Intel
// Corporation or its suppliers and licensors. The Material contains trade
// secrets and proprietary and confidential information of Intel or its
// suppliers and licensors. The Material is protected by worldwide copyright
// and trade secret laws and treaty provisions. No part of the Material may be
// used, copied, reproduced, modified, published, uploaded, posted,
// transmitted, distributed, or disclosed in any way without Intel's prior
// express written permission.
//
// No license under any patent, copyright, trade secret or other intellectual
// property right is granted to or conferred upon you by disclosure or
// delivery of the Materials, either expressly, by implication, inducement,
// estoppel or otherwise. Any license under such intellectual property rights
// must be express and approved by Intel in writing.

//! @file
//! @brief APIs to control HBI TIU and Diagnostic Test Board.

#pragma once
#include "HilDefs.h"

#ifdef __cplusplus
extern "C" {
#endif

//! @brief Writes the board-level traceability values of an HBI TIU.
//!
//! This function writes the board-level traceability values of an HBI TIU.  The values are defined in the #BLT structure.
//!
//! The \c flags field allows this function to update individual values.  The flags can be ORed together.  Only the fields indicated by
//! \c flags need to be populated in the provided \c pBlt parameter.
//!
//! \par Updating the Full BLT - C
//! \code{.c}
//! #include <Windows.h>
//! #include "HbiTiuApi.h"
//! #include "string.h"
//!
//! int main()
//! {
//!     BLT blt;
//!     HIL_STATUS status;
//!     blt.Id = 0x010001F4;
//!     strcpy(blt.DeviceName,"HBITIU");
//!     strcpy(blt.VendorName,"INTEL");
//!     strcpy(blt.PartNumberAsBuilt,"AAxxxxxx-xxx");
//!     strcpy(blt.PartNumberCurrent,blt.PartNumberAsBuilt);
//!     strcpy(blt.SerialNumber,"12345678");
//!     strcpy(blt.ManufactureDate,"YYMMDD");
//!     status = hbiTiuBltBoardWrite(0, 0, &blt, BF_ALL);
//!     if (status != HS_SUCCESS)
//!         return -1;
//!     return 0;
//! }
//! \endcode
//! \par Updating the Current Part Number - Python
//! \code{.py}
//! blt = hil.BLT()
//! blt.PartNumberCurrent = 'AAyyyyyy-yyy'
//! tiuIndex = 0
//! eepromIndex = 0
//! hil.hbiTiuBltBoardWrite(tiuIndex, eepromIndex, blt, hil.BF_PART_NUMBER_CURRENT)
//! # Note Python returns None for HIL_STATUS if successful and raises RuntimeError on failure.
//! \endcode
//!
//! \note HS_BLT_NOT_FOUND can be returned on an update (lack of #BF_ALL flag).
//!
//! @param[in] tiuIndex    A zero-based TIU index as illustrated below. Valid values are 0-7.
//! \image html hbi_tiu_config.png
//! @param[in] eepromIndex Index of the EEPROM to write.  Valid values are 0-7.
//!                        Consult the following table for 8-bit I2C address corresponding to an eepromIndex.
//!                        | eepromIndex | 8-bit I2C address |
//!                        | :---------: | :---------------: |
//!                        |      0      |       0xA0        |
//!                        |      1      |       0xA2        |
//!                        |      2      |       0xA4        |
//!                        |      3      |       0xA6        |
//!                        |      4      |       0xA8        |
//!                        |      5      |       0xAA        |
//!                        |      6      |       0xAC        |
//!                        |      7      |       0xAE        |
//!                        Note that 0 is required to be present. Others are optional and will return #HS_I2C_NOACK if not present.
//! @param[in] pBlt The address of a #BLT structure that contains the data to be written or updated.
//! @param[in] flags #BLT_FLAGS bits indicating what fields in \c pBlt are valid.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiTiuBltBoardWrite(_In_ UINT tiuIndex, _In_ UINT eepromIndex, _In_ PCBLT pBlt, _In_ BLT_FLAGS flags);

//! @brief Reads the board-level traceability values from an HBI TIU.
//!
//! This function reads the board-level traceability values from an HBI TIU.  The values are defined in the #BLT structure.
//!
//! @param[in] tiuIndex    A zero-based TIU index as illustrated in hbiTiuBltBoardWrite(). Valid values are 0-7.
//! @param[in] eepromIndex Index of the EEPROM to write.  Valid values are 0-7.
//!                        Consult the following table for 8-bit I2C address corresponding to an eepromIndex.
//!                        | eepromIndex | 8-bit I2C address |
//!                        | :---------: | :---------------: |
//!                        |      0      |       0xA0        |
//!                        |      1      |       0xA2        |
//!                        |      2      |       0xA4        |
//!                        |      3      |       0xA6        |
//!                        |      4      |       0xA8        |
//!                        |      5      |       0xAA        |
//!                        |      6      |       0xAC        |
//!                        |      7      |       0xAE        |
//!                        Note that 0 is required to be present. Others are optional and will return #HS_I2C_NOACK if not present.
//! @param[out] pBlt The address of a #BLT structure that will be populated with the data.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiTiuBltBoardRead(_In_ UINT tiuIndex, _In_ UINT eepromIndex, _Out_ PBLT pBlt);

//! @brief Writes data to a 24LC512 (EEPROM) device on an HBI TIU.
//!
//! This function writes data to upto eight 24LC512 (EEPROM) devices on an HBI TIU.
//!
//! @note See specialized \c hbiDtbXXXXX functions to access 24LC512 device(s) on an HBI Diagnostic Test Board (DTB).
//!
//! @param[in] tiuIndex    A zero-based TIU index as illustrated in hbiTiuBltBoardWrite(). Valid values are 0-7.
//! @param[in] eepromIndex Index of the EEPROM to write.  Valid values are 0-7.
//!                        Consult the following table for 8-bit I2C address corresponding to an eepromIndex.
//!                        | eepromIndex | 8-bit I2C address |
//!                        | :---------: | :---------------: |
//!                        |      0      |       0xA0        |
//!                        |      1      |       0xA2        |
//!                        |      2      |       0xA4        |
//!                        |      3      |       0xA6        |
//!                        |      4      |       0xA8        |
//!                        |      5      |       0xAA        |
//!                        |      6      |       0xAC        |
//!                        |      7      |       0xAE        |
//!                        Note that 0 is required to be present. Others are optional and will return #HS_I2C_NOACK if not present.
//! @param[in] address     The location of the first byte in the EEPROM to which the data in the \c pData buffer will be written.
//! @param[in] pData       A data buffer containing data to write to the EEPROM device.
//! @param[in] length      The byte length of the \c pData buffer.
//! @returns \ref HIL_STATUS
//! @see hbiDtbEepromWrite()
HIL_API HIL_STATUS hbiTiuEepromWrite(_In_ UINT tiuIndex, _In_ UINT eepromIndex, _In_ DWORD address, _In_reads_bytes_(length) const void* pData, _In_ DWORD length);

//! @brief Reads data from a 24LC512 (EEPROM) device on an HBI TIU.
//!
//! This function reads data from upto eight 24LC512 (EEPROM) devices on an HBI TIU.
//!
//! @note See specialized \c hbiDtbXXXXX functions to access 24LC512 device(s) on an HBI Diagnostic Test Board (DTB).
//!
//! @param[in] tiuIndex    A zero-based TIU index as illustrated in hbiTiuBltBoardWrite(). Valid values are 0-7.
//! @param[in] eepromIndex Index of the EEPROM to write.  Valid values are 0-7.
//!                        Consult the following table for 8-bit I2C address corresponding to an eepromIndex.
//!                        | eepromIndex | 8-bit I2C address |
//!                        | :---------: | :---------------: |
//!                        |      0      |       0xA0        |
//!                        |      1      |       0xA2        |
//!                        |      2      |       0xA4        |
//!                        |      3      |       0xA6        |
//!                        |      4      |       0xA8        |
//!                        |      5      |       0xAA        |
//!                        |      6      |       0xAC        |
//!                        |      7      |       0xAE        |
//!                        Note that 0 is required to be present. Others are optional and will return #HS_I2C_NOACK if not present.
//! @param[in] address     The location of the first byte in the EEPROM from which the data will be read.
//! @param[out] pData      A pointer to a data buffer in which the read data will be stored. Note that this buffer must have at least \c length
//!                        bytes allocated in order to complete successfully.
//! @param[in] length      The number of bytes to read from the EEPROM.
//! @returns \ref HIL_STATUS
//! @see hbiDtbEepromRead()
HIL_API HIL_STATUS hbiTiuEepromRead(_In_ UINT tiuIndex, _In_ UINT eepromIndex, _In_ DWORD address, _Out_writes_bytes_all_(length) LPVOID pData, _In_ DWORD length);

//! @brief Reads the extended board-level traceability values from a TIU.
//!
//! This function reads the extended board-level traceability values from a TIU.  The values are defined in the #BLTEX structure.
//! @note In Python, extra methods are exposed in the #BLTEX structure for updating fields.  See #BLTEX for details.
//!
//! @param[in] tiuIndex    A zero-based TIU index as illustrated in hbiTiuBltBoardWrite(). Valid values are 0-7.
//! @param[in] eepromIndex Index of the EEPROM to write.  Valid values are 0-7.
//!                        Consult the following table for 8-bit I2C address corresponding to an eepromIndex.
//!                        | eepromIndex | 8-bit I2C address |
//!                        | :---------: | :---------------: |
//!                        |      0      |       0xA0        |
//!                        |      1      |       0xA2        |
//!                        |      2      |       0xA4        |
//!                        |      3      |       0xA6        |
//!                        |      4      |       0xA8        |
//!                        |      5      |       0xAA        |
//!                        |      6      |       0xAC        |
//!                        |      7      |       0xAE        |
//!                        Note that 0 is required to be present. Others are optional and will return #HS_I2C_NOACK if not present.
//! @param[out] pBltEx     The address of a #BLTEX structure that will be populated with the data.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiTiuBltExRead(_In_ UINT tiuIndex, _In_ UINT eepromIndex, _Out_ PBLTEX pBltEx);

//! @brief Writes the extended board-level traceability values of a TIU.
//!
//! This function writes the extended board-level traceability values of a TIU.  The values are defined in the #BLTEX structure.
//! @note In Python, extra methods are exposed in the #BLTEX structure for updating fields.  See #BLTEX for details.
//!
//! @param[in] tiuIndex    A zero-based TIU index as illustrated in hbiTiuBltBoardWrite(). Valid values are 0-7.
//! @param[in] eepromIndex Index of the EEPROM to write.  Valid values are 0-7.
//!                        Consult the following table for 8-bit I2C address corresponding to an eepromIndex.
//!                        | eepromIndex | 8-bit I2C address |
//!                        | :---------: | :---------------: |
//!                        |      0      |       0xA0        |
//!                        |      1      |       0xA2        |
//!                        |      2      |       0xA4        |
//!                        |      3      |       0xA6        |
//!                        |      4      |       0xA8        |
//!                        |      5      |       0xAA        |
//!                        |      6      |       0xAC        |
//!                        |      7      |       0xAE        |
//!                        Note that 0 is required to be present. Others are optional and will return #HS_I2C_NOACK if not present.
//! @param[in] pBltEx      The address of a #BLTEX structure that contains the data to be written.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiTiuBltExWrite(_In_ UINT tiuIndex, _In_ UINT eepromIndex, _In_ PCBLTEX pBltEx);

//! @brief Reads extended counter values from a TIU.
//!
//! This function reads extended counter values from a TIU.  The values are defined in the #BLTEX_COUNTERS structure.
//! @note In Python, extra methods are exposed in the #BLTEX_COUNTERS structure for updating fields.  See #BLTEX_COUNTERS for details.
//!
//! @param[in] tiuIndex    A zero-based TIU index as illustrated in hbiTiuBltBoardWrite(). Valid values are 0-7.
//! @param[in] eepromIndex Index of the EEPROM to write.  Valid values are 0-7.
//!                        Consult the following table for 8-bit I2C address corresponding to an eepromIndex.
//!                        | eepromIndex | 8-bit I2C address |
//!                        | :---------: | :---------------: |
//!                        |      0      |       0xA0        |
//!                        |      1      |       0xA2        |
//!                        |      2      |       0xA4        |
//!                        |      3      |       0xA6        |
//!                        |      4      |       0xA8        |
//!                        |      5      |       0xAA        |
//!                        |      6      |       0xAC        |
//!                        |      7      |       0xAE        |
//!                        Note that 0 is required to be present. Others are optional and will return #HS_I2C_NOACK if not present.
//! @param[in] counterIndex    Index of the counter record to read.  There are 10 counter records available.  Valid indexes are 0-9.
//! @param[out] pBltExCounters The address of a #BLTEX_COUNTERS structure that will be populated with the data.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiTiuBltExCountersRead(_In_ UINT tiuIndex, _In_ UINT eepromIndex, _In_ UINT counterIndex, _Out_ PBLTEX_COUNTERS pBltExCounters);

//! @brief Writes extended counter values of a TIU.
//!
//! This function writes extended counter values of a TIU.  The values are defined in the #BLTEX_COUNTERS structure.
//! @note In Python, extra methods are exposed in the #BLTEX_COUNTERS structure for updating fields.  See #BLTEX_COUNTERS for details.
//!
//! @param[in] tiuIndex    A zero-based TIU index as illustrated in hbiTiuBltBoardWrite(). Valid values are 0-7.
//! @param[in] eepromIndex Index of the EEPROM to write.  Valid values are 0-7.
//!                        Consult the following table for 8-bit I2C address corresponding to an eepromIndex.
//!                        | eepromIndex | 8-bit I2C address |
//!                        | :---------: | :---------------: |
//!                        |      0      |       0xA0        |
//!                        |      1      |       0xA2        |
//!                        |      2      |       0xA4        |
//!                        |      3      |       0xA6        |
//!                        |      4      |       0xA8        |
//!                        |      5      |       0xAA        |
//!                        |      6      |       0xAC        |
//!                        |      7      |       0xAE        |
//!                        Note that 0 is required to be present. Others are optional and will return #HS_I2C_NOACK if not present.
//! @param[in] counterIndex   Index of the counter record to write.  There are 10 counter records available.  Valid indexes are 0-9.
//! @param[in] pBltExCounters The address of a #BLTEX_COUNTERS structure that contains the data to be written.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiTiuBltExCountersWrite(_In_ UINT tiuIndex, _In_ UINT eepromIndex, _In_ UINT counterIndex, _In_ PCBLTEX_COUNTERS pBltExCounters);

//! @brief Reads cycle counter values from a TIU.
//!
//! This function reads cycle counter values from a TIU.  The values are defined in the #BLTCY_COUNTERS structure.
//!
//! @param[in] tiuIndex    A zero-based TIU index as illustrated in hbiTiuBltBoardWrite(). Valid values are 0-7.
//! @param[in] eepromIndex Index of the EEPROM to write.  Valid values are 0-7.
//!                        Consult the following table for 8-bit I2C address corresponding to an eepromIndex.
//!                        | eepromIndex | 8-bit I2C address |
//!                        | :---------: | :---------------: |
//!                        |      0      |       0xA0        |
//!                        |      1      |       0xA2        |
//!                        |      2      |       0xA4        |
//!                        |      3      |       0xA6        |
//!                        |      4      |       0xA8        |
//!                        |      5      |       0xAA        |
//!                        |      6      |       0xAC        |
//!                        |      7      |       0xAE        |
//!                        Note that 0 is required to be present. Others are optional and will return #HS_I2C_NOACK if not present.
//!                        By spec cycle counters are only allowed on EEPROM 0, but allowing multiples for consistency.
//! @param[out] pBltCyCounters The address of a #BLTCY_COUNTERS structure that will be populated with the data.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiTiuBltCyCountersRead(_In_ UINT tiuIndex, _In_ UINT eepromIndex, _Out_ PBLTCY_COUNTERS pBltCyCounters);

//! @brief Writes cycle counter values of a TIU.
//!
//! This function writes cycle counter values of a TIU.  The values are defined in the #BLTCY_COUNTERS structure.
//!
//! @param[in] tiuIndex    A zero-based TIU index as illustrated in hbiTiuBltBoardWrite(). Valid values are 0-7.
//! @param[in] eepromIndex Index of the EEPROM to write.  Valid values are 0-7.
//!                        Consult the following table for 8-bit I2C address corresponding to an eepromIndex.
//!                        | eepromIndex | 8-bit I2C address |
//!                        | :---------: | :---------------: |
//!                        |      0      |       0xA0        |
//!                        |      1      |       0xA2        |
//!                        |      2      |       0xA4        |
//!                        |      3      |       0xA6        |
//!                        |      4      |       0xA8        |
//!                        |      5      |       0xAA        |
//!                        |      6      |       0xAC        |
//!                        |      7      |       0xAE        |
//!                        Note that 0 is required to be present. Others are optional and will return #HS_I2C_NOACK if not present.
//!                        By spec cycle counters are only allowed on EEPROM 0, but allowing multiples for consistency.
//! @param[in] pBltCyCounters The address of a #BLTEX_COUNTERS structure that contains the data to be written.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiTiuBltCyCountersWrite(_In_ UINT tiuIndex, _In_ UINT eepromIndex, _In_ PCBLTCY_COUNTERS pBltCyCounters);

//! @brief Writes an 8-bit register of a PCA9505 device on an HBI DTB.
//!
//! This function writes an 8-bit register of upto eight PCA9505 devices on an HBI Diagnostic Test Board.
//!
//! @note See specialized \c hbiDtbXXXXX functions to access PCA9505 device(s) on an HBI Diagnostic Test Board (DTB).
//!
//! @param[in] tiuIndex  A zero-based TIU index as illustrated in hbiTiuBltBoardWrite(). Valid values are 0-7.
//! @param[in] gpioIndex Index of the EEPROM to write.  Valid values are 0-7.
//!                      Consult the following table for 8-bit I2C address corresponding to a gpioIndex.
//!                      | gpioIndex | 8-bit I2C address |
//!                      | :-------: | :---------------: |
//!                      |     0     |       0x40        |
//!                      |     1     |       0x42        |
//!                      |     2     |       0x44        |
//!                      |     3     |       0x46        |
//!                      |     4     |       0x48        |
//!                      |     5     |       0x4A        |
//!                      |     6     |       0x4C        |
//!                      |     7     |       0x4E        |
//!                      Note that 0 is required to be present. Others are optional and will return #HS_I2C_NOACK if not present.
//! @param[in] reg       A valid register address for the PCA9505 device.
//! @param[in] data      The value to write.
//! @returns \ref HIL_STATUS
//! @see hbiDtbPca9505Write()
HIL_API HIL_STATUS hbiTiuPca9505Write(_In_ UINT tiuIndex, _In_ UINT gpioIndex, _In_ BYTE reg, _In_ BYTE data);

//! @brief Reads an 8-bit register of a PCA9505 device on an HBI DTB.
//!
//! This function reads an 8-bit register of upto eight PCA9505 devices on an HBI Diagnostic Test Board.
//!
//! @note See specialized \c hbiDtbXXXXX functions to access PCA9505 device(s) on an HBI Diagnostic Test Board (DTB).
//!
//! @param[in] tiuIndex  A zero-based TIU index as illustrated in hbiTiuBltBoardWrite(). Valid values are 0-7.
//! @param[in] gpioIndex Index of the EEPROM to write.  Valid values are 0-7.
//!                      Consult the following table for 8-bit I2C address corresponding to a gpioIndex.
//!                      | gpioIndex | 8-bit I2C address |
//!                      | :-------: | :---------------: |
//!                      |     0     |       0x40        |
//!                      |     1     |       0x42        |
//!                      |     2     |       0x44        |
//!                      |     3     |       0x46        |
//!                      |     4     |       0x48        |
//!                      |     5     |       0x4A        |
//!                      |     6     |       0x4C        |
//!                      |     7     |       0x4E        |
//!                      Note that 0 is required to be present. Others are optional and will return #HS_I2C_NOACK if not present.
//! @param[in] reg       A valid register address for the PCA9505 device.
//! @param[out] pData    The register contents read from the device.
//! @returns \ref HIL_STATUS
//! @see hbiDtbPca9505Read()
HIL_API HIL_STATUS hbiTiuPca9505Read(_In_ UINT tiuIndex, _In_ UINT gpioIndex, _In_ BYTE reg, _Out_ PBYTE pData);

//! @brief Writes the board-level traceability values of an HBI Diagnostic Test Board (DTB).
//!
//! This function writes the board-level traceability values of an HBI Diagnostic Test Board (DTB).  The values are defined in the #BLT structure.
//!
//! The \c flags field allows this function to update individual values.  The flags can be ORed together.  Only the fields indicated by
//! \c flags need to be populated in the provided \c pBlt parameter.
//!
//! \par Updating the Full BLT - C
//! \code{.c}
//! #include <Windows.h>
//! #include "HbiTiuApi.h"
//! #include "string.h"
//!
//! int main()
//! {
//!     BLT blt;
//!     HIL_STATUS status;
//!     blt.Id = 0x010001F4;
//!     strcpy(blt.DeviceName,"HBITIU");
//!     strcpy(blt.VendorName,"INTEL");
//!     strcpy(blt.PartNumberAsBuilt,"AAxxxxxx-xxx");
//!     strcpy(blt.PartNumberCurrent,blt.PartNumberAsBuilt);
//!     strcpy(blt.SerialNumber,"12345678");
//!     strcpy(blt.ManufactureDate,"YYMMDD");
//!     status = hbiDtbBltBoardWrite(0, &blt, BF_ALL);
//!     if (status != HS_SUCCESS)
//!         return -1;
//!     return 0;
//! }
//! \endcode
//! \par Updating the Current Part Number - Python
//! \code{.py}
//! blt = hil.BLT()
//! blt.PartNumberCurrent = 'AAyyyyyy-yyy'
//! dtbIndex = 0
//! hil.hbiDtbBltBoardWrite(dtbIndex, blt, hil.BF_PART_NUMBER_CURRENT)
//! # Note Python returns None for HIL_STATUS if successful and raises RuntimeError on failure.
//! \endcode
//!
//! \note HS_BLT_NOT_FOUND can be returned on an update (lack of #BF_ALL flag).
//!
//! @param[in] dtbIndex    A zero-based DTB index as illustrated below. Valid values are 0-3.
//! \image html hbi_dtb.png
//! @param[in] pBlt The address of a #BLT structure that contains the data to be written or updated.
//! @param[in] flags #BLT_FLAGS bits indicating what fields in \c pBlt are valid.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiDtbBltBoardWrite(_In_ UINT dtbIndex, _In_ PCBLT pBlt, _In_ BLT_FLAGS flags);

//! @brief Reads the board-level traceability values from an HBI Diagnostic Test Board (DTB).
//!
//! This function reads the board-level traceability values from an HBI DTB.  The values are defined in the #BLT structure.
//!
//! @param[in] dtbIndex    A zero-based Diagnostic Test Board (DTB) index as illustrated in hbiDtbBltBoardWrite(). Valid values are 0-3.
//! @param[out] pBlt The address of a #BLT structure that will be populated with the data.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiDtbBltBoardRead(_In_ UINT dtbIndex, _Out_ PBLT pBlt);

//! @brief Writes data to a 24LC512 (EEPROM) device on an HBI Diagnostic Test Board (DTB).
//!
//! This function writes data to a 24LC512 (EEPROM) device on an HBI Diagnostic Test Board (DTB).
//!
//! @note The two EEPROM devices on an HBI DTB can be accessed using \c hbiTiuXXXXX with an appropriate TIU index. See eepromIndex description for details.
//!
//! @param[in] dtbIndex    A zero-based DTB index as illustrated in hbiDtbBltBoardWrite(). Valid values are 0-3.
//! @param[in] eepromIndex Selects one of the two EEPROM devices to write. Consult the following table-
//!                        | eepromIndex | Fab A Locator | equivalent tiuIndex for \c hbiTiuXXXXX functions | 8-bit I2C address |
//!                        | :---------: | :-----------: | :----------------------------------------------: | :---------------: |
//!                        |      0      |      U23      |                 dtbIndex + 4                     |        0xA0       |
//!                        |      1      |      U77      |                 dtbIndex                         |        0xA0       |
//! @param[in] address     The location of the first byte in the EEPROM to which the data in the \c pData buffer will be written.
//! @param[in] pData       A data buffer containing data to write to the EEPROM device.
//! @param[in] length      The byte length of the \c pData buffer.
//! @returns \ref HIL_STATUS
//! @see hbiTiuEepromWrite()
HIL_API HIL_STATUS hbiDtbEepromWrite(_In_ UINT dtbIndex, _In_ UINT eepromIndex, _In_ DWORD address, _In_reads_bytes_(length) const void* pData, _In_ DWORD length);

//! @brief Reads data from a 24LC512 (EEPROM) device on an HBI Diagnostic Test Board (DTB).
//!
//! This function reads data from a 24LC512 (EEPROM) device on an HBI Diagnostic Test Board (DTB).
//!
//! @note The two EEPROM devices on an HBI DTB can be accessed using \c hbiTiuXXXXX with an appropriate TIU index. See eepromIndex description for details.
//!
//! @param[in] dtbIndex    A zero-based DTB index as illustrated in hbiDtbBltBoardWrite(). Valid values are 0-3.
//! @param[in] eepromIndex Selects one of the two EEPROM devices to read from. Consult the following table-
//!                        | eepromIndex | Fab A Locator | equivalent tiuIndex for \c hbiTiuXXXXX functions | 8-bit I2C address |
//!                        | :---------: | :-----------: | :----------------------------------------------: | :---------------: |
//!                        |      0      |      U23      |                 dtbIndex + 4                     |        0xA0       |
//!                        |      1      |      U77      |                 dtbIndex                         |        0xA0       |
//! @param[in] address     The location of the first byte in the EEPROM from which the data will be read.
//! @param[out] pData      A pointer to a data buffer in which the read data will be stored. Note that this buffer must have at least \c length
//!                        bytes allocated in order to complete successfully.
//! @param[in] length      The number of bytes to read from the EEPROM.
//! @returns \ref HIL_STATUS
//! @see hbiTiuEepromRead()
HIL_API HIL_STATUS hbiDtbEepromRead(_In_ UINT dtbIndex, _In_ UINT eepromIndex, _In_ DWORD address, _Out_writes_bytes_all_(length) LPVOID pData, _In_ DWORD length);

//! @brief Writes an 8-bit register of a PCA9505 device on an HBI Diagnostic Test Board (DTB).
//!
//! This function writes an 8-bit register of a PCA9505 device on an HBI Diagnostic Test Board (DTB).
//!
//! @note The three PCA9505 devices on an HBI DTB can be accessed using \c hbiTiuXXXXX functions with an appropriate TIU index. See chip description for details.
//!
//! @param[in] dtbIndex    A zero-based DTB index as illustrated in hbiDtbBltBoardWrite(). Valid values are 0-3.
//! @param[in] chip A zero-based chip index. Consult the following table.
//!                 Note that reference designators may change in new fabs.
//!                 |  Chip   | Fab A | equivalent tiuIndex for \c hbiTiuXXXXX functions | 8-bit I2C address |
//!                 | :-----: | :---: | :----------------------------------------------: | :---------------: |
//!                 |    0    |  U24  |                  dtbIndex + 4                    |        0x40       |
//!                 |    1    |  U22  |                  dtbIndex + 4                    |        0x42       |
//!                 |    2    |  U78  |                  dtbIndex                        |        0x40       |
//! @param[in] reg A valid register address for the PCA9505 device.  Valid values are 0-39 (00h-27h).
//! @param[in] data The value to write.  Valid values are 0-255 (00h-FFh).
//! @returns \ref HIL_STATUS
//! @see hbiTiuPca9505Write()
HIL_API HIL_STATUS hbiDtbPca9505Write(_In_ UINT dtbIndex, _In_ UINT chip, _In_ BYTE reg, _In_ BYTE data);

//! @brief Reads an 8-bit register of a PCA9505 device on an HBI Diagnostic Test Board (DTB).
//!
//! This function reads an 8-bit register of a PCA9505 device on an HBI Diagnostic Test Board (DTB).
//!
//! @note The three PCA9505 devices on an HBI DTB can be accessed using \c hbiTiuXXXXX functions with an appropriate TIU index. See chip description for details.
//!
//! @param[in] dtbIndex    A zero-based DTB index as illustrated in hbiDtbBltBoardWrite(). Valid values are 0-3.
//! @param[in] chip A zero-based chip index. Consult the following table.
//!                 Note that reference designators may change in new fabs.
//!                 |  Chip   | Fab A | equivalent tiuIndex for \c hbiTiuXXXXX functions | 8-bit I2C address |
//!                 | :-----: | :---: | :----------------------------------------------: | :---------------: |
//!                 |    0    |  U24  |                  dtbIndex + 4                    |        0x40       |
//!                 |    1    |  U22  |                  dtbIndex + 4                    |        0x42       |
//!                 |    2    |  U78  |                  dtbIndex                        |        0x40       |
//! @param[in] reg A valid register address for the PCA9505 device.  Valid values are 0-39 (00h-27h).
//! @param[out] pData The register contents read from the device.
//! @returns \ref HIL_STATUS
//! @see hbiTiuPca9505Read()
HIL_API HIL_STATUS hbiDtbPca9505Read(_In_ UINT dtbIndex, _In_ UINT chip, _In_ BYTE reg, _Out_ PBYTE pData);

//! @brief Writes an 8-bit register of a MAX14661 device on an HBI Diagnostic Test Board (DTB).
//!
//! This function writes an 8-bit register of a MAX14661 device on an HBI Diagnostic Test Board (DTB).
//!
//! @param[in] dtbIndex    A zero-based DTB index as illustrated in hbiDtbBltBoardWrite(). Valid values are 0-3.
//! @param[in] chip     A zero-based chip index. Consult the following table.
//!                     Note that reference designators may change in new fabs.
//!                     |  Chip   | Fab A |
//!                     | :-----: | :---: |
//!                     |    0    |  EU1  |
//!                     |    1    |  EU2  |
//!                     |    2    |  EU5  |
//!                     |    3    |  EU6  |
//!                     |    4    |  EU3  |
//!                     |    5    |  EU4  |
//!                     |    6    |  EU7  |
//!                     |    7    |  EU8  |
//! @param[in] reg      A valid register address for the MAX14661 device.
//! @param[in] data     The value to write.  Valid values are 0-255 (00h-FFh).
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiDtbMax14661Write(_In_ UINT dtbIndex, _In_ UINT chip, _In_ BYTE reg, _In_ BYTE data);

//! @brief Reads an 8-bit register of a MAX14661 device on an HBI Diagnostic Test Board (DTB).
//!
//! This function reads an 8-bit register of a MAX14661 device on HBI Diagnostic Test Board (DTB).
//!
//! @param[in] dtbIndex    A zero-based DTB index as illustrated in hbiDtbBltBoardWrite(). Valid values are 0-3.
//! @param[in] chip     A zero-based chip index. Consult the following table.
//!                     Note that reference designators may change in new fabs.
//!                     |  Chip   | Fab A |
//!                     | :-----: | :---: |
//!                     |    0    |  EU1  |
//!                     |    1    |  EU2  |
//!                     |    2    |  EU5  |
//!                     |    3    |  EU6  |
//!                     |    4    |  EU3  |
//!                     |    5    |  EU4  |
//!                     |    6    |  EU7  |
//!                     |    7    |  EU8  |
//! @param[in] reg      A valid register address for the MAX14661 device.
//! @param[out] pData   The register contents read from the device.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiDtbMax14661Read(_In_ UINT dtbIndex, _In_ UINT chip, _In_ BYTE reg, _Out_ PBYTE pData);

//! @brief Reads the fan tachometer channel on a specified HBI Diagnostic Test Board (DTB).
//!
//! This function reads the fan tachometer channel on a specified HBI Diagnostic Test Board (DTB).
//!
//! @param[in] dtbIndex    A zero-based DTB index as illustrated in hbiDtbBltBoardWrite(). Valid values are 0-3.
//! @param[out] pTach   The fan speed in revolutions per second (RPS).
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiDtbFanRead(_In_ UINT dtbIndex, _Out_ double* pTach);

//! @brief Writes a register within the MAX6650 (fan controller) device on an HBI Diagnostic Test Board (DTB).
//!
//! This function writes a register within the MAX6650 (fan controller) device on an HBI Diagnostic Test Board (DTB).
//!
//! @param[in] dtbIndex    A zero-based DTB index as illustrated in hbiDtbBltBoardWrite(). Valid values are 0-3.
//! @param[in] reg      Writable MAX6650 register address.
//!                      | Register | Name         | Function              |
//!                      | :------: | :----------: | :-------------------- |
//!                      |   0x00   | SPEED        | Fan speed             |
//!                      |   0x02   | CONFIG       | Configuration         |
//!                      |   0x04   | GPIO DEF     | GPIO definition       |
//!                      |   0x06   | DAC          | DAC                   |
//!                      |   0x08   | ALARM ENABLE | Alarm Enable          |
//!                      |   0x16   | COUNT        | Tachometer count time |
//! @param[in] data     The value to write.  Valid values are 0-255 (00h-FFh).
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiDtbMax6650Write(_In_ UINT dtbIndex, _In_ BYTE reg, _In_ BYTE data);

//! @brief Reads a register within the MAX6650 (fan controller) device on an HBI Diagnostic Test Board (DTB).
//!
//! This function reads a register within the MAX6650 (fan controller) device on an HBI Diagnostic Test Board (DTB).
//!
//! @param[in] dtbIndex    A zero-based DTB index as illustrated in hbiDtbBltBoardWrite(). Valid values are 0-3.
//! @param[in] reg  MAX6650 register address.
//!                 | Register | Name         | Function              |
//!                 | :------: | :----------: | :-------------------- |
//!                 |   0x00   | SPEED        | Fan speed             |
//!                 |   0x02   | CONFIG       | Configuration         |
//!                 |   0x04   | GPIO DEF     | GPIO definition       |
//!                 |   0x06   | DAC          | DAC                   |
//!                 |   0x08   | ALARM ENABLE | Alarm Enable          |
//!                 |   0x0A   | ALARM        | Alarm Status          |
//!                 |   0x0C   | TACH0        | Tachometer 0 count    |
//!                 |   0x0E   | TACH1        | Tachometer 1 count    |
//!                 |   0x10   | TACH2        | Tachometer 2 count    |
//!                 |   0x12   | TACH3        | Tachometer 3 count    |
//!                 |   0x14   | GPIO STAT    | GPIO Status           |
//!                 |   0x16   | COUNT        | Tachometer count time |
//! @param[out]     pData The register contents.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiDtbMax6650Read(_In_ UINT dtbIndex, _In_ BYTE reg, _Out_ PBYTE pData);

//! @brief Reads the specified temperature channel on an HBI Diagnostic Test Board (DTB).
//!
//! This function reads the specified temperature channel on an HBI Diagnostic Test Board (DTB).
//!
//! @param[in] dtbIndex    A zero-based DTB index as illustrated in hbiDtbBltBoardWrite(). Valid values are 0-3.
//! @param[in] channel  Selects a monitoring channel. Consult the following table:
//!                     | Channel | Measured Temperature  |
//!                     | :-----: | :-------------------: |
//!                     |    0    |     LM75A (U80)       |
//!                     |    1    |     LM75A (U81)       |
//!                     |    2    |     LM75A (U82)       |
//!                     |    3    |     LM75A (U83)       |
//!                     |    4    |     LM75A (U84)       |
//!                     |    5    |     LM75A (U85)       |
//! @param[out] pTemp  The temperature in degrees Celsius.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiDtbTmonRead(_In_ UINT dtbIndex, _In_ UINT channel, _Out_ double* pTemp);

//! @brief Writes a register of a LM75A (Temperature Sensor) device on an HBI Diagnostic Test Board (DTB).
//!
//! This function writes a register of a LM75A (Temperature Sensor) device on an HBI Diagnostic Test Board (DTB).
//!
//! @param[in] dtbIndex    A zero-based DTB index as illustrated in hbiDtbBltBoardWrite(). Valid values are 0-3.
//! @param[in] chip A zero-based chip index. Consult the following table.
//!                 Note that reference designators may change in new fabs.
//!                 |  Chip   | Fab A |
//!                 | :-----: | :---: |
//!                 |    0    |  U80  |
//!                 |    1    |  U81  |
//!                 |    2    |  U82  |
//!                 |    3    |  U83  |
//!                 |    4    |  U84  |
//!                 |    5    |  U85  |
//! @param[in] reg  Writable LM75A register address.
//!                 | Register | Description    | Bits |
//!                 | :------: | :------------- | :--: |
//!                 |   0x01   | Configuration  |   8  |
//!                 |   0x02   | THYST          |  16  |
//!                 |   0x03   | TOS            |  16  |
//! @param[in] data The value to write.  Valid values are 0-255 (00h-FFh).
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiDtbLm75aWrite(_In_ UINT dtbIndex, _In_ UINT chip, _In_ BYTE reg, _In_ WORD data);

//! @brief Reads a register of a LM75A (Temperature Sensor) device on an HBI Diagnostic Test Board (DTB).
//!
//! This function reads a register of a LM75A (Temperature Sensor) device on an HBI Diagnostic Test Board (DTB).
//!
//! @param[in] dtbIndex    A zero-based DTB index as illustrated in hbiDtbBltBoardWrite(). Valid values are 0-3.
//! @param[in] chip A zero-based chip index. Consult the following table.
//!                 Note that reference designators may change in new fabs.
//!                 |  Chip   | Fab A |
//!                 | :-----: | :---: |
//!                 |    0    |  U80  |
//!                 |    1    |  U81  |
//!                 |    2    |  U82  |
//!                 |    3    |  U83  |
//!                 |    4    |  U84  |
//!                 |    5    |  U85  |
//! @param[in] reg LM75A register address.
//!                 | Register | Description         | Bits |
//!                 | :------: | :------------------ | :--: |
//!                 |   0x00   | Temperature         |  16  |
//!                 |   0x01   | Configuration       |   8  |
//!                 |   0x02   | THYST               |  16  |
//!                 |   0x03   | TOS                 |  16  |
//!                 |   0x07   | Product ID Register |   8  |
//! @param[out] pData The register contents read from the device.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiDtbLm75aRead(_In_ UINT dtbIndex, _In_ UINT chip, _In_ BYTE reg, _Out_ LPWORD pData);

#ifdef __cplusplus
}
#endif
