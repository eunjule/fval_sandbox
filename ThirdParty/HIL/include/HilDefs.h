// INTEL CONFIDENTIAL
// Copyright 2014-2020 Intel Corporation All Rights Reserved.
//
// The source code contained or described herein and all documents related to
// the source code ("Material") are owned by Intel Corporation or its
// suppliers or licensors. Title to the Material remains with Intel
// Corporation or its suppliers and licensors. The Material contains trade
// secrets and proprietary and confidential information of Intel or its
// suppliers and licensors. The Material is protected by worldwide copyright
// and trade secret laws and treaty provisions. No part of the Material may be
// used, copied, reproduced, modified, published, uploaded, posted,
// transmitted, distributed, or disclosed in any way without Intel's prior
// express written permission.
//
// No license under any patent, copyright, trade secret or other intellectual
// property right is granted to or conferred upon you by disclosure or
// delivery of the Materials, either expressly, by implication, inducement,
// estoppel or otherwise. Any license under such intellectual property rights
// must be express and approved by Intel in writing.

//! @file
//! @brief Non-function definitions such as constants and structures used by the global HIL APIs.

#pragma once

// The following ifdef block is the standard way of creating macros which make exporting
// from a DLL simpler. All files within this DLL are compiled with the RC_EXPORTS
// symbol defined on the command line. This symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see
// HIL_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.

// This condition skips having to document the DLL import/export details in Doxygen.
//! @cond SKIP
#ifndef HIL_API
#  ifdef HIL_EXPORTS
#    define HIL_API __declspec(dllexport)
#  else
#    define HIL_API __declspec(dllimport)
#  endif
#endif
//! @endcond

// NOTE: This version number is documented in THREE places:
// 1. Here.
// 2. In the release notes in HilApi.h.
// 3. In doxygen.cfg as "PROJECT_NUMBER = MM.mm.pp.bb"
#define HIL_VERSION_MAJOR      7  //!< HIL major version number.
#define HIL_VERSION_MINOR      8  //!< HIL minor version number.
#define HIL_VERSION_SUBMINOR   0  //!< HIL sub-minor version number.
#define HIL_VERSION_REVISION   0  //!< HIL revision number.
const DWORD HIL_CURRENT_VERSION = (HIL_VERSION_MAJOR << 24) | (HIL_VERSION_MINOR << 16) | (HIL_VERSION_SUBMINOR << 8) | HIL_VERSION_REVISION;  //!< Major, minor, subminor, revision (0xMMmmssrr).

#define HIL_SLOTS 12                     //!< Number of physical slots in an HDMT chassis.
#define HBI_DPS_SLOTS 16                 //!< Number of physical DPS slots in an HBI chassis.
#define HBI_PSDB_SLOTS 2                 //!< Number of physical PSDB slots in an HBI chassis.
#define HIL_ANY_ID 0                     //!< Wildcard value matching any device ID.
#define HIL_ANY_SLOT -1                  //!< Wildcard value matching any slot (or no slot at all)
#define HIL_MAX_VERSION_STRING_SIZE 32   //!< Maximum version string length possible.
#define HIL_MAX_ID_STRING_SIZE 32        //!< Maximum MesID/SysID string length possible.

#define MAX_DUT_DOMAIN_ID 63  //!< Maximum device under test (DUT) Domain ID for APIs that require one.  Valid DUT Domain IDs are 0-MAX_DUT_DOMAIN_ID.

//! \brief Return value enumeration for HIL functions.
//!
//! Returned by most HIL functions.  Zero is success and non-zero is a failure.
typedef _Return_type_success_(return == 0) enum HIL_STATUS
{
    // NOTE: NEVER DELETE OR REASSIGN THE VALUE.  ADD NEW VALUES TO THE END.
    //
    HS_SUCCESS             =  0,  //!< The operation completed successfully.
    HS_UNSUCCESSFUL        =  1,  //!< A non-specific error occurred.
    HS_INSUFFICIENT_BUFFER =  2,  //!< The buffer provided to the function was too small.
    HS_INVALID_PARAMETER   =  3,  //!< A parameter to the function was invalid.
    HS_INVALID_HANDLE      =  4,  //!< The handle parameter to the function was invalid.
    HS_DEVICE_NOT_FOUND    =  5,  //!< The device acted upon by the function was not present.
    HS_DEVICE_IN_USE       =  6,  //!< The function could not complete because the device was in use.
    HS_ALIGNMENT_VIOLATION =  7,  //!< A buffer provided to the function does not meet physical memory alignment requirements.
    HS_CRC_CHECK           =  8,  //!< The operation completed but a CRC check on the result failed.
    HS_TIMEOUT             =  9,  //!< The operation did not complete within a specified time period.
    HS_OUT_OF_MEMORY       = 10,  //!< A memory allocation failed.
    HS_FILE_NOT_FOUND      = 11,  //!< The indicated file was not found.
    HS_FILE_ERROR          = 12,  //!< A read or write error occurred on the specified file or file handle.
    HS_CANCELED            = 13,  //!< The operation was canceled by the user.
    // This is the set of FTDI error codes.  If an FTDI call used internally by HIL fails, it will be mapped to these error codes.
    //
    // FT_OK = HS_SUCCESS
    // FT_INVALID_HANDLE = HS_INVALID_HANDLE
    // FT_DEVICE_NOT_FOUND = HS_DEVICE_NOT_FOUND
    HS_DEVICE_NOT_OPENED      = 14,  //!< An attempt to open a device failed.
    HS_IO_ERROR               = 15,  //!< An I/O error occurred.
    HS_INSUFFICIENT_RESOURCES = 16,  //!< There are insufficient resources to complete the operation.
    // FT_INVALID_PARAMETER = HS_INVALID_PARAMETER
    HS_INVALID_BAUD_RATE           = 17,  //!< Invalid baud rate specified.
    HS_DEVICE_NOT_OPENED_FOR_ERASE = 18,  //!< Device not opened for erase.
    HS_DEVICE_NOT_OPENED_FOR_WRITE = 19,  //!< Device not opened for write.
    HS_FAILED_TO_WRITE_DEVICE      = 20,  //!< Failed to write to device.
    HS_EEPROM_READ_FAILED          = 21,  //!< EEPROM read failed.
    HS_EEPROM_WRITE_FAILED         = 22,  //!< EEPROM write failed.
    HS_EEPROM_ERASE_FAILED         = 23,  //!< EEPROM erase failed.
    HS_EEPROM_NOT_PRESENT          = 24,  //!< EEPROM not present.
    HS_EEPROM_NOT_PROGRAMMED       = 25,  //!< EEPROM not programmed.
    // FT_INVALID_ARGS = HS_INVALID_PARAMETER
    // FT_NOT_SUPPORTED = HS_UNSUCCESSFUL
    // FT_OTHER_ERRO  = HS_UNSUCCESSFUL
    // FT_DEVICE_LIST_NOT_READY = HS_UNSUCCESSFUL
    HS_BLT_NOT_FOUND       = 26,  //!< A BLT record was not found in the device.
    HS_BLT_INVALID         = 27,  //!< A field in the BLT is invalid.  This can occur if a function requires a BLT field to follow a particular formatting convention.
    HS_UNSUPPORTED         = 28,  //!< This function is not supported.
    HS_SC_BLT_INVALID      = 29,  //!< A field in the site controller BLT is invalid.  This can occur on device APIs that are affected by site controller hardware differences.  Use scBltBoardRead() to debug.
    HS_SC_BLT_NOT_FOUND    = 30,  //!< A required BLT record is missing from the site controller.  This can occur on device APIs that are affected by site controller hardware differences.  Use scBltBoardRead() to debug.
    HS_SC_BLT_CRC_CHECK    = 31,  //!< The site controller BLT record failed a CRC check.  This can occur on device APIs that are affected by site controller hardware differences.  Use scBltBoardRead() to debug.
    HS_I2C_TIMEOUT         = 32,  //!< An I2C bus timeout occurred.
    HS_I2C_NOACK           = 33,  //!< An I2C transaction was not acknowledged.
    HS_I2C_BUS_ERROR       = 34,  //!< An I2C bus error occurred.
    HS_I2C_NOT_READY       = 35,  //!< The I2C bus is not ready.
    HS_NOT_ENABLED         = 36,  //!< The operation has not been enabled by the user.
    HS_INVALID_DATA_LENGTH = 37,  //!< The data length is invalid.
    HS_FTDI_ENUMERATION    = 38,  //!< FTDI enumeration not completing (Location IDs returning zero).
    HS_FPGA_VERSION_UNSUPPORTED = 39, //!< FPGA version not supported.
    HS_FIFO_EMPTY          = 40,  //!< The FIFO is empty.
    HS_OVERFLOW            = 41,  //!< A computation could not complete due to exceeding arithmetic limits.
    HS_FILE_OPEN_ERROR     = 42,  //!< A file was not opened successfully.
    HS_DATA_MISCOMPARE     = 43,  //!< Data comparison rendered mismatch.
    HS_USB_VIDPID_WRONG    = 44,  //!< A USB device was found, but the device ID is wrong.  The device programming or slot specified may be incorrect.

    // HIL Statuses for JBI_RETURN_TYPE
    HS_UNEXPECTED_END      = 45,  //!< unexpected end of file.
    HS_UNDEFINED_SYMBOL    = 46,  //!< undefined symbol.
    HS_INTEGER_OVERFLOW    = 47,  //!< integer overflow.
    HS_DIVIDE_BY_ZERO      = 48,  //!< divide by zero.
    HS_CRC_ERROR           = 49,  //!< CRC mismatch.
    HS_INTERNAL_ERROR      = 50,  //!< internal error.
    HS_BOUNDS_ERROR        = 51,  //!< bounds error.
    HS_VECTOR_MAP_FAILED   = 52,  //!< vector signal name not found.
    HS_STACK_OVERFLOW      = 53,  //!< stack overflow.
    HS_ILLEGAL_OPCODE      = 54,  //!< illegal instruction code.
    HS_ACTION_NOT_FOUND    = 55,  //!< action not found.

    // HIL Statuses for exit codes from <jbistub.cpp> JbcExecute.
    HS_CHIAN_CHECK_FAILED        = 56,  //!< Checking chain failure.
    HS_IDCODE_READ_FAILED        = 57,  //!< Reading IDCODE failure.
    HS_USERCODE_READ_FAILED      = 58,  //!< Reading USERCODE failure.
    HS_UESCODE_READ_FAILED       = 59,  //!< Reading UESCODE failure.
    HS_ISP_ENTERING_FAILED       = 60,  //!< Entering ISP failure.
    HS_DEVICE_UNRECOGNIZED       = 61,  //!< Unrecognized device.
    HS_REVISION_UNSUPPORTED      = 62,  //!< Device revision is not supported.
    HS_ERAZE_FAILED              = 63,  //!< Erase failure.
    HS_DEVICE_NOT_BLANK          = 64,  //!< Device is not blank.
    HS_DEVICE_PROGRAMMING_FAILED = 65,  //!< Device programming failure.
    HS_DEVICE_VERIFY_FAILED      = 66,  //!< Device verify failure.
    HS_READ_FAILED               = 67,  //!< Read failure.
    HS_CHECKSUM_CALC_FAILED      = 68,  //!< Calculating checksum failure.
    HS_SECURITY_BIT_SET_FAILED   = 69,  //!< Setting security bit failure.
    HS_SECURITY_BIT_QUERY_FAILED = 70,  //!< Querying security bit failure.
    HS_ISP_EXIT_FAILED           = 71,  //!< Exiting ISP failure.
    HS_SYSTEM_TEST_FAILED        = 72,  //!< Performing system test failure.
    HS_INIT_VALUES_ILLEGAL       = 73,  //!< Illegal initialization values.
    HS_SRAM_CONFIG_FAILED        = 74,  //!< SRAM configuration failure

    HS_PARITY_ERROR                 = 75,  //!< Parity error detected.
    HS_OVERLOAD_ERROR               = 76,  //!< Hardware out of range error detected.
    HS_TEMPERATURE_ERROR            = 77,  //!< Hardware outside operating temperature.
    HS_COMM_ERROR                   = 78,  //!< Hardware detected invalid communication.
    HS_I2C_ADDR_NOACK               = 79,  //!< An I2C device did not respond at that address.
    HS_I2C_DATA_NOACK               = 80,  //!< An I2C device did not acknowledge a data byte.
    HS_FTDI_ENUMERATION_LOCK_FAILED = 81,  //!< An FTDI device handle is held open preventing enumeration or an older, incompatible HIL is in use.
    HS_UNSUPPORTED_ON_RCTC2         = 82,  //!< This function is not supported on RCTC2.
    HS_UNSUPPORTED_ON_RCTC3         = 83,  //!< This function is not supported on RCTC3.
    HS_UPDATE_HBI_RCTC_DRIVER       = 84,  //!< Updated HBI_RCTC driver required.
    HS_FTDI_LOCK_PERMISSION         = 85,  //!< Failed to open FTDI multiprocess locks.  Ensure all processes using HIL use the same login credentials.
    HS_MAX31855_FAULT_DETECTED      = 86,  //!< MAX31855 temperature sensor fault detected.  See MAX10 register 15 for fault details.
    HS_MISUSE                       = 87,  //!< HIL mis-use detected.  Possible disconnect in another thread of execution.
    HS_METADATA_TOO_LARGE           = 88,  //!< Metadata file length larger than a FLASH page.
    HS_MISSING_METADATA             = 89,  //!< Required \<filename\>.mta metadata file not found.
    HS_INVALID_METASIG              = 90,  //!< Invalid metadata signature.  First three data bytes should be "MTA".
    HS_INVALID_METASUM              = 91,  //!< Invalid metadata checksum.
    HS_INVALID_METALEN              = 92,  //!< Invalid metadata length.
    HS_MISMATCH_METALEN             = 93,  //!< Data length in metadata does not match actual data length.
    HS_META_CRC                     = 94,  //!< CRC in metadata does not match actual data CRC.
    HS_META_INVALID_TARGET          = 95,  //!< FPGA image is not intended for the target FPGA.
    HS_UNSUPPORTED_FAB              = 96,  //!< Unsupported fab.
    HS_META_WRITE_FAILED            = 97,  //!< Metadata write failed.
    HS_METADATA_NOT_FOUND           = 98,  //!< Metadata not found in FLASH.
    HS_SPM_POLLING_ENABLED          = 99,  //!< API not supported when SPM polling is enabled.
    HS_FPGA_CONFIG_NOT_READY       = 100,  //!< Timeout waiting for FPGA to indicate ready for configuration.
    HS_FPGA_CONFIG_NOT_DONE        = 101,  //!< Timeout waiting for FPGA to indicate configuration done.
} HIL_STATUS;

#define BLT_MAX_STRING_LENGTH 32 //!< The maximum length of #BLT string fields.  This includes a required NUL terminator.

//! @brief Board-level traceability fields used to identify instrument components.
//!
//! These board-level traceability fields are used to uniquely identify a device.  The strings are required to be null-terminated.
typedef struct BLT
{
    DWORD Id;                                      //!< An 32-bit identifier whose meaning is user-defined.
    char VendorName[BLT_MAX_STRING_LENGTH];        //!< The vendor name of this component.
    char DeviceName[BLT_MAX_STRING_LENGTH];        //!< The device name of this component.
    char PartNumberAsBuilt[BLT_MAX_STRING_LENGTH]; //!< The original part number of this component.
    char PartNumberCurrent[BLT_MAX_STRING_LENGTH]; //!< If this component has been modified, this is the new part number.
    char SerialNumber[BLT_MAX_STRING_LENGTH];      //!< The serial number of this component.
    char ManufactureDate[BLT_MAX_STRING_LENGTH];   //!< The manufacturing date code of this component.
} BLT;

typedef BLT* PBLT;         //!< The address of a mutable (read/write) #BLT structure.
typedef const BLT* PCBLT;  //!< The address of an immutable (read-only) #BLT structure.

#define BLTEX_NUM_COUNTERS 10 //!< The number of #BLTEX_COUNTERS records in an EEPROM.
#define BLTEX_NUM_SITES    18 //!< The number of sites on a TIU.

//! @brief Counters stored in BLT EEPROM. There can be #BLTEX_NUM_COUNTERS of these in each EEPROM.
//!
//! Ten of these structures are written as an array to the board-level traceability EEPROM.  These counters
//! are available for customer use.  There is also special handling in the Python version
//! of this structure to handle the array field.
//!
//! In the Python version of this structure, there is one additional method exposed to modify the DWORD array.
//! This method is not available in C/C++.  In C/C++ the structure can be modified directly.
//! * HIL_STATUS CounterSet(UINT index, DWORD value);
//!
//! @par Python Example
//! @code{.py}
//! >>> import hil
//! >>> counters = hil.tiuBltExCountersRead(0,0) # Read the first of #BLTEX_NUM_COUNTERS counter structures.
//! >>> counters # SWIG is the tool generating the Python interface.
//! <Swig Object of type 'BLTEX_COUNTERS *' at 0x0000000002719F48>
//! >>> counters.Counter
//! (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17)
//! >>> counters.Counter[5]
//! 5
//! >>> counters.CounterSet(5,10)   # Set an individual Counter
//! >>> counters.Counter
//! (0, 1, 2, 3, 4, 10, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17)
//! >>> counters.Counter[5]
//! 10
//! >>> # The entire array can be updated if assigned a sequence of the correct length.
//! >>> hil.Counter = [0] * hil.BLTEX_NUM_SITES
//! >>> hil.Counter
//! [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
//! >>> hil.tiuBltExCountersWrite(0,0)   # Write the updated Counter.
//! @endcode
typedef struct BLTEX_COUNTERS
{
    DWORD Counter[BLTEX_NUM_SITES]; //!< A 32-bit identifier whose meaning is user-defined.
} BLTEX_COUNTERS;
typedef BLTEX_COUNTERS* PBLTEX_COUNTERS;         //!< The address of a mutable (read/write) #BLTEX_COUNTERS structure.
typedef const BLTEX_COUNTERS* PCBLTEX_COUNTERS;  //!< The address of an immutable (read-only) #BLTEX_COUNTERS structure.

//! @brief Counters stored in BLT EEPROMs to track mating counts.
typedef struct BLTCY_COUNTERS
{
    DWORD CycleCounter;                        //!< Mating count since the last preventative maintenance.
    DWORD LifeTimeCounter;                     //!< Mating count for the lifetime of the board.
    char SerialNumber[BLT_MAX_STRING_LENGTH];  //!< The serial number of the mated board.
} BLTCY_COUNTERS;
typedef BLTCY_COUNTERS* PBLTCY_COUNTERS;         //!< The address of a mutable (read/write) #BLTCY_COUNTERS structure.
typedef const BLTCY_COUNTERS* PCBLTCY_COUNTERS;  //!< The address of an immutable (read-only) #BLTCY_COUNTERS structure.

#define BLTEX_MAX_SITEID_LENGTH  4  //!< Number of characters allowed in a site ID.
#define BLTEX_NUM_AUX_STRINGS   40  //!< Number of auxiliary string fields in a #BLTEX structure.

//! @brief Extended BLT data stored in BLT EEPROM.
//!
//! This extended BLT data stored in BLT EEPROM describes some customer-specific fields.  Note that the
//! string array fields must be null-terminated.  There is also special handling in the Python version
//! of this structure to handle the array fields.
//!
//! In the Python version of this structure, there are three additional methods exposed to modify the string arrays.
//! These methods are not available in C/C++.  In C/C++ the structure can be modified directly.
//! * HIL_STATUS AuxiliarySet(UINT index, LPCSTR value);
//! * HIL_STATUS SiteSet(UINT index, LPCSTR value);
//! * HIL_STATUS SiteIdSet(UINT index, LPCSTR value);
//!
//! @par Python Example
//! @code{.py}
//! >>> import hil
//! >>> bltex = hil.tiuBltExRead(0) # Read an existing structure.
//! >>> bltex                       # SWIG is the tool generating the Python interface.
//! <Swig Object of type 'BLTEX *' at 0x00000000027197A0>
//! >>> bltex.Eco
//! 111
//! >>> bltex.HwVersion
//! 222
//! >>> bltex.SiteId                # View one of the string arrays.
//! ('X00', 'X01', 'X02', 'X03', 'X04', 'X05', 'X06', 'X07', 'X08', 'X09', 'X10', 'X11', 'X12', 'X13', 'X14', 'X15', 'X16',
//! 'X17')
//! >>> bltex.SiteIdSet(0,'ABC')    # Extra method to set one string.
//! >>> bltex.SiteId
//! ('ABC', 'X01', 'X02', 'X03', 'X04', 'X05', 'X06', 'X07', 'X08', 'X09', 'X10', 'X11', 'X12', 'X13', 'X14', 'X15', 'X16',
//! 'X17')
//! >>> # The entire array can be updated if assigned a sequence of the correct length.
//! >>> bltex.SiteId = ['A{:02}'.format(i) for i in range(hil.BLTEX_NUM_SITES)]
//! >>> bltex.SiteId
//! ('A00', 'A01', 'A02', 'A03', 'A04', 'A05', 'A06', 'A07', 'A08', 'A09', 'A10', 'A11', 'A12', 'A13', 'A14', 'A15', 'A16',
//! 'A17')
//! >>> hil.tiuBltExWrite(0,bltex)      # Write the updated structure.
//! @endcode
typedef struct BLTEX
{
    DWORD Eco;                                                     //!< ECO (Engineering Change Order) identifier.
    DWORD HwVersion;                                               //!< The hardware version.
    char SiteId[BLTEX_NUM_SITES][BLTEX_MAX_SITEID_LENGTH];         //!< An array of null-terminated site ID strings.
    char Site[BLTEX_NUM_SITES][BLT_MAX_STRING_LENGTH];             //!< An array of null-terminated site names strings.
    char Auxiliary[BLTEX_NUM_AUX_STRINGS][BLT_MAX_STRING_LENGTH];  //!< An array of null-terminated auxillary strings.
} BLTEX;
typedef BLTEX* PBLTEX;        //!< The address of a mutable (read/write) #BLTEX structure.
typedef const BLTEX* PCBLTEX; //!< The address of an immutable (read-only) #BLTEX structure.

#define TIU_MAX_EEPROMS 8 //!< The maximum number of EEPROMs allowed on a TIU. At least one is required.
#define TIU_ROOT_EEPROM 0 //!< EEPROM 0 must contain the Board #BLT record.

//! @brief Bit field definitions for functions that write #BLT information.
//!
//! These bit field definitions indicate to functions that write #BLT information
//! what fields have been populated in the provided #BLT structure.  The bits may
//! be bitwise-ORed together to updated multiple fields.
typedef enum BLT_FLAGS
{
    BF_ALL                  = -1,     //!< All fields must be populated in the provided #BLT structure.
    BF_ID                   = 1 << 0, //!< The Id field must be valid in the provided #BLT structure.
    BF_VENDOR_NAME          = 1 << 1, //!< The VendorName field must be valid in the provided #BLT structure.
    BF_DEVICE_NAME          = 1 << 2, //!< The DeviceName field must be valid in the provided #BLT structure.
    BF_PART_NUMBER_AS_BUILT = 1 << 3, //!< The PartNumberAsBuilt field must be valid in the provided #BLT structure.
    BF_PART_NUMBER_CURRENT  = 1 << 4, //!< The PartNumberCurrent field must be valid in the provided #BLT structure.
    BF_SERIAL_NUMBER        = 1 << 5, //!< The SerialNumber field must be valid in the provided #BLT structure.
    BF_MANUFACTURE_DATE     = 1 << 6, //!< The ManufactureDate field must be valid in the provided #BLT structure.
} BLT_FLAGS;

//! @brief Bit field definitions for HDMT Tester Slots
//!
//! These bit field definitions are a binary representation for an HDMT Tester Slot.
typedef enum HIL_SLOT_BITS
{
    HSB_S0  = 1 <<  0,  //!< Binary representation of HDMT Tester Slot 0
    HSB_S1  = 1 <<  1,  //!< Binary representation of HDMT Tester Slot 1
    HSB_S2  = 1 <<  2,  //!< Binary representation of HDMT Tester Slot 2
    HSB_S3  = 1 <<  3,  //!< Binary representation of HDMT Tester Slot 3
    HSB_S4  = 1 <<  4,  //!< Binary representation of HDMT Tester Slot 4
    HSB_S5  = 1 <<  5,  //!< Binary representation of HDMT Tester Slot 5
    HSB_S6  = 1 <<  6,  //!< Binary representation of HDMT Tester Slot 6
    HSB_S7  = 1 <<  7,  //!< Binary representation of HDMT Tester Slot 7
    HSB_S8  = 1 <<  8,  //!< Binary representation of HDMT Tester Slot 8
    HSB_S9  = 1 <<  9,  //!< Binary representation of HDMT Tester Slot 9
    HSB_S10 = 1 << 10,  //!< Binary representation of HDMT Tester Slot 10
    HSB_S11 = 1 << 11,  //!< Binary representation of HDMT Tester Slot 11
    HSB_ALL = 0xFFF     //!< Binary representation of of all HDMT Tester Slots combined
} HIL_SLOT_BITS;

//! @brief Enumeration of tester types.
typedef enum TESTER
{
    TESTER_UNKNOWN,  //!< Tester type undetermined.
    TESTER_HDMT,     //!< HDMT tester type.
    TESTER_HBI       //!< HBI tester type.
} TESTER;

//! \brief Callback definition used with all \c xxxxFpgaBaseLoad() functions such as rcFpgaBaseLoad().
//!
//! This callback is called periodically during the \c xxxxFpgaBaseLoad() functions such as rcFpgaBaseLoad()
//! to provide progress updates while the EEPROM is being programmed (which can take multiple minutes).
//! @note Any errors that occur in the callback are not handled by HIL and simply suppressed.
//!
//! The callback and context could be used to update a GUI progress bar.  A pointer to the progress bar
//! object could be passed as the context and the callback would cast the context back to an object
//! and update the progress bar with the percent done value.  A simpler version could print the
//! percent done value to the console and ignore the context parameter.
//!
//! @par Python Example
//! This example uses an anonymous function to update a progress line on the console.
//! @code{.py}
//! >>> # Pass an anonymous function to print the percent done as the function proceeds.
//! >>> import hil
//! >>> hil.rcFpgaBaseLoad(filename, lambda val,ctx: print('\r{:5.1f}%'.format(val),end='',flush=True))
//!  19.4%   # This will continuously update due to carriage return(\r) but no linefeed(end='')
//! @endcode
//!
//! @par C++ Example
//! This example uses a callback function to collect all the progress values in a vector
//! as an example of using a context.
//! @code{.cpp}
//! void BaseLoadCallback(double percentDone, void* context)
//! {
//!     auto pVector = reinterpret_cast<std::vector<double>*>(context);
//!     pVector->push_back(percentDone);
//! }
//!
//! std::vector<double> context;
//! auto result = rcFpgaBaseLoad("fpgaimage.bin", BaseLoadCallback, &context);
//! @endcode
//! @param[in] percentDone The percent completed (0.0 - 100.0) towards the entire programming of the EEPROM.
//! @param[in] context The context that was provided in the associated call to one of the
//!                    \c xxxxFpgaBaseLoad() functions such as rcFpgaBaseLoad().
typedef void (*HIL_BASE_LOAD_CALLBACK)(_In_ double percentDone, _In_opt_ void* context);

//! @brief Information about an FPGA base FLASH's contents.
//!
typedef struct FLASH_INFO
{
    DWORD Version;                                    //!< The 32-bit "version" register that this flash content will report once loaded into an FPGA.
    DWORD Compatibility;                              //!< The 32-bit "compatibility" that this flash content will report once loaded into an FPGA.
    char  VersionString[HIL_MAX_VERSION_STRING_SIZE]; //!< String representation of the version.  This will match the FPGA's corresponding \c xxxFpgaVersionString() function.
    char  SourceHash[HIL_MAX_VERSION_STRING_SIZE];    //!< Source control hash value.  Typically a 12-digit hexadecimal value optionally followed by a plus(+) indicating a compilation from partially uncommitted sources.
} FLASH_INFO;

typedef FLASH_INFO* PFLASH_INFO;         //!< The address of a mutable (read/write) #FLASH_INFO structure.
typedef const FLASH_INFO* PCFLASH_INFO;  //!< The address of an immutable (read-only) #FLASH_INFO structure.
