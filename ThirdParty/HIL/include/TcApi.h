// INTEL CONFIDENTIAL
// Copyright 2016-2018 Intel Corporation All Rights Reserved.
//
// The source code contained or described herein and all documents related to
// the source code ("Material") are owned by Intel Corporation or its
// suppliers or licensors. Title to the Material remains with Intel
// Corporation or its suppliers and licensors. The Material contains trade
// secrets and proprietary and confidential information of Intel or its
// suppliers and licensors. The Material is protected by worldwide copyright
// and trade secret laws and treaty provisions. No part of the Material may be
// used, copied, reproduced, modified, published, uploaded, posted,
// transmitted, distributed, or disclosed in any way without Intel's prior
// express written permission.
//
// No license under any patent, copyright, trade secret or other intellectual
// property right is granted to or conferred upon you by disclosure or
// delivery of the Materials, either expressly, by implication, inducement,
// estoppel or otherwise. Any license under such intellectual property rights
// must be express and approved by Intel in writing.

//! @file
//! @brief APIs to control the HDMT thermal card.
#pragma once
#include "HilDefs.h"

#ifdef __cplusplus
extern "C" {
#endif

//! @brief Reads the board-level traceability values from the thermal card.
//!
//! This function reads the board-level traceability values from the thermal card.  The values are defined in the #BLT structure.
//!
//! @param[out] pBlt The address of a #BLT structure that will be populated with the data.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS tcBltBoardRead(_Out_ PBLT pBlt);

//! @brief Writes the board-level traceability values of the thermal card.
//!
//! This function writes the board-level traceability values of the thermal card.  The values are defined in the #BLT structure.
//!
//! The \c flags field allows this function to update individual values.  The flags can be ORed together.  Only the fields indicated by
//! \c flags need to be populated in the provided \c pBlt parameter.
//!
//! \par Updating the Full BLT - C
//! \code{.c}
//! #include <Windows.h>
//! #include "TcApi.h"
//! #include "string.h"
//!
//! int main()
//! {
//!     BLT blt;
//!     HIL_STATUS status;
//!     blt.Id = 0x0100000f;
//!     strcpy(blt.DeviceName,"DeviceName");
//!     strcpy(blt.VendorName,"INTEL");
//!     strcpy(blt.PartNumberAsBuilt,"AAxxxxxx-xxx");
//!     strcpy(blt.PartNumberCurrent,blt.PartNumberAsBuilt);
//!     strcpy(blt.SerialNumber,"12345678");
//!     strcpy(blt.ManufactureDate,"YYMMDD");
//!     status = tcBltBoardWrite(&blt, BF_ALL);
//!     if (status != HS_SUCCESS)
//!         return -1;
//!     return 0;
//! }
//! \endcode
//! \par Updating the Current Part Number - Python
//! \code{.py}
//! blt = hil.BLT()
//! blt.PartNumberCurrent = 'AAyyyyyy-yyy'
//! hil.tcBltBoardWrite(blt,hil.BF_PART_NUMBER_CURRENT)
//! # Note Python returns None for HIL_STATUS if successful and raises RuntimeError on failure.
//! \endcode
//!
//! \note HS_BLT_NOT_FOUND can be returned on an update (lack of #BF_ALL flag).
//!
//! @param[in] pBlt The address of a #BLT structure that contains the data to be written or updated.
//! @param[in] flags #BLT_FLAGS bits indicating what fields in \c pBlt are valid.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS tcBltBoardWrite(_In_ PCBLT pBlt, _In_ BLT_FLAGS flags);

#ifdef __cplusplus
}
#endif
