// INTEL CONFIDENTIAL
// Copyright 2018-2020 Intel Corporation All Rights Reserved.
//
// The source code contained or described herein and all documents related to
// the source code ("Material") are owned by Intel Corporation or its
// suppliers or licensors. Title to the Material remains with Intel
// Corporation or its suppliers and licensors. The Material contains trade
// secrets and proprietary and confidential information of Intel or its
// suppliers and licensors. The Material is protected by worldwide copyright
// and trade secret laws and treaty provisions. No part of the Material may be
// used, copied, reproduced, modified, published, uploaded, posted,
// transmitted, distributed, or disclosed in any way without Intel's prior
// express written permission.
//
// No license under any patent, copyright, trade secret or other intellectual
// property right is granted to or conferred upon you by disclosure or
// delivery of the Materials, either expressly, by implication, inducement,
// estoppel or otherwise. Any license under such intellectual property rights
// must be express and approved by Intel in writing.

//! @file
//! @brief APIs to control the HBI Mainboard.

#pragma once
#include "HilDefs.h"
#include "HbiMbDefs.h"

#ifdef __cplusplus
extern "C" {
#endif

//! @brief Initializes the components of the HBI mainboard for operation.
//!
//! @note Correctly programmed board BLT is a prerequisite for using this function as it relies on #BLT.PartNumberCurrent
//! to differentiate between fab revisions.
//!
//! This function initializes the components of the HBI mainboard for operation.  Calling this
//! function is required before using most other \c hbiMbXXXXXX() functions.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMbInit(void);

//! @brief Verifies an HBI mainboard is present.
//!
//! This function verifies an HBI mainboard is present.  It connects to and caches driver resources for use
//! by other \c hbiMbXXXXX functions. hbiMbDisconnect() is its complementary function and frees any cached resources.
//!
//! Neither hbiMbConnect() or hbiMbDisconnect() are required to be called to use the other \c hbiMbXXXXX functions. All
//! \c hbiMbXXXXX functions will allocate resources if they are not already allocated and free them if required to
//! perform their function.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMbConnect(void);

//! @brief Frees any resources cached from using the HBI mainboard functions.
//!
//! This function frees any resources associated with using the HBI mainboard HIL functions. hbiMbConnect()
//! is its complementary function.
//!
//! Neither hbiMbConnect() or hbiMbDisconnect() are required to be called to use the other \c hbiMbXXXXX functions. All
//! \c hbiMbXXXXX functions will allocate resources if they are not already allocated and free them if required to
//! perform their function.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMbDisconnect(void);

//! @brief Programs the vendor and product IDs of all USB devices on an HBI mainboard.
//!
//! This function programs the vendor and product IDs (VID/PIDs) of the FT240X, FT2232 devices on an HBI mainboard.
//! Correct VID/PIDs are required to ensure Windows Device Manager displays a proper description of the device,
//! and HIL requires correct VID/PIDs to ensure software can locate and communicate with the correct component.
//! @note FTDI devices have a limited number of updates, but this function is safe to call because it
//! only writes if the component programming is incorrect.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMbVidPidsSet(void);

//! @brief **[DEPRECATED]** Disables or enables the HBI failsafe timer.
//!
//! This function disables or enables the HBI failsafe timer.
//! @warning DEPRECATED! Use hilFailsafeDisable().
//! @note hbiMbFailsafePing() will also renable the timer, so starting any safety system software that pings will automatically re-enable the timer.
//! @param[in] disable \c TRUE disables the timer.  \c FALSE enables it.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMbFailsafeDisable(_In_ BOOL disable);

//! @brief **[DEPRECATED]** Sets the failsafe timer count value.
//!
//! This function sets the failsafe timer count value.  Once a time is set hbiMbFailsafePing() must be called
//! periodically before the timer expires or the tester will shut down.
//! @warning DEPRECATED! Use hilFailsafeDisable().
//! @param[in] seconds The count value in seconds.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMbFailsafeTimerSet(_In_ DWORD seconds);

//! @brief **[DEPRECATED]** Resets the failsafe timer count.
//!
//! This function resets the failsafe timer to the count set by hbiMbFailsafeTimerSet().
//! If the timer expires the tester shuts down, so calling this function periodically keeps the tester powered up.
//! @warning DEPRECATED! Use hilFailsafePing().
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMbFailsafePing(void);

//! @brief Reports the number of milliseconds that have elapsed since the the FPGA was last programmed.
//!
//! This function reports the number of milliseconds that have elapsed since the the FPGA was last programmed (through SPI flash or dynamically).
//!
//! @param[out] pUptime The uptime in milliseconds.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMbUptime(_Out_ PUINT64 pUptime);

//! @brief Enables or disables SPM alarm monitoring on the HBI Resource FPGA.
//!
//! This function enables or disables System Power Monitoring (SPM) alarm monitoring on the HBI Resource FPGA.  Supported alarms are defined in the #HBIMB_ALARMS enumeration.
//! @param[in] enable TRUE to enable alarms, FALSE to disable.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMbSpmAlarmEnable(_In_ BOOL enable);

//! @brief Configures the System Power Monitoring alarm limits.
//!
//! This function configures the System Power Monitor alarm limits.  There are separate limits for the individual supplies and for the cumulative
//! sum of the supplies.  The power alarm limits are set in watts and must be exceeded "filter count" times **in a row** to trigger an alarm.
//! This is to prevent short spikes in power from causing an alarm.
//!
//! @note This function returns #HS_UNSUPPORTED if current HBI RC FPGA image does not support the System Power Monitoring (SPM) feature.
//!
//! @param[in] filterCount    Individual power supply filter count(N).  If a supply is polled above \c limit watts N times **in a row** the alarm will trigger.
//! @param[in] limit          Individual power supply power limit in watts.
//! @param[in] sumFilterCount Cumulative power supply filter count(N).  If the cumulative power of the supplies is polled above \c sumLimit watts N times **in a row** the alarm will trigger.
//! @param[in] sumLimit       Cumulative power supply power limit in watts.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMbSpmAlarmLimitsSet(_In_ DWORD filterCount, _In_ double limit, _In_ DWORD sumFilterCount, _In_ double sumLimit);

//! @brief Reads the current System Power Monitoring alarm limits.
//!
//! This function reads the current System Power Monitoring alarm limits.  For descriptions of the limits see hbiMbSpmAlarmLimitsSet().
//!
//! @note This function returns #HS_UNSUPPORTED if current HBI RC FPGA image does not support the System Power Monitoring (SPM) feature.
//!
//! @param[out] pFilterCount    Current individual power supply filter count(N).  If a supply is polled above \c limit watts N times **in a row** the alarm will trigger.
//! @param[out] pLimit          Current individual power supply power limit in watts.
//! @param[out] pSumFilterCount Current cumulative power supply filter count(N).  If the cumulative power of the supplies is polled above \c sumLimit watts N times **in a row** the alarm will trigger.
//! @param[out] pSumLimit       Current cumulative power supply power limit in watts.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMbSpmAlarmLimitsGet(_Out_ LPDWORD pFilterCount, _Out_ double* pLimit, _Out_ LPDWORD pSumFilterCount, _Out_ double* pSumLimit);

//! @brief Blocks execution until an HBI Resource FPGA alarm is received.
//!
//! This function blocks execution until an HBI Resource FPGA alarm is received.  Supported alarms are defined in the #HBIMB_ALARMS enumeration.
//! This function can be unblocked by calling hbiMbAlarmWaitCancel() in another thread of execution.
//! @param[out] pAlarms Reported alarms are returned as bits set in this output DWORD.  See #HBIMB_ALARMS for bit definitions.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMbAlarmWait(_Out_ LPDWORD pAlarms);

//! @brief Aborts the blocking hbiMbAlarmWait() function.
//!
//! This function aborts the blocking hbiMbAlarmWait() function.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMbAlarmWaitCancel(void);

//! @brief Enables the hardware interrupt for HBI RCTC software triggers.
//!
//! This function enables the hardware interrupt for HBI RCTC software triggers.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMbTriggerEnable(void);

//! @brief Disables the hardware interrupt for HBI RCTC software triggers.
//!
//! This function disables the hardware interrupt for HBI RCTC software triggers.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMbTriggerDisable(void);

//! @brief [\b DEPRECATED] Waits for a software trigger interrupt from the HBI RCTC FPGA.
//!
//! This function waits for a software trigger interrupt from the HBI RCTC FPGA and returns with the trigger value.
//!
//! This function does not return until a trigger interrupt is received.  Since the function is blocking, the user
//! should call it from a worker thread.  The wait can be canceled via another thread by calling hbiMbTriggerWaitCancel().
//! @note In simulation mode, the trigger value returned will be 0x12345678.
//! @warning DEPRECATED FUNCTION! Use hbiMbTriggerWaitEx().
//! @param[out] pTrigger The value of the trigger.
//!                      | Bits  | Description                                                              |
//!                      | :---: | :----------------------------------------------------------------------- |
//!                      | 31:26 | Card type (0:Patgen, 1:DPS, 2:RCTC, 63:Broadcast, other values reserved) |
//!                      | 25:20 | DUT Domain ID                                                            |
//!                      |  19   | Software Trigger flag (0:not a software trigger, 1:software trigger)     |
//!                      | 18:0  | Payload (instrument defined)                                             |
//! @returns \ref HIL_STATUS
//! @see hbiMbTriggerEnable()
//! @see hbiMbTriggerDisable()
//! @see hbiMbTriggerWaitEx()
//! @see hbiMbTriggerWaitCancel()

HIL_API HIL_STATUS hbiMbTriggerWait(_Out_ LPDWORD pTrigger);

//! @brief Waits for a software trigger interrupt from the HBI RCTC FPGA.
//!
//! This function waits for a software trigger interrupt from the HBI RCTC FPGA and returns with the trigger value and channel.
//!
//! This function does not return until a trigger interrupt is received.  Since the function is blocking, the user
//! should call it from a worker thread.  The wait can be canceled via another thread by calling hbiMbTriggerWaitCancel().
//! @note In simulation mode, the trigger value returned will be 0x12345678abcdef0.
//! @param[out] pTrigger The value of the trigger.
//!                      | Bits  | Description                                                              |
//!                      | :---: | :----------------------------------------------------------------------- |
//!                      | 63:48 | Reserved                                                                 |
//!                      | 47:32 | Aurora channel                                                           |
//!                      | 31:26 | Card type (0:Patgen, 1:DPS, 2:RCTC, 63:Broadcast, other values reserved) |
//!                      | 25:20 | DUT Domain ID                                                            |
//!                      |  19   | Software Trigger flag (0:not a software trigger, 1:software trigger)     |
//!                      | 18:0  | Payload (instrument defined)                                             |
//! @returns \ref HIL_STATUS
//! @see hbiMbTriggerEnable()
//! @see hbiMbTriggerDisable()
//! @see hbiMbTriggerWaitCancel()
HIL_API HIL_STATUS hbiMbTriggerWaitEx(_Out_ PDWORDLONG pTrigger);

//! @brief Cancels a pending hbiMbTriggerWait().
//!
//! This function cancels a pending hbiMbTriggerWait().  Since hbiMbTriggerWait() is blocking, this must be performed from a separate thread of execution.
//! @returns \ref HIL_STATUS
//! @see hbiMbTriggerEnable()
//! @see hbiMbTriggerDisable()
//! @see hbiMbTriggerWaitEx()
HIL_API HIL_STATUS hbiMbTriggerWaitCancel(void);

//! @brief Retrieves the peer-to-peer base physical memory address.
//!
//! This function retrieves the HBI peer-to-peer base physical memory address from the resource FPGA required for DPS FPGA peer-to-peer capability.
//!
//! @param[out] pPhysicalAddress The physical base address of the peer-to-peer memory area.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMbPeerToPeerMemoryBase(_Out_ PLARGE_INTEGER pPhysicalAddress);

//! @brief Retrieves the HPS mailbox user memory address.
//!
//! This function retrieves the HPS mailbox user memory address.
//!
//! @param[out] ppBase The virtual address in user memory where the HPS mailbox memory is mapped.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMbHpsMailboxBase(_Out_ LPVOID* ppBase);

//! @brief Retrieves the user-mapped data bulk data streaming interface buffer for the HBI Resource FPGA.
//!
//! This function retrieves the user-mapped bulk data data streaming interface buffer for the HBI Resource FPGA.
//! @note This function exists for compatibility with older HBI_RCTC drivers that only implemented the bulk data streaming interface.
//!       Use hbiMbStreamingBufferEx() to retrieve later-implemented streaming interfaces such as SPM (System Power Monitoring).
//! @param[out] pAddress The returned virtual address of the buffer.
//! @param[out] pSize    The returned size of the buffer in bytes.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMbStreamingBuffer(_Out_ LPVOID* pAddress, _Out_ LPDWORD pSize);

//! @brief Retrieves a specified user-mapped data streaming interface buffer for the HBI Resource FPGA.
//!
//! This function retrieves a specified user-mapped data streaming interface buffer for the HBI Resource FPGA.
//! @note Serial Capture Stream (index = 1) and System Power Monitoring (index = 2) requires an HBI_RCTC driver
//!       dated 11/09/2020 or later to retrieve the buffer or \ref HS_INVALID_PARAMETER is returned.
//! @param[in]  index    The streaming interface buffer index to retrieve.  See the following table:
//!                      | index | Streaming buffer        |
//!                      | :---: | :---------------------- |
//!                      |   0   | Bulk Data               |
//!                      |   1   | Serial Capture Stream   |
//!                      |   2   | System Power Monitoring |
//! @param[out] pAddress The returned virtual address of the buffer.
//! @param[out] pSize    The returned size of the buffer in bytes.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMbStreamingBufferEx(_In_ UINT index, _Out_ LPVOID* pAddress, _Out_ LPDWORD pSize);

//! @brief Reads the version number of a CPLD on an HBI mainboard.
//!
//! This function reads the version number of a CPLD on an HBI mainboard.
//!
//! @note Correctly programmed board BLT is a prerequisite for using this function as it relies on #BLT.PartNumberCurrent
//! to differentiate between fab revisions.
//!
//! @param[in] cpldIndex Selects one of the four CPLDs. Consult the following table.
//!                      Note that reference designators may change in new fabs.
//!                      |  cpldIndex  | Reference designator | Comment                                       |
//!                      | :---------: | :------------------: | :-------------------------------------------: |
//!                      |      0      | U57                  | Resource/Thermal FPGA configuration CPLD      |
//!                      |      1      | U6                   | Pattern Generator FPGA configuration CPLD     |
//!                      |      2      | U51                  | Ring Multiplier FPGA configuration CPLD       |
//!                      |      3      | U291                 | MAX 10 CPLD                                   |
//! @param[out] pVersion Returns version value of the specified CPLD.  The pointer must not be NULL.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMbCpldVersion(_In_ UINT cpldIndex, _Out_ LPDWORD pVersion);

//! @brief Reads the version string of a CPLD on an HBI mainboard.
//!
//! This function reads the version of a CPLD on an HBI mainboard and converts it to an ASCII string representation.
//!
//! @note Correctly programmed board BLT is a prerequisite for using this function as it relies on #BLT.PartNumberCurrent
//! to differentiate between fab revisions.
//!
//! @param[in] cpldIndex Selects one of the four CPLDs. Consult the following table.
//!                      Note that reference designators may change in new fabs.
//!                      |  cpldIndex  | Reference designator | Comment                                       |
//!                      | :---------: | :------------------: | :-------------------------------------------: |
//!                      |      0      | U57                  | Resource/Thermal FPGA configuration CPLD      |
//!                      |      1      | U6                   | Pattern Generator FPGA configuration CPLD     |
//!                      |      2      | U51                  | Ring Multiplier FPGA configuration CPLD       |
//!                      |      3      | U291                 | MAX 10 CPLD                                   |
//! @param[out] pVersion Returns version string of the specified CPLD.  The pointer must not be NULL.
//! @param[in] length The length of the \c pVersion buffer.  It should be at least five characters to hold a null-terminated version string.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMbCpldVersionString(_In_ UINT cpldIndex, _Out_writes_z_(length) LPSTR pVersion, _In_ DWORD length);

//! @brief Programs a CPLD .JBC file into a CPLD on an HBI mainboard.
//!
//! This function programs a CPLD .JBC file into a CPLD on an HBI mainboard.
//!
//! @note Correctly programmed board BLT is a prerequisite for using this function as it relies on #BLT.PartNumberCurrent
//! to differentiate between fab revisions.
//!
//! @param[in] cpldIndex Selects one of the four CPLDs. Consult the following table.
//!                      Note that reference designators may change in new fabs.
//!                      |  cpldIndex  | Reference designator | Comment                                       |
//!                      | :---------: | :------------------: | :-------------------------------------------: |
//!                      |      0      | U57                  | Resource/Thermal FPGA configuration CPLD      |
//!                      |      1      | U6                   | Pattern Generator FPGA configuration CPLD     |
//!                      |      2      | U51                  | Ring Multiplier FPGA configuration CPLD       |
//!                      |      3      | U291                 | MAX 10 CPLD                                   |
//! @param[in] filename An ANSI string containing an absolute or relative (to the current directory) CPLD .JBC filename.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMbCpldProgram(_In_ UINT cpldIndex, _In_z_ LPCSTR filename);

//! @brief Programs a buffer containing .JBC file content into a CPLD on an HBI mainboard.
//!
//! This function programs a buffer containing .JBC file content into a CPLD on an HBI mainboard.
//!
//! @note Correctly programmed board BLT is a prerequisite for using this function as it relies on #BLT.PartNumberCurrent
//! to differentiate between fab revisions.
//!
//! @param[in] cpldIndex Selects one of four CPLDs. Consult the following table.
//!                      Note that reference designators may change in new fabs.
//!                      |  cpldIndex  | Reference designator | Comment                                       |
//!                      | :---------: | :------------------: | :-------------------------------------------: |
//!                      |      0      | U57                  | Resource/Thermal FPGA configuration CPLD      |
//!                      |      1      | U6                   | Pattern Generator FPGA configuration CPLD     |
//!                      |      2      | U51                  | Ring Multiplier FPGA configuration CPLD       |
//!                      |      3      | U291                 | MAX 10 CPLD                                   |
//! @param[in] pData The CPLD .JBC image data.
//! @param[in] length The length of the CPLD .JBC image data in bytes.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMbCpldBufferProgram(_In_ UINT cpldIndex, _In_reads_bytes_(length) LPCVOID pData, _In_ DWORD length);

//! @brief Compares an existing image within the selected CPLD on an HBI mainboard against a CPLD .JBC file.
//!
//! This function compares an existing image within the selected CPLD on an HBI mainboard against a CPLD .JBC file.
//!
//! @note Correctly programmed board BLT is a prerequisite for using this function as it relies on #BLT.PartNumberCurrent
//! to differentiate between fab revisions.
//!
//! @param[in] cpldIndex Selects one of the four CPLDs. Consult the following table.
//!                      Note that reference designators may change in new fabs.
//!                      |  cpldIndex  | Reference designator | Comment                                       |
//!                      | :---------: | :------------------: | :-------------------------------------------: |
//!                      |      0      | U57                  | Resource/Thermal FPGA configuration CPLD      |
//!                      |      1      | U6                   | Pattern Generator FPGA configuration CPLD     |
//!                      |      2      | U51                  | Ring Multiplier FPGA configuration CPLD       |
//!                      |      3      | U291                 | MAX 10 CPLD                                   |
//! @param[in] imageFilename An ANSI string containing an absolute or relative (to the current directory) CPLD .JBC filename to be compared against.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMbCpldVerify(_In_ UINT cpldIndex, _In_z_ LPCSTR imageFilename);

//! @brief Compares an existing image within the selected CPLD on an HBI mainboard against a buffer containing .JBC file content.
//!
//! This function compares an existing image within the selected CPLD on an HBI mainboard against a buffer containing .JBC file content.
//!
//! @note Correctly programmed board BLT is a prerequisite for using this function as it relies on #BLT.PartNumberCurrent
//! to differentiate between fab revisions.
//!
//! @param[in] cpldIndex Selects one of four CPLDs. Consult the following table.
//!                      Note that reference designators may change in new fabs.
//!                      |  cpldIndex  | Reference designator | Comment                                       |
//!                      | :---------: | :------------------: | :-------------------------------------------: |
//!                      |      0      | U57                  | Resource/Thermal FPGA configuration CPLD      |
//!                      |      1      | U6                   | Pattern Generator FPGA configuration CPLD     |
//!                      |      2      | U51                  | Ring Multiplier FPGA configuration CPLD       |
//!                      |      3      | U291                 | MAX 10 CPLD                                   |
//! @param[in] pData The CPLD .JBC image data to be compared against.
//! @param[in] length The length of the CPLD .JBC image data in bytes.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMbCpldBufferVerify(_In_ UINT cpldIndex, _In_reads_bytes_(length) LPCVOID pData, _In_ DWORD length);

//! @brief Writes data to contiguous registers within the MAX10 CPLD on an HBI mainboard.
//!
//! This function writes data to contiguous registers within the MAX10 CPLD on an HBI mainboard.
//!
//! @param[in] reg    The first register in the CPLD to which the data in the \c pData buffer will be written.
//! @param[in] pData  A data buffer containing 16-bit words to write to the CPLD.
//! @param[in] length The number of words in the \c pData buffer.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMbMax10CpldWrite(_In_ WORD reg, _In_reads_(length) const WORD* pData, _In_ WORD length);

//! @brief Reads data from contiguous registers within the MAX10 CPLD on an HBI mainboard.
//!
//! This function reads data from contiguous registers within the MAX10 CPLD on an HBI mainboard.
//!
//! @param[in] reg    The first register in the CPLD from which the data will be read.
//! @param[out] pData A pointer to a data buffer in which the read data will be stored. Note that this buffer must have at least \c length * 2
//!                   bytes allocated in order to complete successfully.
//! @param[in] length The number of words to read from the CPLD.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMbMax10CpldRead(_In_ WORD reg, _Out_writes_all_(length) LPWORD pData, _In_ WORD length);

//! @brief Reads the version number of a FPGA on an HBI mainboard.
//!
//! This function reads the version number of a FPGA on an HBI mainboard.  The returned 32-bit value is in the
//! format \c 0xMMMMmmmm where the upper 16-bits are the major version and lower 16-bits are the minor version.
//! NOTE: the compatibility bits are in separate registers.
//!
//! For example, 0x00050001 is version 5.1.
//! @param[in] fpgaIndex Selects one of the three FPGAs. Consult the following table.
//!                      |  fpgaIndex  |       FPGA        |
//!                      | :---------: | :---------------: |
//!                      |      0      | Resource/Thermal  |
//!                      |      1      | Pattern Generator |
//!                      |      2      | Ring Multiplier   |
//! @param[out] pVersion The address of a DWORD of memory.  It may not be NULL.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMbFpgaVersion(_In_ UINT fpgaIndex, _Out_ LPDWORD pVersion);

//! @brief Reads version string of a FPGA on an HBI mainboard.
//!
//! This function reads the version register of a FPGA on an HBI mainboard and converts it to an ASCII string representation.  As of HIL 7.0, it also
//! appends the compatibility register value.  The format is \c "MM.mm-C" where \c MM is the major version, \c mm is the minor version, and \c C is the
//! compatibility value.  All values are unsigned decimal numbers.
//!
//! @param[in] fpgaIndex Selects one of the three FPGAs. Consult the following table.
//!                      |  fpgaIndex  |       FPGA        |
//!                      | :---------: | :---------------: |
//!                      |      0      | Resource/Thermal  |
//!                      |      1      | Pattern Generator |
//!                      |      2      | Ring Multiplier   |
//! @param[out] pVersion Returns the version string.  The pointer must not be NULL.
//! @param[in] length The length of the \c pVersion buffer.  It should be at least #HIL_MAX_VERSION_STRING_SIZE characters to hold a null-terminated version string.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMbFpgaVersionString(_In_ UINT fpgaIndex, _Out_writes_z_(length) LPSTR pVersion, _In_ DWORD length);

//! @brief Returns an HBI mainboard FPGA's base FLASH content information.
//!
//! This function returns an HBI mainboard FPGA's base FLASH content information.
//! @param[in] fpgaIndex Selects the appropriate SPI FLASH for one of the three FPGAs. Consult the following table.
//!                      |  fpgaIndex  |       FPGA        |
//!                      | :---------: | :---------------: |
//!                      |      0      | Resource/Thermal  |
//!                      |      1      | Pattern Generator |
//!                      |      2      | Ring Multiplier   |
//! @param[out] pInfo Returns FLASH_INFO structure describing the FLASH contents.  The pointer must not be NULL.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMbFpgaBaseInfo(_In_ UINT fpgaIndex, _Out_ PFLASH_INFO pInfo);

//! @brief Returns an HBI mainboard FPGA's compatibility register value.
//!
//! This function returns an HBI mainboard FPGA's compatibility register value.
//! @param[in] fpgaIndex Selects the appropriate SPI FLASH for one of the three FPGAs. Consult the following table.
//!                      |  fpgaIndex  |       FPGA        |
//!                      | :---------: | :---------------: |
//!                      |      0      | Resource/Thermal  |
//!                      |      1      | Pattern Generator |
//!                      |      2      | Ring Multiplier   |
//! @param[out] pData Returns the value of the FPGA's compatibility register.  The pointer must not be NULL.  At a minimum, bits 0-3 of the returned value
//!                   indicates hardware capability.  An FPGA that reports a particular value in bits 0-3 should only be overwritten with a compatible image.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMbFpgaCompatibility(_In_ UINT fpgaIndex, _Out_ LPDWORD pData);

//! @brief Returns the source hash used to identify the source code that build the indicated FPGA.
//!
//! This function returns the source hash used to identify the source code that build the indicated FPGA.  The format of this field
//! is a 48-bit hexadecimal value with an optional trailing plus(+) indicating it was built from uncommitted sources.
//! @param[in] fpgaIndex Selects the appropriate SPI FLASH for one of the three FPGAs. Consult the following table.
//!                      |  fpgaIndex  |       FPGA        |
//!                      | :---------: | :---------------: |
//!                      |      0      | Resource/Thermal  |
//!                      |      1      | Pattern Generator |
//!                      |      2      | Ring Multiplier   |
//! @param[out] pHash  Returns a string representation of the FPGA's source control hash. The pointer must not be NULL.
//! @param[in] length The length of the \c pHash buffer.  It should be at least #HIL_MAX_VERSION_STRING_SIZE characters to hold a null-terminated version string.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMbFpgaSourceHash(_In_ UINT fpgaIndex, _Out_writes_z_(length) LPSTR pHash, _In_ DWORD length);

//! @brief Returns the 64-bit value of the indicated FPGA's chip ID registers.
//!
//! This function returns the 64-bit value of the indicated FPGA's chip ID registers that uniquely identifies the FPGA.
//! @param[in] fpgaIndex Selects the appropriate SPI FLASH for one of the three FPGAs. Consult the following table.
//!                      |  fpgaIndex  |       FPGA        |
//!                      | :---------: | :---------------: |
//!                      |      0      | Resource/Thermal  |
//!                      |      1      | Pattern Generator |
//!                      |      2      | Ring Multiplier   |
//! @param[out] pChipId Returns the 64-bit chip ID.  The pointer must not be NULL.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMbFpgaChipId(_In_ UINT fpgaIndex, _Out_ PDWORD64 pChipId);

//! @brief Disables the PCI device driver for a FPGA on an HBI mainboard.
//!
//! This function disables the PCI device driver for a FPGA on an HBI mainboard.
//! It should be called before using functions such as hbiMbFpgaLoad() that affect the hardware used by the driver.
//! @param[in] fpgaIndex Selects one of the three FPGAs. Consult the following table.
//!                      |  fpgaIndex  |       FPGA        |
//!                      | :---------: | :---------------: |
//!                      |      0      | Resource/Thermal  |
//!                      |      1      | Pattern Generator |
//!                      |      2      | Ring Multiplier   |
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMbDeviceDisable(_In_ UINT fpgaIndex);

//! @brief Enables the PCI device driver for a FPGA on an HBI mainboard.
//!
//! This function enables the PCI device driver for a FPGA on an HBI mainboard.
//! It should be called after using functions such as hbiMbFpgaLoad() that affect the hardware used by the driver.
//! @param[in] fpgaIndex Selects one of the three FPGAs. Consult the following table.
//!                      |  fpgaIndex  |       FPGA        |
//!                      | :---------: | :---------------: |
//!                      |      0      | Resource/Thermal  |
//!                      |      1      | Pattern Generator |
//!                      |      2      | Ring Multiplier   |
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMbDeviceEnable(_In_ UINT fpgaIndex);

//! @brief Loads an FPGA binary image file into a FPGA on an HBI mainboard.
//!
//! This function loads an FPGA binary image file into a FPGA on an HBI mainboard.
//! This image is volatile and the FPGA will load its original base program from its SPI FLASH after a cold reboot.
//! @warning HBI mainboard device drivers are sensitive to FPGA changes.  If they are present, wrap this call in hbiMbDeviceDisable() and hbiMbDeviceEnable().
//!
//! @note Correctly programmed board BLT is a prerequisite for using this function as it relies on #BLT.PartNumberCurrent
//! to differentiate between fab revisions.
//!
//! @param[in] fpgaIndex Selects one of the three FPGAs. Consult the following table.
//!                      |  fpgaIndex  |       FPGA        |
//!                      | :---------: | :---------------: |
//!                      |      0      | Resource/Thermal  |
//!                      |      1      | Pattern Generator |
//!                      |      2      | Ring Multiplier   |
//! @param[in] filename An ANSI string containing an absolute or relative (to the current directory) FPGA binary filename.  As of HIL 7.0,
//!                     there must also be a <code><i>\<filename\></i>.mta</code> metadata file in the same directory as <code><i>\<filename\></i></code>.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMbFpgaLoad(_In_ UINT fpgaIndex, _In_z_ LPCSTR filename);

//! @brief Loads an FPGA image file into the selected base SPI FLASH used to boot initialize a FPGA on an HBI mainboard.
//!
//! This function loads an FPGA image file into the selected base SPI FLASH used to boot initialize a FPGA on an HBI mainboard.
//!
//! The callback and context parameters are used to report progress back during a long operation.  See #HIL_BASE_LOAD_CALLBACK
//! for details.
//!
//! @note Correctly programmed board BLT is a prerequisite for using this function as it relies on #BLT.PartNumberCurrent
//! to differentiate between fab revisions.
//!
//! @param[in] fpgaIndex Selects the appropriate SPI FLASH for one of the three FPGAs. Consult the following table.
//!                      |  fpgaIndex  |       FPGA        |
//!                      | :---------: | :---------------: |
//!                      |      0      | Resource/Thermal  |
//!                      |      1      | Pattern Generator |
//!                      |      2      | Ring Multiplier   |
//! @param[in] filename An ANSI string containing an absolute or relative (to the current directory) FPGA binary filename.  As of HIL 7.0,
//!                     there must also be a <code><i>\<filename\></i>.mta</code> metadata file in the same directory as <code><i>\<filename\></i></code>.
//! @param[in] callback The callback function to be called with progress updates while programming the SPI FLASH.  Value can be NULL.
//! @param[in] context  The context that will be passed back as an argument in the #HIL_BASE_LOAD_CALLBACK callback function.  Value can be NULL.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMbFpgaBaseLoad(_In_ UINT fpgaIndex, _In_z_ LPCSTR filename, _In_opt_ HIL_BASE_LOAD_CALLBACK callback, _In_opt_ void* context);

//! @brief Validates the selected base SPI FLASH contents on an HBI mainboard.
//!
//! This function validates the selected base SPI FLASH contents on an HBI mainboard and optionally dumps the contents to a file.
//!
//! @note Correctly programmed board BLT is a prerequisite for using this function as it relies on #BLT.PartNumberCurrent
//! to differentiate between fab revisions.
//!
//! @param[in] fpgaIndex Selects the appropriate SPI FLASH for one of the three FPGAs. Consult the following table.
//!                      |  fpgaIndex  |       FPGA        |
//!                      | :---------: | :---------------: |
//!                      |      0      | Resource/Thermal  |
//!                      |      1      | Pattern Generator |
//!                      |      2      | Ring Multiplier   |
//! @param[in] dumpFilename An ANSI string containing an absolute or relative (to the current directory) binary dump filename.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMbFpgaBaseVerify(_In_ UINT fpgaIndex, _In_opt_z_ LPCSTR dumpFilename);

//! @brief Loads an FPGA binary image buffer into a FPGA on an HBI mainboard.
//!
//! This function loads an FPGA binary image buffer into a FPGA on an HBI mainboard.  This image is volatile and the FPGA
//! will load its original base program from its SPI FLASH after a cold reboot.
//! @warning HBI mainboard device drivers are sensitive to FPGA changes.  If they are present, wrap this call in hbiMbDeviceDisable() and hbiMbDeviceEnable().
//!
//! @note Correctly programmed board BLT is a prerequisite for using this function as it relies on #BLT.PartNumberCurrent
//! to differentiate between fab revisions.
//!
//! @param[in] fpgaIndex Selects one of the three FPGAs. Consult the following table.
//!                      |  fpgaIndex  |       FPGA        |
//!                      | :---------: | :---------------: |
//!                      |      0      | Resource/Thermal  |
//!                      |      1      | Pattern Generator |
//!                      |      2      | Ring Multiplier   |
//! @param[in] pData      The FPGA image data.
//! @param[in] length     The length of the FPGA image data in bytes.
//! @param[in] pMetaData  The FPGA image metadata.  This data is in a side-by-side file with .MTA extension with the original FPGA image.  It is
//!                       loaded automatically by hbiMbFpgaLoad() but needs to be read and provided to this version of the function.
//! @param[in] metaLength The length of the FPGA image metadata in bytes.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMbFpgaBufferLoad(_In_ UINT fpgaIndex, _In_reads_bytes_(length) LPCVOID pData, _In_ DWORD length, _In_reads_bytes_opt_(metaLength) LPCVOID pMetaData, _In_ DWORD metaLength);

//! @brief Loads an FPGA image buffer into the selected base SPI FLASH used to boot initialize a FPGA on an HBI mainboard.
//!
//! This function loads an FPGA image buffer into the selected base SPI FLASH used to boot initialize a FPGA on an HBI mainboard.
//!
//! The callback and context parameters are used to report progress back during a long operation.  See #HIL_BASE_LOAD_CALLBACK
//! for details.
//!
//! @note Correctly programmed board BLT is a prerequisite for using this function as it relies on #BLT.PartNumberCurrent
//! to differentiate between fab revisions.
//!
//! @param[in] fpgaIndex Selects the appropriate SPI FLASH for one of the three FPGAs. Consult the following table.
//!                      |  fpgaIndex  |       FPGA        |
//!                      | :---------: | :---------------: |
//!                      |      0      | Resource/Thermal  |
//!                      |      1      | Pattern Generator |
//!                      |      2      | Ring Multiplier   |
//! @param[in] pData      The FPGA image data.
//! @param[in] length     The length of the FPGA image data in bytes.
//! @param[in] pMetaData  The FPGA image metadata.  This data is in a side-by-side file with .MTA extension with the original FPGA image.  It is
//!                       loaded automatically by hbiMbFpgaBaseLoad() but needs to be read and provided to this version of the function.
//! @param[in] metaLength The length of the FPGA image metadata in bytes.
//! @param[in] callback   The callback function to be called with progress updates while programming the SPI FLASH.  Value can be NULL.
//! @param[in] context    The context that will be passed back as an argument in the #HIL_BASE_LOAD_CALLBACK callback function.  Value can be NULL.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMbFpgaBaseBufferLoad(_In_ UINT fpgaIndex, _In_reads_bytes_(length) LPCVOID pData, _In_ DWORD length, _In_reads_bytes_opt_(metaLength) LPCVOID pMetaData, _In_ DWORD metaLength, _In_opt_ HIL_BASE_LOAD_CALLBACK callback, _In_opt_ void* context);

//! @brief Reads a 32-bit register in one of the BAR regions of a FPGA on an HBI mainboard.
//!
//! This function reads a 32-bit register in one of the BAR regions of a FPGA on an HBI mainboard.
//! @param[in] fpgaIndex Selects one of the three FPGAs. Consult the following table.
//!                      |  fpgaIndex  |       FPGA        |
//!                      | :---------: | :---------------: |
//!                      |      0      | Resource/Thermal  |
//!                      |      1      | Pattern Generator |
//!                      |      2      | Ring Multiplier   |
//! @param[in] bar A valid PCI base address register region to access.
//! @param[in] offset The DWORD-aligned offset address in BAR0 memory to read.
//! @param[out] pData The address of a DWORD to contain the result.  It cannot be NULL.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMbBarRead(_In_ UINT fpgaIndex, _In_ UINT bar, _In_ DWORD offset, _Out_ LPDWORD pData);

//! @brief Writes a 32-bit register in one of the BAR regions of a FPGA on an HBI mainboard.
//!
//! This function writes a 32-bit register in one the BAR regions of a FPGA on an HBI mainboard.
//! @param[in] fpgaIndex Selects one of the three FPGAs. Consult the following table.
//!                      |  fpgaIndex  |       FPGA        |
//!                      | :---------: | :---------------: |
//!                      |      0      | Resource/Thermal  |
//!                      |      1      | Pattern Generator |
//!                      |      2      | Ring Multiplier   |
//! @param[in] bar A valid PCI base address register region to access.
//! @param[in] offset The DWORD-aligned offset address in BAR0 memory to write.
//! @param[in] data The DWORD value to write.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMbBarWrite(_In_ UINT fpgaIndex, _In_ UINT bar, _In_ DWORD offset, _In_ DWORD data);

//! @brief Reads from the DDR memory of a FPGA on an HBI mainboard.
//!
//! This function reads from the DDR memory of a FPGA on an HBI mainboard.
//! @note The pattern generator FPGA accesses five 64GB SO DIMMs.  If smaller DIMMs are used the 320GB address is non-contiguous.  Each DIMM starts on a 64GB boundary.
//! @param[in] fpgaIndex Selects one of the three FPGAs. Consult the following table.
//!                      |  fpgaIndex  |       FPGA        |  DDR Size |
//!                      | :---------: | :---------------: | :-------: |
//!                      |      0      | Resource/Thermal  |     2GB   |
//!                      |      1      | Pattern Generator |   320GB   |
//!                      |      2      | Ring Multiplier   |     2GB   |
//! @param[in] address The zero-based address within the DDR memory to read.  This must be a multiple of four and the read must be within the DDR address space.
//! @param[out] pData The address of a buffer to hold the result.
//! @param[in] length The length of the \c pData buffer in bytes.  This must be a multiple of four and the read must be within the DDR address space.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMbDmaRead(_In_ UINT fpgaIndex, _In_ INT64 address, _Out_writes_bytes_all_(length) LPVOID pData, _In_ DWORD length);

//! @brief Writes to the DDR memory of a FPGA on an HBI mainboard.
//!
//! This function writes to the DDR memory of of a FPGA on an HBI mainboard.
//! @note The pattern generator FPGA accesses five 64GB SO DIMMs.  If smaller DIMMs are used the 320GB address is non-contiguous.  Each DIMM starts on a 64GB boundary.
//! @param[in] fpgaIndex Selects one of the three FPGAs. Consult the following table.
//!                      |  fpgaIndex  |       FPGA        |  DDR Size |
//!                      | :---------: | :---------------: | :-------: |
//!                      |      0      | Resource/Thermal  |     2GB   |
//!                      |      1      | Pattern Generator |   320GB   |
//!                      |      2      | Ring Multiplier   |     2GB   |
//! @param[in] address The zero-based address within the DDR memory to write.  This must be a multiple of four and the write must be within the DDR address space.
//! @param[in] pData The address of a buffer containing the data to write.
//! @param[in] length The length of the \c pData buffer in bytes.  This must be a multiple of four and the write must be within the DDR address space.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMbDmaWrite(_In_ UINT fpgaIndex, _In_ INT64 address, _In_reads_bytes_(length) const void* pData, _In_ DWORD length);

//! @brief Reads from HBI mainboard HPS's SDRAM memory space.
//!
//! This function reads from HBI mainboard HPS's 0.5GB SDRAM memory space.
//!
//! @note This function returns #HS_UNSUPPORTED on HBI mainboard Fab A, B.
//!
//! @note Correctly programmed board BLT is a prerequisite for using this function as it relies on #BLT.PartNumberCurrent
//! to differentiate between fab revisions.
//!
//! @param[in] address The zero-based address within the SDRAM memory to read.  This must be a multiple of four and the read must be within the SDRAM address space.
//! @param[out] pData The address of a buffer to hold the result.
//! @param[in] length The length of the \c pData buffer in bytes.  This must be a multiple of four and the read must be within the SDRAM address space.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMbHpsDmaRead(_In_ INT64 address, _Out_writes_bytes_all_(length) LPVOID pData, _In_ DWORD length);

//! @brief Writes to HBI mainboard HPS's SDRAM memory space.
//!
//! This function writes to HBI mainboard HPS's 0.5GB SDRAM memory space.
//!
//! @note This function returns #HS_UNSUPPORTED on HBI mainboard Fab A, B.
//!
//! @note Correctly programmed board BLT is a prerequisite for using this function as it relies on #BLT.PartNumberCurrent
//! to differentiate between fab revisions.
//!
//! @param[in] address The zero-based address within the SDRAM memory to write.  This must be a multiple of four and the write must be within the SDRAM address space.
//! @param[in] pData The address of a buffer containing the data to write.
//! @param[in] length The length of the \c pData buffer in bytes.  This must be a multiple of four and the write must be within the SDRAM address space.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMbHpsDmaWrite(_In_ INT64 address, _In_reads_bytes_(length) LPCVOID pData, _In_ DWORD length);

//! @brief Reads the board-level traceability values from an HBI mainboard.
//!
//! This function reads the board-level traceability values from an HBI mainboard.
//!
//! @note Correctly programmed board BLT is a prerequisite for using this function as it relies on #BLT.PartNumberCurrent
//! to differentiate between fab revisions.
//!
//! @param[out] pBlt The address of a #BLT structure that will be populated with the BLT data.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMbBltBoardRead(_Out_ PBLT pBlt);

//! @brief Writes the board-level traceability values of an HBI mainboard.
//!
//! This function writes the board-level traceability values of an HBI mainboard.
//!
//! @note Correctly programmed board BLT is a prerequisite for using this function as it relies on #BLT.PartNumberCurrent
//! to differentiate between fab revisions.
//!
//! The \c flags field allows this function to update individual values.  The flags can be ORed together.  Only the fields indicated by
//! \c flags need to be populated in the provided \c pBlt parameter.
//!
//! \par Updating the Full BLT - C
//! \code{.c}
//! #include <Windows.h>
//! #include "HbiMbApi.h"
//! #include "string.h"
//!
//! int main()
//! {
//!     BLT blt;
//!     HIL_STATUS status;
//!     blt.Id = 0x100000FF; /* NOT the actual card ID */
//!     strcpy(blt.DeviceName,"DeviceName");
//!     strcpy(blt.VendorName,"INTEL");
//!     strcpy(blt.PartNumberAsBuilt,"AAxxxxxx-xxx");
//!     strcpy(blt.PartNumberCurrent,blt.PartNumberAsBuilt);
//!     strcpy(blt.SerialNumber,"12345678");
//!     strcpy(blt.ManufactureDate,"YYMMDD");
//!     status = hbiMbBltBoardWrite(&blt, BF_ALL);
//!     if (status != HS_SUCCESS)
//!         return -1;
//!     return 0;
//! }
//! \endcode
//! \par Updating the Current Part Number - Python
//! \code{.py}
//! blt = hil.BLT()
//! blt.PartNumberCurrent = 'AAyyyyyy-yyy'
//! hil.hbiMbBltBoardWrite(blt,hil.BF_PART_NUMBER_CURRENT)
//! # Note Python returns None for HIL_STATUS if successful and raises RuntimeError on failure.
//! \endcode
//!
//! \note HS_BLT_NOT_FOUND can be returned on an update (lack of #BF_ALL flag).
//!
//! @param[in] pBlt The address of a #BLT structure that contains the data to be written or updated.
//! @param[in] flags #BLT_FLAGS bits indicating what fields in \c pBlt are valid.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMbBltBoardWrite(_In_ PCBLT pBlt, _In_ BLT_FLAGS flags);

//! @brief Reads the instrument-level traceability values from an HBI mainboard.
//!
//! This function reads the instrument-level traceability values from an HBI mainboard.
//!
//! @note Correctly programmed board BLT is a prerequisite for using this function as it relies on #BLT.PartNumberCurrent
//! to differentiate between fab revisions.
//!
//! @param[out] pBlt The address of a #BLT structure that will be populated with the BLT data.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMbBltInstrumentRead(_Out_ PBLT pBlt);

//! @brief Writes the instrument-level traceability values of an HBI mainboard.
//!
//! This function writes the instrument-level traceability values of an HBI mainboard.
//!
//! @note Correctly programmed board BLT is a prerequisite for using this function as it relies on #BLT.PartNumberCurrent
//! to differentiate between fab revisions.
//!
//! The \c flags field allows this function to update individual values.  The flags can be ORed together.  Only the fields indicated by
//! \c flags need to be populated in the provided \c pBlt parameter.
//!
//! \par Updating the Full BLT - C
//! \code{.c}
//! #include <Windows.h>
//! #include "HbiMbApi.h"
//! #include "string.h"
//!
//! int main()
//! {
//!     BLT blt;
//!     HIL_STATUS status;
//!     blt.Id = 0x10000320;
//!     strcpy(blt.DeviceName,"HBIMainInstrument");
//!     strcpy(blt.VendorName,"INTEL");
//!     strcpy(blt.PartNumberAsBuilt,"AAxxxxxx-xxx");
//!     strcpy(blt.PartNumberCurrent,blt.PartNumberAsBuilt);
//!     strcpy(blt.SerialNumber,"12345678");
//!     strcpy(blt.ManufactureDate,"YYMMDD");
//!     status = hbiMbBltInstrumentWrite(&blt, BF_ALL);
//!     if (status != HS_SUCCESS)
//!         return -1;
//!     return 0;
//! }
//! \endcode
//! \par Updating the Current Part Number - Python
//! \code{.py}
//! blt = hil.BLT()
//! blt.PartNumberCurrent = 'AAyyyyyy-yyy'
//! hil.hbiMbBltInstrumentWrite(blt,hil.BF_PART_NUMBER_CURRENT)
//! # Note Python returns None for HIL_STATUS if successful and raises RuntimeError on failure.
//! \endcode
//!
//! \note HS_BLT_NOT_FOUND can be returned on an update (lack of #BF_ALL flag).
//!
//! @param[in] pBlt The address of a #BLT structure that contains the data to be written or updated.
//! @param[in] flags #BLT_FLAGS bits indicating what fields in \c pBlt are valid.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMbBltInstrumentWrite(_In_ PCBLT pBlt, _In_ BLT_FLAGS flags);

//! @brief Reads raw data from an HBI mainboard's BLT EEPROM.
//!
//! This function reads raw data from an HBI mainboard's BLT EEPROM.
//!
//! @note Correctly programmed board BLT is a prerequisite for using this function as it relies on #BLT.PartNumberCurrent
//! to differentiate between fab revisions.
//!
//! @param[out] pEepromData The output buffer.
//! @param[in,out] pLength On input, the size of the output buffer referenced by \c pData.  On output, the size of the EEPROM data.
//! @retval HS_INSUFFICIENT_BUFFER The buffer provided was too small.  \c pLength will indicate the minimum buffer size required.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMbBltEepromRead(_Out_writes_bytes_to_(_Old_(*pLength), *pLength) LPVOID pEepromData, _Inout_ LPDWORD pLength);

//! @brief Writes data to the 24LC512 (EEPROM) device on an HBI mainboard.
//!
//! This function writes data to the 24LC512 (EEPROM) device on an HBI mainboard.
//!
//! @note Correctly programmed board BLT is a prerequisite for using this function as it relies on #BLT.PartNumberCurrent
//! to differentiate between fab revisions.
//!
//! @param[in] address The location of the first byte in the EEPROM to which the data in the \c pData buffer will be written.
//! @param[in] pData   A data buffer containing data to write to the EEPROM device.
//! @param[in] length  The byte length of the \c pData buffer.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMbEepromWrite(_In_ DWORD address, _In_reads_bytes_(length) const void* pData, _In_ DWORD length);

//! @brief Reads data from the 24LC512 (EEPROM) device on an HBI mainboard.
//!
//! This function reads data from the 24LC512 (EEPROM) device on an HBI mainboard.
//!
//! @note Correctly programmed board BLT is a prerequisite for using this function as it relies on #BLT.PartNumberCurrent
//! to differentiate between fab revisions.
//!
//! @param[in] address The location of the first byte in the EEPROM from which the data will be read.
//! @param[out] pData  A pointer to a data buffer in which the read data will be stored. Note that this buffer must have at least \c length
//!                    bytes allocated in order to complete successfully.
//! @param[in] length  The number of bytes to read from the EEPROM.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMbEepromRead(_In_ DWORD address, _Out_writes_bytes_all_(length) LPVOID pData, _In_ DWORD length);

//! @brief Issues a read command to the PMBUS interface on a LTM4680 Power Manager device on an HBI mainboard.
//!
//! This function issues a read command to the PMBUS interface on a LTM4680 Power Manager device on an HBI mainboard.
//!
//! @note Of the five LTM4680 devices on an HBI mainboard Fab A, B, only U25 and U13 can be controlled through HIL.
//! Other chips do not have their I2C interfaces exposed and hence return #HS_UNSUPPORTED.
//!
//! @note Correctly programmed board BLT is a prerequisite for using this function as it relies on #BLT.PartNumberCurrent
//! to differentiate between fab revisions.
//!
//! @param[in] chip A zero-based chip index.  Consult the following table.
//!                 Note that reference designators may change in new fabs.
//!                 |  Chip   | Fab A,B | Fab C |
//!                 | :-----: | :-----: | :---: |
//!                 |    0    |   U25   |  U25  |
//!                 |    1    |   U13   |  U13  |
//!                 |    2    |    -    |  U8   |
//!                 |    3    |    -    |  U44  |
//!                 |    4    |    -    |  U87  |
//!                 |    5    |    -    |  U198 |
//! @param[in] page       Selects a page (corresponding to Channel 0 or Channel 1) for any command that supports paging.
//!                       This parameter may be -1 for commands that do not support paging.
//! @param[in] command    PMBUS command to read.  See the device spec sheet for valid commands.
//! @param[out] pReadData A data buffer of bytes to hold the data read from the associated \c command.
//!                       Note that the bytes received from the device are located sequentially
//!                       in the buffer in the order they were received with the first byte being located
//!                       directly at \c pReadData.  This parameter cannot be NULL since all reads return at least one byte.
//! @param[in] readLength The byte length of the \c pReadData buffer.  Note that this parameter cannot be zero
//!                       since all reads return at least one byte.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMbLtm4680PmbusRead(_In_ UINT chip, _In_ INT page, _In_ BYTE command, _Out_writes_bytes_all_(readLength) LPVOID pReadData, _In_ DWORD readLength);

//! @brief Issues a write command to the PMBUS interface on one of the five LTM4680 Power Manager devices on an HBI mainboard.
//!
//! This function issues a write command to the PMBUS interface on one of the five LTM4680 Power Manager devices on an HBI mainboard.
//!
//! @note Of the five LTM4680 devices on an HBI mainboard Fab A, B, only U25 and U13 can be controlled through HIL.
//! Other chips do not have their I2C interfaces exposed and hence return #HS_UNSUPPORTED.
//!
//! @note Correctly programmed board BLT is a prerequisite for using this function as it relies on #BLT.PartNumberCurrent
//! to differentiate between fab revisions.
//!
//! @param[in] chip A zero-based chip index.  Consult the following table.
//!                 Note that reference designators may change in new fabs.
//!                 |  Chip   | Fab A,B | Fab C |
//!                 | :-----: | :-----: | :---: |
//!                 |    0    |   U25   |  U25  |
//!                 |    1    |   U13   |  U13  |
//!                 |    2    |    -    |  U8   |
//!                 |    3    |    -    |  U44  |
//!                 |    4    |    -    |  U87  |
//!                 |    5    |    -    |  U198 |
//! @param[in] page      Selects a page (corresponding to Channel 0 or Channel 1) for any command that supports paging.
//!                      This parameter may be -1 for commands that do not support paging.
//! @param[in] command   PMBUS command to write. See the device spec sheet for valid commands.
//! @param[in] pCmdData  A data buffer of additional bytes to write for the associated \c command.
//!                      Note that the bytes are sent to the device sequentially starting with the first
//!                      byte pointed to by \c pCmdData.  The command byte should not be included in this
//!                      buffer as it is already supplied in \c command.  Note that this parameter may be
//!                      NULL for commands that have no data associated with them.
//! @param[in] cmdLength The byte length of the \c pCmdData buffer.  Note that this parameter may be zero for commands
//!                      that have no data.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMbLtm4680PmbusWrite(_In_ UINT chip, _In_ INT page, _In_ BYTE command, _In_reads_bytes_opt_(cmdLength) const void* pCmdData, _In_ DWORD cmdLength);

//! @brief Reads the specified voltage monitor channel on an HBI mainboard.
//!
//! This function reads the specified voltage monitor channel on an HBI mainboard.
//!
//! @note Correctly programmed board BLT is a prerequisite for using this function as it relies on #BLT.PartNumberCurrent
//! to differentiate between fab revisions.
//!
//! @param[in] channel Selects the monitoring channel. Valid values are 0 to 62.  The channels
//!                    and corresponding voltages monitored are:
//!                    | Channel |  Pin      |    Reference - Fab B       |    Reference - Fab C       |    Reference - Fab D       |
//!                    | :-----: | :-------: | :------------------------: | :------------------------: | :------------------------: |
//!                    |     0   | U45 Vdd   | +5P0V_STBY_1A              | +5P0V_STBY_1A              | +5P0V_STBY_1A              |
//!                    |     1   | U45 AIN1  | AD7411_GND_FAULT_DETECT_P  | AD7411_GND_FAULT_DETECT_P  | AD7411_GND_FAULT_DETECT_P  |
//!                    |     2   | U45 AIN2  | AD7411_GND_FAULT_DETECT_N  | AD7411_GND_FAULT_DETECT_N  | AD7411_GND_FAULT_DETECT_N  |
//!                    |     3   | U45 AIN3  | ADT7411_H2O_LEAK_DETECT_0  | ADT7411_H2O_LEAK_DETECT_0  | ADT7411_H2O_LEAK_DETECT_0  |
//!                    |     4   | U45 AIN4  | ADT7411_H2O_LEAK_DETECT_1  | ADT7411_H2O_LEAK_DETECT_1  | ADT7411_H2O_LEAK_DETECT_1  |
//!                    |     5   | U45 AIN5  | ADT7411_H2O_LEAK_DETECT_2  | not connected              | not connected              |
//!                    |     6   | U45 AIN6  | ADT7411_H2O_LEAK_DETECT_3  | +3P3V_VCC_MAX10_2A         | +3P3V_VCC_MAX10_2A         |
//!                    |     7   | U45 AIN7  | not connected              | +3P3V_STBY_3A              | +3P3V_STBY_3A              |
//!                    |     8   | U45 AIN8  | not connected              | +3P3V_SC_2A                | +3P3V_SC_2A                |
//!                    |     9   | U19 Vdd   | +5P0V_16A                  | +3P3V_16A                  | +3P3V_16A                  |
//!                    |    10   | U19 AIN1  | +3P3V_VCC_MAX10_2A         | +1P8V_VBAT_PG_1A           | +1P8V_VBAT_PG_1A           |
//!                    |    11   | U19 AIN2  | +3P3V_STBY_3A              | not connected              | not connected              |
//!                    |    12   | U19 AIN3  | +3P3V_SC_2A                | not connected              | +2P5V_VPP_DDR4_RCTC_1A     |
//!                    |    13   | U19 AIN4  | +1P8V_RC_8A                | +1P8V_RCTC_8A              | +1P8V_RCTC_8A              |
//!                    |    14   | U19 AIN5  | +1P2V_VDD_DDR4_RC_5A       | +1P2V_VDD_DDR4_RCTC_5A     | +1P2V_VDD_DDR4_RCTC_5A     |
//!                    |    15   | U19 AIN6  | +1P03V_VCC_GXB_RC_4A       | +1P03V_VCC_GXB_RCTC_4A     | +1P03V_VCC_GXB_RCTC_4A     |
//!                    |    16   | U19 AIN7  | +1P8V_PEX_1A               | +1P8V_PEX_1A               | +1P8V_PEX_1A               |
//!                    |    17   | U19 AIN8  | +0P9V_PEX_16A              | +0P9V_PEX_16A              | +0P9V_PEX_16A              |
//!                    |    18   | U80 Vdd   | +5P0V_16A                  | +3P3V_16A                  | +3P3V_16A                  |
//!                    |    19   | U80 AIN1  | +1P8V_16A                  | +1P8V_16A                  | +1P8V_16A                  |
//!                    |    20   | U80 AIN2  | +3P3V_BLT_500MA            | +3P3V_BLT_500MA            | +3P3V_BLT_500MA            |
//!                    |    21   | U80 AIN3  | +0P9V_VCCERAM_RC_4A        | +0P9V_VCCERAM_RCTC_4A      | +0P9V_VCCERAM_RCTC_4A      |
//!                    |    22   | U80 AIN4  | +2P5V_SPD_4A               | +2P5V_SPD_4A               | +2P5V_SPD_4A               |
//!                    |    23   | U80 AIN5  | +0P9V_VCCERAM_RM_5A        | +0P9V_VCCERAM_RM_5A        | +0P9V_VCCERAM_RM_5A        |
//!                    |    24   | U80 AIN6  | +1P8V_PG_30A               | +1P8V_PG_30A               | +1P8V_PG_30A               |
//!                    |    25   | U80 AIN7  | +1P12V_VCCR_GXB_PG_12A     | +1P12V_VCCR_GXB_PG_12A     | +1P12V_VCCR_GXB_PG_12A     |
//!                    |    26   | U80 AIN8  | +1P12V_VCCT_GXB_PG_4A      | +1P12V_VCCT_GXB_PG_4A      | +1P12V_VCCT_GXB_PG_4A      |
//!                    |    27   | U81 Vdd   | +5P0V_16A                  | +3P3V_16A                  | +3P3V_16A                  |
//!                    |    28   | U81 AIN1  | +3P3V_16A                  | +3P3V_16A                  | +3P3V_16A                  |
//!                    |    29   | U81 AIN2  | +1P2_VCC_THERM_5A          | +1P2_VCC_THERM_5A          | +1P2_VCC_THERM_5A          |
//!                    |    30   | U81 AIN3  | +1P2V_VCCIO_DDR4_RM_5A     | +1P2V_VCCIO_DDR4_RM_5A     | +1P2V_VCCIO_DDR4_RM_5A     |
//!                    |    31   | U81 AIN4  | +1P12V_VCCT_GXB_RM_4A      | +1P12V_VCCT_GXB_RM_4A      | +1P12V_VCCT_GXB_RM_4A      |
//!                    |    32   | U81 AIN5  | +1P8V_VCCPT_PGEN_12A       | +1P8V_VCCPT_PG_12A         | +1P8V_VCCPT_PG_12A         |
//!                    |    33   | U81 AIN6  | +1P12V_VCCR_GXB_RM_12A     | +1P12V_VCCR_GXB_RM_12A     | +1P12V_VCCR_GXB_RM_12A     |
//!                    |    34   | U81 AIN7  | +1P8V_VCCPT_RM_16A         | +1P8V_VCCPT_RM_16A         | +1P8V_VCCPT_RM_16A         |
//!                    |    35   | U81 AIN8  | +0P9V_VCCERAM_PG_5A        | +0P9V_VCCERAM_PG_5A        | +0P9V_VCCERAM_PG_5A        |
//!                    |    36   | U160 Vdd  | +5P0V_16A                  | +3P3V_16A                  | +3P3V_16A                  |
//!                    |    37   | U160 AIN1 | +5P0V_ISO_350MA            | not connected              | +0P6V_VTT_DDR4_HPS_1A      |
//!                    |    38   | U160 AIN2 | SO_GND                     | not connected              | +0P6V_VREF_DDR4_HPS_1A     |
//!                    |    39   | U160 AIN3 | not connected              | SPARE1_TO_BE_USED          | SPARE1_TO_BE_USED          |
//!                    |    40   | U160 AIN4 | not connected              | +0P6V_VREF_DDR4_RCTC_1A    | +0P6V_VREF_DDR4_RCTC_1A    |
//!                    |    41   | U160 AIN5 | +0P6V_VTT_DDR4_CH0_1A      | +0P6V_VTT_DDR4_CH0_1A      | +0P6V_VTT_DDR4_CH0_1A      |
//!                    |    42   | U160 AIN6 | +0P6V_VTT_DDR4_CH3_1A      | +0P6V_VTT_DDR4_CH3_1A      | +0P6V_VTT_DDR4_CH3_1A      |
//!                    |    43   | U160 AIN7 | +0P6V_VTT_DDR4_CH4_1A      | +0P6V_VTT_DDR4_CH4_1A      | +0P6V_VTT_DDR4_CH4_1A      |
//!                    |    44   | U160 AIN8 | not connected              | +0P6V_VREF_DDR4_RM_1A      | not connected              |
//!                    |    45   | U161 Vdd  | +5P0V_16A                  | +3P3V_16A                  | +3P3V_16A                  |
//!                    |    46   | U161 AIN1 | not connected              | +3P3V_VCCO_LMK_3A          | +3P3V_VCCO_LMK_3A          |
//!                    |    47   | U161 AIN2 | not connected              | +3P3V_VDD_LMK_3A           | +3P3V_VDD_LMK_3A           |
//!                    |    48   | U161 AIN3 | not connected              | +3P6V_LDO_5A               | +3P6V_LDO_5A               |
//!                    |    49   | U161 AIN4 | not connected              | +5P0V_STBY_1A              | +5P0V_STBY_1A              |
//!                    |    50   | U161 AIN5 | +0P6V_VTT_DDR4_CH1_1A      | +0P6V_VTT_DDR4_CH1_1A      | +0P6V_VTT_DDR4_CH1_1A      |
//!                    |    51   | U161 AIN6 | +0P6V_VTT_DDR4_CH2_1A      | +0P6V_VTT_DDR4_CH2_1A      | +0P6V_VTT_DDR4_CH2_1A      |
//!                    |    52   | U161 AIN7 | +2P4V_VCCFUSE_SDM_PG_1A    | +2P4V_VCCFUSE_SDM_PG_1A    | +2P4V_VCCFUSE_SDM_PG_1A    |
//!                    |    53   | U161 AIN8 | +2P4V_VCCFUSE_RC_1A        | +2P4V_VCCFUSE_RCTC_1A      | +2P4V_VCCFUSE_RCTC_1A      |
//!                    |    54   | U162 Vdd  | +5P0V_16A                  | +3P3V_16A                  | +3P3V_16A                  |
//!                    |    55   | U162 AIN1 | not connected              | +2P5V_VPP_DDR4_RM_1A       | +2P5V_VPP_DDR4_RM_1A       |
//!                    |    56   | U162 AIN2 | not connected              | +2P5V_VPP_DDR4_RCTC_1A     | +0P6V_VREF_DDR4_RM_1A      |
//!                    |    57   | U162 AIN3 | not connected              | +1P8V_VDD_CLK_GEN_1A       | +1P8V_VDD_CLK_GEN_1A       |
//!                    |    58   | U162 AIN4 | not connected              | +3P3V_VDDA_CLK_GEN_200MA   | +3P3V_VDDA_CLK_GEN_200MA   |
//!                    |    59   | U162 AIN5 | not connected              | +1P8V_VDDO_CLK_GEN_1A      | +5P0V_16A                  |
//!                    |    60   | U162 AIN6 | not connected              | +1P8V_VDDS_CLK_GEN_200MA   | +1P8V_VDDS_CLK_GEN_200MA   |
//!                    |    61   | U162 AIN7 | not connected              | SPARE_TO_BE _USED          | +1P8V_VDDO_CLK_GEN_200MA   |
//!                    |    62   | U162 AIN8 | +2P4V_VCCFUSE_SDM_RM_1A    | +2P4V_VCCFUSE_SDM_RM_1A    | +2P4V_VCCFUSE_SDM_RM_1A    |
//! @param[out] pVoltage The voltage in volts.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMbVmonRead(_In_ UINT channel, _Out_ double* pVoltage);

//! @brief Reads all nine voltage channels of an ADT7411 on an HBI mainboard.
//!
//! This function reads all nine voltage channels of an ADT7411 on an HBI mainboard.  The values are scaled by the factors implemented by
//! the voltage dividers feeding the AIN1-AIN8 inputs and represent the voltage at the signals listed in the \c chip table below, not the
//! voltage at the ADT7411 input.
//!
//! @note Correctly programmed board BLT is a prerequisite for using this function as it relies on #BLT.PartNumberCurrent
//! to differentiate between fab revisions.
//!
//! @param[in] chip Selects the ADT7411 to read. Valid values are 0 to 6. The voltages monitored for each chip are (per HBI mainboard schematics):
//!                 |  Channel |  Chip  | Voltages     |    Reference - Fab B       |    Reference - Fab C       |    Reference - Fab D       |
//!                 | :------: | :----: | :----------: | :------------------------: | :------------------------: | :------------------------: |
//!                 |     0    | (U45)  | pVoltages[0] | +5P0V_STBY_1A              | +5P0V_STBY_1A              | +5P0V_STBY_1A              |
//!                 |     0    | (U45)  | pVoltages[1] | AD7411_GND_FAULT_DETECT_P  | AD7411_GND_FAULT_DETECT_P  | AD7411_GND_FAULT_DETECT_P  |
//!                 |     0    | (U45)  | pVoltages[2] | AD7411_GND_FAULT_DETECT_N  | AD7411_GND_FAULT_DETECT_N  | AD7411_GND_FAULT_DETECT_N  |
//!                 |     0    | (U45)  | pVoltages[3] | ADT7411_H2O_LEAK_DETECT_0  | ADT7411_H2O_LEAK_DETECT_0  | ADT7411_H2O_LEAK_DETECT_0  |
//!                 |     0    | (U45)  | pVoltages[4] | ADT7411_H2O_LEAK_DETECT_1  | ADT7411_H2O_LEAK_DETECT_1  | ADT7411_H2O_LEAK_DETECT_1  |
//!                 |     0    | (U45)  | pVoltages[5] | ADT7411_H2O_LEAK_DETECT_2  | not connected              | not connected              |
//!                 |     0    | (U45)  | pVoltages[6] | ADT7411_H2O_LEAK_DETECT_3  | +3P3V_VCC_MAX10_2A         | +3P3V_VCC_MAX10_2A         |
//!                 |     0    | (U45)  | pVoltages[7] | not connected              | +3P3V_STBY_3A              | +3P3V_STBY_3A              |
//!                 |     0    | (U45)  | pVoltages[8] | not connected              | +3P3V_SC_2A                | +3P3V_SC_2A                |
//!                 |     1    | (U19)  | pVoltages[0] | +5P0V_16A                  | +3P3V_16A                  | +3P3V_16A                  |
//!                 |     1    | (U19)  | pVoltages[1] | +3P3V_VCC_MAX10_2A         | +1P8V_VBAT_PG_1A           | +1P8V_VBAT_PG_1A           |
//!                 |     1    | (U19)  | pVoltages[2] | +3P3V_STBY_3A              | not connected              | not connected              |
//!                 |     1    | (U19)  | pVoltages[3] | +3P3V_SC_2A                | not connected              | +2P5V_VPP_DDR4_RCTC_1A     |
//!                 |     1    | (U19)  | pVoltages[4] | +1P8V_RC_8A                | +1P8V_RCTC_8A              | +1P8V_RCTC_8A              |
//!                 |     1    | (U19)  | pVoltages[5] | +1P2V_VDD_DDR4_RC_5A       | +1P2V_VDD_DDR4_RCTC_5A     | +1P2V_VDD_DDR4_RCTC_5A     |
//!                 |     1    | (U19)  | pVoltages[6] | +1P03V_VCC_GXB_RC_4A       | +1P03V_VCC_GXB_RCTC_4A     | +1P03V_VCC_GXB_RCTC_4A     |
//!                 |     1    | (U19)  | pVoltages[7] | +1P8V_PEX_1A               | +1P8V_PEX_1A               | +1P8V_PEX_1A               |
//!                 |     1    | (U19)  | pVoltages[8] | +0P9V_PEX_16A              | +0P9V_PEX_16A              | +0P9V_PEX_16A              |
//!                 |     2    | (U80)  | pVoltages[0] | +5P0V_16A                  | +3P3V_16A                  | +3P3V_16A                  |
//!                 |     2    | (U80)  | pVoltages[1] | +1P8V_16A                  | +1P8V_16A                  | +1P8V_16A                  |
//!                 |     2    | (U80)  | pVoltages[2] | +3P3V_BLT_500MA            | +3P3V_BLT_500MA            | +3P3V_BLT_500MA            |
//!                 |     2    | (U80)  | pVoltages[3] | +0P9V_VCCERAM_RC_4A        | +0P9V_VCCERAM_RCTC_4A      | +0P9V_VCCERAM_RCTC_4A      |
//!                 |     2    | (U80)  | pVoltages[4] | +2P5V_SPD_4A               | +2P5V_SPD_4A               | +2P5V_SPD_4A               |
//!                 |     2    | (U80)  | pVoltages[5] | +0P9V_VCCERAM_RM_5A        | +0P9V_VCCERAM_RM_5A        | +0P9V_VCCERAM_RM_5A        |
//!                 |     2    | (U80)  | pVoltages[6] | +1P8V_PG_30A               | +1P8V_PG_30A               | +1P8V_PG_30A               |
//!                 |     2    | (U80)  | pVoltages[7] | +1P12V_VCCR_GXB_PG_12A     | +1P12V_VCCR_GXB_PG_12A     | +1P12V_VCCR_GXB_PG_12A     |
//!                 |     2    | (U80)  | pVoltages[8] | +1P12V_VCCT_GXB_PG_4A      | +1P12V_VCCT_GXB_PG_4A      | +1P12V_VCCT_GXB_PG_4A      |
//!                 |     3    | (U81)  | pVoltages[0] | +5P0V_16A                  | +3P3V_16A                  | +3P3V_16A                  |
//!                 |     3    | (U81)  | pVoltages[1] | +3P3V_16A                  | +3P3V_16A                  | +3P3V_16A                  |
//!                 |     3    | (U81)  | pVoltages[2] | +1P2_VCC_THERM_5A          | +1P2_VCC_THERM_5A          | +1P2_VCC_THERM_5A          |
//!                 |     3    | (U81)  | pVoltages[3] | +1P2V_VCCIO_DDR4_RM_5A     | +1P2V_VCCIO_DDR4_RM_5A     | +1P2V_VCCIO_DDR4_RM_5A     |
//!                 |     3    | (U81)  | pVoltages[4] | +1P12V_VCCT_GXB_RM_4A      | +1P12V_VCCT_GXB_RM_4A      | +1P12V_VCCT_GXB_RM_4A      |
//!                 |     3    | (U81)  | pVoltages[5] | +1P8V_VCCPT_PGEN_12A       | +1P8V_VCCPT_PG_12A         | +1P8V_VCCPT_PG_12A         |
//!                 |     3    | (U81)  | pVoltages[6] | +1P12V_VCCR_GXB_RM_12A     | +1P12V_VCCR_GXB_RM_12A     | +1P12V_VCCR_GXB_RM_12A     |
//!                 |     3    | (U81)  | pVoltages[7] | +1P8V_VCCPT_RM_16A         | +1P8V_VCCPT_RM_16A         | +1P8V_VCCPT_RM_16A         |
//!                 |     3    | (U81)  | pVoltages[8] | +0P9V_VCCERAM_PG_5A        | +0P9V_VCCERAM_PG_5A        | +0P9V_VCCERAM_PG_5A        |
//!                 |     4    | (U160) | pVoltages[0] | +5P0V_16A                  | +3P3V_16A                  | +3P3V_16A                  |
//!                 |     4    | (U160) | pVoltages[1] | +5P0V_ISO_350MA            | not connected              | +0P6V_VTT_DDR4_HPS_1A      |
//!                 |     4    | (U160) | pVoltages[2] | SO_GND                     | not connected              | +0P6V_VREF_DDR4_HPS_1A     |
//!                 |     4    | (U160) | pVoltages[3] | not connected              | SPARE1_TO_BE_USED          | SPARE1_TO_BE_USED          |
//!                 |     4    | (U160) | pVoltages[4] | not connected              | +0P6V_VREF_DDR4_RCTC_1A    | +0P6V_VREF_DDR4_RCTC_1A    |
//!                 |     4    | (U160) | pVoltages[5] | +0P6V_VTT_DDR4_CH0_1A      | +0P6V_VTT_DDR4_CH0_1A      | +0P6V_VTT_DDR4_CH0_1A      |
//!                 |     4    | (U160) | pVoltages[6] | +0P6V_VTT_DDR4_CH3_1A      | +0P6V_VTT_DDR4_CH3_1A      | +0P6V_VTT_DDR4_CH3_1A      |
//!                 |     4    | (U160) | pVoltages[7] | +0P6V_VTT_DDR4_CH4_1A      | +0P6V_VTT_DDR4_CH4_1A      | +0P6V_VTT_DDR4_CH4_1A      |
//!                 |     4    | (U160) | pVoltages[8] | not connected              | +0P6V_VREF_DDR4_RM_1A      | not connected              |
//!                 |     5    | (U161) | pVoltages[0] | +5P0V_16A                  | +3P3V_16A                  | +3P3V_16A                  |
//!                 |     5    | (U161) | pVoltages[1] | not connected              | +3P3V_VCCO_LMK_3A          | +3P3V_VCCO_LMK_3A          |
//!                 |     5    | (U161) | pVoltages[2] | not connected              | +3P3V_VDD_LMK_3A           | +3P3V_VDD_LMK_3A           |
//!                 |     5    | (U161) | pVoltages[3] | not connected              | +3P6V_LDO_5A               | +3P6V_LDO_5A               |
//!                 |     5    | (U161) | pVoltages[4] | not connected              | +5P0V_STBY_1A              | +5P0V_STBY_1A              |
//!                 |     5    | (U161) | pVoltages[5] | +0P6V_VTT_DDR4_CH1_1A      | +0P6V_VTT_DDR4_CH1_1A      | +0P6V_VTT_DDR4_CH1_1A      |
//!                 |     5    | (U161) | pVoltages[6] | +0P6V_VTT_DDR4_CH2_1A      | +0P6V_VTT_DDR4_CH2_1A      | +0P6V_VTT_DDR4_CH2_1A      |
//!                 |     5    | (U161) | pVoltages[7] | +2P4V_VCCFUSE_SDM_PG_1A    | +2P4V_VCCFUSE_SDM_PG_1A    | +2P4V_VCCFUSE_SDM_PG_1A    |
//!                 |     5    | (U161) | pVoltages[8] | +2P4V_VCCFUSE_RC_1A        | +2P4V_VCCFUSE_RCTC_1A      | +2P4V_VCCFUSE_RCTC_1A      |
//!                 |     6    | (U162) | pVoltages[0] | +5P0V_16A                  | +3P3V_16A                  | +3P3V_16A                  |
//!                 |     6    | (U162) | pVoltages[1] | not connected              | +2P5V_VPP_DDR4_RM_1A       | +2P5V_VPP_DDR4_RM_1A       |
//!                 |     6    | (U162) | pVoltages[2] | not connected              | +2P5V_VPP_DDR4_RCTC_1A     | +0P6V_VREF_DDR4_RM_1A      |
//!                 |     6    | (U162) | pVoltages[3] | not connected              | +1P8V_VDD_CLK_GEN_1A       | +1P8V_VDD_CLK_GEN_1A       |
//!                 |     6    | (U162) | pVoltages[4] | not connected              | +3P3V_VDDA_CLK_GEN_200MA   | +3P3V_VDDA_CLK_GEN_200MA   |
//!                 |     6    | (U162) | pVoltages[5] | not connected              | +1P8V_VDDO_CLK_GEN_1A      | +5P0V_16A                  |
//!                 |     6    | (U162) | pVoltages[6] | not connected              | +1P8V_VDDS_CLK_GEN_200MA   | +1P8V_VDDS_CLK_GEN_200MA   |
//!                 |     6    | (U162) | pVoltages[7] | not connected              | SPARE_TO_BE _USED          | +1P8V_VDDO_CLK_GEN_200MA   |
//!                 |     6    | (U162) | pVoltages[8] | +2P4V_VCCFUSE_SDM_RM_1A    | +2P4V_VCCFUSE_SDM_RM_1A    | +2P4V_VCCFUSE_SDM_RM_1A    |
//! @param[out] pVoltages   An array of 9 readings in volts.
//! @param[in]  numVoltages The size of the \c pVoltages array.  Currently required to be 9.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMbVmonReadAll(_In_ UINT chip, _Out_writes_all_(numVoltages) double* pVoltages, _In_ SIZE_T numVoltages);

//! @brief Reads the specified temperature channel on an HBI mainboard.
//!
//! This function reads the specified temperature channel on an HBI mainboard.
//!
//! @note This function returns #HS_UNSUPPORTED for channel 8 on HBI mainboard Fab C as it excludes SHT21 device.
//!
//! @note Correctly programmed board BLT is a prerequisite for using this function as it relies on #BLT.PartNumberCurrent
//! to differentiate between fab revisions.
//!
//! @param[in] channel Selects a monitoring channel. Consult the following table:
//!                    | Channel | Measured Temperature      | Reference designator  |
//!                    | :-----: | :-----------------------: | :-------------------: |
//!                    |    0    | ADT7411                   |  U45                  |
//!                    |    1    | ADT7411                   |  U19                  |
//!                    |    2    | ADT7411                   |  U80                  |
//!                    |    3    | ADT7411                   |  U81                  |
//!                    |    4    | ADT7411                   |  U160                 |
//!                    |    5    | ADT7411                   |  U161                 |
//!                    |    6    | ADT7411                   |  U162                 |
//!                    |    7    | SHTC3 (air)               |  EU25                 |
//!                    |    8    | SHT21 (air)               |  U145 (Fab A, B only) |
//!                    |    9    | MAX31855 internal         |  U140                 |
//!                    |   10    | MAX31855 external (water) |  U140                 |
//!                    |   11    | Resource/Thermal FPGA     |  U94                  |
//!                    |   12    | Pattern Generator FPGA    |  U10                  |
//!                    |   13    | Ring Multiplier FPGA      |  U63                  |
//! @param[out] pTemp The temperature in degrees Celsius.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMbTmonRead(_In_ UINT channel, _Out_ double* pTemp);

//! @brief Reads the specified fan tachometer channels on an HBI mainboard.
//!
//! This function reads the specified fan tachometer channels on an HBI mainboard.
//!
//! @warning To use this function on an HBI mainboard Fab C, desired modes must be set for respective MAX6650 devices, once-only, using hbiMbFanModeSet().
//! Typically, Standalone Safety System (SASS), is expected to to do this for all fan controller chips.
//! Do not set modes if already set by another process (like SASS). However, if a process which had initialized modes is killed,
//! they must be set again, once, before using this function.
//!
//! @note Correctly programmed board BLT is a prerequisite for using this function as it relies on #BLT.PartNumberCurrent
//! to differentiate between fab revisions.
//!
//! @param[in] channel Selects the tachometer channel. Consult the following table.
//!                    Note that reference designators may change in new fabs.
//!                    |  fan  | Tachometer (Fab A, B) | Tachometer (Fab C)  | Connector |
//!                    | :---: | :-------------------: | :-----------------: | :-------: |
//!                    |   0   |   U62 FAN_00_TACH     |   U92 FAN_00_TACH   |    J37    |
//!                    |   1   |   U62 FAN_01_TACH     |   U102 FAN_01_TACH  |    J37    |
//!                    |   2   |   U62 FAN_02_TACH     |   U104 FAN_02_TACH  |    J44    |
//!                    |   3   |   U62 FAN_03_TACH     |   U105 FAN_03_TACH  |    J44    |
//!                    |   4   |   U68 FAN_10_TACH     |   U107 FAN_04_TACH  |    J43    |
//!                    |   5   |   U68 FAN_11_TACH     |   U108 FAN_05_TACH  |    J43    |
//!                    |   6   |   U68 FAN_12_TACH     |   U159 FAN_06_TACH  |    J52    |
//!                    |   7   |   U68 FAN_13_TACH     |   U164 FAN_07_TACH  |    J52    |
//!                    |   8   |   U69 FAN_20_TACH     |   U165 FAN_08_TACH  |    J53    |
//!                    |   9   |   U69 FAN_21_TACH     |   U166 FAN_09_TACH  |    J53    |
//!                    |   10  |   U69 FAN_22_TACH     |   U167 FAN_10_TACH  |    J58    |
//!                    |   11  |   U69 FAN_23_TACH     |   U168 FAN_11_TACH  |    J58    |
//! @param[out] pTach The fan speed in revolutions per second (RPS).
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMbFanRead(_In_ UINT channel, _Out_ double* pTach);

//! @brief Sets operating mode of a fan controller on an HBI mainboard Fab C or later.
//!
//! This function sets operating mode of a fan controller on an HBI mainboard Fab C or later.
//!
//! @note This function returns #HS_UNSUPPORTED on HBI mainboard Fab A, B.
//!
//! @note Correctly programmed board BLT is a prerequisite for using this function as it relies on #BLT.PartNumberCurrent
//! to differentiate between fab revisions.
//!
//! @param[in] channel Selects the fan controller chip. Consult the following table.
//!                    | channel | Fan selected (Fab C)| Connector |
//!                    | :-----: | :-----------------: | :-------: |
//!                    |    0    |   U92 FAN_00_TACH   |    J37    |
//!                    |    1    |   U102 FAN_01_TACH  |    J37    |
//!                    |    2    |   U104 FAN_02_TACH  |    J44    |
//!                    |    3    |   U105 FAN_03_TACH  |    J44    |
//!                    |    4    |   U107 FAN_04_TACH  |    J43    |
//!                    |    5    |   U108 FAN_05_TACH  |    J43    |
//!                    |    6    |   U159 FAN_06_TACH  |    J52    |
//!                    |    7    |   U164 FAN_07_TACH  |    J52    |
//!                    |    8    |   U165 FAN_08_TACH  |    J53    |
//!                    |    9    |   U166 FAN_09_TACH  |    J53    |
//!                    |    10   |   U167 FAN_10_TACH  |    J58    |
//!                    |    11   |   U168 FAN_11_TACH  |    J58    |
//! @param[in] mode One the three MAX6650 operating modes supported on an HBI mainboard Fab C or later. Consult the following table.
//!                    | mode | Operating Mode selected | Description
//!                    | :--: | :---------------------: | :---------------
//!                    |  0   | Software off            | All fans are turned off.  \c ratedSpeed parameter is ignored.
//!                    |  1   | Closed-loop             | Fans are automatically maintained at a requested speed.
//!                    |  2   | Software full-on        | Maximum voltage applied to fans.
//! @param[in] ratedFanSpeed The nominal fan speed in RPM for the connected fans for closed loop mode.
//!                          Valid values are 600-15000 for closed loop mode.  Use 0 for other modes.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMbFanModeSet(_In_ UINT channel, _In_ UINT mode, _In_ UINT ratedFanSpeed);

//! @brief Reads the rate of flow of pre-chilled water used in an HBI tester's cooling system.
//!
//! This function reads the rate of flow of pre-chilled water used in an HBI tester's cooling system.
//!
//! @note Correctly programmed board BLT is a prerequisite for using this function as it relies on #BLT.PartNumberCurrent
//! to differentiate between fab revisions.
//!
//! @param[out] pWaterFlow The waterflow rate in Gallons per Minute.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMbWaterflowMon(_Out_ double* pWaterFlow);

//! @brief Reads a relative humidity sensor on an HBI mainboard.
//!
//! This function reads a relative humidity sensor on an HBI mainboard.
//!
//! @note This function returns #HS_UNSUPPORTED for chip index 1 on HBI mainboard Fab C as it excludes SHT21 device.
//!
//! @note Correctly programmed board BLT is a prerequisite for using this function as it relies on #BLT.PartNumberCurrent
//! to differentiate between fab revisions.
//!
//! @param[in] index Selects one of two sensors. Consult the following table.
//!                  Note that reference designators may change in new fabs.
//!                  | index  | Reference (Fab A, B) | Reference (Fab C) |
//!                  |:-----: |:--------------------:|:-----------------:|
//!                  |   0    |    EU25  SHTC3       |    EU25  SHTC3    |
//!                  |   1    |    U145  SHT21       |         -         |
//! @param[out] pHumidity The current relative humidity as a percentage (0-100).
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMbHumidityRead(_In_ UINT index, _Out_ double* pHumidity);

//! @brief Computes the dew point based on current humidity and air temperature readings from a specified sensor.
//!
//! This function computes the dew point based on current humidity and air temperature readings,
//! as read by hbiMbHumidityRead() and hbiMbTmonRead() for a specified sensor.  If this value nears or exceeds an hbiMbTmonRead() of the
//! pre-chilled water (PCW) temperature, condensation in the tester may occur.
//!
//! @note This function returns #HS_UNSUPPORTED for chip index 1 on HBI mainboard Fab C as it excludes SHT21 device.
//!
//! @note Correctly programmed board BLT is a prerequisite for using this function as it relies on #BLT.PartNumberCurrent
//! to differentiate between fab revisions.
//!
//! @param[in] index Selects one of two sensors to use for the dew point computation.
//!                  Consult the following table.
//!                  Note that reference designators may change in new fabs.
//!                  | index  | Reference (Fab A, B) | Reference (Fab C) |
//!                  |:-----: |:--------------------:|:-----------------:|
//!                  |   0    |    EU25  SHTC3       |    EU25  SHTC3    |
//!                  |   1    |    U145  SHT21       |         -         |
//! @param[out] pDewpoint The calculated dew point of the tester in degrees Celsius.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMbDewpointRead(_In_ UINT index, _Out_ double* pDewpoint);

//! @brief Issues a write command to the AD5684 device on an HBI mainboard.
//!
//! This function issues a write command to the AD5684 device on an HBI mainboard.
//! @param[in] command See Table 8 in spec sheet for valid commands. Valid values are 0-8.
//! @param[in] address See Table 9 in spec sheet for valid addresses.  Valid values are 1, 2, 3, 4, 8, 15.
//! @param[in] data    12-bit data to write to the DAC.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMbAd5684Write(_In_ BYTE command, _In_ BYTE address, _In_ WORD data);

//! @brief Issues a read command to the AD5684 device on an HBI mainboard.
//!
//! This function issues a command to the AD5684 device on an HBI mainboard.
//! @param[in] address See Table 9 in spec sheet for valid addresses. Valid values are 1, 2, 3, 4, 8, 15.
//! @param[out] pData  12-bit data read from the DAC.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMbAd5684Read(_In_ BYTE address, _Out_ LPWORD pData);

//! @brief Directly writes an 8-bit register on a ADT7411 (8-channel ADC) device on an HBI mainboard.
//!
//! This function writes an 8-bit register on a ADT7411 (8-channel ADC) device on an HBI mainboard.
//!
//! @note Correctly programmed board BLT is a prerequisite for using this function as it relies on #BLT.PartNumberCurrent
//! to differentiate between fab revisions.
//!
//! @param[in] chip A zero-based chip index. Consult the following table.
//!                 Note that reference designators may change in new fabs.
//!                 |  Chip   | Reference designator |
//!                 | :-----: | :------------------: |
//!                 |    0    |         U45          |
//!                 |    1    |         U19          |
//!                 |    2    |         U80          |
//!                 |    3    |         U81          |
//!                 |    4    |         U160         |
//!                 |    5    |         U161         |
//!                 |    6    |         U162         |
//! @param[in] reg A valid register address for the Analog Devices ADT7411.
//! @param[in] data The data to write.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMbAdt7411Write(_In_ UINT chip, _In_ BYTE reg, _In_ BYTE data);

//! @brief Directly reads an 8-bit register on a ADT7411 (8-channel ADC) device on an HBI mainboard.
//!
//! This function reads an 8-bit register on a ADT7411 (8-channel ADC) device on an HBI mainboard.
//!
//! @note Correctly programmed board BLT is a prerequisite for using this function as it relies on #BLT.PartNumberCurrent
//! to differentiate between fab revisions.
//!
//! @param[in] chip A zero-based chip index.Consult the following table.
//!                 Note that reference designators may change in new fabs.
//!                 |  Chip   | Reference designator |
//!                 | :-----: | :------------------: |
//!                 |    0    |         U45          |
//!                 |    1    |         U19          |
//!                 |    2    |         U80          |
//!                 |    3    |         U81          |
//!                 |    4    |         U160         |
//!                 |    5    |         U161         |
//!                 |    6    |         U162         |
//! @param[in] reg  A valid register address for the Analog Devices ADT7411.
//! @param[out] pData The register contents.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMbAdt7411Read(_In_ UINT chip, _In_ BYTE reg, _Out_ PBYTE pData);

//! @brief Writes a register within the specified MAX6650/MAX6651 (fan controller) device on an HBI mainboard.
//!
//! This function writes a register within the specified MAX6650/MAX6651 (fan controller) device on an HBI mainboard.
//!
//! @note Correctly programmed board BLT is a prerequisite for using this function as it relies on #BLT.PartNumberCurrent
//! to differentiate between fab revisions.
//!
//! @param[in] chip A zero-based chip index.Consult the following table.
//!                 Note that HBI mainboard Fab A, B host only 4 MAX665x devices and will return #HS_UNSUPPORTED for chip 4-12.
//!                 |  Chip   |   Fab A,B     |     Fab C      |
//!                 | :-----: | :-----------: |:-------------: |
//!                 |    0    | U62 (MAX6651) | U92  (MAX6650) |
//!                 |    1    | U68 (MAX6651) | U102 (MAX6650) |
//!                 |    2    | U69 (MAX6651) | U104 (MAX6650) |
//!                 |    3    | U67 (MAX6650) | U105 (MAX6650) |
//!                 |    4    |      -        | U107 (MAX6650) |
//!                 |    5    |      -        | U108 (MAX6650) |
//!                 |    6    |      -        | U159 (MAX6650) |
//!                 |    7    |      -        | U164 (MAX6650) |
//!                 |    8    |      -        | U165 (MAX6650) |
//!                 |    9    |      -        | U166 (MAX6650) |
//!                 |    10   |      -        | U167 (MAX6650) |
//!                 |    11   |      -        | U168 (MAX6650) |
//!                 |    12   |      -        | U67  (MAX6650) |
//! @param[in] reg Writable MAX6650/MAX6651 register address.
//!                 | Register | Name         | Function              |
//!                 | :------: | :----------: | :-------------------- |
//!                 |   0x00   | SPEED        | Fan speed             |
//!                 |   0x02   | CONFIG       | Configuration         |
//!                 |   0x04   | GPIO DEF     | GPIO definition       |
//!                 |   0x06   | DAC          | DAC                   |
//!                 |   0x08   | ALARM ENABLE | Alarm Enable          |
//!                 |   0x16   | COUNT        | Tachometer count time |
//! @param[in] data The value to write.  Valid values are 0-255 (00h-FFh).
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMbMax665xWrite(_In_ UINT chip, _In_ BYTE reg, _In_ BYTE data);

//! @brief Reads a register within the specified MAX6650/MAX6651 (fan controller) device on an HBI mainboard.
//!
//! This function reads a register within the specified MAX6650/MAX6651 (fan controller) device on an HBI mainboard.
//!
//! @note Correctly programmed board BLT is a prerequisite for using this function as it relies on #BLT.PartNumberCurrent
//! to differentiate between fab revisions.
//!
//! @param[in] chip A zero-based chip index.Consult the following table.
//!                 Note that HBI mainboard Fab A, B host only 4 MAX665x devices and will return #HS_UNSUPPORTED for chip 4-12.
//!                 |  Chip   |   Fab A,B     |     Fab C      |
//!                 | :-----: | :-----------: | :------------: |
//!                 |    0    | U62 (MAX6651) | U92  (MAX6650) |
//!                 |    1    | U68 (MAX6651) | U102 (MAX6650) |
//!                 |    2    | U69 (MAX6651) | U104 (MAX6650) |
//!                 |    3    | U67 (MAX6650) | U105 (MAX6650) |
//!                 |    4    |      -        | U107 (MAX6650) |
//!                 |    5    |      -        | U108 (MAX6650) |
//!                 |    6    |      -        | U159 (MAX6650) |
//!                 |    7    |      -        | U164 (MAX6650) |
//!                 |    8    |      -        | U165 (MAX6650) |
//!                 |    9    |      -        | U166 (MAX6650) |
//!                 |    10   |      -        | U167 (MAX6650) |
//!                 |    11   |      -        | U168 (MAX6650) |
//!                 |    12   |      -        | U67  (MAX6650) |
//! @param[in] reg MAX6650/MAX6651 register address.
//!                 | Register | Name         | Function              |
//!                 | :------: | :----------: | :-------------------- |
//!                 |   0x00   | SPEED        | Fan speed             |
//!                 |   0x02   | CONFIG       | Configuration         |
//!                 |   0x04   | GPIO DEF     | GPIO definition       |
//!                 |   0x06   | DAC          | DAC                   |
//!                 |   0x08   | ALARM ENABLE | Alarm Enable          |
//!                 |   0x0A   | ALARM        | Alarm Status          |
//!                 |   0x0C   | TACH0        | Tachometer 0 count    |
//!                 |   0x0E   | TACH1        | Tachometer 1 count    |
//!                 |   0x10   | TACH2        | Tachometer 2 count    |
//!                 |   0x12   | TACH3        | Tachometer 3 count    |
//!                 |   0x14   | GPIO STAT    | GPIO Status           |
//!                 |   0x16   | COUNT        | Tachometer count time |
//! @param[out] pData The register contents.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMbMax665xRead(_In_ UINT chip, _In_ BYTE reg, _Out_ PBYTE pData);

//! @brief Executes a command within the SHTC3 (humidity and temperature sensor) device on an HBI mainboard.
//!
//! This function executes a command within the SHTC3 (humidity and temperature sensor) device on an HBI mainboard.
//! This function is for debugging only.  Prefer to use other specialized \c hbiMbXXXXX functions instead.
//!
//! @param[in] pCmdData   A data buffer of bytes to write to the SHTC3 device via the root I2C bus.  Note
//!                       that the bytes are sent to the device sequentially starting with the first
//!                       byte pointed to by \c pCmdData.  The first byte in this buffer must always
//!                       be a command byte (see the SHTC3 data sheet for details).
//! @param[in] cmdLength  The byte length of the \c pCmdData buffer.  Note that this parameter cannot be
//!                       zero since there must always be at least one byte (the command byte) in \c pCmdData.
//! @param[out] pReadData A data buffer of bytes to hold the data read from the SHTC3 device via the
//!                       root I2C bus.  Note that the bytes received from the device are located sequentially
//!                       in the buffer in the order they were received with the first byte being located
//!                       directly at \c pReadData.  This parameter can be NULL if the command being sent
//!                       has a \c readLength of zero.
//! @param[in] readLength The byte length of the \c pReadData buffer.  Note that this parameter can be zero if
//!                       the command being sent has no bytes of read back from the device.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMbShtc3Execute(_In_reads_bytes_(cmdLength) const void* pCmdData, _In_ DWORD cmdLength, _Out_writes_bytes_all_opt_(readLength) LPVOID pReadData, _In_ DWORD readLength);

//! @brief Executes a command within the SHT21 (humidity and temperature sensor) device on an HBI mainboard Fab A, B.
//!
//! This function executes a command within the SHT21 (humidity and temperature sensor) device on an HBI mainboard Fab A, B.
//! This function is for debugging only.  Prefer to use other specialized \c hbiMbXXXXX functions instead.
//!
//! @note This function returns #HS_UNSUPPORTED on HBI mainboard Fab C as it excludes SHT21 device.
//!
//! @note Correctly programmed board BLT is a prerequisite for using this function as it relies on #BLT.PartNumberCurrent
//! to differentiate between fab revisions.
//!
//! @param[in] pCmdData   A data buffer of bytes to write to the SHT21 device via the root I2C bus.  Note
//!                       that the bytes are sent to the device sequentially starting with the first
//!                       byte pointed to by \c pCmdData.  The first byte in this buffer must always
//!                       be a command byte (see the SHT21 data sheet for details).
//! @param[in] cmdLength  The byte length of the \c pCmdData buffer.  Note that this parameter cannot be
//!                       zero since there must always be at least one byte (the command byte) in \c pCmdData.
//! @param[out] pReadData A data buffer of bytes to hold the data read from the SHT21 device via the
//!                       root I2C bus.  Note that the bytes received from the device are located sequentially
//!                       in the buffer in the order they were received with the first byte being located
//!                       directly at \c pReadData.  This parameter can be NULL if the command being sent
//!                       has a \c readLength of zero.
//! @param[in] readLength The byte length of the \c pReadData buffer.  Note that this parameter can be zero if
//!                       the command being sent has no bytes of read back from the device.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMbSht21Execute(_In_reads_bytes_(cmdLength) const void* pCmdData, _In_ DWORD cmdLength, _Out_writes_bytes_all_opt_(readLength) LPVOID pReadData, _In_ DWORD readLength);

//! @brief Writes a 16-bit register on the SI5344 (programmable clock generator) device on an HBI mainboard.
//!
//! This function writes a 16-bit register on the SI5344 (programmable clock generator) device on an HBI mainboard.
//!
//! @note This function returns #HS_UNSUPPORTED on HBI mainboard Fab A, B.
//!
//! @note Correctly programmed board BLT is a prerequisite for using this function as it relies on #BLT.PartNumberCurrent
//! to differentiate between fab revisions.
//!
//! @param[in] reg A valid 16-bit register address where Upper 8-bits represent the page to be accessed and remaining bits denote the 8-bit address within that page.
//!                \n Example: reg 0x0A05 (N_PDNB- Powers down the N dividers) refers to the 8-bit address 0x05 on page A. See datasheet for specifics.
//! @param[in] data The 8-bit data to write.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMbSi5344Write(_In_ WORD reg, _In_ BYTE data);

//! @brief Reads a 16-bit register on the SI5344 (programmable clock generator) device on an HBI mainboard.
//!
//! This function Reads a 16-bit register on the SI5344 (programmable clock generator) device on an HBI mainboard.
//!
//! @note This function returns #HS_UNSUPPORTED on HBI mainboard Fab A, B.
//!
//! @note Correctly programmed board BLT is a prerequisite for using this function as it relies on #BLT.PartNumberCurrent
//! to differentiate between fab revisions.
//!
//! @param[in] reg A valid 16-bit register address where Upper 8-bits represent the page to be accessed and remaining bits denote the 8-bit address within that page.
//!                \n Example: reg 0x0A05 (N_PDNB- Powers down the N dividers) refers to the 8-bit address 0x05 on page A. See datasheet for specifics.
//! @param[out] pData The register contents.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMbSi5344Read(_In_ WORD reg, _Out_ PBYTE pData);

//! @brief Writes data to the user-programmable space of the DDR4 Serial Presence Detect (SPD) EEPROM device within the specified SODIMM socket on an HBI mainboard.
//!
//! This function writes data to the user-programmable space of the DDR4 Serial Presence Detect (SPD) EEPROM device within the specified SODIMM socket on an HBI mainboard.
//!
//! @note This function returns #HS_UNSUPPORTED on HBI mainboard Fab A, B and #HS_I2C_ADDR_NOACK if the specified DIMM socket is empty.
//!
//! @note Correctly programmed board BLT is a prerequisite for using this function as it relies on #BLT.PartNumberCurrent
//! to differentiate between fab revisions.
//!
//! @param[in] index A zero-based SODIMM socket index. Consult the following table.
//!                  |  index  |   SODIMM socket selected (Fab C)   |
//!                  | :-----: | :--------------------------------: |
//!                  |    0    |  J010 (DDR4 SODIMM - CH0 - R1:R0)  |
//!                  |    1    |  J032 (DDR4 SODIMM - CH0 - R3:R2)  |
//!                  |    2    |  J110 (DDR4 SODIMM - CH1 - R1:R0)  |
//!                  |    3    |  J132 (DDR4 SODIMM - CH1 - R3:R2)  |
//!                  |    4    |  J210 (DDR4 SODIMM - CH2 - R1:R0)  |
//!                  |    5    |  J232 (DDR4 SODIMM - CH2 - R3:R2)  |
//!                  |    6    |  J310 (DDR4 SODIMM - CH3 - R1:R0)  |
//!                  |    7    |  J332 (DDR4 SODIMM - CH3 - R3:R2)  |
//!                  |    8    |  J410 (DDR4 SODIMM - CH4 - R1:R0)  |
//!                  |    9    |  J432 (DDR4 SODIMM - CH4 - R3:R2)  |
//! @param[in] address The location of the first byte in the DDR4 SPD EEPROM's user-programmable space to which the data in the \c pData buffer will be written. Valid values are 384-511 (180h-1FFh).
//!                    Note that bytes 0-383 (000h-17Fh) are read-only and can not be accessed using this API. See device specific documentation for details.
//! @param[in] pData A data buffer containing data to write to the DDR4 SPD EEPROM device.
//! @param[in] length The byte length of the \c pData buffer.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMbDdr4SpdEepromWrite(_In_ UINT index, _In_ WORD address, _In_reads_bytes_(length) const void* pData, _In_ DWORD length);

//! @brief Reads data from the DDR4 Serial Presence Detect (SPD) EEPROM device within the specified SODIMM socket on an HBI mainboard.
//!
//! This function reads data from the DDR4 Serial Presence Detect (SPD) EEPROM device within the specified SODIMM socket on an HBI mainboard.
//!
//! @note This function returns #HS_UNSUPPORTED on HBI mainboard Fab A, B and #HS_I2C_ADDR_NOACK if the specified DIMM socket is empty.
//!
//! @note Correctly programmed board BLT is a prerequisite for using this function as it relies on #BLT.PartNumberCurrent
//! to differentiate between fab revisions.
//!
//! @param[in] index A zero-based SODIMM socket index. Consult the following table.
//!                  |  index  |   SODIMM socket selected (Fab C)   |
//!                  | :-----: | :--------------------------------: |
//!                  |    0    |  J010 (DDR4 SODIMM - CH0 - R1:R0)  |
//!                  |    1    |  J032 (DDR4 SODIMM - CH0 - R3:R2)  |
//!                  |    2    |  J110 (DDR4 SODIMM - CH1 - R1:R0)  |
//!                  |    3    |  J132 (DDR4 SODIMM - CH1 - R3:R2)  |
//!                  |    4    |  J210 (DDR4 SODIMM - CH2 - R1:R0)  |
//!                  |    5    |  J232 (DDR4 SODIMM - CH2 - R3:R2)  |
//!                  |    6    |  J310 (DDR4 SODIMM - CH3 - R1:R0)  |
//!                  |    7    |  J332 (DDR4 SODIMM - CH3 - R3:R2)  |
//!                  |    8    |  J410 (DDR4 SODIMM - CH4 - R1:R0)  |
//!                  |    9    |  J432 (DDR4 SODIMM - CH4 - R3:R2)  |
//! @param[in] address The location of the first byte in the DDR4 SPD EEPROM from which the data will be read. Valid values are 0-511 (000h-1FFh).
//! @param[out] pData  A pointer to a data buffer in which the read data will be stored. Note that this buffer must have at least \c length
//!                    bytes allocated in order to complete successfully.
//! @param[in] length  The number of bytes to read from the EEPROM.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMbDdr4SpdEepromRead(_In_ UINT index, _In_ WORD address, _Out_writes_bytes_all_(length) LPVOID pData, _In_ DWORD length);

//! @brief Reads temperature from the integrated DDR4 SPD EEPROM and Temperature Sensor (TSE2004av) module within the specified SODIMM socket on an HBI mainboard.
//!
//! This function reads temperature from the integrated DDR4 SPD EEPROM and Temperature Sensor (TSE2004av) module within the specified SODIMM socket on an HBI mainboard.
//!
//! @note This function returns #HS_UNSUPPORTED on HBI mainboard Fab A, B and #HS_I2C_ADDR_NOACK if the specified DIMM socket is empty.
//!
//! @note Correctly programmed board BLT is a prerequisite for using this function as it relies on #BLT.PartNumberCurrent
//! to differentiate between fab revisions.
//!
//! @param[in] index A zero-based SODIMM socket index. Consult the following table.
//!                  |  index  |   SODIMM socket selected (Fab C)   |
//!                  | :-----: | :--------------------------------: |
//!                  |    0    |  J010 (DDR4 SODIMM - CH0 - R1:R0)  |
//!                  |    1    |  J032 (DDR4 SODIMM - CH0 - R3:R2)  |
//!                  |    2    |  J110 (DDR4 SODIMM - CH1 - R1:R0)  |
//!                  |    3    |  J132 (DDR4 SODIMM - CH1 - R3:R2)  |
//!                  |    4    |  J210 (DDR4 SODIMM - CH2 - R1:R0)  |
//!                  |    5    |  J232 (DDR4 SODIMM - CH2 - R3:R2)  |
//!                  |    6    |  J310 (DDR4 SODIMM - CH3 - R1:R0)  |
//!                  |    7    |  J332 (DDR4 SODIMM - CH3 - R3:R2)  |
//!                  |    8    |  J410 (DDR4 SODIMM - CH4 - R1:R0)  |
//!                  |    9    |  J432 (DDR4 SODIMM - CH4 - R3:R2)  |
//! @param[out] pTemp The ambient temperature read by temperature sensor in degrees Celsius.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiMbDdr4TmonRead(_In_ UINT index, _Out_ double* pTemp);

//! @brief Writes a row on the HBI front panel e-paper display.
//!
//! This function writes a row on the HBI front panel e-paper display.
//!
//! @note This function returns #HS_UNSUPPORTED on HBI mainboard Fab A, B.
//!
//! @note Correctly programmed board BLT is a prerequisite for using this function as it relies on #BLT.PartNumberCurrent
//! to differentiate between fab revisions.
//!
//! @param[in] row The row to write. Valid values are 0-29. Note that large font sized text can not be written on row 29.
//! @param[in] column The column within the specified row to begin writing from. Valid values are 0-49 for small font sized text and 0-24 for large font sized text.
//! @param[in] textType Selects one of the three text options. Consult the following table.
//!                     | textType |    text option selected     |  rows occupied  |
//!                     | :------: | :-------------------------: | :-------------: |
//!                     |    0     |  Small font size            |       1         |
//!                     |    1     |  Large font size            |       2         |
//!                     |    2     |  Large font size (inverted) |       2         |
//! @param[in] text The text to write - begining from the specified row, column location.
//! @param[in] update \c TRUE forces a full-screen e-paper update with immediate effect. Usage of this option must be limited to reduce the e-paper write cycles. \n
//!                   \c FALSE defers the e-paper update until the next forced update command or failsafe, whichever occurs first.
//!                   This option can be used to queue up multiple e-paper update commands (including the full-screen clear command).
//! @returns \ref HIL_STATUS
//! @see hbiEPaperClear()
HIL_API HIL_STATUS hbiEPaperWrite(_In_ UINT row, _In_ UINT column, _In_ UINT textType, _In_z_ LPCSTR text, _In_ BOOL update);

//! @brief Clears the user-exposed region of the HBI front panel e-paper display.
//!
//! This function clears the user-exposed region (i.e. row 0-29) of the HBI front panel e-paper display.
//!
//! @note This function returns #HS_UNSUPPORTED on HBI mainboard Fab A, B.
//!
//! @note Correctly programmed board BLT is a prerequisite for using this function as it relies on #BLT.PartNumberCurrent
//! to differentiate between fab revisions.
//!
//! @param[in] update \c TRUE forces a full-screen e-paper clear with immediate effect. Usage of this option must be limited to reduce the e-paper write cycles. \n
//!                   \c FALSE defers the full-screen e-paper clear until the next forced update command or failsafe disable, whichever occurs first.
//!                   This option can be used to queue up multiple e-paper update commands (followed by the full-screen clear command).
//! @returns \ref HIL_STATUS
//! @see hbiEPaperWrite()
HIL_API HIL_STATUS hbiEPaperClear(_In_ BOOL update);

#ifdef __cplusplus
}
#endif
