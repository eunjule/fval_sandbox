// INTEL CONFIDENTIAL
// Copyright 2019 Intel Corporation All Rights Reserved.
//
// The source code contained or described herein and all documents related to
// the source code ("Material") are owned by Intel Corporation or its
// suppliers or licensors. Title to the Material remains with Intel
// Corporation or its suppliers and licensors. The Material contains trade
// secrets and proprietary and confidential information of Intel or its
// suppliers and licensors. The Material is protected by worldwide copyright
// and trade secret laws and treaty provisions. No part of the Material may be
// used, copied, reproduced, modified, published, uploaded, posted,
// transmitted, distributed, or disclosed in any way without Intel's prior
// express written permission.
//
// No license under any patent, copyright, trade secret or other intellectual
// property right is granted to or conferred upon you by disclosure or
// delivery of the Materials, either expressly, by implication, inducement,
// estoppel or otherwise. Any license under such intellectual property rights
// must be express and approved by Intel in writing.

//! @file
//! @brief APIs to control the HBI bulk power supplies (BPS).

#pragma once
#include "HilDefs.h"

#ifdef __cplusplus
extern "C" {
#endif

//! @brief Verifies an HBI bulk power supply is present.
//!
//! This function verifies an HBI bulk power supply (BPS) is present.  It connects to and caches driver resources for use
//! by other \c hbiPsXXXXX functions.  hbiPsDisconnect() is its complementary function and frees any cached resources.
//!
//! Neither hbiPsConnect() or hbiPsDisconnect() are required to be called to use the other \c hbiPsXXXXX functions.  All
//! \c hbiPsXXXXX functions will allocate resources if they are not already allocated and free them if required to
//! perform their function.
//! @note If System Power Monitoring (SMP) is available and enabled via the hbiPsPmbusPollingEnable() API, that function
//!       will detect and cache the power supply presence, and this function will return the cached status.  This is due
//!       to SPM polling making the I<sup>2</sup>C bus used to detect BPS presence unavailable.  Turn off polling for
//!       instantaneous presence detection.
//! @param[in] psSlot The index of an HBI bulk power supply according to the table below:
//! | psSlot | Comment                 |
//! | :----: | :-----------------------|
//! |   0    | HBI Bulk Power Supply 0 |
//! |   1    | HBI Bulk Power Supply 1 |
//! |   2    | HBI Bulk Power Supply 2 |
//!
//! @returns \ref HIL_STATUS
//! @see hbiPsPmbusPollingEnable()
HIL_API HIL_STATUS hbiPsConnect(_In_ UINT psSlot);

//! @brief Frees any resources cached from using the HBI bulk power supply functions.
//!
//! This function frees any resources associated with using the HBI bulk power supply functions. hbiPsConnect() is its
//! complementary function.
//!
//! Neither hbiPsConnect() or hbiPsDisconnect() are required to be called to use the other \c hbiPsXXXXX functions.  All
//! \c hbiPsXXXXX functions will allocate resources if they are not already allocated and free them if required to
//! perform their function.
//! @param[in] psSlot The index of an HBI bulk power supply according to the table below:
//! | psSlot | Comment                 |
//! | :----: | :---------------------- |
//! |   0    | HBI Bulk Power Supply 0 |
//! |   1    | HBI Bulk power Supply 1 |
//! |   2    | HBI Bulk power Supply 2 |
//!
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiPsDisconnect(_In_ UINT psSlot);

//! @brief Issues a write command to the PMBUS interface on an HBI bulk power supply.
//!
//! This function issues a write command to the PMBUS interface on an HBI bulk power supply.
//!
//! @param[in] psSlot The index of an HBI bulk power supply according to the table below:
//! | psSlot | Comment                 |
//! | :----: | :---------------------- |
//! |   0    | HBI Bulk Power Supply 0 |
//! |   1    | HBI Bulk power Supply 1 |
//! |   2    | HBI Bulk power Supply 2 |
//!
//! @param[in] command   PMBUS command to write.  See the PMBUS Specification Part II online for commands and additional data requirements.
//! @param[in] pCmdData  A data buffer of additional bytes to write for the associated \c command.
//!                      Note that the bytes are sent to the device sequentially starting with the first
//!                      byte pointed to by \c pCmdData.  Also note that the I<sup>2</sup>C address and the PEC (Packet
//!                      Error Checking) bytes should not be included in \c pCmdData because this is handled
//!                      automatically within the API.  Lastly, the command byte should not be included in this
//!                      buffer as it is already supplied in \c command.  Note that this parameter may be
//!                      NULL for commands that have no data associated with them.
//! @param[in] cmdLength The byte length of the \c pCmdData buffer.  Note that this parameter may be 0 for commands
//!                      that have no data.
//! @returns \ref HIL_STATUS
//! @retval #HS_SPM_POLLING_ENABLED This API is unavailable if SPM polling is enabled.  Turn it off by passing \c FALSE to the hbiPsPmbusPollingEnable() API.
//! @see hbiPsPmbusPollingEnable()
HIL_API HIL_STATUS hbiPsPmbusWrite(_In_ UINT psSlot, _In_ BYTE command, _In_reads_bytes_opt_(cmdLength) const void* pCmdData, _In_ DWORD cmdLength);

//! @brief Issues a read command to the PMBUS interface on an HBI bulk power supply.
//!
//! This function issues a read command to the PMBUS interface on an HBI bulk power supply.
//!
//! @param[in] psSlot The index of an HBI bulk power supply according to the table below:
//! | psSlot | Comment                 |
//! | :----: | :---------------------- |
//! |   0    | HBI Bulk Power Supply 0 |
//! |   1    | HBI Bulk power Supply 1 |
//! |   2    | HBI Bulk power Supply 2 |
//!
//! @param[in] command    PMBUS command to read.
//! @param[out] pReadData A data buffer of bytes to hold the data read from the associated \c command.
//!                       Note that the bytes received from the device are located sequentially
//!                       in the buffer in the order they were received with the first byte being located
//!                       directly at \c pReadData.  Also note that the PEC (Packet Error Checking) byte
//!                       is automatically checked within the API and is not included in \c pReadData.
//!                       Because of this, the PEC byte should not be included in the \c readLength value.
//!                       Lastly, this parameter cannot be NULL since all reads return at least one byte.
//! @param[in] readLength The byte length of the \c pReadData buffer.  Note that this parameter cannot be 0
//!                       since all reads return at least one byte.
//! @returns \ref HIL_STATUS
//! @retval #HS_SPM_POLLING_ENABLED This API is unavailable if SPM polling is enabled.  Turn it off by passing \c FALSE to the hbiPsPmbusPollingEnable() API.
//! @see hbiPsPmbusPollingEnable()
HIL_API HIL_STATUS hbiPsPmbusRead(_In_ UINT psSlot, _In_ BYTE command, _Out_writes_bytes_all_(readLength) LPVOID pReadData, _In_ DWORD readLength);

//! @brief Reads bytes from an HBI bulk power supply's AT24C02D EEPROM.
//!
//! This function reads bytes from an HBI bulk power supply's EEPROM.  It is a 8KB device containing the FRU (Field
//! Replaceable Unit) information of the supply such as the vendor name, part number and product code.
//!
//! @param[in] psSlot The index of an HBI bulk power supply according to the table below:
//! | psSlot | Comment                 |
//! | :----: | :---------------------- |
//! |   0    | HBI Bulk Power Supply 0 |
//! |   1    | HBI Bulk power Supply 1 |
//! |   2    | HBI Bulk power Supply 2 |
//!
//! @param[in]  address The address to begin reading from the FRU EEPROM.  Valid values are 0x00-0x1FFF.
//! @param[out] pData   The output buffer.  It cannot be NULL.
//! @param[in]  length  The amount of data to read.  \c address plus \c length cannot exceed 0x2000 (8192 bytes).
//! @returns \ref HIL_STATUS
//! @retval #HS_SPM_POLLING_ENABLED This API is unavailable if SPM polling is enabled.  Turn it off by passing \c FALSE to the hbiPsPmbusPollingEnable() API.
//! @see hbiPsPmbusPollingEnable()
HIL_API HIL_STATUS hbiPsFruEepromRead(_In_ UINT psSlot, _In_ UINT address, _Out_writes_bytes_all_(length) LPVOID pData, _In_ DWORD length);

//! @brief Reads the board-level traceability values from an HBI bulk power supply.
//!
//! This function reads the board-level traceability values from an HBI bulk power supply.  The values are defined in the #BLT structure.
//! Though the supplies do not store equivalent #BLT fields, a #BLT structure is returned populated with compatible fields from the
//! FRU (Field Replaceable Unit) information of the supplies.
//!
//! @param[in] psSlot The index of an HBI bulk power supply according to the table below:
//! | psSlot | Comment                 |
//! | :----: | :---------------------- |
//! |   0    | HBI Bulk Power Supply 0 |
//! |   1    | HBI Bulk power Supply 1 |
//! |   2    | HBI Bulk power Supply 2 |
//!
//! @param[out] pBlt   The address of a #BLT structure that will be populated with the data.
//! @returns \ref HIL_STATUS
//! @retval #HS_SPM_POLLING_ENABLED This API is unavailable if SPM polling is enabled.  Turn it off by passing \c FALSE to the hbiPsPmbusPollingEnable() API.
//! @see hbiPsPmbusPollingEnable()
HIL_API HIL_STATUS hbiPsBltBoardRead(_In_ UINT psSlot, _Out_ PBLT pBlt);

//! @brief Enables or disables System Power Monitoring (SPM) data streaming.
//!
//! This function enables or disables System Power Monitoring (SPM) data streaming.  When enabled, it detects which of the
//! three HBI power supplies are present and configures the SPM implementation to cache sensor readings of the supplies as
//! well as stream individual and cumulative power readings to an in-memory buffer.  The virtual address and size of this buffer
//! can be fetched from the hbiMbStreamingBufferEx() API.
//!
//! The following APIs cannot be used when polling is enabled due to the I<sup>2</sup>C bus used for communication is in constant use by the
//! streaming implementation and will return #HS_SPM_POLLING_ENABLED.  Disable polling to use these APIs:
//!
//! * hbiPsPmbusRead()
//! * hbiPsPmbusWrite()
//! * hbiPsFruEepromRead()
//! * hbiPsBltBoardRead()
//!
//! The following functions' operation is modified:
//! | API | Polling Enabled | Polling Disabled |
//! | :-: | :-------------- | :--------------- |
//! | hbiPsConnect() | Cached presence of supply as detected by hbiPsPmbusPollingEnable(). | ACK check of direct I<sup>2</sup>C PMBUS read. |
//! | hbiPsPmbusSensorRead() | Read SPM-cached sensor. | Direct I<sup>2</sup>C PMBUS read. |
//!
//! @param[in] enable \c TRUE enables SPM. \c FALSE  disables it.
//! @retval HS_SUCCESS     Polling was successfully enabled or disabled as requested.
//! @retval HS_UNSUPPORTED The HBI RCTC FPGA does not support SPM.
//! @see hbiPsPmbusRead()\n
//! @see hbiPsPmbusWrite()\n
//! @see hbiPsFruEepromRead()\n
//! @see hbiPsBltBoardRead()\n
//! @see hbiPsConnect()\n
//! @see hbiPsPmbusSensorRead()
HIL_API HIL_STATUS hbiPsPmbusPollingEnable(_In_ BOOL enable);

//! @brief Reads a specified HBI bulk power supply (BPS) PMBUS sensor.
//!
//! This function reads a specified HBI bulk power supply (BPS) PMBUS sensor.  If the Streaming Power Monitor (SPM) feature
//! is supported and enabled, this function will read sensor values cached from the SPM implementation; otherwise, it will
//! read the requested sensor directly from the supply via the appropriate PMBUS I<sup>2</sup>C command.
//!
//! @param[in] psSlot      The index of an HBI bulk power supply. Valid range 0-2 (unused/ignored for \c sensorIndex == 0).
//! @param[in] sensorIndex A zero-based sensor index. Consult the following table:
//!            | Index | Sensor                                      | PMBUS Command       | Unit   |
//!            | :---: | :-----------------------------------------: | :-----------------: | :----: |
//!            |   0   |   Cumulative power out (\c psSlot ignored)  | READ_POUT x ?       | W      |
//!            |   1   |   Power out                                 | READ_POUT           | W      |
//!            |   2   |   Voltage out                               | VOUT_MODE/READ_VOUT | V      |
//!            |   3   |   Current out                               | READ_IOUT           | A      |
//!            |   4   |   Read ambient temperature                  | READ_TEMPERATURE_1  | &deg;C |
//!            |   5   |   Read PFC/Boost heatsink temperature       | READ_TEMPERATURE_2  | &deg;C |
//!            |   6   |   Read SR board temperature                 | READ_TEMPERATURE_3  | &deg;C |
//!            |   7   |   Read fan speed                            | READ_FAN_SPEED_1    | RPM    |
//! @param[out] pOut The sensor value.
//! @returns \ref HIL_STATUS
//! @see hbiPsPmbusPollingEnable()
HIL_API HIL_STATUS hbiPsPmbusSensorRead(_In_ UINT psSlot, _In_ UINT sensorIndex, _Out_ double* pOut);

#ifdef __cplusplus
}
#endif
