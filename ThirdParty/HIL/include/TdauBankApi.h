// INTEL CONFIDENTIAL
// Copyright 2016-2018 Intel Corporation All Rights Reserved.
//
// The source code contained or described herein and all documents related to
// the source code ("Material") are owned by Intel Corporation or its
// suppliers or licensors. Title to the Material remains with Intel
// Corporation or its suppliers and licensors. The Material contains trade
// secrets and proprietary and confidential information of Intel or its
// suppliers and licensors. The Material is protected by worldwide copyright
// and trade secret laws and treaty provisions. No part of the Material may be
// used, copied, reproduced, modified, published, uploaded, posted,
// transmitted, distributed, or disclosed in any way without Intel's prior
// express written permission.
//
// No license under any patent, copyright, trade secret or other intellectual
// property right is granted to or conferred upon you by disclosure or
// delivery of the Materials, either expressly, by implication, inducement,
// estoppel or otherwise. Any license under such intellectual property rights
// must be express and approved by Intel in writing.

//! @file
//! @brief APIs to control an HDMT TDAU Bank card.
#pragma once
#include "HilDefs.h"

#ifdef __cplusplus
extern "C" {
#endif

//! @brief Initializes the components of the TDAU Bank card for operation.
//!
//! This function initializes the components of the TDAU Bank card for operation.  Calling this
//! function is required before using most other \c tdbXXXXXX() functions.
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS tdbInit(_In_ INT slot);

//! @brief Verifies an HDMT TDAU Bank card is present.
//!
//! This function verifies an HDMT TDAU Bank card is present.  It connects to and caches driver resources for use
//! by other \c tdbXXXXX functions.  tdbDisconnect() is its complementary function and frees any cached resources.
//!
//! Neither tdbConnect() or tdbDisconnect() are required to be called to use the other \c tdbXXXXX functions.  All
//! \c tdbXXXXX functions will allocate resources if they are not already allocated and free them if required to
//! perform their function.
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS tdbConnect(_In_ INT slot);

//! @brief Frees any resources cached from using the TDAU Bank card functions.
//!
//! This function frees any resources associated with using the TDAU Bank card HIL functions. tdbConnect() is its
//! complementary function.
//!
//! Neither tdbConnect() or tdbDisconnect() are required to be called to use the other \c tdbXXXXX functions.  All
//! \c tdbXXXXX functions will allocate resources if they are not already allocated and free them if required to
//! perform their function.
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS tdbDisconnect(_In_ INT slot);

//! @brief Programs the vendor and product IDs of all USB devices on an HDMT TDAU Bank card.
//!
//! This function programs the vendor and product IDs (VID/PIDs) of the FT240X and Cypress
//! USB devices on an HDMT TDAU Bank card.  Correct VID/PIDs are required to ensure Windows Device
//! Manager displays a proper description of the device, and HIL requires correct VID/PIDs to ensure
//! software can locate and communicate with the correct component.
//! @note FTDI devices have a limited number of updates, but this function is safe to call because it
//! only writes if the component programming is incorrect.
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS tdbVidPidsSet(_In_ INT slot);

//! @brief Reads a TDAU Bank card's CPLD version number.
//!
//! This function reads one of the CPLD version numbers from a TDAU Bank card.
//!
//! @param[in] slot A physical slot number.  Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[out] pVersion Returns the version register value.  The pointer must not be NULL.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS tdbCpldVersion(_In_ INT slot, _Out_ LPDWORD pVersion);

//! @brief Reads a TDAU Bank card's CPLD version string.
//!
//! This function reads the version register of a TDAU Bank card's CPLD and converts it to an ASCII string representation.
//!
//! @param[in] slot A physical slot number.  Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[out] pVersion Returns the version string.  The pointer must not be NULL.
//! @param[in] length The length of the \c pVersion buffer.  It should be at least five characters to hold a null-terminated version string.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS tdbCpldVersionString(_In_ INT slot, _Out_writes_z_(length) LPSTR pVersion, _In_ DWORD length);

//! @brief Reads a TDAU Bank card's FPGA version number.
//!
//! This function reads the FPGA version number from a TDAU Bank card.  The returned 32-bit value is in the
//! format 0xMMMMmmmm where the upper 16-bits are the major version and lower 16-bits are the minor version.
//!
//! For example 0x00050001 is version 5.1.
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[out] pVersion The address of a DWORD of memory.  It may not be NULL.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS tdbFpgaVersion(_In_ INT slot, _Out_ LPDWORD pVersion);

//! @brief Reads a TDAU Bank card's FPGA version string.
//!
//! This function reads the version register of a TDAU Bank card's FPGA and converts it to an ASCII string representation.
//!
//! @param[in] slot A physical slot number.  Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[out] pVersion Returns the version string.  The pointer must not be NULL.
//! @param[in] length The length of the \c pVersion buffer.  It should be at least #HIL_MAX_VERSION_STRING_SIZE characters to hold a null-terminated version string.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS tdbFpgaVersionString(_In_ INT slot, _Out_writes_z_(length) LPSTR pVersion, _In_ DWORD length);

//! @brief Disables the PCI device driver for the selected TDAU Bank card.
//!
//! This function disables the PCI device driver for the selected TDAU Bank card.  It should be called before using functions
//! such as tdbFpgaLoad() that affect the hardware used by the driver.
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS tdbDeviceDisable(_In_ INT slot);

//! @brief Enables the PCI device driver for the selected TDAU Bank card.
//!
//! This function enables the PCI device driver for the selected TDAU Bank card.  It should be called after using functions
//! such as tdbFpgaLoad() that affect the hardware used by the driver.
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS tdbDeviceEnable(_In_ INT slot);

//! @brief Loads an FPGA binary image file into a TDAU Bank card FPGA.
//!
//! This function loads an FPGA binary image file into a TDAU Bank card FPGA.  This image is volatile and the FPGA
//! will load its original base program from its SPI FLASH after a cold reboot.
//! @warning TDAU Bank device drivers are sensitive to FPGA changes.  If they are present, wrap this call in tdbDeviceDisable() and tdbDeviceEnable().
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] filename An ANSI string containing an absolute or relative (to the current directory) FPGA binary filename.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS tdbFpgaLoad(_In_ INT slot, _In_z_ LPCSTR filename);

//! @brief Programs an FPGA image file into the base SPI FLASH used to boot initialize a TDAU Bank card FPGA.
//!
//! This function programs an FPGA image file into the base SPI FLASH used to boot initialize a TDAU Bank card FPGA.
//!
//! The callback and context parameters are used to report progress back during a long operation.  See #HIL_BASE_LOAD_CALLBACK
//! for details.
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] filename An ANSI string containing an absolute or relative (to the current directory) FPGA binary filename.
//! @param[in] callback The callback function to be called with progress updates while programming the SPI FLASH.  Value can be NULL.
//! @param[in] context The context that will be passed back as an argument in the #HIL_BASE_LOAD_CALLBACK callback function.  Value can be NULL.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS tdbFpgaBaseLoad(_In_ INT slot, _In_z_ LPCSTR filename, _In_opt_ HIL_BASE_LOAD_CALLBACK callback, _In_opt_ void* context);

//! @brief Compares an existing image within the base SPI FLASH on a TDAU Bank card against an FPGA binary image file.
//!
//! This function compares an existing image within the base SPI FLASH on a TDAU Bank card against an FPGA binary image file
//! and optionally dumps that image into a separate binary file.
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] imageFilename An ANSI string containing an absolute or relative (to the current directory) FPGA binary filename to be compared against.
//! @param[in] dumpFilename An ANSI string containing an absolute or relative (to the current directory) binary dump filename.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS tdbFpgaBaseVerify(_In_ INT slot, _In_z_ LPCSTR imageFilename, _In_opt_z_ LPCSTR dumpFilename);

//! @brief Executes a Xilinx .XSVF file over a TDAU Bank card's JTAG interface.
//!
//! This function executes a Xilinx .XSVF file over a TDAU Bank card's JTAG interface.  A Xilinx CPLD is the only device
//! in the scan chain.  An appropriate .XSVF file can be generated through Xilinx's iMPACT software to perform
//! functions such as programming and/or verifying the content of the CPLD.
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] filename An ANSI string containing an absolute or relative (to the current directory) .XSVF binary filename.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS tdbJtagExecute(_In_ INT slot, _In_z_ LPCSTR filename);

//! @brief Loads an FPGA binary image buffer into a TDAU Bank card FPGA.
//!
//! This function loads an FPGA binary image buffer into a TDAU Bank card FPGA.  This image is volatile and the FPGA
//! will load its original base program from its SPI FLASH after a cold reboot.
//! @warning TDAU Bank device drivers are sensitive to FPGA changes.  If they are present, wrap this call in tdbDeviceDisable() and tdbDeviceEnable().
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] pData The FPGA image data.
//! @param[in] length The length of the FPGA image data in bytes.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS tdbFpgaBufferLoad(_In_ INT slot, _In_reads_bytes_(length) LPCVOID pData, _In_ DWORD length);

//! @brief Programs an FPGA image buffer into the base SPI FLASH used to boot initialize a TDAU Bank card FPGA.
//!
//! This function programs an FPGA image buffer into the base SPI FLASH used to boot initialize a TDAU Bank card FPGA.
//!
//! The callback and context parameters are used to report progress back during a long operation.  See #HIL_BASE_LOAD_CALLBACK
//! for details.
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] pData The FPGA image data.
//! @param[in] length The length of the FPGA image data in bytes.
//! @param[in] callback The callback function to be called with progress updates while programming the SPI FLASH.  Value can be NULL.
//! @param[in] context The context that will be passed back as an argument in the #HIL_BASE_LOAD_CALLBACK callback function.  Value can be NULL.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS tdbFpgaBaseBufferLoad(_In_ INT slot, _In_reads_bytes_(length) LPCVOID pData, _In_ DWORD length, _In_opt_ HIL_BASE_LOAD_CALLBACK callback, _In_opt_ void* context);

//! @brief Execute a buffer containing Xilinx XSVF data over a TDAU Bank card's JTAG interface.
//!
//! This function executes a Xilinx XSVF data buffer over a TDAU Bank card's JTAG interface.  A Xilinx CPLD
//! is the only device in the scan chain.  XSVF data can be generated through Xilinx's iMPACT software to perform
//! functions such as programming and/or verifying the content of the CPLD.
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] pData The XSVF image data.
//! @param[in] length The length of the XSVF image data in bytes.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS tdbJtagBufferExecute(_In_ INT slot, _In_reads_bytes_(length) LPCVOID pData, _In_ DWORD length);

//! @brief Reads a 32-bit register in one of the BAR regions of a TDAU Bank card.
//!
//! This function reads a 32-bit register in one of the BAR regions of a TDAU Bank card.
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] bar The PCI base address register region to access.  Valid values are 0 and 1.
//! @param[in] offset The DWORD-aligned offset address in BAR0 memory to read.
//! @param[out] pData The address of a DWORD to contain the result.  It cannot be NULL.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS tdbBarRead(_In_ INT slot, _In_ UINT bar, _In_ DWORD offset, _Out_ LPDWORD pData);

//! @brief Writes a 32-bit register in one of the BAR regions of a TDAU Bank card.
//!
//! This function writes a 32-bit register in one the BAR regions of a TDAU Bank card.
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] bar The PCI base address register region to access.  Valid values are 0 and 1.
//! @param[in] offset The DWORD-aligned offset address in BAR0 memory to write.
//! @param[in] data The DWORD value to write.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS tdbBarWrite(_In_ INT slot, _In_ UINT bar, _In_ DWORD offset, _In_ DWORD data);

//! @brief Reads the board-level traceability values from a TDAU Bank instrument assembly.
//!
//! This function reads the board-level traceability values from a TDAU Bank instrument assembly.  The values are defined in the #BLT structure.
//! This data refers to the TDAU Bank card in the specified slot as a collective instrument.
//!
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[out] pBlt The address of a #BLT structure that will be populated with the data.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS tdbBltInstrumentRead(_In_ INT slot, _Out_ PBLT pBlt);

//! @brief Writes the board-level traceability values for a TDAU Bank instrument.
//!
//! This function writes the board-level traceability values for a TDAU Bank instrument assembly.
//! The values are defined in the #BLT structure.
//! This data refers to the TDAU Bank card in the specified slot as a collective instrument.
//! Use tdbBltBoardWrite() to write the mainboard BLT, and use tdbBltRiserWrite() to write the riser board BLTs.
//!
//! The \c flags field allows this function to update individual values.  The flags can be ORed together.  Only the fields indicated by
//! \c flags need to be populated in the provided \c pBlt parameter.
//!
//! \par Updating the Full BLT - C
//! \code{.c}
//! #include <Windows.h>
//! #include "TdauBankApi.h"
//! #include "string.h"
//!
//! int main()
//! {
//!     int slot = 9;
//!     BLT blt;
//!     HIL_STATUS status;
//!     blt.Id = 0x100000FF; // Not the actual ID.
//!     strcpy(blt.DeviceName,"DeviceName");
//!     strcpy(blt.VendorName,"INTEL");
//!     strcpy(blt.PartNumberAsBuilt,"IAxxxxxx-xxx");
//!     strcpy(blt.PartNumberCurrent,blt.PartNumberAsBuilt);
//!     strcpy(blt.SerialNumber,"12345678");
//!     strcpy(blt.ManufactureDate,"YYMMDD");
//!     status = tdbBltInstrumentWrite(slot, &blt, BF_ALL);
//!     if (status != HS_SUCCESS)
//!         return -1;
//!     return 0;
//! }
//! \endcode
//! \par Updating the Current Part Number - Python
//! \code{.py}
//! slot = 9
//! blt = hil.BLT()
//! blt.PartNumberCurrent = 'IAyyyyyy-yyy'
//! hil.tdbBltInstrumentWrite(slot,blt,hil.BF_PART_NUMBER_CURRENT)
//! # Note Python returns None for HIL_STATUS if successful and raises RuntimeError on failure.
//! \endcode
//! \par Updating the Current Part Number - Python
//! \code{.py}
//! slot = 8
//! hil.tdbBltInstrumentWrite(slot,None,0) # Note flags is ignored
//! \endcode
//!
//! \note HS_BLT_NOT_FOUND can be returned on an update (lack of #BF_ALL flag).
//!
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] pBlt The address of a #BLT structure that contains the data to be written or updated.  If NULL, erases the existing instrument BLT.
//! @param[in] flags #BLT_FLAGS bits indicating what fields in \c pBlt are valid.  Ignored if pBlt is NULL.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS tdbBltInstrumentWrite(_In_ INT slot, _In_opt_ PCBLT pBlt, _In_ BLT_FLAGS flags);

//! @brief Reads the board-level traceability values from a TDAU Bank card.
//!
//! This function reads the board-level traceability values from a TDAU Bank card.
//! @note Use tdbBltInstrumentRead() for the traceability values of the assembled TDAU Bank instrument.
//!
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[out] pBlt The address of a #BLT structure that will be populated with the data.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS tdbBltBoardRead(_In_ INT slot, _Out_ PBLT pBlt);

//! @brief Writes the board-level traceability values for a TDAU Bank card.
//!
//! This function writes the board-level traceability values from a TDAU Bank card.
//! @note Use tdbBltInstrumentWrite() to update the traceability values of the assembled TDAU Bank instrument.
//!
//! The \c flags field allows this function to update individual values.  The flags can be ORed together.  Only the fields indicated by
//! \c flags need to be populated in the provided \c pBlt parameter.
//!
//! \par Updating the Full BLT - C
//! \code{.c}
//! #include <Windows.h>
//! #include "TdauBankApi.h"
//! #include "string.h"
//!
//! int main()
//! {
//!     int slot = 0;
//!     BLT blt;
//!     HIL_STATUS status;
//!     blt.Id = 0x100000FF; /* NOT the actual card ID */
//!     strcpy(blt.DeviceName,"DeviceName");
//!     strcpy(blt.VendorName,"INTEL");
//!     strcpy(blt.PartNumberAsBuilt,"AAxxxxxx-xxx");
//!     strcpy(blt.PartNumberCurrent,blt.PartNumberAsBuilt);
//!     strcpy(blt.SerialNumber,"12345678");
//!     strcpy(blt.ManufactureDate,"YYMMDD");
//!     status = tdbBltBoardWrite(slot, &blt, BF_ALL);
//!     if (status != HS_SUCCESS)
//!         return -1;
//!     return 0;
//! }
//! \endcode
//! \par Updating the Current Part Number - Python
//! \code{.py}
//! slot = 0
//! blt = hil.BLT()
//! blt.PartNumberCurrent = 'AAyyyyyy-yyy'
//! hil.tdbBltBoardWrite(slot,blt,hil.BF_PART_NUMBER_CURRENT)
//! # Note Python returns None for HIL_STATUS if successful and raises RuntimeError on failure.
//! \endcode
//!
//! \note HS_BLT_NOT_FOUND can be returned on an update (lack of #BF_ALL flag).
//!
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] pBlt The address of a #BLT structure that contains the data to be written or updated.
//! @param[in] flags #BLT_FLAGS bits indicating what fields in \c pBlt are valid.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS tdbBltBoardWrite(_In_ INT slot, _In_ PCBLT pBlt, _In_ BLT_FLAGS flags);

//! @brief Reads the raw data from the specified TDAU Bank card's BLT EEPROM.
//!
//! This function reads the raw data from the specified TDAU Bank card's BLT EEPROM.
//!
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[out] pEepromData The output buffer.
//! @param[in,out] pLength On input, the size of the output buffer referenced by \c pData.  On output, the size of the EEPROM data.
//! @retval HS_INSUFFICIENT_BUFFER The buffer provided was too small.  \c pLength will indicate the minimum buffer size required.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS tdbBltEepromRead(_In_ INT slot, _Out_writes_bytes_to_(_Old_(*pLength), *pLength) LPVOID pEepromData, _Inout_ LPDWORD pLength);

//! @brief Sends a write command and data bytes to the specified TDAU on a TDAU Bank card.
//!
//! This function sends a write command and data bytes to the specified TDAU on a TDAU Bank card.
//! @note Some TDAU commands do not send a data byte like LOCK, UNLOCK etc. For Python, use \c b'' (an empty byte string) for the data.
//! @param[in] slot   A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] index  Targets a TDAU for the command (0-6).
//! @param[in] cmd    TDAU write command byte to send.
//! @param[in] pData  The address of the data bytes to write.  It must hold at least \c length bytes.
//! @param[in] length The number of data bytes to write (0-255).
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS tdbTdauWrite(_In_ INT slot, _In_ INT index, _In_ BYTE cmd, _In_reads_bytes_opt_(length) const void* pData, _In_ DWORD length);

//! @brief Sends a read command and reads data bytes from the specified TDAU on a TDAU Bank card.
//!
//! This function sends a read command and reads data bytes from the specified TDAU on a TDAU Bank card.
//! @note The documented TDAU read commands include as returned data a checksum byte.  Do not include this byte when
//!       requesting data.  It is read and validated internally by this function.
//! @param[in] slot   A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] index  Targets a TDAU for the command (0-6).
//! @param[in] cmd    TDAU read command byte to send.
//! @param[out] pData The address of a buffer to hold the result.  It must hold at least \c length bytes of data.
//! @param[in] length The number of data bytes to read (1-255).  Do not include the checksum byte listed in the TDAU documentation.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS tdbTdauRead(_In_ INT slot, _In_ INT index, _In_ BYTE cmd, _Out_writes_bytes_all_(length) LPVOID pData, _In_ DWORD length);

//! @brief Writes data to a TDAU Bank card's memory map.
//!
//! This function writes data to the TDAU Bank card's memory map.
//! @param[in] slot   A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] index  Targets a TDAU for the command (0-6).
//! @param[in] addr   The starting memory map address.  Data bytes are written sequentially starting at this address.
//! @param[in] pData  The address of the data bytes to write.  It must hold at least \c length bytes.
//! @param[in] length The number of data bytes to write (1-255).
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS tdbTdauWriteMem(_In_ INT slot, _In_ INT index, _In_ WORD addr, _In_reads_bytes_(length) const void* pData, _In_ DWORD length);

//! @brief Reads data from a TDAU Bank card's memory map.
//!
//! This function reads data from the TDAU Bank card's memory map.
//! @param[in] slot   A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] index  Targets a TDAU for the command (0-6).
//! @param[in] addr   The starting memory map address.  Data bytes are read sequentially starting at this address.
//! @param[out] pData The address of a buffer to hold the result.  It must hold at least \c length bytes.
//! @param[in] length The length of the \c pData buffer in bytes (1-255).
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS tdbTdauReadMem(_In_ INT slot, _In_ INT index, _In_ WORD addr, _Out_writes_bytes_all_(length) LPVOID pData, _In_ DWORD length);

//! @brief Writes a command within the BMR454 (Intermediate Bus Converter) device on a TDAU bank card.
//!
//! This function writes a command to the BMR454 (Intermediate Bus Converter) device on a TDAU bank card.
//! @note Some BMR454 command writes cause the device to NACK further commands for some amount of time (sometimes as
//!       long as 500ms).
//! @param[in] slot      A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] command   BMR454 command to write.
//! @param[in] pCmdData  A data buffer of bytes to write to the BMR454 device via the I2C bus for the associated
//!                      \c command.  Note that the bytes are sent to the device sequentially starting with the first
//!                      byte pointed to by \c pCmdData.  Also note that the I2C address and the PEC (Packet
//!                      Error Checking) bytes should not be included in \c pCmdData because this is handled
//!                      automatically within the API.  Lastly, the command byte should not be included in this
//!                      buffer as it is already supplied in \c command.  Note that this parameter may be
//!                      NULL for commands that have no data associated with them.
//! @param[in] cmdLength The byte length of the \c pCmdData buffer.  Note that this parameter may be 0 for commands
//!                      that have no data.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS tdbBmr454Write(_In_ INT slot, _In_ BYTE command, _In_reads_bytes_opt_(cmdLength) const void* pCmdData, _In_ DWORD cmdLength);

//! @brief Reads a command within the BMR454 (Intermediate Bus Converter) device on a TDAU bank card.
//!
//! This function reads a command from the BMR454 (Intermediate Bus Converter) device on a TDAU bank card.
//!
//! @note Some BMR454 command reads cause the device to NACK further commands for some amount of time (sometimes as
//!       long as 500ms).
//! @param[in] slot       A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] command    BMR454 command to write.
//! @param[out] pReadData A data buffer of bytes to hold the data read from the BMR454 device via the
//!                       I2C bus.  Note that the bytes received from the device are located sequentially
//!                       in the buffer in the order they were received with the first byte being located
//!                       directly at \c pReadData.  Also note that the PEC (Packet Error Checking) byte
//!                       is automatically checked within the API and is not included in \c pReadData.
//!                       Because of this, the PEC byte should not be included in the \c readLength value.
//!                       Lastly, this parameter cannot be NULL since all reads return at least one byte.
//! @param[in] readLength The byte length of the \c pReadData buffer.  Note that this parameter cannot be 0
//!                       since all reads return at least one byte.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS tdbBmr454Read(_In_ INT slot, _In_ BYTE command, _Out_writes_bytes_all_(readLength) LPVOID pReadData, _In_ DWORD readLength);

//! @brief Reads from the DDR memory of a TDAU bank card via DMA.
//!
//! This function reads from the DDR memory of a TDAU bank card through its FPGA DMA interface.
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] address The zero-based address within the DDR memory to read.  This must be a multiple of four and the read must be within a 512MB address space.
//! @param[out] pData The address of a buffer to hold the result.
//! @param[in] length The length of the \c pData buffer in bytes.  This must be a multiple of four and the read must be within a 512MB address space.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS tdbDmaRead(_In_ INT slot, _In_ INT64 address, _Out_writes_bytes_all_(length) LPVOID pData, _In_ DWORD length);

//! @brief Writes to the DDR memory of a TDAU bank card via DMA.
//!
//! This function writes to the DDR memory of a TDAU bank card through its FPGA DMA interface.
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] address The zero-based address within the DDR memory to write.  This must be a multiple of four and the write must be within a 512MB address space.
//! @param[in] pData The address of a buffer containing the data to write.
//! @param[in] length The length of the \c pData buffer in bytes.  This must be a multiple of four and the write must be within a 512MB address space.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS tdbDmaWrite(_In_ INT slot, _In_ INT64 address, _In_reads_bytes_(length) const void* pData, _In_ DWORD length);

//! @brief Reads a temperature value from a PECI channel of a TDAU bank card.
//!
//! This function reads a temperature value from a PECI channel of a TDAU bank card.  tdbPeciConfig() must be called to enable
//! temperature reads for the corresponding channel.
//! @param[in] slot    A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] channel A physical channel number. Valid values are 0 to 17.
//! @param[out] pTemp  The returned temperature in degrees Celsius.
//! @retval HS_SUCCESS     The call succeeded.
//! @retval HS_IO_ERROR    An I/O error occurred.
//! @retval HS_NOT_ENABLED The channel is not enabled.  See tdbPeciConfig().
HIL_API HIL_STATUS tdbPeciTemperatureRead(_In_ INT slot, _In_ INT channel, _Out_ double* pTemp);

//! @brief Configures a PECI channel of a TDAU bank card.
//!
//! This function configures a PECI channel.  It specifies the whether the channel is enabled or disabled, the PECI address,
//! and the bit rate of the channel in bits/sec.
//! @param[in] slot    A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] channel A physical PECI channel number. Valid values are 0 to 17.
//! @param[in] enable  TRUE to enable temperature measurements, FALSE otherwise.
//! @param[in] address Address of the PECI channel.
//! @param[in] bitrate The bit rate of the channel in bits per second.  Valid values are 2000 to 2000000.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS tdbPeciConfig(_In_ INT slot, _In_ INT channel, _In_ BOOL enable, _In_ BYTE address, _In_ DWORD bitrate);

//! @brief Reads one of nine voltage monitor channels on a TDAU bank card.
//!
//! This function reads one of the nine monitor channels on a TDAU bank card.
//! @note tdbInit() is required to be called before using this function.
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] channel Selects the monitoring channel. Valid values are 0 to 8.  The channels
//! and corresponding voltages monitored are (per TDAU bank fab B schematic):
//!                    | Channel |  Reference       | Notes         |
//!                    | :-----: |  :---------:     | :--------     |
//!                    | 0       |   +5V_24A        | VDD for U85   |
//!                    | 1       |   +3P3V_12A      |               |
//!                    | 2       |   1P5V_12A       |               |
//!                    | 3       |   1P2V_12A       |               |
//!                    | 4       |   +1P8V_SYS      |               |
//!                    | 5       |   No connect     |               |
//!                    | 6       |   +12V_11A       |               |
//!                    | 7       |   No connect     |               |
//!                    | 8       |   No connect     |               |
//! @param[out] pVoltage The voltage in volts.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS tdbVmonRead(_In_ INT slot, _In_ INT channel, _Out_ double* pVoltage);

//! @brief Reads a temperature channel on a TDAU bank card.
//!
//! This function reads a temperature channel on a TDAU bank card.
//! @note tdbInit() is required to be called before using this function.
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] channel Selects the monitoring channel. Consult the following table:
//!                    | Channel |  Reference (Fab B)                 |
//!                    | :-----: |  :-------------------------------: |
//!                    | 0       |   U85 ADT7411 Internal Temperature |
//! @param[out] pTemp The temperature in degrees Celsius.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS tdbTmonRead(_In_ INT slot, _In_ INT channel, _Out_ double* pTemp);

//! @brief Directly writes an 8-bit register on the ADT7411(U85) on the TDAU bank card.
//!
//! This function writes an 8-bit register on ADT7411(U85) on the TDAU bank card.
//! @note tdbInit() is required to be called before using this function.
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] reg  A valid register address for the Analog Devices ADT7411.
//! @param[in] data The data to write.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS tdbAdt7411Write(_In_ INT slot, _In_ BYTE reg, _In_ BYTE data);

//! @brief Directly reads an 8-bit register on the ADT7411(U85) on the TDAU bank card.
//!
//! This function reads an 8-bit register on ADT7411(U85) on the TDAU bank card.
//! @note tdbInit() is required to be called before using this function.
//! @param[in] slot   A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] reg    A valid register address for the Analog Devices ADT7411.
//! @param[out] pData The register contents.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS tdbAdt7411Read(_In_ INT slot, _In_ BYTE reg, _Out_ PBYTE pData);

//! @brief Retrieves the user-mapped data streaming interface buffer for a TDAU bank card.
//!
//! This function retrieves the user-mapped data streaming interface buffer for a TDAU bank card.
//! See the Host Sensor Streaming and TDAUBANK FPGA HLDs for details on the content of this buffer.
//! @param[in]  slot     A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[out] pAddress The returned virtual address of the buffer.
//! @param[out] pSize    The returned size of the buffer in bytes.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS tdbStreamingBuffer(_In_ INT slot, _Out_ LPVOID* pAddress, _Out_ LPDWORD pSize);

#ifdef __cplusplus
}
#endif
