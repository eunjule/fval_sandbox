// INTEL CONFIDENTIAL
// Copyright 2018-2019 Intel Corporation All Rights Reserved.
//
// The source code contained or described herein and all documents related to
// the source code ("Material") are owned by Intel Corporation or its
// suppliers or licensors. Title to the Material remains with Intel
// Corporation or its suppliers and licensors. The Material contains trade
// secrets and proprietary and confidential information of Intel or its
// suppliers and licensors. The Material is protected by worldwide copyright
// and trade secret laws and treaty provisions. No part of the Material may be
// used, copied, reproduced, modified, published, uploaded, posted,
// transmitted, distributed, or disclosed in any way without Intel's prior
// express written permission.
//
// No license under any patent, copyright, trade secret or other intellectual
// property right is granted to or conferred upon you by disclosure or
// delivery of the Materials, either expressly, by implication, inducement,
// estoppel or otherwise. Any license under such intellectual property rights
// must be express and approved by Intel in writing.

//! @file
//! @brief APIs to control HBI DUT Power Supply (DPS) card.

#pragma once
#include "HilDefs.h"

#ifdef __cplusplus
extern "C" {
#endif

//! @brief Declares that a PSDB breakout board (BOB) configuration is present.
//!
//! This function declares that a PSDB breakout board (BOB) configuration is present;
//! otherwise, a DPS BOB configuration is currently assumed.  This affects how the
//! \c slot parameter is processed for other \c hbiDpsXxxxxx() APIs.
//!
//! In a DPS BOB configuration, The \c slot parameter in other hbiDpsXxxxxx() APIs indicates the HDMT tester slot of the PCIe
//! translator board used to host the DPS BOB (0-11).
//!
//! In a PSDB BOB configuration the \c slot parameter of this function indicates the HDMT tester slot of the PCIe translator
//! board used to host the PSDB BOB (0-11).  The \c slot parameter in other hbiDpsXxxxxx() APIs indicates the DPS slot within the
//! PSDB board itself (0-7).
//! @param[in] slot An HDMT tester physical slot number.  Valid values are 0 to \ref HIL_SLOTS - 1,
//!                 or -1 (the default) to reset back to the DPS BOB configuration.  This function is not valid for an HBI tester.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiDpsPsdbBob(_In_ INT slot);

//! @brief Initializes the components of an HBI DPS card for operation.
//!
//! This function initializes the components of an HBI DPS card for operation.  Calling this
//! function is required before using most other \c hbiDpsXXXXXX() functions.
//! @param[in] slot A physical slot number. Valid values in an HBI tester are 0 to \ref HBI_DPS_SLOTS - 1,
//!                 or \ref HIL_SLOTS - 1 in an HDMT tester using a DPS break-out board (BOB).
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiDpsInit(_In_ INT slot);

//! @brief Verifies an HBI DPS card is present.
//!
//! This function verifies an HBI DPS card is present.  It connects to and caches driver resources for use
//! by other \c hbiDpsXXXXX functions. hbiDpsDisconnect() is its complementary function and frees any cached resources.
//!
//! Neither hbiDpsXXXXX() or hbiDpsDisconnect() are required to be called to use the other \c hbiDpsXXXXX functions. All
//! \c hbiDpsXXXXX functions will allocate resources if they are not already allocated and free them if required to
//! perform their function.
//! @param[in] slot A physical slot number. Valid values in an HBI tester are 0 to \ref HBI_DPS_SLOTS - 1,
//!                 or \ref HIL_SLOTS - 1 in an HDMT tester using a DPS break-out board (BOB).
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiDpsConnect(_In_ INT slot);

//! @brief Frees any resources cached from using the HBI DPS card functions.
//!
//! This function frees any resources associated with using the HBI DPS card HIL functions. hbiDpsConnect() is its
//! complementary function.
//!
//! Neither hbiDpsXXXXX() or hbiDpsDisconnect() are required to be called to use the other \c hbiDpsXXXXX functions. All
//! \c hbiDpsXXXXX functions will allocate resources if they are not already allocated and free them if required to
//! perform their function.
//! @param[in] slot A physical slot number. Valid values in an HBI tester are 0 to \ref HBI_DPS_SLOTS - 1,
//!                 or \ref HIL_SLOTS - 1 in an HDMT tester using a DPS break-out board (BOB).
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiDpsDisconnect(_In_ INT slot);

//! @brief Programs the vendor and product IDs of all USB devices on an HBI DPS card.
//!
//! This function programs the vendor and product IDs (VID/PIDs) of the FT240X on an HBI DPS card.  Correct VID/PIDs are required to ensure Windows Device
//! Manager displays a proper description of the device, and HIL requires correct VID/PIDs to ensure
//! software can locate and communicate with the correct component.
//! @note FTDI devices have a limited number of updates, but this function is safe to call because it
//! only writes if the component programming is incorrect.
//! @param[in] slot A physical slot number. Valid values in an HBI tester are 0 to \ref HBI_DPS_SLOTS - 1,
//!                 or \ref HIL_SLOTS - 1 in an HDMT tester using a DPS break-out board (BOB).
//! @param[in] tester The tester type used from programming.  Valid values are \ref TESTER_HDMT or \ref TESTER_HBI.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiDpsVidPidsSet(_In_ INT slot, TESTER tester);

//! @brief Retrieves the user-mapped data streaming interface buffer for an HBI DPS.
//!
//! This function retrieves the user-mapped data streaming interface buffer for an HBI DPS.
//! @param[in]  slot     A physical slot number. Valid values in an HBI tester are 0 to \ref HBI_DPS_SLOTS - 1,
//!                      or \ref HIL_SLOTS - 1 in an HDMT tester using a DPS break-out board (BOB).
//! @param[out] pAddress The returned virtual address of the buffer.
//! @param[out] pSize    The returned size of the buffer in bytes.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiDpsStreamingBuffer(_In_ INT slot, _Out_ LPVOID* pAddress, _Out_ LPDWORD pSize);

//! @brief Reads an HBI DPS card's CPLD version number.
//!
//! This function reads the CPLD version number from an HBI DPS card.
//!
//! @param[in] slot A physical slot number. Valid values in an HBI tester are 0 to \ref HBI_DPS_SLOTS - 1,
//!                 or \ref HIL_SLOTS - 1 in an HDMT tester using a DPS break-out board (BOB).
//! @param[out] pVersion Returns the version register value.  The pointer must not be NULL.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiDpsCpldVersion(_In_ INT slot, _Out_ LPDWORD pVersion);

//! @brief Reads an HBI DPS card's CPLD version string.
//!
//! This function reads the version register of an HBI DPS card's CPLD and converts it to an ASCII string representation.
//!
//! @param[in] slot A physical slot number. Valid values in an HBI tester are 0 to \ref HBI_DPS_SLOTS - 1,
//!                 or \ref HIL_SLOTS - 1 in an HDMT tester using a DPS break-out board (BOB).
//! @param[out] pVersion Returns the version string.  The pointer must not be NULL.
//! @param[in] length The length of the \c pVersion buffer.  It should be at least five characters to hold a null-terminated version string.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiDpsCpldVersionString(_In_ INT slot, _Out_writes_z_(length) LPSTR pVersion, _In_ DWORD length);

//! @brief Reads an HBI DPS card's FPGA version number.
//!
//! This function reads the FPGA version number from an HBI DPS card.  The returned 32-bit value is in the
//! format 0xMMMMmmmm where the upper 16-bits are the major version and lower 16-bits are the minor version.
//!
//! For example 0x00050001 is version 5.1.
//! @param[in] slot A physical slot number. Valid values in an HBI tester are 0 to \ref HBI_DPS_SLOTS - 1,
//!                 or \ref HIL_SLOTS - 1 in an HDMT tester using a DPS break-out board (BOB).
//! @param[out] pVersion The address of a DWORD of memory.  It may not be NULL.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiDpsFpgaVersion(_In_ INT slot, _Out_ LPDWORD pVersion);

//! @brief Reads an HBI DPS card's FPGA version string.
//!
//! This function reads the version register of an HBI DPS card's FPGA and converts it to an ASCII string representation.
//! As of HIL 7.0, it also appends the compatibility register value.  The format is \c "MM.mm-C" where \c MM is the major version,
//! \c mm is the minor version, and \c C is the compatibility value.  All values are unsigned decimal numbers.
//!
//! @param[in] slot A physical slot number. Valid values in an HBI tester are 0 to \ref HBI_DPS_SLOTS - 1,
//!                 or \ref HIL_SLOTS - 1 in an HDMT tester using a DPS break-out board (BOB).
//! @param[out] pVersion Returns the version string.  The pointer must not be NULL.
//! @param[in] length The length of the \c pVersion buffer.  It should be at least #HIL_MAX_VERSION_STRING_SIZE characters to hold a null-terminated version string.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiDpsFpgaVersionString(_In_ INT slot, _Out_writes_z_(length) LPSTR pVersion, _In_ DWORD length);

//! @brief Returns an HBI DPS FPGA's base FLASH content information.
//!
//! This function returns an HBI DPS FPGA's base FLASH content information.
//! @param[in] slot A physical slot number. Valid values in an HBI tester are 0 to \ref HBI_DPS_SLOTS - 1,
//!                 or \ref HIL_SLOTS - 1 in an HDMT tester using a DPS break-out board (BOB).
//! @param[out] pInfo Returns FLASH_INFO structure describing the FLASH contents.  The pointer must not be NULL.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiDpsFpgaBaseInfo(_In_ INT slot, _Out_ PFLASH_INFO pInfo);

//! @brief Returns an HBI DPS FPGA's compatibility register value.
//!
//! This function returns an HBI DPS FPGA's compatibility register value.
//! @param[in] slot A physical slot number. Valid values in an HBI tester are 0 to \ref HBI_DPS_SLOTS - 1,
//!                 or \ref HIL_SLOTS - 1 in an HDMT tester using a DPS break-out board (BOB).
//! @param[out] pData Returns the value of the FPGA's compatibility register.  The pointer must not be NULL.  At a minimum, bits 0-3 of the returned value
//!                   indicates hardware capability.  An FPGA that reports a particular value in bits 0-3 should only be overwritten with a compatible image.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiDpsFpgaCompatibility(_In_ INT slot, _Out_ LPDWORD pData);

//! @brief Returns the source hash used to identify the source code that build the indicated FPGA.
//!
//! This function returns the source hash used to identify the source code that build the indicated FPGA.  The format of this field
//! is a 48-bit hexadecimal value with an optional trailing plus(+) indicating it was built from uncommitted sources.
//! @param[in] slot A physical slot number. Valid values in an HBI tester are 0 to \ref HBI_DPS_SLOTS - 1,
//!                 or \ref HIL_SLOTS - 1 in an HDMT tester using a DPS break-out board (BOB).
//! @param[out] pHash  Returns a string representation of the FPGA's source control hash. The pointer must not be NULL.
//! @param[in] length The length of the \c pHash buffer.  It should be at least #HIL_MAX_VERSION_STRING_SIZE characters to hold a null-terminated version string.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiDpsFpgaSourceHash(_In_ INT slot, _Out_writes_z_(length) LPSTR pHash, _In_ DWORD length);

//! @brief Returns the 64-bit value of the indicated FPGA's chip ID registers.
//!
//! This function returns the 64-bit value of the indicated FPGA's chip ID registers that uniquely identifies the FPGA.
//! @param[in] slot A physical slot number. Valid values in an HBI tester are 0 to \ref HBI_DPS_SLOTS - 1,
//!                 or \ref HIL_SLOTS - 1 in an HDMT tester using a DPS break-out board (BOB).
//! @param[out] pChipId Returns the 64-bit chip ID.  The pointer must not be NULL.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiDpsFpgaChipId(_In_ INT slot, _Out_ PDWORD64 pChipId);

//! @brief Disables the PCI device driver for the selected HBI DPS card.
//!
//! This function disables the PCI device driver for the selected HBI DPS card.  It should be called before using functions
//! such as hbiDpsFpgaLoad() that affect the hardware used by the driver.
//! @param[in] slot A physical slot number. Valid values in an HBI tester are 0 to \ref HBI_DPS_SLOTS - 1,
//!                 or \ref HIL_SLOTS - 1 in an HDMT tester using a DPS break-out board (BOB).
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiDpsDeviceDisable(_In_ INT slot);

//! @brief Enables the PCI device driver for the selected HBI DPS card.
//!
//! This function enables the PCI device driver for the selected HBI DPS card.  It should be called after using functions
//! such as hbiDpsFpgaLoad() that affect the hardware used by the driver.
//! @param[in] slot A physical slot number. Valid values in an HBI tester are 0 to \ref HBI_DPS_SLOTS - 1,
//!                 or \ref HIL_SLOTS - 1 in an HDMT tester using a DPS break-out board (BOB).
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiDpsDeviceEnable(_In_ INT slot);

//! @brief Loads an FPGA binary image file into an HBI DPS card FPGA.
//!
//! This function loads an FPGA binary image file into an HBI DPS card FPGA.  This image is volatile and the FPGA
//! will load its original base program from its SPI FLASH after a cold reboot.
//! @warning HBI DPS device drivers are sensitive to FPGA changes.  If they are present, wrap this call in hbiDpsDeviceDisable() and hbiDpsDeviceEnable().
//! @param[in] slot A physical slot number. Valid values in an HBI tester are 0 to \ref HBI_DPS_SLOTS - 1,
//!                 or \ref HIL_SLOTS - 1 in an HDMT tester using a DPS break-out board (BOB).
//! @param[in] filename An ANSI string containing an absolute or relative (to the current directory) FPGA binary filename.  As of HIL 7.0,
//!                     there must also be a <code><i>\<filename\></i>.mta</code> metadata file in the same directory as <code><i>\<filename\></i></code>.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiDpsFpgaLoad(_In_ INT slot, _In_z_ LPCSTR filename);

//! @brief Programs an FPGA image file into the base SPI FLASH used to boot initialize an HBI DPS card FPGA.
//!
//! This function programs an FPGA image file into the base SPI FLASH used to boot initialize an HBI DPS card FPGA.
//!
//! The callback and context parameters are used to report progress back during a long operation.  See #HIL_BASE_LOAD_CALLBACK
//! for details.
//! @param[in] slot A physical slot number. Valid values in an HBI tester are 0 to \ref HBI_DPS_SLOTS - 1,
//!                 or \ref HIL_SLOTS - 1 in an HDMT tester using a DPS break-out board (BOB).
//! @param[in] filename An ANSI string containing an absolute or relative (to the current directory) FPGA binary filename.  As of HIL 7.0,
//!                     there must also be a <code><i>\<filename\></i>.mta</code> metadata file in the same directory as <code><i>\<filename\></i></code>.
//! @param[in] callback The callback function to be called with progress updates while programming the SPI FLASH.  Value can be NULL.
//! @param[in] context The context that will be passed back as an argument in the #HIL_BASE_LOAD_CALLBACK callback function.  Value can be NULL.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiDpsFpgaBaseLoad(_In_ INT slot, _In_z_ LPCSTR filename, _In_opt_ HIL_BASE_LOAD_CALLBACK callback, _In_opt_ void* context);

//! @brief Validates the selected base SPI FLASH contents on an HBI DPS card against an FPGA binary image file.
//!
//! This function validates the selected base SPI FLASH contents on an HBI DPS card and optionally dumps the contents to a file.
//! @param[in] slot A physical slot number. Valid values in an HBI tester are 0 to \ref HBI_DPS_SLOTS - 1,
//!                 or \ref HIL_SLOTS - 1 in an HDMT tester using a DPS break-out board (BOB).
//! @param[in] dumpFilename An ANSI string containing an absolute or relative (to the current directory) binary dump filename.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiDpsFpgaBaseVerify(_In_ INT slot, _In_opt_z_ LPCSTR dumpFilename);

//! @brief Programs a CPLD .JBC file into an HBI DPS card CPLD.
//!
//! This function programs a CPLD .JBC file into an HBI DPS card CPLD.
//! @param[in] slot A physical slot number. Valid values in an HBI tester are 0 to \ref HBI_DPS_SLOTS - 1,
//!                 or \ref HIL_SLOTS - 1 in an HDMT tester using a DPS break-out board (BOB).
//! @param[in] filename An ANSI string containing an absolute or relative (to the current directory) CPLD .JBC filename.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiDpsCpldProgram(_In_ INT slot, _In_z_ LPCSTR filename);

//! @brief Programs a buffer containing CPLD .JBC file content into an HBI DPS card CPLD.
//!
//! This function programs a buffer containing CPLD .JBC file content into an HBI DPS card CPLD.
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] pData The CPLD .JBC image data.
//! @param[in] length The length of the CPLD .JBC image data in bytes.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiDpsCpldBufferProgram(_In_ INT slot, _In_reads_bytes_(length) LPCVOID pData, _In_ DWORD length);

//! @brief Compares an existing image within HBI DPS card CPLD against a CPLD .JBC file.
//!
//! This function compares an existing image within HBI DPS card CPLD against a CPLD .JBC file.
//! @param[in] slot A physical slot number. Valid values in an HBI tester are 0 to \ref HBI_DPS_SLOTS - 1,
//!                 or \ref HIL_SLOTS - 1 in an HDMT tester using a DPS break-out board (BOB).
//! @param[in] imageFilename An ANSI string containing an absolute or relative (to the current directory) CPLD .JBC filename to be compared against.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiDpsCpldVerify(_In_ INT slot, _In_z_ LPCSTR imageFilename);

//! @brief Compares an existing image within HBI DPS card CPLD against a buffer containing .JBC file content.
//!
//! This function compares an existing image within HBI DPS card CPLD against a buffer containing .JBC file content.
//! @param[in] slot A physical slot number. Valid values in an HBI tester are 0 to \ref HBI_DPS_SLOTS - 1,
//!                 or \ref HIL_SLOTS - 1 in an HDMT tester using a DPS break-out board (BOB).
//! @param[in] pData The CPLD .JBC image data to be compared against.
//! @param[in] length The length of the CPLD .JBC image data in bytes.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiDpsCpldBufferVerify(_In_ INT slot, _In_reads_bytes_(length) LPCVOID pData, _In_ DWORD length);

//! @brief Loads an FPGA binary image buffer into an HBI DPS card FPGA.
//!
//! This function loads an FPGA binary image buffer into an HBI DPS card FPGA.  This image is volatile and the FPGA
//! will load its original base program from its SPI FLASH after a cold reboot.
//! @warning HBI DPS device drivers are sensitive to FPGA changes.  If they are present, wrap this call in hbiDpsDeviceDisable() and hbiDpsDeviceEnable().
//! @param[in] slot A physical slot number. Valid values in an HBI tester are 0 to \ref HBI_DPS_SLOTS - 1,
//!                 or \ref HIL_SLOTS - 1 in an HDMT tester using a DPS break-out board (BOB).
//! @param[in] pData The FPGA image data.
//! @param[in] length The length of the FPGA image data in bytes.
//! @param[in] pMetaData  The FPGA image metadata.  This data is in a side-by-side file with .MTA extension with the original FPGA image.  It is
//!                       loaded automatically by hbiMbFpgaLoad() but needs to be read and provided to this version of the function.
//! @param[in] metaLength The length of the FPGA image metadata in bytes.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiDpsFpgaBufferLoad(_In_ INT slot, _In_reads_bytes_(length) LPCVOID pData, _In_ DWORD length, _In_reads_bytes_opt_(metaLength) LPCVOID pMetaData, _In_ DWORD metaLength);

//! @brief Programs an FPGA image buffer into the base SPI FLASH used to boot initialize an HBI DPS card FPGA.
//!
//! This function programs an FPGA image buffer into the base SPI FLASH used to boot initialize an HBI DPS card FPGA.
//!
//! The callback and context parameters are used to report progress back during a long operation.  See #HIL_BASE_LOAD_CALLBACK
//! for details.
//! @param[in] slot A physical slot number. Valid values in an HBI tester are 0 to \ref HBI_DPS_SLOTS - 1,
//!                 or \ref HIL_SLOTS - 1 in an HDMT tester using a DPS break-out board (BOB).
//! @param[in] pData The FPGA image data.
//! @param[in] length The length of the FPGA image data in bytes.
//! @param[in] pMetaData  The FPGA image metadata.  This data is in a side-by-side file with .MTA extension with the original FPGA image.  It is
//!                       loaded automatically by hbiMbFpgaBaseLoad() but needs to be read and provided to this version of the function.
//! @param[in] metaLength The length of the FPGA image metadata in bytes.
//! @param[in] callback The callback function to be called with progress updates while programming the SPI FLASH.  Value can be NULL.
//! @param[in] context The context that will be passed back as an argument in the #HIL_BASE_LOAD_CALLBACK callback function.  Value can be NULL.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiDpsFpgaBaseBufferLoad(_In_ INT slot, _In_reads_bytes_(length) LPCVOID pData, _In_ DWORD length, _In_reads_bytes_opt_(metaLength) LPCVOID pMetaData, _In_ DWORD metaLength, _In_opt_ HIL_BASE_LOAD_CALLBACK callback, _In_opt_ void* context);

//! @brief Reads a 32-bit register in one of the BAR regions of an HBI DPS card.
//!
//! This function reads a 32-bit register in one of the BAR regions of an HBI DPS card.
//! @param[in] slot A physical slot number. Valid values in an HBI tester are 0 to \ref HBI_DPS_SLOTS - 1,
//!                 or \ref HIL_SLOTS - 1 in an HDMT tester using a DPS break-out board (BOB).
//! @param[in] bar The PCI base address register region to access.  Valid values are 0 and 1.
//! @param[in] offset The DWORD-aligned offset address in BAR0 memory to read.
//! @param[out] pData The address of a DWORD to contain the result.  It cannot be NULL.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiDpsBarRead(_In_ INT slot, _In_ UINT bar, _In_ DWORD offset, _Out_ LPDWORD pData);

//! @brief Writes a 32-bit register in one of the BAR regions of an HBI DPS card.
//!
//! This function writes a 32-bit register in one the BAR regions of an HBI DPS card.
//! @param[in] slot A physical slot number. Valid values in an HBI tester are 0 to \ref HBI_DPS_SLOTS - 1,
//!                 or \ref HIL_SLOTS - 1 in an HDMT tester using a DPS break-out board (BOB).
//! @param[in] bar The PCI base address register region to access.  Valid values are 0 and 1.
//! @param[in] offset The DWORD-aligned offset address in BAR0 memory to write.
//! @param[in] data The DWORD value to write.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiDpsBarWrite(_In_ INT slot, _In_ UINT bar, _In_ DWORD offset, _In_ DWORD data);

//! @brief Reads from the DDR memory of an HBI DPS card via DMA.
//!
//! This function reads from the DDR memory of an HBI DPS card through its FPGA DMA interface.
//! @param[in] slot A physical slot number. Valid values in an HBI tester are 0 to \ref HBI_DPS_SLOTS - 1,
//!                 or \ref HIL_SLOTS - 1 in an HDMT tester using a DPS break-out board (BOB).
//! @param[in] address The zero-based address within the DDR memory to read.  This must be a multiple
//!                    of four and the read must be within a 2GB address space.
//! @param[out] pData The address of a buffer to hold the result.
//! @param[in] length The length of the \c pData buffer in bytes.  This must be a multiple of four and the read must be within a 512MB address space.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiDpsDmaRead(_In_ INT slot, _In_ INT64 address, _Out_writes_bytes_all_(length) LPVOID pData, _In_ DWORD length);

//! @brief Writes to the DDR memory of an HBI DPS card via DMA.
//!
//! This function writes to the DDR memory of an HBI DPS card through its FPGA DMA interface.
//! @param[in] slot A physical slot number. Valid values in an HBI tester are 0 to \ref HBI_DPS_SLOTS - 1,
//!                 or \ref HIL_SLOTS - 1 in an HDMT tester using a DPS break-out board (BOB).
//! @param[in] address The zero-based address within the DDR memory to write.  This must be a multiple
//!                    of four and the write must be within a 2GB address space.
//! @param[in] pData The address of a buffer containing the data to write.
//! @param[in] length The length of the \c pData buffer in bytes.  This must be a multiple of four and the write must be within a 2GB address space.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiDpsDmaWrite(_In_ INT slot, _In_ INT64 address, _In_reads_bytes_(length) const void* pData, _In_ DWORD length);

//! @brief Reads the board-level traceability values from an HBI DPS card.
//!
//! This function reads the board-level traceability values from an HBI DPS card.
//!
//! @param[in] slot A physical slot number. Valid values in an HBI tester are 0 to \ref HBI_DPS_SLOTS - 1,
//!                 or \ref HIL_SLOTS - 1 in an HDMT tester using a DPS break-out board (BOB).
//! @param[out] pBlt The address of a #BLT structure that will be populated with the data.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiDpsBltBoardRead(_In_ INT slot, _Out_ PBLT pBlt);

//! @brief Writes the board-level traceability values of an HBI DPS card.
//!
//! This function writes the board-level traceability values of an HBI DPS card.
//!
//! The \c flags field allows this function to update individual values.  The flags can be ORed together.  Only the fields indicated by
//! \c flags need to be populated in the provided \c pBlt parameter.
//!
//! \par Updating the Full BLT - C
//! \code{.c}
//! #include <Windows.h>
//! #include "HbiDpsApi.h"
//! #include "string.h"
//!
//! int main()
//! {
//!     int slot = 0;
//!     BLT blt;
//!     HIL_STATUS status;
//!     blt.Id = 0x100000FF; /* NOT the actual card ID */
//!     strcpy(blt.DeviceName,"DeviceName");
//!     strcpy(blt.VendorName,"INTEL");
//!     strcpy(blt.PartNumberAsBuilt,"AAxxxxxx-xxx");
//!     strcpy(blt.PartNumberCurrent,blt.PartNumberAsBuilt);
//!     strcpy(blt.SerialNumber,"12345678");
//!     strcpy(blt.ManufactureDate,"YYMMDD");
//!     status = hbiDpsBltBoardWrite(slot, &blt, BF_ALL);
//!     if (status != HS_SUCCESS)
//!         return -1;
//!     return 0;
//! }
//! \endcode
//! \par Updating the Current Part Number - Python
//! \code{.py}
//! slot = 0
//! blt = hil.BLT()
//! blt.PartNumberCurrent = 'AAyyyyyy-yyy'
//! hil.hbiDpsBltBoardWrite(slot,blt,hil.BF_PART_NUMBER_CURRENT)
//! # Note Python returns None for HIL_STATUS if successful and raises RuntimeError on failure.
//! \endcode
//!
//! \note HS_BLT_NOT_FOUND can be returned on an update (lack of #BF_ALL flag).
//!
//! @param[in] slot A physical slot number. Valid values in an HBI tester are 0 to \ref HBI_DPS_SLOTS - 1,
//!                 or \ref HIL_SLOTS - 1 in an HDMT tester using a DPS break-out board (BOB).
//! @param[in] pBlt The address of a #BLT structure that contains the data to be written or updated.
//! @param[in] flags #BLT_FLAGS bits indicating what fields in \c pBlt are valid.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiDpsBltBoardWrite(_In_ INT slot, _In_ PCBLT pBlt, _In_ BLT_FLAGS flags);

//! @brief Reads the raw data from the specified HBI DPS card's BLT EEPROM.
//!
//! This function reads the raw data from the specified HBI DPS card's BLT EEPROM.
//!
//! @param[in] slot A physical slot number. Valid values in an HBI tester are 0 to \ref HBI_DPS_SLOTS - 1,
//!                 or \ref HIL_SLOTS - 1 in an HDMT tester using a DPS break-out board (BOB).
//! @param[out] pEepromData The output buffer.
//! @param[in,out] pLength On input, the size of the output buffer referenced by \c pData.  On output, the size of the EEPROM data.
//! @retval HS_INSUFFICIENT_BUFFER The buffer provided was too small.  \c pLength will indicate the minimum buffer size required.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiDpsBltEepromRead(_In_ INT slot, _Out_writes_bytes_to_(_Old_(*pLength), *pLength) LPVOID pEepromData, _Inout_ LPDWORD pLength);

//! @brief Reads a voltage monitor channel on an HBI DPS card.
//!
//! This function reads a voltage monitor channel on an HBI DPS card.
//! @note -200 (original Fab B) is not supported.  Must have -201 rework to report channels 10-13 properly.
//!
//! @note Correctly programmed board BLT is a prerequisite for using this function as it relies on #BLT.PartNumberCurrent
//! to differentiate between fab revisions.
//!
//! @param[in] slot A physical slot number. Valid values in an HBI tester are 0 to \ref HBI_DPS_SLOTS - 1,
//!                 or \ref HIL_SLOTS - 1 in an HDMT tester using a DPS break-out board (BOB).
//! @param[in] channel Selects the monitoring channel. Valid values are 0 to 17.  The channels
//!                    and corresponding voltages monitored are (per HBI DPS schematics):
//!                    | Channel |  Fab A                | Fab B (-201 only!), D, E | Notes                                                  |
//!                    | :-----: |  :------------------: | :---------------------:  | :----------------------------------------------------: |
//!                    |    0    |   V5P0_SYS            |    V5P0_SYS              | VDD for U15 measured using internal 7.0V reference.    |
//!                    |    1    |   TEMP_LTM4644_V95_18 |    TEMP_LTM4644_V95_18   | Measured using internal 2.28V reference.               |
//!                    |    2    |   V2P5_DDR4_DF        |    V2P5_DDR4_MON         | Measured using internal 2.28V reference.               |
//!                    |    3    |   V3P3_ISNS_DF        |    V3P3_ISNS_MON         | Measured using internal 2.28V reference.               |
//!                    |    4    |   V5P0_SYS_DF         |    V12P0_SYS_MON         | Measured using internal 2.28V reference.  Note fab A/B voltage change! |
//!                    |    5    |   V3P15_DPS_DF        |    V3P15_DPS_MON         | Measured using internal 2.28V reference.               |
//!                    |    6    |   V1P8_DPS_DF         |    V1P8_DPS_MON          | Measured using internal 2.28V reference.               |
//!                    |    7    |   V1P2_DPS_DF         |    V1P2_DPS_MON          | Measured using internal 2.28V reference.               |
//!                    |    8    |   V0P95_DPS_DF        |    V0P95_DPS_MON         | Measured using internal 2.28V reference.               |
//!                    |    9    |          -            |    V5P0_SYS              | VDD for U39 measured using internal 7.0V reference.    |
//!                    |   10    |          -            |    +V10P2_LC_DPS_MON     | 2.28V ref. -201 rework reduces to 7V (on Fab B).       |
//!                    |   11    |          -            |    -V10P2_LC_DPS_MON     | 2.28V ref. -201 rework reduces to -2.4V (on Fab B).    |
//!                    |   12    |          -            |    +V10P2_HC_DPS_MON     | 2.28V ref. -201 rework reduces to 7V (on Fab B).       |
//!                    |   13    |          -            |    -V10P2_HC_DPS_MON     | 2.28V ref. -201 rework reduces to -2.4V (on Fab B).    |
//!                    |   14    |          -            |    V5P0_DPS_MON          | Measured using internal 2.28V reference.               |
//!                    |   15    |          -            |    V5P0_SYS_MON          | Measured using internal 2.28V reference.               |
//!                    |   16    |          -            |    SPARE1_ADC_TP         | Measured using internal 2.28V reference.               |
//!                    |   17    |          -            |    SPARE2_ADC_TP         | Measured using internal 2.28V reference.               |
//! @param[out] pVoltage The voltage in volts.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiDpsVmonRead(_In_ INT slot, _In_ INT channel, _Out_ double* pVoltage);

//! @brief Reads all nine voltage channels of an ADT7411 on an HBI DPS card.
//!
//! This function reads all nine voltage channels of an ADT7411 on an HBI DPS card.  The values are scaled by the factors implemented by
//! the voltage dividers feeding the AIN1-AIN8 inputs and represent the voltage at the signals listed in the \c chip table below, not the
//! voltage at the ADT7411 input.
//! @note -200 (original Fab B) is not supported.  Must have -201 rework to report channels AIN1-AIN8 properly on chip 1.
//!
//! @note Correctly programmed board BLT is a prerequisite for using this function as it relies on #BLT.PartNumberCurrent
//! to differentiate between fab revisions.
//!
//! @param[in] slot A physical slot number. Valid values in an HBI tester are 0 to \ref HBI_DPS_SLOTS - 1,
//!                 or \ref HIL_SLOTS - 1 in an HDMT tester using a DPS break-out board (BOB).
//! @param[in] chip Selects the ADT7411 to read. Valid values are 0 for Fab A and 0 or 1 for Fab B+.  The voltages for each chip are (per HBI DPS schematics):
//!                 | Chip | Voltages     | Fab A               | Fab B (-201 only!), D, E | Notes                                                  |
//!                 | :--: | :----------: | :-----------------: | :---------------------:  | :----------------------------------------------------: |
//!                 |  0   | pVoltages[0] | V5P0_SYS            |    V5P0_SYS              | VDD for U15 measured using internal 7.0V reference.    |
//!                 |  0   | pVoltages[1] | TEMP_LTM4644_V95_18 |    TEMP_LTM4644_V95_18   | Measured using internal 2.28V reference.               |
//!                 |  0   | pVoltages[2] | V2P5_DDR4_DF        |    V2P5_DDR4_MON         | Measured using internal 2.28V reference.               |
//!                 |  0   | pVoltages[3] | V3P3_ISNS_DF        |    V3P3_ISNS_MON         | Measured using internal 2.28V reference.               |
//!                 |  0   | pVoltages[4] | V5P0_SYS_DF         |    V12P0_SYS_MON         | Measured using internal 2.28V reference.  Note fab A/B voltage change! |
//!                 |  0   | pVoltages[5] | V3P15_DPS_DF        |    V3P15_DPS_MON         | Measured using internal 2.28V reference.               |
//!                 |  0   | pVoltages[6] | V1P8_DPS_DF         |    V1P8_DPS_MON          | Measured using internal 2.28V reference.               |
//!                 |  0   | pVoltages[7] | V1P2_DPS_DF         |    V1P2_DPS_MON          | Measured using internal 2.28V reference.               |
//!                 |  0   | pVoltages[8] | V0P95_DPS_DF        |    V0P95_DPS_MON         | Measured using internal 2.28V reference.               |
//!                 |  1   | pVoltages[0] |       n/a           |    V5P0_SYS              | VDD for U39 measured using internal 7.0V reference.    |
//!                 |  1   | pVoltages[1] |       n/a           |    +V10P2_LC_DPS_MON     | 2.28V ref. -201 rework reduces to 7V (on Fab B).       |
//!                 |  1   | pVoltages[2] |       n/a           |    -V10P2_LC_DPS_MON     | 2.28V ref. -201 rework reduces to -2.4V (on Fab B).    |
//!                 |  1   | pVoltages[3] |       n/a           |    +V10P2_HC_DPS_MON     | 2.28V ref. -201 rework reduces to 7V (on Fab B).       |
//!                 |  1   | pVoltages[4] |       n/a           |    -V10P2_HC_DPS_MON     | 2.28V ref. -201 rework reduces to -2.4V (on Fab B).    |
//!                 |  1   | pVoltages[5] |       n/a           |    V5P0_DPS_MON          | Measured using internal 2.28V reference.               |
//!                 |  1   | pVoltages[6] |       n/a           |    V5P0_SYS_MON          | Measured using internal 2.28V reference.               |
//!                 |  1   | pVoltages[7] |       n/a           |    SPARE1_ADC_TP         | Measured using internal 2.28V reference.               |
//!                 |  1   | pVoltages[8] |       n/a           |    SPARE2_ADC_TP         | Measured using internal 2.28V reference.               |
//! @param[out] pVoltages   An array of 9 readings in volts.
//! @param[in]  numVoltages The size of the \c pVoltages array.  Currently required to be 9.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiDpsVmonReadAll(_In_ INT slot, _In_ UINT chip, _Out_writes_all_(numVoltages) double* pVoltages, _In_ SIZE_T numVoltages);

//! @brief Reads a temperature channel on an HBI DPS card.
//!
//! This function reads a temperature channel on an HBI DPS card.
//! @note HBI DPS FPGA debug version F6_00_0022 is supported, as well as 1.0+ for channel 6.  Other debug versions are \b not supported.
//!       Internally, the implementation to read this temperature changed after F6_00_0022 and a bit was added in 1.0 to detect the difference.
//!
//! @note Correctly programmed board BLT is a prerequisite for using this function as it relies on #BLT.PartNumberCurrent
//! to differentiate between fab revisions.
//!
//! @param[in] slot A physical slot number. Valid values in an HBI tester are 0 to \ref HBI_DPS_SLOTS - 1,
//!                 or \ref HIL_SLOTS - 1 in an HDMT tester using a DPS break-out board (BOB).
//! @param[in] channel Selects the monitoring channel. Consult the following table:
//!                    | Channel |  Reference (Fab A)                 |Reference (Fab B, Fab D, Fab E)              |
//!                    | :-----: |  :-------------------------------: |:-------------------------------------------:|
//!                    |    0    |   U15 ADT7411 Internal Temperature | U15 ADT7411 Internal Temperature            |
//!                    |    1    |   U12 LTM4644 Internal Temperature | U12 LTM4644 Internal Temperature            |
//!                    |    2    |   U32 Max6627 Temperature          | U32 Max6628 Temperature                     |
//!                    |    3    |   U33 Max6627 Temperature          | U33 Max6628 Temperature                     |
//!                    |    4    |   U34 Max6627 Temperature          | U34 Max6628 Temperature                     |
//!                    |    5    |   U35 Max6627 Temperature          | U35 Max6628 Temperature                     |
//!                    |    6    |   U63 FPGA Temperature             | U63 FPGA Temperature (see note)             |
//!                    |    7    |          -                         | U39 ADT7411 Internal Temperature            |
//!                    |    8    |          -                         | U8 LTM4644 (LC 0 VREG) Rail Temperature     |
//!                    |    9    |          -                         | U7 LTM4644 (LC 1 VREG) Rail Temperature     |
//!                    |    10   |          -                         | U6 LTM4644 (LC 2 VREG) Rail Temperature     |
//!                    |    11   |          -                         | U5 LTM4644 (LC 3 VREG) Rail Temperature     |
//!                    |    12   |          -                         | U3 LTM4680 (HC 0 VREG) Rail 0 Temperature   |
//!                    |    13   |          -                         | U3 LTM4680 (HC 0 VREG) Rail 1 Temperature   |
//!                    |    14   |          -                         | U11 LTM4680 (HC 1 VREG) Rail 0 Temperature  |
//!                    |    15   |          -                         | U11 LTM4680 (HC 1 VREG) Rail 1 Temperature  |
//!                    |    16   |          -                         | U9 LTM4680 (HC 2 VREG) Rail 0 Temperature   |
//!                    |    17   |          -                         | U9 LTM4680 (HC 2 VREG) Rail 1 Temperature   |
//!                    |    18   |          -                         | U10 LTM4680 (HC 3 VREG) Rail 0 Temperature  |
//!                    |    19   |          -                         | U10 LTM4680 (HC 3 VREG) Rail 1 Temperature  |
//!                    |    20   |          -                         | U157 LTM4680 (HC 4 VREG) Rail 0 Temperature |
//!                    |    21   |          -                         | U157 LTM4680 (HC 4 VREG) Rail 1 Temperature |
//! @param[out] pTemp The temperature in degrees Celsius.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiDpsTmonRead(_In_ INT slot, _In_ INT channel, _Out_ double* pTemp);

//! @brief Directly writes an 8-bit register on the ADT7411(U15) of an HBI DPS card.
//!
//! This function writes an 8-bit register on ADT7411(U15) on an HBI DPS card.
//!
//! @note Correctly programmed board BLT is a prerequisite for using this function as it relies on #BLT.PartNumberCurrent
//! to differentiate between fab revisions.
//!
//! @param[in] slot A physical slot number. Valid values in an HBI tester are 0 to \ref HBI_DPS_SLOTS - 1,
//!                 or \ref HIL_SLOTS - 1 in an HDMT tester using a DPS break-out board (BOB).
//! @param[in] chip A zero-based chip index.  Consult the following table.
//!                 Note that reference designators may change in new fabs.
//!                 | Chip    |Fab A | Fab B |
//!                 | :-----: |:---: | :---: |
//!                 |    0    |U15   | U15   |
//!                 |    1    |  -   | U39   |
//! @param[in] reg  A valid register address for the Analog Devices ADT7411.
//! @param[in] data The data to write.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiDpsAdt7411Write(_In_ INT slot, _In_ UINT chip, _In_ BYTE reg, _In_ BYTE data);

//! @brief Directly reads an 8-bit register on the ADT7411(U15) of an HBI DPS card.
//!
//! This function reads an 8-bit register on ADT7411(U15) on an HBI DPS card.
//!
//! @note Correctly programmed board BLT is a prerequisite for using this function as it relies on #BLT.PartNumberCurrent
//! to differentiate between fab revisions.
//!
//! @param[in] slot A physical slot number. Valid values in an HBI tester are 0 to \ref HBI_DPS_SLOTS - 1,
//!                 or \ref HIL_SLOTS - 1 in an HDMT tester using a DPS break-out board (BOB).
//! @param[in] chip A zero-based chip index.  Consult the following table.
//!                 Note that reference designators may change in new fabs.
//!                 | Chip    | Fab A |Fab B |
//!                 | :-----: | :---: |:---: |
//!                 |    0    | U15   |U15   |
//!                 |    1    |   -   |U39   |
//! @param[in] reg    A valid register address for the Analog Devices ADT7411.
//! @param[out] pData The register contents.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiDpsAdt7411Read(_In_ INT slot, _In_ UINT chip, _In_ BYTE reg, _Out_ PBYTE pData);

//! @brief Reads a high current rail voltage on an HBI DPS card.
//!
//! This function reads a high current rail voltage on an HBI DPS card.
//! @param[in] slot A physical slot number. Valid values in an HBI tester are 0 to \ref HBI_DPS_SLOTS - 1,
//!                 or \ref HIL_SLOTS - 1 in an HDMT tester using a DPS break-out board (BOB).
//! @param[in]  rail     The rail number to read.  Valid values are 0-9.
//! @param[out] pVoltage A pointer to double that receives the voltage in volts.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiDpsHcRailVoltageRead(_In_ INT slot, _In_ UINT rail, _Out_ double* pVoltage);

//! @brief Reads a high current rail current on an HBI DPS card.
//!
//! This function reads a high current rail current on an HBI DPS card.
//! @param[in] slot A physical slot number. Valid values in an HBI tester are 0 to \ref HBI_DPS_SLOTS - 1,
//!                 or \ref HIL_SLOTS - 1 in an HDMT tester using a DPS break-out board (BOB).
//! @param[in]  rail     The rail number to read.  Valid values are 0-9.
//! @param[out] pCurrent A pointer to double that receives the current in amps.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiDpsHcRailCurrentRead(_In_ INT slot, _In_ UINT rail, _Out_ double* pCurrent);

//! @brief Reads a low current rail voltage on an HBI DPS card.
//!
//! This function reads a low current rail voltage on an HBI DPS card.
//! @param[in] slot A physical slot number. Valid values in an HBI tester are 0 to \ref HBI_DPS_SLOTS - 1,
//!                 or \ref HIL_SLOTS - 1 in an HDMT tester using a DPS break-out board (BOB).
//! @param[in]  rail     The rail number to read.  Valid values are 0-15.
//! @param[out] pVoltage A pointer to double that receives the voltage in volts.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiDpsLcRailVoltageRead(_In_ INT slot, _In_ UINT rail, _Out_ double* pVoltage);

//! @brief Reads a low current rail current on an HBI DPS card.
//!
//! This function reads a low current rail current on an HBI DPS card.
//! @param[in] slot A physical slot number. Valid values in an HBI tester are 0 to \ref HBI_DPS_SLOTS - 1,
//!                 or \ref HIL_SLOTS - 1 in an HDMT tester using a DPS break-out board (BOB).
//! @param[in]  rail     The rail number to read.  Valid values are 0-15.
//! @param[out] pCurrent A pointer to double that receives the current in amps.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiDpsLcRailCurrentRead(_In_ INT slot, _In_ UINT rail, _Out_ double* pCurrent);

//! @brief Issue a read command to the PMBUS interface on an LTC2975 Power Manager of an HBI DPS card.
//!
//! This function issues a read command to the PMBUS interface on an LTC2975 device of an HBI DPS card.
//! This call uses an interprocess lock to ensure the \c page command and the \c command itself are issued together.
//! @param[in] slot A physical slot number. Valid values in an HBI tester are 0 to \ref HBI_DPS_SLOTS - 1,
//!                 or \ref HIL_SLOTS - 1 in an HDMT tester using a DPS break-out board (BOB).
//! @param[in] chip A zero-based chip index.  Consult the following table.
//!                 Note that reference designators may change in new fabs.
//!                 |  Chip   | Fab A |
//!                 | :-----: | :---: |
//!                 |    0    | EU11  |
//!                 |    1    | EU10  |
//!                 |    2    | EU9   |
//!                 |    3    | EU8   |
//! @param[in] page      PMBUS page to pre-select the page via the PMBUS PAGE command that \c command will target.
//!                      Use -1 to leave the page set to its existing value or for commands that are not page sensitive.
//!                      See the LTC2975 data sheet.
//! @param[in] command    PMBUS command to read.  See the device spec sheet for valid commands.
//! @param[out] pReadData A data buffer of bytes to hold the data read from the associated \c command.
//!                       Note that the bytes received from the device are located sequentially
//!                       in the buffer in the order they were received with the first byte being located
//!                       directly at \c pReadData.  This parameter cannot be NULL since all reads return at least one byte.
//! @param[in] readLength The byte length of the \c pReadData buffer.  Note that this parameter cannot be zero
//!                       since all reads return at least one byte.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiDpsLtc2975PmbusRead(_In_ INT slot, _In_ UINT chip, _In_ INT page, _In_ BYTE command, _Out_writes_bytes_all_(readLength) LPVOID pReadData, _In_ DWORD readLength);

//! @brief Issue a write command to the PMBUS interface on an LTC2975 Power Manager of an HBI DPS card.
//!
//! This function issues a write command to the PMBUS interface on an LTC2975 device of an HBI DPS card.
//! This call uses an interprocess lock to ensure the \c page command and the \c command itself are issued together.
//! @param[in] slot A physical slot number. Valid values in an HBI tester are 0 to \ref HBI_DPS_SLOTS - 1,
//!                 or \ref HIL_SLOTS - 1 in an HDMT tester using a DPS break-out board (BOB).
//! @param[in] chip      A zero-based chip index.  Consult the following table.
//!                 Note that reference designators may change in new fabs.
//!                 | Chip    | Fab A |
//!                 | :-----: | :---: |
//!                 |    0    | EU11  |
//!                 |    1    | EU10  |
//!                 |    2    | EU9   |
//!                 |    3    | EU8   |
//! @param[in] page      PMBUS page to pre-select the page via the PMBUS PAGE command that \c command will target.
//!                      Use -1 to leave the page set to its existing value or for commands that are not page sensitive.
//!                      See the LTC2975 data sheet.
//! @param[in] command   PMBUS command to write. See the device spec sheet for valid commands.
//! @param[in] pCmdData  A data buffer of additional bytes to write for the associated \c command.
//!                      Note that the bytes are sent to the device sequentially starting with the first
//!                      byte pointed to by \c pCmdData.  The command byte should not be included in this
//!                      buffer as it is already supplied in \c command.  Note that this parameter may be
//!                      NULL for commands that have no data associated with them.
//! @param[in] cmdLength The byte length of the \c pCmdData buffer.  Note that this parameter may be zero for commands
//!                      that have no data.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiDpsLtc2975PmbusWrite(_In_ INT slot, _In_ UINT chip, _In_ INT page, _In_ BYTE command, _In_reads_bytes_opt_(cmdLength) const void* pCmdData, _In_ DWORD cmdLength);

//! @brief Issue a read command to the PMBUS interface on an LTM4680 Power Manager of an HBI DPS card.
//!
//! This function issues a read command to the PMBUS interface on an LTM4680 Power Manager of an HBI DPS card.
//! This call uses an interprocess lock to ensure the \c page command and the \c command itself are issued together.
//! @param[in] slot A physical slot number. Valid values in an HBI tester are 0 to \ref HBI_DPS_SLOTS - 1,
//!                 or \ref HIL_SLOTS - 1 in an HDMT tester using a DPS break-out board (BOB).
//! @param[in] chip      A zero-based chip index.  Consult the following table.
//!                 Note that reference designators may change in new fabs.
//!                 |  Chip   | Fab A |
//!                 | :-----: | :---: |
//!                 |    0    | U3    |
//!                 |    1    | U11   |
//!                 |    2    | U9    |
//!                 |    3    | U10   |
//!                 |    4    | U157  |
//! @param[in] page      PMBUS page to pre-select the page via the PMBUS PAGE command that \c command will target.
//!                      Use -1 to leave the page set to its existing value or for commands that are not page sensitive.
//!                      See the LTC2975 data sheet.
//! @param[in] command    PMBUS command to read.  See the device spec sheet for valid commands.
//! @param[out] pReadData A data buffer of bytes to hold the data read from the associated \c command.
//!                       Note that the bytes received from the device are located sequentially
//!                       in the buffer in the order they were received with the first byte being located
//!                       directly at \c pReadData.  This parameter cannot be NULL since all reads return at least one byte.
//! @param[in] readLength The byte length of the \c pReadData buffer.  Note that this parameter cannot be zero
//!                       since all reads return at least one byte.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiDpsLtm4680PmbusRead(_In_ INT slot, _In_ UINT chip, _In_ INT page, _In_ BYTE command, _Out_writes_bytes_all_(readLength) LPVOID pReadData, _In_ DWORD readLength);

//! @brief Issue a write command to the PMBUS interface on an LTM4680 Power Manager of an HBI DPS card.
//!
//! This function issues a write command to the PMBUS interface on an LTM4680 Power Manager of an HBI DPS card.
//! This call uses an interprocess lock to ensure the \c page command and the \c command itself are issued together.
//! @param[in] slot A physical slot number. Valid values in an HBI tester are 0 to \ref HBI_DPS_SLOTS - 1,
//!                 or \ref HIL_SLOTS - 1 in an HDMT tester using a DPS break-out board (BOB).
//! @param[in] chip      A zero-based chip index.  Consult the following table.
//!                 Note that reference designators may change in new fabs.
//!                 |  Chip   | Fab A |
//!                 | :-----: | :---: |
//!                 |    0    | U3    |
//!                 |    1    | U11   |
//!                 |    2    | U9    |
//!                 |    3    | U10   |
//!                 |    4    | U157  |
//! @param[in] page      PMBUS page to pre-select the page via the PMBUS PAGE command that \c command will target.
//!                      Use -1 to leave the page set to its existing value or for commands that are not page sensitive.
//!                      See the LTC2975 data sheet.
//! @param[in] command   PMBUS command to write. See the device spec sheet for valid commands.
//! @param[in] pCmdData  A data buffer of additional bytes to write for the associated \c command.
//!                      Note that the bytes are sent to the device sequentially starting with the first
//!                      byte pointed to by \c pCmdData.  The command byte should not be included in this
//!                      buffer as it is already supplied in \c command.  Note that this parameter may be
//!                      NULL for commands that have no data associated with them.
//! @param[in] cmdLength The byte length of the \c pCmdData buffer.  Note that this parameter may be zero for commands
//!                      that have no data.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiDpsLtm4680PmbusWrite(_In_ INT slot, _In_ UINT chip, _In_ INT page, _In_ BYTE command, _In_reads_bytes_opt_(cmdLength) const void* pCmdData, _In_ DWORD cmdLength);

//! @brief Issues a write command to an AD5676 device of an HBI DPS card.
//!
//! This function issues a write command to an AD5676 device of an HBI DPS card.
//! @param[in] slot A physical slot number. Valid values in an HBI tester are 0 to \ref HBI_DPS_SLOTS - 1,
//!                 or \ref HIL_SLOTS - 1 in an HDMT tester using a DPS break-out board (BOB).
//! @param[in] chip A zero-based chip index.  Consult the following table.
//!                 Note that reference designators may change in new fabs.
//!                 |   Chip  | Fab A |
//!                 | :-----: | :---: |
//!                 |    0    |  U29  |
//!                 |    1    |  U28  |
//! @param[in] command See Table 9 in spec sheet for valid commands. Valid values are 0-11.
//! @param[in] address See Table 10 in spec sheet for valid addresses.  Valid values are 0-7.
//! @param[in] data    16-bit data to write to the DAC.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiDpsAd5676Write(_In_ INT slot, _In_ UINT chip, _In_ BYTE command, _In_ BYTE address, _In_ WORD data);

//! @brief Issues a read command to an AD5676 device of an HBI DPS card.
//!
//! This function issues a read command to an AD5676 device of an HBI DPS card.
//! @param[in] slot A physical slot number. Valid values in an HBI tester are 0 to \ref HBI_DPS_SLOTS - 1,
//!                 or \ref HIL_SLOTS - 1 in an HDMT tester using a DPS break-out board (BOB).
//! @param[in] chip A zero-based chip index.  Consult the following table.
//!                 Note that reference designators may change in new fabs.
//!                 |  Chip   | Fab A |
//!                 | :-----: | :---: |
//!                 |    0    |  U29  |
//!                 |    1    |  U28  |
//! @param[in] address See Table 10 in spec sheet for valid addresses. Valid values are 0-7.
//! @param[out] pData  16-bit data read from the DAC.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiDpsAd5676Read(_In_ INT slot, _In_ UINT chip,  _In_ BYTE address, _Out_ LPWORD pData);

//! @brief Reads the voltage of the selected channel on the VMeasure ADC.
//!
//! This function reads the voltage of the selected channel on the VMeasure ADC.
//! @note HBI DPS FPGA debug version F6_00_0022 is supported, as well as 1.0+ for channel 6.  Other debug versions are \b not supported.
//!       Internally, the implementation to read these voltages changed after F6_00_0022 and a bit was added in 1.0 to detect the difference.
//! @param[in] slot A physical slot number. Valid values in an HBI tester are 0 to \ref HBI_DPS_SLOTS - 1,
//!                 or \ref HIL_SLOTS - 1 in an HDMT tester using a DPS break-out board (BOB).
//! @param[in] channel A zero-based channel index.  Consult the following table.
//!                    Note that reference designators may change in new fabs.
//!                 | Channel |      Fab A      |
//!                 | :-----: | :------------:  |
//!                 |    0    | ADC A Channel 0 |
//!                 |    1    | ADC A Channel 1 |
//!                 |    2    | ADC A Channel 2 |
//!                 |    3    | ADC A Channel 3 |
//!                 |    4    | ADC A Channel 4 |
//!                 |    5    | ADC A Channel 5 |
//!                 |    6    | ADC A Channel 6 |
//!                 |    7    | ADC A Channel 7 |
//!                 |    8    | ADC B Channel 0 |
//!                 |    9    | ADC B Channel 1 |
//!                 |    10   | ADC B Channel 2 |
//!                 |    11   | ADC B Channel 3 |
//!                 |    12   | ADC B Channel 4 |
//!                 |    13   | ADC B Channel 5 |
//!                 |    14   | ADC B Channel 6 |
//!                 |    15   | ADC B Channel 7 |
//! @param[out] pVoltage The voltage in volts.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiDpsVMeasureVoltageRead(_In_ INT slot, _In_ UINT channel, _Out_ double* pVoltage);

//! @brief Issues a write command to the VTarget Vmeasure Loopback Max14662 switch.
//!
//! This function issues a write command to the VTarget Vmeasure Loopback Max14662 switch.
//! @note These chips are default in shutdown(SD).  BAR 1 register 0x020C contains the shutdown bits.  See table.  Write 0 to enable.
//! @warning HIL release 7.7 and earlier used an incorrect multiprocess lock name to protect parallel I2C bus access. As the use
//!          case for these switches does not involve parallelism, that should not be an issue, but correct protection is not applied
//!          unless all processes accessing MAX14662 in parallel use a version later than HIL release 7.7.
//! @param[in] slot A physical slot number. Valid values in an HBI tester are 0 to \ref HBI_DPS_SLOTS - 1,
//!                 or \ref HIL_SLOTS - 1 in an HDMT tester using a DPS break-out board (BOB).
//! @param[in] chip A zero-based chip index.  Consult the following table.
//!                 Note that reference designators may change in new fabs.
//!                 |  Chip   | Fab A | SD bit |
//!                 | :-----: | :---: | :----: |
//!                 |    0    | EU15  |   0    |
//!                 |    1    | EU14  |   0    |
//!                 |    2    | EU12  |   1    |
//!                 |    3    | EU13  |   1    |
//! @param[in] data 8-bit control data written to the switch.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiDpsVtVmLbMax14662Write(_In_ INT slot, _In_ UINT chip, _In_ BYTE data);

//! @brief Issues a read command to the VTarget Vmeasure Loopback Max14662 switch.
//!
//! This function issues a read command to the VTarget Vmeasure Loopback Max14662 switch.
//! @note These chips are default in shutdown(SD).  BAR 1 register 0x020C contains the shutdown bits.  See table.  Write 0 to enable.
//! @warning HIL release 7.7 and earlier used an incorrect multiprocess lock name to protect parallel I2C bus access. As the use
//!          case for these switches does not involve parallelism, that should not be an issue, but correct protection is not applied
//!          unless all processes accessing MAX14662 in parallel use a version later than HIL release 7.7.
//! @param[in] slot A physical slot number. Valid values in an HBI tester are 0 to \ref HBI_DPS_SLOTS - 1,
//!                 or \ref HIL_SLOTS - 1 in an HDMT tester using a DPS break-out board (BOB).
//! @param[in] chip A zero-based chip index.  Consult the following table.
//!                 Note that reference designators may change in new fabs.
//!                 |  Chip   | Fab A | SD bit |
//!                 | :-----: | :---: | :----: |
//!                 |    0    | EU15  |   0    |
//!                 |    1    | EU14  |   0    |
//!                 |    2    | EU12  |   1    |
//!                 |    3    | EU13  |   1    |
//! @param[out] pData 8-bit control data read from the switch.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiDpsVtVmLbMax14662Read(_In_ INT slot, _In_ UINT chip, _Out_ PBYTE pData);

//! @brief Issues a write command to the ISource Vsense Test Max14662 switch.
//!
//! This function issues a write command to the ISource Vsense Test Max14662 switch.
//! @note These chips are default in shutdown.  Write BAR 1 register 0x020C bit 2 = 0 to disable shutdown.
//! @warning HIL release 7.7 and earlier used an incorrect multiprocess lock name to protect parallel I2C bus access. As the use
//!          case for these switches does not involve parallelism, that should not be an issue, but correct protection is not applied
//!          unless all processes accessing MAX14662 in parallel use a version later than HIL release 7.7.
//! @param[in] slot A physical slot number. Valid values in an HBI tester are 0 to \ref HBI_DPS_SLOTS - 1,
//!                 or \ref HIL_SLOTS - 1 in an HDMT tester using a DPS break-out board (BOB).
//! @param[in] chip A zero-based chip index.  Consult the following table.
//!                 Note that reference designators may change in new fabs.
//!                 |  Chip   | Fab A | SD bit |
//!                 | :-----: | :---: | :----: |
//!                 |    0    |  EU3  |   2    |
//!                 |    1    |  EU6  |   2    |
//!                 |    2    |  EU5  |   2    |
//!                 |    3    |  EU4  |   2    |
//! @param[in] data 8-bit control data written to the switch.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiDpsISourceVsenseMax14662Write(_In_ INT slot, _In_ UINT chip, _In_ BYTE data);

//! @brief Issues a read command to the ISource Vsense Test Max14662 switch.
//!
//! This function issues a read command to the ISource Vsense Test Max14662 switch.
//! @note These chips are default in shutdown(SD).  Write BAR 1 register 0x020C bit 2 = 0 to disable shutdown.
//! @warning HIL release 7.7 and earlier used an incorrect multiprocess lock name to protect parallel I2C bus access. As the use
//!          case for these switches does not involve parallelism, that should not be an issue, but correct protection is not applied
//!          unless all processes accessing MAX14662 in parallel use a version later than HIL release 7.7.
//! @param[in] slot A physical slot number. Valid values in an HBI tester are 0 to \ref HBI_DPS_SLOTS - 1,
//!                 or \ref HIL_SLOTS - 1 in an HDMT tester using a DPS break-out board (BOB).
//! @param[in] chip A zero-based chip index.  Consult the following table.
//!                 Note that reference designators may change in new fabs.
//!                 |  Chip   | Fab A | SD bit |
//!                 | :-----: | :---: | :----: |
//!                 |    0    |  EU3  |   2    |
//!                 |    1    |  EU6  |   2    |
//!                 |    2    |  EU5  |   2    |
//!                 |    3    |  EU4  |   2    |
//! @param[out] pData 8-bit control data read from the switch.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiDpsISourceVsenseMax14662Read(_In_ INT slot, _In_ UINT chip, _Out_ PBYTE pData);

//! @brief Issues a write command to the low current(LC) 2 and LC 3 Ganging Max14662 switch.
//!
//! This function issues a write command to the low current(LC) 2 and LC 3 Ganging Max14662 switch.
//! @note These chips are default in shutdown(SD).  BAR 1 register 0x020C contains the shutdown bits.  See table.  Write 0 to enable.
//! @warning HIL release 7.7 and earlier used an incorrect multiprocess lock name to protect parallel I2C bus access. As the use
//!          case for these switches does not involve parallelism, that should not be an issue, but correct protection is not applied
//!          unless all processes accessing MAX14662 in parallel use a version later than HIL release 7.7.
//! @param[in] slot A physical slot number. Valid values in an HBI tester are 0 to \ref HBI_DPS_SLOTS - 1,
//!                 or \ref HIL_SLOTS - 1 in an HDMT tester using a DPS break-out board (BOB).
//! @param[in] chip A zero-based chip index.  Consult the following table.
//!                 Note that reference designators may change in new fabs.
//!                 |  Chip   | Fab A | SD bit |
//!                 | :-----: | :---: | :----: |
//!                 |    0    | EU25  |   3    |
//!                 |    1    | EU29  |   3    |
//!                 |    2    | EU24  |   4    |
//!                 |    3    | EU28  |   4    |
//!                 |    4    | EU23  |   5    |
//!                 |    5    | EU27  |   5    |
//!                 |    6    | EU22  |   6    |
//!                 |    7    | EU26  |   6    |
//! @param[in] data 8-bit control data written to the switch.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiDpsLcmGangMax14662Write(_In_ INT slot, _In_ UINT chip, _In_ BYTE data);

//! @brief Issues a read command to the low current(LC) 2 and LC 3 Ganging Max14662 switch.
//!
//! This function issues a read command to the low current(LC) 2 and LC 3 Ganging Max14662 switch.
//! @note These chips are default in shutdown(SD).  BAR 1 register 0x020C contains the shutdown bits.  See table.  Write 0 to enable.
//! @warning HIL release 7.7 and earlier used an incorrect multiprocess lock name to protect parallel I2C bus access. As the use
//!          case for these switches does not involve parallelism, that should not be an issue, but correct protection is not applied
//!          unless all processes accessing MAX14662 in parallel use a version later than HIL release 7.7.
//! @param[in] slot A physical slot number. Valid values in an HBI tester are 0 to \ref HBI_DPS_SLOTS - 1,
//!                 or \ref HIL_SLOTS - 1 in an HDMT tester using a DPS break-out board (BOB).
//! @param[in] chip A zero-based chip index.  Consult the following table.
//!                 Note that reference designators may change in new fabs.
//!                 |  Chip   | Fab A | SD bit |
//!                 | :-----: | :---: | :----: |
//!                 |    0    | EU25  |   3    |
//!                 |    1    | EU29  |   3    |
//!                 |    2    | EU24  |   4    |
//!                 |    3    | EU28  |   4    |
//!                 |    4    | EU23  |   5    |
//!                 |    5    | EU27  |   5    |
//!                 |    6    | EU22  |   6    |
//!                 |    7    | EU26  |   6    |
//! @param[out] pData 8-bit control data read from the switch.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiDpsLcmGangMax14662Read(_In_ INT slot, _In_ UINT chip, _Out_ PBYTE pData);

//! @brief Reads the 12V current input current of an HBI DPS card.
//!
//! This function reads the 12V current input current of an HBI DPS card.  The measuring device is an
//! Infineon TLI4970 and is U66 on Fab A.
//! @note HBI DPS FPGA debug version F6_00_0022 is supported, as well as 1.0+ for channel 6.  Other debug versions are \b not supported.
//!       Internally, the implementation to read this current changed after F6_00_0022 and a bit was added in 1.0 to detect the difference.
//! @param[in] slot A physical slot number. Valid values in an HBI tester are 0 to \ref HBI_DPS_SLOTS - 1,
//!                 or \ref HIL_SLOTS - 1 in an HDMT tester using a DPS break-out board (BOB).
//! @param[out] pAmps The input current in Amps.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiDps12vInputCurrent(_In_ INT slot, _Out_ double* pAmps);

//! @brief Writes data to the 24LC512 EEPROM on an HBI DPS card.
//!
//! This function writes data to the 24LC512 EEPROM on an HBI DPS card.
//! @note This device does not exist on HBI DPS Fab A.
//! @param[in] slot A physical slot number. Valid values in an HBI tester are 0 to \ref HBI_DPS_SLOTS - 1,
//!                 or \ref HIL_SLOTS - 1 in an HDMT tester using a DPS break-out board (BOB).
//! @param[in] address The location of the first byte in the EEPROM to which the data in the \c pData buffer will be written.
//! @param[in] pData   A data buffer containing data to write to the EEPROM.
//! @param[in] length  The byte length of the \c pData buffer.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiDpsEepromWrite(_In_ INT slot, _In_ DWORD address, _In_reads_bytes_(length) const void* pData, _In_ DWORD length);

//! @brief Reads data from the 24LC512 EEPROM device on an HBI DPS card.
//!
//! This function reads data from the 24LC512 EEPROM device on an HBI DPS card.
//! @note This device does not exist on HBI DPS Fab A.
//! @param[in] slot A physical slot number. Valid values in an HBI tester are 0 to \ref HBI_DPS_SLOTS - 1,
//!                 or \ref HIL_SLOTS - 1 in an HDMT tester using a DPS break-out board (BOB).
//! @param[in] address The location of the first byte in the EEPROM from which the data will be read.
//! @param[out] pData  A pointer to a data buffer in which the read data will be stored. Note that this buffer must have at least \c length
//!                    allocated in order to complete successfully.
//! @param[in] length  The number of bytes to read from the EEPROM.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiDpsEepromRead(_In_ INT slot, _In_ DWORD address, _Out_writes_bytes_all_(length) LPVOID pData, _In_ DWORD length);

#ifdef __cplusplus
}
#endif
