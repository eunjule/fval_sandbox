// INTEL CONFIDENTIAL
// Copyright 2014-2018 Intel Corporation All Rights Reserved.
//
// The source code contained or described herein and all documents related to
// the source code ("Material") are owned by Intel Corporation or its
// suppliers or licensors. Title to the Material remains with Intel
// Corporation or its suppliers and licensors. The Material contains trade
// secrets and proprietary and confidential information of Intel or its
// suppliers and licensors. The Material is protected by worldwide copyright
// and trade secret laws and treaty provisions. No part of the Material may be
// used, copied, reproduced, modified, published, uploaded, posted,
// transmitted, distributed, or disclosed in any way without Intel's prior
// express written permission.
//
// No license under any patent, copyright, trade secret or other intellectual
// property right is granted to or conferred upon you by disclosure or
// delivery of the Materials, either expressly, by implication, inducement,
// estoppel or otherwise. Any license under such intellectual property rights
// must be express and approved by Intel in writing.

//! @file
//! @brief Low-level APIs to directly control HDMT PCI drivers.
#pragma once
#include "HilDefs.h"
#include "PciDefs.h"

#ifdef __cplusplus
extern "C" {
#endif

//! @brief Locates a PCI device with a specific driver interface.
//!
//! This function locates a PCI device with a specific driver interface.  The search can be optionally
//! constrained by a specific PCI vendor/device ID and physical slot.  A handle to the device interface
//! is returned.  This handle should be closed with pciDeviceClose() when it is no longer required.
//! @param[in] interfaceGuid The driver interface to locate.  There are a number of pre-defined PCI_GUID_xxxx
//!                          constants or a hexadecimal GUID in "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx" format
//!                          can be specified.
//! @param[in] viddid A DWORD in 0xVVVVDDDD format where VVVV is the vendor ID and DDDD is the device ID of
//!                   The PCI device to locate.  Common PCI_VIDDID_xxxx constants are provided.  HIL_ANY_ID(0)
//!                   can also be specified as a wildcard.
//! @param[in] slot   Physical slot 0-11 can be specified, or HIL_ANY_SLOT(-1).
//! @param[in] additional Additional PCI hardware path to match beyond the slot.  Useful if two identical devices can appear beyond a physical tester slot.  Can be NULL.
//! @param[out] pHandle Pointer to returned handle value.
//! @retval HS_SUCCESS The device was located and \c pHandle contains a valid handle to the device.
//! @retval HS_INVALID_PARAMETER The \c interfaceGuid parameter format is invalid.
//! @retval HS_DEVICE_NOT_FOUND The device was not found.
HIL_API HIL_STATUS pciDeviceOpen(_In_z_ LPCSTR interfaceGuid, _In_ DWORD viddid, _In_ INT slot, _In_opt_z_ LPCWSTR additional, _Out_ PHANDLE pHandle);

//! @brief Closes device handles returned by pciDeviceOpen().
//!
//! This function closes device handles returned by pciDeviceOpen().  Handles must be closed
//! when the user is through with them or there will be a resource leak that can interfere
//! with other APIs using the device.  For example, load FPGA firmware on a PCI device
//! requires all PCI device handles to be closed.  Prefer using card-level APIs that use
//! internally-cached handles instead.
//! @param[in] handle The device handle to close.
//! @retval HS_SUCCESS The handle was closed successfully.
//! @retval HS_INVALID_HANDLE The \c handle parameter was not an open, valid handle.
//! @retval HS_UNSUCCESSFUL A non-specific error occurred.
HIL_API HIL_STATUS pciDeviceClose(_In_ HANDLE handle);

//! @brief Enables the selected device in device manager.
//!
//! This function locates a device by its PCI vendor ID, device ID, and physical slot
//! and enables the device.  Windows device manager can be used to view the result.
//! The function is the complement of pciDeviceDisable().  The pair of functions are
//! used to temporarily disable a device's driver when updating firmware.
//! @par Example - C
//! \code{.c}
//! #include <Windows.h>
//! #include "PciApi.h"
//!
//! int main()
//! {
//!     INT slot = 9;
//!     // Enable both HPCC daughterboard FPGAs.
//!     auto status = pciDeviceEnable(PCI_VIDDID_HPCC_AC, slot, nullptr);
//!     if(status != HS_SUCCESS)
//!         return -1;
//!     // Enable just FPGA0 attached to PEX8724 port 2
//!     status = pciDeviceEnable(PCI_VIDDID_HPCC_AC, slot, L"#PCI(0000)#PCI(0200)#PCI(0000)");
//!     if(status != HS_SUCCESS)
//!         return -1;
//!     // Enable just FPGA1 attached to PEX8724 port 9
//!     status = pciDeviceEnable(PCI_VIDDID_HPCC_AC, slot, L"#PCI(0000)#PCI(0900)#PCI(0000)");
//!     if(status != HS_SUCCESS)
//!         return -1;
//!     return 0;
//! }
//! \endcode
//! @par Example - Python
//! \code{.py}
//! import hil
//! hil.pciDeviceEnable(hil.PCI_VIDDID_HPCC_AC,9,None) # Enable both HPCC daughterboard FPGAs in slot 9.
//! hil.pciDeviceEnable(hil.PCI_VIDDID_HPCC_AC,9,'#PCI(0000)#PCI(0200)#PCI(0000)') # Enable just FPGA0 attached to PEX8724 port 2
//! hil.pciDeviceEnable(hil.PCI_VIDDID_HPCC_AC,9,'#PCI(0000)#PCI(0900)#PCI(0000)') # Enable just FPGA1 attached to PEX8724 port 9
//! \endcode
//! @param[in] viddid A DWORD in 0xVVVVDDDD format where VVVV is the vendor ID and DDDD is the device ID of
//!                   The PCI device to locate.  Common PCI_VIDDID_xxxx constants are provided.  HIL_ANY_ID(0)
//!                   can also be specified as a wildcard.
//! @param[in] slot   Physical slot 0-11 can be specified, or HIL_ANY_SLOT(-1).
//! @param[in] additional An additional location path string to uniquely identify a device beyond its slot location.
//!                       This is useful for locating identical devices beyond a slot with identical VID/DIDs.
//! @retval HS_SUCCESS The device was enabled.
//! @retval HS_UNSUCCESSFUL The device was not successfully enabled.
//! @retval HS_DEVICE_IN_USE The device was in use.  All device handles must be closed before this call can succeed.  The device may already be enabled since handles must be closed to disable a device and they cannot be opened on disabled devices.
//! @retval HS_DEVICE_NOT_FOUND The specified device was not found.
HIL_API HIL_STATUS pciDeviceEnable(_In_ DWORD viddid, _In_ INT slot, _In_opt_z_ LPCWSTR additional);

//! @brief Disables the selected device in device manager.
//!
//! This function locates a device by its PCI vendor ID, device ID, and physical slot
//! and disables the device.  Windows device manager can be used to view the result.
//! The function is the complement of pciDeviceEnable().  The pair of functions are
//! used to temporarily disable a device's driver when updating firmware.
//!
//! \note All handles open to the device, such as through pciDeviceOpen(), must be closed for this function
//! to succeed.
//! @par Example - C
//! \code{.c}
//! #include <Windows.h>
//! #include "PciApi.h"
//!
//! int main()
//! {
//!     INT slot = 9;
//!     // Disable both HPCC daughterboard FPGAs.
//!     auto status = pciDeviceDisable(PCI_VIDDID_HPCC_AC, slot, nullptr);
//!     if(status != HS_SUCCESS)
//!         return -1;
//!     // Disable just FPGA0 attached to PEX8724 port 2
//!     status = pciDeviceDisable(PCI_VIDDID_HPCC_AC, slot, L"#PCI(0000)#PCI(0200)#PCI(0000)");
//!     if(status != HS_SUCCESS)
//!         return -1;
//!     // Disable just FPGA1 attached to PEX8724 port 9
//!     status = pciDeviceDisable(PCI_VIDDID_HPCC_AC, slot, L"#PCI(0000)#PCI(0900)#PCI(0000)");
//!     if(status != HS_SUCCESS)
//!         return -1;
//!     return 0;
//! }
//! \endcode
//! @par Example - Python
//! \code{.py}
//! import hil
//! hil.pciDeviceDisable(hil.PCI_VIDDID_HPCC_AC,9,None) # Disable both HPCC daughterboard FPGAs in slot 9.
//! hil.pciDeviceDisable(hil.PCI_VIDDID_HPCC_AC,9,'#PCI(0000)#PCI(0200)#PCI(0000)') # Disable just FPGA0 attached to PEX8724 port 2
//! hil.pciDeviceDisable(hil.PCI_VIDDID_HPCC_AC,9,'#PCI(0000)#PCI(0900)#PCI(0000)') # Disable just FPGA1 attached to PEX8724 port 9
//! \endcode
//! @param[in] viddid A DWORD in 0xVVVVDDDD format where VVVV is the vendor ID and DDDD is the device ID of
//!                   The PCI device to locate.  Common PCI_VIDDID_xxxx constants are provided.  HIL_ANY_ID(0)
//!                   can also be specified as a wildcard.
//! @param[in] slot   Physical slot 0-11 can be specified, or HIL_ANY_SLOT(-1).
//! @param[in] additional An additional location path string to uniquely identify a device beyond its slot location.
//!                       This is useful for locating identical devices beyond a slot with identical VID/DIDs.
//! @retval HS_SUCCESS The device was disabled.
//! @retval HS_UNSUCCESSFUL The device was not successfully disabled.
//! @retval HS_DEVICE_IN_USE The device was in use.  All device handles must be closed before this call can succeed.
//! @retval HS_DEVICE_NOT_FOUND The specified device was not found.
HIL_API HIL_STATUS pciDeviceDisable(_In_ DWORD viddid, _In_ INT slot, _In_opt_z_ LPCWSTR additional);

//! @brief Read device memory through a DMA operation.
//!
//! This function reads device memory though a driver-specific DMA operation.  This operation is currently only supported
//! by the HDMT_RC (Resource card) and HDMT_CC (Gen 1 Channel Card) drivers and requires specific knowledge of the memory
//! layout of the underlying card.  Prefer the card-specific APIs, such as rcDmaRead() and rcDmaWrite(), instead.
//! @param[in] device A handle to an appropriate device opened with pciDeviceOpen().
//! @param[in] offset The memory offset at which to start the DMA operation.  It must be 8-byte aligned.
//! @param[out] pData A buffer a least \c length bytes long to hold the data read from device memory.
//! @param[in] length The length in bytes of the \c pData buffer.
//! @retval HS_SUCCESS The operation completed successfully.
//! @retval HS_UNSUCCESSFUL The operation did not complete successfully.
//! @retval HS_ALIGNMENT_VIOLATION The \c pData address was not 8-byte aligned.
HIL_API HIL_STATUS pciDmaRead(_In_ HANDLE device, _In_ INT64 offset, _Out_writes_bytes_all_(length) LPVOID pData, _In_ DWORD length);

//! @brief Write device memory through a DMA operation.
//!
//! This function writes device memory though a driver-specific DMA operation.  This operation is currently only supported
//! by the HDMT_RC (Resource card) and HDMT_CC (Gen 1 Channel Card) drivers and requires specific knowledge of the memory
//! layout of the underlying card.  Prefer the card-specific APIs, such as rcDmaRead() and rcDmaWrite(), instead.
//! @param[in] device A handle to an appropriate device opened with pciDeviceOpen().
//! @param[in] offset The memory offset at which to start the DMA operation.  It must be 8-byte aligned.
//! @param[out] pData A buffer a least \c length bytes long containing the data to write to device memory.
//! @param[in] length The length in bytes of the \c pData buffer.
//! @retval HS_SUCCESS The operation completed successfully.
//! @retval HS_UNSUCCESSFUL The operation did not complete successfully.
//! @retval HS_ALIGNMENT_VIOLATION The \c pData address was not 8-byte aligned.
HIL_API HIL_STATUS pciDmaWrite(_In_ HANDLE device, _In_ INT64 offset, _In_reads_bytes_(length) const void* pData, _In_ DWORD length);

//! @brief Reads a contiguous range of memory-mapped device registers.
//!
//! This function reads a memory-mapped device register (also known as base address registers or BARs) though a driver-specific operation.  This operation
//! is currently supported on most HDMT_xxxx drivers.  Known exceptions are HDMT_DMA (the HPCC driver) and HDMT_TC (the Thermal Card driver).
//! Prefer the card-specific APIs, such as rcBarRead() and hddpsBarRead(), or use specific APIs that hide requiring specific register
//! mapping knowledge, like rcFpgaVersion(), instead.
//! @param[in] device A handle to an appropriate device opened with pciDeviceOpen().
//! @param[in] bar The PCI base address region to access.  Valid values are 0-2.
//! @param[in] offset The offset into the BAR to access.  This offset must be DWORD aligned.
//! @param[out] pData The address of a DWORD to contain the result.  It cannot be NULL.
//! @retval HS_SUCCESS The operation completed successfully.
//! @retval HS_UNSUCCESSFUL The operation did not complete successfully.  The \c offset or \c length may exceed driver limits.
//! @retval HS_INVALID_PARAMETER The \c bar parameter was invalid.
//! @retval HS_ALIGNMENT_VIOLATION The \c offset parameter was not DWORD-byte aligned.
HIL_API HIL_STATUS pciBarRead(_In_ HANDLE device, _In_ UINT bar, _In_ DWORD offset, _Out_ LPDWORD pData);

//! @brief Writes a contiguous range of memory-mapped device registers.
//!
//! This function writes a memory-mapped device register (also known as base address registers or BARs) though a driver-specific operation.  This operation
//! is currently supported on most HDMT_xxxx drivers.  Known exceptions are HDMT_DMA (the HPCC driver) and HDMT_TC (the Thermal Card driver).
//! Prefer the card-specific APIs, such as rcBarWrite() and hddpsBarWrite(), instead.
//! @param[in] device A handle to an appropriate device opened with pciDeviceOpen().
//! @param[in] bar The PCI base address region to access.  Valid values are 0-2.
//! @param[in] offset The offset into the BAR to access.
//! @param[in] data The DWORD value to write.
//! @retval HS_SUCCESS The operation completed successfully.
//! @retval HS_UNSUCCESSFUL The operation did not complete successfully.  The \c offset or \c length may exceed driver limits.
//! @retval HS_INVALID_PARAMETER The \c bar parameter was invalid.
//! @retval HS_ALIGNMENT_VIOLATION The \c offset parameter was not DWORD-byte aligned.
HIL_API HIL_STATUS pciBarWrite(_In_ HANDLE device, _In_ UINT bar, _In_ DWORD offset, _In_ DWORD data);

//! @brief Enables or disables user mode hardware access.
//!
//! This function globally enables or disables user mode hardware access.  HIL defaults to enabling the faster user mode hardware access,
//! but if problems are suspected this function can be used to switch to slower kernel mode accesses.
//! @param[in] enable TRUE enables user mode access, FALSE disables.
HIL_API void pciUserBarEnable(_In_ BOOL enable);

//! @brief Queries whether user mode PCI register access is enabled.
//!
//! This function queries if HIL accesses device PCI registers by memory mapping their base address register (BAR) regions to user mode memory
//! or through kernel device drivers.
//! User mode allows faster register access but can also create timing problems.
//! @retval TRUE Simulation is enabled.
//! @retval FALSE Simulation is not enabled.
//! @see pciUserBarEnable()
HIL_API BOOL pciUserBarIsEnabled(void);

//! @brief Overrides default PCI simulation APIs to customize PCI responses.
//!
//! This function overrides default PCI simulation APIs to customize PCI responses.
//! @param[in] funcs Address of a \ref PCISIM structure containing overrides for PCI simulation functions.  May be NULL to use the default simulation behavior.
//! @returns \ref PCISIM The previous functions used for simulation behavior.
//! @see hilSimEnable()
//! @see hilSimIsEnabled()
HIL_API PCISIM pciSimOverride(_In_opt_ const PCISIM* funcs);

#ifdef __cplusplus
}
#endif
