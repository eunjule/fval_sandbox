// INTEL CONFIDENTIAL
// Copyright 2014-2018 Intel Corporation All Rights Reserved.
//
// The source code contained or described herein and all documents related to
// the source code ("Material") are owned by Intel Corporation or its
// suppliers or licensors. Title to the Material remains with Intel
// Corporation or its suppliers and licensors. The Material contains trade
// secrets and proprietary and confidential information of Intel or its
// suppliers and licensors. The Material is protected by worldwide copyright
// and trade secret laws and treaty provisions. No part of the Material may be
// used, copied, reproduced, modified, published, uploaded, posted,
// transmitted, distributed, or disclosed in any way without Intel's prior
// express written permission.
//
// No license under any patent, copyright, trade secret or other intellectual
// property right is granted to or conferred upon you by disclosure or
// delivery of the Materials, either expressly, by implication, inducement,
// estoppel or otherwise. Any license under such intellectual property rights
// must be express and approved by Intel in writing.

//! @file
//! @brief Non-function definitions such as constants and structures used by the FTDI USB APIs.

#pragma once

typedef struct FtdiDevice* HFTDI; //!< HIL FTDI device handle.
typedef HFTDI* PHFTDI;            //!< Pointer to HIL FTDI device handle.

//! @brief FTDI device information returned by ftdiDeviceInfoList().
typedef struct FTDI_DEVICE_LIST_INFO
{
    ULONG Flags; //!< Device flags. Bit 0 - device open, Bit 1 - high speed device.
    ULONG Type;  //!< FTDI device type.  See the FTDI_DEVICE_XXXX values.
    ULONG ID;    //!< The vendor and product ID of the device as 0xVVVVPPPP.
    DWORD LocId; //!< A unique value indicating the hardware location of the device.
    char  SerialNumber[16];  //!< The serial number of the device.
    char  Description[64];   //!< The device description.
    PVOID ftHandle;          //!< The \e internal FTDI device handle if the device is open, else zero. \b NOT the same as \ref HFTDI.
} FTDI_DEVICE_LIST_INFO;

#define FTDI_I2C_STANDARD_MODE 0x01 //!< \c i2cMode value to use Standard Mode (100kbps)
#define FTDI_I2C_FAST_MODE     0x02 //!< \c i2cMode value to use Fast Mode (400kbps)

#define FTDI_SPI_WR_POS        0x00 //!< \c edgeType value for ftdiMpsseSpiWrite() to write on the positive clock edge.  **Note: Clock must start high in this mode.**
#define FTDI_SPI_WR_NEG        0x01 //!< \c edgeType value for ftdiMpsseSpiWrite() to write on the negative clock edge.  **Note: Clock must start low in this mode.**
#define FTDI_SPI_RD_POS        0x00 //!< \c edgeType value for ftdiMpsseSpiRead() to read on the positive clock edge.  **Note: Clock must start low in this mode.**
#define FTDI_SPI_RD_NEG        0x04 //!< \c edgeType value for ftdiMpsseSpiRead() to read on the negative clock edge.  **Note: Clock must start high in this mode.**
#define FTDI_SPI_WR_POS_RD_NEG (FTDI_SPI_WR_POS | FTDI_SPI_RD_NEG) //!< \c edgeType value for ftdiMpsseSpiParWriteRead() to write on positive clock edge and read on negative clock edge.
#define FTDI_SPI_WR_NEG_RD_POS (FTDI_SPI_WR_NEG | FTDI_SPI_RD_POS) //!< \c edgeType value for ftdiMpsseSpiParWriteRead() to write on negative clock edge and read on positive clock edge.

#define FTDI_GPIO_NONE  0x00 //!< Select no GPIO bits.
#define FTDI_GPIO_0     0x01 //!< Select GPIO 0.
#define FTDI_GPIO_1     0x02 //!< Select GPIO 1.
#define FTDI_GPIO_2     0x04 //!< Select GPIO 2.
#define FTDI_GPIO_3     0x08 //!< Select GPIO 3.
#define FTDI_GPIO_4     0x10 //!< Select GPIO 4.
#define FTDI_GPIO_5     0x20 //!< Select GPIO 5.
#define FTDI_GPIO_6     0x40 //!< Select GPIO 6.
#define FTDI_GPIO_7     0x80 //!< Select GPIO 7.
#define FTDI_GPIO_ALL   0xFF //!< Select GPIO 0-7.

// Modes used by ftdiModeSet()
#define FTDI_MODE_RESET         0x00 //!< Mode flag used by ftdiModeSet().
#define FTDI_MODE_ASYNC_BITBANG 0x01 //!< Mode flag used by ftdiModeSet().
#define FTDI_MODE_MPSSE         0x02 //!< Mode flag used by ftdiModeSet().
#define FTDI_MODE_SYNC_BITBANG  0x04 //!< Mode flag used by ftdiModeSet().
#define FTDI_MODE_MCU_HOST      0x08 //!< Mode flag used by ftdiModeSet().
#define FTDI_MODE_FAST_SERIAL   0x10 //!< Mode flag used by ftdiModeSet().
#define FTDI_MODE_CBUS_BITBANG  0x20 //!< Mode flag used by ftdiModeSet().
#define FTDI_MODE_SYNC_FIFO     0x40 //!< Mode flag used by ftdiModeSet().

typedef ULONG FTDI_DEVICE; //!< FTDI device type number.  See the FTDI_DEVICE_XXX constants.

#define FTDI_DEVICE_BM       0  //!< FTDI BM device type.
#define FTDI_DEVICE_AM       1  //!< FTDI AM device type.
#define FTDI_DEVICE_100AX    2  //!< FTDI 100AX device type.
#define FTDI_DEVICE_UNKNOWN  3  //!< FTDI unknown device type.
#define FTDI_DEVICE_2232C    4  //!< FTDI 2232C device type.
#define FTDI_DEVICE_232R     5  //!< FTDI 232R device type.
#define FTDI_DEVICE_2232H    6  //!< FTDI 2232H device type.
#define FTDI_DEVICE_4232H    7  //!< FTDI 4232H device type.
#define FTDI_DEVICE_232H     8  //!< FTDI 232H device type.
#define FTDI_DEVICE_X_SERIES 9  //!< FTDI X series device type, such as FT240X.
