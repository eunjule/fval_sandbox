// INTEL CONFIDENTIAL
// Copyright 2014-2020 Intel Corporation All Rights Reserved.
//
// The source code contained or described herein and all documents related to
// the source code ("Material") are owned by Intel Corporation or its
// suppliers or licensors. Title to the Material remains with Intel
// Corporation or its suppliers and licensors. The Material contains trade
// secrets and proprietary and confidential information of Intel or its
// suppliers and licensors. The Material is protected by worldwide copyright
// and trade secret laws and treaty provisions. No part of the Material may be
// used, copied, reproduced, modified, published, uploaded, posted,
// transmitted, distributed, or disclosed in any way without Intel's prior
// express written permission.
//
// No license under any patent, copyright, trade secret or other intellectual
// property right is granted to or conferred upon you by disclosure or
// delivery of the Materials, either expressly, by implication, inducement,
// estoppel or otherwise. Any license under such intellectual property rights
// must be express and approved by Intel in writing.

//! @file
//! @brief APIs to query and control global library items.
//!
//! APIs in this header query and control global library features such as library version and simulation control.

//! @mainpage Hardware Implementation Layer (HIL)
//!
//! \par General HIL APIs:
//! HilApi.h\n
//!
//! \par HDMT Tester APIs:
//! BackplaneApi.h\n
//! CalHddpsApi.h\n
//! CalHpccApi.h\n
//! CalHpccSnicApi.h\n
//! CalHvdpsApi.h\n
//! DpsApi.h\n
//! FrontPanelApi.h\n
//! HddpsApi.h\n
//! HpccAcApi.h\n
//! HpccDcApi.h\n
//! HvdpsApi.h\n
//! HvilApi.h\n
//! PowersupplyApi.h\n
//! RcApi.h\n
//! Rc3Api.h\n
//! Rc3LbApi.h\n
//! SiteControllerApi.h\n
//! TcApi.h\n
//! TdauBankApi.h\n
//! TdauDiagApi.h\n
//! TiuCalBaseApi.h\n
//! \par HBI Tester APIs:
//! HbiBpApi.h\n
//! HbiDpsApi.h\n
//! HbiMbApi.h\n
//! HbiMtbApi.h\n
//! HbiPowerSupplyApi.h\n
//! HbiPsdbApi.h\n
//! HbiTiuApi.h\n
//!
//! \par Driver-level APIs:
//!
//! These APIs are for algorithm development, not general use.  Prefer Card APIs unless a new feature isn't supported by HIL yet.\n\n
//! CypressApi.h\n
//! FtdiApi.h\n
//! PciApi.h\n
//! \see
//!  \ref notes\n
//!  \ref pyapi\n
//!  \ref renames\n

//! @page pyapi Python API Conventions
//! The HIL APIs wrapped by Python follow the following conventions to make their use in Python more natural to use in the language:
//! \li Passing \ref HIL_STATUS converted to None.
//! \li Failing \ref HIL_STATUS converted to Python RuntimeError exception.
//! \li Output parameters suppressed and appended to return value tuple.
//! \li Input/output parameters pass input value and return output value appended to return value tuple.
//! \li The return value of a \ref HIL_STATUS or \c void function and one output value will be the output value and not a tuple of (None, param).
//! \li An input void* buffer plus size parameter pair are replaced with a single Python byte string parameter.
//! \li An output void* buffer plus size parameter pair are replaced with just the buffer size.
//! \li A returned buffer is appended to the return value tuple as a Python byte string.
//! \li Functions with small known upper limits on an output buffer do not require the size to be passed.
//! \li Input buffers of specific C types such as int or DWORD can be passed as Python lists of the values.
//! \li Output buffers of specific C types can be passed a count and output value list is appended to the return tuple.
//! \li Functions with C callbacks and \c void* contexts are replaced with functions that take Python callbacks and Python objects for context.
//!
//! @par Examples
//!
//! <PRE>HIL_STATUS hilVersionString(LPSTR pVersion, DWORD length)</PRE>
//!
//! @code{.py}
//! >>> hil.hilVersionString() # Small known buffer
//! '0.5'
//! @endcode
//!
//! <PRE>HIL_STATUS rcFpgaVersion(LPDWORD pVersion)</PRE>
//!
//! @code{.py}
//! >>> hex(hil.rcFpgaVersion()) # No parameter required.
//! 0x50001
//! @endcode
//!
//! <pre>HIL_STATUS rcDmaRead(INT64 address, LPVOID pData, DWORD length)</pre>
//!
//! @code{.py}
//! >>> hil.rcDmaRead(0,16) # Output buffer not required, just size of read.
//! b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00'
//! @endcode
//!
//! <pre>HIL_STATUS rcDmaWrite(INT64 address, const void* pData, DWORD length)</pre>
//!
//! @code{.py}
//! >>> hil.rcDmaWrite(0,b'ABCDEFGHIJKLMNOP') # Input is Python byte string.  No return value on success.
//! >>>
//! @endcode
//!
//! <pre>HIL_STATUS rcDmaWrite(INT64 address, const void* pData, DWORD length)</pre>
//!
//! @code{.py}
//! >>> hil.rcDmaWrite(0,[1,2,3,4]) # Input is list of DWORDs.  Example failure.
//! Traceback (most recent call last):
//!   File "<stdin>", line 1, in <module>
//! TypeError: expected bytes
//! @endcode
//!
//! <pre>HIL_API HIL_STATUS rcFpgaBaseLoad(LPCSTR filename, HIL_BASE_LOAD_CALLBACK callback, void* context)</pre>
//!
//! @code{.py}
//! >>> # Pass an anonymous function to print the percent done as the function proceeds.
//! >>> hil.rcFpgaBaseLoad(filename,lambda val,ctx: print('\r{:.1f}%'.format(val),end=''))
//!  19.4%   # This will continuously update due to carriage return(\r) but no linefeed(end='')
//! @endcode

//! @page renames Complete List of Name Changes in HIL 1.0
//! * HIL
//!   * hilGetVersion() renamed to hilVersion().
//!   * hilGetVersionString() renamed to hilVersionString().
//!   * hilGetDescription() renamed to hilStatusDescription().
//!   * Constant VERSION_MAJOR renamed to #HIL_VERSION_MAJOR.
//!   * Constant VERSION_MINOR renamed to #HIL_VERSION_MINOR.
//!   * Constant VERSION_SUBMINOR renamed to #HIL_VERSION_SUBMINOR.
//!   * Constant VERSION_REVISION renamed to #HIL_VERSION_REVISION.
//! * HDDPS
//!   * hddpsGetCpldVersion() renamed to hddpsCpldVersion().
//!   * hddpsGetCpldVersionString() renamed to hddpsCpldVersionString().
//!   * hddpsGetFpgaVersion() renamed to hddpsFpgaVersion().
//! * HPCC
//!   * hpccAcGetFpgaVersion() renamed to hpccAcFpgaVersion().
//!   * hpccAcGetCpldVersion() renamed to hpccAcCpldVersion().
//!   * hpccAcGetCpldVersionString() renamed to hpccAcCpldVersionString().
//!   * hpccDcGetFpgaVersion() renamed to hpccDcFpgaVersion().
//!   * hpccDcGetCpldVersion() renamed to hpccDcCpldVersion().
//!   * hpccDcGetCpldVersionString() renamed to hpccDcCpldVersionString().
//! * PXI
//!   * pxiGetCpldVersion() renamed to pxiCpldVersion().
//!   * pxiGetCpldVersionString() renamed to pxiCpldVersionString().
//!   * pxiGetFpgaVersion() renamed to pxiFpgaVersion().
//!   * pxiGetFpgaVersionString() renamed to pxiFpgaVersionString().
//!   * pxiAdt7411RegRead() renamed to pxiAdt7411Read().
//!   * pxiAdt7411RegWrite() renamed to pxiAdt7411Write().
//!   * pxiIsRiserPresent() renamed to pxiRiserDetect().
//!   * pxiTmonAlarmLimitsSet() renamed to pxiTmonAlarmLimitsWrite().
//!   * pxiTmonAlarmLimitsGet() renamed to pxiTmonAlarmLimitsRead().
//!   * pxiVmonAlarmLimitsSet() renamed to pxiVmonAlarmLimitsWrite().
//!   * pxiVmonAlarmLimitsGet() renamed to pxiVmonAlarmLimitsRead().
//! * Resource Card
//!   * rcGetFpgaVersion() renamed to rcFpgaVersion().
//!   * rcGetCpldVersion() renamed to rcCpldVersion().
//!   * rcGetCpldVersionString() renamed to rcCpldVersionString().
//!   * rcGetPeerToPeerMemoryBase() renamed to rcPeerToPeerMemoryBase().
//!   * rcQueueInstruction() renamed to rcQueueAppend().
//! * TIU Calibration Base
//!   * calBaseConnect() renamed to tiuCalBaseConnect().
//!   * calBaseDisconnect() renamed to tiuCalBaseDisconnect().
//!   * calBaseInit() renamed to tiuCalBaseInit().
//!   * calBaseGetCpldVersion() renamed to tiuCalBaseCpldVersion().
//!   * calBaseGetCpldVersionString() renamed to tiuCalBaseCpldVersionString().
//!   * calBaseJtagExecute() renamed to tiuCalBaseJtagExecute().
//!   * calBaseRelayWrite() renamed to tiuCalBaseGpioWrite().
//!   * calBaseRelayRead() renamed to tiuCalBaseGpioRead().
//!   * calBasePca9505Write() renamed to tiuCalBasePca9505Write().
//!   * calBasePca9505Read() renamed to tiuCalBasePca9505Read().
//!   * CALBASE_RELAY enumeration renamed to #TIU_CAL_BASE_GPIO.
//!     * TCBG_ prefix added to enumeration values.
//! * HDDPS Calibration Card
//!   * calHddpsRelaysReset() renamed to calHddpsGpiosReset().
//!   * calHddpsRelayWrite() renamed to calHddpsGpioWrite().
//!   * calHddpsRelayRead() renamed to calHddpsGpioRead().
//!   * CAL_HDDPS_RELAY enumeration renamed to #CAL_HDDPS_GPIO.
//!     * CHR_ prefix on enumeration values changed to CHG_.
//!   * calHpccGetCpldVersion() renamed to calHpccCpldVersion().
//!   * calHpccGetCpldVersionString() renamed to calHpccCpldVersionString().
//! * PXI Calibration Card
//!   * calPxiM95m02CommandExecute() renamed to calPxiM95m02Execute().
//!   * calPxiS70fl01gsCommandExecute() renamed to calPxiS70fl01gsExecute().
//! * PCI
//!   * pciOpenDevice() renamed to pciDeviceOpen().
//!   * pciCloseDevice() renamed to pciDeviceClose().
//!   * pciEnableDevice() renamed to pciDeviceEnable().
//!   * pciDisableDevice() renamed to pciDeviceDisable().
//! * Cypress USB
//!   * cypOpenDevice() renamed to cypDeviceOpen().
//!   * cypCloseDevice() renamed to cypDeviceClose().
//!   * cypGetVidPid() renamed to cypVidPid().
//!   * cypPortSetDirection() renamed to cypPortDirectionWrite().
//!   * cypPortGetDirection() renamed to cypPortDirectionRead().
//!   * cypI2cGetStatus() renamed to cypI2cStatus().
//!   * cypProgramViaGpif() renamed to cypGpifFpgaLoad().
//!   * cypProgramViaDataIo() renamed to cypDataIoFpgaLoad().
//!   * cypProgramAndVerifySpi() renamed to cypSpiFpgaBaseLoad().
//!   * CypressPort enumeration renamed to #CYP_PORT.
//!   * I2cStatus enumeration renamed to #CYP_I2C_STATUS.
//! * FTDI USB
//!   * ftdiCreateDeviceInfoList() renamed to ftdiDeviceInfoListCreate().
//!   * ftdiGetDeviceInfoList() renamed to ftdiDeviceInfoList().
//!   * ftdiOpenByLocation() renamed to ftdiOpenEx().
//!   * ftdiSetBaudRate() renamed to ftdiBaudRateSet().
//!   * ftdiSetMode() renamed to ftdiModeSet().
//!   * ftdiGetQueueStatus() renamed to ftdiQueueStatus().
//!
//! @page notes Release Notes
//! \par Version 7.8
//! * Added HBI system power monitor streaming support and alarm APIs.
//!  * Needs HBI_RCTC driver dated 11/11/2020 or greater for correct operation.
//!  * Added APIs:
//!   * hbiMbStreamingBufferEx()
//!   * hbiMbSpmAlarmEnable()
//!   * hbiMbAlarmWait()
//!   * hbiMbAlarmWaitCancel()
//!   * hbiPsPmbusPollingEnable()
//!   * hbiPsPmbusSensorRead()
//!   * hbiMbSpmAlarmLimitsSet()
//!   * hbiMbSpmAlarmLimitsGet()
//!  * The following APIs are unavailable and fail with HS_SPM_POLLING_ENABLED if polling is enabled:
//!   * hbiPsPmbusWrite()
//!   * hbiPsPmbusRead()
//!   * hbiPsFruEepromRead()
//!   * hbiPsBltBoardRead()
//!  * hbiPsConnect() returns cached bulk power supply presence when SPM is enabled.  Disable SPM for an instantaneous check.
//! * HIL is now compiled with VS2019.  **Ensure VS2019 runtime is installed!**
//! * Altera FPGA loading now reports differentiated timeouts.  **If legacy code specifically checks for HS_TIMEOUT instead of non-HS_SUCCESS on these APIs it will break.**
//!  * HS_FPGA_CONFIG_NOT_READY/HS_FPGA_CONFIG_NOT_DONE instead of HS_TIMEOUT.
//!  * Affected APIs:
//!   * hbiDpsFpgaLoad()
//!   * hbiMbFpgaLoad()
//!   * hbiPsdbFpgaLoad()
//!   * rc3FpgaLoad()
//!   * hbiDpsFpgaBufferLoad()
//!   * hbiMbFpgaBufferLoad()
//!   * hbiPsdbFpgaBufferLoad()
//!   * rc3FpgaBufferLoad()
//! * FIX: Corrected inter-process lock name in MAX14662-related APIs.
//!  * hbiDpsISourceVsenseMax14662Read()
//!  * hbiDpsISourceVsenseMax14662Write()
//!  * hbiDpsLcmGangMax14662Read()
//!  * hbiDpsLcmGangMax14662Write()
//!  * hbiDpsVtVmLbMax14662Read()
//!  * hbiDpsVtVmLbMax14662Write()
//!
//! \par Version 7.7
//! * Updates for RCTC3 Fab B:
//!  * Added rc3EepromWrite() and rc3EepromRead() APIs for reading I2C EEPROM.
//!  * rc3BltEepromRead() returns HS_UNSUPPORTED due to removal of FT240X.
//!  * Per circuit change, adjusted scaling factor for VmonRead() channel 7.
//!  * RCTC_VCCP_PMBUS_DEBUG_EN signal remapped from FT2232 ADBUS4 to ACBUS7.
//! * FIX: rc3StreamingBuffer() wouldn't work with older HDMT_RCTC3 drivers.
//! * Added hbiMbDdr4TmonRead() API to read DDR memory temperature sensors.
//!
//! \par Version 7.6
//! * Added rc3StreamingBufferEx() API.
//!  * Supports retrieving bulk data streaming buffer as index 0.
//!  * Supports retrieving serial capture stream buffer as index 1.
//!  * Note that existing rc3StreamingBuffer() only returns bulk data buffer.
//! * Added compatibility support for version HBI x.x.3 FPGAs to be loaded on Fab D/E RCTC FPGAs.
//!  * Note that x.x.3 RC FPGAs support DDR.
//! * FIX: Any FTDI cache update was re-enabling failsafe if it was disabled.
//!
//! \par Version 7.5.3
//! * Fixed a bug introduced in 7.5.2 when adding event tracing.
//!  * FTDI enumeration could return #HS_FTDI_ENUMERATION instead of #HS_FTDI_ENUMERATION_LOCK_FAILED
//!    when an FTDI device was held open, causing incorrect cleanup of the error.
//!
//! \par Version 7.5.2
//! * Fixed the FTDI enumeration algorithm.
//! * Added hilFailsafePingTimeElapsed() API.
//! * Extra event tracing for Windows (ETW) added around FTDI semphore and mutex.
//!
//! \par Version 7.5.1
//! * Event Tracing for Windows (ETW) added to HIL for FTDI mutex/semaphore/open tracing.
//!
//! \par Version 7.5
//! * New feature: Python help strings implemented for all HIL APIs.
//!  * Python helper function <code>hil.cmds(<i>string</i>)</code> will list commands containing case-insensitive <i>string</i> and display the new help strings showing parameters and return value of the API.
//!  * <code>help(hil.<i>function</i>)</code> will also list the help string of a particular API.
//! * Updated ADT7411 initialization sequence to fix intermittent bad first readings after power-on.
//!  * Affects the following boards:
//!   * HDMT: Backplane, Front Panel, HVIL (HVDPS daughterboard), Resource Card (rev 2), and TDAU Bank.
//!   * HBI: Mainboard, PSDB, MTB, DPS.
//!  * Added software reset.
//!  * Wrote config registers 3,2,1 instead of config 1,2,3 per spec recommendation.
//!  * Added round-robin update delay of 20ms after config.
//!
//! @page notes Release Notes
//! \par Version 7.4.1
//! * FIX: missing source slot information in return value of rc3TriggerWait() API.
//!
//! \par Version 7.4
//! * Added hbiDpsTmonRead() support for 14 additional LC/HC VReg temperatures.
//!  * Uses PMBus-polled FPGA registers if supported by capability bit in FPGA.
//!  * Directly uses PMBus to read the temperatures otherwise.
//! * Added rcFpgaBaseVerify() API.
//! * Added alternate relative import to hil_utils.py to support hil/hil_utils imported in a Python package format.
//!
//! \par Version 7.3
//! * HBI Mainboard/PSDB/DPS voltage monitor improvements.
//!  * New APIs which read Vdd and AIN1-AIN8 ADCs for a single ADT7411 chip.
//!   * hbiMbVmonReadAll()
//!   * hbiPsdbVmonReadAll()
//!   * hbiDpsVmonReadAll()
//!  * Optimized APIs slightly for a single ADC read:
//!   * hbiMbVmonRead()
//!   * hbiPsdbVmonRead()
//!   * hbiDpsVmonRead()
//! * HIL updates to support HPCC AC Calibration use of CB2/HPCC Cal FT2232 devices to HIL.
//!  * New APIs:
//!   * tiuCalBaseWindowCompOutputEnable()
//!   * tiuCalBaseWindowCompOutputSelect()
//!   * tiuCalBaseWindowCompOutputRead()
//!   * calHpccWindowCompOutputEnable()
//!   * calHpccWindowCompOutputSelect()
//!   * calHpccWindowCompOutputRead()
//!  * Changed APIs:
//!   * tiuCalBaseDelayLineSet()
//!   * tiuCalBaseWindowCompRefSet()
//!  * SPI bus frequency set to match those used in HAL implementation.
//!  * Added caching of CB2 devices to keep handles open during HPCC AC Calibration to avoid performance hit.
//!   * **NOTE: Use of all TIU calibration base APIs listed above must call tiuCalBaseDisconnect() when calibration is complete.**
//!    * HPPC calibration APIs do not cache handles and do not have this requirement.
//! * Added common HIL functions to manipulate failsafe.  These APIs detect the hardware and call the appropriate hardware-specific internal functions.
//!  * New APIs:
//!   * hilFailsafeDisable()
//!   * hilFailsafeTimerSet()
//!   * hilFailsafePing()
//!  * Older rc/rc3/hbiMb hardware-specific APIs deprecated.
//!
//! \par Version 7.2
//! * Changes required to port HPCC AC Calibration use of CB2/HPCC Cal FT4232 devices to HIL.
//!  * Added tiuCalBaseLmk04808PllIsLocked() API.
//!  * SPI bus initialization and CB2 Lmk04808 device behavior aligned with RC implementation.
//!  * SPI bus frequencies set to match those used in HAL implementation.
//!  * Fix byte order of data in internal tiuCalBaseDelayLineSet() and calHpccDelayLineSet() functions.
//!  * TIU APIs that required FTDI initalization now self-initialize.
//!  * Added tiuCalBaseClocksInit() utility hil_utils.py to init TIU clocks as TOS requires.
//!  * Added automatic disconnect of TIU when TIU power is removed via bpTiuAuxPowerEnable().
//! * Updated BLT audit scripts included in HIL release:
//!  * Renamed GetBlt.py to HdmtBlts.py and added support for RCTC3.
//!  * Added HbiBlts.py.
//! * Added one-second delay to rcFpgaLoad() and rcFpgaBufferLoad().
//!  * Allows slow sensor registers to update once before other APIs access them.
//! * Updated FPGA compatibility check for HBI mainboard Fab E support.
//! * Populate blank HBI bulk power supply fields.
//!  * PartNumberAsBuilt and PartNumberCurrent contain FRU Model Type.
//!  * ManufactureDate is now "NA".
//!
//! \par Version 7.1.2
//! * Optimization to speed up JTAG transactions involving Altera CPLDs.
//!  * Altera MAX II CPLDs reduced from 4 min to 19 sec programming time.
//! * Remove FTDI StopInTask/RestartInTask from Ftdi::ModeSet.
//!  * RestartInTask() sometimes hangs in the FTDI driver for unknown reasons.
//!  * Not needed in current code (past problems with excess open FTDI handles solved).
//!
//! \par Version 7.1.1
//! * Fix RCTC2 regression on TOS from RCTC3 implementation.
//!
//! \par Version 7.1
//! * Added RCTC3 APIs:
//!  * rc3Init()
//!  * rc3Connect()
//!  * rc3Disconnect()
//!  * rc3VidPidsSet()
//!  * rc3FailsafeDisable()
//!  * rc3FailsafeTimerSet()
//!  * rc3FailsafePing()
//!  * rc3Uptime()
//!  * rc3FpgaVersion()
//!  * rc3FpgaVersionString()
//!  * rc3CpldVersion()
//!  * rc3CpldVersionString()
//!  * rc3CpldProgram()
//!  * rc3CpldBufferProgram()
//!  * rc3CpldVerify()
//!  * rc3CpldBufferVerify()
//!  * rc3DeviceDisable()
//!  * rc3DeviceEnable()
//!  * rc3FpgaLoad()
//!  * rc3FpgaBaseLoad()
//!  * rc3FpgaBaseVerify()
//!  * rc3FpgaBufferLoad()
//!  * rc3FpgaBaseBufferLoad()
//!  * rc3BarRead()
//!  * rc3BarWrite()
//!  * rc3DmaRead()
//!  * rc3DmaWrite()
//!  * rc3HpsDmaRead()
//!  * rc3HpsDmaWrite()
//!  * rc3BltBoardRead()
//!  * rc3BltBoardWrite()
//!  * rc3BltInstrumentWrite()
//!  * rc3BltInstrumentRead()
//!  * rc3BltEepromRead()
//!  * rc3SysIdRead()
//!  * rc3SysIdWrite()
//!  * rc3MesIdRead()
//!  * rc3MesIdWrite()
//!  * rc3TiuAlarmEnable()
//!  * rc3AlarmWait()
//!  * rc3AlarmWaitCancel()
//!  * rc3TriggerEnable()
//!  * rc3TriggerDisable()
//!  * rc3TriggerWait()
//!  * rc3TriggerWaitCancel()
//!  * rc3PeerToPeerMemoryBase()
//!  * rc3HpsMailboxBase()
//!  * rc3StreamingBuffer()
//!  * rc3Lmk04832Write()
//!  * rc3Lmk04832Read()
//!  * rc3Ad5064Write()
//!  * rc3Ltm4680PmbusRead()
//!  * rc3Ltm4680PmbusWrite()
//!  * rc3VmonRead()
//!  * rc3Ltc2358VmonRead()
//!  * rc3TmonRead()
//!  * rc3FanRead()
//!  * rc3FanModeSet()
//!  * rc3Max6650Write()
//!  * rc3Max6650Read()
//! * For backward-compatibility, the following RCTC2 I2C APIs now work on RCTC3:
//!  * rcRootI2cLock()
//!  * rcRootI2cUnlock()
//!  * rcRootI2cMuxSelect()
//!  * rcRootI2cWrite()
//!  * rcRootI2cRead()
//!  * rcRootI2cRegisterRead()
//!  * rcRootI2cInit()
//!  * rcRootI2cCacheInvalidate()
//! * Added the following RCTC3 Loopback Accessory Board APIs:
//!  * rc3LbConnect()
//!  * rc3LbVidPidSet()
//!  * rc3LbBltBoardRead()
//!  * rc3LbBltBoardWrite()
//!  * rc3LbBltEepromRead()
//! * hbiDpsVmonRead() now supports Fab D and onwards. Fixes HIL failures discovered by Fab E.
//! * Added PSDB EEPROM APIs:
//!  * hbiPsdbEepromWrite()
//!  * hbiPsdbEepromRead()
//! * Removed unused DPS Support Bar BLT APIs:
//!  * hbiDpsBltSupportBarRead()
//!  * hbiDpsBltSupportBarWrite()
//! * Help file correction for \c psSlot parameter of psFirmwareVersions().
//!
//! \par Version 7.0
//! * Implemented FLASH metadata programming.
//!  * Added FpgaMeta project to generate metadata from FPGA build parameters.
//!  * Increased HIL_MAX_VERSION_STRING_SIZE from 20 to 32.
//!  * Added many new meta-related HIL_STATUS error messages.
//!  * Added FLASH_INFO structure to report metadata.
//!  * New APIs:
//!   * hilFpgaMetaBypass() suppresses metadata file requirement when loading FLASH or FPGA.
//!   * hilFpgaMetaIsBypassed() queries metadata file suppression status.
//!   * hbiMbFpgaBaseInfo() reads FPGA base FLASH metadata into a FLASH_INFO structure.
//!   * hbiMbFpgaCompatibility() reads the hardware compatibility register.
//!   * hbiMbFpgaSourceHash() returns the source control system hash of the source used to build the FPGA.
//!   * hbiMbFpgaChipId() returns the unique FPGA chip ID for the requested FPGA.
//!   * hbiDpsFpgaBaseInfo() reads FPGA base FLASH metadata into a FLASH_INFO structure.
//!   * hbiDpsFpgaCompatibility() reads the hardware compatibility register.
//!    * Returns 0 on currently released FPGAs.  Planned to be implemented it FPGA version 3.1.
//!   * hbiDpsFpgaSourceHash() returns the source control system hash of the source used to build the FPGA.
//!    * Returns "000000000000+" on currently released FPGAs.  Planned to be implemented it FPGA version 3.1.
//!   * hbiDpsFpgaChipId() returns the unique FPGA chip ID.
//!    * Returns 0 on currently released FPGAs.  Planned to be implemented it FPGA version 3.1.
//!   * hbiPsdbFpgaBaseInfo() reads FPGA base FLASH metadata into a FLASH_INFO structure.
//!   * hbiPsdbFpgaCompatibility() reads the hardware compatibility register.
//!   * hbiPsdbFpgaSourceHash() returns the source control system hash of the source used to build the FPGA.
//!   * hbiPsdbFpgaChipId() returns the unique FPGA chip ID for the requested FPGA.
//!  * Behavior changes in APIs:
//!   * hbiMbFpgaVersionString() appends hardware compatiblity register value.
//!   * hbiDpsFpgaVersionString() appends hardware compatiblity register value.
//!   * hbiPsdbFpgaVersionString() appends hardware compatiblity register value.
//!   * hbiMbFpgaLoad() and hbiMbFpgaBaseLoad() require metadata files.
//!   * hbiDpsFpgaLoad() and hbiDpsFpgaBaseLoad() require metadata files.
//!   * hbiPsdbFpgaLoad() and hbiPsdbFpgaBaseLoad() require metadata files.
//!  * \b BREAKING \b CHANGES:
//!   * hbiMbFpgaBaseVerify() no longer requires the imageFilename parameter and instead used metadata.
//!   * hbiDpsFpgaBaseVerify() no longer requires the imageFilename parameter and instead used metadata.
//!   * hbiPsdbFpgaBaseVerify() no longer requires the imageFilename parameter and instead used metadata.
//!   * hbiMbFpgaBufferLoad() and hbiMbFpgaBaseBufferLoad() have new metadata buffer and size parameters.
//!   * hbiDpsFpgaBufferLoad() and hbiDpsFpgaBaseBufferLoad() have new metadata buffer and size parameters.
//!   * hbiPsdbFpgaBufferLoad() and hbiPsdbFpgaBaseBufferLoad() have new metadata buffer and size parameters.
//!   * hbiMbFpgaBaseBufferVerify() has been removed.
//!   * hbiDpsFpgaBaseBufferVerify() has been removed.
//!   * hbiPsdbFpgaBaseBufferVerify() has been removed.
//! * Additional \b BREAKING \b CHANGES:
//!  * hbiMbUptime() API signature changed.
//!   * Parameter changed from double* to PUINT64.
//!  * Removed stubbed PECI APIs due to POR is "will not use":
//!   * hbiMbPeciConfig()
//!   * hbiMbPeciTemperatureRead()
//!  * Removed stubbed PSDB APIs that have no direct interface (trigger queues only):
//!   * hbiPsdbLtc2975PmbusRead()
//!   * hbiPsdbLtc2975PmbusWrite()
//!   * hbiPsdbAd5684CmdExecute()
//!  * Removed HBI mainboard TDAU APIs due to direct FPGA interface removed (trigger queues only now):
//!   * hbiMbTdauWrite()
//!   * hbiMbTdauRead()
//!   * hbiMbTdauWriteMem()
//!   * hbiMbTdauReadMem()
//! * Added delay after hbiDpsFpgaLoad() loads an image to allow FPGA to update polled registers.
//! * In Python, BLT structures now have a repr() implementation that displays the fields of the BLT structure.
//! * hilTester() updated to detect HBI tester using HBI RCTC FPGA presence as well.
//! * Bug Fix: hbiMtbTmonRead() now uses correct internal function for channel 6.
//! * In hbiDpsVmonRead() implementation, don't read VDD on non-VDD channels.
//!  * It was used on other channels when using external reference.
//!  * Internal reference is now used so it is not necessary.
//!  * Twice as fast to read non-VDD channels now.
//!
//! \par Version 6.4
//! * Modified APIs to support HBI DPS Fab D.
//!  * hbiDpsVmonRead() now detects fab to compute correct channel scaling.
//! * Added hbiDpsStreamingBuffer() API.
//!  * Requires HBI_DPS driver dated 10/16/2019 or later.
//! * FIX: Calling a disconnect in another thread can crash BAR read/write.
//!  * Catches exception and reports HS_MISUSE error code instead.
//! * Updates to support HBI mainboard Fab D.
//!  * Voltage Monitor changes.  See hbiMbVmonRead() help for details.
//!  * Supports fab C/D differences to enabled the debug path for LTM4680.
//! * Memory leak fix.  The following APIs are affected:
//!  * hbiMbCpldProgram()
//!  * hbiMbCpldVerify()
//!  * hbiPsdbCpldProgram()
//!  * hbiPsdbCpldVerify()
//!  * hbiDpsCpldProgram()
//!  * hbiDpsCpldVerify()
//!
//! \par Version 6.3
//! * Added the following APIs:
//!  * hbiMbUptime()
//!  * hbiMbHpsDmaRead()
//!  * hbiMbHpsDmaWrite()
//!  * hbiEPaperWrite()
//!  * hbiEPaperClear()
//!  * bpBltCyCountersRead()
//!  * bpBltCyCountersWrite()
//! * Updates to hbiMbTmonRead() API:
//!  * Removed fault check for internal MAX31855 temperature (channel 9).
//!  * Added a better error message for MAX31855 external temp sensor faults.
//! * Added a better error message when internal FTDI locks cannot be created vs. acquired.
//! * HDMT Ticket 59607: HDMT USB devices slot number addition.
//!  * Updated hilDeviceManagerSlotUpdate() API to support updating Device Manager description only when it has not already been updated.
//!  * Prevents undoing more specific customizations by TOS.
//! * Minor documentation fixes.
//!
//! \par Version 6.2
//! * Updated help documentation and added warning for hbiPsdbFpgaLoad() and hbiPsdbFpgaBufferLoad() usage on PSDB Fab C.
//! * Updated USB paths to support SC3 Fab B on HBI and HDMT legacy tester.
//!
//! \par Version 6.1
//! * Added the following APIs for HBI Mainboard Fab C :
//!   * hbiMbSi5344Write()
//!   * hbiMbSi5344Read()
//!   * hbiMbDdr4SpdEepromWrite()
//!   * hbiMbDdr4SpdEepromRead()
//! * Added hbiMtbConnect().
//! * Fixes:
//!   * hbiMtbVidPidsSet() now has checks to distinguish between HBI MTB and HBI PSDB.
//!     Prior to this fix, hbiMtbVidPidsSet() could potentially set MTB VID PIDs on an
//!     HBI PSDB PM0 FT2232 device and HBI PSDB BLT(FT240X) device.
//!   * Updated hbiPsdbConnect() to succeed if any one of the two PM FPGAs are found.
//!
//! \par Version 6.0
//! * Fix: **Breaking Change** HBI TIU indexing corrected per TIU numbering convention.
//!  * HBI MTB and HBI DTB functions internally using TIU APIs also needed indexing correction, but should be transparent to the user.
//! * Updated hbiMbXxxxx() APIs to support HBI mainboard fab C.
//!  * Added hbiMbFanModeSet()
//!  * Support FT4232 and FT2232 on the HBI mainboard Fab B/C for MAX10 programming.
//! * Added HBI DPS AIC MFG test board APIs:
//!  * hbiMtbVidPidsSet()
//!  * hbiMtbBltBoardRead()
//!  * hbiMtbBltBoardWrite()
//!  * hbiMtbBltEepromRead()
//!  * hbiMtbDpsPresentStatus()
//!  * hbiMtbDpsFuseOkStatus()
//!  * hbiMtbEepromWrite()
//!  * hbiMtbEepromRead()
//!  * hbiMtbAdt7411Write()
//!  * hbiMtbAdt7411Read()
//!  * hbiMtbVmonRead()
//!  * hbiMtbPca9505Write()
//!  * hbiMtbPca9505Read()
//!  * hbiMtbMax14661Write()
//!  * hbiMtbMax14661Read()
//!  * hbiMtbLm75aWrite()
//!  * hbiMtbLm75aRead()
//!  * hbiMtbTmonRead()
//!  * hbiMtbMax6650Write()
//!  * hbiMtbMax6650Read()
//!  * hbiMtbFanRead()
//! * Added hilLocationPathSlot() API that returns slot based on location path.
//! * Added hilDeviceManagerSlotUpdate() API.
//!  * For debugging use without TOS.
//!  * Adds slot information in DeviceManager.
//! * Implemented hbiMbTriggerWaitEx().
//!  * Trigger data extended to 64-bit and contains trigger source information.
//!  * Requires RCTC post-1.2 FPGA that supports trigger source register.
//!  * Requires HBI_RCTC driver dated 06/28/2019 or later.
//! * HPCC AC CPLD SC3 fix for unstable reads of last bit of CPLD version.
//!
//! \par Version 5.0
//! * \b BREAKING \b CHANGES due to API removals:
//!  * Removed all \c pxiXxxxx() APIs.
//!  * Removed all \c calPxiXxxxx() and \c calRfXxxxx() APIs.
//!  * Removed all \c fessXxxxxx() APIs.
//!  * Removed all \c snicSdXxxxx() APIs.
//! * Added hilTester() API to identify the tester chassis.
//! * Added HBI APIs (See header files for a complete list):
//!  * HBI Backplane (HbiBpApi.h)
//!  * HBI Bulk Power Supplies (HbiPowerSupplyApi.h)
//!  * HBI Mainboard (HbiMbApi.h)
//!  * HBI Power and Signal Distribution Board (HbiPsdbApi.h)
//!   * Includes limited PSDB BOB support in an HDMT chassis.
//!   * HBI DPSs installed can be accessed after calling hbiDpsPsdbBob() API.
//!  * HBI DUT Power Supply (HbiDpsApi.h)
//!   * Includes support for DPS BOB in HDMT chassis.
//!  * HBI Test Interface Unit and Diagnostic Test Board (HbiTiuApi.h)
//! * FIX: Internal \c Pci::DeviceDisable() needed to be called twice to resolve HS_DEVICE_IN_USE.
//!  * Calling \c xxxDeviceDisable() APIs fails if handles are open in another process.
//!  * After resolution, \c xxxDeviceDisable() would still fail, but pass on another try.
//!  * This fix removes the need to retry at the user-level to work around this.
//! * Added \c -threads switch to SWIG so Python multithreading can be supported.
//! * Added Site Controller 3 Fab A support.
//! * \ref HBI_DPS_SLOTS and \ref HBI_PSDB_SLOTS constants added.
//! * hilSimEnable() now takes an INT instead of BOOL.  Since \c INT and \c BOOL are both as \c int, \c TRUE(1) and \c FALSE(0) still work correctly for backward compatibility.
//!  * 0 - disable simulation.  hilTester() will re-detect tester.
//!  * 1 - simulate HDMT tester.  hilTester() returns \c TESTER_HDMT.
//!  * 2 - simulate HBI tester.  hilTester() returns \c TESTER_HBI.
//!  * For compatibility, any other non-zero number simulates HDMT tester.
//! * Added new FTDI low-level APIs for debugging purposes.
//!  * ftdiStatus()
//!  * ftdiRead()
//!  * ftdiWrite()
//! * \c xxxVidPidsSet() APIs now program FTDI devices without requiring reboot.
//! * FTDI locking implementation.
//!  * All FTDI device management should be done through HIL 5.0 and later to correctly manage locks.
//! * HIL release no longer contains \c ftd2xx.h header file.
//!  * FTDI devices are completely managed internally by HIL now.
//! * Removal of obsolete PCIe VID/DID and USB VID/PID constants.
//! * Added more \c ftdiXxxxx() API documentation.
//!
//! \par Version 4.4
//! * Fixed sign bug with LTC2485 reads in TIU and CalHPCC APIs:
//!  * tiuCalBaseLtc2485VoltageRead()
//!  * calHpccLtc2485VoltageRead()
//! * Added pciSimOverride() API.  This supports more sophisticated PCIe simulation in C/C++ only.  Not available in Python.
//!
//! \par Version 4.3.1
//! * fpVmonRead() channel 6 implementation updated to fix timeouts when thread is starved.
//! * For Windows error to HIL error translation, added ERROR_TIMEOUT -> HS_TIMEOUT.
//!  * New driver builds weren't returning timeout on DMA when it should.  An updated Windows Driver Kit changed the return value of a timeout.
//! * Updated the HVIL Help file with FAB B part numbers for temperature monitors.
//! * Updated HIL error codes so all development branches are consistent.
//!
//! \par Version 4.3
//! * Added FPGA base image verify APIs.These APIs allow verifying the base FLASH image of an FPGA to the expected image.
//! Optionally, they can dump the FLASH contents to a file for analysis as well.
//!  * hpccDcFpgaBaseVerify()
//!  * hpccAcFpgaBaseVerify()
//!  * dpsFpgaBaseVerify()
//!  * tdbFpgaBaseVerify()
//! * calHvdpsInit() now will not update MAX6650 registers unless they read incorrect values.
//!  * Previously caused SASS calHvdpsFanRead() to read zero if another process called this function due to resetting the fan tachometers
//!    when certain registers were written, even if the value did not change.
//! * An incorrect USB VID/PID now triggers a new #HS_USB_VIDPID_WRONG error code.
//!  * Previously returned #HS_DEVICE_NOT_FOUND.
//!  * An API looking for a specific device will now give a better error code.The device is not missing, it is missing required programming.
//!   * Use the appropriate xxxVidPidsSet() API to fix it.
//! * The internal FTDI enumeration check for devices in use by other processes has been disabled.
//!  * This allows more devices to work properly instead of returning #HS_FTDI_ENUMERATION for *all* FTDI devices.
//!  * The device that had its handle open in other process will fail with #HS_DEVICE_NOT_FOUND instead.
//!  * HIL 5.0 will have a better solution.
//! * Now uses the FTDI API FT_CyclePort() internally to prevent needing a reboot when xxxVidPidsSet() APIs program FTDI devices.
//! * VID/PID programming bug fixes:
//!  * xxxVidPidsSet() APIs now check for Cypress handle validity and work correctly when a SiteC Fab D or earlier is installed.
//!  * calHpccSnicVidPidSet() sets default FTDI VID/PID on the extra FT240XQ used for USB port testing.
//!   * This extra FT240X is only visible if the HPCC/HPCC2 instrument has a test SNIC installed as well.
//!  * Partial ID of the extra test FT240XQ on the HPCC calibration card corrected.  It is set to a default FTDI VID/PID if found.
//!    It is only visible if an HPCC2 instrument is installed.
//!
//! \par Version 4.2
//! * Added HDDPS calibration APIs:
//!  * calHddpsCalEepromRead()   // The 64KB "Cal" EEPROM @ U4
//!  * calHddpsCalEepromWrite()
//!  * calHddpsEepromRead()      // The  8KB "BLT" EEPROM (schematic name, but not used for BLT) @ U5
//!  * calHddpsEepromWrite()
//! * FTDI enumeration check disabled.
//!
//! \par Version 4.1
//! * Added PMBus Linear floating point format helper APIs.
//!  * hilFromL11()
//!  * hilToL11()
//!  * hilFromL11()
//!  * hilToL11()
//! * bpTiuAuxPowerEnable() clears cached resources when powering off the TIU.
//!  * xxxDisconnect() for calibration base and calibration cards no longer required before power off.
//!  * Regression tests added for all calibration cards and calibration base to test power off cleanup.
//! * Added rcLmk04808PllIsLocked() API.
//!  * Reads the PLL_LOCK status pin of the clock chip.
//! * Added interthread and interprocess protections. **NOTE: ALL processes MUST be using HIL 4.1 or later to take advantage of the protections!**
//! * Added interthread and interprocess protection to TDAU diagnostics card APIs.
//!  * All APIs affected.
//! * Added interthread and interprocess protection to TDAU Bank APIs:
//!  * tdbInit()
//!  * tdbTdauWrite()
//!  * tdbTdauRead()
//!  * tdbTdauWriteMem()
//!  * tdbTdauReadMem()
//!  * tdbBmr454Write()
//!  * tdbBmr454Read()
//!  * tdbVmonRead()
//!  * tdbTmonRead()
//!  * tdbAdt7411Write()
//!  * tdbAdt7411Read()
//! * Added interthread and interprocess protection to HPCC calibration card APIs.
//!  * All APIs affected.
//! * Added interthread and interprocess protection to resource card APIs:
//!  * rcInit()
//!  * rcConnect()
//!  * rcDisconnect()
//!  * rcVidPidsSet()
//!  * rcTriggerWaitCancel()
//!  * rcFailsafeDisable()
//!  * rcFailsafeTimerSet()
//!  * rcFailsafePing()
//!  * rcAdt7411Write()
//!  * rcTriggerBusTrxReset()
//!  * rcTriggerBusStatus()
//!  * rcTriggerSend()
//!  * rcTriggerSyncSend()
//!  * rcBltBoardRead()
//!  * rcBltBoardWrite()
//!  * rcEepromRead()
//!  * rcEepromWrite()
//!  * rcSysIdRead()
//!  * rcSysIdWrite()
//!  * rcMesIdRead()
//!  * rcMesIdWrite()
//! * Added interthread and interprocess protection to HDDPS calibration card APIs:
//!  * calHddpsInit()
//!  * calHddpsPca9505Init()
//!  * calHddpsPca9505Write()
//!  * calHddpsPca9505Read()
//!  * calHddpsGpiosReset()
//!  * calHddpsGpioWrite()
//!  * calHddpsGpioRead()
//!  * calHddpsTmonRead()
//!  * calHddpsLm75aRead()
//!  * calHddpsLm75aWrite()
//! * Added interthread and interprocess protection to HVDPS calibration card APIs:
//!  * calHvdpsInit()
//!  * calHvdpsEepromRead()
//!  * calHvdpsEepromWrite()
//!  * calHvdpsTmonRead()
//!  * calHvdpsLm75aRead()
//!  * calHvdpsLm75aWrite()
//!  * calHvdpsPca9505Init()
//!  * calHvdpsPca9505Write()
//!  * calHvdpsPca9505Read()
//!  * calHvdpsMax6650Write()
//!  * calHvdpsMax6650Read()
//!  * calHvdpsFanRead()
//!  * calHvdpsGpiosReset()
//!  * calHvdpsGpioWrite()
//!  * calHvdpsGpioRead()
//! * Added interthread and interprocess protection to TIU calibration base APIs:
//!  * tiuCalBaseGpioWrite()
//!  * tiuCalBaseGpioRead()
//!  * tiuCalBasePca9505Write()
//!  * tiuCalBasePca9505Read()
//!  * tiuCalBaseMax6651Write()
//!  * tiuCalBaseMax6651Read()
//!  * tiuCalBaseFanRead()
//!  * tiuCalBaseLtc2485ModeSet()
//!  * tiuCalBaseLtc2485VoltageRead()
//! * Added/Updated copyrights to 2018 standards.
//!
//! \par Version 4.0.2
//! * hvilVoltageComparatorLimitsSet() valid range for limits updated per HLD.
//!  * Old limits: -6.25V to +6.25V.
//!  * New limits: -0.5V to +5.0V.
//! * calHvdpsPca9505Init() updated for a CAL HVDPS Fab B change.
//!  * A relay output control was added to port 1 in Fab B.
//!  * U1 Port 1 updated direction from all inputs to all outputs.
//!  * calHvdpsInit() also affected.
//!
//! \par Version 4.0.1
//! * FIX: Changed the LTC2453 code so it doesn't hide other I2C errors as HS_I2C_TIMEOUT.
//!  * Affects fpVmonRead() for channel 6.
//! * HVDPS - DB Fab B load resistor documentation change.
//!
//! \par Version 4.0
//! * Major version change due to updated build tools.
//!  * Updated Visual Studio 2012 to Visual Studio 2017.
//!   * Visual Studio 2017 or at a minimum the Microsoft Visual C++ Redistributable 2017 64-bit (VC_redist.x64.exe) must be installed.
//!   * https://www.itechtics.com/microsoft-visual-c-redistributable-versions-direct-download-links/
//!  * Updated SWIG 2.0.12 to SWIG 3.0.12 (Python extension generator).
//!  * Updated Doxygen 1.8.1.2 to 1.8.14 (Help file generator).
//!  * The Python extension now targets Python 3.6.
//!   * On HDMT testers that haven't been updated, Python 3.6.x 64-bit (python-3.6.5-amd64.exe) must be installed.
//!   * https://www.python.org/ftp/python/3.6.5/python-3.6.5-amd64.exe
//! * HVDPS APIs updated to support FPGA change to from raw ADC values to floating point registers in some register interfaces.  The change was made in a backward-compatible manner older FPGAs will still report correctly.  Affected functions:
//!  * hvdpsRailCurrentRead()
//!  * hvdpsRailVoltageRead()
//!  * hvdpsTurboRailVoltageRead()
//!  * hvdpsTurboRailCurrentRead()
//!  * hvdpsDvmRailVoltageRead()
//!  * hvdpsDvmTurboRailVoltageRead()
//!  * hvilRailCurrentRead()
//!  * hvilTurboRailCurrentRead()
//! * FIX: hvilTurboRailCurrentRead() channel select written correctly.
//!
//! \par Version 3.1
//! * HVDPS/HVIL interprocess protection added for FPGA access.
//! * Thread safety of internal handle caches improved.
//! * Fixed hvdpsHvLtmRailVoltageRead() voltage conversion.  Scalar for raw data to voltage conversion corrected from 2.1 to 2.402.
//! * Fixed warning when HilDefs.h was used in a Managed C++ project.
//! * Increased WaitForChangeDetectIdle() default from 2 to 4 seconds.  This affects the wait time for USB devices to connect/disconnect when cycling power on the TIU.
//! * Added calHvdpsFanRead() API.
//! * Fixed incorrect FPGA register used in hvilTurboRailCurrentRead() implementation.
//! * Added tiuCalBaseFanRead() API.
//! * Added hvdpsLtm4620TmonRead() API.
//!  * Translates the voltage already exposed by hvdpsLtc2450AdcVoltageRead().
//! * Updated GetBlt.py to handle missing BLT errors.
//!
//! \par Version 3.0
//! * Added some missing front panel sensor functions for SASS:
//!  * fpVmonRead()
//!  * Updated fpTmonRead() to read Adt7411 internal temperature.
//! * Added an API to monitor the internal temperature of the ADT7411s on TDAU bank cards:
//!  * tdbTmonRead()
//! * Added bpTmonRead() API to read the backplane's three ADT7411 internal temperatures.
//! * Added HVDPS, HVIL and HVDPS calibration APIs.  The new APIs are listed and documented in:
//!  * HvdpsApi.h
//!  * HvilApi.h
//!  * CalHvdpsApi.h
//! * Removed PST support (**BREAKING CHANGE**):
//!  * EsnicApi.h removed.
//!  * CalEhcdpsApi.h removed.
//!  * CalEsnicApi.h removed.
//!  * Support for EHCDPS removed from generic dpsXxxxxx() APIs.
//! * HDDPS APIs refactored:
//!  * The following common DPS functions were removed (**BREAKING CHANGE**):
//!   * hddpsCpldVersion()
//!   * hddpsCpldVersionString()
//!   * hddpsFpgaVersion()
//!   * hddpsDeviceDisable()
//!   * hddpsDeviceEnable()
//!   * hddpsFpgaLoad()
//!   * hddpsFpgaBaseLoad()
//!   * hddpsJtagExecute()
//!   * hddpsFpgaBufferLoad()
//!   * hddpsFpgaBaseBufferLoad()
//!   * hddpsJtagBufferExecute()
//!   * hddpsBarRead()
//!   * hddpsBarWrite()
//!   * hddpsDmaRead()
//!   * hddpsDmaWrite()
//!   * hddpsMemRead()
//!   * hddpsMemWrite()
//!   * hddpsBltInstrumentRead()
//!   * hddpsBltInstrumentWrite()
//!   * hddpsBltBoardRead()
//!   * hddpsBltBoardWrite()
//!   * hddpsBltEepromRead()
//!   * hddpsStreamingBuffer()
//!  * The following HDDPS-specific APIs were added:
//!   * hddpsAd5764Read()
//!   * hddpsAd5764Write()
//!   * hddpsAd5560Read()
//!   * hddpsAd5560Write()
//!   * hddpsRailVoltageRead()
//!   * hddpsRailCurrentRead()
//!   * hddpsVlcRailVoltageRead()
//!   * hddpsVlcRailCurrentRead()
//! * DPS APIs refactored:
//!  * The following APIs removed EHCDPS support, but added HVDPS/HVIL and HDDPS support (**BREAKING CHANGE**):
//!   * dpsConnect()
//!   * dpsDisconnect()
//!   * dpsCpldVersion()
//!   * dpsCpldVersionString()
//!   * dpsFpgaVersion()
//!   * dpsDeviceDisable()
//!   * dpsDeviceEnable()
//!   * dpsFpgaLoad()
//!   * dpsFpgaBaseLoad()
//!   * dpsJtagExecute()
//!   * dpsFpgaBufferLoad()
//!   * dpsFpgaBaseBufferLoad()
//!   * dpsJtagBufferExecute()
//!   * dpsBarRead()
//!   * dpsBarWrite()
//!   * dpsDmaRead()
//!   * dpsDmaWrite()
//!   * dpsMemRead()
//!   * dpsMemWrite()
//!   * dpsBltInstrumentRead()
//!   * dpsBltInstrumentWrite()
//!   * dpsBltBoardRead()
//!   * dpsBltBoardWrite()
//!   * dpsBltEepromRead()
//!   * dpsStreamingBuffer()
//!  * The following APIs were removed as they were EHCDPS-specific (**BREAKING CHANGE**):
//!   * dpsBmr453Configure()
//!   * dpsBmr453Read()
//!   * dpsBmr453Write()
//!   * dpsMax6662Read()
//!   * dpsMax6662Write()
//!   * dpsAd5764Read()
//!   * dpsAd5764Write()
//!   * dpsAd5560Read()
//!   * dpsAd5560Write()
//!   * dpsRailVoltageRead()
//!   * dpsRailCurrentRead()
//!   * ehcdpsBoardIdRead()
//!   * dpsMax6662TmonRead()
//!  * The following APIs were added with generic support for HVDPS/HVIL and HDDPS:
//!   * dpsTriggerUpTest()
//!   * dpsTriggerDownTest()
//! * Added support for programming VID/PIDs of Cypress/FTDI devices to allow scripting instead of using VidPidProgrammer manually.
//!   Note that while Cypress devices change their VID/PID immediately, a reboot is required for FTDI VID/PID changes to take effect.
//!  * calHddpsVidPidsSet()
//!  * calHpccSnicVidPidSet()
//!  * calHpccVidPidsSet()
//!  * calHvdpsVidPidsSet()
//!  * hddpsVidPidsSet()
//!  * hpccAcVidPidsSet()
//!  * hpccDcVidPidsSet()
//!  * hpccSnicTestVidPidSet()
//!  * hvdpsVidPidsSet()
//!  * hvilVidPidsSet()
//!  * rcVidPidsSet()
//!  * scVidPidSet()
//!  * tdbVidPidsSet()
//!  * tddVidPidSet()
//!  * tiuCalBaseVidPidsSet()
//! * Additional HDDPS calibration APIs added:
//!  * calHddpsTmonRead()
//!  * calHddpsLm75aRead()
//!  * calHddpsLm75aWrite()
//! * Added additional TIU calibration base APIs:
//!  * tiuCalBaseLmk04808Write()
//!  * tiuCalBaseLmk04808Read()
//!  * tiuCalBaseHmc987Write()
//!  * tiuCalBaseDelayLineSet()
//!  * tiuCalBaseWindowCompRefSet()
//! * Added additional HPCC Cal APIs.
//!  * calHpccEepromWrite()
//!  * calHpccEepromRead()
//!  * calHpccLmk01000Write()
//!  * calHpccLmk01000ClocksEnable()
//!  * calHpccLmk01000ClocksSync()
//!  * calHpccPca9554Write()
//!  * calHpccPca9554Read()
//!  * calHpccWindowCompRefSet()
//!  * calHpccDelayLineSet()
//!  * calHpccLtc2485ModeSet()
//!  * calHpccLtc2485VoltageRead()
//! * Updated maximum memory offset allowed from 512MB to 1GB in the following functions.
//!  * dpsDmaRead()
//!  * dpsDmaWrite()
//!  * dpsMemRead()
//!  * dpsMemWrite()
//!
//! \par Version 2.8.1
//! * BLT board write APIs will now update the AA number, serial number, and manufacturing date fields in a Gen1 BLT.
//!  * This will also correct CRC errors in the Gen1 BLT.
//!  * Only updates an existing Gen1 BLT to be consistent with the Gen2 BLT.  It will not add a Gen1 BLT if it doesn't exist.
//! * The following APIs now support nullptr/None to indicate erasing the indicated BLT.
//!  * dpsBltInstrumentWrite()
//!  * hddpsBltInstrumentWrite()
//! * FTDI caching implemented.
//!  * Monitors device arrival and removal of FTDI and Cypress USB devices.
//!  * Maintains FTDI device cache that is invalidated by device changes.
//!  * bpTiuAuxPowerEnable() now waits until device changes are recognized and stable.
//!   * Code can eliminate hard-coded sleeps after changing the TIU power state!
//! * User Story 39085: Create new Instrument BLT HIL APIs for HPCC2-CE CAL.
//!  * calHpccBltInstrumentRead()
//!  * calHpccBltInstrumentWrite()
//! * VID/PIDs now verified for all FTDI device open calls.
//!  * Previously FTDI devices were opened by hardware location only, and some hardware had FTDI devices in the same relative locations.
//! * Fix intraprocess C threading race condition with RC I2C mutexes.
//!  * Only affected threads in a single C/C++ process.
//!  * Did not affect Python.
//!
//! \par Version 2.8
//! * Added streaming-to-host support APIs.
//!  * tdbStreamingBuffer()   # for TDAUBANK
//!  * dpsStreamingBuffer()   # for EHCDPS
//!  * hddpsStreamingBuffer() # for HDDPS/MDDPS
//!  * Requires latest HDMT_TDAUBANK and HDMT_HDDPS drivers.
//! * Added new APIs for TDAUBANK Fab B.
//!  * tdbInit()
//!  * tdbAdt7411Write()  # Direct access to ADT7411 voltage monitor registers.
//!  * tdbAdt7411Read()
//!  * tdbVmonRead()      # Reads voltages from ADT7411 channels.
//! * Cal Ehcdps functions added:
//!  * calEhcdpsConnect()
//!  * calEhcdpsDisconnect()
//!  * calEhcdpsInit()
//!  * calEhcdpsBltBoardRead()  # Reads FTDI 240X BLT.
//!  * calEhcdpsBltBoardWrite()
//!  * calEhcdpsBltEepromRead()
//!  * calEhcdpsEepromRead()    # Reads 24LC512 "CAL" EEPROM.
//!  * calEhcdpsEepromWrite()
//!  * calEhcdpsMc24Lc64Read()  # Reads 24LC64 "BLT" EEPROM (schematic label, but not used for BLT).
//!  * calEhcdpsMc24Lc64Write()
//!  * calEhcdpsTmonRead()      # Reads five LM75A temperature sensors.
//!  * calEhcdpsLm75aRead()     # Temperature sensors device register access.
//!  * calEhcdpsLm75aWrite()
//!  * calEhcdpsPca9505Init()   # Sets PCA9505 GPIO default direction and state.
//!  * calEhcdpsPca9505Write()
//!  * calEhcdpsPca9505Read()
//!  * calEhcdpsMax6650Write()  # Fan controller register access.
//!  * calEhcdpsMax6650Read()
//! * Added TDAU Diagnostics card APIs:
//!  * tddConnect()
//!  * tddDisconnect()
//!  * tddInit()
//!  * tddBltBoardRead()
//!  * tddBltBoardWrite()
//!  * tddEepromRead()
//!  * tddEepromWrite()
//!  * tddPeciVrefSet()  # Set/Get the PECI Vref voltage.  tddInit() sets this to 1.2V.
//!  * tddPeciVrefGet()
//! * Added ESNIC calibration snap-in APIs:
//!  * calEsnicConnect()
//!  * calEsnicDisconnect()
//!  * calEsnicBltBoardRead()
//!  * calEsnicBltBoardWrite()
//!  * calEsnicBltEepromRead()
//!  * calEsnicEhcdpsCalIsPresent()
//! * Made ehcdpsBoardIdRead() return something sensible when BayID doesn't work yet.
//!  * Assumes bay 0 if slot is 0-5.
//!  * Assumes bay 1 if slot is 6-11.
//! * Added more device-level HPCC calibration card APIs:
//!  * calHpccInit()
//!  * calHpccPca9506Write()  # Direct register access to the GPIO device.
//!  * calHpccPca9506Read()
//! * Added more device-level TIU calibration base APIs:
//!  * tiuCalBasePca9555Write() # Direct register access to the GPIO device.
//!  * tiuCalBasePca9555Read()
//!  * tiuCalBaseMax6651Write() # Direct register access to the fan controller.
//!  * tiuCalBaseMax6651Read()
//!  * tiuCalBaseLtc2485ModeSet()     # Set the ADC mode.
//!  * tiuCalBaseLtc2485VoltageRead() # Read the ADC voltage.
//! * Updated Serdes SNIC USB paths due to hardware change from DOE to Fab A.
//!  * DOE no longer supported.
//!
//! \par Version 2.7.1
//! * HDMT Ticket 37896: Unable to retrain HPCC AC FPGA PCIe link for width downgrade after loading AC FPGA image.
//!  * Added support for 6MHz RunClk command in HPCC AC CPLD starting with "D050", with backwards compatibility for older CPLDs.
//!  * Affected functions: hpccAcFpgaLoad(), hpccAcFpgaBufferLoad().
//!
//! \par Version 2.7
//! * Added EHCDPS APIs.
//!  * NOTE: APIs use dpsXxxxx() naming convention except where specific to EHCDPS.
//!  * NOTE: Future common support of HDDPS, MDDPS, HVDPS.
//!  * NOTE: hddpsXxxxx() APIs will be deprecated in a future release.
//!  * dpsConnect()
//!  * dpsDisconnect()
//!  * dpsCpldVersion()
//!  * dpsCpldVersionString()
//!  * dpsFpgaVersion()
//!  * dpsDeviceDisable()
//!  * dpsDeviceEnable()
//!  * dpsFpgaLoad()
//!  * dpsFpgaBaseLoad()
//!  * dpsJtagExecute()
//!  * dpsFpgaBufferLoad()
//!  * dpsFpgaBaseBufferLoad()
//!  * dpsJtagBufferExecute()
//!  * dpsBarRead()
//!  * dpsBarWrite()
//!  * dpsDmaRead()
//!  * dpsDmaWrite()
//!  * dpsMemRead()
//!  * dpsMemWrite()
//!  * dpsBltInstrumentRead()
//!  * dpsBltInstrumentWrite()
//!  * dpsBltBoardRead()
//!  * dpsBltBoardWrite()
//!  * dpsBltEepromRead()
//!  * dpsBmr453Configure()
//!  * dpsBmr453Read()
//!  * dpsBmr453Write()
//!  * dpsMax6662TmonRead()
//!  * dpsMax6662Read()
//!  * dpsMax6662Write()
//!  * dpsAd5764Read()
//!  * dpsAd5764Write()
//!  * dpsAd5560Read()
//!  * dpsAd5560Write()
//!  * dpsRailVoltageRead()
//!  * dpsRailCurrentRead()
//!  * ehcdpsBoardIdRead()
//! * Added ESNIC APIs:
//!  * esnicConnect()
//!  * esnicDisconnect()
//!  * esnicInit()
//!  * esnicVmonRead()
//!  * esnicAdt7411Read()
//!  * esnicAdt7411Write()
//!  * esnicTmonRead()
//!  * esnicMax6642Read()
//!  * esnicMax6642Write()
//!  * esnicBltBoardRead()
//!  * esnicBltBoardWrite()
//!  * esnicEepromRead()
//!  * esnicEepromWrite()
//! * Added fessTcControl() API.
//!  * Configures the FESS tone combiner daughterboard relays.
//!  * This API returns HS_UNSUPPORTED if a FESS tone combiner is not installed.
//!  * Documentation updated.  The following APIs return HS_UNSUPPORTED if a FESS tone combiner is installed.  They required a FESS RF daughterboard:
//!   * fessChannelConfig()
//!   * fessRxSwitchPathEnable()
//!   * fessTxAttenuatorSet()
//!   * fessTxPowerVoltage()
//!   * fessRxPowerVoltage()
//! * Updated FESS documentation for Fab A.
//!  * fessRxSwitchPathEnable() documented that Fab A has no attenuator.
//! * Fix: calHpccConnect() and calPxiConnect() were detecting each other's cards.
//!  * Now checks VID/PID of devices to differentiate.
//!  * Run VidPidProgrammer to ensure the FTDI devices have Intel vendor/device IDs.
//! * Added HDDPS MAX6662 temperature monitor APIs.
//!  * hddpsMax6662Read()
//!  * hddpsMax6662Write()
//!  * hddpsMax6662TmonRead()
//! * Added HPCC temperature monitor APIs:
//!  * hpccAdAte320TmonRead()
//!  * hpccMax1230TmonRead()
//!  * hpccAcFpgaTmonRead()
//!  * hpccVmonRead()
//!
//! \par Version 2.6.1
//! * Fix for HDMT Ticket 31394: HPCC in slot x, AC FPGA 1, Failed to enable device driver after loading FPGA.
//!  * Affected functions: hpccAcFpgaLoad(), hpccAcFpgaBufferLoad().
//!   * Added two-second delay before sending image data to CPLD.
//!   * Now sends a 24KB buffer extra data to provide clocking for configuration after image data.
//!   * Increased the CPLD reset timeout from 6 seconds to 30 seconds, for compatibility with future CPLD improvements.
//!
//! \par Version 2.6
//! * Added power supply firmware APIs:
//!  * psFirmwareVersions()
//!  * psFirmwareLoad()
//!  * psFirmwareBufferLoad()
//! * Fixed a bug in psConnect().  It would pass on empty bulk power supply bays.
//!
//! \par Version 2.5
//! * Added TIU cycle counter APIs:
//!  * tiuBltCyCountersWrite()
//!  * tiuBltCyCountersRead()
//! * Added eepromIndex parameter to tiuBltBoardRead() and tiuBltBoardWrite().
//!  * API CHANGE!
//! * Added psBltBoardWrite() API.
//!  * Only supported on Delta supplies.
//!  * Only supports updating the current part number.
//! * FESS updates to support Fab A.  Slot and subslot parameters are now used.
//!  * DOE/DOE2 still supported if -1 is used for slot parameter.
//!  * Only supports one DOE/DOE2 cabled to the front panel.
//!
//! \par Version 2.4
//! * Added TIU BLT APIs:
//!  * tiuBltBoardRead()
//!  * tiuBltBoardWrite()
//!  * tiuEepromRead()
//!  * tiuEepromWrite()
//! * Added Extended BLT APIs to TIU.  These support up to eight separate BLT EEPROMs available on some production TIUs.
//!  * tiuBltExRead()
//!  * tiuBltExWrite()
//! * Added Extended Counters APIs to TIU.  These support up to eight separate BLT EEPROMs available on some production TIUs.
//!  * tiuBltExCountersRead()
//!  * tiuBltExCountersWrite()
//! * Validated that HDDPS APIs work correctly on an MDDPS.
//!  * MDDPS is equivalent to HDDPS mainboard and uses the same APIs.
//!  * Use hddpsXxxxx(slot,subslot,...) APIs.
//!  * Always use subslot=0 since there is never a daughterboard.
//! * Added RB2 calibration APIs:
//!  * calRfMax6627Read() - Raw data read access to five sensors on RB2 daughterboard.
//!  * calRfEepromRead()  - Direct access to RB2 daughterboard EEPROM.
//!  * calRfEepromWrite() - Direct access to RB2 daughterboard EEPROM.
//!  * calRfBltBoardRead()
//!  * calRfBltBoardWrite()
//! Updated SilverCreek APIs to support SilverCreek 2 and RB2 Calibration Daughterboard.
//!  * calPxiTmonRead()    - Reads and translates temperature-monitoring sensors across all cards.
//!  * calPxiPca9506Read() - Low-level register access. Now supports SilverCreek 2 (fewer chips).
//!  * calPxiMax6627Read() - Raw data read access. Now supports SilverCreek 2 (fewer chips).
//! The following APIs now return HS_UNSUPPORTED on SilverCreek 2 (hardware removed):
//!  * calPxiAd5791Read()
//!  * calPxiAd5791Write()
//!  * calPxiAd9914Read()
//!  * calPxiAd9914Write()
//!  * calPxiAd9838Write()
//!  * calPxiAd5660Write()
//!  * calPxiLmk04808Read()
//!  * calPxiLmk04808Write()
//!  * calPxiLmk01000Write()
//! * Added TDAU Bank PECI APIs:
//!  * tdbPeciConfig() - Configures a PECI channel (enable/disable, address, bit rate).
//!  * tdbPeciTemperatureRead() - Reads the temperature of the specified PECI channel.
//! * Expanded power supply and power supply extender APIs to support four bays. APIs affected:
//!  * psConnect()
//!  * psDisconnect()
//!  * psD1uRead() - Only available on Murata supplies.
//!  * psPmbusWrite() - Only available on Delta supplies.
//!  * psPmbusRead() - Only available on Delta supplies.
//!  * psExtBltBoardRead()
//!  * psExtBltBoardWrite()
//!  * psExtEepromRead()
//!  * psExtEepromWrite()
//!  * psFruEepromRead() - Direct read of 3rd party FRU (field replaceable unit) EEPROM (similar to Intel BLT)
//!  * psBltBoardRead()  - Intel pseudo-BLT constructed from FRU information.  No write available.
//!  * Note: No BMR454 on EPSEs in outer bays, psExtBmr454Xxxxx() commands are unaffected.
//! * Added BLT support for HPCC manufacturing test snap-in card.
//!  * hpccSnicTestBltBoardRead()
//!  * hpccSnicTestBltBoardWrite()
//!  * hpccSnicTestBltEepromRead()
//! * Added a clock mux control API to the HPCC calibration snap-in card.
//!  * calHpccSnicClockSelect()
//! * Added hilComplianceEnable() API.
//!  * Internal flag.
//!  * Allows HIL to ignore some bugs that exist in field testers.
//!    * For example, HAL's existing implementation ignores problems with BLT structure of no interest, like legacy Gen1 BLT CRC or payload size errors, and the BLT record count has been higher than actual.
//!  * Allows diagnostics to enable checks to detect problems in the factory.
//!    * For example, writing invalid BLT records or invalid counts.
//!  * For compatibility with HAL, this flag is default disabled.
//! * Added resource card SysId/MesID APIs.
//!  * rcSysIdRead()
//!  * rcSysIdWrite()
//!  * rcMesIdRead()
//!  * rcMesIdWrite()
//! * Added site controller SysId/MesID APIs.
//!  * scSysIdRead()
//!  * scSysIdWrite()
//!  * scMesIdRead()
//!  * scMesIdWrite()
//! * Added front panel SysId/MesID APIs.
//!  * fpSysIdRead()
//!  * fpSysIdWrite()
//!  * fpMesIdRead()
//!  * fpMesIdWrite()
//! * Added Backplane SysId/MesID APIs.
//!  * bpSysIdRead()
//!  * bpSysIdWrite()
//!  * bpMesIdRead()
//!  * bpMesIdWrite()
//!
//! \par Version 2.3.1
//! * Updated psBltBoardRead() API's simulated Intel BLT Id field per HAL request.
//!  * Murata supplies return 0x12000001.
//!  * Delta supplies return 0x13000001.
//!
//! \par Version 2.3
//! * Added FESS APIs:
//!  * fessInit()
//!  * fessFlashBulkErase() - supports the configuration FLASH.
//!  * fessFlashSectorErase()
//!  * fessFlashProgram()
//!  * fessFlashRead()
//!  * fessS70fl01gsExecute() - debug API for FLASH.
//!  * fessChannelConfig() - configure FESS channels for transmit or receive.
//!  * fessRxSwitchPathEnable() - select an Rx channel and optional attenuation.
//!  * fessTxAttenuatorSet() - set the attenuation on a Tx channel.
//!  * fessTxPowerVoltage() - Read the Tx power (ADC voltage)
//!  * fessRxPowerVoltage() - Read the Rx power (ADC voltage)
//!  * fessTmonRead() - Read the Tx/Rx temperatures, and the MAX6627 thermal diodes.
//!  * fessAlarmWait() - Supports the MAX6627 temperature sensors.
//!  * fessAlarmWaitCancel()
//!  * fessTmonAlarmEnable()
//!  * fessTmonAlarmLimitsRead()
//!  * fessTmonAlarmLimitsWrite()
//!  * fessBltInstrumentRead()
//!  * fessBltInstrumentWrite()
//!  * fessBltBoardRead()
//!  * fessBltBoardWrite()
//!  * fessEepromRead()
//!  * fessEepromWrite()
//!  * fessDmaRead() - 512MB accessible.
//!  * fessDmaWrite()
//! * The following functions have a workaround applied to support the FESS DOE and DOE2 boards.  The workaround will be removed in the next HIL release supporting FESS Fab A:
//!  * fessCpldVersion()
//!  * fessCpldVersionString()
//!  * fessFpgaLoad()
//!  * fessFpgaBaseLoad()
//!  * fessJtagExecute()
//!  * fessFpgaBufferLoad()
//!  * fessFpgaBaseBufferLoad()
//!  * fessJtagBufferExecute()
//! * Added Serdes SNIC APIs.
//!  * snicSdConnect()
//!  * snicSdDisconnect()
//!  * snicSdCpldVersion()
//!  * snicSdCpldVersionString()
//!  * snicSdFpgaVersion()
//!  * snicSdFpgaVersionString()
//!  * snicSdDeviceEnable()
//!  * snicSdDeviceDisable()
//!  * snicSdBarRead()
//!  * snicSdBarWrite()
//!  * snicSdBltBoardRead()
//!  * snicSdBltBoardWrite()
//!  * snicSdBltEepromRead()
//!  * snicSdHmc7043Read()
//!  * snicSdHmc7043Write()
//!  * snicSdAd5560TxRead()
//!  * snicSdAd5560TxWrite()
//!  * snicSdAd5560RxRead()
//!  * snicSdAd5560RxWrite()
//! * Added power supply extender BLT APIs (note, no EPSE support in this release, only the internal PSEs).
//!  * psExtBltBoardRead()
//!  * psExtBltBoardWrite()
//!  * psExtEepromWrite()
//!  * psExtEepromRead()
//! * Added bulk power supply APIs.
//!  * psFruEepromRead() - Note: FRU (field replaceable unit) is different that our BLT.  Configured by vendor so no write supported.
//!  * psBltBoardRead()  - Note: Simulated BLT populated from FRU.
//!  * psPmbusRead()     - Interface supported on Delta supplies only.
//!  * psPmbusWrite()    - Interface supported on Delta supplies only.
//! * Added PXI 32-bit trigger (mDUT) APIs:
//!  * pxiTriggerEncode32()
//!  * pxiDutDomainIdSet()
//! * The -402 resource card has a change that breaks programmatic JTAG access.  -403 restores the functionality.  Added note to affected functions in the help file.
//! * Corrected bpInit() documentation saying it was implicitly called.  It must be explicitly called.
//! * Removed a delay in the TDAU Bank's BMR454 APIs added to fix a problem with FPGA 4.3. It is fixed in FPGA 4.4. TDAU Bank FPGA 4.4 must be used with this version of HIL.
//!
//! \par Version 2.2
//! * Added TDAU Bank DMA APIs.  Requires 12/19/2016 HDMT_TDAUBANK driver and FPGA version 4.4.
//!  * tdbDmaWrite()
//!  * tdbDmaRead()
//! * Added BLT APIs for three more boards:
//!  * bpBltBoardRead()
//!  * bpBltBoardWrite()
//!  * rcBltBoardRead()
//!  * rcBltBoardWrite()
//!  * tcBltBoardRead()
//!  * tcBltBoardWrite()
//! * added new low-level EEPROM access APIs:
//!  * bpEepromRead() - This 8KB EEPROM contains the backplane BLT.
//!  * bpEepromWrite()
//!  * rcEepromRead() - This 64KB EEPROM contains both the RC and TC BLTs.
//!  * rcEepromWrite()
//!
//! \par Version 2.1
//! * Added TDAU and BMR464 APIs for TDAU Bank.
//!  * tdbTdauWrite() sends TDAU device write commands.  See TDAU documentation.
//!  * tdbTdauRead() sends TDAU device read commands.
//!  * tdbTdauWriteMem() updates the TDAU memory map.
//!  * tdbTdauReadMem() read the TDAU memory map.
//!  * tdbBmr454Write() sends BMR454 device write commands.
//!  * tdbBmr454Read() sends BMR454 device read commands.
//!
//! \par Version 2.0
//! * Fix for non-constant FTDI location IDs when new USB host controllers are introduced.
//!  * Instruments with additional USB host controllers break the pre-HIL 2.0 and HAL FTDI enumeration algorithms. HAL needs this fix as well.
//! * Major revision to 2.0 due to API interface changes. (**BEHAVIOR CHANGE**)
//!  * A re-factor to support instruments with sub-slots enabled the following additional features:
//!   * HPCC device driver enable/disable enhanced to specify the target FPGA.  Previously it couldn't differentiate and targeted both.
//!    * hpccAcDeviceEnable() added a parameter.
//!    * hpccAcDeviceDisable() added a parameter.
//!    * hpccDcDeviceEnable() added a parameter.
//!    * hpccDcDeviceDisable() added a parameter.
//!   * Debugging APIs used internally to implement above APIs and allow for future support of sub-slots in instruments (e.g. FESS).
//!    * pciDeviceEnable() added a parameter.
//!    * pciDeviceDisable() added a parameter.
//!    * cypDeviceOpen() added a parameter.
//!    * pciDeviceOpen() changed a parameter.
//! * The following sets of APIs were added to support passing a pre-allocated buffer of data instead of a filename to support encrypted files.
//!  * JTAG Execution (CPLD programming and verifying)
//!   * calHpccJtagBufferExecute()
//!   * hddpsJtagBufferExecute()
//!   * hpccAcJtagBufferExecute()
//!   * hpccDcJtagBufferExecute()
//!   * pxiJtagBufferExecute()
//!   * rcJtagBufferExecute()
//!   * tdbJtagBufferExecute()
//!   * tiuCalBaseJtagBufferExecute()
//!  * FPGA Dynamic Load
//!   * hddpsFpgaBufferLoad()
//!   * hpccAcFpgaBufferLoad()
//!   * hpccDcFpgaBufferLoad()
//!   * pxiFpgaBufferLoad()
//!   * rcFpgaBufferLoad()
//!   * tdbFpgaBufferLoad()
//!   * cypDataIoFpgaBufferLoad() (for debugging not general use)
//!   * cypGpifFpgaBufferLoad() (for debugging not general use)
//!  * Programming FPGA Base Image (SPI FLASH)
//!   * hddpsFpgaBaseBufferLoad()
//!   * hpccAcFpgaBaseBufferLoad()
//!   * hpccDcFpgaBaseBufferLoad()
//!   * pxiFpgaBaseBufferLoad()
//!   * rcFpgaBaseBufferLoad()
//!   * tdbFpgaBaseBufferLoad()
//!   * cypSpiFpgaBaseBufferLoad() (for debugging not general use)
//! * Added Front Panel BLT APIs and supporting EEPROM read/write APIs.
//!  * fpBltBoardRead()
//!  * fpBltBoardWrite()
//!  * fpEepromRead() - read interface to EEPROM where BLT is stored.
//!  * fpEepromWrite() - write interface to EEPROM where BLT is stored.
//!
//! \par Version 1.3
//! * Added first-level TDAU Bank APIs (FPGA access and device loading).
//!  * tdbConnect() - presence check for a TDAU Bank in a slot.
//!  * tdbDisconnect() - free cached resources from using \c tdbXxxxx functions.
//!  * tdbCpldVersion()
//!  * tdbCpldVersionString()
//!  * tdbFpgaVersion()
//!  * tdbFpgaVersionString()
//!  * tdbDeviceDisable() - disable device driver (required to load FPGA).
//!  * tdbDeviceEnable()
//!  * tdbFpgaLoad() - update FPGA "on-the-fly".
//!  * tdbFpgaBaseLoad() - update boot EEPROM for FPGA.
//!  * tdbJtagExecute() - perform JTAG operations, including loading the CPLD image.
//!  * tdbBarRead() - access FPGA registers.
//!  * tdbBarWrite()
//!  * tdbBltInstrumentRead()
//!  * tdbBltInstrumentWrite()
//!  * tdbBltBoardRead()
//!  * tdbBltBoardWrite()
//!  * tdbBltEepromRead() - raw dump of %BLT EEPROM.
//! * Added new RC APIs to support implementing a HIL-based safety system.
//!  * rcInit() - Call once before before the \c rcXxxxx APIs below.
//!  * rcAd5064Write() - write to write-only DAC.
//!  * rcAdt7411Read() - read voltage monitor registers.
//!  * rcAdt7411Write() - write voltage monitor registers.
//!  * rcVmonRead() - reads ADT7411 voltage monitors.
//!  * rcTmonRead() - reads ADT7411 and FPGA temperature monitors.
//!  * rcFailsafeDisable() - disable the failsafe timer.
//!  * rcFailsafeTimerSet() - set failsafe timeout from 0 - 1023 seconds.  If it counts to zero system will turn off.
//!  * rcFailsafePing() - send a keep-alive for the failsafe timer.  Resets the timer to last rcFailsafeTimerSet() value.
//! * Added new Power Supply and additional Backplane APIs to support a HIL-based safety system.
//!  * psConnect() - power supply present detect (primary and secondary)
//!  * psDisconnect() - frees any cached resources from using \c psXxxxx functions.
//!  * bpVmonRead() - 27 voltages monitored on 3 ADT7411s.
//!  * bpAdt7411Read() - chip-level register access.
//!  * bpAdt7411Write() - chip-level register access.
//!  * psExtBmr454Read() - chip-level read access to read PMBus command data.
//!  * psExtBmr454Write() - chip-level write access to send PMBus commands.
//!  * psD1uRead() - power supply status, fault, voltage and current information.
//! * Updated hil_utils.rcClocksInit() programming to match HAL.
//! * Updated help for rcLmk04808Xxxxx() functions.
//!
//! \par Version 1.2
//! * Implemented bpBmr454Write() and bpBmr454Read() APIs.
//! * Set the over current limits of the backplane's BMR454 in bpTiuAuxPowerEnable().
//! * Updated bpTiuAuxPowerEnable() and bpTiuAuxPowerState() help documentation.
//! * Front panel APIs added.
//!  * fpInit() - required before other \c fpXxxxx() calls.
//!  * fpFanRead() - Reads the revs/sec for four fans.
//!  * fpHumidityRead() - Reads the relative humidity from the SHT21.
//!  * fpDewpointRead() - Computes the dewpoint from the humidity and air temperature.
//!  * fpTmonRead() - Reads the pre-chilled water and air temperatures.
//!  * fpMax6651Read()
//!  * fpMax6651Write()
//!  * fpAdt7411Read()
//!  * fpAdt7411Write()
//!  * fpPca9555Read()
//!  * fpPca9555Write()
//!  * fpSht21Execute()
//! * Improved rcLmk04808Write() execution time by performing chip select assert/de-assert and data write
//!   in a single FTDI FT_Write() operation and caching the FTDI handle after the first use of write.
//!   Release the handle by calling rcDisconnect().
//! * cypI2cRegisterRead() API added.
//!  * This is a low-level API to support I2C devices that require write/read without an intervening I2C stop.
//! * \c RcI2cRootXxxxx() APIs added.  These functions are multi-process safe.  Locking the root I2C bus restricts use
//!   of the bus to the calling thread until rcRootI2cUnlock() is called.  Note that the individual \c rcRootI2cXxxxx()
//!   functions will hold the lock internally, so calling rcRootI2cLock() and rcRootI2cUnlock() is only required
//!   to specify a timeout to wait for the lock or to perform multiple operations without releasing the lock.
//!  * rcRootI2cLock()
//!  * rcRootI2cUnlock()
//!  * rcRootI2cMuxSelect() - directs write/read operations to one of four I2C busses (backplane, TIU, front panel, RC).
//!  * rcRootI2cWrite()
//!  * rcRootI2cRead()
//!  * rcRootI2cRegisterRead() - Use when devices requires write/read without intervening I2C stop.
//!  * rcRootI2cInit() - Used to initialize the \c rcRootI2cXxxxxx() functions with a timeout.  Normally this function
//!                      is called internally with infinite timeout the first time the bus is locked.
//!  * rcRootI2cCacheInvalidate() - MUXs and switches on the I2C bus have their position cached to reduce the I2C
//!                                 transaction times.  This cache is multi-process safe, but if software
//!                                 bypasses the safe HIL functions this function will flush the cache.
//! * Added new #HIL_STATUS I2C error codes:
//!  * #HS_I2C_TIMEOUT
//!  * #HS_I2C_NOACK
//!  * #HS_I2C_BUS_ERROR
//!  * #HS_I2C_NOT_READY
//! * Added bpInit(), bpPca9555Write(), and bpPca9555Read() APIs.
//! * Implemented bpRootI2cSwitchSelect() API.
//!  * This API caches the switch state to minimize I2C bus traffic with the switch is already in the requested state.
//!  * rcRootI2cCacheInvalidate() flushes the cache.
//!  * Added #BP_ROOT_I2C_CHANNELS enumeration to describe the switch channels.
//! * Added PRB_FLASH_MEM_DOMAIN bit to the PXI_RESET_BITS enumeration used by pxiFpgaReset().
//! * Added note to hilSimEnable() that, if used, must be the first API called after importing HIL.
//!
//! \par Version 1.1
//! * Updated PXI APIs to support the MAX6642 temperature sensors on PXI RF Fab D.
//!   * Requires HDMT_PXI.SYS driver dated 5/26/2016 or later.
//!   * New temperature monitor channels.
//!     * Channel 4 is the internal MAX6642 temperature.
//!     * Channel 5 is the external MAX6642 temperature wired to PEX8724 PCI bridge thermal diode.
//!   * The following APIs support the new temperature channels:
//!     * pxiTmonRead()
//!     * pxiTmonAlarmLimitsWrite()
//!     * pxiTmonAlarmLimitsRead()
//!     * pxiTmonAlarmEnable()
//!   * pxiAlarmWait() reports alarms on the new channels.
//! * Added PXI APIs to support the S70FL01GS flash device on PXI RF Fab D.
//!   * pxiFlashBulkErase()
//!   * pxiFlashSectorErase()
//!   * pxiFlashProgram()
//!   * pxiFlashRead()
//! * Added PXI Legacy Trigger APIs for configuring, triggering, and reading legacy triggers.
//!   * pxiLegacyTriggerModeWrite()
//!   * pxiLegacyTriggerModeRead()
//!   * pxiLegacyTriggerWrite()
//!   * pxiLegacyTriggerRead()
//! * Added low-level pxiS70fl01gsExecute() API for debugging the new flash device.  Prefer the \c pxiFlashXxxxx() functions.
//! * Added low-level pxiMax6642Read() and pxiMax6642Write() APIs for debugging the MAX6642.  Prefer the \c pxiTmonXxxxx() functions.
//!
//! \par Version 1.0
//! * Many APIs and constants were renamed to follow a consistent naming convention.
//!   * 309 items were renamed.  See \ref renames.
//!   * A helper script, hil_rename.py, is provided to detect and rename all old names to new.
//!     * Usage: hil_rename.py \<filename_or_filename_pattern\>...
//!     * Example: hil_rename.py *.cpp *.h
//!     * The script warns when it sees the functions below that changed behavior.
//!     * The complete list of renamed symbols can also be read in the source for hil_rename.py.
//!   * General HIL constants now start with HIL_.
//!   * APIs generally follow a prefixNounVerb() naming convention.
//!   * Enumeration constants are prefixed by an acronym of the enumeration name to prevent name collisions.
//!   * Constants and enumerations were moved from XxxxApi.h files to XxxxDefs.h files.
//!     * This improved the layout and readability of the HIL.chm help file.
//!   * calHddpsRelayWrite() renamed to calHddpsGpioWrite().
//!     * **BEHAVIOR CHANGE**: parameters are now GPIOs and not relays, and the relay "onoff"
//!       parameter is now GPIO "state".  Relays were active low, so on(1) drove GPIO low(0).
//!       Now state is actual logic state of GPIO: high(1) or low(0).
//!   * calHddpsRelayRead() renamed to calHddpsGpioRead().
//!     * **BEHAVIOR CHANGE**: parameters are now GPIOs and not relays, and the relay "pOnoff"
//!       value returned is now GPIO "pState".  Relays were active low, so returning 1 meant the relay was on, but GPIO was low.
//!       Now pState returns the actual logic state of the GPIO: high(1) or low(0).
//!   * calBaseRelayWrite() renamed to tiuCalBaseGpioWrite().
//!     * **BEHAVIOR CHANGE**: parameters are now GPIOs and not relays, and the relay "onoff"
//!       parameter is now GPIO "state".  Relays were active low, so on(1) drove GPIO low(0).
//!       Now state is actual logic state of GPIO: high(1) or low(0).
//!   * calBaseRelayRead() renamed to tiuCalBaseGpioRead().
//!     * **BEHAVIOR CHANGE**: parameters are now GPIOs and not relays, and the relay "pOnoff"
//!       value returned is now GPIO "pState".  Relays were active low, so returning 1 meant the relay was on, but GPIO was low.
//!       Now pState returns the actual logic state of the GPIO: high(1) or low(0).
//! * All PXI ETest APIs have been removed from HIL due to project cancellation.
//! * Added pciUserBarIsEnabled() API.
//! @see
//!  @subpage renames\n
//!  @subpage older\n
//!
//! @page older Older Release Notes
//! \par Version 0.17.1
//! * Internal use of Win32 \c SetupDiXxxxx APIs required common mutex protection with HALs use of the same APIs to fix multithreaded race conditions.
//!   This stabilizes many APIs in a multithreaded environment when used in HAL, including but not limited to:
//!   * pciOpenDevice()
//!   * pciEnableDevice()
//!   * pciDisableDevice()
//!   * \c xxxCpldLoad() functions.
//!   * \c xxxFpgaLoad() functions.
//!
//! \par Version 0.17
//! * The SPI interfaces controlled by the FTDI parts on the PXI calibration card were slowed down to 5MHz.
//!   This was done due to signal integrity issues on Fab A that caused intermittent failures when running at higher speeds.
//! * Added calPxiBltInstrumentRead() and calPxiBltInstrumentWrite() APIs.
//! * Added PXI calibration card EEPROM and FLASH APIs:
//!   * calPxiEepromWrite()
//!   * calPxiEepromRead()
//!   * calPxiFlashBulkErase()
//!   * calPxiFlashSectorErase()
//!   * calPxiFlashProgram()
//!   * calPxiFlashRead()
//!
//! \par Version 0.16
//! * Added hpccAcFpgaBaseLoad() API.
//!   * This new HIL function takes approximately 3 minutes 20 seconds to program one HPCC AC daughterboard base EEPROM.
//!   * This is down from 45 minutes from existing standalone tool.
//! * Added RF calibration board GPIO APIs:
//!   * calPxiGpioRead()
//!   * calPxiGpioWrite()
//!   * The CAL_PXI_GPIO enumeration lists the GPIO signal names.
//! * Added HPCC calibration snap-in card BLT APIs:
//!   * calHpccSnicBltBoardRead()
//!   * calHpccSnicBltBoardWrite()
//!   * calHpccSnicBltEepromRead()
//! * Added HDDPS CAL BLT APIs:
//!   * calHddpsBltBoardRead()
//!   * calHddpsBltBoardWrite()
//!   * calHddpsBltEepromRead()
//! * Added pciUserBarEnable() API.
//!   * This allows faster PCI register access by direct mapping PCI register memory.
//!   * User-mapped mode is enabled by default, but pciUserBarEnable(FALSE) can disable it for debugging.
//! * Updated HIL_BASE_LOAD_CALLBACK documentation to show examples of using callbacks to get long-running EEPROM base programming to report percent complete status.
//!
//! \par Version 0.15
//! * rcTriggerWait() API changed to support 32-bit triggers.
//!   * **Breaking change**: Now optionally returns two DWORD values instead of one to support more information.
//! * Added RC TIU Force Safety APIs.
//!   * rcTiuAlarmEnable()
//!   * rcAlarmWait()
//!   * rcAlarmWaitCancel()
//! * **Breaking change**: Removed hpccAcGetCpldFpgaStatus() API.
//! * Updated hpccAcGetCpldVersion() API:
//!   * **Breaking change**: CPLD version implementation changed.
//!     * Returns 32-bit JTAG USERCODE register for version.
//!     * Old custom 8-bit version implementation removed.
//! * Updated hpccDcGetCpldVersion() API:
//!   * **Breaking change**: CPLD version implementation changed.
//!     * Returns 32-bit JTAG USERCODE register for version.
//!     * Old custom 8-bit version implementation removed.
//! * Added CPLD version string APIs.  The existing APIs such as rcGetCpldVersion() return a DWORD representing four ASCII characters.  These APIs decode the
//!   DWORD and return the ASCII characters directly for easier human readability.
//!   * hpccAcGetCpldVersionString()
//!   * hpccDcGetCpldVersionString()
//!   * hddpsGetCpldVersionString()
//!   * pxiGetCpldVersionString()
//!   * calHpccGetCpldVersionString()
//!   * rcGetCpldVersionString()
//!   * calBaseGetCpldVersionString()
//! * Added HDDPS calibration card APIs:
//!   * calHddpsInit()
//!   * calHddpsConnect()
//!   * calHddpsDisconnect()
//!   * calHddpsPca9505Init()
//!   * calHddpsPca9505Write()
//!   * calHddpsPca9505Read()
//!   * calHddpsRelaysReset()
//!   * calHddpsRelayWrite()
//!   * calHddpsRelayRead()
//! * Fab E+ site controllers change USB hardware paths and HIL requires the site controller #BLT \c PartNumberCurrent
//!   field to be programmed correctly.  New HIL error codes are returned to make it clear where the fault lies
//!   for APIs that need to determine correct USB paths.
//!   * #HS_SC_BLT_NOT_FOUND
//!   * #HS_SC_BLT_INVALID
//!   * #HS_SC_BLT_CRC_CHECK
//! * **Breaking change**: Non-DWORD-aligned BAR offsets in BAR reading and writing functions now return #HS_ALIGNMENT_VIOLATION instead of #HS_UNSUCCESSFUL.
//! * Fixed Cypress handle leak in hddpsGetCpldVersion() and pxiGetCpldVersion().
//!
//! \par Version 0.14
//! * Added five more CPLD version-reading APIs.  Please note that the CPLDs need to be re-released with a version number correctly advertised in their JTAG USERCODE register.  Default values for legacy CPLDs listed below:
//!   * rcGetCpldVersion() - default 0x636f6e66 (that is 'conf' in ASCII)
//!   * pxiGetCpldVersion() - default 0x636f6e66 (that is 'conf' in ASCII)
//!   * hddpsGetCpldVersion() - default 0x636f6e66 (that is 'conf' in ASCII)
//!   * calBaseGetCpldVersion() - default 0x68706363 (that is 'hpcc' in ASCII)
//!   * calHpccGetCpldVersion() - default 0x68706363 (that is 'hpcc' in ASCII)
//! * Bug fixes:
//!   * For devices controlled by FTDI ports in the PXI calibration board, the chip select line is now correctly de-asserted after a failure.
//!   * Fixed a DMA race condition that occurred when two threads in the same process shared a handle to the same device and performed DMA in parallel on that device.
//! * Improvements:
//!   * PXI calibration board SPI bus speeds increased.
//!
//! \par Version 0.13
//! * Completed remaining core JTAG support.  This supports CPLD programming and verification.
//!   * Added calBaseJtagExecute() API.
//!   * Added hddpsJtagExecute() API.
//!   * Added pxiJtagExecute() API.
//! * Completed remaining low-level device APIs for the Analog RF PXI calibration board.
//!   * Added calPxiS70fl01gsCommandExecute() API.
//!   * Added calPxiM95m02CommandExecute() API.
//!
//! \par Version 0.12
//! * Implemented rcJtagExecute() API to support programming the CPLD.  **Note**: the -402 resource card has had the JTAG interface disconnected.
//! * Basic APIs for the HPCC calibration card are now supported:
//!   * calHpccConnect() and calHpccDisconnect().
//!   * calHpccBltBoardRead() and calHpccBltBoardWrite().
//!   * calHpccBltEepromRead() allows raw EEPROM dumps to determine BLT data offsets for ATE programming.
//!   * calHpccJtagExecute() allows programming the CPLD.  The JTAG instructions are generated through Xilinx's Impact software as an XSVF file.
//! * Implemented ftdiMpsseGpioHighWrite() and ftdiMpsseGpioHighRead() to support 16-bit ports such as FT2232H.  These read the high 8-bits of the port to complement the existing
//!   ftdiMpsseGpioRead() and ftdiMpsseGpioWrite() that read the lower 8-bits.
//! * Additional support for the PXI calibration board:
//!   * Implemented the calPxiMax6651Write() and calPxiMax6651Read() APIs.
//!   * Implemented the calPxiMax6627Read() API.  This is a read-only device.
//!   * Implemented the calPxiAd5660Write() API.  This is a write-only device.
//!
//! \par Version 0.11
//! * Implemented hpccAcGetCpldFpgaStatus().  Requires CPLD version 4.2 or later.  Returns HS_UNSUPPORTED for older CPLDs.
//! * hpccAcGetCpldVersion() updated to support backward-compatible CPLD versioning.  Requires CPLD version 4.2 or later.  Returns HS_UNSUPPORTED for older CPLDs.
//! * Implemented hpccAcBltEepromRead(), hpccDcBltEepromRead(), hddpsBltEepromRead(), pxiBltEepromRead(), calPxiBltEepromRead(), and scBltEepromRead().
//! * Added the pxiStarOutModeWrite() and pxiStarOutModeRead() APIs.
//! * Added a hexdump() function to hil_utils.py.  It formats a text hexdump of binary data to the terminal or a file.
//! * Implemented support for PXI Fab D.  NOTE: APIs continue to support pre-Fab D; however, the API parameters have **breaking changes**:
//!   * enum PXI_RESET_BITS value PRB_MAX_6651_DOMAIN changed to PRB_MAX_665X_DOMAIN.
//!   * enum PXI_ALARMS split out into PXI_VT_ALARAMS and PXI_FAN_ALARMS.  Constants in enums prefixed with PVTA_ and PFA_.  There are more than 32 alarms types now.
//!   * pxiAlarmWait(INT slot, LPDWORD pAlarms) changed to pxiAlarmWait(INT slot, LPDWORD pVoltTempAlarms, LPDWORD pFanAlarms).
//!   * For PXI Fab D new alarms, HDMT_PXI driver dated 12/3/2015 is **required**.
//!   * pxiMax6651Read() and pxiMax6651Write() renamed to pxiMax665xRead() and pxiMax665xWrite().  They now support a \c chip parameter from 0-8.  Pre-fab D is limited to 0-2.
//!   * pxiFanRead() now returns INVALID_PARAMETER for fan channels 1-3 on PXI fab D.
//!   * pxiFanAlarmEnable() now takes a zero-based fan channel number.  Only 0, 4, and 8 are valid on pre-Fab D as secondary fans on a Max6651 cannot be monitored.  0, 4-11 are valid on fab D.
//!   * pxiFanModeSet() now takes a zero-based fan channel number.  Same restrictions as pxiFanAlarmEnable() for channel.
//! * Updated pxiTriggerPulseWidthWrite() and pxiTriggerPulseWidthRead() to allow both Star Out and Star In trigger pulse widths to be written and read.
//! * Updated pxiTriggerEnablesWrite() and pxiTriggerEnablesRead() to allow Star In triggers to be enabled and disabled.
//! * BLT Read/Write now validates CRC only on the record being accessed. Invalid CRCs in non-accessed records are maintained. This came about due to Gen1 BLTs with invalid CRC present in new boards.
//! * Added copyright notices to public headers.
//!
//! \par Version 0.10
//! * This release was validated on both Windows 7 and Windows 8.1.
//! * HIL now supports the USB change in the fab E and F site controllers and continues to older fabs as well.
//!   **NOTE**: The site controller's BLT's \c PartNumberCurrent field's dash number is used to determine the fab.
//!   **Ensure your BLT is programmed with the correct part number!**
//! * Added initial calibration base TIU APIs:
//!   * calBaseConnect() and calBaseDisconnect().
//!   * calBaseInit().
//!   * calBaseRelayWrite() and calBaseRelayRead().
//!   * calBasePca9505Write() and calBasePca9505Read().
//! * Added additional PXI calibration card APIs:
//!   * calPxiAd9914Write() and calPxiAd9914Read().
//!   * calPxiLmk04808Write() and calPxiLmk04808Read().
//!   * calPxiLmk01000Write().  Note this is a write-only device.
//!   * calPxiBltBoardRead() and calPxiBltBoardWrite().
//! * In the Python hil.py import library, a helper function was added to list HIL functions and constants
//!   that contain a string.  The syntax is: \c hil.cmds(string)
//! * Added #HS_BLT_INVALID return code.  This currently will only be returned by calPxiConnect() and calPxiInit()
//!   if the site controller's BLT is present but the PartNumberCurrent field is not recognizable to determine
//!   the fab.
//!
//! \par Version 0.9
//! * hpccDcGetCpldVersion() and hpccAcGetCpldVersion() added to support reading the CPLD version from HPCC DC motherboards and AC daughterboards.
//! * hddpsDmaRead() and hddpsDmaWrite() updated to use the HDDPS DMA engine.  Note this *requires* HDDPS firmware
//!   with DMA support (version not yet finalized) and the HDMT_HDDPS driver dated 10/15/2015 or later.
//!   Older firmware has the DMA engine logic present, but is not yet interfaced to the HDDPS memory.  These
//!   functions will return HS_TIMEOUT when used on older firmware.  The former versions of these function that
//!   used the slower HDDPS FPGA register-based memory access method were renamed to hddpsMemRead() and hddpsMemWrite().
//! * hddpsMemRead() and hddpsMemWrite() added to support the old HDDPS FPGA register-based memory access method.
//!   The functionality was retained to aid in porting code to the new DMA methods.
//! * Added a Windows version resource to HIL.DLL. Right-clicking it in Windows Explorer and viewing the Properties, Details
//!   tab will now show the DLL version.  The existing hilGetVersion() can read the version through software.
//! * Added the following PXI calibration card APIs:
//!   * calPxiConnect() and calPxiDisconnect().
//!   * calPxiInit() and calPxiFtdiInit().
//!   * calPxiPca9506Write() and calPxiPca9506Read().
//!   * calPxiAd5791Write() and calPxiAd5791Read().
//!   * calPxiAd9838Write() - Note this device is write-only.
//!   * calPxiFtdiPinsWrite() and calPxiFtdiPinsRead().
//!
//! \par Version 0.8
//! * The following RcApi functions have been renamed to follow HIL API conventions. This is a **breaking change**:
//!   * From rcEncodePlutoInstruction() to rcPlutoInstructionEncode().
//!   * From rcEnableTriggers() to rcTriggerEnable().
//!   * From rcDisableTriggers() to rcTriggerDisable().
//!   * From rcWaitForTrigger() to rcTriggerWait().
//!   * From rcCancelTrigger() to rcTriggerWaitCancel().
//! * New functions to support trigger bus initialization and PXI triggers:
//!   * rcTriggerBusCoreReset()
//!   * rcTriggerBusTrxReset()
//!   * rcTriggerBusStatus()
//!   * rcTriggerSend()
//!   * rcTriggerSyncSend()
//!   * pxiFanModeSet()
//!   * pxiTriggerBusCoreReset()
//!   * pxiTriggerBusTrxReset()
//!   * pxiTriggerBusStatus()
//!   * pxiTriggerEnablesWrite()
//!   * pxiTriggerEnablesRead()
//!   * pxiTriggerPulseWidthWrite()
//!   * pxiTriggerPulseWidthRead()
//!   * pxiTriggerEncode()
//!   * pxiDstarbModeWrite()
//!   * pxiDstarbModeRead()
//!   * pxiDstarbClockEnablesWrite()
//!   * pxiDstarbClockEnablesRead()
//!   * pxiDstarbStatus()
//! * PXI fan support has been refactored to support controllers running at different fan RPMs. This is a **breaking change**.
//!   * pxiInit() now initializes fan controllers to full on mode.
//!   * pxiFanModeSet() has been added to control fan modes and initialize fan speed.  It supports off, full on, and closed loop modes.  Fan speed is initialized in revolutions per minute (RPM) per controller.
//!   * pxiFanAlarmEnable() help descriptions updated to document that closed loop mode is required for fan alarms to be reported.
//!   * **Note that this release does not support PXI FPGA 0.7 or PXI mainboard Fab D with the 6550 controllers**.  It has been tested with PXI FPGAs 0.6 and 0.8.
//!   * See pxiInit() for a PXI fan initialization example.
//! * Added rcOutOfProcessTriggerWait() and pxiOutOfProcessAlarmWait() Python helper functions to hil_utils.py.  Waiting for alarms
//!   is a blocking operation so these helpers check for alarms in another process.  Note that if an out-of-process wait
//!   times out the process can be signaled to exit with the appropriate rcTriggerWaitCancel() or pxiAlarmWaitCancel() in the main process.
//!
//! \par Version 0.7.8
//! * Added initial ETest APIs. See ETestApi.h for details.
//! * hpccAcFpgaLoad() now correctly checks for FPGA done bit.  It returns HS_TIMEOUT if done is not detected.
//! * Fixed a case sensitivity issue due to a change in Windows 8.1 vs. Windows 7.
//! * rcLmk04808FullWrite() added to optimizing programming multiple clock registers.
//! * Created a new hil_utils.py wrapper function to use rcLmk04808FullWrite().
//! * Updated register settings for clock initialization in hil_utils.py to align with what is being programmed by HAL.
//!
//! \par Version 0.7.7
//! * pxiBltInstrumentRead() and pxiBltInstrumentWrite() board-level traceability APIs added.
//! * pxiBltBoardRead() and pxiBltBoardWrite() board-level traceability APIs added.
//! * pxiBltRiserRead() and pxiBltRiserWrite() board-level traceability APIs added.
//! * hpccBltInstrumentRead() and hpccBltInstrumentWrite() board-level traceability APIs added.
//! * hpccAcBltBoardRead() and hpccAcBltBoardWrite() board-level traceability APIs added.
//! * hpccDcBltBoardRead() and hpccDcBltBoardWrite() board-level traceability APIs added.
//! * hddpsBltInstrumentRead() and hddpsBltInstrumentWrite() board-level traceability APIs added.
//! * hddpsBltBoardRead() and hddpsBltBoardWrite() board-level traceability APIs added.
//! * scBltBoardRead() and scBltBoardWrite() board-level traceability APIs added.
//!
//! \par Version 0.7.6
//! * hddpsDmaRead() and hddpsDmaWrite() APIs implemented using indirect FPGA registers until DMA is supported.
//! * hpccDcJtagExecute() and hpccAcJtagExecute() APIs added.  These APIs will execute an .XSVF file over the selected JTAG interface.
//!   .XSVF files are generated by Xilinx's iMPACT software.  Operations such as CPLD image load and scan chain verification can be
//!   directed to an .XSVF file instead of over a JTAG cable via iMPACT and later played to a JTAG interface using these APIs, removing
//!   the need for a Xilinx programming cable.
//! * pxiDeviceDisable() and pxiDeviceEnable() APIs have been separated from pxiFpgaLoad().  This is a **breaking change**.
//!   Existing code should wrap pxiFpgaLoad() in these new calls.  Separating the functionality allows more intelligent algorithms
//!   for parallel processing, as the APIs used to implement driver enable/disable have had issues with parallelism.
//! * rcDeviceDisable() and rcDeviceEnable() APIs have been separated from rcFpgaLoad().  This is a **breaking change**.
//!   Existing code should wrap rcFpgaLoad() in these new calls.  Separating the functionality allows more intelligent algorithms
//!   for parallel processing, as the APIs used to implement driver enable/disable have had issues with parallelism.
//! * hpccAcDeviceDisable() and hpccAcDeviceEnable() APIs have been separated from hpccAcFpgaLoad().  This is a **breaking change**.
//!   Existing code should wrap hpccAcFpgaLoad() in these new calls.  Separating the functionality allows more intelligent algorithms
//!   for parallel processing, as the APIs used to implement driver enable/disable have had issues with parallelism.
//! * hpccDcDeviceDisable() and hpccDcDeviceEnable() APIs have been separated from hpccDcFpgaLoad().  This is a **breaking change**.
//!   Existing code should wrap hpccDcFpgaLoad() in these new calls.  Separating the functionality allows more intelligent algorithms
//!   for parallel processing, as the APIs used to implement driver enable/disable have had issues with parallelism.
//! * hddpsDeviceDisable() and hddpsDeviceEnable() APIs have been separated from hddpsFpgaLoad().  This is a **breaking change**.
//!   Existing code should wrap hddpsFpgaLoad() in these new calls.  Separating the functionality allows more intelligent algorithms
//!   for parallel processing, as the APIs used to implement driver enable/disable have had issues with parallelism.
//!
//! \par Version 0.7.5
//! \li Added Backplane TIU Aux Power API: bpTiuAuxPowerEnable() and bpTiuAuxPowerState()
//! \li Added RC clock chip read/write API: rcLmk04808Read() and rcLmk04808Write()
//! \li Initial release of the hil_utils.py script.  The script includes rcClocksInit() which uses
//!     the rcLmk04808Write() API.
//! \li Updated pciOpenDevice() to match any device whose device ID string starts with the specified
//!     VID/DID rather than matches it exactly.  This enables support for opening 3rd party PCI devices.
//!
//! \par Version 0.7.4
//! \li Added FTDI I2C API: ftdiMpsseI2cInit(), ftdiMpsseI2cWrite(), ftdiMpsseI2cRead(), ftdiMpsseI2cWriteRead()
//! \li Added FTDI User EEPROM API: ftdiUserEepromSize(), ftdiUserEepromWrite(), ftdiUserEepromRead()
//! \li Fixed all FTDI API functions to be exported as "C" functions.  Note that in prior releases they were incorrectly
//!     exported as "C++" functions.
//!
//! \par Version 0.7.3
//! \li Serialized ftdiOpenByLocation() to fix issues with using hpccAcFpgaLoad() in parallel threads and processes.
//!
//! \par Version 0.7.2
//! \li Renamed \c xxxLoadFpga() and \c xxxLoadFpgaBase() APIs to \c xxxFpgaLoad() and \c xxxFpgaBaseLoad().
//! \li Added optional callbacks to the long \c xxxFpgaBaseLoad() EEPROM programming functions to provide percent done feedback.  See \ref pyapi for a Python example.
//! \li New HPCC core APIs for FPGA load, BAR read/write and DMA read/write.  See HpccAcApi.h and HpccDcApi.h.
//! \li pciOpenDevice() now takes an index if there are multiple devices that match the search.  This was required for handling the dual FPGAs on HPCCs.
//! \li cypProgramAndVerifySpi() implements a callback.  It is the lower-level function enabling FPGA base loads.
//! \li New FTDI low-level APIs to enabling implementing instrument APIs utilizing FTDI parts for device communication.  See FtdiApi.h.
//!
//! \par Version 0.7.0.4
//! \li Added PXI interrupt support via pxiAlarmWait().  See PxiApi.h for details.
//! \li Tested with PXI FPGA 0.4.
//!
//! \par Version 0.7.0.3
//! \li Added Voltage monitor limits to PXI API.  See PxiApi.h for details.
//! \li Tested with PXI FPGA 0.4.
//!
//! \par Version 0.7.0.2
//! \li Added Temperature monitor capability to PXI API.  See PxiApi.h for details.
//! \li Tested with PXI FPGA 0.4.
//!
//! \par Version 0.7.0.1
//! \li Added MAX6651 capability to PXI API.  See PxiApi.h for details.
//! \li Tested with PXI FPGA 0.2.
//!
//! \par Version 0.7
//! \li Scaled voltage for 5V and 12V channels on pxiVmonRead() fixed. Schematic comment
//!     is .1 divider, but actual divider is 1/11th with 4.7K and 47K resistors.
//! \li Removed \c pxiVmonTempRead() API function and allowed the temperature to be read
//!     back using channel 0 of pxiTmonRead() which is the common temperature read back
//!     function.
//! \li Extensively refactored functions to follow a \c brdNounVerb() naming convention.
//! \li The functions to read register now take a base address register (BAR) parameter instead
//!     of having the BAR in the function name.
//! \li Changed the name of \c pciBarDwordsRead() and \c pciBarDwordsWrite() to pciBarRead() and pciBarWrite()
//!     and only allowed single DWORD reads and writes.  This removed unneeded complexity since
//!     there has never been a need to read/write multiple DWORDs at a time.
//!
//! \par Version 0.6
//! \li Initial release.
//! \li Expanded PXI support.  See PxiApi.h for details.
//! \li Tested with PXI FPGA 0.1.
//!
//! \par Version 0.5
//! \li Initial implementation.
//! \li BAR read/write across RC, PXI, HDDPS.
//! \li DMA read/write of RC instruction memory.
//! \li Simple simulation capability to enable unit testing.
//! \li Ability to dynamically load FPGAs for RC, PXI, HDDPS.
//! \li Ability to load base EEPROM for RC, PXI, HDDPS FPGAs.
//! \li Lower-level PCI and Cypress functions for raw access to other devices.

#pragma once
#include "HilDefs.h"

#ifdef __cplusplus
extern "C" {
#endif

//! @brief Returns the current version of the DLL's API as a number.
//!
//! This function retrieves the version number of the HIL APIs as an unsigned 32-bit value
//! for use in comparisons.  If displayed, use hilVersionString().
//! @return Version number in 0xMMnnssrr (Major,minor,sub-minor,revision) format.
HIL_API DWORD hilVersion(void);

//! @brief Returns the current version of the DLL's API as a string.
//!
//! This function retrieves the version number of the HIL APIs as a string formatted
//! as two to four decimal numbers separated by periods.  The values represent major, minor,
//! sub-minor and revision.  Sub-minor and revision are not displayed if both are zero.
//! Revision is not displayed if it is zero.
//!
//! @par Examples
//! 0x01020304 => "1.2.3.4"<BR>
//! 0x010a0b00 => "1.10.11"<BR>
//! 0x01030000 => "1.3"<BR>
//! @param[out] pVersion String buffer for returned version.  Cannot be NULL.
//! @param[in] length Number of characters in the buffer.
//! @retval HS_SUCCESS The version was returned successfully.
//! @retval HS_INSUFFICIENT_BUFFER \c length of \c pVersion buffer was insufficient to hold result.
HIL_API HIL_STATUS hilVersionString(_Out_writes_z_(length) LPSTR pVersion, _In_ DWORD length);

//! @brief Returns a text description of a HIL status code.
//!
//! This function returns a text description of a HIL status code.
//! @param[in] status The status code in question.
//! @return A text description of the status code.
HIL_API LPCSTR hilStatusDescription(_In_ HIL_STATUS status);

//! @brief Prepend slot number to HDMT device descriptions in Device Manager.
//!
//! This function prepends the slot number to device descriptions in the "HDMT PCIe Devices" and
//! "HDMT USB Devices" classes of Device Manager.  The descriptions do not update immediately.
//! Open Device Manager, right-click any node and select "Scan for hardware changes".
//! @note TOS updates some descriptions with more information that this generic function provides.  For example,
//!       there are two HPCC daughterboard FPGAs but the description provided by this function will not differentiate between them.
//! @note Recommended operation is to call this function with mode 1 to set all descriptions, then customize descriptions if desired.  If
//!       more devices become available, call this function with mode 2 to update the new devices without resetting the customized descriptions.
//! @param[in] mode The following values are used to control the prepending of slot numbers.
//!                 | Value | Description                                                           |
//!                 | :---: | :-------------------------------------------------------------------- |
//!                 |   0   | Reset to default device descriptions as specified by device driver.   |
//!                 |   1   | Prepend slot numbers to HDMT devices that have slots.                 |
//!                 |   2   | Prepend slot numbers only to HDMT devices that have not been updated. |
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hilDeviceManagerSlotUpdate(_In_ INT mode);

//! @brief Returns slot information based on the LocationPath.
//!
//! This function returns slot information based on the LocationPath obtained from a device.  This value can
//! be found in Device Manager, Details tab.  Obtaining it programmatically is beyond the scope of this
//! help file, but see the Win32 APIs \c SetupDiGetClassDevs, \c SetupDiEnumDeviceInfo, and \c SetupDiGetDeviceProperty.
//! The property needed is \c DEVPKEY_Device_LocationPaths.
//! @param[in] locationPath LocationPath string.
//! @returns The slot number of the device or -1 if it is not considered a slotted device in HDMT or HBI testers.
HIL_API INT hilLocationPathSlot(_In_z_ LPCWSTR locationPath);

//! @brief Returns the detected tester type.
//!
//! This function returns an enumeration value indicating the tester chassis type.
//! @return \ref TESTER
HIL_API TESTER hilTester(void);

//! @brief Disables or enables the HBI/HDMT failsafe timer.
//!
//! This function disables or enables the HBI/HDMT failsafe timer.
//! @note hilFailsafePing() will also renable the timer, so starting any safety system software that pings will automatically re-enable the timer.
//! @param[in] disable \c TRUE disables the timer.  \c FALSE enables it.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hilFailsafeDisable(_In_ BOOL disable);

//! @brief Sets the failsafe timer count value.
//!
//! This function sets the failsafe timer count value.  Once a time is set hilFailsafePing() must be called
//! periodically before the timer expires or the tester will shut down.
//! @param[in] seconds The count value in seconds.  Valid values are 0-1023.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hilFailsafeTimerSet(_In_ DWORD seconds);

//! @brief Resets the failsafe timer count.
//!
//! This function resets the failsafe timer to the count set by hilFailsafeTimerSet().
//! If the timer expires the tester shuts down, so calling this function periodically keeps the tester powered up.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hilFailsafePing(void);

//! @brief Reports the time in milliseconds since hilFailsafePing() was called.
//!
//! This function reports the time since hilFailsafePing() was called.  If never called, time is relative to Windows start.
//! @note The deprecated functions rcFailsafePing(), rc3FailsafePing(), and hbiMbFailsafePing() also reset the timer.
//! @param[out] pElapsed Returned time in milliseconds.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hilFailsafePingTimeElapsed(_Out_ PULONGLONG pElapsed);

//! @brief Enables or disable simulation mode.
//!
//! This function controls enabling and disabling simulation mode.  This allows offline testing without hardware present.
//! @param[in] enable The following values are used to control the simulation mode.  Undefined values enable HDMT tester simulation.
//!                   | Value | Description                   |
//!                   | :---: | :---------------------------: |
//!                   |   0   | Disable simulation            |
//!                   |   1   | Enable HDMT tester simulation |
//!                   |   2   | Enable HBI tester simulation  |
//!
//! @note hilSimEnable() (if used) must be the first API called after importing HIL.  Calling it at other times
//!       is not supported and undefined behavior.
HIL_API void hilSimEnable(_In_ INT enable);

//! @brief Queries simulation mode.
//!
//! This function queries if HIL simulation mode is currently enabled.
//! @retval TRUE  Simulation is enabled.
//! @retval FALSE Simulation is disabled.
HIL_API BOOL hilSimIsEnabled(void);

//! @brief Enables or disable compliance mode.
//!
//! This function controls enabling and disabling compliance mode (default: disabled).
//!
//! During HIL development there were cases where HIL's implementation discovered problems that HAL/TOS allowed to escape.
//! Because the problems made it into the field, for compatibility HIL had to allow the same issues to work with field testers.
//! To be able to discover these problems in manufacturing, enabling compliance mode will make HIL fail when the problems are observed.
//! In TOS, compliance mode will be disabled to prevent failures in existing field testers.
//!
//! Currently only #BLT, SysId, and MesId read/write APIs are affected.  The EEPROM data structures storing these items have been found
//! with incorrect counts and invalid records in the field, but good records can still be found by not rigorously testing each record seen.
//! With compliance mode disabled (default), the good records can be read.  If compliance mode is enabled, the entire chain of records in
//! the BLT structures are checked and BLT calls will fail if any are invalid.
//!
//! @param[in] enable TRUE to enable, FALSE to disable.
HIL_API void hilComplianceEnable(_In_ BOOL enable);

//! @brief Queries compliance mode.
//!
//! This function queries if HIL compliance mode is currently enabled.  See hilComplianceEnable() for the mode description.
//! @retval TRUE  Compliance mode is enabled.
//! @retval FALSE Compliance mode is disabled.
HIL_API BOOL hilComplianceIsEnabled(void);

//! @brief Allows bypassing the metafile requirement for FPGA load functions that require it.
//!
//! This function allows bypassing the metafile requirement for FPGA load functions that require it by allowing \c NULL or \c nullptr
//! to be passed for the \c pMetaData parameter of those functions.  If metadata is passed, it is still used.
//! It is intended for backwards compatibility with files that have no metadata, debugging, simulation testing, and fault injection.
//! @param[in] bypass TRUE to allow bypass, FALSE to operate normally and require metadata files for FPGA loads that require it.
HIL_API void hilFpgaMetaBypass(_In_ BOOL bypass);

//! @brief Queries FPGA metafile bypass mode.
//!
//! This function queries if metafiles are required for FPGA load functions.  See hilFpgaMetaBypass() for the mode description.
//! @retval TRUE  Metafile is not required.
//! @retval FALSE Metafile is required.
HIL_API BOOL hilFpgaMetaIsBypassed(void);

//! @brief Helper function for PMBus conversion from 11-bit linear floating point format.
//!
//! This function implements the conversion from the PMBus specification's LINEAR 11-bit floating point format to a C double as stated
//! in <i>section 7.1 Linear Data Format</i> of the <i>PMBus Power System Management Protocol Specification, Part II - Command Language</i>.
//!
//! The Linear Data Format is a two-byte value with:
//! * An 11-bit, two's complement mantissa (range -1024 to 1023).
//! * A 5-bit, two's complement exponent (range -16 to 15).
//!
//! The format of the two data bytes in binary is:
//! \image html L11.png "Linear Data Format Data Bytes"
//! Note that the two bytes are transmitted little-endian.  The floating point value is computed as <code>value = Y &sdot; 2<sup>N</sup></code>.
//!
//! \par Example of PMBus READ_IOUT command - C
//! \code{.c}
//! #include <Windows.h>
//! #include <stdio.h>
//! #include "HilApi.h"
//! #include "RcApi.h"
//! #include "PowersupplyApi.h"
//!
//! /* PMBus Power System Management Protocol Specification - Part II */
//! const BYTE PAGE = 0x00;      /* read/write one byte to read or select page */
//! const BYTE READ_IOUT = 0x8C; /* two data bytes returned in L11 format for output current */
//!
//! int main()
//! {
//!     HIL_STATUS status;
//!     double voltage;
//!     INT slot = 1;
//!     BYTE data[2];
//!
//!     status = rcInit();
//!     if(status != HS_SUCCESS)
//!         return 1;
//!
//!     status = rcRootI2cLock(1000); /* Lock the bus to ensure write/read is atomic */
//!     if(status != HS_SUCCESS)
//!         return 1;
//!
//!     /* Read the main output current from a Delta supply in the secondary bay (slot=1) */
//!     data[0] = 0;                                /* Select page 0 - Main voltage */
//!     status = psPmbusWrite(slot, PAGE, data, 1); /* Send one byte */
//!     if(status != HS_SUCCESS) {
//!         rcRootI2cUnlock();
//!         return 1;
//!     }
//!
//!     status = psPmbusRead(slot, READ_IOUT, data, sizeof(data)); /* read two bytes */
//!     if(status != HS_SUCCESS) {
//!         rcRootI2cUnlock();
//!         return 1;
//!     }
//!
//!     rcRootI2cUnlock();
//!
//!     current = hilFromL11(data[0], data[1]);    /* convert from L11 to current */
//!     printf("READ_IOUT = %.3lf A\n", current);
//!     return 0;
//! }
//! \endcode
//! \par Example of PMBus READ_IOUT command - Python
//!
//! @note The Python-wrapped version of this API takes a single byte string of length 2 as
//!       returned by the Python APIs that access the PMBus.  This allows the return value of PMBus
//!       reads to be passed directly into this function, instead of an awkward
//!       <code>hil.hilFromL11(raw[0],raw[1])</code>.
//!
//! \code{.py}
//! import hil
//!
//! # PMBus Power System Management Protocol Specification - Part II
//! PAGE = 0x00;      # read / write one byte to read or select page
//! READ_IOUT = 0x8C; # two data bytes returned in L11 format for output current
//!
//! slot = 1
//!
//! hil.rcInit()
//!
//! hil.rcRootI2cLock(1000)
//! try:
//!     hil.psPmbusWrite(slot, PAGE, b'\x00');     # Send one byte, 0 = main
//!     raw = hil.psPmbusRead(slot, READ_IOUT, 2) # read two bytes
//! finally:
//!     hil.rcRootI2cUnlock();
//!
//! current = hil.hilFromL11(raw) # convert from L11 to current
//! print(f'READ_IOUT = {current:.3f} A')
//! \endcode
//! @param[in] lsb The least significant byte of the 11-bit linear floating point format.  Note bytes are returned little-endian from relevant
//!                PMBus commands so this would be the first byte returned.
//! @param[in] msb The most significant byte of the 11-bit linear floating point format.  Note bytes are returned little-endian from relevant
//!                PMBus commands so this would be the second byte returned.
//! @return The floating point result of the conversion.
HIL_API double hilFromL11(BYTE lsb, BYTE msb);

//! @brief Helper function to convert a C floating point value to the PMBus 11-bit linear floating point format.
//!
//! This function implements the conversion from a C double to the PMBus specification's LINEAR 11-bit floating point format as stated
//! in <i>section 7.1 Linear Data Format</i> of the <i>PMBus Power System Management Protocol Specification, Part II - Command Language</i>.
//!
//! The Linear Data Format is a two-byte value with:
//! * An 11-bit, two's complement mantissa (range -1024 to 1023).
//! * A 5-bit, two's complement exponent (range -16 to 15).
//!
//! The format of the two data bytes in binary is:
//! \image html L11.png "Linear Format Data Bytes"
//! Note that the exponent chosen for the conversion is the smallest value that provides a valid mantissa, which gives the highest resolution
//! for the final result.
//!
//! \par Example of PMBus IOUT_OC_FAULT_LIMIT command - C
//! \code{.c}
//! #include <Windows.h>
//! #include <stdio.h>
//! #include "HilApi.h"
//! #include "RcApi.h"
//! #include "PowersupplyApi.h"
//!
//! /* PMBus Power System Management Protocol Specification - Part II */
//! const BYTE IOUT_OC_FAULT_LIMIT = 0x46;  /* over-current fault limit */
//!
//! /* This example alters the over-current fault limit of the primary power supply extender's
//!  * BMR454 device via its PMBus interface
//!  */
//! int main()
//! {
//!     HIL_STATUS status;
//!     INT slot = 0;  /* primary power supply slot */
//!     BYTE data[2];
//!
//!     status = rcInit();
//!     if(status != HS_SUCCESS) {
//!         return 1;
//!     }
//!     status = rcRootI2cLock(1000); /* Lock the bus to ensure write/read is atomic, 1 second timeout */
//!     if(status != HS_SUCCESS) {
//!         return 2;
//!     }
//!     status = hilToL11(25.0, &data[0], &data[1]); /* determine LINEAR format bytes for 25 amps. */
//!     if(status != HS_SUCCESS) {
//!         rcRootI2cUnlock();
//!         return 3;
//!     }
//!     status = psExtBmr454Write(slot, IOUT_OC_FAULT_LIMIT, data, sizeof(data)); /* write two bytes */
//!     if(status != HS_SUCCESS) {
//!         rcRootI2cUnlock();
//!         return 4;
//!     }
//!     status = psExtBmr454Read(slot, IOUT_OC_FAULT_LIMIT, data, sizeof(data)); /* read two bytes */
//!     if(status != HS_SUCCESS) {
//!         rcRootI2cUnlock();
//!         return 5;
//!     }
//!     printf("IOUT_OC_FAULT_LIMIT = %lf A\n", hilFromL11(data[0], data[1]));
//!     rcRootI2cUnlock();
//!     return 0;
//! }
//! \endcode
//! \par Example of PMBus IOUT_OC_FAULT_LIMIT command - Python
//!
//! @note The Python-wrapped version of this API returns a byte string of length 2 as
//!       accepted by the Python APIs that access the PMBus.  This allows the return value of
//!       the hilToL11() conversion to be passed directly into the write APIs, instead of
//!       passing the individual bytes separately as in the C API.
//!
//! \code{.py}
//! import hil
//!
//! # PMBus Power System Management Protocol Specification - Part II
//! IOUT_OC_FAULT_LIMIT = 0x46  # over - current fault limit
//!
//! slot = 0   # primary power supply slot
//!
//! hil.rcInit()
//!
//! hil.rcRootI2cLock(1000)
//! try:
//!     data = hil.hilToL11(25)  # determine LINEAR format bytes for 25 amps.
//!     hil.psExtBmr454Write(slot, IOUT_OC_FAULT_LIMIT, data)
//!     raw = hil.psExtBmr454Read(slot, IOUT_OC_FAULT_LIMIT, 2) # read two bytes
//! finally:
//!     hil.rcRootI2cUnlock();
//!
//! limit = hil.hilFromL11(raw) # convert from L11 to current limit
//! print(f'IOUT_OC_FAULT_LIMIT = {limit:.3f} A')
//! \endcode
//! @param[in] data The C double floating point value to convert.
//! @param[out] pLsb The computed least significant byte of the 11-bit linear floating point format.  Pointer cannot be NULL.
//! @param[out] pMsb The computed most significant byte of the 11-bit linear floating point format.  Pointer cannot be NULL.
//! @retval HS_SUCCESS  The conversion was successful.
//! @retval HS_OVERFLOW The C double was too large to be represented in L11 format.
//! @retval HS_INVALID_PARAMETER \c pLsb or \c pMsb were null pointers.
HIL_API HIL_STATUS hilToL11(double data, BYTE* pLsb, BYTE* pMsb);

//! @brief Helper function for PMBus conversion from 16-bit linear floating point format.
//!
//! This function implements the conversion from the PMBus specification's LINEAR 16-bit floating point format to a C double as stated
//! in <i>section 8.3.1 Linear Mode</i> of the <i>PMBus Power System Management Protocol Specification, Part II - Command Language</i>.
//!
//! The 16-bit Linear Data Format is used by PMBus output voltage and output voltage-related parameters when Linear Mode is reported by
//! the \c VOUT_MODE command.  If another mode is reported this function will fail with #HS_INVALID_PARAMETER.
//!
//! The \c mode byte is the byte returned from the PMBus \c VOUT_MODE command.  It contains the PMBus data mode used by the device (which
//! must be Linear mode (bits 7-5 are 000b), and a fixed, 5-bit, two's complement common exponent used for commands using or returning data
//! in Linear mode.
//!
//! 16-bit Linear data is the result of the \c VOUT_MODE 5-bit, two's complement exponent and two data bytes that represent an unsigned
//! 16-bit mantissa.  For example, the PMBus \c VOUT_COMMAND instruction returns:
//! \image html L16.png "Linear Format Data Bytes"
//! Note that the two bytes are transmitted or returned little-endian.  The \c mode bits must be 000b.
//! The floating point value is computed as <code>value = V &sdot; 2<sup>N</sup></code>.
//!
//! \par Example of PMBus VOUT_MAX command - C
//! \code{.c}
//! #include <Windows.h>
//! #include <stdio.h>
//! #include "HilApi.h"
//! #include "RcApi.h"
//! #include "PowersupplyApi.h"
//!
//! /* PMBus Power System Management Protocol Specification - Part II */
//! const BYTE VOUT_MODE = 0x20;  /* one data byte returned */
//! const BYTE VOUT_MAX  = 0x24;  /* two data bytes in L16 format */
//!
//! /* This example alters the maximum VOUT allowed to be set for the primary power supply extender's
//!    BMR454 device via its PMBus interface */
//!
//! int main()
//! {
//!     HIL_STATUS status;
//!     INT slot = 0;  /* primary power supply slot */
//!     BYTE mode;
//!     BYTE data[2];
//!     double voltage;
//!
//!     status = rcInit();
//!     if(status != HS_SUCCESS) {
//!         return 1;
//!     }
//!     status = rcRootI2cLock(1000); /* Lock the bus to ensure write/read is atomic, 1 second timeout */
//!     if(status != HS_SUCCESS) {
//!         return 2;
//!     }
//!     status = psExtBmr454Read(slot, VOUT_MODE, &mode, sizeof(mode)); /* Read VOUT mode byte required by hilToL16() */
//!     if(status != HS_SUCCESS) {
//!         rcRootI2cUnlock();
//!         return 3;
//!     }
//!     status = hilToL16(14.0, mode, &data[0], &data[1]); /* determine LINEAR format bytes for 14 volts. */
//!     if(status != HS_SUCCESS) {
//!         rcRootI2cUnlock();
//!         return 4;
//!     }
//!     status = psExtBmr454Write(slot, VOUT_MAX, data, sizeof(data)); /* write two bytes */
//!     if(status != HS_SUCCESS) {
//!         rcRootI2cUnlock();
//!         return 5;
//!     }
//!     status = psExtBmr454Read(slot, VOUT_MAX, data, sizeof(data)); /* read two bytes */
//!     if(status != HS_SUCCESS) {
//!         rcRootI2cUnlock();
//!         return 6;
//!     }
//!
//!     rcRootI2cUnlock();
//!
//!     status = hilFromL16(data[0], data[1], mode, &voltage);
//!     if(status != HS_SUCCESS) {
//!         return 7;
//!     }
//!     printf("VOUT_MAX = %lf\n", voltage);
//!     return 0;
//! }
//! \endcode
//! \par Example of PMBus VOUT_MAX command - Python
//!
//! @note The Python-wrapped version of this API takes a single byte string of length 2 as
//!       returned by the Python APIs that access the PMBus.  This allows the return value of PMBus
//!       reads to be passed directly into this function, instead of an awkward
//!       <code>hil.hilFromL16(raw[0],raw[1],mode)</code>.
//!
//! \code{.py}
//! import hil
//!
//! # PMBus Power System Management Protocol Specification - Part II
//! VOUT_MODE = 0x20  # one data byte returned.
//! VOUT_MAX  = 0x24  # two data bytes read / written in L16 format
//!
//! slot = 0 # primary power supply extender
//!
//! hil.rcInit()
//! hil.rcRootI2cLock(1000)
//! try:
//!     mode = hil.psExtBmr454Read(slot, VOUT_MODE, 1)[0]  # returns 1 - byte string, dereference first byte to get value.
//!     hil.psExtBmr454Write(slot, VOUT_MAX, hil.hilToL16(14.0, mode))   # convert and write 2 bytes.
//!     voltage = hil.hilFromL16(hil.psExtBmr454Read(slot, VOUT_MAX, 2), mode) # read 2 bytes and convert.
//! finally:
//!     hil.rcRootI2cUnlock();
//!
//! print(f'VOUT_MAX = {voltage:.3f} V')
//! \endcode
//! @param[in] lsb  The least significant byte of the 16-bit linear floating point format.  Note bytes are returned little-endian from relevant
//!                 PMBus commands so this would be the first byte returned.
//! @param[in] msb  The most significant byte of the 16-bit linear floating point format.  Note bytes are returned little-endian from relevant
//!                 PMBus commands so this would be the second byte returned.
//! @param[in] mode This must be the byte returned from the PMBus \c VOUT_MODE command.  If bits 7-5 are not 000, #HS_INVALID_PARAMETER is returned.
//! @param[out] pResult The floating point result of the conversion, if successful.
//! @retval HS_SUCCESS The conversion was successful.
//! @retval HS_INVALID_PARAMETER \c pResult was NULL, or \c mode did not indicate Linear mode.  Bits 7-5 of \c mode must be zero.
HIL_API HIL_STATUS hilFromL16(BYTE lsb, BYTE msb, BYTE mode, double* pResult);

//! @brief Helper function to convert a C floating point value to the PMBus 16-bit linear floating point format.
//!
//! This function implements the conversion from a C double to the PMBus specification's LINEAR 16-bit floating point format as stated
//! in <i>section 8.3.1 Linear Data Format</i> of the <i>PMBus Power System Management Protocol Specification, Part II - Command Language</i>.
//!
//! 16-bit Linear data is the result of the \c VOUT_MODE 5-bit exponent and two data bytes that represent the 16-bit mantissa.  For example,
//! the PMBus \c VOUT_COMMAND instruction returns:
//! \image html L16.png "Linear Format Data Bytes"
//! Note that the two bytes are transmitted or returned little-endian.  The \c mode bits must be 000b.
//! The floating point value is computed as <code>value = V &sdot; 2<sup>N</sup></code>.  This function solves \c V for a particular \c data value.
//!
//! \par Example of PMBus VOUT_MAX command - C
//! \code{.c}
//! #include <Windows.h>
//! #include <stdio.h>
//! #include "HilApi.h"
//! #include "RcApi.h"
//! #include "PowersupplyApi.h"
//!
//! /* PMBus Power System Management Protocol Specification - Part II */
//! const BYTE VOUT_MODE = 0x20;  /* one data byte returned */
//! const BYTE VOUT_MAX  = 0x24;  /* two data bytes in L16 format */
//!
//! /* This example alters the maximum VOUT allowed to be set for the primary power supply extender's
//!    BMR454 device via its PMBus interface */
//!
//! int main()
//! {
//!     HIL_STATUS status;
//!     INT slot = 0;  /* primary power supply slot */
//!     BYTE mode;
//!     BYTE data[2];
//!     double voltage;
//!
//!     status = rcInit();
//!     if(status != HS_SUCCESS) {
//!         return 1;
//!     }
//!     status = rcRootI2cLock(1000); /* Lock the bus to ensure write/read is atomic, 1 second timeout */
//!     if(status != HS_SUCCESS) {
//!         return 2;
//!     }
//!     status = psExtBmr454Read(slot, VOUT_MODE, &mode, sizeof(mode)); /* Read VOUT mode byte required by hilToL16() */
//!     if(status != HS_SUCCESS) {
//!         rcRootI2cUnlock();
//!         return 3;
//!     }
//!     status = hilToL16(14.0, mode, &data[0], &data[1]); /* determine LINEAR format bytes for 14 volts. */
//!     if(status != HS_SUCCESS) {
//!         rcRootI2cUnlock();
//!         return 4;
//!     }
//!     status = psExtBmr454Write(slot, VOUT_MAX, data, sizeof(data)); /* write two bytes */
//!     if(status != HS_SUCCESS) {
//!         rcRootI2cUnlock();
//!         return 5;
//!     }
//!     status = psExtBmr454Read(slot, VOUT_MAX, data, sizeof(data)); /* read two bytes */
//!     if(status != HS_SUCCESS) {
//!         rcRootI2cUnlock();
//!         return 6;
//!     }
//!
//!     rcRootI2cUnlock();
//!
//!     status = hilFromL16(data[0], data[1], mode, &voltage);
//!     if(status != HS_SUCCESS) {
//!         return 7;
//!     }
//!     printf("VOUT_MAX = %lf\n", voltage);
//!     return 0;
//! }
//! \endcode
//! \par Example of PMBus VOUT_MAX command - Python
//!
//! @note The Python-wrapped version of this API returns a byte string of length 2 as
//!       accepted by the Python APIs that access the PMBus.  This allows the return value of
//!       the hilToL16() conversion to be passed directly into the write APIs, instead of
//!       passing the individual bytes separately as in the C API.
//!
//! \code{.py}
//! import hil
//!
//! # PMBus Power System Management Protocol Specification - Part II
//! VOUT_MODE = 0x20  # one data byte returned.
//! VOUT_MAX  = 0x24  # two data bytes read / written in L16 format
//!
//! slot = 0 # primary power supply extender
//!
//! hil.rcInit()
//! hil.rcRootI2cLock(1000)
//! try:
//!     mode = hil.psExtBmr454Read(slot, VOUT_MODE, 1)[0]  # returns 1 - byte string, dereference first byte to get value.
//!     hil.psExtBmr454Write(slot, VOUT_MAX, hil.hilToL16(14.0, mode))   # convert and write 2 bytes.
//!     voltage = hil.hilFromL16(hil.psExtBmr454Read(slot, VOUT_MAX, 2), mode) # read 2 bytes and convert.
//! finally:
//!     hil.rcRootI2cUnlock();
//!
//! print(f'VOUT_MAX = {voltage:.3f} V')
//! \endcode
//! @param[in] data The C double floating point value to convert.
//! @param[in] mode This must be the byte returned from the PMBus \c VOUT_MODE command.  If bits 7-5 are not 000, #HS_INVALID_PARAMETER is returned.
//! @param[out] pLsb The computed least significant byte of the 16-bit linear floating point format.  Pointer cannot be NULL.
//! @param[out] pMsb The computed most significant byte of the 16-bit linear floating point format.  Pointer cannot be NULL.
//! @retval HS_SUCCESS  The conversion was successful.
//! @retval HS_INVALID_PARAMETER \c pLsb or \c pMsb were null pointers, or \c data was negative.
//! @retval HS_OVERFLOW \c data was too large to be represented in L16 format for the given \c mode.
HIL_API HIL_STATUS hilToL16(double data, BYTE mode, BYTE* pLsb, BYTE* pMsb);

#ifdef __cplusplus
}
#endif
