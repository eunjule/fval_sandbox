// INTEL CONFIDENTIAL
// Copyright 2014-2018 Intel Corporation All Rights Reserved.
//
// The source code contained or described herein and all documents related to
// the source code ("Material") are owned by Intel Corporation or its
// suppliers or licensors. Title to the Material remains with Intel
// Corporation or its suppliers and licensors. The Material contains trade
// secrets and proprietary and confidential information of Intel or its
// suppliers and licensors. The Material is protected by worldwide copyright
// and trade secret laws and treaty provisions. No part of the Material may be
// used, copied, reproduced, modified, published, uploaded, posted,
// transmitted, distributed, or disclosed in any way without Intel's prior
// express written permission.
//
// No license under any patent, copyright, trade secret or other intellectual
// property right is granted to or conferred upon you by disclosure or
// delivery of the Materials, either expressly, by implication, inducement,
// estoppel or otherwise. Any license under such intellectual property rights
// must be express and approved by Intel in writing.

//! @file
//! @brief Non-function definitions such as constants and structures used by the HDMT backplane APIs.

#pragma once

//! @brief Backplane Root I2C Channel bit field definitions.
typedef enum BP_ROOT_I2C_CHANNELS
{
    BRIC_NONE     = 0,       //!< Binary representation of no channels selected
    BRIC_UNUSED_0 = 1 << 0,  //!< Binary representation of unused channel (previously connected to instrument slot 8)
    BRIC_UNUSED_1 = 1 << 1,  //!< Binary representation of unused channel (previously connected to instrument slot 9)
    BRIC_UNUSED_2 = 1 << 2,  //!< Binary representation of unused channel (previously connected to instrument slot 10)
    BRIC_UNUSED_3 = 1 << 3,  //!< Binary representation of unused channel (previously connected to instrument slot 11)
    BRIC_UNUSED_4 = 1 << 4,  //!< Binary representation of unused channel (previously connected to %BLT)
    BRIC_PRI_PS   = 1 << 5,  //!< Binary representation of Primary Bulk Power Supply channel selected
    BRIC_AUX      = 1 << 6,  //!< Binary representation of Aux 5V and Aux 12V channel selected
    BRIC_SEC_PS   = 1 << 7,  //!< Binary representation of Secondary Bulk Power Supply and System 12V channel selected
    BRIC_ALL      = BRIC_UNUSED_0 | BRIC_UNUSED_1 | BRIC_UNUSED_2 | BRIC_UNUSED_3 |
                    BRIC_UNUSED_4 | BRIC_PRI_PS   | BRIC_AUX      | BRIC_SEC_PS  //!< Binary representation of all channels selected
} BP_ROOT_I2C_CHANNELS;
