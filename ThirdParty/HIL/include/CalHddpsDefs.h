// INTEL CONFIDENTIAL
// Copyright 2014-2018 Intel Corporation All Rights Reserved.
//
// The source code contained or described herein and all documents related to
// the source code ("Material") are owned by Intel Corporation or its
// suppliers or licensors. Title to the Material remains with Intel
// Corporation or its suppliers and licensors. The Material contains trade
// secrets and proprietary and confidential information of Intel or its
// suppliers and licensors. The Material is protected by worldwide copyright
// and trade secret laws and treaty provisions. No part of the Material may be
// used, copied, reproduced, modified, published, uploaded, posted,
// transmitted, distributed, or disclosed in any way without Intel's prior
// express written permission.
//
// No license under any patent, copyright, trade secret or other intellectual
// property right is granted to or conferred upon you by disclosure or
// delivery of the Materials, either expressly, by implication, inducement,
// estoppel or otherwise. Any license under such intellectual property rights
// must be express and approved by Intel in writing.

//! @file
//! @brief Non-function definitions such as constants and structures used by the HDMT HDDPS calibration card APIs.

#pragma once

//! @brief Enumeration of the possible general purpose I/O lines on the Cal HDDPS that can be passed to functions that
//!        have a ::CAL_HDDPS_GPIO argument.  These names coincide with the IO Expander net
//!        names on the HDDPS Cal schematic (except that these names also have a CHG_ prefix).
typedef enum CAL_HDDPS_GPIO
{
    CHG_SEL_SENSE_VLC00,      // I2C IO EXPANDER Chip #0 (U3 - Control for Sense Connections)
    CHG_SEL_SENSE_VLC01,
    CHG_SEL_SENSE_VLC02,
    CHG_SEL_SENSE_VLC03,
    CHG_SEL_SENSE_VLC04,
    CHG_SEL_SENSE_VLC05,
    CHG_SEL_SENSE_VLC06,
    CHG_SEL_SENSE_VLC07,
    CHG_SEL_SENSE_VLC08,
    CHG_SEL_SENSE_VLC09,
    CHG_SEL_SENSE_VLC10,
    CHG_SEL_SENSE_VLC11,
    CHG_SEL_SENSE_VLC12,
    CHG_SEL_SENSE_VLC13,
    CHG_SEL_SENSE_VLC14,
    CHG_SEL_SENSE_VLC15,
    CHG_SEL_SENSE_LC0,
    CHG_SEL_SENSE_LC1,
    CHG_SEL_SENSE_LC2,
    CHG_SEL_SENSE_LC3,
    CHG_SEL_SENSE_LC4,
    CHG_SEL_SENSE_LC5,
    CHG_SEL_SENSE_LC6,
    CHG_SEL_SENSE_LC7,
    CHG_SEL_SENSE_LC8,
    CHG_SEL_SENSE_LC9,
    CHG_SEL_SENSE_HC0,
    CHG_SEL_SENSE_HC1,
    CHG_SEL_SENSE_HC2,
    CHG_SEL_SENSE_HC3,
    CHG_SEL_SENSE_HC4,
    CHG_SEL_SENSE_HC5,
    CHG_SEL_SENSE_HC6,
    CHG_SEL_SENSE_HC7,
    CHG_SEL_SENSE_HC8,
    CHG_SEL_SENSE_HC9,
    CHG_CROSS_BRD_GANG_SEL,
    CHG_EXC_S_DMM_NULL_SEL,
    CHG_TP_U3PIN53,
    CHG_TP_U3PIN54,

    CHG_SEL_0P025OHM_EXC,      // I2C IO EXPANDER Chip #1 (U1 - Control for Load Connections)
    CHG_SEL_0P05OHM_EXC,
    CHG_SEL_0P25OHM_EXC,
    CHG_SEL_0P8OHM_EXC,
    CHG_SEL_4OHM_EXC,
    CHG_SEL_10OHM_EXC,
    CHG_SEL_100OHM_EXC,
    CHG_SEL_1KOHM_EXC,
    CHG_SEL_10KOHM_EXC,
    CHG_SEL_100KOHM_EXC,
    CHG_SEL_500KOHM_EXC,
    CHG_SEL_1MOHM_EXC,
    CHG_SEL_0P025OHM_SENSE,
    CHG_SEL_0P05OHM_SENSE,
    CHG_SEL_0P25OHM_SENSE,
    CHG_SEL_0P8OHM_SENSE,
    CHG_SEL_4OHM_SENSE,
    CHG_SEL_10OHM_SENSE,
    CHG_SEL_100OHM_SENSE,
    CHG_SEL_1KOHM_SENSE,
    CHG_SEL_10KOHM_SENSE,
    CHG_SEL_100KOHM_SENSE,
    CHG_SEL_500KOHM_SENSE,
    CHG_SEL_1MOHM_SENSE,
    CHG_SEL_0P025OHM_FORCE_1,
    CHG_SEL_0P025OHM_FORCE_2,
    CHG_SEL_0P05OHM_FORCE,
    CHG_SEL_0P25OHM_FORCE,
    CHG_SEL_0P8OHM_FORCE,
    CHG_SEL_4OHM_FORCE,
    CHG_SEL_10OHM_FORCE,
    CHG_SEL_100OHM_FORCE,
    CHG_SEL_1KOHM_FORCE,
    CHG_SEL_10KOHM_FORCE,
    CHG_SEL_100KOHM_FORCE,
    CHG_SEL_500KOHM_FORCE,
    CHG_SEL_1MOHM_FORCE,
    CHG_SEL_DMM_SENSE,
    CHG_SEL_DMM_EXC,
    CHG_SEL_EXT_LOAD,

    CHG_SEL_FORCE_VLC00,      // I2C IO EXPANDER Chip #2 (U2 - Control for Force Connections)
    CHG_SEL_FORCE_VLC01,
    CHG_SEL_FORCE_VLC02,
    CHG_SEL_FORCE_VLC03,
    CHG_SEL_FORCE_VLC04,
    CHG_SEL_FORCE_VLC05,
    CHG_SEL_FORCE_VLC06,
    CHG_SEL_FORCE_VLC07,
    CHG_SEL_FORCE_VLC08,
    CHG_SEL_FORCE_VLC09,
    CHG_SEL_FORCE_VLC10,
    CHG_SEL_FORCE_VLC11,
    CHG_SEL_FORCE_VLC12,
    CHG_SEL_FORCE_VLC13,
    CHG_SEL_FORCE_VLC14,
    CHG_SEL_FORCE_VLC15,
    CHG_SEL_FORCE_LC0,
    CHG_SEL_FORCE_LC1,
    CHG_SEL_FORCE_LC2,
    CHG_SEL_FORCE_LC3,
    CHG_SEL_FORCE_LC4,
    CHG_SEL_FORCE_LC5,
    CHG_SEL_FORCE_LC6,
    CHG_SEL_FORCE_LC7,
    CHG_SEL_FORCE_LC8,
    CHG_SEL_FORCE_LC9,
    CHG_SEL_FORCE_HC0,
    CHG_SEL_FORCE_HC1,
    CHG_SEL_FORCE_HC2,
    CHG_SEL_FORCE_HC3,
    CHG_SEL_FORCE_HC4,
    CHG_SEL_FORCE_HC5,
    CHG_SEL_FORCE_HC6,
    CHG_SEL_FORCE_HC7,
    CHG_SEL_FORCE_HC8,
    CHG_SEL_FORCE_HC9,
    CHG_TP_U2PIN50,
    CHG_TP_U2PIN52,
    CHG_TP_U2PIN53,
    CHG_SEL_DMM_NULL,
    CHG_LAST_INVALID
} CAL_HDDPS_GPIO;
