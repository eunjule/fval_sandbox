// INTEL CONFIDENTIAL
// Copyright 2017-2018 Intel Corporation All Rights Reserved.
//
// The source code contained or described herein and all documents related to
// the source code ("Material") are owned by Intel Corporation or its
// suppliers or licensors. Title to the Material remains with Intel
// Corporation or its suppliers and licensors. The Material contains trade
// secrets and proprietary and confidential information of Intel or its
// suppliers and licensors. The Material is protected by worldwide copyright
// and trade secret laws and treaty provisions. No part of the Material may be
// used, copied, reproduced, modified, published, uploaded, posted,
// transmitted, distributed, or disclosed in any way without Intel's prior
// express written permission.
//
// No license under any patent, copyright, trade secret or other intellectual
// property right is granted to or conferred upon you by disclosure or
// delivery of the Materials, either expressly, by implication, inducement,
// estoppel or otherwise. Any license under such intellectual property rights
// must be express and approved by Intel in writing.

//! @file
//! @brief APIs to control an HDMT HVDPS calibration card.
#pragma once
#include "HilDefs.h"
#include "CalHvdpsDefs.h"

#ifdef __cplusplus
extern "C" {
#endif

//! @brief Verifies an HVDPS calibration card is present.
//!
//! This function verifies an HVDPS calibration card is present.  It connects to and caches driver resources for use
//! by other \c calHvdpsXXXXX functions.  calHvdpsDisconnect() is its complementary function and frees any cached resources.
//!
//! Neither calHvdpsConnect() or calHvdpsDisconnect() are required to be called to use the other \c calHvdpsXXXXX functions.  All
//! \c calHvdpsXXXXX functions will allocate resources if they are not already allocated and free them if required to
//! perform their function.
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS calHvdpsConnect(_In_ INT slot);

//! @brief Frees any resources cached from using an HVDPS calibration card functions.
//!
//! This function frees any resources cached from using an HVDPS calibration card functions. calHvdpsConnect() is its
//! complementary function.
//!
//! Neither calHvdpsConnect() or calHvdpsDisconnect() are required to be called to use the other \c calHvdpsXXXXX functions.  All
//! \c calHvdpsXXXXX functions will allocate resources if they are not already allocated and free them if required to
//! perform their function.
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS calHvdpsDisconnect(_In_ INT slot);

//! @brief Programs the vendor and product IDs of all USB devices on an HVDPS calibration card.
//!
//! This function programs the vendor and product IDs (VID/PIDs) of the FT240X and Cypress
//! USB devices on an HVDPS calibration card.  Correct VID/PIDs are required to ensure Windows Device
//! Manager displays a proper description of the device, and HIL requires correct VID/PIDs to ensure
//! software can locate and communicate with the correct component.
//! @note FTDI devices have a limited number of updates, but this function is safe to call because it
//! only writes if the component programming is incorrect.
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS calHvdpsVidPidsSet(_In_ INT slot);

//! @brief Initializes the components of an HVDPS calibration card for operation.
//!
//! This function initializes the components of an HVDPS calibration card for operation.  Calling this
//! function is required before using most other \c calHvdpsXxxxx() functions.
//! @param[in] slot A physical slot number.  Valid values are 0 to \ref HIL_SLOTS - 1.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS calHvdpsInit(_In_ INT slot);

//! @brief Reads the board-level traceability values from an HVDPS calibration card.
//!
//! This function reads the board-level traceability values from an HVDPS calibration card.  The values are defined in the #BLT structure.
//!
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[out] pBlt The address of a #BLT structure that will be populated with the data.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS calHvdpsBltBoardRead(_In_ INT slot, _Out_ PBLT pBlt);

//! @brief Writes the board-level traceability values for an HVDPS calibration card.
//!
//! This function writes the board-level traceability values for an HVDPS calibration card.  The values are defined in the #BLT structure.
//!
//! The \c flags field allows this function to update individual values.  The flags can be ORed together.  Only the fields indicated by
//! \c flags need to be populated in the provided \c pBlt parameter.
//!
//! \par Updating the Full BLT - C
//! \code{.c}
//! #include <Windows.h>
//! #include "CalHvdpsApi.h"
//! #include "string.h"
//!
//! int main()
//! {
//!     int slot = 8;
//!     BLT blt;
//!     HIL_STATUS status;
//!     blt.Id = 0x10000FF;
//!     strcpy(blt.DeviceName,"DeviceName");
//!     strcpy(blt.VendorName,"INTEL");
//!     strcpy(blt.PartNumberAsBuilt,"AAxxxxxx-xxx");
//!     strcpy(blt.PartNumberCurrent,blt.PartNumberAsBuilt);
//!     strcpy(blt.SerialNumber,"12345678");
//!     strcpy(blt.ManufactureDate,"YYMMDD");
//!     status = calHvdpsBltBoardWrite(slot, &blt, BF_ALL);
//!     if (status != HS_SUCCESS)
//!         return -1;
//!     return 0;
//! }
//! \endcode
//! \par Updating the Current Part Number - Python
//! \code{.py}
//! slot = 8
//! blt = hil.BLT()
//! blt.PartNumberCurrent = 'AAyyyyyy-yyy'
//! hil.calHvdpsBltBoardWrite(slot,blt,hil.BF_PART_NUMBER_CURRENT)
//! # Note Python returns None for HIL_STATUS if successful and raises RuntimeError on failure.
//! \endcode
//!
//! \note HS_BLT_NOT_FOUND can be returned on an update (lack of #BF_ALL flag).
//!
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] pBlt The address of a #BLT structure that contains the data to be written or updated.
//! @param[in] flags #BLT_FLAGS bits indicating what fields in \c pBlt are valid.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS calHvdpsBltBoardWrite(_In_ INT slot, _In_ PCBLT pBlt, _In_ BLT_FLAGS flags);

//! @brief Reads the raw data from the specified HVDPS calibration card's BLT EEPROM.
//!
//! This function reads the raw data from the specified HVDPS calibration card's BLT EEPROM.
//!
//! @param[in] slot A physical slot number.  Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[out] pEepromData The output buffer.
//! @param[in,out] pLength On input, the size of the output buffer referenced by \c pData.  On output, the size of the EEPROM data.
//! @retval HS_INSUFFICIENT_BUFFER The buffer provided was too small.  \c pLength will indicate the minimum buffer size required.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS calHvdpsBltEepromRead(_In_ INT slot, _Out_writes_bytes_to_(_Old_(*pLength), *pLength) LPVOID pEepromData, _Inout_ LPDWORD pLength);

//! @brief Reads data from the CAL EEPROM on an HVDPS calibration card.
//!
//! This function reads data from the CAL EEPROM on an HVDPS calibration card.  The designation for this chip is U4 on Fab A and it is a 512Kb part (64KB).
//!
//! @param[in] slot    A physical slot number.  Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] address The location of the first byte in the EEPROM from which the data will be read.  Valid values are 0 - 0xFFFF.
//! @param[out] pData  A pointer to a data buffer in which the read data will be stored. Note that this buffer must have at least \c length
//!                    allocated in order to complete successfully.
//! @param[in] length  The number of bytes to read from the EEPROM.  \c address plus \c length cannot exceed 0x10000 (64KB).
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS calHvdpsEepromRead(_In_ INT slot, _In_ UINT address, _Out_writes_bytes_all_(length) LPVOID pData, _In_ DWORD length);

//! @brief Writes data to the CAL EEPROM on an HVDPS calibration card.
//!
//! This function writes data to the CAL EEPROM on an HVDPS calibration card.  The designation for this chip is U4 on Fab A and it is a 512Kb part (64KB).
//!
//! @param[in] slot    A physical slot number.  Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] address The location of the first byte in the EEPROM to which the data in the \c pData buffer will be written.  Valid values are 0 - 0xFFFF.
//! @param[in] pData   A data buffer containing data to write to the EEPROM.  \c address plus \c length cannot exceed 0x10000 (64KB).
//! @param[in] length  The byte length of the \c pData buffer.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS calHvdpsEepromWrite(_In_ INT slot, _In_ UINT address, _In_reads_bytes_(length) LPCVOID pData, _In_ DWORD length);

//! @brief Reads a temperature on an HVDPS calibration card.
//!
//! This function reads a temperature on an HVDPS calibration card.
//!
//! @param[in] slot    A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] channel The temperature channel to read.  Consult the following table.
//!                    Note that reference designators are for Fab A and may change in new fabs.
//!                    | channel | Designator | Comment                          |
//!                    | :-----: | :--------: | :------------------------------: |
//!                    |    0    |    U6T1    | LM75A Digital Temperature Sensor |
//!                    |    1    |    U6R1    |    \"                            |
//!                    |    2    |    U7V1    |    \"                            |
//!                    |    3    |    U6V1    |    \"                            |
//!                    |    4    |    U6N1    |    \"                            |
//! @param[out] pTemp The temperature in degrees Celsius.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS calHvdpsTmonRead(_In_ INT slot, _In_ UINT channel, _Out_ double* pTemp);

//! @brief Reads a register within the specified LM75A device (digital temperature sensor) on an HVDPS calibration card.
//!
//! This function reads a register within the specified LM75A device (digital temperature sensor) on an HVDPS calibration card.
//!
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] chip A physical chip number. Consult the following table.
//!                 Note that reference designators are for Fab A and may change in new fabs.
//!                 | Index |  Designator |
//!                 | :---: | :---------: |
//!                 |   0   |    U6T1     |
//!                 |   1   |    U6R1     |
//!                 |   2   |    U7V1     |
//!                 |   3   |    U6V1     |
//!                 |   4   |    U6N1     |
//! @param[in] reg Register to read. Consult the following table for valid read registers.  Note that some registers are 16-bit and some are 8-bit.
//!                 | Index |  Register  | Bits |
//!                 | :---: | :--------: | :--: |
//!                 |   0   |   Temp     |  16  |
//!                 |   1   |   Config   |   8  |
//!                 |   2   |   Thyst    |  16  |
//!                 |   3   |   Tos      |  16  |
//! @param[out] pData The returned data.  8-bit registers are zero-extended into the output word.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS calHvdpsLm75aRead(_In_ INT slot, _In_ UINT chip, _In_ BYTE reg, _Out_ LPWORD pData);

//! @brief Writes a register within the specified LM75A device (digital temperature sensor) on an HVDPS calibration card.
//!
//! This function writes a register within the specified LM75A device (digital temperature sensor) on an HVDPS calibration card.
//!
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] chip A physical chip number. Consult the following table.
//!                 Note that reference designators are for Fab A and may change in new fabs.
//!                 | Index |  Designator |
//!                 | :---: | :---------: |
//!                 |   0   |    U6T1     |
//!                 |   1   |    U6R1     |
//!                 |   2   |    U7V1     |
//!                 |   3   |    U6V1     |
//!                 |   4   |    U6N1     |
//! @param[in] reg Register to write. Consult the following table for valid write registers.  Note that some registers are 16-bit and some are 8-bit.
//!                 | Index |  Register  | Bits |
//!                 | :---: | :--------: | :--: |
//!                 |   1   |   Config   |   8  |
//!                 |   2   |   Thyst    |  16  |
//!                 |   3   |   Tos      |  16  |
//! @param[in] data The data to write.  HS_INVALID_PARAMETER will be returned if the data exceeds the register size.  Valid values are 00h-FFh (8-bit) or 0000h-FFFFh (16-bit).
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS calHvdpsLm75aWrite(_In_ INT slot, _In_ UINT chip, _In_ BYTE reg, _In_ WORD data);

//! @brief Initializes the PCA9505 device to a known state on an HVDPS calibration card.
//!
//! This function initializes the PCA9505 device to a known state, including setting all GPIOs to logic high.
//! This ensures that the active low relays attached to the GPIOs are off.  Note that this function can only be called after power
//! has been applied to the TIU using bpTiuAuxPowerEnable().
//! @param[in] slot A physical slot number.  Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] chip A physical chip number. Consult the following table.
//!                 Note that reference designators are for Fab A and may change in new fabs.
//!                 | Index | Designator |
//!                 | :---: | :--------: |
//!                 |   0   |     U3     |
//!                 |   1   |     U11    |
//!                 |   2   |     U1     |
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS calHvdpsPca9505Init(_In_ INT slot, _In_ INT chip);

//! @brief Writes a register within the specified PCA9505 (GPIO expander) device  on an HVDPS calibration card.
//!
//! This function writes a register within the specified PCA9505 (GPIO expander) device  on an HVDPS calibration card.
//!
//! @param[in] slot A physical slot number.  Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] chip A physical chip number. Consult the following table.
//!                 Note that reference designators are for Fab A and may change in new fabs.
//!                 | Index | Designator |
//!                 | :---: | :--------: |
//!                 |   0   |     U3     |
//!                 |   1   |     U11    |
//!                 |   2   |     U1     |
//! @param[in] reg  PCA9505 register address.
//! @param[in] data The data value to write.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS calHvdpsPca9505Write(_In_ INT slot, _In_ INT chip, _In_ BYTE reg, _In_ BYTE data);

//! @brief Reads a register within the specified PCA9505 (GPIO Expander) device on an HVDPS calibration card.
//!
//! This function reads a register within the specified PCA9505 (GPIO Expander) device on an HVDPS calibration card.
//!
//! @param[in] slot A physical slot number.  Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] chip A physical chip number. Consult the following table.
//!                 Note that reference designators are for Fab A and may change in new fabs.
//!                 | Index | Designator |
//!                 | :---: | :--------: |
//!                 |   0   |     U3     |
//!                 |   1   |     U11    |
//!                 |   2   |     U1     |
//! @param[in]  reg   PCA9505 register address.
//! @param[out] pData The data value read back.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS calHvdpsPca9505Read(_In_ INT slot, _In_ INT chip, _In_ BYTE reg, _Out_ PBYTE pData);

//! @brief Writes a register within the specified MAX6650 (fan controller) device on an HVDPS calibration card.
//!
//! This function writes a register within the specified MAX6650 (fan controller) device on an HVDPS calibration card.
//!
//! @param[in] slot A physical slot number.  Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] chip A physical chip number. Consult the following table.
//!               Note that reference designators are for Fab A and may change in future fabs.
//!                 | Index |    MAX6650    | Connector |
//!                 | :---: | :-----------: | :-------: |
//!                 |   0   |       U48     |    J95    |
//!                 |   1   |       U45     |    J96    |
//! @param[in] reg Writable MAX6650 register address.
//!                 | Register | Name         | Function              |
//!                 | :------: | :----------: | :-------------------- |
//!                 |   0x00   | SPEED        | Fan speed             |
//!                 |   0x02   | CONFIG       | Configuration         |
//!                 |   0x04   | GPIO DEF     | GPIO definition       |
//!                 |   0x06   | DAC          | DAC                   |
//!                 |   0x08   | ALARM ENABLE | Alarm Enable          |
//!                 |   0x16   | COUNT        | Tachometer count time |
//! @param[in] data The data value to write.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS calHvdpsMax6650Write(_In_ INT slot, _In_ INT chip, _In_ BYTE reg, _In_ BYTE data);

//! @brief Reads a register within the specified MAX6650 (fan controller) device on an HVDPS calibration card.
//!
//! This function reads a register within the specified MAX6650 (fan controller) device on an HVDPS calibration card.
//!
//! @param[in] slot A physical slot number.  Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] chip A physical chip number. Consult the following table.
//!               Note that reference designators are for Fab A and may change in future fabs.
//!                 | Index |    MAX6650    | Connector |
//!                 | :---: | :-----------: | :-------: |
//!                 |   0   |       U48     |    J95    |
//!                 |   1   |       U45     |    J96    |
//! @param[in] reg MAX6650 register address.
//!                 | Register | Name         | Function              |
//!                 | :------: | :----------: | :-------------------- |
//!                 |   0x00   | SPEED        | Fan speed             |
//!                 |   0x02   | CONFIG       | Configuration         |
//!                 |   0x04   | GPIO DEF     | GPIO definition       |
//!                 |   0x06   | DAC          | DAC                   |
//!                 |   0x08   | ALARM ENABLE | Alarm Enable          |
//!                 |   0x0A   | ALARM        | Alarm Status          |
//!                 |   0x0C   | TACH0        | Tachometer 0 count    |
//!                 |   0x0E   | TACH1        | Tachometer 1 count    |
//!                 |   0x10   | TACH2        | Tachometer 2 count    |
//!                 |   0x12   | TACH3        | Tachometer 3 count    |
//!                 |   0x14   | GPIO STAT    | GPIO Status           |
//!                 |   0x16   | COUNT        | Tachometer count time |
//! @param[out] pData The data value read back.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS calHvdpsMax6650Read(_In_ INT slot, _In_ INT chip, _In_ BYTE reg, _Out_ PBYTE pData);

//! @brief Reads a fan tachometer on an HVDPS calibration card.
//!
//! This function reads a fan tachometer on an HVDPS calibration card.
//! @param[in] slot A physical slot number.  Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] fan  The fan to read. Consult the following table:
//!               Note that reference designators are for Fab A and may change in future fabs.
//!                 |  fan  |    MAX6650    | Connector |
//!                 | :---: | :-----------: | :-------: |
//!                 |   0   | U48 TACH_FAN0 |    J95    |
//!                 |   1   | U45 TACH_FAN1 |    J96    |
//! @note Optionally, calHvdpsInit() can be called to enable the tachometer alarm.
//!       If calHvdpsInit() is not called, the first call to this function can take up to two seconds to configure the fan controller
//!       and update the tachometer.
//! @param[out] pTach The fan speed in revolutions per second (RPS).
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS calHvdpsFanRead(_In_ INT slot, _In_ UINT fan, _Out_ double* pTach);

//! @brief Sets the GPIOs controlled by all the PCA9505 devices to a known state.
//!
//! This function sets the relays controlled by all the PCA9505 devices to a known state.  All GPIO pins are
//! set to logic high which ensures that the active low relays attached to them are off.
//! @note This function requires calHvdpsInit() to be called to initialize the GPIO direction lines.
//!
//! @param[in] slot A physical slot number.  Valid values are 0 to \ref HIL_SLOTS - 1.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS calHvdpsGpiosReset(_In_ INT slot);

//! @brief Sets the specified GPIO line to a logic high or low.
//!
//! This function sets the specified GPIO line to a logic high or low.
//! @note This function requires calHvdpsInit() to be called to initialize the GPIO direction lines.
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] gpio  Specifies which single GPIO on the HDDPS calibration card to set high or low.
//! @param[in] state Specifies whether the GPIO line is set to logic high (TRUE) or logic low (FALSE).
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS calHvdpsGpioWrite(_In_ INT slot, _In_ CAL_HVDPS_GPIO gpio, _In_ BOOL state);

//! @brief Reads the value of the specified GPIO line.
//!
//! This function reads the logic value of the specified GPIO line.
//! @note This function requires calHvdpsInit() to be called to initialize the GPIO direction lines.
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in]  gpio Specifies which single GPIO on the HDDPS calibration card to read.
//! @param[out] pState Contains the logic state of the GPIO where TRUE is logic high and FALSE is logic low.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS calHvdpsGpioRead(_In_ INT slot, _In_ CAL_HVDPS_GPIO gpio, _Out_ LPBOOL pState);

#ifdef __cplusplus
}
#endif
