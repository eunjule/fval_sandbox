// INTEL CONFIDENTIAL
// Copyright 2014-2018 Intel Corporation All Rights Reserved.
//
// The source code contained or described herein and all documents related to
// the source code ("Material") are owned by Intel Corporation or its
// suppliers or licensors. Title to the Material remains with Intel
// Corporation or its suppliers and licensors. The Material contains trade
// secrets and proprietary and confidential information of Intel or its
// suppliers and licensors. The Material is protected by worldwide copyright
// and trade secret laws and treaty provisions. No part of the Material may be
// used, copied, reproduced, modified, published, uploaded, posted,
// transmitted, distributed, or disclosed in any way without Intel's prior
// express written permission.
//
// No license under any patent, copyright, trade secret or other intellectual
// property right is granted to or conferred upon you by disclosure or
// delivery of the Materials, either expressly, by implication, inducement,
// estoppel or otherwise. Any license under such intellectual property rights
// must be express and approved by Intel in writing.

//! @file
//! @brief APIs to control HDMT DPS cards, currently the HDDPS, MDDPS, HVDPS and HVIL are supported.
#pragma once
#include "HilDefs.h"

#ifdef __cplusplus
extern "C" {
#endif

//! @brief Verifies an HDMT DPS card is present.
//!
//! This function verifies an HDMT DPS card is present.  It connects to and caches driver resources for use
//! by other \c dpsXXXXX functions.  dpsDisconnect() is its complementary function and frees any cached resources.
//!
//! Neither dpsConnect() or dpsDisconnect() are required to be called to use the other \c dpsXXXXX functions.  All
//! \c dpsXXXXX functions will allocate resources if they are not already allocated and free them if required to
//! perform their function.
//! @param[in] slot    A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] subslot Always use 0 for a HVDPS and 1 for HVIL.  For an HDDPS,
//!                    0=mainboard, 1=daughterboard.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS dpsConnect(_In_ INT slot, _In_ INT subslot);

//! @brief Frees any resources cached from using the DPS card functions.
//!
//! This function frees any resources associated with using the DPS card HIL functions. dpsConnect() is its
//! complementary function.
//!
//! Neither dpsConnect() or dpsDisconnect() are required to be called to use the other \c dpsXXXXX functions.  All
//! \c dpsXXXXX functions will allocate resources if they are not already allocated and free them if required to
//! perform their function.
//! @param[in] slot    A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] subslot Always use 0 for a HVDPS and 1 for HVIL.  For an HDDPS,
//!                    0=mainboard, 1=daughterboard.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS dpsDisconnect(_In_ INT slot, _In_ INT subslot);

//! @brief Reads the one of the CPLD version numbers from a DPS card.
//!
//! This function reads the one of the CPLD version numbers from a DPS card.
//!
//! @param[in] slot    A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] subslot Always use 0 for a HVDPS and 1 for HVIL.  For an HDDPS,
//!                    0=mainboard, 1=daughterboard.
//! @param[out] pVersion Returns the version register value.  The pointer must not be NULL.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS dpsCpldVersion(_In_ INT slot, _In_ INT subslot, _Out_ LPDWORD pVersion);

//! @brief Reads a DPS card's CPLD version string.
//!
//! This function reads the version register of a DPS card's CPLD and converts it to an ASCII string representation.
//!
//! @param[in] slot    A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] subslot Always use 0 for a HVDPS and 1 for HVIL.  For an HDDPS,
//!                    0=mainboard, 1=daughterboard.
//! @param[out] pVersion Returns the version string.  The pointer must not be NULL.
//! @param[in] length The length of the \c pVersion buffer.  It should be at least five characters to hold a null-terminated version string.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS dpsCpldVersionString(_In_ INT slot, _In_ INT subslot, _Out_writes_z_(length) LPSTR pVersion, _In_ DWORD length);

//! @brief Reads a DPS card's FPGA version number.
//!
//! This function reads the FPGA version number from  DPS card's FPGA.  The returned 32-bit value is in the
//! format 0xMMMMmmmm where the upper 16-bits are the major version and lower 16-bits are the minor version.
//!
//! For example 0x00050001 is version 5.1.
//! @param[in] slot    A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] subslot Always use 0 for a HVDPS and 1 for HVIL.  For an HDDPS,
//!                    0=mainboard, 1=daughterboard.
//! @param[out] pVersion The address of a DWORD of memory.  It may not be NULL.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS dpsFpgaVersion(_In_ INT slot, _In_ INT subslot, _Out_ LPDWORD pVersion);

//! @brief Disables the PCI device driver for the selected DPS card in a slot.
//!
//! This function disables the PCI device driver for the selected DPS card in a slot.  It should be called before using functions
//! such as dpsFpgaLoad() that affect the hardware used by the driver.
//! @param[in] slot    A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] subslot Always use 0 for a HVDPS and 1 for HVIL.  For an HDDPS,
//!                    0=mainboard, 1=daughterboard.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS dpsDeviceDisable(_In_ INT slot, _In_ INT subslot);

//! @brief Enables the PCI device driver for the selected DPS card in a slot.
//!
//! This function enables the PCI device driver for the selected DPS card in a slot.  It should be called after using functions
//! such as dpsFpgaLoad() that affect the hardware used by the driver.
//! @param[in] slot    A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] subslot Always use 0 for a HVDPS and 1 for HVIL.  For an HDDPS,
//!                    0=mainboard, 1=daughterboard.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS dpsDeviceEnable(_In_ INT slot, _In_ INT subslot);

//! @brief Loads an FPGA binary image file into an DPS card FPGA.
//!
//! This function loads an FPGA binary image file into an DPS card FPGA.  This image is volatile and the FPGA
//! will load its original base program from its SPI FLASH after a cold reboot.
//! @warning DPS device drivers are sensitive to FPGA changes.  If they are present, wrap this call in dpsDeviceDisable() and dpsDeviceEnable().
//! @param[in] slot    A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] subslot Always use 0 for a HVDPS and 1 for HVIL.  For an HDDPS,
//!                    0=mainboard, 1=daughterboard.
//! @param[in] filename An ANSI string containing an absolute or relative (to the current directory) FPGA binary filename.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS dpsFpgaLoad(_In_ INT slot, _In_ INT subslot, _In_z_ LPCSTR filename);

//! @brief Programs an FPGA image file into the base SPI FLASH used to boot initialize a DPS card's FPGA.
//!
//! This function programs an FPGA image file into the base SPI FLASH used to boot initialize an DPS card's FPGA.
//!
//! The callback and context parameters are used to report progress back during a long operation.  See #HIL_BASE_LOAD_CALLBACK
//! for details.
//! @param[in] slot    A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] subslot Always use 0 for a HVDPS and 1 for HVIL.  For an HDDPS,
//!                    0=mainboard, 1=daughterboard.
//! @param[in] filename An ANSI string containing an absolute or relative (to the current directory) FPGA binary filename.
//! @param[in] callback The callback function to be called with progress updates while programming the SPI FLASH.  Value can be NULL.
//! @param[in] context The context that will be passed back as an argument in the #HIL_BASE_LOAD_CALLBACK callback function.  Value can be NULL.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS dpsFpgaBaseLoad(_In_ INT slot, _In_ INT subslot, _In_z_ LPCSTR filename, _In_opt_ HIL_BASE_LOAD_CALLBACK callback, _In_opt_ void* context);

//! @brief Compares an existing image within the base SPI FLASH on a DPS card against an FPGA binary image file.
//!
//! This function compares an existing image within the base SPI FLASH on a DPS card against an FPGA binary image file
//! and optionally dumps that image into a separate binary file.
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] subslot Always use 0 for a HVDPS and 1 for HVIL.  For an HDDPS,
//!                    0=mainboard, 1=daughterboard.
//! @param[in] imageFilename An ANSI string containing an absolute or relative (to the current directory) FPGA binary filename to be compared against.
//! @param[in] dumpFilename An ANSI string containing an absolute or relative (to the current directory) binary dump filename.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS dpsFpgaBaseVerify(_In_ INT slot, _In_ INT subslot, _In_z_ LPCSTR imageFilename, _In_opt_z_ LPCSTR dumpFilename);

//! @brief Executes a Xilinx .XSVF file over a DPS card's JTAG interface.
//!
//! This function executes a Xilinx .XSVF file over a DPS card's JTAG interface.  A Xilinx CPLD is the only device
//! in the scan chain.  An appropriate .XSVF file can be generated through Xilinx's iMPACT software to perform
//! functions such as programming and/or verifying the content of the CPLD.
//! @param[in] slot    A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] subslot Always use 0 for a HVDPS and 1 for HVIL.  For an HDDPS,
//!                    0=mainboard, 1=daughterboard.
//! @param[in] filename An ANSI string containing an absolute or relative (to the current directory) .XSVF binary filename.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS dpsJtagExecute(_In_ INT slot, _In_ INT subslot, _In_z_ LPCSTR filename);

//! @brief Loads an FPGA binary image buffer into a DPS card's FPGA.
//!
//! This function loads an FPGA binary image buffer into a DPS card's FPGA.  This image is volatile and the FPGA
//! will load its original base program from its SPI FLASH after a cold reboot.
//! @warning HDDPS device drivers are sensitive to FPGA changes.  If they are present, wrap this call in dpsDeviceDisable() and dpsDeviceEnable().
//! @param[in] slot    A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] subslot Always use 0 for a HVDPS and 1 for HVIL.  For an HDDPS,
//!                    0=mainboard, 1=daughterboard.
//! @param[in] pData The FPGA image data.
//! @param[in] length The length of the FPGA image data in bytes.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS dpsFpgaBufferLoad(_In_ INT slot, _In_ INT subslot, _In_reads_bytes_(length) LPCVOID pData, _In_ DWORD length);

//! @brief Programs an FPGA image buffer into the base SPI FLASH used to boot initialize a DPS card's FPGA.
//!
//! This function programs an FPGA image buffer into the base SPI FLASH used to boot initialize a DPS card's FPGA.
//!
//! The callback and context parameters are used to report progress back during a long operation.  See #HIL_BASE_LOAD_CALLBACK
//! for details.
//! @param[in] slot    A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] subslot Always use 0 for a HVDPS and 1 for HVIL.  For an HDDPS,
//!                    0=mainboard, 1=daughterboard.
//! @param[in] pData The FPGA image data.
//! @param[in] length The length of the FPGA image data in bytes.
//! @param[in] callback The callback function to be called with progress updates while programming the SPI FLASH.  Value can be NULL.
//! @param[in] context The context that will be passed back as an argument in the #HIL_BASE_LOAD_CALLBACK callback function.  Value can be NULL.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS dpsFpgaBaseBufferLoad(_In_ INT slot, _In_ INT subslot, _In_reads_bytes_(length) LPCVOID pData, _In_ DWORD length, _In_opt_ HIL_BASE_LOAD_CALLBACK callback, _In_opt_ void* context);

//! @brief Execute a buffer containing Xilinx XSVF data over a DPS card's JTAG interface.
//!
//! This function executes a Xilinx XSVF data buffer over a DPS card's JTAG interface.  A Xilinx CPLD
//! is the only device in the scan chain.  XSVF data can be generated through Xilinx's iMPACT software to perform
//! functions such as programming and/or verifying the content of the CPLD.
//! @param[in] slot    A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] subslot Always use 0 for a HVDPS and 1 for HVIL.  For an HDDPS,
//!                    0=mainboard, 1=daughterboard.
//! @param[in] pData The XSVF image data.
//! @param[in] length The length of the XSVF image data in bytes.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS dpsJtagBufferExecute(_In_ INT slot, _In_ INT subslot, _In_reads_bytes_(length) LPCVOID pData, _In_ DWORD length);

//! @brief Reads a 32-bit register in one of the BAR regions of a DPS card.
//!
//! This function reads a 32-bit register in one of the BAR regions of a DPS card.
//! @param[in] slot    A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] subslot Always use 0 for a HVDPS and 1 for HVIL.  For an HDDPS,
//!                    0=mainboard, 1=daughterboard.
//! @param[in] bar The PCI base address register region to access.  Valid values are 0-2.
//! @param[in] offset The DWORD-aligned offset address in BAR0 memory to read.
//! @param[out] pData The address of a DWORD to contain the result.  It cannot be NULL.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS dpsBarRead(_In_ INT slot, _In_ INT subslot, _In_ UINT bar, _In_ DWORD offset, _Out_ LPDWORD pData);

//! @brief Writes a 32-bit register in one of the BAR regions of a DPS card.
//!
//! This function writes a 32-bit register in one the BAR regions of a DPS card.
//! @param[in] slot    A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] subslot Always use 0 for a HVDPS and 1 for HVIL.  For an HDDPS,
//!                    0=mainboard, 1=daughterboard.
//! @param[in] bar The PCI base address register region to access.  Valid values are 0-2.
//! @param[in] offset The DWORD-aligned offset address in BAR0 memory to write.
//! @param[in] data The DWORD value to write.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS dpsBarWrite(_In_ INT slot, _In_ INT subslot, _In_ UINT bar, _In_ DWORD offset, _In_ DWORD data);

//! @brief Reads from the DDR memory of a DPS card via DMA.
//!
//! This function reads from the DDR memory of a DPS card through its FPGA DMA interface.
//! @note The HVDPS mainboard has 1GB of memory, but implemented as two ports in the FPGA.  DMA across the 512MB boundary is not supported.
//! @param[in] slot    A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] subslot Always use 0 for a HVDPS and 1 for HVIL.  For an HDDPS,
//!                    0=mainboard, 1=daughterboard.
//! @param[in] address The zero-based address within the DDR memory to read.  This must be a multiple of four and the read must be within a 512MB address space.
//! @param[out] pData The address of a buffer to hold the result.
//! @param[in] length The length of the \c pData buffer in bytes.  This must be a multiple of four and the read must be within a 512MB address space.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS dpsDmaRead(_In_ INT slot, _In_ INT subslot, _In_ INT64 address, _Out_writes_bytes_all_(length) LPVOID pData, _In_ DWORD length);

//! @brief Writes to the DDR memory of a DPS card via DMA.
//!
//! This function writes to the DDR memory of a DPS card through its FPGA DMA interface.
//! @note The HVDPS mainboard has 1GB of memory, but implemented as two ports in the FPGA.  DMA across the 512MB boundary is not supported.
//! @param[in] slot    A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] subslot Always use 0 for a HVDPS and 1 for HVIL.  For an HDDPS,
//!                    0=mainboard, 1=daughterboard.
//! @param[in] address The zero-based address within the DDR memory to write.  This must be a multiple of four and the write must be within a 512MB address space.
//! @param[in] pData The address of a buffer containing the data to write.
//! @param[in] length The length of the \c pData buffer in bytes.  This must be a multiple of four and the write must be within a 512MB address space.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS dpsDmaWrite(_In_ INT slot, _In_ INT subslot, _In_ INT64 address, _In_reads_bytes_(length) const void* pData, _In_ DWORD length);

//! @brief Reads from the DDR memory of a DPS card using the FPGA register interface.
//!
//! This function reads from the DDR memory of a DPS card using the FPGA register interface.  It was the legacy interface before DMA access was supported.
//! @param[in] slot    A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] subslot Always use 0 for a HVDPS and 1 for HVIL.  For an HDDPS,
//!                    0=mainboard, 1=daughterboard.
//! @param[in] address The zero-based address within the DDR memory to read.  This must be a multiple of four and the read must be within a 512MB address space.
//! @param[out] pData The address of a buffer to hold the result.
//! @param[in] length The length of the \c pData buffer in bytes.  This must be a multiple of four and the read must be within a 512MB address space.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS dpsMemRead(_In_ INT slot, _In_ INT subslot, _In_ INT64 address, _Out_writes_bytes_all_(length) LPVOID pData, _In_ DWORD length);

//! @brief Writes to the DDR memory of a DPS card using the FPGA register interface.
//!
//! This function writes to the DDR memory of a DPS card using the FPGA register interface.  It was the legacy interface before DMA access was supported.
//! @param[in] slot    A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] subslot Always use 0 for a HVDPS and 1 for HVIL.  For an HDDPS,
//!                    0=mainboard, 1=daughterboard.
//! @param[in] address The zero-based address within the DDR memory to write.  This must be a multiple of four and the write must be within a 512MB address space.
//! @param[in] pData The address of a buffer containing the data to write.
//! @param[in] length The length of the \c pData buffer in bytes.  This must be a multiple of four and the write must be within a 512MB address space.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS dpsMemWrite(_In_ INT slot, _In_ INT subslot, _In_ INT64 address, _In_reads_bytes_(length) const void* pData, _In_ DWORD length);

//! @brief Reads the board-level traceability values from a DPS instrument.
//!
//! This function reads the board-level traceability values from a DPS instrument assembly.  The values are defined in the #BLT structure.
//! This data refers to a DPS card in the specified slot as a collective instrument, including daughtercards and/or snap-in cards.
//!
//! Use dpsBltBoardRead() to read the individual board BLTs.
//!
//! @param[in] slot  A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[out] pBlt The address of a #BLT structure that will be populated with the data.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS dpsBltInstrumentRead(_In_ INT slot, _Out_ PBLT pBlt);

//! @brief Writes the board-level traceability values for a DPS instrument.
//!
//! This function writes the board-level traceability values for a DPS instrument assembly.  The values are defined in the #BLT structure.
//! This data refers to a DPS card in the specified slot as a collective instrument, including daughtercards and/or snap-in cards.
//!
//! Use dpsBltBoardWrite() to write the individual board BLTs.
//!
//! The \c flags field allows this function to update individual values.  The flags can be ORed together.  Only the fields indicated by
//! \c flags need to be populated in the provided \c pBlt parameter.
//!
//! \par Updating the Full BLT - C
//! \code{.c}
//! #include <Windows.h>
//! #include "DpsApi.h"
//! #include "string.h"
//!
//! int main()
//! {
//!     int slot = 0;
//!     BLT blt;
//!     HIL_STATUS status;
//!     blt.Id = 0x010000FF;
//!     strcpy(blt.DeviceName,"DeviceName");
//!     strcpy(blt.VendorName,"INTEL");
//!     strcpy(blt.PartNumberAsBuilt,"IAxxxxxx-xxx");
//!     strcpy(blt.PartNumberCurrent,blt.PartNumberAsBuilt);
//!     strcpy(blt.SerialNumber,"12345678");
//!     strcpy(blt.ManufactureDate,"YYMMDD");
//!     status = dpsBltInstrumentWrite(slot, &blt, BF_ALL);
//!     if (status != HS_SUCCESS)
//!         return -1;
//!     return 0;
//! }
//! \endcode
//! \par Updating the Current Part Number - Python
//! \code{.py}
//! slot = 0
//! blt = hil.BLT()
//! blt.PartNumberCurrent = 'IAyyyyyy-yyy'
//! hil.dpsBltInstrumentWrite(slot,blt,hil.BF_PART_NUMBER_CURRENT)
//! # Note Python returns None for HIL_STATUS if successful and raises RuntimeError on failure.
//! \endcode
//! \par Erasing the Instrument BLT - Python
//! \code{.py}
//! slot = 8
//! hil.dpsBltInstrumentWrite(slot,None,0) # Note flags is ignored
//! \endcode
//!
//! \note HS_BLT_NOT_FOUND can be returned on an update (lack of #BF_ALL flag).
//!
//! @param[in] slot  A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] pBlt The address of a #BLT structure that contains the data to be written or updated.  If NULL, erases the existing instrument BLT.
//! @param[in] flags #BLT_FLAGS bits indicating what fields in \c pBlt are valid.  Ignored if pBlt is NULL.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS dpsBltInstrumentWrite(_In_ INT slot, _In_opt_ PCBLT pBlt, _In_ BLT_FLAGS flags);

//! @brief Reads the board-level traceability values from a DPS card.
//!
//! This function reads the board-level traceability values from a DPS card.
//!
//! @param[in] slot    A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] subslot Always use 0 for a HVDPS and 1 for HVIL.  For an HDDPS,
//!                    0=mainboard, 1=daughterboard.
//! @param[out] pBlt The address of a #BLT structure that will be populated with the data.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS dpsBltBoardRead(_In_ INT slot, _In_ INT subslot, _Out_ PBLT pBlt);

//! @brief Writes the board-level traceability values to a DPS card.
//!
//! This function writes the board-level traceability values to a DPS card.
//!
//! The \c flags field allows this function to update individual values.  The flags can be ORed together.  Only the fields indicated by
//! \c flags need to be populated in the provided \c pBlt parameter.
//!
//! \par Updating the Full BLT - C
//! \code{.c}
//! #include <Windows.h>
//! #include "DpsApi.h"
//! #include "string.h"
//!
//! int main()
//! {
//!     int slot = 0;
//!     int subslot = 1;
//!     BLT blt;
//!     HIL_STATUS status;
//!     blt.Id = 0x010000FF;
//!     strcpy(blt.DeviceName,"DeviceName");
//!     strcpy(blt.VendorName,"INTEL");
//!     strcpy(blt.PartNumberAsBuilt,"AAxxxxxx-xxx");
//!     strcpy(blt.PartNumberCurrent,blt.PartNumberAsBuilt);
//!     strcpy(blt.SerialNumber,"12345678");
//!     strcpy(blt.ManufactureDate,"YYMMDD");
//!     status = dpsBltBoardWrite(slot, subslot, &blt, BF_ALL);
//!     if (status != HS_SUCCESS)
//!         return -1;
//!     return 0;
//! }
//! \endcode
//! \par Updating the Current Part Number - Python
//! \code{.py}
//! slot = 0
//! subslot = 1
//! blt = hil.BLT()
//! blt.PartNumberCurrent = 'AAyyyyyy-yyy'
//! hil.dpsBltBoardWrite(slot,subslot,blt,hil.BF_PART_NUMBER_CURRENT)
//! # Note Python returns None for HIL_STATUS if successful and raises RuntimeError on failure.
//! \endcode
//!
//! \note HS_BLT_NOT_FOUND can be returned on an update (lack of #BF_ALL flag).
//!
//! @param[in] slot    A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] subslot Always use 0 for a HVDPS and 1 for HVIL.  For an HDDPS,
//!                    0=mainboard, 1=daughterboard.
//! @param[in] pBlt The address of a #BLT structure that contains the data to be written or updated.
//! @param[in] flags #BLT_FLAGS bits indicating what fields in \c pBlt are valid.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS dpsBltBoardWrite(_In_ INT slot, _In_ INT subslot, _In_ PCBLT pBlt, _In_ BLT_FLAGS flags);

//! @brief Reads the raw data from a DPS card's BLT EEPROM.
//!
//! This function reads the raw data from a DPS card's BLT EEPROM.
//!
//! @param[in] slot    A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] subslot Always use 0 for a HVDPS and 1 for HVIL.  For an HDDPS,
//!                    0=mainboard, 1=daughterboard.
//! @param[out] pEepromData The output buffer.
//! @param[in,out] pLength On input, the size of the output buffer referenced by \c pData.  On output, the size of the EEPROM data.
//! @retval HS_INSUFFICIENT_BUFFER The buffer provided was too small.  \c pLength will indicate the minimum buffer size required.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS dpsBltEepromRead(_In_ INT slot, _In_ INT subslot, _Out_writes_bytes_to_(_Old_(*pLength), *pLength) LPVOID pEepromData, _Inout_ LPDWORD pLength);

//! @brief Retrieves the user-mapped data streaming interface buffer for a DPS card.
//!
//! This function retrieves the user-mapped data streaming interface buffer for a DPS card.
//! See the Host Sensor Streaming and appropriate FPGA HLDs for details on the content of this buffer.
//! @param[in]  slot     A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in]  subslot  Always use 0 for a HVDPS and 1 for HVIL.  For an HDDPS,
//!                    0=mainboard, 1=daughterboard.
//! @param[out] pAddress The returned virtual address of the buffer.
//! @param[out] pSize    The returned size of the buffer in bytes.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS dpsStreamingBuffer(_In_ INT slot, _In_ INT subslot, _Out_ LPVOID* pAddress, _Out_ LPDWORD pSize);

//! @brief Writes data to be transmitted on the upward-facing trigger interface.
//!
//! This function transmits the data on the upward-facing trigger interface.
//! @param[in]  slot     A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in]  subslot Always use 0 for a HVDPS and 1 for HVIL.  For an HDDPS,
//!                    0=mainboard, 1=daughterboard.
//! @param[in]  data     The data written will be transmitted on the upward-facing trigger interface.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS dpsTriggerUpTest(_In_ INT slot, _In_ INT subslot, _In_ DWORD data);

//! @brief Reads the next trigger in the receive trigger debug FIFO.
//!
//! This function reads from this register and returns the next trigger in the receive trigger debug FIFO
//! @param[in]  slot     A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in]  subslot  Always use 0 for a HVDPS and 1 for HVIL.  For an HDDPS,
//!                    0=mainboard, 1=daughterboard.
//! @param[out] pData    The returned data of the next trigger in the receive trigger debug FIFO.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS dpsTriggerDownTest(_In_ INT slot, _In_ INT subslot, _Out_ LPDWORD pData);

#ifdef __cplusplus
}
#endif
