// INTEL CONFIDENTIAL
// Copyright 2017-2018 Intel Corporation All Rights Reserved.
//
// The source code contained or described herein and all documents related to
// the source code ("Material") are owned by Intel Corporation or its
// suppliers or licensors. Title to the Material remains with Intel
// Corporation or its suppliers and licensors. The Material contains trade
// secrets and proprietary and confidential information of Intel or its
// suppliers and licensors. The Material is protected by worldwide copyright
// and trade secret laws and treaty provisions. No part of the Material may be
// used, copied, reproduced, modified, published, uploaded, posted,
// transmitted, distributed, or disclosed in any way without Intel's prior
// express written permission.
//
// No license under any patent, copyright, trade secret or other intellectual
// property right is granted to or conferred upon you by disclosure or
// delivery of the Materials, either expressly, by implication, inducement,
// estoppel or otherwise. Any license under such intellectual property rights
// must be express and approved by Intel in writing.

//! @file
//! @brief APIs to control an HDMT HDDPS card.
#pragma once
#include "HilDefs.h"

#ifdef __cplusplus
extern "C" {
#endif

//! @brief Verifies an HDMT HDDPS card is present.
//!
//! This function verifies an HDMT Gen 2 digital power supply card (HDDPS) is present.  It connects to and caches driver resources for use
//! by other \c hddpsXXXXX functions.  hddpsDisconnect() is its complementary function and frees any cached resources.
//!
//! Neither hddpsConnect() or hddpsDisconnect() are required to be called to use the other \c hddpsXXXXX functions.  All
//! \c hddpsXXXXX functions will allocate resources if they are not already allocated and free them if required to
//! perform their function.
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] subslot 0 = motherboard (low current), 1 = daughterboard (high current).
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hddpsConnect(_In_ INT slot, _In_ INT subslot);

//! @brief Frees any resources cached from using the HDDPS card functions.
//!
//! This function frees any resources associated with using the HDDPS card HIL functions. hddpsConnect() is its
//! complementary function.
//!
//! Neither hddpsConnect() or hddpsDisconnect() are required to be called to use the other \c hddpsXXXXX functions.  All
//! \c hddpsXXXXX functions will allocate resources if they are not already allocated and free them if required to
//! perform their function.
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] subslot 0 = motherboard (low current), 1 = daughterboard (high current).
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hddpsDisconnect(_In_ INT slot, _In_ INT subslot);

//! @brief Programs the vendor and product IDs of all USB devices on an HDDPS card.
//!
//! This function programs the vendor and product IDs (VID/PIDs) of the FT240X and Cypress
//! USB devices on an HDDPS motherboard or daughterboard. Correct VID/PIDs are required to ensure Windows Device
//! Manager displays a proper description of the device, and HIL requires correct VID/PIDs to ensure
//! software can locate and communicate with the correct component.
//! @note FTDI devices have a limited number of updates, but this function is safe to call because it
//! only writes if the component programming is incorrect.
//! @param[in] slot A physical slot number.  Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] subslot 0 = motherboard (low current), 1 = daughterboard (high current).
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hddpsVidPidsSet(_In_ INT slot, _In_ INT subslot);

//! @brief Directly executes a read command on a MAX6662 of an HDDPS card.
//!
//! This function executes a read command on a MAX6662 of an HDDPS card.
//!
//! @param[in] slot      A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] subslot   Use 0 for motherboard, 1 for daughterboard.
//! @param[in] index     A physical index number. Consult the following table.
//!                 Note that reference designators may change in new fabs.
//!                 | Index | HDDPS MB | HDDPS DB |
//!                 | :---: | :------: | :------: |
//!                 |   0   |   U75    |   U81    |
//!                 |   1   |   U76    |   U82    |
//! @param[in] cmd A valid read command for the Maxim Integrated MAX6662.  See datasheet for specifics.
//!                 Read commands (from datasheet) are:
//!                 | Command Byte | Function                    |
//!                 | :----------: | :-------------------------: |
//!                 |     C1h      | Read Temperature register   |
//!                 |     C3h      | Read Configuration register |
//!                 |     C5h      | Read THYST register         |
//!                 |     C7h      | Read TMAX register          |
//!                 |     C9h      | Read TLOW register          |
//!                 |     CBh      | Read THIGH register         |
//! @param[out] pData The 16-bit data from the command.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hddpsMax6662Read(_In_ INT slot, _In_ INT subslot, _In_ UINT index, _In_ BYTE cmd, _Out_ LPWORD pData);

//! @brief Directly executes a write command on a MAX6662 of an HDDPS card.
//!
//! This function executes a write command on a MAX6662 of an HDDPS card.
//!
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] subslot Use 0 for motherboard, 1 for daughterboard.
//! @param[in] index A physical index number. Consult the following table.
//!                 Note that reference designators may change in new fabs.
//!                 | Index | HDDPS MB | HDDPS DB |
//!                 | :---: | :------: | :------: |
//!                 |   0   |   U75    |   U81    |
//!                 |   1   |   U76    |   U82    |
//! @param[in] cmd A valid write command for the Maxim Integrated MAX6662.  See datasheet for specifics.
//!                 Write commands (from datasheet) are:
//!                 | Command Byte | Function                     |
//!                 | :----------: | :--------------------------: |
//!                 |     83h      | Write Configuration register |
//!                 |     85h      | Write THYST register         |
//!                 |     87h      | Write TMAX register          |
//!                 |     89h      | Write TLOW register          |
//!                 |     8Bh      | Write THIGH register         |
//! @param[in] data The 16-bit data to write.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hddpsMax6662Write(_In_ INT slot, _In_ INT subslot, _In_ UINT index, _In_ BYTE cmd, _In_ WORD data);

//! @brief Reads the internal temperature monitor for one of two MAX6662 on an HDDPS motherboard or daughterboard.
//!
//! This function Reads the internal temperature monitor for one of two MAX6662 on an HDDPS motherboard or daughterboard.
//!
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] subslot Use 0 for motherboard, 1 for daughterboard.
//! @param[in] index A physical index number. Consult the following table.
//!                 Note that reference designators may change in new fabs.
//!                 | Index | HDDPS MB | HDDPS DB |
//!                 | :---: | :------: | :------: |
//!                 |   0   |   U75    |   U81    |
//!                 |   1   |   U76    |   U82    |
//! @param[out] pTemp The temperature in degrees Celsius.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hddpsMax6662TmonRead(_In_ INT slot, _In_ INT subslot, _In_ UINT index, _Out_ double* pTemp);

//! @brief Reads a register on an AD5764 of an HDDPS card.
//!
//! This function reads a register on an AD5764 of an HDDPS card.
//! @param[in] slot    A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] subslot 0 = motherboard (low current), 1 = daughterboard (high current).
//! @param[in] index   The index of the AD5764 to read. Consult the following table:
//!                    | Register |  HDDPS Fab D Ref. |
//!                    | :------: | :---------------: |
//!                    |    0     |        U93        |
//!                    |    1     |        U43        |
//!                    |    2     |        U94        |
//! @param[in] reg     The register value (REG2,REG1,REG0) to be read.  See the data sheet for the AD5764.
//!                    Valid Register values (from datasheet) are:
//!                    | Register | Function             |
//!                    | :------: | :------------------: |
//!                    |    0     | Function register    |
//!                    |    2     | Data register        |
//!                    |    3     | Coarse Gain register |
//!                    |    4     | Fine Gain register   |
//!                    |    5     | Offset register      |
//! @param[in] dac     A valid selection of the DAC address bits (A2,A1,A0) for the AD5764.  See datasheet for specifics.
//!                    Valid DAC channel selections (from datasheet) are as follows.  Note that \c dac is used for another purpose when
//!                    used with the function register.
//!                    | Register | DAC      | Function register meaning |
//!                    | :------: | :------: | :-----------------------: |
//!                    |    0     | DAC A    | NOP, data = don't care.   |
//!                    |    1     | DAC B    | See data sheet.           |
//!                    |    2     | DAC C    |            -              |
//!                    |    3     | DAC D    |            -              |
//!                    |    4     | All DACs | Clear, data = don't care. |
//!                    |    5     |    -     | Load, data = don't care.  |
//! @param[out] pData The 16-bit data to read.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hddpsAd5764Read(_In_ INT slot, _In_ INT subslot,  _In_ UINT index, _In_ UINT reg, _In_ UINT dac, _Out_ LPWORD pData);

//! @brief Writes a register on an AD5764 of an HDDPS card.
//!
//! This function writes a register on an AD5764 of an HDDPS card.
//! @param[in] slot    A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] subslot 0 = motherboard (low current), 1 = daughterboard (high current).
//! @param[in] index   The index of the AD5764 to write. Consult the following table:
//!                    | Register | HDDPS Fab D Ref.  |
//!                    | :------: | :---------------: |
//!                    |    0     |        U93        |
//!                    |    1     |        U43        |
//!                    |    2     |        U94        |
//! @param[in] reg     The register value (REG2,REG1,REG0) to be written.  See the data sheet for the AD5764.
//!                    Valid Register values (from datasheet) are:
//!                    | Register | Function             |
//!                    | :------: | :------------------: |
//!                    |    0     | Function register    |
//!                    |    2     | Data register        |
//!                    |    3     | Coarse Gain register |
//!                    |    4     | Fine Gain register   |
//!                    |    5     | Offset register      |
//! @param[in] dac     A valid selection of the DAC address bits (A2,A1,A0) for the AD5764.  See datasheet for specifics.
//!                    Valid DAC channel selections (from datasheet) are as follows.  Note that \c dac is used for another purpose when
//!                    used with the function register.
//!                    | Register | DAC      | Function register meaning |
//!                    | :------: | :------: | :-----------------------: |
//!                    |    0     | DAC A    | NOP, data = don't care.   |
//!                    |    1     | DAC B    | See data sheet.           |
//!                    |    2     | DAC C    |            -              |
//!                    |    3     | DAC D    |            -              |
//!                    |    4     | All DACs | Clear, data = don't care. |
//!                    |    5     |    -     | Load, data = don't care.  |
//! @param[in] data The 16-bit data to write.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hddpsAd5764Write(_In_ INT slot, _In_ INT subslot,  _In_ UINT index, _In_ UINT reg, _In_ UINT dac, _In_ WORD data);

//! @brief Reads a 16-bit register of an AD5560 device on an HDDPS card.
//!
//! This function reads a 16-bit register of an AD5560 device on an HDDPS card.
//! \par C Example
//! \code{.c}
//!  HIL_STATUS result;
//!  INT slot = 0;
//!  INT subslot = 0;
//!  UINT rail = 0;
//!  BYTE address = 0;
//!  WORD data;
//!  result = hddpsAd5560Read(slot, subslot, rail, address, &data);
//!  if(result != HS_SUCCESS)
//!      /* report failure */
//! \endcode
//! \par Python Example
//! \code{.py}
//! import hil
//! slot,subslot,rail,address = 0,0,0,0
//! data = hil.hddpsAd5560Read(slot,subslot,rail,address) # output parameter returned, RuntimeError raised on failure.
//! \endcode
//! @param[in]  slot    A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in]  subslot subslot 0 = motherboard (low current), 1 = daughterboard (high current).
//! @param[in]  rail    The rail number of the AD5560 to control.  Valid values are 0-9.
//! @param[in]  address A valid 7-bit register address for the AD5560.
//! @param[out] pData   The 16-bit register contents.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hddpsAd5560Read(_In_ INT slot, _In_ INT subslot,  _In_ UINT rail, _In_ BYTE address, _Out_ LPWORD pData);

//! @brief Writes a 16-bit register of an AD5560 device on an HDDPS card.
//!
//! This function writes a 16-bit register of an AD5560 device on an HDDPS card.
//! \par C Example
//! \code{.c}
//!  HIL_STATUS result;
//!  INT slot = 0;
//!  INT subslot = 0;
//!  UINT rail = 0;
//!  BYTE address = 0;
//!  WORD data = 0x1234;
//!  result = hddpsAd5560Write(slot, subslot, rail, address, data);
//!  if(result != HS_SUCCESS)
//!      /* report failure */
//! \endcode
//! \par Python Example
//! \code{.py}
//! import hil
//! slot,subslot,rail,address,data = 0,0,0,0,0x1234
//! hil.hddpsAd5560Write(slot,subslot,rail,address,data) # RuntimeError raised on failure.
//! \endcode
//! @param[in] slot    A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] subslot 0 = motherboard (low current), 1 = daughterboard (high current).
//! @param[in]  rail    The rail number of the AD5560 to control.  Valid values are 0-9.
//! @param[in] address A valid 7-bit register address for the AD5560.
//! @param[in] data    The 16-bit value to write to the register.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hddpsAd5560Write(_In_ INT slot, _In_ INT subslot, _In_ UINT rail, _In_ BYTE address, _In_ WORD data);

//! @brief Reads a rail voltage on an HDDPS card.
//!
//! This function reads a rail voltage on an HDDPS card.
//! @param[in] slot    A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] subslot 0 = motherboard (low current), 1 = daughterboard (high current).
//! @param[in]  rail     The rail number to read.  Valid values are 0-9.
//! @param[out] pVoltage A pointer to double that receives the voltage in volts.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hddpsRailVoltageRead(_In_ INT slot, _In_ INT subslot, _In_ UINT rail, _Out_ double* pVoltage);

//! @brief Reads a rail current on an HDDPS card.
//!
//! This function reads a rail current on an HDDPS card.
//! @param[in] slot    A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] subslot 0 = motherboard (low current), 1 = daughterboard (high current).
//! @param[in]  rail     The rail number to read.  Valid values are 0-9
//! @param[out] pCurrent A pointer to double that receives the current in amps.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hddpsRailCurrentRead(_In_ INT slot, _In_ INT subslot, _In_ UINT rail, _Out_ double* pCurrent);

//! @brief Reads a VLC rail voltage on a HDDPS mainboard.
//!
//! This function reads a VLC rail voltage on a HDDPS mainboard.
//! @param[in] slot      A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in]  rail     The rail number to read.  Valid values are 0-15.
//! @param[out] pVoltage A pointer to double that receives the voltage in volts.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hddpsVlcRailVoltageRead(_In_ INT slot, _In_ UINT rail, _Out_ double* pVoltage);

//! @brief Reads a VLC rail current on a HDDPS mainboard.
//!
//! This function reads a VLC rail current on a HDDPS mainboard.
//! @param[in] slot      A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in]  rail     The rail number to read.  Valid values are 0-15.
//! @param[out] pCurrent A pointer to double that receives the current in amps.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hddpsVlcRailCurrentRead(_In_ INT slot, _In_ UINT rail, _Out_ double* pCurrent);

#ifdef __cplusplus
}
#endif
