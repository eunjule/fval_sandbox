// INTEL CONFIDENTIAL
// Copyright 2016-2018 Intel Corporation All Rights Reserved.
//
// The source code contained or described herein and all documents related to
// the source code ("Material") are owned by Intel Corporation or its
// suppliers or licensors. Title to the Material remains with Intel
// Corporation or its suppliers and licensors. The Material contains trade
// secrets and proprietary and confidential information of Intel or its
// suppliers and licensors. The Material is protected by worldwide copyright
// and trade secret laws and treaty provisions. No part of the Material may be
// used, copied, reproduced, modified, published, uploaded, posted,
// transmitted, distributed, or disclosed in any way without Intel's prior
// express written permission.
//
// No license under any patent, copyright, trade secret or other intellectual
// property right is granted to or conferred upon you by disclosure or
// delivery of the Materials, either expressly, by implication, inducement,
// estoppel or otherwise. Any license under such intellectual property rights
// must be express and approved by Intel in writing.

//! @file
//! @brief APIs to control the HDMT front panel board.
#pragma once
#include "HilDefs.h"

#ifdef __cplusplus
extern "C" {
#endif

//! @brief Initializes the components of the front panel for operation.
//!
//! This function initializes the components of the front panel for operation.  Calling this
//! function is required before using most other \c fpXXXXXX() functions.
//! This function also calls rcRootI2cLock() and rcRootI2cUnlock() during its execution since it accesses
//! devices on the root I2C bus.  If additional I2C commands need to be atomically executed on the root I2C
//! bus, then manually call rcRootI2cLock() and rcRootI2cUnlock() around the entire root I2C bus access.
//!
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS fpInit(void);

//! @brief Reads one of four fan tachometer channels on the front panel board.
//!
//! This function reads one of four fan tachometer channels on the front panel board.
//!
//! @param[in] channel Selects the tachometer channel from the following table.
//!                    Note that reference designators may change in new fabs.
//! | Channel | Tachometer  | Fab A |
//! | :-----: | :---------: | :---: |
//! | 0       | TACH_FAN0   | J1    |
//! | 1       | TACH_FAN1   | J3    |
//! | 2       | TACH_FAN2   | J5    |
//! | 3       | TACH_FAN3   | J6    |
//! @param[out] pTach The fan speed in revolutions per second (RPS).
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS fpFanRead(_In_ INT channel, _Out_ double* pTach);

//! @brief Reads the relative humidity sensor on the front panel board.
//!
//! This function reads the relative humidity sensor on the front panel board.
//!
//! @param[out] pHumidity The current relative humidity as a percentage (0-100).
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS fpHumidityRead(_Out_ double* pHumidity);

//! @brief Computes the dewpoint based on the current humidity and air temperature.
//!
//! This function computes the dewpoint based on the current humidity and air temperature,
//! as read by fpHumidityRead() and fpTmonRead().  If this value nears or exceeds an fpTmonRead() of the
//! pre-chilled water (PCW) temperature, condensation in the tester may occur.
//!
//! @param[out] pDewpoint The calculated dewpoint of the tester in degrees Celsius.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS fpDewpointRead(_Out_ double* pDewpoint);

//! @brief Reads one of the temperature monitors on the front panel board.
//!
//! This function reads one of the temperature monitors on the front panel board.
//! @note fpInit() is required to be called before using this function.
//! @param[in] channel Selects the temperature channel to read from the following table:
//! | Channel | Temperature                         |
//! | :-----: | :------------------------           |
//! | 0       | Pre-chilled water (PCW)             |
//! | 1       | Air                                 |
//! | 2       | Adt7411(U13) internal temperature   |
//! | 3       | Adt7411(U22) internal temperature   |
//! @param[out] pTemp The temperature in degrees Celsius.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS fpTmonRead(_In_ INT channel, _Out_ double* pTemp);

//! @brief Reads one of the voltage monitor channels on the front panel.
//!
//! This function reads one of the voltage monitor channels on the front panel.
//! @note fpInit() is required to be called before using this function.
//! @param[in] channel Selects the monitoring channel. Valid values are 0 to 6.  The channels
//! and corresponding voltages monitored are (per Front Panel fab A schematic):
//!                    | Channel | Reference           | Notes                   |
//!                    | :-----: | :-----------------: | :---------------------: |
//!                    | 0       | U13 ADT7411 VDD     |                         |
//!                    | 1       | U13 ADT7411 AIN7    | VBAT_MON                |
//!                    | 2       | U22 ADT7411 VDD     |                         |
//!                    | 3       | U22 ADT7411 AIN1    | THERM_SENSE+            |
//!                    | 4       | U22 ADT7411 AIN2    | MOIST_SENSE+_BUF        |
//!                    | 5       | U22 ADT7411 AIN3    | SHROUD_MOIST_SENSE+_BUF |
//!                    | 6       | U16 LTC2453 voltage | HDL_PRESENT             |
//! @param[out] pVoltage The voltage in volts.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS fpVmonRead(_In_ UINT channel, _Out_ double* pVoltage);

//! @brief Directly reads an 8-bit register of the MAX6651 on the front panel board.
//!
//! This function reads an 8-bit register of the MAX6651 on the front panel board.  This function
//! is for debugging only.  Prefer to use other specialized \c fpXXXXX functions instead.
//! @param[in] reg A valid register address for the Maxim Integrated MAX6651.
//! @param[out] pData The register contents.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS fpMax6651Read(_In_ BYTE reg, _Out_ PBYTE pData);

//! @brief Directly writes an 8-bit register of the MAX6651 on the front panel board.
//!
//! This function writes an 8-bit register of the MAX6651 on the front panel board.  This function
//! is for debugging only.  Prefer to use other specialized \c fpXXXXX functions instead.
//! @param[in] reg A valid register address for the Maxim Integrated MAX6651.
//! @param[in] data The value to write.  Valid values are 0-255 (00h-FFh).
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS fpMax6651Write(_In_ BYTE reg, BYTE _In_ data);

//! @brief Directly reads an 8-bit register on an ADT7411 on the front panel board.
//!
//! This function reads an 8-bit register on an ADT7411 on the front panel board.  This function
//! is for debugging only.  Prefer to use other specialized \c fpXXXXX functions instead.
//! @param[in] chip A zero-based chip index.  Consult the following table.
//!                 Note that reference designators may change in new fabs.
//!                 | Chip    | Fab A |
//!                 | :-----: | :---: |
//!                 |    0    | U13   |
//!                 |    1    | U22   |
//! @param[in] reg A valid register address for the Analog Devices ADT7411.
//! @param[out] pData The register contents.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS fpAdt7411Read(_In_ INT chip, _In_ BYTE reg, _Out_ PBYTE pData);

//! @brief Directly writes an 8-bit register on an ADT7411 on the front panel board.
//!
//! This function writes an 8-bit register on an ADT7411 on the front panel board.  This function
//! is for debugging only.  Prefer to use other specialized \c fpXXXXX functions instead.
//! @param[in] chip A zero-based chip index.  Consult the following table.
//!                 Note that reference designators may change in new fabs.
//!                 | Chip    | Fab A |
//!                 | :-----: | :---: |
//!                 |    0    | U13   |
//!                 |    1    | U22   |
//! @param[in] reg A valid register address for the Analog Devices ADT7411.
//! @param[in] data The data to write.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS fpAdt7411Write(_In_ INT chip, _In_ BYTE reg, _In_ BYTE data);

//! @brief Executes a command within the SHT21 (humidity and temperature) device.
//!
//! This function executes a command within the SHT21 (humidity and temperature) device.
//! This function is for debugging only.  Prefer to use other specialized \c fpXXXXX functions instead.
//!
//! @param[in] pCmdData   A data buffer of bytes to write to the SHT21 device via the root I2C bus.  Note
//!                       that the bytes are sent to the device sequentially starting with the first
//!                       byte pointed to by \c pCmdData.  The first byte in this buffer must always
//!                       be a command byte (see the SHT21 datasheet for details).
//! @param[in] cmdLength  The byte length of the \c pCmdData buffer.  Note that this parameter cannot be
//!                       zero since there must always be at least one byte (the command byte) in \c pCmdData.
//! @param[out] pReadData A data buffer of bytes to hold the data read from the SHT21 device via the
//!                       root I2C bus.  Note that the bytes received from the device are located sequentially
//!                       in the buffer in the order they were received with the first byte being located
//!                       directly at \c pReadData.  This parameter can be NULL if the command being sent
//!                       has a \c readLength of zero.
//! @param[in] readLength The byte length of the \c pReadData buffer.  Note that this parameter can be zero if
//!                       the command being sent has no bytes of read back from the device.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS fpSht21Execute(
    _In_reads_bytes_(cmdLength) const void* pCmdData, _In_ DWORD cmdLength,
    _Out_writes_bytes_all_opt_(readLength) LPVOID pReadData, _In_ DWORD readLength);

//! @brief Directly reads an 8-bit register of the PCA9555 on the front panel board.
//!
//! This function reads an 8-bit register from one of the PCA9555s on the front panel board.  This function
//! is for debugging only.  Prefer to use other specialized \c fpXXXXX functions instead.
//! @param[in] chip A zero-based chip index.  Consult the following table.
//!                 Note that reference designators may change in new fabs.
//!                 | Chip    | Fab A |
//!                 | :-----: | :---: |
//!                 |    0    | U4    |
//!                 |    1    | U20   |
//! @param[in] reg A valid register address for the Texas Instruments PCA9555.
//! @param[out] pData The register contents.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS fpPca9555Read(_In_ INT chip, _In_ BYTE reg, _Out_ PBYTE pData);

//! @brief Directly writes an 8-bit register of the PCA9555 on the front panel board.
//!
//! This function writes an 8-bit register of the PCA9555 on the front panel board.  This function
//! is for debugging only.  Prefer to use other specialized \c fpXXXXX functions instead.
//! @param[in] chip A zero-based chip index.  Consult the following table.
//!                 Note that reference designators may change in new fabs.
//!                 | Chip    | Fab A |
//!                 | :-----: | :---: |
//!                 |    0    | U4    |
//!                 |    1    | U20   |
//! @param[in] reg A valid register address for the Texas Instruments PCA9555.
//! @param[in] data The data to write.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS fpPca9555Write(_In_ INT chip, _In_ BYTE reg, _In_ BYTE data);

//! @brief Reads the board-level traceability values from the front panel board.
//!
//! This function reads the board-level traceability values from the front panel board.  The values are defined in the #BLT structure.
//!
//! @param[out] pBlt The address of a #BLT structure that will be populated with the data.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS fpBltBoardRead(_Out_ PBLT pBlt);

//! @brief Writes the board-level traceability values of the front panel board.
//!
//! This function writes the board-level traceability values of the front panel board.  The values are defined in the #BLT structure.
//!
//! The \c flags field allows this function to update individual values.  The flags can be ORed together.  Only the fields indicated by
//! \c flags need to be populated in the provided \c pBlt parameter.
//!
//! \par Updating the Full BLT - C
//! \code{.c}
//! #include <Windows.h>
//! #include "FrontPanelApi.h"
//! #include "string.h"
//!
//! int main()
//! {
//!     int slot = 8;
//!     BLT blt;
//!     HIL_STATUS status;
//!     blt.Id = 0x1000000;
//!     strcpy(blt.DeviceName,"DeviceName");
//!     strcpy(blt.VendorName,"INTEL");
//!     strcpy(blt.PartNumberAsBuilt,"AAxxxxxx-xxx");
//!     strcpy(blt.PartNumberCurrent,blt.PartNumberAsBuilt);
//!     strcpy(blt.SerialNumber,"12345678");
//!     strcpy(blt.ManufactureDate,"YYMMDD");
//!     status = fpBltBoardWrite(&blt, BF_ALL);
//!     if (status != HS_SUCCESS)
//!         return -1;
//!     return 0;
//! }
//! \endcode
//! \par Updating the Current Part Number - Python
//! \code{.py}
//! slot = 8
//! blt = hil.BLT()
//! blt.PartNumberCurrent = 'AAyyyyyy-yyy'
//! hil.fpBltBoardWrite(blt,hil.BF_PART_NUMBER_CURRENT)
//! # Note Python returns None for HIL_STATUS if successful and raises RuntimeError on failure.
//! \endcode
//!
//! \note HS_BLT_NOT_FOUND can be returned on an update (lack of #BF_ALL flag).
//!
//! @param[in] pBlt The address of a #BLT structure that contains the data to be written or updated.
//! @param[in] flags #BLT_FLAGS bits indicating what fields in \c pBlt are valid.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS fpBltBoardWrite(_In_ PCBLT pBlt, _In_ BLT_FLAGS flags);

//! @brief Reads bytes from the front panel 24LC515 EEPROM.
//!
//! This function reads bytes from the front panel 24LC515 EEPROM.  It is a 64KB device, accessed as two 32KB areas
//! using I2C addresses 0xAE and 0xA6.  Because the schematic documents 0xAE as the address of the device, historically
//! the address used for this function uses 0x0000-0x7FFF as the 0xAE-addressed region, and 0x8000-0xFFFF as the 0xA6-addressed
//! region.
//!
//! @param[in]  address The address to begin reading from the EEPROM.  Valid values are 0x0000-0xFFFF.
//! @param[out] pData   The output buffer.  It cannot be NULL.
//! @param[in]  length  The amount of data to read.  \c address plus \c length cannot exceed 0x10000 (64KB).
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS fpEepromRead(UINT address, _Out_writes_bytes_all_(length) LPVOID pData, _In_ DWORD length);

//! @brief Writes bytes to the front panel 24LC515 EEPROM.
//!
//! This function reads bytes from the front panel 24LC515 EEPROM.  It is a 64KB device, accessed as two 32KB areas
//! using I2C addresses 0xAE and 0xA6.  Because the schematic documents 0xAE as the address of the device, historically
//! the address used for this function uses 0x0000-0x7FFF as the 0xAE-addressed region, and 0x8000-0xFFFF as the 0xA6-addressed
//! region.
//!
//! @param[in]  address The address to begin writing to the EEPROM.  Valid values are 0x0000-0xFFFF.
//! @param[in]  pData   The input buffer.  It cannot be NULL.
//! @param[in]  length  The amount of data to write.  \c address plus \c length cannot exceed 0x10000 (64KB).
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS fpEepromWrite(UINT address, _In_reads_bytes_(length) const void* pData, _In_ DWORD length);

//! @brief Reads the system identifier from the front panel.
//!
//! This function reads the system identifier from the front panel.
//!
//! @param[out] pSysId A buffer for the returned system ID.  It cannot be NULL.  A buffer of #HIL_MAX_ID_STRING_SIZE is guaranteed
//!                    to be large enough to hold the identifier.
//! @param[in] length  The size of the \c pSysId buffer in bytes.
//! @retval HS_SUCCESS The system ID was returned successfully.
//! @retval HS_INSUFFICIENT_BUFFER The \c length of the \c pSysId buffer was insufficient to hold the result.
HIL_API HIL_STATUS fpSysIdRead(_Out_writes_z_(length) LPSTR pSysId, _In_ DWORD length);

//! @brief Writes a system identifier to the front panel.
//!
//! This function writes a system identifier to the front panel.
//!
//! @param[in] pSysId  A pointer to a null-terminated system ID string no longer than #HIL_MAX_ID_STRING_SIZE including the null.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS fpSysIdWrite(_In_z_ LPCSTR pSysId);

//! @brief Reads the material equipment system identifier from the front panel.
//!
//! This function reads the material equipment system(MES) identifier from the front panel.
//!
//! @param[out] pMesId A buffer for the returned MES ID.  It cannot be NULL.  A buffer of #HIL_MAX_ID_STRING_SIZE is guaranteed
//!                    to be large enough to hold the identifier.
//! @param[in] length  The size of the \c pMesId buffer in bytes.
//! @retval HS_SUCCESS The MES ID was returned successfully.
//! @retval HS_INSUFFICIENT_BUFFER The \c length of the \c pMesId buffer was insufficient to hold the result.
HIL_API HIL_STATUS fpMesIdRead(_Out_writes_z_(length) LPSTR pMesId, _In_ DWORD length);

//! @brief Writes a material equipment system identifier to the front panel.
//!
//! This function writes a material equipment system(MES) identifier to the front panel.
//!
//! @param[in] pMesId  A pointer to a null-terminated MES ID string no longer than #HIL_MAX_ID_STRING_SIZE including the null.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS fpMesIdWrite(_In_z_ LPCSTR pMesId);

#ifdef __cplusplus
}
#endif
