// INTEL CONFIDENTIAL
// Copyright 2016-2020 Intel Corporation All Rights Reserved.
//
// The source code contained or described herein and all documents related to
// the source code ("Material") are owned by Intel Corporation or its
// suppliers or licensors. Title to the Material remains with Intel
// Corporation or its suppliers and licensors. The Material contains trade
// secrets and proprietary and confidential information of Intel or its
// suppliers and licensors. The Material is protected by worldwide copyright
// and trade secret laws and treaty provisions. No part of the Material may be
// used, copied, reproduced, modified, published, uploaded, posted,
// transmitted, distributed, or disclosed in any way without Intel's prior
// express written permission.
//
// No license under any patent, copyright, trade secret or other intellectual
// property right is granted to or conferred upon you by disclosure or
// delivery of the Materials, either expressly, by implication, inducement,
// estoppel or otherwise. Any license under such intellectual property rights
// must be express and approved by Intel in writing.

//! @file
//! @brief APIs to control the HDMT HPCC calibration card.
#pragma once
#include "HilDefs.h"

#ifdef __cplusplus
extern "C" {
#endif

//! @brief Verifies an HDMT HPCC calibration card is present.
//!
//! This function verifies an HDMT HPCC calibration card is present in the requested slot.  It connects to and caches
//! driver resources for use by other \c calHpccXXXXX functions.  calHpccDisconnect() is its complementary function and
//! frees any cached resources.
//!
//! Note that calHpccConnect() does not need to be called to use the other \c calHpccXXXXX functions.  All \c calHpccXXXXX
//! functions will allocate resources if they are not already allocated.  However, the only way to free all resources
//! that have been allocated is to call calHpccDisconnect().
//! @note This call will fail to detect the calibration card if the FTDI components have not been programmed with their Intel vendor and device IDs.
//! @param[in] slot A physical slot number.  Valid values are 0 to \ref HIL_SLOTS - 1.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS calHpccConnect(_In_ INT slot);

//! @brief Frees any resources cached from using the HPCC calibration card functions.
//!
//! This function frees any resources associated with using the HPCC calibration card HIL functions.  calHpccConnect() is its
//! complementary function.
//!
//! Note that the only way to free all resources that have been allocated is to call calHpccDisconnect().
//! @param[in] slot A physical slot number.  Valid values are 0 to \ref HIL_SLOTS - 1.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS calHpccDisconnect(_In_ INT slot);

//! @brief Programs the vendor and product IDs of all USB devices on an HPCC calibration card.
//!
//! This function programs the vendor and product IDs (VID/PIDs) of the FT4232, FT2232 and FT240X
//! USB devices on an HPCC calibration card.  Correct VID/PIDs are required to ensure Windows Device Manager
//! displays a proper description of the device,
//! and HIL requires correct VID/PIDs to ensure software can locate and communicate with the correct component.
//!
//! Note that there are two additional FT240X devices on a
//! calibration card intended to be used for testing USB paths through the VHDD connector from the HPCC instrument,
//! but one is inaccessible for all current HPCCs and the other is only visible in Windows Device Manager
//! if the HPCC/HPCC2 in the matching instrument slot is present and supports the required VHDD connector path.
//! This API, if it can see the additional device, will ensure it has the FTDI default VID and PID.
//! @note FTDI devices have a limited number of updates, but this function is safe to call because it
//! only writes if the component programming is incorrect.
//! @param[in] slot A physical slot number.  Valid values are 0 to \ref HIL_SLOTS - 1.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS calHpccVidPidsSet(_In_ INT slot);

//! @brief Initializes the components of a HPCC calibration card for operation.
//!
//! This function initializes the components of a HPCC calibration card for operation.  Calling this
//! function is required before using most other \c calHpccXXXXXX() functions.
//!
//! @param[in] slot A physical slot number.  Valid values are 0 to \ref HIL_SLOTS - 1.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS calHpccInit(_In_ INT slot);

//! @brief Reads an HPCC calibration card CPLD version number.
//!
//! This function reads the CPLD version number from an HPCC calibration card.
//!
//! @param[in] slot A physical slot number.  Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[out] pVersion Returns the version register value.  The pointer must not be NULL.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS calHpccCpldVersion(_In_ INT slot, _Out_ LPDWORD pVersion);

//! @brief Reads an HPCC calibration card's CPLD version string.
//!
//! This function reads the version register of an HPCC calibration card's CPLD and converts it to an ASCII string representation.
//!
//! @param[in] slot A physical slot number.  Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[out] pVersion Returns the version string.  The pointer must not be NULL.
//! @param[in] length The length of the \c pVersion buffer.  It should be at least five characters to hold a null-terminated version string.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS calHpccCpldVersionString(_In_ INT slot, _Out_writes_z_(length) LPSTR pVersion, _In_ DWORD length);

//! @brief Executes a Xilinx .XSVF file over an HPCC calibration card's JTAG interface.
//!
//! This function executes a Xilinx .XSVF file over an HPCC calibration card's JTAG interface.  A Xilinx CPLD is the only device
//! in the scan chain.  An appropriate .XSVF file can be generated through Xilinx's iMPACT software to perform
//! functions such as programming and/or verifying the content of the CPLD.
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] filename An ANSI string containing an absolute or relative (to the current directory) CPLD .XSVF binary filename.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS calHpccJtagExecute(_In_ INT slot, _In_z_ LPCSTR filename);

//! @brief Execute a buffer containing Xilinx XSVF data over an HPCC calibration card's JTAG interface.
//!
//! This function executes a Xilinx XSVF data buffer over an HPCC calibration card's JTAG interface.  A Xilinx CPLD
//! is the only device in the scan chain.  XSVF data can be generated through Xilinx's iMPACT software to perform
//! functions such as programming and/or verifying the content of the CPLD.
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] pData The CPLD image data.
//! @param[in] length The length of the CPLD image data in bytes.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS calHpccJtagBufferExecute(_In_ INT slot, _In_reads_bytes_(length) LPCVOID pData, _In_ DWORD length);

//! @brief Reads the instrument-level traceability values from an HPCC calibration board.
//!
//! This function reads the instrument-level traceability values from an HPCC calibration board.  The values are defined in the #BLT structure.
//!
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[out] pBlt The address of a #BLT structure that will be populated with the data.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS calHpccBltInstrumentRead(_In_ INT slot, _Out_ PBLT pBlt);

//! @brief Writes the instrument-level traceability values for an HPCC calibration board.
//!
//! This function writes the instrument-level traceability values for an HPCC calibration board.  The values are defined in the #BLT structure.
//!
//! The \c flags field allows this function to update individual values.  The flags can be ORed together.  Only the fields indicated by
//! \c flags need to be populated in the provided \c pBlt parameter.
//!
//! \par Updating the Full BLT - C
//! \code{.c}
//! #include <Windows.h>
//! #include "CalHpccApi.h"
//! #include "string.h"
//!
//! int main()
//! {
//!     int slot = 8;
//!     BLT blt;
//!     HIL_STATUS status;
//!     blt.Id = 0x10000FF;
//!     strcpy(blt.DeviceName,"DeviceName");
//!     strcpy(blt.VendorName,"INTEL");
//!     strcpy(blt.PartNumberAsBuilt,"AAxxxxxx-xxx");
//!     strcpy(blt.PartNumberCurrent,blt.PartNumberAsBuilt);
//!     strcpy(blt.SerialNumber,"12345678");
//!     strcpy(blt.ManufactureDate,"YYMMDD");
//!     status = calHpccBltInstrumentWrite(slot, &blt, BF_ALL);
//!     if (status != HS_SUCCESS)
//!         return -1;
//!     return 0;
//! }
//! \endcode
//! \par Updating the Current Part Number - Python
//! \code{.py}
//! slot = 8
//! blt = hil.BLT()
//! blt.PartNumberCurrent = 'AAyyyyyy-yyy'
//! hil.calHpccBltInstrumentWrite(slot,blt,hil.BF_PART_NUMBER_CURRENT)
//! # Note Python returns None for HIL_STATUS if successful and raises RuntimeError on failure.
//! \endcode
//! \par Erasing the Instrument BLT - Python
//! \code{.py}
//! slot = 8
//! hil.calHpccBltInstrumentWrite(slot,None,0) # Note flags is ignored.
//! \endcode
//!
//! \note HS_BLT_NOT_FOUND can be returned on an update (lack of #BF_ALL flag).
//!
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] pBlt The address of a #BLT structure that contains the data to be written or updated.  If NULL, erases the existing instrument BLT.
//! @param[in] flags #BLT_FLAGS bits indicating what fields in \c pBlt are valid.  Ignored if pBlt is NULL.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS calHpccBltInstrumentWrite(_In_ INT slot, _In_opt_ PCBLT pBlt, _In_ BLT_FLAGS flags);

//! @brief Reads the board-level traceability values from an HPCC calibration board.
//!
//! This function reads the board-level traceability values from an HPCC calibration board.  The values are defined in the #BLT structure.
//!
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[out] pBlt The address of a #BLT structure that will be populated with the data.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS calHpccBltBoardRead(_In_ INT slot, _Out_ PBLT pBlt);

//! @brief Writes the board-level traceability values for an HPCC calibration board.
//!
//! This function writes the board-level traceability values for an HPCC calibration board.  The values are defined in the #BLT structure.
//!
//! The \c flags field allows this function to update individual values.  The flags can be ORed together.  Only the fields indicated by
//! \c flags need to be populated in the provided \c pBlt parameter.
//!
//! \par Updating the Full BLT - C
//! \code{.c}
//! #include <Windows.h>
//! #include "CalHpccApi.h"
//! #include "string.h"
//!
//! int main()
//! {
//!     int slot = 8;
//!     BLT blt;
//!     HIL_STATUS status;
//!     blt.Id = 0x1000039;
//!     strcpy(blt.DeviceName,"DeviceName");
//!     strcpy(blt.VendorName,"INTEL");
//!     strcpy(blt.PartNumberAsBuilt,"AAxxxxxx-xxx");
//!     strcpy(blt.PartNumberCurrent,blt.PartNumberAsBuilt);
//!     strcpy(blt.SerialNumber,"12345678");
//!     strcpy(blt.ManufactureDate,"YYMMDD");
//!     status = calHpccBltBoardWrite(slot, &blt, BF_ALL);
//!     if (status != HS_SUCCESS)
//!         return -1;
//!     return 0;
//! }
//! \endcode
//! \par Updating the Current Part Number - Python
//! \code{.py}
//! slot = 8
//! blt = hil.BLT()
//! blt.PartNumberCurrent = 'AAyyyyyy-yyy'
//! hil.calHpccBltBoardWrite(slot,blt,hil.BF_PART_NUMBER_CURRENT)
//! # Note Python returns None for HIL_STATUS if successful and raises RuntimeError on failure.
//! \endcode
//!
//! \note HS_BLT_NOT_FOUND can be returned on an update (lack of #BF_ALL flag).
//!
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] pBlt The address of a #BLT structure that contains the data to be written or updated.
//! @param[in] flags #BLT_FLAGS bits indicating what fields in \c pBlt are valid.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS calHpccBltBoardWrite(_In_ INT slot, _In_ PCBLT pBlt, _In_ BLT_FLAGS flags);

//! @brief Reads the raw data from the specified HPCC calibration card's BLT EEPROM.
//!
//! This function reads the raw data from the specified HPCC calibration card's BLT EEPROM.
//!
//! @param[in] slot A physical slot number.  Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[out] pEepromData The output buffer.
//! @param[in,out] pLength On input, the size of the output buffer referenced by \c pData.  On output, the size of the EEPROM data.
//! @retval HS_INSUFFICIENT_BUFFER The buffer provided was too small.  \c pLength will indicate the minimum buffer size required.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS calHpccBltEepromRead(_In_ INT slot, _Out_writes_bytes_to_(_Old_(*pLength), *pLength) LPVOID pEepromData, _Inout_ LPDWORD pLength);

//! @brief Writes data to the 24LC512 (EEPROM) device on an HPCC calibration card.
//!
//! This function writes data to the 24LC512 (EEPROM) device on an HPCC calibration card.
//! @note Calling calHpccInit() is required before using this function.
//! @param[in] slot    A physical slot number.  Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] address The location of the first byte in the EEPROM to which the data in the \c pData buffer will be written.
//! @param[in] pData   A data buffer containing data to write to the EEPROM device.
//! @param[in] length  The byte length of the \c pData buffer.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS calHpccEepromWrite(_In_ INT slot, _In_ DWORD address, _In_reads_bytes_(length) const void* pData, _In_ DWORD length);

//! @brief Reads data from the 24LC512 (EEPROM) device on an HPCC calibration card.
//!
//! This function reads data from the 24LC512 (EEPROM) device on an HPCC calibration card.
//! @note Calling calHpccInit() is required before using this function.
//! @param[in] slot    A physical slot number.  Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] address The location of the first byte in the EEPROM from which the data will be read.
//! @param[out] pData  A pointer to a data buffer in which the read data will be stored. Note that this buffer must have at least \c length
//!                    allocated in order to complete successfully.
//! @param[in] length  The number of bytes to read from the EEPROM.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS calHpccEepromRead(_In_ INT slot, _In_ DWORD address, _Out_writes_bytes_all_(length) LPVOID pData, _In_ DWORD length);

//! @brief Writes a byte of data to a register within the PCA9506 (GPIO Expander) device on an HPCC calibration card.
//!
//! This function writes a byte of data to a register within the PCA9506 (GPIO Expander) device on an HPCC calibration card.
//! @note Calling calHpccInit() is required before using this function.
//! @param[in] slot A physical slot number.  Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] reg  A writable PCA9506 register address (0x08-0x0C, 0x10-0x14, 0x18-0x1C, 0x20-0x24).
//! @param[in] data The data value to write.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS calHpccPca9506Write(_In_ INT slot, _In_ BYTE reg, _In_ BYTE data);

//! @brief Reads a byte of data from a register within the PCA9506 (GPIO Expander) device on an HPCC calibration card.
//!
//! This function reads a byte of data from a register within the PCA9506 (GPIO Expander) device on an HPCC calibration card.
//! @note Calling calHpccInit() is required before using this function.
//! @param[in] slot A physical slot number.  Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] reg A readable PCA9506 register address (0x00-0x04, 0x08-0x0C, 0x10-0x14, 0x18-0x1C, 0x20-0x24).
//! @param[out] pData The data value read back.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS calHpccPca9506Read(_In_ INT slot, _In_ BYTE reg, _Out_ PBYTE pData);

//! @brief Writes data to a register within the LMK01000 (Clock Distributor) device on an HPCC calibration card.
//!
//! This function writes data to a register within the LMK01000 (Clock Distributor) device on an HPCC calibration card.
//! @note Calling calHpccInit() is required before using this function.
//! @param[in] slot A physical slot number.  Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] reg  A valid LMK01000 register address.
//! @param[in] data The data value to write.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS calHpccLmk01000Write(_In_ INT slot, _In_ BYTE reg, _In_ DWORD data);

//! @brief Enables/Disables all clock outputs of the LMK01000 device on an HPCC calibration card.
//!
//! This function enables/disables all clock outputs of the LMK01000 device on an HPCC calibration card.
//! @note Calling calHpccInit() is required before using this function.
//! @param[in] slot   A physical slot number.  Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] enable Sets the Global Output Enable (GOE) pin of the LMK01000 to Enabled if TRUE or Disabled if FALSE.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS calHpccLmk01000ClocksEnable(_In_ INT slot, _In_ BOOL enable);

//! @brief Synchronizes all clock outputs of the LMK01000 device on an HPCC calibration card.
//!
//! This function synchronizes all clock outputs of the LMK01000 device on an HPCC calibration card.
//! @note Calling calHpccInit() is required before using this function.
//! @note Clocks in the bypassed state are not affected by SYNC* and are always synchronized with the divided outputs.
//! @param[in] slot  A physical slot number.  Valid values are 0 to \ref HIL_SLOTS - 1.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS calHpccLmk01000ClocksSync(_In_ INT slot);

//! @brief Writes a byte of data to a register within the PCA9554 (8-Bit GPIO Expander) device on an HPCC calibration card.
//!
//! This function writes a byte of data to a register within the PCA9554 (8-Bit GPIO Expander) device on an HPCC calibration card.
//! @note Calling calHpccInit() is required before using this function.
//! @param[in] slot A physical slot number.  Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] reg PCA9554 register address.  Valid values are 0-3.
//! @param[in] data The data value to write.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS calHpccPca9554Write(_In_ INT slot, _In_ BYTE reg, _In_ BYTE data);

//! @brief Reads a byte of data from a register within the PCA9554 (8-Bit GPIO Expander) device on an HPCC calibration card.
//!
//! This function reads a byte of data from a register within the PCA9554 (8-Bit GPIO Expander) device on an HPCC calibration card.
//! @note Calling calHpccInit() is required before using this function.
//! @param[in] slot A physical slot number.  Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] reg PCA9554 register address.  Valid values are 0-3.
//! @param[out] pData The data value read back.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS calHpccPca9554Read(_In_ INT slot, _In_ BYTE reg, _Out_ PBYTE pData);

//! @brief Sets the specified reference voltage level for the window comparator (HMC974LC3C) device on an HPCC calibration card.
//!
//! This function sets the specified reference voltage level for the window comparator (HMC974LC3C) device on an HPCC calibration card using LTC1592 DACs.
//! @note Calling calHpccInit() is required before using this function.
//! @note Due to implementation details, an input reference level is recommended to be within &plusmn;2.0 volts.
//! @param[in]  slot   A physical slot number.  Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in]  chip   Selects the DAC associated with a reference level. Consult the following table.
//!                    | Chip    | Comment                         |
//!                    | :-----: | :------------------------------ |
//!                    |    0    | Reference Top (Input to RT)     |
//!                    |    1    | Reference Bottom (Input to RB)  |
//! @param[in] voltage Desired value of reference voltage for Window Comparator in volts.
//! @returns \ref HIL_STATUS
//! @see calHpccWindowCompOutputEnable()\n
//!      calHpccWindowCompOutputSelect()\n
//!      calHpccWindowCompOutputRead()
HIL_API HIL_STATUS calHpccWindowCompRefSet(_In_ INT slot, _In_ UINT chip, _In_ double voltage);

//! @brief Enables the selected data output from the CPLD for the window comparator (HMC974LC3C) on an HPCC calibration card.
//!
//! This function enables sending the data selected by tiuCalBaseWindowCompOutputSelect() from the CPLD for the window comparator
//! (HMC974LC3C) on an HPCC calibration card.
//! @note Calling calHpccInit() is required before using this function.
//! @param[in] slot   A physical slot number.  Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] enable Sends the data from the CPLD for the selected windows comparator output if TRUE or sends the ~CPLD_WOUTB if FALSE.
//! @returns \ref HIL_STATUS
//! @see calHpccWindowCompRefSet()\n
//!      calHpccWindowCompOutputSelect()\n
//!      calHpccWindowCompOutputRead()
HIL_API HIL_STATUS calHpccWindowCompOutputEnable(_In_ INT slot, _In_ BOOL enable);

//! @brief Selects which output signal of the window comparator (HMC974LC3C) will be sent from the CPLD on an HPCC calibration card.
//!
//! This function selects which output signal of the window comparator (HMC974LC3C) device will be sent from the CPLD on an HPCC calibration card.
//! @note Calling calHpccInit() is required before using this function.
//! @param[in] slot A physical slot number.  Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] select The window comparator output for CPLD to send. Consult the following table:
//!                   | Select | Description |
//!                   | :----: | :---------: |
//!                   | 0      | WOUTB       |
//!                   | 1      | ORB         |
//!                   | 2      | URB         |
//! @returns \ref HIL_STATUS
//! @see calHpccWindowCompRefSet()\n
//!      calHpccWindowCompOutputEnable()\n
//!      calHpccWindowCompOutputRead()
HIL_API HIL_STATUS calHpccWindowCompOutputSelect(_In_ INT slot, _In_ BYTE select);

//! @brief Reads bytes of selected data output from the CPLD for the window comparator (HMC974LC3C) on an HPCC calibration card.
//!
//! This function reads bytes of selected data output from the CPLD for the window comparator (HMC974LC3C) on an HPCC calibration card.
//! Each byte contains 8 1-bit samples.
//! @note Calling calHpccInit() is required before using this function.
//! @param[in] slot    A physical slot number.  Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[out] pData  The output buffer.  It cannot be NULL.
//! @param[in]  length The amount of data to read in bytes (minimum 1).
//! @returns \ref HIL_STATUS
//! @see calHpccWindowCompRefSet()\n
//!      calHpccWindowCompOutputEnable()\n
//!      calHpccWindowCompOutputSelect()
HIL_API HIL_STATUS calHpccWindowCompOutputRead(_In_ INT slot, _Out_writes_bytes_all_(length) LPVOID pData, _In_ DWORD length);

//! @brief Sets delay values for two channels within the SY89297U device on an HPCC calibration card.
//!
//! This function sets delay values for two channels within the SY89297U device on an HPCC calibration card.
//! @note Calling calHpccInit() is required before using this function.
//! @param[in] slot   A physical slot number.  Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] delayA Desired value of delay for channel A. Valid range is 0 - 5115ps (rounds to the nearest 5ps increment).
//! @param[in] delayB Desired value of delay for channel B. Valid range is 0 - 5115ps (rounds to the nearest 5ps increment).
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS calHpccDelayLineSet(_In_ INT slot, _In_ UINT delayA, _In_ UINT delayB);

//! @brief Sets operating mode for the LTC2485 (24-Bit ADC) device on an HPCC calibration card.
//!
//! This function sets operating mode for the LTC2485 (24-Bit ADC) device on an HPCC calibration card.
//! @note Calling calHpccInit() is required before using this function.
//! @param[in] slot A physical slot number.  Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] mode One of the valid modes of operation of a LTC2485 (24-Bit ADC) device. Consult the following table.
//! | LTC2485 Mode | Description                                                  |
//! | :--:         | :----------------------------------------------------------- |
//! | 0            | External Input, 50Hz and 60Hz Rejection, Auto-Calibration    |
//! | 1            | External Input, 50Hz and 60Hz Rejection, 2x Speed            |
//! | 2            | External Input, 50Hz Rejection, Auto-Calibration             |
//! | 3            | External Input, 50Hz Rejection, 2x Speed                     |
//! | 4            | External Input, 60Hz Rejection, Auto-Calibration             |
//! | 5            | External Input, 60Hz Rejection, 2x Speed                     |
//! | 8            | Temperature Input, 50Hz and 60Hz Rejection, Auto-Calibration |
//! | 10           | Temperature Input, 50Hz Rejection, Auto-Calibration          |
//! | 11           | Temperature Input, 50Hz Rejection, Auto-Calibration          |
//! | 12           | Temperature Input, 60Hz Rejection, Auto-Calibration          |
//! | 13           | Temperature Input, 60Hz Rejection, Auto-Calibration          |
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS calHpccLtc2485ModeSet(_In_ INT slot, _In_ BYTE mode);

//! @brief Reads a voltage value from the LTC2485 (24-Bit ADC) device on an HPCC calibration card.
//!
//! This function reads a voltage value from the LTC2485 (24-Bit ADC) device on an HPCC calibration card.
//! @note Calling calHpccInit() is required before using this function.
//! @param[in]  slot     A physical slot number.  Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[out] pVoltage A pointer to a double that receives the voltage (in volts).
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS calHpccLtc2485VoltageRead(_In_ INT slot, _Out_ double* pVoltage);

#ifdef __cplusplus
}
#endif
