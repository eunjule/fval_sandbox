// INTEL CONFIDENTIAL
// Copyright 2017-2018 Intel Corporation All Rights Reserved.
//
// The source code contained or described herein and all documents related to
// the source code ("Material") are owned by Intel Corporation or its
// suppliers or licensors. Title to the Material remains with Intel
// Corporation or its suppliers and licensors. The Material contains trade
// secrets and proprietary and confidential information of Intel or its
// suppliers and licensors. The Material is protected by worldwide copyright
// and trade secret laws and treaty provisions. No part of the Material may be
// used, copied, reproduced, modified, published, uploaded, posted,
// transmitted, distributed, or disclosed in any way without Intel's prior
// express written permission.
//
// No license under any patent, copyright, trade secret or other intellectual
// property right is granted to or conferred upon you by disclosure or
// delivery of the Materials, either expressly, by implication, inducement,
// estoppel or otherwise. Any license under such intellectual property rights
// must be express and approved by Intel in writing.

//! @file
//! @brief Non-function definitions such as constants and structures used by the HDMT HPCC calibration snap-in card APIs.
#pragma once

//! @brief Clock select definitions for HPCC calibation snap-in card.
//!
//! These are the available clocks that can be selected though the U1, EU4 and EU5 clock multiplexers to
//! the SNAPIN_DOWN line of an HPCC calibration snap-in card.  See HDMT_SNAPIN_CAL Fab A (AAH58187-100) schematic, page 10.
typedef enum CAL_HPCC_SNIC_CLOCK
{
    CHSC_LVDS_CLK_PIN1_PR12,
    CHSC_LVDS_CLK_PIN2_PR10,
    CHSC_LVDS_CLK_SNP_HSSD_CLKA,
    CHSC_LVDS_CLK_SNP_HSSD_CLKB,
    CHSC_LVDS_CLK_SNAPIN_CLK0,
    CHSC_LVDS_CLK_MSCLK_SNAPIN_PR17,
    CHSC_LVPECL_CLK_LMK_CLK7,
    CHSC_LVDS_CLK_LMK_HP_A,
    CHSC_LVDS_CLK_LMK_HP_B,
    CHSC_LVDS_CLK_LMK_HP_C
} CAL_HPCC_SNIC_CLOCK;
