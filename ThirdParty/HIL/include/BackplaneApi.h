// INTEL CONFIDENTIAL
// Copyright 2014-2018 Intel Corporation All Rights Reserved.
//
// The source code contained or described herein and all documents related to
// the source code ("Material") are owned by Intel Corporation or its
// suppliers or licensors. Title to the Material remains with Intel
// Corporation or its suppliers and licensors. The Material contains trade
// secrets and proprietary and confidential information of Intel or its
// suppliers and licensors. The Material is protected by worldwide copyright
// and trade secret laws and treaty provisions. No part of the Material may be
// used, copied, reproduced, modified, published, uploaded, posted,
// transmitted, distributed, or disclosed in any way without Intel's prior
// express written permission.
//
// No license under any patent, copyright, trade secret or other intellectual
// property right is granted to or conferred upon you by disclosure or
// delivery of the Materials, either expressly, by implication, inducement,
// estoppel or otherwise. Any license under such intellectual property rights
// must be express and approved by Intel in writing.

//! @file
//! @brief APIs to control the HDMT Backplane.
#pragma once
#include "HilDefs.h"
#include "BackplaneDefs.h"

#ifdef __cplusplus
extern "C" {
#endif

//! @brief Initializes the components of the backplane for operation.
//!
//! This function initializes the components of the backplane for operation.  Calling this
//! function is required before using most other \c bpXXXXXX() functions.
//! This function also calls rcRootI2cLock() and rcRootI2cUnlock() during its execution since it accesses
//! devices on the root I2C bus.  If additional I2C commands need to be atomically executed on the root I2C
//! bus, then manually call rcRootI2cLock() and rcRootI2cUnlock() around the entire root I2C bus access.
//!
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS bpInit(void);

//! @brief Routes the root I2C bus traffic on the backplane to selected device paths.
//!
//! This function routes the root I2C bus traffic on the backplane to selected device paths.
//! For efficiency, an internal cache remembers the switch settings and will only send the command
//! if the cache indicates the requested path is not already set.  It is possible for external programs to
//! modify the hardware without HIL's knowledge.  If this is suspected, call rcRootI2cCacheInvalidate() to
//! invalidate the cache.
//!
//! This function calls rcRootI2cLock() and rcRootI2cUnlock() during its execution.  If additional
//! I2C commands require the switch selections to be maintained, manually call rcRootI2cLock() and
//! rcRootI2cUnlock() around the entire root I2C bus access.
//!
//! @param[in] channels The bit encoded value of the I2C channels to switch onto the root I2C bus.  See the
//!                     #BP_ROOT_I2C_CHANNELS encoding for all possible encoded channels that can be ORed together.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS bpRootI2cSwitchSelect(_In_ BYTE channels);

//! @brief Writes a byte of data to a register within the specified PCA9555 (GPIO Expander) device.
//!
//! This function writes a byte of data to a register within the specified PCA9555 (GPIO Expander) device.
//! This function also calls rcRootI2cLock() and rcRootI2cUnlock() during its execution since it accesses
//! devices on the root I2C bus.  If additional I2C commands need to be atomically executed on the root I2C
//! bus, then manually call rcRootI2cLock() and rcRootI2cUnlock() around the entire root I2C bus access.
//! @note bpInit() is required to be called before using this function.
//! @param[in] reg PCA9555 register address.
//! @param[in] data The data value to write.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS bpPca9555Write(_In_ BYTE reg, _In_ BYTE data);

//! @brief Reads a byte of data from a register within the specified PCA9555 (GPIO Expander) device.
//!
//! This function reads a byte of data from a register within the specified PCA9555 (GPIO Expander) device.
//! This function also calls rcRootI2cLock() and rcRootI2cUnlock() during its execution since it accesses
//! devices on the root I2C bus.  If additional I2C commands need to be atomically executed on the root I2C
//! bus, then manually call rcRootI2cLock() and rcRootI2cUnlock() around the entire root I2C bus access.
//! @note bpInit() is required to be called before using this function.
//! @param[in] reg PCA9555 register address.
//! @param[out] pData The data value read back.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS bpPca9555Read(_In_ BYTE reg, _Out_ PBYTE pData);

//! @brief Reads one of twenty-seven voltage monitor channels on the backplane.
//!
//! This function reads one of twenty-seven voltage monitor channels on the backplane.
//! @note bpInit() is required to be called before using this function.
//! @param[in] channel Selects the monitoring channel. Valid values are 0 to 26.  The channels
//! and corresponding voltages monitored are (per backplane fab C/D schematic):
//!                    | Channel |  Chip  | Reference                 | Notes                                                 |
//!                    | :-----: |  :---: | :----------------------:  | :-------------------------                            |
//!                    | 0       |  U30   |  3.3V_USB                 | VDD for U30                                           |
//!                    | 1       |  U30   |  48V_AUX_FUSE_MON_IN      |                                                       |
//!                    | 2       |  U30   |  12V_AUX_20A_MON_IN       |                                                       |
//!                    | 3       |  U30   |  5V_AUX_20A_MON_IN        |                                                       |
//!                    | 4       |  U30   |  3.3V_AUX_12A_MON_IN      |                                                       |
//!                    | 5       |  U30   |  12V_SYS_MON              |                                                       |
//!                    | 6       |  U30   |  3.3V_SYS_15A_MON         |                                                       |
//!                    | 7       |  U30   |  CHGNDA                   |  Chassis Ground                                       |
//!                    | 8       |  U30   |  No connect               |                                                       |
//!                    | 9       |  U13   |  3.3V_USB                 |  VDD for U13                                          |
//!                    | 10      |  U13   |  PRESENT_FUSE_0           |  3.3V - No Card,2.2V-Card Good,1.1V - Card Fuse Blown |
//!                    | 11      |  U13   |  PRESENT_FUSE_1           |  3.3V - No Card,2.2V-Card Good,1.1V - Card Fuse Blown |
//!                    | 12      |  U13   |  PRESENT_FUSE_2           |  3.3V - No Card,2.2V-Card Good,1.1V - Card Fuse Blown |
//!                    | 13      |  U13   |  PRESENT_FUSE_3           |  3.3V - No Card,2.2V-Card Good,1.1V - Card Fuse Blown |
//!                    | 14      |  U13   |  PRESENT_FUSE_4           |  3.3V - No Card,2.2V-Card Good,1.1V - Card Fuse Blown |
//!                    | 15      |  U13   |  PRESENT_FUSE_5           |  3.3V - No Card,2.2V-Card Good,1.1V - Card Fuse Blown |
//!                    | 16      |  U13   |  PRESENT_FUSE_6           |  3.3V - No Card,2.2V-Card Good,1.1V - Card Fuse Blown |
//!                    | 17      |  U13   |  PRESENT_FUSE_7           |  3.3V - No Card,2.2V-Card Good,1.1V - Card Fuse Blown |
//!                    | 18      |  U14   |  3.3V_USB                 |  VDD for U14                                          |
//!                    | 19      |  U14   |  PRESENT_FUSE_8           |  3.3V - No Card,2.2V-Card Good,1.1V - Card Fuse Blown |
//!                    | 20      |  U14   |  PRESENT_FUSE_9           |  3.3V - No Card,2.2V-Card Good,1.1V - Card Fuse Blown |
//!                    | 21      |  U14   |  PRESENT_FUSE_10          |  3.3V - No Card,2.2V-Card Good,1.1V - Card Fuse Blown |
//!                    | 22      |  U14   |  PRESENT_FUSE_11          |  3.3V - No Card,2.2V-Card Good,1.1V - Card Fuse Blown |
//!                    | 23      |  U14   |  PRESENT_FUSE_RCTC        |  3.3V - No Card,2.2V-Card Good,1.1V - Card Fuse Blown |
//!                    | 24      |  U14   |  JUMPER_DETECT_ADC2       |                                                       |
//!                    | 25      |  U14   |  JUMPER_DETECT_ADC1       |                                                       |
//!                    | 26      |  U14   |  48V_RET                  |                                                       |
//! @param[out] pVoltage The voltage in volts.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS bpVmonRead(_In_ INT channel, _Out_ double* pVoltage);

//! @brief Reads one of the temperature monitors on the backplane.
//!
//! This function reads one of the temperature monitors on the backplane.
//! @note bpInit() is required to be called before using this function.
//! @param[in] channel A zero-based channel index.  Consult the following table.
//!                 Note that reference designators may change in new fabs.
//!                 | Channel | Fab D                             |
//!                 | :-----: | :-------------------------------: |
//!                 |    0    | ADT7411 U30 internal temperature. |
//!                 |    1    | ADT7411 U13 internal temperature. |
//!                 |    2    | ADT7411 U14 internal temperature. |
//! @param[out] pTemp The temperature in degrees Celsius.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS bpTmonRead(_In_ UINT channel, _Out_ double* pTemp);

//! @brief Directly writes an 8-bit register on an ADT7411 on the backplane.
//!
//! This function writes an 8-bit register on an ADT7411 on the backplane.
//! This function also calls rcRootI2cLock() and rcRootI2cUnlock() during its execution since it accesses
//! devices on the root I2C bus.  If additional I2C commands need to be atomically executed on the root I2C
//! bus, then manually call rcRootI2cLock() and rcRootI2cUnlock() around the entire root I2C bus access.
//! @note bpInit() is required to be called before using this function.
//! @param[in] chip A zero-based chip index.  Consult the following table.
//!                 Note that reference designators may change in new fabs.
//!                 | Chip    | Fab D |
//!                 | :-----: | :---: |
//!                 |    0    | U30   |
//!                 |    1    | U13   |
//!                 |    2    | U14   |
//! @param[in] reg A valid register address for the Analog Devices ADT7411.
//! @param[in] data The data to write.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS bpAdt7411Write(_In_ INT chip, _In_ BYTE reg, _In_ BYTE data);

//! @brief Directly reads an 8-bit register on an ADT7411 on the backplane.
//!
//! This function reads an 8-bit register on an ADT7411 on the backplane.
//! This function also calls rcRootI2cLock() and rcRootI2cUnlock() during its execution since it accesses
//! devices on the root I2C bus.  If additional I2C commands need to be atomically executed on the root I2C
//! bus, then manually call rcRootI2cLock() and rcRootI2cUnlock() around the entire root I2C bus access.
//! @note bpInit() is required to be called before using this function.
//! @param[in] chip A zero-based chip index.  Consult the following table.
//!                 Note that reference designators may change in new fabs.
//!                 | Chip    | Fab D |
//!                 | :-----: | :---: |
//!                 |    0    | U30   |
//!                 |    1    | U13   |
//!                 |    2    | U14   |
//! @param[in] reg A valid register address for the Analog Devices ADT7411.
//! @param[out] pData The register contents.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS bpAdt7411Read(_In_ INT chip, _In_ BYTE reg, _Out_ PBYTE pData);

//! @brief Writes a command within the BMR454 (Intermediate Bus Converter) device.
//!
//! This function writes a command to the BMR454 (Intermediate Bus Converter) device.
//! This function also calls rcRootI2cLock() and rcRootI2cUnlock() during its execution since it accesses
//! devices on the root I2C bus.  If additional I2C commands need to be atomically executed on the root I2C
//! bus, then manually call rcRootI2cLock() and rcRootI2cUnlock() around the entire root I2C bus access.
//!
//! @note The power to the TIU must be enabled by calling bpTiuAuxPowerEnable() before using this function.
//!       If bpTiuAuxPowerEnable() is not called, then the BMR454 is not active and does not respond to commands.
//! @note Some BMR454 command writes cause the device to NACK further commands for some amount of time (sometimes as
//!       long as 500ms).
//!
//! @param[in] command   BMR454 command to write.
//! @param[in] pCmdData  A data buffer of bytes to write to the BMR454 device via the I2C bus for the associated
//!                      \c command.  Note that the bytes are sent to the device sequentially starting with the first
//!                      byte pointed to by \c pCmdData.  Also note that the I2C address and the PEC (Packet
//!                      Error Checking) bytes should not be included in \c pCmdData because this is handled
//!                      automatically within the API.  Lastly, the command byte should not be included in this
//!                      buffer as it is already supplied in \c command.  Note that this parameter may be
//!                      NULL for commands that have no data associated with them.
//! @param[in] cmdLength The byte length of the \c pCmdData buffer.  Note that this parameter may be
//!                      0 for commands that have no data.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS bpBmr454Write(_In_ BYTE command, _In_reads_bytes_opt_(cmdLength) const void* pCmdData, _In_ DWORD cmdLength);

//! @brief Reads a command within the BMR454 (Intermediate Bus Converter) device.
//!
//! This function reads a command from the BMR454 (Intermediate Bus Converter) device.
//! This function also calls rcRootI2cLock() and rcRootI2cUnlock() during its execution since it accesses
//! devices on the root I2C bus.  If additional I2C commands need to be atomically executed on the root I2C
//! bus, then manually call rcRootI2cLock() and rcRootI2cUnlock() around the entire root I2C bus access.
//!
//! @note The power to the TIU must be enabled by calling bpTiuAuxPowerEnable() before using this function.
//!       If bpTiuAuxPowerEnable() is not called, then the BMR454 is not active and does not respond to commands.
//! @note Some BMR454 command reads cause the device to NACK further commands for some amount of time (sometimes as
//!       long as 500ms).
//!
//! @param[in] command    BMR454 command to read.
//! @param[out] pReadData A data buffer of bytes to hold the data read from the BMR454 device via the
//!                       I2C bus.  Note that the bytes received from the device are located sequentially
//!                       in the buffer in the order they were received with the first byte being located
//!                       directly at \c pReadData.  Also note that the PEC (Packet Error Checking) byte
//!                       is automatically checked within the API and is not included in \c pReadData.
//!                       Because of this, the PEC byte should not be included in the \c readLength value.
//!                       Lastly, this parameter cannot be NULL since all reads return at least one byte.
//! @param[in] readLength The byte length of the \c pReadData buffer.  Note that this parameter cannot be 0
//!                       since all reads return at least one byte.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS bpBmr454Read(_In_ BYTE command, _Out_writes_bytes_all_(readLength) LPVOID pReadData, _In_ DWORD readLength);

//! @brief Enables/Disables the TIU Aux power supply.
//!
//! This function enables/disables the 5 and 12 volt control signals to the TIU Aux power supply.
//! This function also calls rcRootI2cLock() and rcRootI2cUnlock() during its execution since it accesses
//! devices on the root I2C bus.  If additional I2C commands need to be atomically executed on the root I2C
//! bus, then manually call rcRootI2cLock() and rcRootI2cUnlock() around the entire root I2C bus access.
//! @note When disabling the TIU power, cached resources of the calibration base TIU and calibration cards are released as if
//!       the corresponding \c xxxDisconnect() APIs were called.  Users no longer need to call disconnect APIs before powering
//!       off the TIU if using this HIL version or later.
//! @param[in] enable Sets the TIU Aux power control to Enabled if TRUE or Disabled if FALSE
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS bpTiuAuxPowerEnable(_In_ BOOL enable);

//! @brief Reads the current state of the TIU Aux power supply.
//!
//! This function reads and returns the current state of the TIU Aux power supply.
//! This function also calls rcRootI2cLock() and rcRootI2cUnlock() during its execution since it accesses
//! devices on the root I2C bus.  If additional I2C commands need to be atomically executed on the root I2C
//! bus, then manually call rcRootI2cLock() and rcRootI2cUnlock() around the entire root I2C bus access.
//!
//! @param[out] pAuxPower5V States whether or not the 5V Aux Power supply is current enabled (TRUE) or disabled (FALSE).
//! @param[out] pAuxPower12V States whether or not the 12V Aux Power supply is current enabled (TRUE) or disabled (FALSE).
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS bpTiuAuxPowerState(_Out_ PBOOL pAuxPower5V, _Out_ PBOOL pAuxPower12V);

//! @brief Reads the board-level traceability values from the backplane.
//!
//! This function reads the board-level traceability values from the backplane.  The values are defined in the #BLT structure.
//!
//! @param[out] pBlt The address of a #BLT structure that will be populated with the data.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS bpBltBoardRead(_Out_ PBLT pBlt);

//! @brief Writes the board-level traceability values of the backplane.
//!
//! This function writes the board-level traceability values of the backplane.  The values are defined in the #BLT structure.
//!
//! The \c flags field allows this function to update individual values.  The flags can be ORed together.  Only the fields indicated by
//! \c flags need to be populated in the provided \c pBlt parameter.
//!
//! \par Updating the Full BLT - C
//! \code{.c}
//! #include <Windows.h>
//! #include "BackplaneApi.h"
//! #include "string.h"
//!
//! int main()
//! {
//!     BLT blt;
//!     HIL_STATUS status;
//!     blt.Id = 0x1000000;
//!     strcpy(blt.DeviceName,"DeviceName");
//!     strcpy(blt.VendorName,"INTEL");
//!     strcpy(blt.PartNumberAsBuilt,"AAxxxxxx-xxx");
//!     strcpy(blt.PartNumberCurrent,blt.PartNumberAsBuilt);
//!     strcpy(blt.SerialNumber,"12345678");
//!     strcpy(blt.ManufactureDate,"YYMMDD");
//!     status = bpBltBoardWrite(&blt, BF_ALL);
//!     if (status != HS_SUCCESS)
//!         return -1;
//!     return 0;
//! }
//! \endcode
//! \par Updating the Current Part Number - Python
//! \code{.py}
//! blt = hil.BLT()
//! blt.PartNumberCurrent = 'AAyyyyyy-yyy'
//! hil.bpBltBoardWrite(blt,hil.BF_PART_NUMBER_CURRENT)
//! # Note Python returns None for HIL_STATUS if successful and raises RuntimeError on failure.
//! \endcode
//!
//! \note HS_BLT_NOT_FOUND can be returned on an update (lack of #BF_ALL flag).
//!
//! @param[in] pBlt The address of a #BLT structure that contains the data to be written or updated.
//! @param[in] flags #BLT_FLAGS bits indicating what fields in \c pBlt are valid.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS bpBltBoardWrite(_In_ PCBLT pBlt, _In_ BLT_FLAGS flags);

//! @brief Reads cycle counter values from the backplane.
//!
//! This function reads cycle counter values from the backplane.  The values are defined in the #BLTCY_COUNTERS structure.
//!
//! @param[in] counterIndex    Index of the cycle counter record to read.  There are 13 counter records available.
//!                            Valid indexes are 0-12 representing instruments slot connectors (0-11) and the TIU connector (12).
//! @param[out] pBltCyCounters The address of a #BLTCY_COUNTERS structure that will be populated with the data.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS bpBltCyCountersRead(_In_ UINT counterIndex, _Out_ PBLTCY_COUNTERS pBltCyCounters);

//! @brief Writes cycle counter values of the backplane.
//!
//! This function writes cycle counter values of the backplane.  The values are defined in the #BLTCY_COUNTERS structure.
//!
//! @param[in] counterIndex    Index of the cycle counter record to write.  There are 13 counter records available.
//!                            Valid indexes are 0-12 representing instruments slot connectors (0-11) and the TIU connector (12).
//! @param[in] pBltCyCounters The address of a #BLTEX_COUNTERS structure that contains the data to be written.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS bpBltCyCountersWrite(_In_ UINT counterIndex, _In_ PCBLTCY_COUNTERS pBltCyCounters);

//! @brief Reads bytes from the backplane 24LC64 EEPROM.
//!
//! This function reads bytes from the backplane 24LC64 EEPROM.  It is a 8KB device, accessed using
//! I2C address 0xAC.
//!
//! @param[in]  address The address to begin reading from the EEPROM.  Valid values are 0x0000-0x1FFF.
//! @param[out] pData   The output buffer.  It cannot be NULL.
//! @param[in]  length  The amount of data to read.  \c address plus \c length cannot exceed 0x2000 (8KB).
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS bpEepromRead(_In_ UINT address, _Out_writes_bytes_all_(length) LPVOID pData, _In_ DWORD length);

//! @brief Writes bytes to the backplane 24LC64 EEPROM.
//!
//! This function writes bytes to the backplane 24LC64 EEPROM.  It is a 8KB device, accessed using
//! I2C address 0xAC.
//!
//! @param[in]  address The address to begin writing to the EEPROM.  Valid values are 0x0000-0x1FFF.
//! @param[in]  pData   The input buffer.  It cannot be NULL.
//! @param[in]  length  The amount of data to write.  \c address plus \c length cannot exceed 0x2000 (8KB).
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS bpEepromWrite(_In_ UINT address, _In_reads_bytes_(length) const void* pData, _In_ DWORD length);

//! @brief Reads the system identifier from the backplane.
//!
//! This function reads the system identifier from the backplane.
//!
//! @param[out] pSysId A buffer for the returned system ID.  It cannot be NULL.  A buffer of #HIL_MAX_ID_STRING_SIZE is guaranteed
//!                    to be large enough to hold the identifier.
//! @param[in] length  The size of the \c pSysId buffer in bytes.
//! @retval HS_SUCCESS The system ID was returned successfully.
//! @retval HS_INSUFFICIENT_BUFFER The \c length of the \c pSysId buffer was insufficient to hold the result.
HIL_API HIL_STATUS bpSysIdRead(_Out_writes_z_(length) LPSTR pSysId, _In_ DWORD length);

//! @brief Writes a system identifier to the backplane.
//!
//! This function writes a system identifier to the backplane.
//!
//! @param[in] pSysId  A pointer to a null-terminated system ID string no longer than #HIL_MAX_ID_STRING_SIZE including the null.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS bpSysIdWrite(_In_z_ LPCSTR pSysId);

//! @brief Reads the material equipment system identifier from the backplane.
//!
//! This function reads the material equipment system(MES) identifier from the backplane.
//!
//! @param[out] pMesId A buffer for the returned MES ID.  It cannot be NULL.  A buffer of #HIL_MAX_ID_STRING_SIZE is guaranteed
//!                    to be large enough to hold the identifier.
//! @param[in] length  The size of the \c pMesId buffer in bytes.
//! @retval HS_SUCCESS The MES ID was returned successfully.
//! @retval HS_INSUFFICIENT_BUFFER The \c length of the \c pMesId buffer was insufficient to hold the result.
HIL_API HIL_STATUS bpMesIdRead(_Out_writes_z_(length) LPSTR pMesId, _In_ DWORD length);

//! @brief Writes a material equipment system identifier to the backplane.
//!
//! This function writes a material equipment system(MES) identifier to the backplane.
//!
//! @param[in] pMesId  A pointer to a null-terminated MES ID string no longer than #HIL_MAX_ID_STRING_SIZE including the null.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS bpMesIdWrite(_In_z_ LPCSTR pMesId);

#ifdef __cplusplus
}
#endif
