// INTEL CONFIDENTIAL
// Copyright 2018 Intel Corporation All Rights Reserved.
//
// The source code contained or described herein and all documents related to
// the source code ("Material") are owned by Intel Corporation or its
// suppliers or licensors. Title to the Material remains with Intel
// Corporation or its suppliers and licensors. The Material contains trade
// secrets and proprietary and confidential information of Intel or its
// suppliers and licensors. The Material is protected by worldwide copyright
// and trade secret laws and treaty provisions. No part of the Material may be
// used, copied, reproduced, modified, published, uploaded, posted,
// transmitted, distributed, or disclosed in any way without Intel's prior
// express written permission.
//
// No license under any patent, copyright, trade secret or other intellectual
// property right is granted to or conferred upon you by disclosure or
// delivery of the Materials, either expressly, by implication, inducement,
// estoppel or otherwise. Any license under such intellectual property rights
// must be express and approved by Intel in writing.

//! @file
//! @brief APIs to control the HBI Backplane.

#pragma once
#include "HilDefs.h"

#ifdef __cplusplus
extern "C" {
#endif

//! @brief Programs the vendor and product IDs of the USB device on an HBI backplane.
//!
//! This function programs the vendor and product IDs (VID/PIDs) of the FT240X device on an HBI backplane.
//! Correct VID/PIDs are required to ensure Windows Device Manager displays a proper description of the device,
//! and HIL requires correct VID/PIDs to ensure software can locate and communicate with the correct component.
//! @note FTDI devices have a limited number of updates, but this function is safe to call because it
//! only writes if the component programming is incorrect.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiBpVidPidSet(void);

//! @brief Reads the board-level traceability values from an HBI backplane.
//!
//! This function reads the board-level traceability values from an HBI backplane.
//!
//! @param[out] pBlt The address of a #BLT structure that will be populated with the BLT data.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiBpBltBoardRead(_Out_ PBLT pBlt);

//! @brief Writes the board-level traceability values of an HBI backplane.
//!
//! This function writes the board-level traceability values of an HBI backplane.
//!
//! The \c flags field allows this function to update individual values.  The flags can be ORed together.  Only the fields indicated by
//! \c flags need to be populated in the provided \c pBlt parameter.
//!
//! \par Updating the Full BLT - C
//! \code{.c}
//! #include <Windows.h>
//! #include "HbiBpApi.h"
//! #include "string.h"
//!
//! int main()
//! {
//!     BLT blt;
//!     HIL_STATUS status;
//!     blt.Id = 0x100000FF; /* NOT the actual card ID */
//!     strcpy(blt.DeviceName,"DeviceName");
//!     strcpy(blt.VendorName,"INTEL");
//!     strcpy(blt.PartNumberAsBuilt,"AAxxxxxx-xxx");
//!     strcpy(blt.PartNumberCurrent,blt.PartNumberAsBuilt);
//!     strcpy(blt.SerialNumber,"12345678");
//!     strcpy(blt.ManufactureDate,"YYMMDD");
//!     status = hbiBpBltBoardWrite(&blt, BF_ALL);
//!     if (status != HS_SUCCESS)
//!         return -1;
//!     return 0;
//! }
//! \endcode
//! \par Updating the Current Part Number - Python
//! \code{.py}
//! blt = hil.BLT()
//! blt.PartNumberCurrent = 'AAyyyyyy-yyy'
//! hil.hbiBpBltBoardWrite(blt,hil.BF_PART_NUMBER_CURRENT)
//! # Note Python returns None for HIL_STATUS if successful and raises RuntimeError on failure.
//! \endcode
//!
//! \note HS_BLT_NOT_FOUND can be returned on an update (lack of #BF_ALL flag).
//!
//! @param[in] pBlt The address of a #BLT structure that contains the data to be written or updated.
//! @param[in] flags #BLT_FLAGS bits indicating what fields in \c pBlt are valid.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiBpBltBoardWrite(_In_ PCBLT pBlt, _In_ BLT_FLAGS flags);

//! @brief Reads raw data from an HBI backplane's BLT EEPROM.
//!
//! This function reads raw data from an HBI backplane's BLT EEPROM.
//!
//! @param[out] pEepromData The output buffer.
//! @param[in,out] pLength On input, the size of the output buffer referenced by \c pData.  On output, the size of the EEPROM data.
//! @retval HS_INSUFFICIENT_BUFFER The buffer provided was too small.  \c pLength will indicate the minimum buffer size required.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiBpBltEepromRead(_Out_writes_bytes_to_(_Old_(*pLength), *pLength) LPVOID pEepromData, _Inout_ LPDWORD pLength);

//! @brief Reads the system identifier from the HBI backplane.
//!
//! This function reads the system identifier from the HBI backplane.
//!
//! @param[out] pSysId A buffer for the returned system ID.  It cannot be NULL.  A buffer of #HIL_MAX_ID_STRING_SIZE is guaranteed
//!                    to be large enough to hold the identifier.
//! @param[in] length  The size of the \c pSysId buffer in bytes.
//! @retval HS_SUCCESS The system ID was returned successfully.
//! @retval HS_INSUFFICIENT_BUFFER The \c length of the \c pSysId buffer was insufficient to hold the result.
HIL_API HIL_STATUS hbiBpSysIdRead(_Out_writes_z_(length) LPSTR pSysId, _In_ DWORD length);

//! @brief Writes a system identifier to the HBI backplane.
//!
//! This function writes a system identifier to the HBI backplane.
//!
//! @param[in] pSysId  A pointer to a null-terminated system ID string no longer than #HIL_MAX_ID_STRING_SIZE including the null.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiBpSysIdWrite(_In_z_ LPCSTR pSysId);

//! @brief Reads the material equipment system identifier from the HBI backplane.
//!
//! This function reads the material equipment system(MES) identifier from the HBI backplane.
//!
//! @param[out] pMesId A buffer for the returned MES ID.  It cannot be NULL.  A buffer of #HIL_MAX_ID_STRING_SIZE is guaranteed
//!                    to be large enough to hold the identifier.
//! @param[in] length  The size of the \c pMesId buffer in bytes.
//! @retval HS_SUCCESS The MES ID was returned successfully.
//! @retval HS_INSUFFICIENT_BUFFER The \c length of the \c pMesId buffer was insufficient to hold the result.
HIL_API HIL_STATUS hbiBpMesIdRead(_Out_writes_z_(length) LPSTR pMesId, _In_ DWORD length);

//! @brief Writes a material equipment system identifier to the HBI backplane.
//!
//! This function writes a material equipment system(MES) identifier to the HBI backplane.
//!
//! @param[in] pMesId  A pointer to a null-terminated MES ID string no longer than #HIL_MAX_ID_STRING_SIZE including the null.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiBpMesIdWrite(_In_z_ LPCSTR pMesId);

#ifdef __cplusplus
}
#endif
