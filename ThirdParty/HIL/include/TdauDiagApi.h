// INTEL CONFIDENTIAL
// Copyright 2017-2020 Intel Corporation All Rights Reserved.
//
// The source code contained or described herein and all documents related to
// the source code ("Material") are owned by Intel Corporation or its
// suppliers or licensors. Title to the Material remains with Intel
// Corporation or its suppliers and licensors. The Material contains trade
// secrets and proprietary and confidential information of Intel or its
// suppliers and licensors. The Material is protected by worldwide copyright
// and trade secret laws and treaty provisions. No part of the Material may be
// used, copied, reproduced, modified, published, uploaded, posted,
// transmitted, distributed, or disclosed in any way without Intel's prior
// express written permission.
//
// No license under any patent, copyright, trade secret or other intellectual
// property right is granted to or conferred upon you by disclosure or
// delivery of the Materials, either expressly, by implication, inducement,
// estoppel or otherwise. Any license under such intellectual property rights
// must be express and approved by Intel in writing.
//
//! @file
//! @brief APIs to control the HDMT TDAU diagnostics card.
#pragma once
#include "HilDefs.h"

#ifdef __cplusplus
extern "C" {
#endif

//! @brief Verifies a TDAU diagnostics card is present.
//!
//! This function verifies a TDAU diagnostics card is present.  It connects to and caches driver resources for use
//! by other \c tddXxxxx() functions.  tddDisconnect() is its complementary function and frees any cached resources.
//!
//! Note that tddConnect() does not need to be called to use the other \c tddXxxxx() functions.  All \c tddXxxxx()
//! functions will allocate resources if they are not already allocated.  However, the only way to free all resources
//! that have been allocated is to call tddDisconnect().
//! @param[in] slot A physical slot number.  Valid values are 0 to \ref HIL_SLOTS - 1.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS tddConnect(_In_ INT slot);

//! @brief Frees any resources cached from using a TDAU diagnostics card's functions.
//!
//! This function frees any resources associated with using a TDAU diagnostics card's HIL functions.  tddConnect() is its
//! complementary function.
//!
//! Note that the only way to free all resources that have been allocated is to call tddDisconnect().
//! @param[in] slot A physical slot number.  Valid values are 0 to \ref HIL_SLOTS - 1.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS tddDisconnect(_In_ INT slot);

//! @brief Programs the vendor and product IDs of the FT4232 USB device on a TDAU diagnostics card.
//!
//! This function programs the vendor and product IDs of the FT4232 USB device
//! on a TDAU diagnostics card. Correct VID/PIDs are required to ensure Windows Device
//! Manager displays a proper description of the device, and HIL requires correct VID/PIDs to ensure
//! software can locate and communicate with the correct component.
//! @note FTDI devices have a limited number of updates, but this function is safe to call because it
//! only writes if the component programming is incorrect.
//! @param[in] slot A physical slot number.  Valid values are 0 to \ref HIL_SLOTS - 1.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS tddVidPidSet(_In_ INT slot);

//! @brief Initializes the components of a TDAU diagnostics card for operation.
//!
//! This function initializes the components of a TDAU diagnostics card for operation.  Calling this
//! function is required before using most other \c tddXxxxx() functions.
//! @param[in] slot A physical slot number.  Valid values are 0 to \ref HIL_SLOTS - 1.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS tddInit(_In_ INT slot);

//! @brief Reads the board-level traceability values from a TDAU diagnostics card.
//!
//! This function reads the board-level traceability values from a TDAU diagnostics card.  The values are defined in the #BLT structure.
//! @note tddInit() is required to be called before using this function.
//! @param[in]  slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[out] pBlt The address of a #BLT structure that will be populated with the data.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS tddBltBoardRead(_In_ INT slot, _Out_ PBLT pBlt);

//! @brief Writes the board-level traceability values for a TDAU diagnostics card.
//!
//! This function writes the board-level traceability values for a TDAU diagnostics card.  The values are defined in the #BLT structure.
//!
//! The \c flags field allows this function to update individual values.  The flags can be ORed together.  Only the fields indicated by
//! \c flags need to be populated in the provided \c pBlt parameter.
//!
//! \par Updating the Full BLT - C
//! \code{.c}
//! #include <Windows.h>
//! #include "TdauDiagApi.h"
//! #include "string.h"
//!
//! int main()
//! {
//!     int slot = 8;
//!     BLT blt;
//!     HIL_STATUS status;
//!     blt.Id = 0x010000FF;
//!     strcpy(blt.DeviceName,"DeviceName");
//!     strcpy(blt.VendorName,"INTEL");
//!     strcpy(blt.PartNumberAsBuilt,"AAxxxxxx-xxx");
//!     strcpy(blt.PartNumberCurrent,blt.PartNumberAsBuilt);
//!     strcpy(blt.SerialNumber,"12345678");
//!     strcpy(blt.ManufactureDate,"YYMMDD");
//!     status = tddBltBoardWrite(slot, &blt, BF_ALL);
//!     if (status != HS_SUCCESS)
//!         return -1;
//!     return 0;
//! }
//! \endcode
//! \par Updating the Current Part Number - Python
//! \code{.py}
//! slot = 8
//! blt = hil.BLT()
//! blt.PartNumberCurrent = 'AAyyyyyy-yyy'
//! hil.tddBltBoardWrite(slot,blt,hil.BF_PART_NUMBER_CURRENT)
//! # Note Python returns None for HIL_STATUS if successful and raises RuntimeError on failure.
//! \endcode
//!
//! @note tddInit() is required to be called before using this function.
//! @note HS_BLT_NOT_FOUND can be returned on an update (lack of #BF_ALL flag).
//!
//! @param[in] slot  A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] pBlt  The address of a #BLT structure that contains the data to be written or updated.
//! @param[in] flags #BLT_FLAGS bits indicating what fields in \c pBlt are valid.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS tddBltBoardWrite(_In_ INT slot, _In_ PCBLT pBlt, _In_ BLT_FLAGS flags);

//! @brief Reads data from the 24LC512 (EEPROM) device on a TDAU diagnostics card.
//!
//! This function reads data from the 24LC512 (EEPROM) device on a TDAU diagnostics card.
//! @note Calling tddInit() is required before using this function.
//! @param[in] slot    A physical slot number.  Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] address The location of the first byte in the EEPROM from which the data will be read.
//! @param[out] pData  A pointer to a data buffer in which the read data will be stored. Note that this buffer must have at least \c length
//!                    bytes allocated in order to complete successfully.
//! @param[in] length  The number of bytes to read from the EEPROM.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS tddEepromRead(_In_ INT slot, _In_ DWORD address, _Out_writes_bytes_all_(length) LPVOID pData, _In_ DWORD length);

//! @brief Writes data to the 24LC512 (EEPROM) device on a TDAU diagnostics card.
//!
//! This function writes data to the 24LC512 (EEPROM) device on a TDAU diagnostics card.
//! @note Calling tddInit() is required before using this function.
//! @param[in] slot    A physical slot number.  Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] address The location of the first byte in the EEPROM to which the data in the \c pData buffer will be written.
//! @param[in] pData   A data buffer containing data to write to the EEPROM device.
//! @param[in] length  The byte length of the \c pData buffer.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS tddEepromWrite(_In_ INT slot, _In_ DWORD address, _In_reads_bytes_(length) const void* pData, _In_ DWORD length);

//! @brief Sets the reference voltage for PECI devices on a TDAU diagnostics card.
//!
//! This function sets the reference voltage for PECI devices on a TDAU diagnostics card using a DAC6573.
//! @note Calling tddInit() is required before using this function.
//! @param[in] slot    A physical slot number.  Valid values are 0 to \ref HIL_SLOTS - 1. Valid range is 0.0-3.3.
//! @param[in] voltage Desired value of reference voltage for PECI device in volts.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS tddPeciVrefSet(_In_ INT slot, _In_ double voltage);

//! @brief Reads the reference voltage of PECI devices on a TDAU diagnostics card.
//!
//! This function reads the reference voltage of PECI devices on a TDAU diagnostics card using a DAC6573.
//! @note Calling tddInit() is required before using this function.
//! @param[in]  slot      A physical slot number.  Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[out] pVoltage The reference voltage in volts.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS tddPeciVrefGet(_In_ INT slot, _Out_ double* pVoltage);

#ifdef __cplusplus
}
#endif
