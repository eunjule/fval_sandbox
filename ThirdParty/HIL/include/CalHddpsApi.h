// INTEL CONFIDENTIAL
// Copyright 2014-2018 Intel Corporation All Rights Reserved.
//
// The source code contained or described herein and all documents related to
// the source code ("Material") are owned by Intel Corporation or its
// suppliers or licensors. Title to the Material remains with Intel
// Corporation or its suppliers and licensors. The Material contains trade
// secrets and proprietary and confidential information of Intel or its
// suppliers and licensors. The Material is protected by worldwide copyright
// and trade secret laws and treaty provisions. No part of the Material may be
// used, copied, reproduced, modified, published, uploaded, posted,
// transmitted, distributed, or disclosed in any way without Intel's prior
// express written permission.
//
// No license under any patent, copyright, trade secret or other intellectual
// property right is granted to or conferred upon you by disclosure or
// delivery of the Materials, either expressly, by implication, inducement,
// estoppel or otherwise. Any license under such intellectual property rights
// must be express and approved by Intel in writing.

//! @file
//! @brief APIs to control the HDMT HDDPS calibration card.
#pragma once
#include "HilDefs.h"
#include "CalHddpsDefs.h"

#ifdef __cplusplus
extern "C" {
#endif

//! @brief Verifies an HDMT HDDPS calibration card is present.
//!
//! This function verifies an HDMT HDDPS calibration card is present in the requested slot.  It connects to and caches
//! driver resources for use by other \c calHddpsXXXXX functions.  calHddpsDisconnect() is its complementary function and
//! frees any cached resources.
//!
//! Note that calHddpsConnect() does not need to be called to use the other \c calHddpsXXXXX functions.  All \c calHddpsXXXXX
//! functions will allocate resources if they are not already allocated.  However, the only way to free all resources
//! that have been allocated is to call calHddpsDisconnect().
//! @param[in] slot A physical slot number.  Valid values are 0 to \ref HIL_SLOTS - 1.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS calHddpsConnect(_In_ INT slot);

//! @brief Frees any resources cached from using the HDDPS calibration card functions.
//!
//! This function frees any resources associated with using the HDDPS calibration card HIL functions.  calHddpsConnect() is its
//! complementary function.
//!
//! Note that the only way to free all resources that have been allocated is to call calHddpsDisconnect().
//! @param[in] slot A physical slot number.  Valid values are 0 to \ref HIL_SLOTS - 1.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS calHddpsDisconnect(_In_ INT slot);

//! @brief Programs the vendor and product IDs of all USB devices on an HDDPS calibration card.
//!
//! This function programs the vendor and product IDs (VID/PIDs) of the FT240X and Cypress
//! USB devices on an HDDPS calibration card.  Correct VID/PIDs are required to ensure Windows Device
//! Manager displays a proper description of the device, and HIL requires correct VID/PIDs to ensure
//! software can locate and communicate with the correct component.
//! @note FTDI devices have a limited number of updates, but this function is safe to call because it
//! only writes if the component programming is incorrect.
//! @param[in] slot A physical slot number.  Valid values are 0 to \ref HIL_SLOTS - 1.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS calHddpsVidPidsSet(_In_ INT slot);

//! @brief Initializes the components of an HDDPS calibration card for operation.
//!
//! This function initializes the components of an HDDPS calibration card for operation.  Calling this
//! function is required before using most other \c calHddpsXXXXXX() functions.  Note that calling
//! this function is equivalent to calling the following functions.
//!
//! \code{.c}
//! calHddpsPca9505Init(slot, 0);
//! calHddpsPca9505Init(slot, 1);
//! calHddpsPca9505Init(slot, 2);
//! \endcode
//!
//! @param[in] slot A physical slot number.  Valid values are 0 to \ref HIL_SLOTS - 1.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS calHddpsInit(_In_ INT slot);

//! @brief Initializes the PCA9505 device to a known state.
//!
//! This function initializes the PCA9505 device to a known state, including setting all GPIOs to logic high.
//! This ensures that the active low relays attached to the GPIOs are off.  Note that this function can only be called after power
//! has been applied to the TIU using bpTiuAuxPowerEnable().
//! @param[in] slot A physical slot number.  Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] chip PCA9505 chip identifier.  See calHddpsPca9505Read() for encoding.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS calHddpsPca9505Init(_In_ INT slot, _In_ INT chip);

//! @brief Writes a byte of data to a register within the specified PCA9505 (GPIO Expander) device.
//!
//! This function writes a byte of data to a register within the specified PCA9505 (GPIO Expander) device.
//! See calHddpsPca9505Read() for the chip assignments.
//!
//! @param[in] slot A physical slot number.  Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] chip PCA9505 chip identifier.  See calHddpsPca9505Read() for encoding.
//! @param[in] reg PCA9505 register address.
//! @param[in] data The data value to write.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS calHddpsPca9505Write(_In_ INT slot, _In_ INT chip, _In_ BYTE reg, _In_ BYTE data);

//! @brief Reads a byte of data from a register within the specified PCA9505 (GPIO Expander) device.
//!
//! This function reads a byte of data from a register within the specified PCA9505 (GPIO Expander) device.
//!
//! @param[in] slot A physical slot number.  Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] chip PCA9505 chip identifier.
//! The Chip column of the table below represents the chip input parameter choices.
//! The Symbol column represents the schematic symbol designator.
//! | Chip | Description                   | Symbol | I2C Addr |
//! | :--: | :---------------------------- | :----: | :------: |
//! | 0    | Control for Sense Connections | U3     | 0x20     |
//! | 1    | Control for Load Connections  | U1     | 0x22     |
//! | 2    | Control for Force Connections | U2     | 0x24     |
//! @param[in] reg PCA9505 register address.
//! @param[out] pData The data value read back.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS calHddpsPca9505Read(_In_ INT slot, _In_ INT chip, _In_ BYTE reg, _Out_ PBYTE pData);

//! @brief Sets the GPIOs controlled by all the PCA9505 devices to a known state.
//!
//! This function sets the relays controlled by all the PCA9505 devices to a known state.  All GPIO pins are
//! set to logic high which ensures that the active low relays attached to them are off.  Note that this
//! function assumes all the PCA9505 devices have already initialized their GPIOs to the correct directions
//! (which is done by calling calHddpsInit()).
//!
//! @param[in] slot A physical slot number.  Valid values are 0 to \ref HIL_SLOTS - 1.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS calHddpsGpiosReset(_In_ INT slot);

//! @brief Sets the specified GPIO line to a logic high or low.
//!
//! This function sets the specified GPIO line to a logic high or low.
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] gpio  Specifies which single GPIO on the HDDPS calibration card to set high or low.
//! @param[in] state Specifies whether the GPIO line is set to logic high (TRUE) or logic low (FALSE).
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS calHddpsGpioWrite(_In_ INT slot, _In_ CAL_HDDPS_GPIO gpio, _In_ BOOL state);

//! @brief Reads the value of the specified GPIO line.
//!
//! This function reads the logic value of the specified GPIO line.
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in]  gpio Specifies which single GPIO on the HDDPS calibration card to read.
//! @param[out] pState Contains the logic state of the GPIO where TRUE is logic high and FALSE is logic low.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS calHddpsGpioRead(_In_ INT slot, _In_ CAL_HDDPS_GPIO gpio, _Out_ LPBOOL pState);

//! @brief Reads the board-level traceability values from an HDDPS calibration board.
//!
//! This function reads the board-level traceability values from an HDDPS calibration board.  The values are defined in the #BLT structure.
//!
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[out] pBlt The address of a #BLT structure that will be populated with the data.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS calHddpsBltBoardRead(_In_ INT slot, _Out_ PBLT pBlt);

//! @brief Writes the board-level traceability values for an HDDPS calibration board.
//!
//! This function writes the board-level traceability values for an HDDPS calibration board.  The values are defined in the #BLT structure.
//!
//! The \c flags field allows this function to update individual values.  The flags can be ORed together.  Only the fields indicated by
//! \c flags need to be populated in the provided \c pBlt parameter.
//!
//! \par Updating the Full BLT - C
//! \code{.c}
//! #include <Windows.h>
//! #include "CalHddpsApi.h"
//! #include "string.h"
//!
//! int main()
//! {
//!     int slot = 8;
//!     BLT blt;
//!     HIL_STATUS status;
//!     blt.Id = 0x1000034;
//!     strcpy(blt.DeviceName,"DeviceName");
//!     strcpy(blt.VendorName,"INTEL");
//!     strcpy(blt.PartNumberAsBuilt,"AAxxxxxx-xxx");
//!     strcpy(blt.PartNumberCurrent,blt.PartNumberAsBuilt);
//!     strcpy(blt.SerialNumber,"12345678");
//!     strcpy(blt.ManufactureDate,"YYMMDD");
//!     status = calHddpsBltBoardWrite(slot, &blt, BF_ALL);
//!     if (status != HS_SUCCESS)
//!         return -1;
//!     return 0;
//! }
//! \endcode
//! \par Updating the Current Part Number - Python
//! \code{.py}
//! slot = 8
//! blt = hil.BLT()
//! blt.PartNumberCurrent = 'AAyyyyyy-yyy'
//! hil.calHddpsBltBoardWrite(slot,blt,hil.BF_PART_NUMBER_CURRENT)
//! # Note Python returns None for HIL_STATUS if successful and raises RuntimeError on failure.
//! \endcode
//!
//! \note HS_BLT_NOT_FOUND can be returned on an update (lack of #BF_ALL flag).
//!
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] pBlt The address of a #BLT structure that contains the data to be written or updated.
//! @param[in] flags #BLT_FLAGS bits indicating what fields in \c pBlt are valid.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS calHddpsBltBoardWrite(_In_ INT slot, _In_ PCBLT pBlt, _In_ BLT_FLAGS flags);

//! @brief Reads the raw data from the specified HDDPS calibration card's FT240X EEPROM that stores BLT.
//!
//! This function reads the raw data from the specified HDDPS calibration card's FT240X EEPROM that stores BLT (chip EU24 on Fab C).
//!
//! @param[in] slot A physical slot number.  Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[out] pEepromData The output buffer.
//! @param[in,out] pLength On input, the size of the output buffer referenced by \c pEepromData.  On output, the size of the EEPROM data.
//! @retval HS_INSUFFICIENT_BUFFER The buffer provided was too small.  \c pLength will indicate the minimum buffer size required.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS calHddpsBltEepromRead(_In_ INT slot, _Out_writes_bytes_to_(_Old_(*pLength), *pLength) LPVOID pEepromData, _Inout_ LPDWORD pLength);

//! @brief Reads data from the CAL EEPROM on an HDDPS calibration card.
//!
//! This function reads data from the CAL EEPROM on an HDDPS calibration card.  The designation for this chip is U4 on Fab C and it is a 512Kb part (64KB).
//!
//! @param[in] slot    A physical slot number.  Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] address The location of the first byte in the EEPROM from which the data will be read.  Valid values are 0 - 0xFFFF.
//! @param[out] pData  A pointer to a data buffer in which the read data will be stored. Note that this buffer must have at least \c length
//!                    allocated in order to complete successfully.
//! @param[in] length  The number of bytes to read from the EEPROM.  \c address plus \c length cannot exceed 0x10000 (64KB).
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS calHddpsCalEepromRead(_In_ INT slot, _In_ UINT address, _Out_writes_bytes_all_(length) LPVOID pData, _In_ DWORD length);

//! @brief Writes data to the CAL EEPROM on an HDDPS calibration card.
//!
//! This function writes data to the CAL EEPROM on an HDDPS calibration card.  The designation for this chip is U4 on Fab C and it is a 512Kb part (64KB).
//!
//! @param[in] slot    A physical slot number.  Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] address The location of the first byte in the EEPROM to which the data in the \c pData buffer will be written.  Valid values are 0 - 0xFFFF.
//! @param[in] pData   A data buffer containing data to write to the EEPROM.  \c address plus \c length cannot exceed 0x10000 (64KB).
//! @param[in] length  The byte length of the \c pData buffer.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS calHddpsCalEepromWrite(_In_ INT slot, _In_ UINT address, _In_reads_bytes_(length) LPCVOID pData, _In_ DWORD length);

//! @brief Reads bytes from the MC24LC64 EEPROM on an HDDPS calibration card.
//!
//! This function reads bytes from the MC24LC64 I<sup>2</sup>C EEPROM on an HDDPS calibration card.
//! The designation for this chip is U5 on the Fab C schematic and it is a 64Kb part (8KB).
//!
//! @param[in]  slot    A physical slot number.  Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in]  address The address to begin reading from the EEPROM.  Valid values are 0x0000-0x1FFF.
//! @param[out] pData   The output buffer.  It cannot be NULL.
//! @param[in]  length  The amount of data to read.  \c address plus \c length cannot exceed 0x2000 (8KB).
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS calHddpsEepromRead(_In_ INT slot, _In_ UINT address, _Out_writes_bytes_all_(length) LPVOID pData, _In_ DWORD length);

//! @brief Writes bytes to the MC24LC64 EEPROM on an HDDPS calibration card.
//!
//! This function writes bytes to the MC24LC64 I<sup>2</sup>C EEPROM on an HDDPS calibration card.
//! The designation for this chip is U5 on the Fab C schematic and it is a 64Kb part (8KB).
//!
//! @param[in]  slot    A physical slot number.  Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in]  address The address to begin writing to the EEPROM.  Valid values are 0x0000-0x1FFF.
//! @param[in]  pData   The input buffer.  It cannot be NULL.
//! @param[in]  length  The amount of data to write.  \c address plus \c length cannot exceed 0x2000 (8KB).
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS calHddpsEepromWrite(_In_ INT slot, _In_ UINT address, _In_reads_bytes_(length) const void* pData, _In_ DWORD length);

//! @brief Reads a temperature on an HDDPS calibration card.
//!
//! This function reads a temperature on an HDDPS calibration card.
//!
//! @param[in] slot    A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] channel The temperature channel to read.  Consult the following table.
//!                    Note that reference designators are for Fab A and may change in new fabs.
//!                    | channel | Designator | Comment                          |
//!                    | :-----: | :--------: | :------------------------------: |
//!                    |    0    |    U6T1    | LM75A Digital Temperature Sensor |
//!                    |    1    |    U6R1    |    \"                            |
//!                    |    2    |    U7V1    |    \"                            |
//!                    |    3    |    U6V1    |    \"                            |
//!                    |    4    |    U6N1    |    \"                            |
//! @param[out] pTemp The temperature in degrees Celsius.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS calHddpsTmonRead(_In_ INT slot, _In_ UINT channel, _Out_ double* pTemp);

//! @brief Reads bytes of data from a register within specified LM75A device(Digital temperature sensor) on the HDDPS calibration card.
//! This function read  bytes of data from a register within specified LM75A device(Digital temperature sensor) on the HDDPS calibration card.
//!
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] chip A physical chip number. Consult the following table.
//!                 Note that reference designators are for Fab A and may change in new fabs.
//!                 | Index |  Designator |
//!                 | :---: | :---------: |
//!                 |   0   |    U6T1     |
//!                 |   1   |    U6R1     |
//!                 |   2   |    U7V1     |
//!                 |   3   |    U6V1     |
//!                 |   4   |    U6N1     |
//! @param[in] reg Register to read. Consult the following table for valid read registers.  Note that some registers are 16-bit and some are 8-bit.
//!                 | Index |  Register  | Bits |
//!                 | :---: | :--------: | :--: |
//!                 |   0   |   Temp     |  16  |
//!                 |   1   |   Config   |   8  |
//!                 |   2   |   Thyst    |  16  |
//!                 |   3   |   Tos      |  16  |
//! @param[out] pData The returned data.  8-bit registers are zero-extended into the output word.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS calHddpsLm75aRead(_In_ INT slot, _In_ UINT chip, _In_ BYTE reg, _Out_ LPWORD pData);

//! @brief Writes bytes of data to a register within specified LM75A device(Digital temperature sensor) on the HDDPS calibration card.
//!
//! This function bytes of data to a register within specified LM75A device(Digital temperature sensor) on the HDDPS calibration card.
//!
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] chip A physical chip number. Consult the following table.
//!                 Note that reference designators may change in new fabs.
//!                 | Index |  LM75A     |
//!                 | :---: | :------:   |
//!                 |   0   |   U6T1     |
//!                 |   1   |   U6R1     |
//!                 |   2   |   U7V1     |
//!                 |   3   |   U6V1     |
//!                 |   4   |   U6N1     |
//! @param[in] reg Register to write. Consult the following table for valid write registers.  Note that some registers are 16-bit and some are 8-bit.
//!                 | Index |  Register  | Bits |
//!                 | :---: | :--------: | :--: |
//!                 |   1   |   Config   |   8  |
//!                 |   2   |   Thyst    |  16  |
//!                 |   3   |   Tos      |  16  |
//! @param[in] data The data to write.  HS_INVALID_PARAMETER will be returned if the data exceeds the register size.  Valid values are 00h-FFh (8-bit) or 0000h-FFFFh (16-bit).
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS calHddpsLm75aWrite(_In_ INT slot, _In_ UINT chip, _In_ BYTE reg, _In_ WORD data);

#ifdef __cplusplus
}
#endif
