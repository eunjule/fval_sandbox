// INTEL CONFIDENTIAL
// Copyright 2019 Intel Corporation All Rights Reserved.
//
// The source code contained or described herein and all documents related to
// the source code ("Material") are owned by Intel Corporation or its
// suppliers or licensors. Title to the Material remains with Intel
// Corporation or its suppliers and licensors. The Material contains trade
// secrets and proprietary and confidential information of Intel or its
// suppliers and licensors. The Material is protected by worldwide copyright
// and trade secret laws and treaty provisions. No part of the Material may be
// used, copied, reproduced, modified, published, uploaded, posted,
// transmitted, distributed, or disclosed in any way without Intel's prior
// express written permission.
//
// No license under any patent, copyright, trade secret or other intellectual
// property right is granted to or conferred upon you by disclosure or
// delivery of the Materials, either expressly, by implication, inducement,
// estoppel or otherwise. Any license under such intellectual property rights
// must be express and approved by Intel in writing.

//! @file
//! @brief APIs to control the HDMT resource card rev 3 (RCTC3).
//!
//! These APIs control the HDMT resource card rev 3 (RCTC3).  For compatibility with existing RCTC2 software
//! and to allow low-level I2C debugging, the following RCTC2 APIs also work with RCTC3:
//! * rcRootI2cLock()
//! * rcRootI2cUnlock()
//! * rcRootI2cMuxSelect()
//! * rcRootI2cWrite()
//! * rcRootI2cRead()
//! * rcRootI2cRegisterRead()
//! * rcRootI2cInit()
//! * rcRootI2cCacheInvalidate()

#pragma once
#include "HilDefs.h"
#include "Rc3Defs.h"

#ifdef __cplusplus
extern "C" {
#endif

//! @brief Initializes the components of the resource card (rev 3) for operation.
//!
//! This function initializes the components of the resource card (rev 3) for operation.  Calling this
//! function is required before using most other \c rc3XXXXXX() functions.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rc3Init(void);

//! @brief Verifies the HDMT resource card (rev 3) is present.
//!
//! This function verifies the HDMT resource card (rev 3) is present.  It connects to and caches driver resources for use
//! by other \c rc3XXXXX functions.  rc3Disconnect() is its complementary function and frees any cached resources.
//!
//! Neither rc3Connect() or rc3Disconnect() are required to be called to use the other \c rc3XXXXX functions.  All
//! \c rc3XXXXX functions will allocate resources if they are not already allocated and free them if required to
//! perform their function.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rc3Connect(void);

//! @brief Frees any resources cached from using the resource card (rev 3) functions.
//!
//! This function frees any resources associated with using the resource card (rev 3) HIL functions. rc3Connect() is its
//! complementary function.
//!
//! Neither rc3Connect() or rc3Disconnect() are required to be called to use the other \c rc3XXXXX functions.  All
//! \c rc3XXXXX functions will allocate resources if they are not already allocated and free them if required to
//! perform their function.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rc3Disconnect(void);

//! @brief Programs the vendor and product IDs of all USB devices on the resource card (rev 3).
//!
//! This function programs the vendor and product IDs (VID/PIDs) of the
//! USB devices on the resource card (rev 3).  Correct VID/PIDs are required to ensure Windows Device
//! Manager displays a proper description of the device, and HIL requires correct VID/PIDs to ensure
//! software can locate and communicate with the correct component.
//! @note FTDI devices have a limited number of updates, but this function is safe to call because it
//! only writes if the component programming is incorrect.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rc3VidPidsSet(void);

//! @brief **[DEPRECATED]** Disables or enables the HDMT failsafe timer.
//!
//! This function disables or enables the HDMT failsafe timer.
//! @warning DEPRECATED! Use hilFailsafeDisable().
//! @note rc3FailsafePing() will also renable the timer, so starting any safety system software that pings will automatically re-enable the timer.
//! @param[in] disable \c TRUE disables the timer.  \c FALSE enables it.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rc3FailsafeDisable(_In_ BOOL disable);

//! @brief **[DEPRECATED]** Sets the failsafe timer count value.
//!
//! This function sets the failsafe timer count value.  Once a time is set rc3FailsafePing() must be called
//! periodically before the timer expires or the tester will shut down.
//! @warning DEPRECATED! Use hilFailsafeTimerSet().
//! @param[in] seconds The count value in seconds.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rc3FailsafeTimerSet(_In_ DWORD seconds);

//! @brief **[DEPRECATED]** Resets the failsafe timer count.
//!
//! This function resets the failsafe timer to the count set by rc3FailsafeTimerSet().
//! If the timer expires the tester shuts down, so calling this function periodically keeps the tester powered up.
//! @warning DEPRECATED! Use hilFailsafePing().
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rc3FailsafePing(void);

//! @brief Reports the number of milliseconds that have elapsed since the the RCTC3 FPGA was last programmed.
//!
//! This function reports the number of milliseconds that have elapsed since the the RCTC3 FPGA was last programmed (through SPI flash or dynamically).
//! @param[out] pUptime The uptime in milliseconds.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rc3Uptime(_Out_ PUINT64 pUptime);

//! @brief Reads the resource card (rev 3) FPGA version number.
//!
//! This function reads the FPGA version number from the resource card (rev 3).  The returned 32-bit value is in the
//! format \c 0xMMMMmmmm where \c MMMM is the major version and \c mmmm is the minor version.
//!
//! For example, 0x00010000 is version 1.0.
//! @param[out] pVersion The address of a DWORD of memory.  It may not be NULL.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rc3FpgaVersion(_Out_ LPDWORD pVersion);

//! @brief Returns the resource card (rev 3) FPGA version number as a formatted string.
//!
//! This function reads the version register from the resource card (rev 3) FPGA and converts it to an ASCII string representation.
//! As of HIL 7.0, it also appends the compatibility register value.  The format is \c "MM.mm-C" where \c MM is the major version,
//! \c mm is the minor version, and \c C is the compatibility value.  All values are unsigned decimal numbers.
//! @param[out] pVersion Returns the version string.  The pointer must not be NULL.
//! @param[in]  length   The length of the \c pVersion buffer.  It should be at least #HIL_MAX_VERSION_STRING_SIZE characters to hold a null-terminated version string.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rc3FpgaVersionString(_Out_writes_z_(length) LPSTR pVersion, _In_ DWORD length);

//! @brief Returns the resource card (rev 3) FPGA's base FLASH content information.
//!
//! This function returns the resource card (rev 3) FPGA's base FLASH content information.
//! @param[out] pInfo Returns FLASH_INFO structure describing the FLASH contents.  The pointer must not be NULL.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rc3FpgaBaseInfo(_Out_ PFLASH_INFO pInfo);

//! @brief Returns the resource card (rev 3) FPGA's compatibility register value.
//!
//! This function returns the resource card (rev 3) FPGA's compatibility register value.
//! @param[out] pData Returns the value of the FPGA's compatibility register.  The pointer must not be NULL.  At a minimum, bits 0-3 of the returned value
//!                   indicates hardware capability.  An FPGA that reports a particular value in bits 0-3 should only be overwritten with a compatible image.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rc3FpgaCompatibility(_Out_ LPDWORD pData);

//! @brief Returns the source hash used to identify the source code that built the indicated FPGA.
//!
//! This function returns the source hash used to identify the source code that built the indicated FPGA.  The format of this field
//! is a 48-bit hexadecimal value with an optional trailing plus(+) indicating it was built from uncommitted sources.
//! @param[out] pHash  Returns a string representation of the FPGA's source control hash. The pointer must not be NULL.
//! @param[in] length The length of the \c pHash buffer.  It should be at least #HIL_MAX_VERSION_STRING_SIZE characters to hold a null-terminated version string.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rc3FpgaSourceHash(_Out_writes_z_(length) LPSTR pHash, _In_ DWORD length);

//! @brief Returns the 64-bit value of the resource card (rev 3) FPGA's chip ID registers.
//!
//! This function returns the 64-bit value of the resource card (rev 3) FPGA's chip ID registers that uniquely identifies the FPGA.
//! @param[out] pChipId Returns the 64-bit chip ID.  The pointer must not be NULL.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rc3FpgaChipId(_Out_ PDWORD64 pChipId);

//! @brief Reads the resource card (rev 3) CPLD version number.
//!
//! This function reads the CPLD version number from the resource card (rev 3).
//! @param[out] pVersion Returns the version register value.  The pointer must not be NULL.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rc3CpldVersion(_Out_ LPDWORD pVersion);

//! @brief Reads the resource card (rev 3)'s CPLD version string.
//!
//! This function reads the version register of the resource card (rev 3)'s CPLD and converts it to an ASCII string representation.
//! @param[out] pVersion Returns the version string.  The pointer must not be NULL.
//! @param[in]  length   The length of the \c pVersion buffer.  It should be at least five characters to hold a null-terminated version string.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rc3CpldVersionString(_Out_writes_z_(length) LPSTR pVersion, _In_ DWORD length);

//! @brief Programs a .JBC file into the CPLD on the resource card (rev 3).
//!
//! This function programs a .JBC file into the CPLD on the resource card (rev 3).
//! @param[in] filename An ANSI string containing an absolute or relative (to the current directory) CPLD .JBC filename.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rc3CpldProgram(_In_z_ LPCSTR filename);

//! @brief Programs a buffer containing .JBC file content into the CPLD on the resource card (rev 3).
//!
//! This function programs a buffer containing .JBC file content into the CPLD on the resource card (rev 3).
//! @param[in] pData The CPLD .JBC image data.
//! @param[in] length The length of the CPLD .JBC image data in bytes.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rc3CpldBufferProgram(_In_reads_bytes_(length) LPCVOID pData, _In_ DWORD length);

//! @brief Compares an existing image within the CPLD on the resource card (rev 3) against a CPLD .JBC file.
//!
//! This function compares an existing image within the CPLD on the resource card (rev 3) against a CPLD .JBC file.
//! @param[in] imageFilename An ANSI string containing an absolute or relative (to the current directory) CPLD .JBC filename to be compared against.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rc3CpldVerify(_In_z_ LPCSTR imageFilename);

//! @brief Compares an existing image within the CPLD on the resource card (rev 3) against a buffer containing .JBC file content.
//!
//! This function compares an existing image within the CPLD on the resource card (rev 3) against a buffer containing .JBC file content.
//! @param[in] pData The CPLD .JBC image data to be compared against.
//! @param[in] length The length of the CPLD .JBC image data in bytes.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rc3CpldBufferVerify(_In_reads_bytes_(length) LPCVOID pData, _In_ DWORD length);

//! @brief Disables the PCI device driver for the resource card (rev 3).
//!
//! This function disables the PCI device driver for the resource card (rev 3).  It should be called before using functions
//! such as rc3FpgaLoad() that affect the hardware used by the drivers.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rc3DeviceDisable(void);

//! @brief Enables the PCI device driver for the resource card (rev 3).
//!
//! This function enables the PCI device driver for the resource card (rev 3).  It should be called after using functions
//! such as rc3FpgaLoad() that affect the hardware used by the drivers.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rc3DeviceEnable(void);

//! @brief Loads an FPGA binary image file into the resource card (rev 3) FPGA.
//!
//! This function loads an FPGA binary image file into the resource card (rev 3) FPGA.  This image is volatile and the FPGA
//! will load its original base program from its SPI FLASH after a cold reboot.
//! @warning The resource card (rev 3) device driver is sensitive to FPGA changes.  If present, wrap this call in rc3DeviceDisable() and rc3DeviceEnable().
//! @param[in] filename An ANSI string containing an absolute or relative (to the current directory) FPGA binary filename.  As of HIL 7.0,
//!                     there must also be a <code><i>\<filename\></i>.mta</code> metadata file in the same directory as <code><i>\<filename\></i></code>.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rc3FpgaLoad(_In_z_ LPCSTR filename);

//! @brief Programs an FPGA image file into the base SPI FLASH used to boot initialize the resource card (rev 3) FPGA.
//!
//! This function programs an FPGA image file into the base SPI FLASH used to boot initialize the resource card (rev 3) FPGA.
//!
//! The callback and context parameters are used to report progress back during a long operation.  See #HIL_BASE_LOAD_CALLBACK
//! for details.
//! @param[in] filename An ANSI string containing an absolute or relative (to the current directory) FPGA binary filename.  As of HIL 7.0,
//!                     there must also be a <code><i>\<filename\></i>.mta</code> metadata file in the same directory as <code><i>\<filename\></i></code>.
//! @param[in] callback The callback function to be called with progress updates while programming the SPI FLASH.  Value can be NULL.
//! @param[in] context The context that will be passed back as an argument in the #HIL_BASE_LOAD_CALLBACK callback function.  Value can be NULL.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rc3FpgaBaseLoad(_In_z_ LPCSTR filename, _In_opt_ HIL_BASE_LOAD_CALLBACK callback, _In_opt_ void* context);

//! @brief Validates the selected base SPI FLASH contents on the resource card (rev 3).
//!
//! This function validates the base SPI FLASH on the resource card (rev 3) and optionally dumps the contents to a file.
//! @param[in] dumpFilename  An ANSI string containing an absolute or relative (to the current directory) binary dump filename.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rc3FpgaBaseVerify(_In_opt_z_ LPCSTR dumpFilename);

//! @brief Loads an FPGA binary image buffer into the resource card (rev 3) FPGA.
//!
//! This function loads an FPGA binary image buffer into the resource card (rev 3) FPGA.  This image is volatile and the FPGA
//! will load its original base program from its SPI FLASH after a cold reboot.
//! @warning The resource card (rev 3) device driver is sensitive to FPGA changes.  If present, wrap this call in rc3DeviceDisable() and rc3DeviceEnable().
//! @param[in] pData The FPGA image data.
//! @param[in] length The length of the FPGA image data in bytes.
//! @param[in] pMetaData  The FPGA image metadata.  This data is in a side-by-side file with .MTA extension with the original FPGA image.  It is
//!                       loaded automatically by hbiMbFpgaLoad() but needs to be read and provided to this version of the function.
//! @param[in] metaLength The length of the FPGA image metadata in bytes.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rc3FpgaBufferLoad(_In_reads_bytes_(length) LPCVOID pData, _In_ DWORD length, _In_reads_bytes_opt_(metaLength) LPCVOID pMetaData, _In_ DWORD metaLength);

//! @brief Loads an FPGA image buffer into the base SPI FLASH used to boot initialize the resource card (rev 3) FPGA.
//!
//! This function loads an FPGA image buffer into the base SPI FLASH used to boot initialize the resource card (rev 3) FPGA.
//!
//! The callback and context parameters are used to report progress back during a long operation.  See #HIL_BASE_LOAD_CALLBACK
//! for details.
//! @param[in] pData    The FPGA image data.
//! @param[in] length   The length of the FPGA image data in bytes.
//! @param[in] pMetaData  The FPGA image metadata.  This data is in a side-by-side file with .MTA extension with the original FPGA image.  It is
//!                       loaded automatically by hbiMbFpgaBaseLoad() but needs to be read and provided to this version of the function.
//! @param[in] metaLength The length of the FPGA image metadata in bytes.
//! @param[in] callback The callback function to be called with progress updates while programming the SPI FLASH.  Value can be NULL.
//! @param[in] context  The context that will be passed back as an argument in the #HIL_BASE_LOAD_CALLBACK callback function.  Value can be NULL.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rc3FpgaBaseBufferLoad(_In_reads_bytes_(length) LPCVOID pData, _In_ DWORD length, _In_reads_bytes_opt_(metaLength) LPCVOID pMetaData, _In_ DWORD metaLength, _In_opt_ HIL_BASE_LOAD_CALLBACK callback, _In_opt_ void* context);

//! @brief Reads a 32-bit register in one of the BAR regions of the resource card (rev 3).
//!
//! This function reads a 32-bit register in one of the BAR regions of the resource card (rev 3).
//! @param[in] bar The PCI base address register region to access.  Valid values are 0-1.
//! @param[in] offset The DWORD-aligned offset address in BAR0 memory to read.
//! @param[out] pData The address of a DWORD to contain the result.  It cannot be NULL.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rc3BarRead(_In_ UINT bar, _In_ DWORD offset, _Out_ LPDWORD pData);

//! @brief Writes a 32-bit register in one of the BAR regions of the resource card (rev 3).
//!
//! This function writes a 32-bit register in one of the BAR regions of the resource card (rev 3).
//! @param[in] bar The PCI base address register region to access.  Valid values are 0-1.
//! @param[in] offset The DWORD-aligned offset address in BAR0 memory to write.
//! @param[in] data The DWORD value to write.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rc3BarWrite(_In_ UINT bar, _In_ DWORD offset, _In_ DWORD data);

//! @brief Reads from the DDR memory of the resource card (rev 3).
//!
//! This function reads from the 2GB DDR memory of the resource card (rev 3).
//! @param[in] address The zero-based address within the DDR memory to read.  This must be a multiple of four and the read must be within the DDR address space.
//! @param[out] pData The address of a buffer to hold the result.
//! @param[in] length The length of the \c pData buffer in bytes.  This must be a multiple of four and the read must be within the DDR address space.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rc3DmaRead(_In_ INT64 address, _Out_writes_bytes_all_(length) LPVOID pData, _In_ DWORD length);

//! @brief Writes to the DDR memory of the resource card (rev 3).
//!
//! This function writes to the 2GB DDR memory of the resource card (rev 3).
//! @param[in] address The zero-based address within the DDR memory to write.  This must be a multiple of four and the write must be within the DDR address space.
//! @param[in] pData The address of a buffer containing the data to write.
//! @param[in] length The length of the \c pData buffer in bytes.  This must be a multiple of four and the write must be within the DDR address space.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rc3DmaWrite(_In_ INT64 address, _In_reads_bytes_(length) const void* pData, _In_ DWORD length);

//! @brief Reads from the HPS DDR memory of the resource card (rev 3).
//!
//! This function reads from the 512MB HPS DDR memory region of the resource card (rev 3).
//! @param[in] address The zero-based address within the DDR memory to read.  This must be a multiple of four and the read must be within the DDR address space.
//! @param[out] pData The address of a buffer to hold the result.
//! @param[in] length The length of the \c pData buffer in bytes.  This must be a multiple of four and the read must be within the DDR address space.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rc3HpsDmaRead(_In_ INT64 address, _Out_writes_bytes_all_(length) LPVOID pData, _In_ DWORD length);

//! @brief Writes to the HPS DDR memory of the resource card (rev 3).
//!
//! This function writes to the 512MB HPS DDR memory region of the resource card (rev 3).
//! @param[in] address The zero-based address within the DDR memory to write.  This must be a multiple of four and the write must be within the DDR address space.
//! @param[in] pData The address of a buffer containing the data to write.
//! @param[in] length The length of the \c pData buffer in bytes.  This must be a multiple of four and the write must be within the DDR address space.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rc3HpsDmaWrite(_In_ INT64 address, _In_reads_bytes_(length) const void* pData, _In_ DWORD length);

//! @brief Reads the board-level traceability values from the resource card (rev 3).
//!
//! This function reads the board-level traceability values from the resource card (rev 3).  The values are defined in the #BLT structure.
//!
//! @param[out] pBlt The address of a #BLT structure that will be populated with the data.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rc3BltBoardRead(_Out_ PBLT pBlt);

//! @brief Writes the board-level traceability values of the resource card (rev 3).
//!
//! This function writes the board-level traceability values of the resource card (rev 3).  The values are defined in the #BLT structure.
//!
//! The \c flags field allows this function to update individual values.  The flags can be ORed together.  Only the fields indicated by
//! \c flags need to be populated in the provided \c pBlt parameter.
//!
//! \par Updating the Full BLT - C
//! \code{.c}
//! #include <Windows.h>
//! #include "Rc3Api.h"
//! #include "string.h"
//!
//! int main()
//! {
//!     BLT blt;
//!     HIL_STATUS status;
//!     blt.Id = 0x01000000;
//!     strcpy(blt.DeviceName,"DeviceName");
//!     strcpy(blt.VendorName,"INTEL");
//!     strcpy(blt.PartNumberAsBuilt,"AAxxxxxx-xxx");
//!     strcpy(blt.PartNumberCurrent,blt.PartNumberAsBuilt);
//!     strcpy(blt.SerialNumber,"12345678");
//!     strcpy(blt.ManufactureDate,"YYMMDD");
//!     status = rc3BltBoardWrite(&blt, BF_ALL);
//!     if (status != HS_SUCCESS)
//!         return -1;
//!     return 0;
//! }
//! \endcode
//! \par Updating the Current Part Number - Python
//! \code{.py}
//! blt = hil.BLT()
//! blt.PartNumberCurrent = 'AAyyyyyy-yyy'
//! hil.rc3BltBoardWrite(blt,hil.BF_PART_NUMBER_CURRENT)
//! # Note Python returns None for HIL_STATUS if successful and raises RuntimeError on failure.
//! \endcode
//!
//! \note HS_BLT_NOT_FOUND can be returned on an update (lack of #BF_ALL flag).
//!
//! @param[in] pBlt The address of a #BLT structure that contains the data to be written or updated.
//! @param[in] flags #BLT_FLAGS bits indicating what fields in \c pBlt are valid.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rc3BltBoardWrite(_In_ PCBLT pBlt, _In_ BLT_FLAGS flags);

//! @brief Reads the instrument-level traceability values of the resource card (rev 3).
//!
//! This function reads the instrument-level traceability values of the resource card (rev 3).
//!
//! @param[out] pBlt The address of a #BLT structure that will be populated with the BLT data.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rc3BltInstrumentRead(_Out_ PBLT pBlt);

//! @brief Writes the instrument-level traceability values of the resource card (rev 3).
//!
//! This function writes the instrument-level traceability values of the resource card (rev 3).
//!
//! The \c flags field allows this function to update individual values.  The flags can be ORed together.  Only the fields indicated by
//! \c flags need to be populated in the provided \c pBlt parameter.
//!
//! \par Updating the Full BLT - C
//! \code{.c}
//! #include <Windows.h>
//! #include "Rc3Api.h"
//! #include "string.h"
//!
//! int main()
//! {
//!     BLT blt;
//!     HIL_STATUS status;
//!     blt.Id = 0x01000000;
//!     strcpy(blt.DeviceName,"DeviceName");
//!     strcpy(blt.VendorName,"INTEL");
//!     strcpy(blt.PartNumberAsBuilt,"AAxxxxxx-xxx");
//!     strcpy(blt.PartNumberCurrent,blt.PartNumberAsBuilt);
//!     strcpy(blt.SerialNumber,"12345678");
//!     strcpy(blt.ManufactureDate,"YYMMDD");
//!     status = rc3BltBoardWrite(&blt, BF_ALL);
//!     if (status != HS_SUCCESS)
//!         return -1;
//!     return 0;
//! }
//! \endcode
//! \par Updating the Current Part Number - Python
//! \code{.py}
//! blt = hil.BLT()
//! blt.PartNumberCurrent = 'AAyyyyyy-yyy'
//! hil.rc3BltInstrumentWrite(blt,hil.BF_PART_NUMBER_CURRENT)
//! # Note Python returns None for HIL_STATUS if successful and raises RuntimeError on failure.
//! \endcode
//!
//! \note HS_BLT_NOT_FOUND can be returned on an update (lack of #BF_ALL flag).
//!
//! @param[in] pBlt The address of a #BLT structure that contains the data to be written or updated.
//! @param[in] flags #BLT_FLAGS bits indicating what fields in \c pBlt are valid.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rc3BltInstrumentWrite(_In_ PCBLT pBlt, _In_ BLT_FLAGS flags);

//! @brief Writes data to the 24LC512 (EEPROM) device on the resource card (rev 3).
//!
//! This function writes data to the 24LC512 (EEPROM) device on the resource card (rev 3).
//!
//! @note This function returns #HS_UNSUPPORTED on resource card (rev 3) fab A.  It uses an FT240X BLT device instead of a 24LC512.
//!       Direct EEPROM write is not supported on fab A.  Alternatives are rc3BltBoardWrite() and rc3BltInstrumentWrite().
//! @param[in] address The location of the first byte in the EEPROM to which the data in the \c pData buffer will be written.
//! @param[in] pData   A data buffer containing data to write to the EEPROM device.
//! @param[in] length  The byte length of the \c pData buffer.
//! @retval HS_UNSUPPORTED This API is not supported on fab A.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rc3EepromWrite(_In_ DWORD address, _In_reads_bytes_(length) const void* pData, _In_ DWORD length);

//! @brief Reads data from the 24LC512 (EEPROM) device on the resource card (rev 3).
//!
//! This function reads data from the 24LC512 (EEPROM) device on the resource card (rev 3) fab B or later.
//!
//! @note This function returns #HS_UNSUPPORTED on resource card (rev 3) fab A.  It uses an FT240X BLT device instead.
//!       See rc3BltEepromRead() for fab A.
//! @param[in] address The location of the first byte in the EEPROM from which the data will be read.
//! @param[out] pData  A pointer to a data buffer in which the read data will be stored. Note that this buffer must have at least \c length
//!                    bytes allocated in order to complete successfully.
//! @param[in] length  The number of bytes to read from the EEPROM.
//! @retval HS_UNSUPPORTED This API is not supported on fab A.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rc3EepromRead(_In_ DWORD address, _Out_writes_bytes_all_(length) LPVOID pData, _In_ DWORD length);

//! @brief Reads raw data from resource card (rev 3) fab A's BLT EEPROM.
//! This function reads raw data from the resource card (rev 3) fab A's BLT EEPROM.
//!
//! @note This function returns #HS_UNSUPPORTED on fab B or later.  The FT240X BLT device was removed in favor of an I2C EEPROM.
//!       See rc3EepromRead() and rc3EepromWrite() to read/write the I2C EEPROM on fab B or later.
//!
//! @param[out] pEepromData The output buffer.
//! @param[in,out] pLength On input, the size of the output buffer referenced by \c pData.  On output, the size of the EEPROM data.
//! @retval HS_INSUFFICIENT_BUFFER The buffer provided was too small.  \c pLength will indicate the minimum buffer size required.
//! @retval HS_UNSUPPORTED This API is not supported on fab B or later.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rc3BltEepromRead(_Out_writes_bytes_to_(_Old_(*pLength), *pLength) LPVOID pEepromData, _Inout_ LPDWORD pLength);

//! @brief Reads the system identifier from the resource card (rev 3).
//!
//! This function reads the system identifier from the resource card (rev 3).
//! @param[out] pSysId A buffer for the returned system ID.  It cannot be NULL.  A buffer of #HIL_MAX_ID_STRING_SIZE is guaranteed
//!                    to be large enough to hold the identifier.
//! @param[in] length  The size of the \c pSysId buffer in bytes.
//! @retval HS_SUCCESS The system ID was returned successfully.
//! @retval HS_INSUFFICIENT_BUFFER The \c length of the \c pSysId buffer was insufficient to hold the result.
HIL_API HIL_STATUS rc3SysIdRead(_Out_writes_z_(length) LPSTR pSysId, _In_ DWORD length);

//! @brief Writes a system identifier to the resource card (rev 3).
//!
//! This function writes a system identifier to the resource card (rev 3).
//! @param[in] pSysId  A pointer to a null-terminated system ID string no longer than #HIL_MAX_ID_STRING_SIZE including the null.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rc3SysIdWrite(_In_z_ LPCSTR pSysId);

//! @brief Reads the material equipment system identifier from the resource card (rev 3).
//!
//! This function reads the material equipment system(MES) identifier from the resource card (rev 3).
//! @param[out] pMesId A buffer for the returned MES ID.  It cannot be NULL.  A buffer of #HIL_MAX_ID_STRING_SIZE is guaranteed
//!                    to be large enough to hold the identifier.
//! @param[in] length  The size of the \c pMesId buffer in bytes.
//! @retval HS_SUCCESS The MES ID was returned successfully.
//! @retval HS_INSUFFICIENT_BUFFER The \c length of the \c pMesId buffer was insufficient to hold the result.
HIL_API HIL_STATUS rc3MesIdRead(_Out_writes_z_(length) LPSTR pMesId, _In_ DWORD length);

//! @brief Writes a material equipment system identifier to the resource card (rev 3).
//!
//! This function writes a material equipment system(MES) identifier to the resource card (rev 3).
//! @param[in] pMesId  A pointer to a null-terminated MES ID string no longer than #HIL_MAX_ID_STRING_SIZE including the null.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rc3MesIdWrite(_In_z_ LPCSTR pMesId);

//! @brief Enables or disables TIU alarm monitoring on the resource card (rev 3).
//!
//! This function enables or disables TIU alarm monitoring on the resource card (rev 3).  Supported alarms are defined in the #RC_ALARMS enumeration.
//! @param[in] enable TRUE to enable alarms, FALSE to disable.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rc3TiuAlarmEnable(_In_ BOOL enable);

//! @brief Blocks execution until a resource card (rev 3) alarm is received.
//!
//! This function blocks execution until a resource card (rev 3) alarm is received.  Supported alarms are defined in the #RC_ALARMS enumeration.
//! This function can be unblocked by calling rc3AlarmWaitCancel() in another thread of execution.
//! @param[out] pAlarms Reported alarms are returned as bits set in this output DWORD.  See #RC_ALARMS for bit definitions.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rc3AlarmWait(_Out_ LPDWORD pAlarms);

//! @brief Aborts the blocking rc3AlarmWait() function.
//!
//! This function aborts the blocking rc3AlarmWait() function.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rc3AlarmWaitCancel(void);

//! @brief Enables the hardware interrupt for resource card (rev 3) software triggers.
//!
//! This function enables the hardware interrupt for resource card (rev 3) software triggers.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rc3TriggerEnable(void);

//! @brief Disables the hardware interrupt for resource card (rev 3) software triggers.
//!
//! This function disables the hardware interrupt for resource card (rev 3) software triggers.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rc3TriggerDisable(void);

//! @brief Waits for a software trigger interrupt from the resource card (rev 3).
//!
//! This function waits for a software trigger interrupt from the resource card (rev 3) and returns with the trigger value and channel.
//!
//! This function does not return until a trigger interrupt is received.  Since the function is blocking, the user
//! should call it from a worker thread.  The wait can be canceled via another thread by calling rc3TriggerWaitCancel().
//! @note In simulation mode, the trigger value returned will be 0x12345678abcdef0.
//! @param[out] pTrigger The value of the trigger.
//!                      | Bits  | Description                                                              |
//!                      | :---: | :----------------------------------------------------------------------- |
//!                      | 63:48 | Reserved                                                                 |
//!                      | 47:32 | Aurora channel                                                           |
//!                      | 31:26 | Card type (0:Patgen, 1:DPS, 2:RCTC, 63:Broadcast, other values reserved) |
//!                      | 25:20 | DUT Domain ID                                                            |
//!                      |  19   | Software Trigger flag (0:not a software trigger, 1:software trigger)     |
//!                      | 18:0  | Payload (instrument defined)                                             |
//! @returns \ref HIL_STATUS
//! @see rc3TriggerEnable()
//! @see rc3TriggerDisable()
//! @see rc3TriggerWaitCancel()
HIL_API HIL_STATUS rc3TriggerWait(_Out_ PDWORDLONG pTrigger);

//! @brief Cancels a pending rc3TriggerWait().
//!
//! This function cancels a pending rc3TriggerWait().  Since rc3TriggerWait() is blocking, this must be performed from a separate thread of execution.
//! @returns \ref HIL_STATUS
//! @see rc3TriggerEnable()
//! @see rc3TriggerDisable()
//! @see rc3TriggerWait()
HIL_API HIL_STATUS rc3TriggerWaitCancel(void);

//! @brief Retrieves the resource card (rev 3) peer-to-peer base physical memory address.
//!
//! This function retrieves the resource card (rev 3) peer-to-peer base physical memory address required for DPS peer-to-peer capability.
//! @param[out] pPhysicalAddress The physical base address of the resource card (rev 3) peer-to-peer memory area.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rc3PeerToPeerMemoryBase(_Out_ PLARGE_INTEGER pPhysicalAddress);

//! @brief Retrieves the HPS mailbox user memory address.
//!
//! This function retrieves the HPS mailbox user memory address from the resource card (rev 3) FPGA.
//! @param[out] ppBase The virtual address in user memory where the HPS mailbox memory is mapped.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rc3HpsMailboxBase(_Out_ LPVOID* ppBase);

//! @brief Retrieves the user-mapped data bulk data streaming interface buffer for the resource card (rev 3) FPGA.
//!
//! This function retrieves the user-mapped bulk data data streaming interface buffer for the resource card (rev 3) FPGA.
//! @note This function exists for compatibility with older HDMT_RCTC3 drivers that only implemented the bulk data streaming interface.
//!       Use rc3StreamingBufferEx() to retrieve later-implemented streaming interfaces such as SCS (Serial Capture Stream).
//! @param[out] pAddress The returned virtual address of the buffer.
//! @param[out] pSize    The returned size of the buffer in bytes.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rc3StreamingBuffer(_Out_ LPVOID* pAddress, _Out_ LPDWORD pSize);

//! @brief Retrieves a specified user-mapped data streaming interface buffer for the resource card (rev 3) FPGA.
//!
//! This function retrieves a specified user-mapped data streaming interface buffer for the resource card (rev 3) FPGA.
//! @note Serial Capture Stream (index = 1) requires an HDMT_RCTC3 driver dated 08/21/2020 or later to retrieve the buffer or \ref HS_INVALID_PARAMETER is returned.
//! @param[in]  index    The streaming interface buffer index to retrieve.  See the following table:
//!                      | index | Streaming buffer      |
//!                      | :---: | :-------------------- |
//!                      |   0   | Bulk Data             |
//!                      |   1   | Serial Capture Stream |
//! @param[out] pAddress The returned virtual address of the buffer.
//! @param[out] pSize    The returned size of the buffer in bytes.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rc3StreamingBufferEx(_In_ UINT index, _Out_ LPVOID* pAddress, _Out_ LPDWORD pSize);

//! @brief Writes an 8-bit data register of the LMK04832 device on the resource card (rev 3).
//!
//! This function writes an 8-bit data register of the Texas Instruments LMK04832 device on the resource card (rev 3).
//! @note The registers are generally programmed in numeric order with 0x000 being the first and 0x555 being the last register
//! programmed. See the LMK04832 data sheet's recommended programming sequence for details.
//! @param[in] reg  The register to write.  Valid values are 0-0x555.  Not all registers are available.  See the data sheet.
//! @param[in] data This is the data value to write.  Valid values are 0-0xFF.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rc3Lmk04832Write(_In_ WORD reg, _In_ BYTE data);

//! @brief Reads an 8-bit data register of the LMK04832 device on the resource card (rev 3).
//!
//! This function reads an 8-bit data register of the Texas Instruments LMK04832 device on the resource card (rev 3).
//! @param[in]  reg   The register to read.  Valid values are 0-0x555.  Not all registers are available.  See the data sheet.
//! @param[out] pData The address of a byte that returns the 8-bit data.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rc3Lmk04832Read(_In_ WORD reg, _Out_ PBYTE pData);

//! @brief Issues a write command to the AD5064 device on the resource card (rev 3).
//!
//! This function issues a write command to the AD5064 device on the resource card (rev 3).
//! @param[in] command See Table 8 in spec sheet for valid commands. Valid values are 0-8.
//! @param[in] address See Table 9 in spec sheet for valid addresses.  Valid values are 0, 1, 2, 3, 15.
//! @param[in] data    16-bit data to write to the DAC.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rc3Ad5064Write(_In_ BYTE command, _In_ BYTE address, _In_ WORD data);

//! @brief Issues a read command to the PMBUS interface of the LTM4680 Power Manager device on the resource card (rev 3).
//!
//! This function issues a read command to the PMBUS interface of the LTM4680 Power Manager device on the resource card (rev 3).
//! @param[in]  page       Selects a page (corresponding to Channel 0 or Channel 1) for any command that supports paging.
//!                        This parameter may be -1 for commands that do not support paging.
//! @param[in]  command    PMBUS command to read.  See the device spec sheet for valid commands.
//! @param[out] pReadData  A data buffer of bytes to hold the data read from the associated \c command.
//!                        Note that the bytes received from the device are located sequentially
//!                        in the buffer in the order they were received with the first byte being located
//!                        directly at \c pReadData.  This parameter cannot be NULL since all reads return at least one byte.
//! @param[in]  readLength The byte length of the \c pReadData buffer.  Note that this parameter cannot be zero
//!                        since all reads return at least one byte.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rc3Ltm4680PmbusRead(_In_ INT page, _In_ BYTE command, _Out_writes_bytes_all_(readLength) LPVOID pReadData, _In_ DWORD readLength);

//! @brief Issues a write command to the PMBUS interface of the LTM4680 Power Manager device on the resource card (rev 3).
//!
//! This function issues a write command to the PMBUS interface of the LTM4680 Power Manager device on the resource card (rev 3).
//! @param[in] page      Selects a page (corresponding to Channel 0 or Channel 1) for any command that supports paging.
//!                      This parameter may be -1 for commands that do not support paging.
//! @param[in] command   PMBUS command to write. See the device spec sheet for valid commands.
//! @param[in] pCmdData  A data buffer of additional bytes to write for the associated \c command.
//!                      Note that the bytes are sent to the device sequentially starting with the first
//!                      byte pointed to by \c pCmdData.  The command byte should not be included in this
//!                      buffer as it is already supplied in \c command.  Note that this parameter may be
//!                      NULL for commands that have no data associated with them.
//! @param[in] cmdLength The byte length of the \c pCmdData buffer.  Note that this parameter may be zero for commands
//!                      that have no data.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rc3Ltm4680PmbusWrite(_In_ INT page, _In_ BYTE command, _In_reads_bytes_opt_(cmdLength) const void* pCmdData, _In_ DWORD cmdLength);

//! @brief Reads voltage monitor channels associated with housekeeping (MAX1230) ADCs on the resource card (rev 3).
//!
//! This function reads voltage monitor channels associated with housekeeping (MAX1230) ADCs on the resource card (rev 3).
//! @param[in] channel  Selects the monitoring channel. Valid values are 0-31.  The channels
//!                     and corresponding voltages monitored are (per resource card (rev 3) schematic):
//!                    | Channel |    Pin     |        Reference             |
//!                    | :-----: | :--------: | :--------------------------: |
//!                    |    0    | U187 AIN0  |  3P6V_8A_MON                 |
//!                    |    1    | U187 AIN1  |  1P2V_2P5A_MON               |
//!                    |    2    | U187 AIN2  |  3P3V_VX-501_600MA_MON       |
//!                    |    3    | U187 AIN3  |  3P3V_LMK04832_PLL_600MA_MON |
//!                    |    4    | U187 AIN4  |  3P3V_LMK04832_CLK_MON       |
//!                    |    5    | U187 AIN5  |  3P3V_RCTC_2P5A_MON          |
//!                    |    6    | U187 AIN6  |  3P3V_HPS_SD_MON             |
//!                    |    7    | U187 AIN7  |  V_CNTL_Y9_R                 |
//!                    |    8    | U187 AIN8  |  VREFP_ADC_RC                |
//!                    |    9    | U187 AIN9  |  not connected               |
//!                    |    10   | U187 AIN10 |  not connected               |
//!                    |    11   | U187 AIN11 |  not connected               |
//!                    |    12   | U187 AIN12 |  N9P5V_A_1A_DIV2P5_INV       |
//!                    |    13   | U187 AIN13 |  N9P5V_B_1A_DIV2P5_INV       |
//!                    |    14   | U187 AIN14 |  19P5V_B_1A_DIV6             |
//!                    |    15   | U187 AIN15 |  IMON_R_V19                  |
//!                    |    16   | U186 AIN0  |  5P0V_8A_DIV2                |
//!                    |    17   | U186 AIN1  |  3P3V_16A_MON                |
//!                    |    18   | U186 AIN2  |  2P5V_VPP_DDR4_RC_1A_MON     |
//!                    |    19   | U186 AIN3  |  1P2V_VDD_DDR4_RC_5A_MON     |
//!                    |    20   | U186 AIN4  |  3P0V_3A_MON                 |
//!                    |    21   | U186 AIN5  |  1P03V_VCC_GXB_RC_4A_MON     |
//!                    |    22   | U186 AIN6  |  0P9V_VCCERAM_RC_4A_MON      |
//!                    |    23   | U186 AIN7  |  20P1V_4A_DIV6_MON           |
//!                    |    24   | U186 AIN8  |  0P85V_VCCP_RC_8A_MON        |
//!                    |    25   | U186 AIN9  |  0P94V_HPS_RC_8A_MON         |
//!                    |    26   | U186 AIN10 |  2P4V_VCCFUSE_RC_1A_MON      |
//!                    |    27   | U186 AIN11 |  12V_15A_DIV4                |
//!                    |    28   | U186 AIN12 |  N10P1V_A_1A_DIV2P74_INV     |
//!                    |    29   | U186 AIN13 |  N10P1V_B_1A_DIV2P74_INV     |
//!                    |    30   | U186 AIN14 |  19P5V_A_1A_DIV6             |
//!                    |    31   | U186 AIN15 |  1P8V_8A_MON                 |
//! @param[out] pVoltage The voltage in volts.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rc3VmonRead(_In_ UINT channel, _Out_ double* pVoltage);

//! @brief Reads Accessory Card voltage monitor channels associated with LTC2358 ADCs on the resource card (rev 3).
//!
//! This function reads Accessory Card voltage monitor channels associated with LTC2358 ADCs on the resource card (rev 3).
//!
//! @note This function reports true differential voltage at the specified channel INx (i.e. INx+ - INx-) and
//! does not factor in scaling introduced by voltage dividers (if any) on an Accessory Card.
//!
//! @param[in] channel  Selects the monitoring channel. Valid values are TBD.  The channels
//!                     and corresponding voltages monitored are (per resource card (rev 3) schematic):
//!                    | Channel |  Differential signal  |    Reference      |      Signals monitored on the Loopback Card     | expected voltage on the Loopback Card |
//!                    | :-----: | :-------------------: | :---------------: | :---------------------------------------------: | :-----------------------------------: |
//!                    |    0    |       U23 IN0         |  ADC0_SE_IN_0     |  +1P8V_8A - GND                                 |  1.8 V                                |
//!                    |    1    |       U23 IN1         |  ADC0_SE_IN_1     |  V12_15A - GND                                  |  12.15 V                              |
//!                    |    2    |       U23 IN2         |  ADC0_SE_IN_2     |  V5P0_8A - GND                                  |  5.0 V                                |
//!                    |    3    |       U23 IN3         |  ADC0_SE_IN_3     |  V3P3_16A - GND                                 |  3.3 V                                |
//!                    |    4    |       U23 IN4         |  ADC0_DIFF_IN_4   |  V1P2_2P5A - 12V_PIN30_15A and VN9P5_A_1A       |  1.2 - (-4.13) = 5.33 V               |
//!                    |    5    |       U23 IN5         |  ADC0_DIFF_IN_5   |  12V_PIN28_15A - VN9P5_B_1A                     |  12.0 - (-0.094) = 12.094 V           |
//!                    |    6    |       U23 IN6         |  ADC0_DIFF_IN_6   |  V3P3_16A - VN9P5_A_1A                          |  3.3 - (-4.30) = 7.6 V                |
//!                    |    7    |       U23 IN7         |  ADC0_DIFF_IN_7   |  V3P3_16A - VN9P5_A_1A                          |  3.3 - (-4.30) = 7.6 V                |
//!                    |    8    |       U198 IN0        |  ADC1_SE_IN_0     |  19P5V_PIN41_A_1A - GND                         |  5.6 V                                |
//!                    |    9    |       U198 IN1        |  ADC1_SE_IN_1     |  19P5V_PIN44_B_1A - GND                         |  5.6 V                                |
//!                    |    10   |       U198 IN2        |  ADC1_SE_IN_2     |  V19P5_B_1A - GND                               |  5.6 V                                |
//!                    |    11   |       U198 IN3        |  ADC1_SE_IN_3     |  V19P5_A_1A - GND                               |  5.6 V                                |
//!                    |    12   |       U198 IN4        |  ADC1_DIFF_IN_4   |  1P2V_PIN59_2P5A - VN9P5_A_1A and 5P0V_PIN20_8A |  1.2 - (-2.25) = 3.45 V               |
//!                    |    13   |       U198 IN5        |  ADC1_DIFF_IN_5   |  3P3V_PIN6_16A - VN9P5_A_1A                     |  3.3 - (-4.30) = 7.6 V                |
//!                    |    14   |       U198 IN6        |  ADC1_DIFF_IN_6   |  V3P3_16A - N9P5V_PIN53_A_1A                    |  3.3 - (-4.30) = 7.6 V                |
//!                    |    15   |       U198 IN7        |  ADC1_DIFF_IN_7   |  V1P2_2P5A - N9P5V_PIN56_B_1A                   |  1.2 - (-4.30) = 5.5 V                |
//! @param[in] softSpanCode Configures softSpan code for the specified channel. Note that the softSpan code determines input voltage range of the channel and dictates the precision of measurements.
//!                         Consult the following table for valid values.
//!                         | SoftSpan Code | Analog input range |
//!                         | :-----------: | :----------------: |
//!                         |       7       |  +/-12.5V          |
//!                         |       6       |  +/-12.207V        |
//!                         |       5       |  0V to 12.5V       |
//!                         |       4       |  0V to 12.207V     |
//!                         |       3       |  +/-6.25V          |
//!                         |       2       |  +/-6.104V         |
//!                         |       1       |  0V to 6.25V       |
//!                         |       0       |  Channel Disabled  |
//! @param[out] pVoltage The voltage in volts.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rc3Ltc2358VmonRead(_In_ UINT channel, _In_ UINT softSpanCode, _Out_ double* pVoltage);

//! @brief Reads temperature monitor channels on the resource card (rev 3).
//!
//! This function reads temperature monitor channels on the resource card (rev 3).
//! @param[in] channel Selects the monitoring channel. Valid values are TBD.
//!                    The channels and corresponding temperatures are (per resource card (rev 3) schematic):
//!                    | Channel | Description                         |
//!                    | :-----: | :---------------------------------- |
//!                    |    0    | RCTC3 FPGA core temperature         |
//!                    |    1    | MAX1230 (U187) internal temperature |
//!                    |    2    | MAX1230 (U186) internal temperature |
//! @param[out] pTemp The temperature in degrees Celsius.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rc3TmonRead(_In_ UINT channel, _Out_ double* pTemp);

//! @brief Reads the fan tachometer on the resource card (rev 3).
//!
//! This function reads the fan tachometer on the resource card (rev 3).
//!
//! @warning To use this function, the desired mode must be set using rc3FanModeSet().
//! Typically, Standalone Safety System (SASS), is expected to to do this.
//! Do not set the mode if already set by another process (like SASS). However, if a process which had initialized the chip mode is killed,
//! it must be set again, once, before using this function.
//! @param[out] pTach The fan speed in revolutions per second (RPS).
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rc3FanRead(_Out_ double* pTach);

//! @brief Sets the operating mode of the fan controller on the resource card (rev 3).
//!
//! This function sets the operating mode of the fan controller on the resource card (rev 3).
//! @param[in] mode One the three MAX6650 operating modes. Consult the following table.
//!                    | mode | Operating Mode selected | Description
//!                    | :--: | :---------------------: | :---------------
//!                    |  0   | Software off            | Fan off.  \c ratedSpeed parameter is ignored.
//!                    |  1   | Closed-loop             | Fans are automatically maintained at a requested speed.
//!                    |  2   | Software full-on        | Maximum voltage applied to fan.  \c ratedSpeed parameter is ignored.
//! @param[in] ratedFanSpeed The nominal fan speed in RPM for the connected fans for closed loop mode.
//!                          Valid values are 600-15000 for closed loop mode.  Use 0 for other modes.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rc3FanModeSet(_In_ UINT mode, _In_ UINT ratedFanSpeed);

//! @brief Writes a register of the MAX6650 fan controller on the resource card (rev 3).
//!
//! This function writes a register of the MAX6650 fan controller on the resource card (rev 3).
//! @param[in] reg Writable MAX6650 register address.
//!                 | Register | Name         | Function              |
//!                 | :------: | :----------: | :-------------------- |
//!                 |   0x00   | SPEED        | Fan speed             |
//!                 |   0x02   | CONFIG       | Configuration         |
//!                 |   0x04   | GPIO DEF     | GPIO definition       |
//!                 |   0x06   | DAC          | DAC                   |
//!                 |   0x08   | ALARM ENABLE | Alarm Enable          |
//!                 |   0x16   | COUNT        | Tachometer count time |
//! @param[in] data The value to write.  Valid values are 0-255 (00h-FFh).
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rc3Max6650Write(_In_ BYTE reg, _In_ BYTE data);

//! @brief Reads a register of the MAX6650 fan controller on the resource card (rev 3).
//!
//! This function reads a register of the MAX6650 fan controller on the resource card (rev 3).
//! @param[in] reg MAX6650 register address.
//!                 | Register | Name         | Function              |
//!                 | :------: | :----------: | :-------------------- |
//!                 |   0x00   | SPEED        | Fan speed             |
//!                 |   0x02   | CONFIG       | Configuration         |
//!                 |   0x04   | GPIO DEF     | GPIO definition       |
//!                 |   0x06   | DAC          | DAC                   |
//!                 |   0x08   | ALARM ENABLE | Alarm Enable          |
//!                 |   0x0A   | ALARM        | Alarm Status          |
//!                 |   0x0C   | TACH0        | Tachometer 0 count    |
//!                 |   0x0E   | TACH1        | Tachometer 1 count    |
//!                 |   0x10   | TACH2        | Tachometer 2 count    |
//!                 |   0x12   | TACH3        | Tachometer 3 count    |
//!                 |   0x14   | GPIO STAT    | GPIO Status           |
//!                 |   0x16   | COUNT        | Tachometer count time |
//! @param[out] pData The register contents.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS rc3Max6650Read(_In_ BYTE reg, _Out_ PBYTE pData);

#ifdef __cplusplus
}
#endif
