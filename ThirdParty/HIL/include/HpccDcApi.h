// INTEL CONFIDENTIAL
// Copyright 2014-2018 Intel Corporation All Rights Reserved.
//
// The source code contained or described herein and all documents related to
// the source code ("Material") are owned by Intel Corporation or its
// suppliers or licensors. Title to the Material remains with Intel
// Corporation or its suppliers and licensors. The Material contains trade
// secrets and proprietary and confidential information of Intel or its
// suppliers and licensors. The Material is protected by worldwide copyright
// and trade secret laws and treaty provisions. No part of the Material may be
// used, copied, reproduced, modified, published, uploaded, posted,
// transmitted, distributed, or disclosed in any way without Intel's prior
// express written permission.
//
// No license under any patent, copyright, trade secret or other intellectual
// property right is granted to or conferred upon you by disclosure or
// delivery of the Materials, either expressly, by implication, inducement,
// estoppel or otherwise. Any license under such intellectual property rights
// must be express and approved by Intel in writing.

//! @file
//! @brief APIs to control the HDMT HPCC-DC mainboard.
#pragma once
#include "HilDefs.h"

#ifdef __cplusplus
extern "C" {
#endif

//! @brief Verifies the HDMT HPCC-DC mainboard is present.
//!
//! This function verifies the HDMT Gen 2 digital channel card (HPCC-DC) is present.  It connects to and caches driver resources for use
//! by other \c hpccDcXXXXX functions.  hpccDcDisconnect() is its complementary function and frees any cached resources.
//!
//! Neither hpccDcConnect() or hpccDcDisconnect() are required to be called to use the other \c hpccDcXXXXX functions.  All
//! \c hpccDcXXXXX functions will allocate resources if they are not already allocated and free them if required to
//! perform their function.
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hpccDcConnect(_In_ INT slot);

//! @brief Frees any resources cached from using the HPCC-DC mainboard functions.
//!
//! This function frees any resources associated with using the HPCC-DC mainboard HIL functions. hpccDcConnect() is its
//! complementary function.
//!
//! Neither hpccDcConnect() or hpccDcDisconnect() are required to be called to use the other \c hpccDcXXXXX functions.  All
//! \c hpccDcXXXXX functions will allocate resources if they are not already allocated and free them if required to
//! perform their function.
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hpccDcDisconnect(_In_ INT slot);

//! @brief Programs the vendor and product IDs of all USB devices on an HPCC-DC mainboard.
//!
//! This function programs the vendor and product IDs (VID/PIDs) of the FT240X and Cypress
//! USB devices on an HPCC-DC mainboard.  Correct VID/PIDs are required to ensure Windows Device
//! Manager displays a proper description of the device, and HIL requires correct VID/PIDs to ensure
//! software can locate and communicate with the correct component.
//! @note FTDI devices have a limited number of updates, but this function is safe to call because it
//! only writes if the component programming is incorrect.
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hpccDcVidPidsSet(_In_ INT slot);

//! @brief Reads an HPCC-DC mainboard's CPLD version number.
//!
//! This function reads the CPLD version number from an HPCC-DC mainboard.
//!
//! @param[in] slot A physical slot number.  Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[out] pVersion Returns the version register value.  The pointer must not be NULL.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hpccDcCpldVersion(_In_ INT slot, _Out_ LPDWORD pVersion);

//! @brief Reads an HPCC-DC mainboard's CPLD version string.
//!
//! This function reads the version register of an HPCC-DC mainboard's CPLD and converts it to an ASCII string representation.
//!
//! @param[in] slot A physical slot number.  Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[out] pVersion Returns the version string.  The pointer must not be NULL.
//! @param[in] length The length of the \c pVersion buffer.  It should be at least five characters to hold a null-terminated version string.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hpccDcCpldVersionString(_In_ INT slot, _Out_writes_z_(length) LPSTR pVersion, _In_ DWORD length);

//! @brief Reads the HPCC-DC mainboard FPGA version numbers.
//!
//! This function reads the version number from one of the two FPGAs on the HPCC-DC mainboard.  The returned 32-bit value is in the
//! format \c 0xMMmmttrr where \c MM is the major version, \c mm is the minor version, \c tt is the optional snap-in board type,
//! and \c rr is the snap-in board revision.
//!
//! For example 0x05010000 is version 5.1 with no snap-in board.
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] fpgaIndex Selects one of the two FPGAs: 0 or 1.
//! @param[out] pVersion The address of a DWORD of memory.  It may not be NULL.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hpccDcFpgaVersion(_In_ INT slot, _In_ INT fpgaIndex, _Out_ LPDWORD pVersion);

//! @brief Disables PCI device drivers for the HPCC-DC FPGAs in a slot.
//!
//! This function disables PCI device drivers for the HPCC-DC FPGAs in a slot.  It should be called before using functions
//! such as hpccDcFpgaLoad() that affect the hardware used by the drivers.
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] fpgaIndex Selects the appropriate EEPROM for one of the two FPGAs: 0 or 1.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hpccDcDeviceDisable(_In_ INT slot, _In_ INT fpgaIndex);

//! @brief Enables PCI device drivers for the HPCC-DC FPGAs in a slot.
//!
//! This function enables PCI device drivers for the HPCC-DC FPGAs in a slot.  It should be called after using functions
//! such as hpccDcFpgaLoad() that affect the hardware used by the drivers.
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] fpgaIndex Selects the appropriate EEPROM for one of the two FPGAs: 0 or 1.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hpccDcDeviceEnable(_In_ INT slot, _In_ INT fpgaIndex);

//! @brief Loads an FPGA binary image file into an HPCC-DC mainboard FPGA.
//!
//! This function loads an FPGA binary image file into an HPCC-DC mainboard FPGA.  This image is volatile and the FPGA
//! will load its original base program from its SPI FLASH after a cold reboot.
//! @warning HPCC-DC device drivers are sensitive to FPGA changes.  If they are present, wrap this call in hpccAcDeviceDisable() and hpccAcDeviceEnable().
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] fpgaIndex Selects one of the two FPGAs: 0 or 1.
//! @param[in] filename An ANSI string containing an absolute or relative (to the current directory) FPGA binary filename.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hpccDcFpgaLoad(_In_ INT slot, _In_ INT fpgaIndex, _In_z_ LPCSTR filename);

//! @brief Programs an FPGA image file into the base SPI FLASH used to boot initialize an HPCC-DC mainboard FPGA.
//!
//! This function programs an FPGA image file into the base SPI FLASH used to boot initialize an HPCC-DC mainboard FPGA.
//!
//! The callback and context parameters are used to report progress back during a long operation.  See #HIL_BASE_LOAD_CALLBACK
//! for details.
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] fpgaIndex Selects the appropriate SPI FLASH for one of the two FPGAs: 0 or 1.
//! @param[in] filename An ANSI string containing an absolute or relative (to the current directory) FPGA binary filename.
//! @param[in] callback The callback function to be called with progress updates while programming the SPI FLASH.  Value can be NULL.
//! @param[in] context The context that will be passed back as an argument in the #HIL_BASE_LOAD_CALLBACK callback function.  Value can be NULL.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hpccDcFpgaBaseLoad(_In_ INT slot, _In_ INT fpgaIndex, _In_z_ LPCSTR filename, _In_opt_ HIL_BASE_LOAD_CALLBACK callback, _In_opt_ void* context);

//! @brief Compares an existing image within the base SPI FLASH on an HPCC-DC mainboard against an FPGA binary image file.
//!
//! This function compares an existing image within the base SPI FLASH on an HPCC-DC mainboard against an FPGA binary image file
//! and optionally dumps that image into a separate binary file.
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] fpgaIndex Selects one of the two FPGAs: 0 or 1.
//! @param[in] imageFilename An ANSI string containing an absolute or relative (to the current directory) FPGA binary filename to be compared against.
//! @param[in] dumpFilename An ANSI string containing an absolute or relative (to the current directory) binary dump filename.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hpccDcFpgaBaseVerify(_In_ INT slot, _In_ INT fpgaIndex, _In_z_ LPCSTR imageFilename, _In_opt_z_ LPCSTR dumpFilename);

//! @brief Executes a Xilinx .XSVF file over an HPCC-DC mainboard's JTAG interface.
//!
//! This function executes a Xilinx .XSVF file over an HPCC-DC mainboard's JTAG interface.  A Xilinx CPLD
//! and both FPGAs are in the scan chain.  An appropriate .XSVF file can be generated through Xilinx's iMPACT software to perform
//! functions such as programming and/or verifying the content of the CPLD.
//! @warning The HPCC-DC device drivers are not sensitive to changing the CPLD firmware, unlike the HPCC-AC device drivers, but
//! if the FPGAs are targeted and the drivers are present, wrap this call in hpccDcDeviceDisable() and hpccDcDeviceEnable().
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] filename An ANSI string containing an absolute or relative (to the current directory) CPLD .XSVF binary filename.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hpccDcJtagExecute(_In_ INT slot, _In_z_ LPCSTR filename);

//! @brief Loads an FPGA binary image buffer into an HPCC-DC mainboard FPGA.
//!
//! This function loads an FPGA binary image buffer into an HPCC-DC mainboard FPGA.  This image is volatile and the FPGA
//! will load its original base program from its SPI FLASH after a cold reboot.
//! @warning HPCC-DC device drivers are sensitive to FPGA changes.  If they are present, wrap this call in hpccAcDeviceDisable() and hpccAcDeviceEnable().
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] fpgaIndex Selects one of the two FPGAs: 0 or 1.
//! @param[in] pData The FPGA image data.
//! @param[in] length The length of the FPGA image data in bytes.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hpccDcFpgaBufferLoad(_In_ INT slot, _In_ INT fpgaIndex, _In_reads_bytes_(length) LPCVOID pData, _In_ DWORD length);

//! @brief Programs an FPGA image buffer into the base SPI FLASH used to boot initialize an HPCC-DC mainboard FPGA.
//!
//! This function programs an FPGA image buffer into the base SPI FLASH used to boot initialize an HPCC-DC mainboard FPGA.
//!
//! The callback and context parameters are used to report progress back during a long operation.  See #HIL_BASE_LOAD_CALLBACK
//! for details.
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] fpgaIndex Selects the appropriate EEPROM for one of the two FPGAs: 0 or 1.
//! @param[in] pData The FPGA image data.
//! @param[in] length The length of the FPGA image data in bytes.
//! @param[in] callback The callback function to be called with progress updates while programming the EEPROM.  Value can be NULL.
//! @param[in] context The context that will be passed back as an argument in the #HIL_BASE_LOAD_CALLBACK callback function.  Value can be NULL.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hpccDcFpgaBaseBufferLoad(_In_ INT slot, _In_ INT fpgaIndex, _In_reads_bytes_(length) LPCVOID pData, _In_ DWORD length, _In_opt_ HIL_BASE_LOAD_CALLBACK callback, _In_opt_ void* context);

//! @brief Execute a buffer containing Xilinx XSVF data over an HPCC-DC mainboard's JTAG interface.
//!
//! This function executes a Xilinx .XSVF file over an HPCC-DC mainboard's JTAG interface.  A Xilinx CPLD
//! and both FPGAs are in the scan chain.  An appropriate .XSVF file can be generated through Xilinx's iMPACT software to perform
//! functions such as programming and/or verifying the content of the CPLD.
//! @warning The HPCC-DC device drivers are not sensitive to changing the CPLD firmware, unlike the HPCC-AC device drivers, but
//! if the FPGAs are targeted and the drivers are present, wrap this call in hpccDcDeviceDisable() and hpccDcDeviceEnable().
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] pData The CPLD image data.
//! @param[in] length The length of the CPLD image data in bytes.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hpccDcJtagBufferExecute(_In_ INT slot, _In_reads_bytes_(length) LPCVOID pData, _In_ DWORD length);

//! @brief Reads a 32-bit register in one of the BAR regions of an HPCC-DC mainboard.
//!
//! This function reads a 32-bit register in one of the BAR regions of an HPCC-DC mainboard.
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] fpgaIndex Selects one of the two FPGAs: 0 or 1.
//! @param[in] bar The PCI base address register region to access.  Valid values are 0-2.
//! @param[in] offset The DWORD-aligned offset address in BAR memory to read.
//! @param[out] pData The address of a DWORD to contain the result.  It cannot be NULL.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hpccDcBarRead(_In_ INT slot, _In_ INT fpgaIndex, _In_ UINT bar, _In_ DWORD offset, _Out_ LPDWORD pData);

//! @brief Writes a 32-bit register in one of the BAR regions of an HPCC-DC mainboard.
//!
//! This function writes a 32-bit register in one the BAR regions of an HPCC-DC mainboard.
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] fpgaIndex Selects one of the two FPGAs: 0 or 1.
//! @param[in] bar The PCI base address register region to access.  Valid values are 0-2.
//! @param[in] offset The DWORD-aligned offset address in BAR memory to write.
//! @param[in] data The DWORD value to write.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hpccDcBarWrite(_In_ INT slot, _In_ INT fpgaIndex, _In_ UINT bar, _In_ DWORD offset, _In_ DWORD data);

//! @brief Reads from the DDR memory of an HPCC-DC mainboard.
//!
//! This function reads from the DDR memory of an HPCC-DC mainboard.
//!
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] fpgaIndex Selects one of the two FPGAs: 0 or 1.
//! @param[in] address The zero-based address within the DDR memory to read.
//! @param[out] pData The address of a buffer to hold the result.
//! @param[in] length The length of the \c pData buffer in bytes.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hpccDcDmaRead(_In_ INT slot, _In_ INT fpgaIndex, _In_ INT64 address, _Out_writes_bytes_all_(length) LPVOID pData, _In_ DWORD length);

//! @brief Writes to the DDR memory of an HPCC-DC mainboard.
//!
//! This function writes to the DDR memory of an HPCC-DC mainboard.
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] fpgaIndex Selects one of the two FPGAs: 0 or 1.
//! @param[in] address The zero-based address within the DDR memory to write.
//! @param[in] pData The address of a buffer containing the data to write.
//! @param[in] length The length of the \c pData buffer in bytes.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hpccDcDmaWrite(_In_ INT slot, _In_ INT fpgaIndex, _In_ INT64 address, _In_reads_bytes_(length) const void* pData, _In_ DWORD length);

//! @brief Reads the board-level traceability values from an HPCC instrument assembly.
//!
//! This function reads the board-level traceability values from an HPCC instrument assembly.  The values are defined in the #BLT structure.
//! This data refers to the HPCC card in the specified slot as a collective instrument, which consists of a DC motherboard and an AC daughterboard.
//! Use hpccDcBltBoardRead() to read the DC motherboard BLT, and use hpccAcBltBoardRead() to read the BLT of the AC daughterboard.
//!
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[out] pBlt The address of a #BLT structure that will be populated with the data.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hpccBltInstrumentRead(_In_ INT slot, _Out_ PBLT pBlt);

//! @brief Writes the board-level traceability values for an HPCC instrument assembly.
//!
//! This data refers to the HPCC card in the specified slot as a collective instrument, which consists of a DC motherboard and an AC daughterboard.
//! Use hpccDcBltBoardWrite() to write the DC motherboard BLT, and use hpccAcBltBoardWrite() to write the BLT of the AC daughterboard.
//!
//! The \c flags field allows this function to update individual values.  The flags can be ORed together.  Only the fields indicated by
//! \c flags need to be populated in the provided \c pBlt parameter.
//!
//! \par Updating the Full BLT - C
//! \code{.c}
//! #include <Windows.h>
//! #include "HpccDcApi.h"
//! #include "string.h"
//!
//! int main()
//! {
//!     int slot = 8;
//!     BLT blt;
//!     HIL_STATUS status;
//!     blt.Id = 0x10000036;
//!     strcpy(blt.DeviceName,"DeviceName");
//!     strcpy(blt.VendorName,"INTEL");
//!     strcpy(blt.PartNumberAsBuilt,"IAxxxxxx-xxx");
//!     strcpy(blt.PartNumberCurrent,blt.PartNumberAsBuilt);
//!     strcpy(blt.SerialNumber,"12345678");
//!     strcpy(blt.ManufactureDate,"YYMMDD");
//!     status = hpccBltInstrumentWrite(slot, &blt, BF_ALL);
//!     if (status != HS_SUCCESS)
//!         return -1;
//!     return 0;
//! }
//! \endcode
//! \par Updating the Current Part Number - Python
//! \code{.py}
//! slot = 8
//! blt = hil.BLT()
//! blt.PartNumberCurrent = 'IAyyyyyy-yyy'
//! hil.hpccBltInstrumentWrite(slot,blt,hil.BF_PART_NUMBER_CURRENT)
//! # Note Python returns None for HIL_STATUS if successful and raises RuntimeError on failure.
//! \endcode
//! \par Updating the Current Part Number - Python
//! \code{.py}
//! slot = 8
//! hil.hpccBltInstrumentWrite(slot,None,0) # Note flags is ignored
//! \endcode
//!
//! \note HS_BLT_NOT_FOUND can be returned on an update (lack of #BF_ALL flag).
//!
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] pBlt The address of a #BLT structure that contains the data to be written or updated.  If NULL, erases the existing instrument BLT.
//! @param[in] flags #BLT_FLAGS bits indicating what fields in \c pBlt are valid.  Ignored if pBlt is NULL.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hpccBltInstrumentWrite(_In_ INT slot, _In_opt_ PCBLT pBlt, _In_ BLT_FLAGS flags);

//! @brief Reads the board-level traceability values from an HPCC-DC mainboard.
//!
//! This function reads the board-level traceability values from an HPCC-DC mainboard component.  The values are defined in the #BLT structure.
//! This data refers to the DC motherboard component only, and not the assembled instrument.
//! Use hpccBltInstrumentRead() to read the BLT of the collective instrument.
//!
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[out] pBlt The address of a #BLT structure that will be populated with the data.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hpccDcBltBoardRead(_In_ INT slot, _Out_ PBLT pBlt);

//! @brief Writes the board-level traceability values for an HPCC-DC mainboard.
//!
//! This function writes the board-level traceability values for an HPCC-DC mainboard component.  The values are defined in the #BLT structure.
//! This data refers to the DC motherboard component only, and not the assembled instrument.
//! Use hpccBltInstrumentWrite() to write the BLT of the collective instrument.
//!
//! The \c flags field allows this function to update individual values.  The flags can be ORed together.  Only the fields indicated by
//! \c flags need to be populated in the provided \c pBlt parameter.
//!
//! \par Updating the Full BLT - C
//! \code{.c}
//! #include <Windows.h>
//! #include "HpccDcApi.h"
//! #include "string.h"
//!
//! int main()
//! {
//!     int slot = 8;
//!     BLT blt;
//!     HIL_STATUS status;
//!     blt.Id = 0x10000036;
//!     strcpy(blt.DeviceName,"DeviceName");
//!     strcpy(blt.VendorName,"INTEL");
//!     strcpy(blt.PartNumberAsBuilt,"AAxxxxxx-xxx");
//!     strcpy(blt.PartNumberCurrent,blt.PartNumberAsBuilt);
//!     strcpy(blt.SerialNumber,"12345678");
//!     strcpy(blt.ManufactureDate,"YYMMDD");
//!     status = hpccDcBltBoardWrite(slot, &blt, BF_ALL);
//!     if (status != HS_SUCCESS)
//!         return -1;
//!     return 0;
//! }
//! \endcode
//! \par Updating the Current Part Number - Python
//! \code{.py}
//! slot = 8
//! blt = hil.BLT()
//! blt.PartNumberCurrent = 'AAyyyyyy-yyy'
//! hil.hpccDcBltBoardWrite(slot,blt,hil.BF_PART_NUMBER_CURRENT)
//! # Note Python returns None for HIL_STATUS if successful and raises RuntimeError on failure.
//! \endcode
//!
//! \note HS_BLT_NOT_FOUND can be returned on an update (lack of #BF_ALL flag).
//!
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] pBlt The address of a #BLT structure that contains the data to be written or updated.
//! @param[in] flags #BLT_FLAGS bits indicating what fields in \c pBlt are valid.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hpccDcBltBoardWrite(_In_ INT slot, _In_ PCBLT pBlt, _In_ BLT_FLAGS flags);

//! @brief Reads the raw data from the specified HPCC-DC mainboard's BLT EEPROM.
//!
//! This function reads the raw data from the specified HPCC-DC mainboard's BLT EEPROM.
//!
//! @param[in] slot A physical slot number.  Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[out] pEepromData The output buffer.
//! @param[in,out] pLength On input, the size of the output buffer referenced by \c pData.  On output, the size of the EEPROM data.
//! @retval HS_INSUFFICIENT_BUFFER The buffer provided was too small.  \c pLength will indicate the minimum buffer size required.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hpccDcBltEepromRead(_In_ INT slot, _Out_writes_bytes_to_(_Old_(*pLength), *pLength) LPVOID pEepromData, _Inout_ LPDWORD pLength);

//! @brief Programs the vendor and product IDs of the FT240X device on an HPCC manufacturing test snap-in card.
//!
//! This function programs the vendor and product IDs (VID/PIDs) of the FT240X USB device on an
//! HPCC manufacturing test snap-in card.  Correct VID/PIDs are required to ensure Windows Device
//! Manager displays a proper description of the device, and HIL requires correct VID/PIDs to ensure
//! software can locate and communicate with the correct component.
//! @note FTDI devices have a limited number of updates, but this function is safe to call because it
//! only writes if the component programming is incorrect.
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hpccSnicTestVidPidSet(_In_ INT slot);

//! @brief Reads the board-level traceability values from an HPCC manufacturing test snap-in card.
//!
//! This function reads the board-level traceability values from an HPCC manufacturing test snap-in card.  The values are defined in the #BLT structure.
//! This data refers to the AC test SNIC component only, and not the assembled instrument.
//! Use hpccBltInstrumentRead() to read the BLT of the collective instrument.
//!
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[out] pBlt The address of a #BLT structure that will be populated with the data.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hpccSnicTestBltBoardRead(_In_ INT slot, _Out_ PBLT pBlt);

//! @brief Writes the board-level traceability values for an HPCC daughterboard.
//!
//! This function writes the board-level traceability values for an HPCC manufacturing test snap-in card.  The values are defined in the #BLT structure.
//! This data refers to the AC test SNIC component only, and not the assembled instrument.
//! Use hpccBltInstrumentWrite() to write the BLT of the collective instrument.
//!
//! The \c flags field allows this function to update individual values.  The flags can be ORed together.  Only the fields indicated by
//! \c flags need to be populated in the provided \c pBlt parameter.
//!
//! \par Updating the Full BLT - C
//! \code{.c}
//! #include <Windows.h>
//! #include "HpccAcApi.h"
//! #include "string.h"
//!
//! int main()
//! {
//!     int slot = 8;
//!     BLT blt;
//!     HIL_STATUS status;
//!     blt.Id = 0x10000000;
//!     strcpy(blt.DeviceName,"DeviceName");
//!     strcpy(blt.VendorName,"INTEL");
//!     strcpy(blt.PartNumberAsBuilt,"AAxxxxxx-xxx");
//!     strcpy(blt.PartNumberCurrent,blt.PartNumberAsBuilt);
//!     strcpy(blt.SerialNumber,"12345678");
//!     strcpy(blt.ManufactureDate,"YYMMDD");
//!     status = hpccSnicTestBltBoardWrite(slot, &blt, BF_ALL);
//!     if (status != HS_SUCCESS)
//!         return -1;
//!     return 0;
//! }
//! \endcode
//! \par Updating the Current Part Number - Python
//! \code{.py}
//! slot = 8
//! blt = hil.BLT()
//! blt.PartNumberCurrent = 'AAyyyyyy-yyy'
//! hil.hpccSnicTestBltBoardWrite(slot,blt,hil.BF_PART_NUMBER_CURRENT)
//! # Note Python returns None for HIL_STATUS if successful and raises RuntimeError on failure.
//! \endcode
//!
//! \note HS_BLT_NOT_FOUND can be returned on an update (lack of #BF_ALL flag).
//!
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] pBlt The address of a #BLT structure that contains the data to be written or updated.
//! @param[in] flags #BLT_FLAGS bits indicating what fields in \c pBlt are valid.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hpccSnicTestBltBoardWrite(_In_ INT slot, _In_ PCBLT pBlt, _In_ BLT_FLAGS flags);

//! @brief Reads the raw data from the specified HPCC manufacturing test snap-in card's BLT EEPROM.
//!
//! This function reads the raw data from the specified HPCC manufacturing test snap-in card's BLT EEPROM.
//!
//! @param[in] slot A physical slot number.  Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[out] pEepromData The output buffer.
//! @param[in,out] pLength On input, the size of the output buffer referenced by \c pData.  On output, the size of the EEPROM data.
//! @retval HS_INSUFFICIENT_BUFFER The buffer provided was too small.  \c pLength will indicate the minimum buffer size required.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hpccSnicTestBltEepromRead(_In_ INT slot, _Out_writes_bytes_to_(_Old_(*pLength), *pLength) LPVOID pEepromData, _Inout_ LPDWORD pLength);

//! @brief Reads the temperature monitor for one of fifty-six ADATE320 devices on the HPCC mainboard.
//!
//! This function reads the temperature monitor for one of fifty-six ADATE320 devices on the HPCC mainboard.
//!
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] device Selects one of the fifty-six ADATE320 devices: 0 to 55.
//! @param[out] pTemp The temperature in degrees Celsius.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hpccAdAte320TmonRead(_In_ INT slot, _In_ INT device, _Out_ double* pTemp);

//! @brief Reads the internal temperature monitor for one of four MAX1230 devices on the HPCC mainboard.
//!
//! This function reads the internal temperature monitor for one of four MAX1230 devices on the HPCC mainboard.
//!
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] device Selects one of the three MAX1230 devices: 0 to 3.
//!                   The channel and corresponding temperatures monitored are:
//!                   | Device | Chip | Description                  |
//!                   | :----: | :--: | :--------------------------- |
//!                   | 0      | U11  | MAX1230 Internal Temperature |
//!                   | 1      | U17  | MAX1230 Internal Temperature |
//!                   | 2      | U19  | MAX1230 Internal Temperature |
//!                   | 3      | U27  | MAX1230 Internal Temperature |
//! @param[out] pTemp The temperature in degrees Celsius.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hpccMax1230TmonRead(_In_ INT slot, _In_ INT device, _Out_ double* pTemp);

//! @brief Reads the temperature monitor for one of two AC FPGAs on the HPCC daughterboard.
//!
//! This function reads the temperature monitor for one of two AC FPGAs on the HPCC daughterboard.
//!
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] fpgaIndex Selects one of the two FPGAs: 0 or 1.
//! @param[out] pTemp The temperature in degrees Celsius.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hpccAcFpgaTmonRead(_In_ INT slot, _In_ INT fpgaIndex, _Out_ double* pTemp);

//! @brief Reads one of eight voltage monitor channels on the HPCC mainboard.
//!
//! This function reads one of eight voltage monitor channels on the HPCC mainboard.
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] channel Selects the monitoring channel. Valid values are 0 to 7.
//!                    The channels and corresponding voltages monitored are:
//!                    | Channel | Chip | Reference     |
//!                    | :-----: | :--: | :-----------: |
//!                    | 0       | U17  | 8V30A_ADC_IN  |
//!                    | 1       | U17  | -5V20A_ADC_IN |
//!                    | 2       | U17  | +0.9V_12A     |
//!                    | 3       | U17  | +1.2V_12A     |
//!                    | 4       | U27  | +1.5V_12A     |
//!                    | 5       | U27  | +1.8V_12A     |
//!                    | 6       | U27  | +2.5V_12A     |
//!                    | 7       | U27  | GND_DUT       |
//! @param[out] pVoltage The voltage in volts.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hpccVmonRead(_In_ INT slot, _In_ INT channel, _Out_ double* pVoltage);

#ifdef __cplusplus
}
#endif
