// INTEL CONFIDENTIAL
// Copyright 2017-2018 Intel Corporation All Rights Reserved.
//
// The source code contained or described herein and all documents related to
// the source code ("Material") are owned by Intel Corporation or its
// suppliers or licensors. Title to the Material remains with Intel
// Corporation or its suppliers and licensors. The Material contains trade
// secrets and proprietary and confidential information of Intel or its
// suppliers and licensors. The Material is protected by worldwide copyright
// and trade secret laws and treaty provisions. No part of the Material may be
// used, copied, reproduced, modified, published, uploaded, posted,
// transmitted, distributed, or disclosed in any way without Intel's prior
// express written permission.
//
// No license under any patent, copyright, trade secret or other intellectual
// property right is granted to or conferred upon you by disclosure or
// delivery of the Materials, either expressly, by implication, inducement,
// estoppel or otherwise. Any license under such intellectual property rights
// must be express and approved by Intel in writing.

//! @file
//! @brief APIs to control an HDMT HVDPS card.
#pragma once
#include "HilDefs.h"

#ifdef __cplusplus
extern "C" {
#endif

//! @brief Verifies an HDMT HVDPS card is present.
//!
//! This function verifies an HDMT HVDPS card is present.  It connects to and caches driver resources for use
//! by other \c hvdpsXXXXX functions.  hvdpsDisconnect() is its complementary function and frees any cached resources.
//!
//! Neither hvdpsConnect() or hvdpsDisconnect() are required to be called to use the other \c hvdpsXXXXX functions.  All
//! \c hvdpsXXXXX functions will allocate resources if they are not already allocated and free them if required to
//! perform their function.
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hvdpsConnect(_In_ INT slot);

//! @brief Frees any resources cached from using the HVDPS card functions.
//!
//! This function frees any resources associated with using an HVDPS card's HIL functions. hvdpsConnect() is its
//! complementary function.
//!
//! Neither hvdpsConnect() or hvdpsDisconnect() are required to be called to use the other \c hvdpsXXXXX functions.  All
//! \c hvdpsXXXXX functions will allocate resources if they are not already allocated and free them if required to
//! perform their function.
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hvdpsDisconnect(_In_ INT slot);

//! @brief Programs the vendor and product IDs of all USB devices on an HVDPS card.
//!
//! This function programs the vendor and product IDs (VID/PIDs) of the FT240X and Cypress
//! USB devices on an HVDPS card.  Correct VID/PIDs are required to ensure Windows Device
//! Manager displays a proper description of the device, and HIL requires correct VID/PIDs to ensure
//! software can locate and communicate with the correct component.
//! @note FTDI devices have a limited number of updates, but this function is safe to call because it
//! only writes if the component programming is incorrect.
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hvdpsVidPidsSet(_In_ INT slot);

//! @brief Reads a register on an AD5764 of an HVDPS card.
//!
//! This function reads a register on an AD5764 of an HVDPS card.
//! @param[in] slot   A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] index  The index of the AD5764 to read. Consult the following table:
//!                   | Register | HVDPS Fab A Ref. |
//!                   | :------: | :---------------:|
//!                   |    0     |        U156      |
//!                   |    1     |        U157      |
//!                   |    2     |        U169      |
//! @param[in] reg    The register value (REG2,REG1,REG0) to be read.  See the data sheet for the AD5764.
//!                   Valid Register values (from datasheet) are:
//!                   | Register | Function             |
//!                   | :------: | :------------------: |
//!                   |    0     | Function register    |
//!                   |    2     | Data register        |
//!                   |    3     | Coarse Gain register |
//!                   |    4     | Fine Gain register   |
//!                   |    5     | Offset register      |
//! @param[in] dac    A valid selection of the DAC address bits (A2,A1,A0) for the AD5764.  See datasheet for specifics.
//!                   Valid DAC channel selections (from datasheet) are as follows.  Note that \c dac is used for another purpose when
//!                   used with the function register.
//!                   | Register | DAC      | Function register meaning |
//!                   | :------: | :------: | :-----------------------: |
//!                   |    0     | DAC A    | NOP, data = don't care.   |
//!                   |    1     | DAC B    | See data sheet.           |
//!                   |    2     | DAC C    |            -              |
//!                   |    3     | DAC D    |            -              |
//!                   |    4     | All DACs | Clear, data = don't care. |
//!                   |    5     |    -     | Load, data = don't care.  |
//! @param[out] pData The 16-bit data to read.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hvdpsAd5764Read(_In_ INT slot,  _In_ UINT index, _In_ UINT reg, _In_ UINT dac, _Out_ LPWORD pData);

//! @brief Writes a register on an AD5764 of an HVDPS card.
//!
//! This function writes a register on an AD5764 of an HVDPS card.
//! @param[in] slot  A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] index The index of the AD5764 to write. Consult the following table:
//!                  | Register | HVDPS Fab A Ref. |
//!                  | :------: | :---------------:|
//!                  |    0     |        U156      |
//!                  |    1     |        U157      |
//!                  |    2     |        U169      |
//! @param[in] reg   The register value (REG2,REG1,REG0) to be written.  See the data sheet for the AD5764.
//!                  Valid Register values (from datasheet) are:
//!                  | Register | Function             |
//!                  | :------: | :------------------: |
//!                  |    0     | Function register    |
//!                  |    2     | Data register        |
//!                  |    3     | Coarse Gain register |
//!                  |    4     | Fine Gain register   |
//!                  |    5     | Offset register      |
//! @param[in] dac   A valid selection of the DAC address bits (A2,A1,A0) for the AD5764.  See datasheet for specifics.
//!                  Valid DAC channel selections (from datasheet) are as follows.  Note that \c dac is used for another purpose when
//!                  used with the function register.
//!                  | Register | DAC      | Function register meaning |
//!                  | :------: | :------: | :-----------------------: |
//!                  |    0     | DAC A    | NOP, data = don't care.   |
//!                  |    1     | DAC B    | See data sheet.           |
//!                  |    2     | DAC C    |            -              |
//!                  |    3     | DAC D    |            -              |
//!                  |    4     | All DACs | Clear, data = don't care. |
//!                  |    5     |    -     | Load, data = don't care.  |
//! @param[in] data  The 16-bit data to write.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hvdpsAd5764Write(_In_ INT slot,  _In_ UINT index, _In_ UINT reg, _In_ UINT dac, _In_ WORD data);

//! @brief Reads a 16-bit register of an AD5560 device on an HVDPS card.
//!
//! This function reads a 16-bit register of an AD5560 device on an HVDPS card.
//! \par C Example
//! \code{.c}
//!  HIL_STATUS result;
//!  INT slot = 0;
//!  UINT rail = 0;
//!  BYTE address = 0;
//!  WORD data;
//!  result = hvdpsAd5560Read(slot, rail, address, &data);
//!  if(result != HS_SUCCESS)
//!      /* report failure */
//! \endcode
//! \par Python Example
//! \code{.py}
//! import hil
//! slot,rail,address = 0,0,0,0
//! data = hil.hvdpsAd5560Read(slot,rail,address) # output parameter returned, RuntimeError raised on failure.
//! \endcode
//! @param[in]  slot    A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in]  rail    The rail number of the AD5560 to control.  Valid values are 0-7 for an HVDPS.
//! @param[in]  address A valid 7-bit register address for the AD5560.
//! @param[out] pData   The 16-bit register contents.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hvdpsAd5560Read(_In_ INT slot,  _In_ UINT rail, _In_ BYTE address, _Out_ LPWORD pData);

//! @brief Writes a 16-bit register of an AD5560 device on an HVDPS card.
//!
//! This function writes a 16-bit register of an AD5560 device on an HVDPS card.
//! \par C Example
//! \code{.c}
//!  HIL_STATUS result;
//!  INT slot = 0;
//!  UINT rail = 0;
//!  BYTE address = 0;
//!  WORD data = 0x1234;
//!  result = hvdpsAd5560Write(slot, subslot, rail, address, data);
//!  if(result != HS_SUCCESS)
//!      /* report failure */
//! \endcode
//! \par Python Example
//! \code{.py}
//! import hil
//! slot,rail,address,data = 0,0,0,0,0x1234
//! hil.hvdpsAd5560Write(slot,subslot,rail,address,data) # RuntimeError raised on failure.
//! \endcode
//! @param[in] slot    A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in]  rail   The rail number of the AD5560 to control.  Valid values are 0-7.
//! @param[in] address A valid 7-bit register address for the AD5560.
//! @param[in] data    The 16-bit value to write to the register.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hvdpsAd5560Write(_In_ INT slot,  _In_ UINT rail, _In_ BYTE address, _In_ WORD data);

//! @brief Reads a rail voltage on an HVDPS card.
//!
//! This function reads a rail voltage on an HVDPS card.
//! @param[in] slot      A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in]  rail     The rail number to read.  Valid values are 0-7.
//! @param[out] pVoltage A pointer to double that receives the voltage in volts.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hvdpsRailVoltageRead(_In_ INT slot, _In_ UINT rail, _Out_ double* pVoltage);

//! @brief Reads a rail current on an HVDPS card.
//!
//! This function reads a rail current on an HVDPS card.
//! @param[in] slot      A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in]  rail     The rail number to read.  Valid values are 0-7.
//! @param[out] pCurrent A pointer to double that receives the current in amps.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hvdpsRailCurrentRead(_In_ INT slot, _In_ UINT rail, _Out_ double* pCurrent);

//! @brief Reads a turbo rail voltage on an HVDPS card.
//!
//! This function reads a turbo rail voltage on an HVDPS card.
//! @param[in]  slot     A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in]  rail     The rail number to read.  Valid values are 0-7.
//! @param[out] pVoltage A pointer to double that receives the turbovoltage in volts.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hvdpsTurboRailVoltageRead(_In_ INT slot, _In_ UINT rail, _Out_ double* pVoltage);

//! @brief Reads a turbo rail current on an HVDPS card.
//!
//! This function reads a turbo rail current on an HVDPS card.
//! @param[in]  slot     A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in]  rail     The rail number to read.  Valid values are 0-7.
//! @param[out] pCurrent A pointer to double that receives the turbo current in amps.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hvdpsTurboRailCurrentRead(_In_ INT slot, _In_ UINT rail, _Out_ double* pCurrent);

//! @brief Sets the upper and lower control limits for the MAX6627 thermal ADC devices on an HVDPS card.
//!
//! This function sets the upper and lower control limits for the MAX6627 thermal ADC devices on an HVDPS card.
//! @param[in]  slot  A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in]  lcl   Lower control limit of the MAX6627s. Valid values are -256 to 255 Celsius.
//! @param[in]  ucl   Upper control limit of the MAX6627s. Valid values are -256 to 255 Celsius.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hvdpsMax6627AdcLimitsSet(_In_ INT slot, _In_ double lcl, _In_ double ucl);

//! @brief Reads the upper and lower control limits for the MAX6627 thermal ADC devices on an HVDPS card.
//!
//! This function reads the upper and lower control limits for the MAX6627 thermal ADC devices on an HVDPS card.
//! @param[in]  slot  A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[out] pLcl  The returned lower control limit of the MAX6627s.
//! @param[out] pUcl  The returned upper control limit of the MAX6627s.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hvdpsMax6627AdcLimitsGet(_In_ INT slot, _Out_ double* pLcl, _Out_ double* pUcl);

//! @brief Sets the upper and lower control limits for the MAX6627 BJT sensor devices on an HVDPS card.
//!
//! This function sets the upper and lower control limits for the MAX6627 BJT sensor devices on an HVDPS card.
//! @param[in]  slot  A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in]  lcl   Lower control limit of the MAX6627s. Valid values are -256 to 255 Celsius.
//! @param[in]  ucl   Upper control limit of the MAX6627s. Valid values are -256 to 255 Celsius.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hvdpsMax6627BjtSensorLimitsSet(_In_ INT slot, _In_ double lcl, _In_ double ucl);

//! @brief Reads the upper and lower control limits for the MAX6627 BJT Sensor devices on an HVDPS card.
//!
//! This function gets the upper and lower control limits for the MAX6627 BJT Sensor devices on an HVDPS card.
//! @param[in]  slot  A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[out] pLcl  The returned lower control limit of the MAX6627s.
//! @param[out] pUcl  The returned upper control limit of the MAX6627s.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hvdpsMax6627BjtSensorLimitsGet(_In_ INT slot, _Out_ double* pLcl, _Out_ double* pUcl);

//! @brief Sets the upper and lower control limits for the MAX6627 VICOR devices on an HVDPS card.
//!
//! This function sets the upper and lower control limits for the MAX6627 VICOR devices on an HVDPS card.
//! @param[in]  slot  A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in]  lcl   Lower control limit of the MAX6627s. Valid values are -256 to 255 Celsius.
//! @param[in]  ucl   Upper control limit of the MAX6627s. Valid values are -256 to 255 Celsius.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hvdpsMax6627VicorLimitsSet(_In_ INT slot, _In_ double lcl, _In_ double ucl);

//! @brief Reads the upper and lower control limits for the MAX6627 VICOR devices on an HVDPS card.
//!
//! This function gets the upper and lower control limits for the MAX6627 VICOR devices on an HVDPS card.
//! @param[in]  slot  A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[out] pLcl  The returned lower control limit of the MAX6627s.
//! @param[out] pUcl  The returned upper control limit of the MAX6627s.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hvdpsMax6627VicorLimitsGet(_In_ INT slot, _Out_ double* pLcl, _Out_ double* pUcl);

//! @brief Reads the temperature of selected MAX6627 thermal ADC rail on an HVDPS card.
//!
//! This function returns the temperature of the selected MAX6627 thermal ADC rail on an HVDPS card.
//! @param[in]  slot   A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in]  index  Selects the the MAX6627 thermal ADC rail. Valid values 0-7.
//! @param[out] pTemp  The returned temperature in degrees Celsius.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hvdpsMax6627ThermalAdcTmonRead(_In_ INT slot, _In_ UINT index, _Out_ double* pTemp);

//! @brief Reads the temperature of selected MAX6627 BJT sensor rail on an HVDPS card.
//!
//! This function returns the temperature of the selected MAX6627 BJT sensor rail on an HVDPS card.
//! @param[in]  slot   A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in]  index  Selects the the MAX6627 BJT sensor rail. Valid values 0-7.
//! @param[out] pTemp  The returned temperature in degrees Celsius.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hvdpsMax6627BjtSensorTmonRead(_In_ INT slot, _In_ UINT index, _Out_ double* pTemp);

//! @brief Reads the temperature of selected MAX6627 VICOR rail on an HVDPS card.
//!
//! This function returns the temperature of the selected MAX6627 VICOR rail on an HVDPS card.
//! @param[in]  slot   A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in]  index  Selects the the MAX6627 VICOR rail. Valid values 0-3.
//! @param[out] pTemp  The returned temperature in degrees Celsius.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hvdpsMax6627VicorTmonRead(_In_ INT slot, _In_ UINT index, _Out_ double* pTemp);

//! @brief Reads the voltage from the LTC2450 ADC on an HVDPS card.
//!
//! This function reads the voltage from the LTC2450 ADC on an HVDPS card.
//! @param[in]  slot     A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[out] pVoltage The returned voltage in Volts.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hvdpsLtc2450AdcVoltageRead(_In_ INT slot, _Out_ double* pVoltage);

//! @brief Reads the internal temperature of the LTM4620 (-5V regulator) on an HVDPS card.
//!
//! This function reads the internal temperature of the LTM4620 (-5V regulator) on an HVDPS card.
//! @param[in]  slot   A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[out] pTemp  The returned temperature in degrees Celsius.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hvdpsLtm4620TmonRead(_In_ INT slot, _Out_ double* pTemp);

//! @brief Enables the 48V to 36V converter on an HVDPS card.
//!
//! This function enables the 48V to 36V converter to power the LTM4653 on an HVDPS card.
//! @param[in] slot    A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] enable  Enables the 48V to 36V converter.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hvdpsLtm4653VoltageConverterEnable(_In_ INT slot, _In_ BOOL enable);

//! @brief Reads a channel voltage on an HVDPS card.
//!
//! This function reads a channel voltage on an HVDPS card.
//! @param[in] slot      A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in]  rail     The channel number to read.  Valid values are 0-9.
//! @param[out] pVoltage A pointer to double that receives the voltage in volts.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hvdpsDvmRailVoltageRead(_In_ INT slot, _In_ UINT rail, _Out_ double* pVoltage);

//! @brief Reads an HV rail voltage on an HVDPS card.
//!
//! This function reads a HV rail voltage on an HVDPS card.
//! @param[in] slot      A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in]  rail     The HV rail number to read.  Valid values are 0-7.
//! @param[out] pVoltage A pointer to double that receives the voltage in volts.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hvdpsHvLtmRailVoltageRead(_In_ INT slot, _In_ UINT rail, _Out_ double* pVoltage);

//! @brief Reads a turbo DUT voltage on an HVDPS card.
//!
//! This function reads a turbo DUT voltage on an HVDPS card.
//! @param[in]  slot     A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in]  rail     The channel number to read.  Valid values are 0-9.
//! @param[out] pVoltage A pointer to double that receives the turbo DUT voltage in volts.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hvdpsDvmTurboRailVoltageRead(_In_ INT slot, _In_ UINT rail, _Out_ double* pVoltage);

#ifdef __cplusplus
}
#endif
