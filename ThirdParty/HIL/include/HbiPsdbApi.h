// INTEL CONFIDENTIAL
// Copyright 2018 Intel Corporation All Rights Reserved.
//
// The source code contained or described herein and all documents related to
// the source code ("Material") are owned by Intel Corporation or its
// suppliers or licensors. Title to the Material remains with Intel
// Corporation or its suppliers and licensors. The Material contains trade
// secrets and proprietary and confidential information of Intel or its
// suppliers and licensors. The Material is protected by worldwide copyright
// and trade secret laws and treaty provisions. No part of the Material may be
// used, copied, reproduced, modified, published, uploaded, posted,
// transmitted, distributed, or disclosed in any way without Intel's prior
// express written permission.
//
// No license under any patent, copyright, trade secret or other intellectual
// property right is granted to or conferred upon you by disclosure or
// delivery of the Materials, either expressly, by implication, inducement,
// estoppel or otherwise. Any license under such intellectual property rights
// must be express and approved by Intel in writing.

//! @file
//! @brief APIs to control HBI Power and Signal Distribution Board (PSDB).

#pragma once
#include "HilDefs.h"

#ifdef __cplusplus
extern "C" {
#endif

//! @brief Initializes the components of an HBI PSDB for operation.
//!
//! This function initializes the components of an HBI Power and Signal Distribution Board for operation.
//! Calling this function is required before using most other \c hbiPsdbXXXXXX() functions.
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HBI_PSDB_SLOTS - 1 on HBI or 0 to \ref HIL_SLOTS - 1 on HDMT using a break-out board (BOB).
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiPsdbInit(_In_ INT slot);

//! @brief Verifies an HBI PSDB is present.
//!
//! This function verifies an HBI Power and Signal Distribution Board is present.  It connects to and caches driver resources for use
//! by other \c hbiPsdbXXXXX functions. hbiPsdbDisconnect() is its complementary function and frees any cached resources.
//!
//! Neither hbiPsdbXXXXX() or hbiPsdbDisconnect() are required to be called to use the other \c hbiPsdbXXXXX functions. All
//! \c hbiPsdbXXXXX functions will allocate resources if they are not already allocated and free them if required to
//! perform their function.
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HBI_PSDB_SLOTS - 1 on HBI or 0 to \ref HIL_SLOTS - 1 on HDMT using a break-out board (BOB).
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiPsdbConnect(_In_ INT slot);

//! @brief Frees any resources cached from using the HBI PSDB functions.
//!
//! This function frees any resources associated with using the HBI Power and Signal Distribution Board HIL functions. hbiPsdbConnect()
//! is its complementary function.
//!
//! Neither hbiPsdbXXXXX() or hbiPsdbDisconnect() are required to be called to use the other \c hbiPsdbXXXXX functions. All
//! \c hbiPsdbXXXXX functions will allocate resources if they are not already allocated and free them if required to
//! perform their function.
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HBI_PSDB_SLOTS - 1 on HBI or 0 to \ref HIL_SLOTS - 1 on HDMT using a break-out board (BOB).
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiPsdbDisconnect(_In_ INT slot);

//! @brief Programs the vendor and product IDs of all USB devices on an HBI PSDB.
//!
//! This function programs the vendor and product IDs (VID/PIDs) of the FT240X on an HBI Power and Signal Distribution Board.
//! Correct VID/PIDs are required to ensure Windows Device Manager displays a proper description of the device,
//! and HIL requires correct VID/PIDs to ensure software can locate and communicate with the correct component.
//! @note FTDI devices have a limited number of updates, but this function is safe to call because it
//! only writes if the component programming is incorrect.
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HBI_PSDB_SLOTS - 1 on HBI or 0 to \ref HIL_SLOTS - 1 on HDMT using a break-out board (BOB).
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiPsdbVidPidsSet(_In_ INT slot);

//! @brief Reads the version number of a CPLD on an HBI PSDB.
//!
//! This function reads the version number of a CPLD on an HBI Power and Signal Distribution Board.
//!
//! @param[in] slot      A physical slot number.  Valid values are 0 to \ref HBI_PSDB_SLOTS - 1 on HBI or 0 to \ref HIL_SLOTS - 1 on HDMT using a break-out board (BOB).
//! @param[in] cpldIndex Selects one of the two CPLDs on a PSDB.  Consult the following table. Note that reference designators may change in future fabs.
//!                      |  cpldIndex  |  Reference designator  |                  Comment                  |
//!                      | :---------: | :--------------------: | :---------------------------------------: |
//!                      |      0      |      U53 (MAX II)      |  Pin Multiplier FPGA0 Configuration CPLD  |
//!                      |      1      |      U6  (MAX II)      |  Pin Multiplier FPGA1 Configuration CPLD  |
//! @param[out] pVersion Returns the version register value.  The pointer must not be NULL.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiPsdbCpldVersion(_In_ INT slot, _In_ UINT cpldIndex, _Out_ LPDWORD pVersion);

//! @brief Reads the version string of a CPLDs on an HBI PSDB.
//!
//! This function reads the version register of a CPLDs on an HBI Power and Signal Distribution Board and converts it to an ASCII string representation.
//!
//! @param[in] slot      A physical slot number.  Valid values are 0 to \ref HBI_PSDB_SLOTS - 1 on HBI or 0 to \ref HIL_SLOTS - 1 on HDMT using a break-out board (BOB).
//! @param[in] cpldIndex Selects one of the two CPLDs on a PSDB.  Consult the following table. Note that reference designators may change in future fabs.
//!                      |  cpldIndex  |  Reference designator  |                  Comment                  |
//!                      | :---------: | :--------------------: | :---------------------------------------: |
//!                      |      0      |      U53 (MAX II)      |  Pin Multiplier FPGA0 Configuration CPLD  |
//!                      |      1      |      U6  (MAX II)      |  Pin Multiplier FPGA1 Configuration CPLD  |
//! @param[out] pVersion Returns the version string.  The pointer must not be NULL.
//! @param[in] length The length of the \c pVersion buffer.  It should be at least five characters to hold a null-terminated version string.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiPsdbCpldVersionString(_In_ INT slot, _In_ UINT cpldIndex, _Out_writes_z_(length) LPSTR pVersion, _In_ DWORD length);

//! @brief Programs a CPLD .JBC file into a CPLD on an HBI PSDB.
//!
//! This function programs a CPLD .JBC file into a CPLD on an HBI Power and Signal Distribution Board.
//! @param[in] slot      A physical slot number.  Valid values are 0 to \ref HBI_PSDB_SLOTS - 1 on HBI or 0 to \ref HIL_SLOTS - 1 on HDMT using a break-out board (BOB).
//! @param[in] cpldIndex Selects one of the two CPLDs on a PSDB.  Consult the following table. Note that reference designators may change in future fabs.
//!                      |  cpldIndex  |  Reference designator  |                  Comment                  |
//!                      | :---------: | :--------------------: | :---------------------------------------: |
//!                      |      0      |      U53 (MAX II)      |  Pin Multiplier FPGA0 Configuration CPLD  |
//!                      |      1      |      U6  (MAX II)      |  Pin Multiplier FPGA1 Configuration CPLD  |
//! @param[in] filename An ANSI string containing an absolute or relative (to the current directory) CPLD .JBC filename.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiPsdbCpldProgram(_In_ INT slot, _In_ UINT cpldIndex, _In_z_ LPCSTR filename);

//! @brief Programs a buffer containing .JBC file content into a CPLD on an HBI PSDB.
//!
//! This function programs a buffer containing .JBC file content into a CPLD on an HBI Power and Signal Distribution Board (PSDB).
//! @param[in] slot      A physical slot number.  Valid values are 0 to \ref HBI_PSDB_SLOTS - 1 on HBI or 0 to \ref HIL_SLOTS - 1 on HDMT using a break-out board (BOB).
//! @param[in] cpldIndex Selects one of the two CPLDs on a PSDB.  Consult the following table. Note that reference designators may change in future fabs.
//!                      |  cpldIndex  |  Reference designator  |                  Comment                  |
//!                      | :---------: | :--------------------: | :---------------------------------------: |
//!                      |      0      |       U53 (MAX II)     |  Pin Multiplier FPGA0 Configuration CPLD  |
//!                      |      1      |       U6  (MAX II)     |  Pin Multiplier FPGA1 Configuration CPLD  |
//! @param[in] pData The CPLD .JBC image data.
//! @param[in] length The length of the CPLD .JBC image data in bytes.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiPsdbCpldBufferProgram(_In_ INT slot, _In_ UINT cpldIndex, _In_reads_bytes_(length) LPCVOID pData, _In_ DWORD length);

//! @brief Compares an existing image within a CPLD on an HBI PSDB against a CPLD .JBC file.
//!
//! This function compares an existing image within a CPLD on an HBI PSDB against a CPLD .JBC file.
//! @param[in] slot      A physical slot number.  Valid values are 0 to \ref HBI_PSDB_SLOTS - 1 on HBI or 0 to \ref HIL_SLOTS - 1 on HDMT using a break-out board (BOB).
//! @param[in] cpldIndex Selects one of the two CPLDs on a PSDB.  Consult the following table. Note that reference designators may change in future fabs.
//!                      |  cpldIndex  |  Reference designator  |                  Comment                  |
//!                      | :---------: | :--------------------: | :---------------------------------------: |
//!                      |      0      |       U53 (MAX II)     |  Pin Multiplier FPGA0 Configuration CPLD  |
//!                      |      1      |       U6  (MAX II)     |  Pin Multiplier FPGA1 Configuration CPLD  |
//! @param[in] imageFilename An ANSI string containing an absolute or relative (to the current directory) CPLD .JBC filename to be compared against.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiPsdbCpldVerify(_In_ INT slot, _In_ UINT cpldIndex, _In_z_ LPCSTR imageFilename);

//! @brief Compares an existing image within ta CPLD on an HBI PSDB against a buffer containing .JBC file content.
//!
//! This function compares an existing image within ta CPLD on an HBI PSDB against a buffer containing .JBC file content.
//! @param[in] slot      A physical slot number.  Valid values are 0 to \ref HBI_PSDB_SLOTS - 1 on HBI or 0 to \ref HIL_SLOTS - 1 on HDMT using a break-out board (BOB).
//! @param[in] cpldIndex Selects one of the two CPLDs on a PSDB.  Consult the following table. Note that reference designators may change in future fabs.
//!                      |  cpldIndex  |  Reference designator  |                  Comment                  |
//!                      | :---------: | :--------------------: | :---------------------------------------: |
//!                      |      0      |     U53 (MAX II)       |  Pin Multiplier FPGA0 Configuration CPLD  |
//!                      |      1      |     U6  (MAX II)       |  Pin Multiplier FPGA1 Configuration CPLD  |
//! @param[in] pData The CPLD .JBC image data to be compared against.
//! @param[in] length The length of the CPLD .JBC image data in bytes.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiPsdbCpldBufferVerify(_In_ INT slot, _In_ UINT cpldIndex, _In_reads_bytes_(length) LPCVOID pData, _In_ DWORD length);

//! @brief Reads the version number of a Pin Multiplier FPGA on an HBI PSDB.
//!
//! This function reads the version number of a Pin Multiplier FPGA on an HBI Power and Signal Distribution Board.  The returned 32-bit value is in the
//! format 0xMMMMmmmm where the upper 16-bits are the major version and lower 16-bits are the minor version.
//!
//! For example 0x00050001 is version 5.1.
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HBI_PSDB_SLOTS - 1 on HBI or 0 to \ref HIL_SLOTS - 1 on HDMT using a break-out board (BOB).
//! @param[in] fpgaIndex Selects one of the two Pin Multiplier FPGAs: 0 or 1.
//! @param[out] pVersion The address of a DWORD of memory.  It may not be NULL.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiPsdbFpgaVersion(_In_ INT slot, _In_ UINT fpgaIndex, _Out_ LPDWORD pVersion);

//! @brief Reads version string of a Pin Multiplier FPGA on an HBI PSDB.
//!
//! This function reads the version register of a Pin Multiplier FPGA on an HBI Power and Signal Distribution Board and converts it to an ASCII string representation.
//! As of HIL 7.0, it also appends the compatibility register value.  The format is \c "MM.mm-C" where \c MM is the major version, \c mm is the minor version,
//! and \c C is the compatibility value.  All values are unsigned decimal numbers.
//!
//! @param[in] slot A physical slot number.  Valid values are 0 to \ref HBI_PSDB_SLOTS - 1 on HBI or 0 to \ref HIL_SLOTS - 1 on HDMT using a break-out board (BOB).
//! @param[in] fpgaIndex Selects one of the two Pin Multiplier FPGAs: 0 or 1.
//! @param[out] pVersion Returns the version string.  The pointer must not be NULL.
//! @param[in] length The length of the \c pVersion buffer.  It should be at least #HIL_MAX_VERSION_STRING_SIZE characters to hold a null-terminated version string.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiPsdbFpgaVersionString(_In_ INT slot, _In_ UINT fpgaIndex, _Out_writes_z_(length) LPSTR pVersion, _In_ DWORD length);

//! @brief Returns a Pin Multiplier FPGA's base FLASH content information.
//!
//! This function returns a Pin Multiplier FPGA's base FLASH content information.
//! @param[in] slot A physical slot number.  Valid values are 0 to \ref HBI_PSDB_SLOTS - 1 on HBI or 0 to \ref HIL_SLOTS - 1 on HDMT using a break-out board (BOB).
//! @param[in] fpgaIndex Selects one of the two Pin Multiplier FPGAs: 0 or 1.
//! @param[out] pInfo Returns FLASH_INFO structure describing the FLASH contents.  The pointer must not be NULL.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiPsdbFpgaBaseInfo(_In_ INT slot, _In_ UINT fpgaIndex, _Out_ PFLASH_INFO pInfo);

//! @brief Returns a Pin Multiplier FPGA's compatibility register value.
//!
//! This function returns a Pin Multiplier FPGA's compatibility register value.
//! @param[in] slot A physical slot number.  Valid values are 0 to \ref HBI_PSDB_SLOTS - 1 on HBI or 0 to \ref HIL_SLOTS - 1 on HDMT using a break-out board (BOB).
//! @param[in] fpgaIndex Selects one of the two Pin Multiplier FPGAs: 0 or 1.
//! @param[out] pData Returns the value of the FPGA's compatibility register.  The pointer must not be NULL.  At a minimum, bits 0-3 of the returned value
//!                   indicates hardware capability.  An FPGA that reports a particular value in bits 0-3 should only be overwritten with a compatible image.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiPsdbFpgaCompatibility(_In_ INT slot, _In_ UINT fpgaIndex, _Out_ LPDWORD pData);

//! @brief Returns the source hash used to identify the source code that build the indicated FPGA.
//!
//! This function returns the source hash used to identify the source code that build the indicated FPGA.  The format of this field
//! is a 48-bit hexadecimal value with an optional trailing plus(+) indicating it was built from uncommitted sources.
//! @param[in] slot A physical slot number.  Valid values are 0 to \ref HBI_PSDB_SLOTS - 1 on HBI or 0 to \ref HIL_SLOTS - 1 on HDMT using a break-out board (BOB).
//! @param[in] fpgaIndex Selects one of the two Pin Multiplier FPGAs: 0 or 1.
//! @param[out] pHash  Returns a string representation of the FPGA's source control hash. The pointer must not be NULL.
//! @param[in] length The length of the \c pHash buffer.  It should be at least #HIL_MAX_VERSION_STRING_SIZE characters to hold a null-terminated version string.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiPsdbFpgaSourceHash(_In_ INT slot, _In_ UINT fpgaIndex, _Out_writes_z_(length) LPSTR pHash, _In_ DWORD length);

//! @brief Returns the 64-bit value of the indicated FPGA's chip ID registers.
//!
//! This function returns the 64-bit value of the indicated FPGA's chip ID registers that uniquely identifies the FPGA.
//! @param[in] slot A physical slot number.  Valid values are 0 to \ref HBI_PSDB_SLOTS - 1 on HBI or 0 to \ref HIL_SLOTS - 1 on HDMT using a break-out board (BOB).
//! @param[in] fpgaIndex Selects one of the two Pin Multiplier FPGAs: 0 or 1.
//! @param[out] pChipId Returns the 64-bit chip ID.  The pointer must not be NULL.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiPsdbFpgaChipId(_In_ INT slot, _In_ UINT fpgaIndex, _Out_ PDWORD64 pChipId);

//! @brief Disables the PCI device driver for a Pin Multiplier FPGA on an HBI PSDB.
//!
//! This function disables the PCI device driver for a Pin Multiplier FPGA on an HBI Power and Signal Distribution Board.
//! It should be called before using functions such as hbiPsdbFpgaLoad() that affect the hardware used by the driver.
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HBI_PSDB_SLOTS - 1 on HBI or 0 to \ref HIL_SLOTS - 1 on HDMT using a break-out board (BOB).
//! @param[in] fpgaIndex Selects one of the two Pin Multiplier FPGAs: 0 or 1.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiPsdbDeviceDisable(_In_ INT slot, _In_ UINT fpgaIndex);

//! @brief Enables the PCI device driver for a Pin Multiplier FPGA on an HBI PSDB.
//!
//! This function enables the PCI device driver for a Pin Multiplier FPGA on an HBI Power and Signal Distribution Board.
//! It should be called after using functions such as hbiPsdbFpgaLoad() that affect the hardware used by the driver.
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HBI_PSDB_SLOTS - 1 on HBI or 0 to \ref HIL_SLOTS - 1 on HDMT using a break-out board (BOB).
//! @param[in] fpgaIndex Selects one of the two Pin Multiplier FPGAs: 0 or 1.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiPsdbDeviceEnable(_In_ INT slot, _In_ UINT fpgaIndex);

//! @brief Loads an FPGA binary image file into a Pin Multiplier FPGA on an HBI PSDB.
//!
//! This function loads an FPGA binary image file into a Pin Multiplier FPGA on an HBI PSDB.
//! This image is volatile and the FPGA will load its original base program from its SPI FLASH after a cold reboot.
//! @warning HBI PSDB device drivers are sensitive to FPGA changes.  If they are present, wrap this call in hbiPsdbDeviceDisable() and hbiPsdbDeviceEnable().
//!
//! @warning On HBI PSDB Fab C, this function requires that the VCCIO rail corrosponding to the specified
//! Pin Multiplier FPGA (PM0: VCCIO3_0_6A, PM1: VCCIO3_1_6A) is set to 1.8V.  This is the power-on state of these IOs and changing it correctly is not time
//! efficient, so users should use this API before any attempt at altering the VCCIOs.
//!
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HBI_PSDB_SLOTS - 1 on HBI or 0 to \ref HIL_SLOTS - 1 on HDMT using a break-out board (BOB).
//! @param[in] fpgaIndex Selects one of the two Pin Multiplier FPGAs: 0 or 1.
//! @param[in] filename An ANSI string containing an absolute or relative (to the current directory) FPGA binary filename.  As of HIL 7.0,
//!                     there must also be a <code><i>\<filename\></i>.mta</code> metadata file in the same directory as <code><i>\<filename\></i></code>.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiPsdbFpgaLoad(_In_ INT slot, _In_ UINT fpgaIndex, _In_z_ LPCSTR filename);

//! @brief Loads an FPGA image file into the selected base SPI FLASH used to boot initialize a Pin Multiplier FPGA on an HBI PSDB.
//!
//! This function loads an FPGA image file into the selected base SPI FLASH used to boot initialize a Pin Multiplier FPGA on an HBI Power and Signal Distribution Board.
//!
//! The callback and context parameters are used to report progress back during a long operation.  See #HIL_BASE_LOAD_CALLBACK
//! for details.
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HBI_PSDB_SLOTS - 1 on HBI or 0 to \ref HIL_SLOTS - 1 on HDMT using a break-out board (BOB).
//! @param[in] fpgaIndex Selects the appropriate SPI FLASH for one of the two Pin Multiplier FPGAs: 0 or 1.
//! @param[in] filename An ANSI string containing an absolute or relative (to the current directory) FPGA binary filename.  As of HIL 7.0,
//!                     there must also be a <code><i>\<filename\></i>.mta</code> metadata file in the same directory as <code><i>\<filename\></i></code>.
//! @param[in] callback The callback function to be called with progress updates while programming the SPI FLASH.  Value can be NULL.
//! @param[in] context The context that will be passed back as an argument in the #HIL_BASE_LOAD_CALLBACK callback function.  Value can be NULL.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiPsdbFpgaBaseLoad(_In_ INT slot, _In_ UINT fpgaIndex, _In_z_ LPCSTR filename, _In_opt_ HIL_BASE_LOAD_CALLBACK callback, _In_opt_ void* context);

//! @brief Validates the selected base SPI FLASH contents on an HBI PSDB.
//!
//! This function validates the selected base SPI FLASH contents on an HBI Power and Signal Distribution Board and optionally dumps the contents to a file.
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HBI_PSDB_SLOTS - 1 on HBI or 0 to \ref HIL_SLOTS - 1 on HDMT using a break-out board (BOB).
//! @param[in] fpgaIndex Selects the appropriate SPI FLASH for one of the two Pin Multiplier FPGAs: 0 or 1.
//! @param[in] dumpFilename An ANSI string containing an absolute or relative (to the current directory) binary dump filename.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiPsdbFpgaBaseVerify(_In_ INT slot, _In_ UINT fpgaIndex, _In_opt_z_ LPCSTR dumpFilename);

//! @brief Loads an FPGA binary image buffer into a Pin Multiplier FPGA on an HBI PSDB.
//!
//! This function loads an FPGA binary image buffer into a Pin Multiplier FPGA on an HBI Power and Signal Distribution Board.  This image is volatile and the FPGA
//! will load its original base program from its SPI FLASH after a cold reboot.
//! @warning HBI PSDB device drivers are sensitive to FPGA changes.  If they are present, wrap this call in hbiPsdbDeviceDisable() and hbiPsdbDeviceEnable().
//!
//! @warning On HBI PSDB Fab C, this function requires that the VCCIO rail corrosponding to the specified
//! Pin Multiplier FPGA (PM0: VCCIO3_0_6A, PM1: VCCIO3_1_6A) is set to 1.8V.  This is the power-on state of these IOs and changing it correctly is not time
//! efficient, so users should use this API before any attempt at altering the VCCIOs.
//!
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HBI_PSDB_SLOTS - 1 on HBI or 0 to \ref HIL_SLOTS - 1 on HDMT using a break-out board (BOB).
//! @param[in] fpgaIndex Selects one of the two Pin Multiplier FPGAs: 0 or 1.
//! @param[in] pData The FPGA image data.
//! @param[in] length The length of the FPGA image data in bytes.
//! @param[in] pMetaData  The FPGA image metadata.  This data is in a side-by-side file with .MTA extension with the original FPGA image.  It is
//!                       loaded automatically by hbiMbFpgaLoad() but needs to be read and provided to this version of the function.
//! @param[in] metaLength The length of the FPGA image metadata in bytes.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiPsdbFpgaBufferLoad(_In_ INT slot, _In_ UINT fpgaIndex, _In_reads_bytes_(length) LPCVOID pData, _In_ DWORD length, _In_reads_bytes_opt_(metaLength) LPCVOID pMetaData, _In_ DWORD metaLength);

//! @brief Loads an FPGA image buffer into the selected base SPI FLASH used to boot initialize a Pin Multiplier FPGA on an HBI PSDB.
//!
//! This function loads an FPGA image buffer into the selected base SPI FLASH used to boot initialize a Pin Multiplier FPGA on an HBI Power and Signal Distribution Board.
//!
//! The callback and context parameters are used to report progress back during a long operation.  See #HIL_BASE_LOAD_CALLBACK
//! for details.
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HBI_PSDB_SLOTS - 1 on HBI or 0 to \ref HIL_SLOTS - 1 on HDMT using a break-out board (BOB).
//! @param[in] fpgaIndex Selects the appropriate SPI FLASH for one of the two Pin Multiplier FPGAs: 0 or 1.
//! @param[in] pData The FPGA image data.
//! @param[in] length The length of the FPGA image data in bytes.
//! @param[in] pMetaData  The FPGA image metadata.  This data is in a side-by-side file with .MTA extension with the original FPGA image.  It is
//!                       loaded automatically by hbiMbFpgaBaseLoad() but needs to be read and provided to this version of the function.
//! @param[in] metaLength The length of the FPGA image metadata in bytes.
//! @param[in] callback The callback function to be called with progress updates while programming the SPI FLASH.  Value can be NULL.
//! @param[in] context The context that will be passed back as an argument in the #HIL_BASE_LOAD_CALLBACK callback function.  Value can be NULL.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiPsdbFpgaBaseBufferLoad(_In_ INT slot, _In_ UINT fpgaIndex, _In_reads_bytes_(length) LPCVOID pData, _In_ DWORD length, _In_reads_bytes_opt_(metaLength) LPCVOID pMetaData, _In_ DWORD metaLength, _In_opt_ HIL_BASE_LOAD_CALLBACK callback, _In_opt_ void* context);

//! @brief Reads a 32-bit register in one of the BAR regions of a Pin Multiplier FPGA on an HBI PSDB.
//!
//! This function reads a 32-bit register in one of the BAR regions of a Pin Multiplier FPGA on an HBI Power and Signal Distribution Board.
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HBI_PSDB_SLOTS - 1 on HBI or 0 to \ref HIL_SLOTS - 1 on HDMT using a break-out board (BOB).
//! @param[in] fpgaIndex Selects one of the two Pin Multiplier FPGAs: 0 or 1.
//! @param[in] bar The PCI base address register region to access.  Valid values are 0 and 1.
//! @param[in] offset The DWORD-aligned offset address in BAR0 memory to read.
//! @param[out] pData The address of a DWORD to contain the result.  It cannot be NULL.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiPsdbBarRead(_In_ INT slot, _In_ UINT fpgaIndex, _In_ UINT bar, _In_ DWORD offset, _Out_ LPDWORD pData);

//! @brief Writes a 32-bit register in one of the BAR regions of a Pin Multiplier FPGA on an HBI PSDB.
//!
//! This function writes a 32-bit register in one the BAR regions of a Pin Multiplier FPGA on an HBI Power and Signal Distribution Board.
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HBI_PSDB_SLOTS - 1 on HBI or 0 to \ref HIL_SLOTS - 1 on HDMT using a break-out board (BOB).
//! @param[in] fpgaIndex Selects one of the two Pin Multiplier FPGAs: 0 or 1.
//! @param[in] bar The PCI base address register region to access.  Valid values are 0 and 1.
//! @param[in] offset The DWORD-aligned offset address in BAR0 memory to write.
//! @param[in] data The DWORD value to write.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiPsdbBarWrite(_In_ INT slot, _In_ UINT fpgaIndex, _In_ UINT bar, _In_ DWORD offset, _In_ DWORD data);

//! @brief Reads the board-level traceability values from an HBI PSDB.
//!
//! This function reads the board-level traceability values from an HBI Power and Signal Distribution Board.
//!
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HBI_PSDB_SLOTS - 1 on HBI or 0 to \ref HIL_SLOTS - 1 on HDMT using a break-out board (BOB).
//! @param[out] pBlt The address of a #BLT structure that will be populated with the data.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiPsdbBltBoardRead(_In_ INT slot, _Out_ PBLT pBlt);

//! @brief Writes the board-level traceability values of an HBI PSDB.
//!
//! This function writes the board-level traceability values of an HBI Power and Signal Distribution Board.
//!
//! The \c flags field allows this function to update individual values.  The flags can be ORed together.  Only the fields indicated by
//! \c flags need to be populated in the provided \c pBlt parameter.
//!
//! \par Updating the Full BLT - C
//! \code{.c}
//! #include <Windows.h>
//! #include "HbiPsdbApi.h"
//! #include "string.h"
//!
//! int main()
//! {
//!     int slot = 0;
//!     BLT blt;
//!     HIL_STATUS status;
//!     blt.Id = 0x100000FF; /* NOT the actual card ID */
//!     strcpy(blt.DeviceName,"DeviceName");
//!     strcpy(blt.VendorName,"INTEL");
//!     strcpy(blt.PartNumberAsBuilt,"AAxxxxxx-xxx");
//!     strcpy(blt.PartNumberCurrent,blt.PartNumberAsBuilt);
//!     strcpy(blt.SerialNumber,"12345678");
//!     strcpy(blt.ManufactureDate,"YYMMDD");
//!     status = hbiPsdbBltBoardWrite(slot, &blt, BF_ALL);
//!     if (status != HS_SUCCESS)
//!         return -1;
//!     return 0;
//! }
//! \endcode
//! \par Updating the Current Part Number - Python
//! \code{.py}
//! slot = 0
//! blt = hil.BLT()
//! blt.PartNumberCurrent = 'AAyyyyyy-yyy'
//! hil.hbiPsdbBltBoardWrite(slot,blt,hil.BF_PART_NUMBER_CURRENT)
//! # Note Python returns None for HIL_STATUS if successful and raises RuntimeError on failure.
//! \endcode
//!
//! \note HS_BLT_NOT_FOUND can be returned on an update (lack of #BF_ALL flag).
//!
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HBI_PSDB_SLOTS - 1 on HBI or 0 to \ref HIL_SLOTS - 1 on HDMT using a break-out board (BOB).
//! @param[in] pBlt The address of a #BLT structure that contains the data to be written or updated.
//! @param[in] flags #BLT_FLAGS bits indicating what fields in \c pBlt are valid.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiPsdbBltBoardWrite(_In_ INT slot, _In_ PCBLT pBlt, _In_ BLT_FLAGS flags);

//! @brief Reads raw data from an HBI PSDB's BLT EEPROM.
//!
//! This function reads raw data from an HBI Power and Signal Distribution Board's BLT EEPROM.
//!
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HBI_PSDB_SLOTS - 1 on HBI or 0 to \ref HIL_SLOTS - 1 on HDMT using a break-out board (BOB).
//! @param[out] pEepromData The output buffer.
//! @param[in,out] pLength On input, the size of the output buffer referenced by \c pData.  On output, the size of the EEPROM data.
//! @retval HS_INSUFFICIENT_BUFFER The buffer provided was too small.  \c pLength will indicate the minimum buffer size required.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiPsdbBltEepromRead(_In_ INT slot, _Out_writes_bytes_to_(_Old_(*pLength), *pLength) LPVOID pEepromData, _Inout_ LPDWORD pLength);

//! @brief Reports whether or not each of the four TIUs are connected to an HBI PSDB.
//!
//! This function reports whether or not each of the four TIUs are connected to an HBI Power and Signal Distribution Board.
//! The returned 8-bit value is in the format 0b0000SSSS where the upper 4-bits are to be ignored (always 0) and lower 4-bits
//! indicate presence(1) or absence(0) of a TIU's electrical connection with an HBI PSDB in the following order TIU1-1 : TIU1-0 : TIU0-1: TIU0-0.
//!
//! For example 0b00001111 indicates that all four TIUs are connected to the HBI PSDB.
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HBI_PSDB_SLOTS - 1 on HBI or 0 to \ref HIL_SLOTS - 1 on HDMT using a break-out board (BOB).
//! @param[out] pStatus The TIU present status byte.  It may not be NULL.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiPsdbTiuPresentStatus(_In_ INT slot, _Out_ PBYTE pStatus);

//! @brief Reports whether or not each of the eight DPSs are connected to an HBI PSDB.
//!
//! This function reports whether or not each of the eight DPSs are connected to an HBI Power and Signal Distribution Board.
//! Each bit in the returned 8-bit value indicates presence(1) or absence(0) of a DPS's electrical connection with an HBI PSDB in the
//! following order DPS7 : DPS0.
//!
//! For example 0b11110000 indicates that DPS7-DPS4 are connected to the PSDB and DPS3-DPS0 aren't.
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HBI_PSDB_SLOTS - 1 on HBI or 0 to \ref HIL_SLOTS - 1 on HDMT using a break-out board (BOB).
//! @param[out] pStatus The DPS present status byte.  It may not be NULL.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiPsdbDpsPresentStatus(_In_ INT slot, _Out_ PBYTE pStatus);

//! @brief Reports whether or not fuses of each of the eight DPSs connected to an HBI PSDB are functional.
//!
//! This function reports whether or not fuses of each of the eight DPSs connected to an HBI Power and Signal Distribution Board are functional.
//! Each bit in the returned 8-bit value indicates status of the eight DPS Fuses in the following order DPS7Fuse : DPS0Fuse. (1 = Fuse OK)
//!
//! For example 0b11111110 indicates that DPS7Fuse - DPS1Fuse are OK. DPS0Fuse isn't.
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HBI_PSDB_SLOTS - 1 on HBI or 0 to \ref HIL_SLOTS - 1 on HDMT using a break-out board (BOB).
//! @param[out] pStatus The DPS present status byte.  It may not be NULL.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiPsdbDpsFuseOkStatus(_In_ INT slot, _Out_ PBYTE pStatus);

//! @brief Issues a read command to the PMBUS interface on one of the eight LTM4678 Power Manager devices on an HBI PSDB.
//!
//! This function issues a read command to the PMBUS interface on one of the eight LTM4678 Power Manager devices on an HBI Power and Signal Distribution Board.
//! @note Chip 4 through 7 are controlled by the Ring Multiplier FPGA on an HBI mainboard. Appropriately configuring the FPGA is required before using this function for these chips.
//!
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HBI_PSDB_SLOTS - 1 on HBI or 0 to \ref HIL_SLOTS - 1 on HDMT using a break-out board (BOB).
//! @param[in] chip A zero-based chip index.  Consult the following table.
//!                 Note that reference designators may change in new fabs.
//!                 |  Chip   | Reference designator |
//!                 | :-----: | :------------------: |
//!                 |    0    |          U55         |
//!                 |    1    |          U56         |
//!                 |    2    |          U10         |
//!                 |    3    |          U9          |
//!                 |    4    |          U74         |
//!                 |    5    |          U78         |
//!                 |    6    |          U76         |
//!                 |    7    |          U80         |
//! @param[in] page       Selects a page (corresponding to Channel 0 or Channel 1) for any command that supports paging.
//!                       This parameter may be 0xFF to select all channels or -1 for commands that do not support paging.
//! @param[in] command    PMBUS command to read.  See the device spec sheet for valid commands.
//! @param[out] pReadData A data buffer of bytes to hold the data read from the associated \c command.
//!                       Note that the bytes received from the device are located sequentially
//!                       in the buffer in the order they were received with the first byte being located
//!                       directly at \c pReadData.  This parameter cannot be NULL since all reads return at least one byte.
//! @param[in] readLength The byte length of the \c pReadData buffer.  Note that this parameter cannot be zero
//!                       since all reads return at least one byte.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiPsdbLtm4678PmbusRead(_In_ INT slot, _In_ UINT chip, _In_ INT page, _In_ BYTE command, _Out_writes_bytes_all_(readLength) LPVOID pReadData, _In_ DWORD readLength);

//! @brief Issue a write command to the PMBUS interface on one of the eight LTM4678 Power Manager devices on an HBI PSDB.
//!
//! This function issues a write command to the PMBUS interface on one of the eight LTM4678 Power Manager devices on an HBI Power and Signal Distribution Board.
//! @note Chip 4 through 7 are controlled by the Ring Multiplier FPGA on an HBI mainboard. Appropriately configuring the FPGA is required before using this function for these chips.
//!
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HBI_PSDB_SLOTS - 1 on HBI or 0 to \ref HIL_SLOTS - 1 on HDMT using a break-out board (BOB).
//! @param[in] chip A zero-based chip index.  Consult the following table.
//!                 Note that reference designators may change in new fabs.
//!                 |  Chip   | Reference designator |
//!                 | :-----: | :------------------: |
//!                 |    0    |          U55         |
//!                 |    1    |          U56         |
//!                 |    2    |          U10         |
//!                 |    3    |          U9          |
//!                 |    4    |          U74         |
//!                 |    5    |          U78         |
//!                 |    6    |          U76         |
//!                 |    7    |          U80         |
//! @param[in] page      Selects a page (corresponding to Channel 0 or Channel 1) for any command that supports paging.
//!                       This parameter may be 0xFF to select all channels or -1 for commands that do not support paging.
//! @param[in] command   PMBUS command to write. See the device spec sheet for valid commands.
//! @param[in] pCmdData  A data buffer of additional bytes to write for the associated \c command.
//!                      Note that the bytes are sent to the device sequentially starting with the first
//!                      byte pointed to by \c pCmdData.  The command byte should not be included in this
//!                      buffer as it is already supplied in \c command.  Note that this parameter may be
//!                      NULL for commands that have no data associated with them.
//! @param[in] cmdLength The byte length of the \c pCmdData buffer.  Note that this parameter may be zero for commands
//!                      that have no data.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiPsdbLtm4678PmbusWrite(_In_ INT slot, _In_ UINT chip, _In_ INT page, _In_ BYTE command, _In_reads_bytes_(cmdLength) const void* pCmdData, _In_ DWORD cmdLength);

//! @brief Directly writes an 8-bit register on the ADT7411 (8-channel ADC) device on an HBI PSDB.
//!
//! This function writes an 8-bit register on the ADT7411 (8-channel ADC) device on an HBI Power and Signal Distribution Board.
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HBI_PSDB_SLOTS - 1 on HBI or 0 to \ref HIL_SLOTS - 1 on HDMT using a break-out board (BOB).
//! @param[in] reg A valid register address for the Analog Devices ADT7411.
//! @param[in] data The data to write.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiPsdbAdt7411Write(_In_ INT slot, _In_ BYTE reg, _In_ BYTE data);

//! @brief Directly reads an 8-bit register on the ADT7411 (8-channel ADC) device on an HBI PSDB.
//!
//! This function reads an 8-bit register on the ADT7411 (8-channel ADC) device on an HBI Power and Signal Distribution Board.
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HBI_PSDB_SLOTS - 1 on HBI or 0 to \ref HIL_SLOTS - 1 on HDMT using a break-out board (BOB).
//! @param[in] reg  A valid register address for the Analog Devices ADT7411.
//! @param[out] pData The register contents.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiPsdbAdt7411Read(_In_ INT slot, _In_ BYTE reg, _Out_ PBYTE pData);

//! @brief Reads one of nine voltage monitor channels on an HBI PSDB.
//!
//! This function reads one of the nine monitor channels on an HBI Power and Signal Distribution Board.
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HBI_PSDB_SLOTS - 1 on HBI or 0 to \ref HIL_SLOTS - 1 on HDMT using a break-out board (BOB).
//! @param[in] channel Selects the monitoring channel. Valid values are 0 to 8.  The channels
//!                    and corresponding voltages monitored are :
//!                    | Channel |  Reference (Fab A)    |  Reference (Fab B, C)    |
//!                    | :-----: |  :------------------: |  :---------------------: |
//!                    |    0    |   +3P3V_16A           |   +3P3V_16A              |
//!                    |    1    |   VCCIO0_1_6A         |   VREF0_250MA            |
//!                    |    2    |   VCCIO1_1_6A         |   VREF1_250MA            |
//!                    |    3    |   VCCIO2_1_6A         |   VREF2_250MA            |
//!                    |    4    |   VCCIO3_1_6A         |   VREF3_250MA            |
//!                    |    5    |   +12V_375A           |   +12V_375A              |
//!                    |    6    |   +0P9V_16A_PEX       |   +0P9V_16A_PEX          |
//!                    |    7    |   +5P0V_50A_DPS       |   +5P0V_50A_DPS          |
//!                    |    8    |   VREF0_250MA         |   VCCIO0_1_6A            |
//! @param[out] pVoltage The voltage in volts.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiPsdbVmonRead(_In_ INT slot, _In_ UINT channel, _Out_ double* pVoltage);

//! @brief Reads all nine voltage channels of the ADT7411 on an HBI PSDB.
//!
//! This function reads all nine voltage channels of the ADT7411 on an HBI Power and Signal Distribution Board.  The values are scaled by the factors implemented by
//! the voltage dividers feeding the AIN1-AIN8 inputs and represent the voltage at the signals listed in the table below, not the
//! voltage at the ADT7411 input.
//!
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HBI_PSDB_SLOTS - 1 on HBI or 0 to \ref HIL_SLOTS - 1 on HDMT using a break-out board (BOB).
//! @param[out] pVoltages   An array of 9 readings in volts. The voltage readings (per HBI PSDB schematics) are :
//!                 | Voltages     |  Reference (Fab A)    |  Reference (Fab B, C)    |
//!                 | :----------: |  :------------------: |  :---------------------: |
//!                 | pVoltages[0] |   +3P3V_16A           |   +3P3V_16A              |
//!                 | pVoltages[1] |   VCCIO0_1_6A         |   VREF0_250MA            |
//!                 | pVoltages[2] |   VCCIO1_1_6A         |   VREF1_250MA            |
//!                 | pVoltages[3] |   VCCIO2_1_6A         |   VREF2_250MA            |
//!                 | pVoltages[4] |   VCCIO3_1_6A         |   VREF3_250MA            |
//!                 | pVoltages[5] |   +12V_375A           |   +12V_375A              |
//!                 | pVoltages[6] |   +0P9V_16A_PEX       |   +0P9V_16A_PEX          |
//!                 | pVoltages[7] |   +5P0V_50A_DPS       |   +5P0V_50A_DPS          |
//!                 | pVoltages[8] |   VREF0_250MA         |   VCCIO0_1_6A            |
//! @param[in]  numVoltages The size of the \c pVoltages array.  Currently required to be 9.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiPsdbVmonReadAll(_In_ INT slot, _Out_writes_all_(numVoltages) double* pVoltages, _In_ SIZE_T numVoltages);

//! @brief Reads a temperature channel on an HBI PSDB.
//!
//! This function reads a temperature channel on an HBI Power and Signal Distribution Board.
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HBI_PSDB_SLOTS - 1 on HBI or 0 to \ref HIL_SLOTS - 1 on HDMT using a break-out board (BOB).
//! @param[in] channel Selects the monitoring channel. Consult the following table:
//!                    | Channel |  Reference (Fab B, C)              |
//!                    | :-----: |  :-------------------------------: |
//!                    |    0    |   PM0 FPGA Temperature             |
//!                    |    1    |   PM1 FPGA Temperature             |
//!                    |    2    |   U2 ADT7411 Internal Temperature  |
//! @param[out] pTemp The temperature in degrees Celsius.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiPsdbTmonRead(_In_ INT slot, _In_ UINT channel, _Out_ double* pTemp);

//! @brief Writes a register within the specified MAX6650 (fan controller) device on an HBI PSDB.
//!
//! This function writes a register within the specified MAX6650 (fan controller) device on an HBI Power and Signal Distribution Board.
//!
//! @param[in] slot A physical slot number.  Valid values are 0 to \ref HBI_PSDB_SLOTS - 1 on HBI or 0 to \ref HIL_SLOTS - 1 on HDMT using a break-out board (BOB).
//! @param[in] chip A physical chip number. Consult the following table.
//!                 Note that reference designators are for Fab A and may change in future fabs.
//!                 | Index |    MAX6650    | Connector |
//!                 | :---: | :-----------: | :-------: |
//!                 |   0   |       U81     |   J305    |
//!                 |   1   |       U82     |   J306    |
//! @param[in] reg Writable MAX6650 register address.
//!                 | Register | Name         | Function              |
//!                 | :------: | :----------: | :-------------------- |
//!                 |   0x00   | SPEED        | Fan speed             |
//!                 |   0x02   | CONFIG       | Configuration         |
//!                 |   0x04   | GPIO DEF     | GPIO definition       |
//!                 |   0x06   | DAC          | DAC                   |
//!                 |   0x08   | ALARM ENABLE | Alarm Enable          |
//!                 |   0x16   | COUNT        | Tachometer count time |
//! @param[in] data The data value to write.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiPsdbMax6650Write(_In_ INT slot, _In_ UINT chip, _In_ BYTE reg, _In_ BYTE data);

//! @brief Reads a register within the specified MAX6650 (fan controller) device on an HBI PSDB.
//!
//! This function reads a register within the specified MAX6650 (fan controller) device on an HBI Power and Signal Distribution Board.
//!
//! @param[in] slot A physical slot number.  Valid values are 0 to \ref HBI_PSDB_SLOTS - 1 on HBI or 0 to \ref HIL_SLOTS - 1 on HDMT using a break-out board (BOB).
//! @param[in] chip A physical chip number. Consult the following table.
//!                 Note that reference designators are for Fab A and may change in future fabs.
//!                 | Index |    MAX6650    | Connector |
//!                 | :---: | :-----------: | :-------: |
//!                 |   0   |       U81     |   J305    |
//!                 |   1   |       U82     |   J306    |
//! @param[in] reg MAX6650 register address.
//!                 | Register | Name         | Function              |
//!                 | :------: | :----------: | :-------------------- |
//!                 |   0x00   | SPEED        | Fan speed             |
//!                 |   0x02   | CONFIG       | Configuration         |
//!                 |   0x04   | GPIO DEF     | GPIO definition       |
//!                 |   0x06   | DAC          | DAC                   |
//!                 |   0x08   | ALARM ENABLE | Alarm Enable          |
//!                 |   0x0A   | ALARM        | Alarm Status          |
//!                 |   0x0C   | TACH0        | Tachometer 0 count    |
//!                 |   0x0E   | TACH1        | Tachometer 1 count    |
//!                 |   0x10   | TACH2        | Tachometer 2 count    |
//!                 |   0x12   | TACH3        | Tachometer 3 count    |
//!                 |   0x14   | GPIO STAT    | GPIO Status           |
//!                 |   0x16   | COUNT        | Tachometer count time |
//! @param[out] pData The data value read back.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiPsdbMax6650Read(_In_ INT slot, _In_ UINT chip, _In_ BYTE reg, _Out_ PBYTE pData);

//! @brief Reads a fan tachometer device on an HBI PSDB.
//!
//! This function reads a fan tachometer on an device on an HBI Power and Signal Distribution Board.
//! @param[in] slot A physical slot number.  Valid values are 0 to \ref HBI_PSDB_SLOTS - 1 on HBI or 0 to \ref HIL_SLOTS - 1 on HDMT using a break-out board (BOB).
//! @param[in] fan The fan to read. Consult the following table:
//!                Note that reference designators are for Fab A and may change in future fabs.
//!                 |  fan  |     MAX6650    | Connector |
//!                 | :---: | :------------: | :-------: |
//!                 |   0   | U81 TACH_FAN#0 |   J305    |
//!                 |   1   | U82 TACH_FAN#1 |   J306    |
//! @param[out] pTach The fan speed in revolutions per second (RPS).
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiPsdbFanRead(_In_ INT slot, _In_ UINT fan, _Out_ double* pTach);

//! @brief Writes data to the 24LC512 (EEPROM) device on an HBI PSDB.
//!
//! This function writes data to the 24LC512 (EEPROM) device on an HBI PSDB.
//!
//! @note This device does not exist on HBI DPS Fab A, Fab B.
//!
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HBI_PSDB_SLOTS - 1 on HBI or 0 to \ref HIL_SLOTS - 1 on HDMT using a break-out board (BOB).
//! @param[in] address The location of the first byte in the EEPROM to which the data in the \c pData buffer will be written.
//! @param[in] pData   A data buffer containing data to write to the EEPROM device.
//! @param[in] length  The byte length of the \c pData buffer.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiPsdbEepromWrite(_In_ INT slot, _In_ DWORD address, _In_reads_bytes_(length) const void* pData, _In_ DWORD length);

//! @brief Reads data from the 24LC512 (EEPROM) device on an HBI PSDB.
//!
//! This function reads data from the 24LC512 (EEPROM) device on an HBI PSDB.
//!
//! @note This device does not exist on HBI DPS Fab A, Fab B.
//!
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HBI_PSDB_SLOTS - 1 on HBI or 0 to \ref HIL_SLOTS - 1 on HDMT using a break-out board (BOB).
//! @param[in] address The location of the first byte in the EEPROM from which the data will be read.
//! @param[out] pData  A pointer to a data buffer in which the read data will be stored. Note that this buffer must have at least \c length
//!                    bytes allocated in order to complete successfully.
//! @param[in] length  The number of bytes to read from the EEPROM.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hbiPsdbEepromRead(_In_ INT slot, _In_ DWORD address, _Out_writes_bytes_all_(length) LPVOID pData, _In_ DWORD length);

#ifdef __cplusplus
}
#endif
