// INTEL CONFIDENTIAL
// Copyright 2014-2020 Intel Corporation All Rights Reserved.
//
// The source code contained or described herein and all documents related to
// the source code ("Material") are owned by Intel Corporation or its
// suppliers or licensors. Title to the Material remains with Intel
// Corporation or its suppliers and licensors. The Material contains trade
// secrets and proprietary and confidential information of Intel or its
// suppliers and licensors. The Material is protected by worldwide copyright
// and trade secret laws and treaty provisions. No part of the Material may be
// used, copied, reproduced, modified, published, uploaded, posted,
// transmitted, distributed, or disclosed in any way without Intel's prior
// express written permission.
//
// No license under any patent, copyright, trade secret or other intellectual
// property right is granted to or conferred upon you by disclosure or
// delivery of the Materials, either expressly, by implication, inducement,
// estoppel or otherwise. Any license under such intellectual property rights
// must be express and approved by Intel in writing.

//! @file
//! @brief APIs to control the HDMT calibration base TIU.
//!
//! These APIs control the HDMT calibration base TIU.  They are stateless APIs.  This means that the user is responsible for cleaning up after themselves.
//! For example, if a call is made to turn off a GPIO data bit to turn on a relay, a matching call to turn the GPIO on (relay off) must be made, or it will remain on.
//!
//! \par Example (Python)
//! \code{.py}
//! import hil
//! hil.tiuCalBaseConnect()
//! hil.tiuCalBaseInit() # Turns off all active-low relays by writing GPIOs high.
//! slot=0
//! channel=0
//! hil.tiuCalBaseGpioWrite(hil.TCBG_SCOPE_FQCNTR_SEL,False) # Turns on active-low relay.
//! try:
//!     # Do something
//! finally:
//!     # Exceptions will still call cleanup.
//!     hil.tiuCalBaseGpioWrite(hil.TCBG_SCOPE_FQCNTR_SEL,True) # Turns off active-low relay.
//! \endcode

#pragma once
#include "HilDefs.h"
#include "TiuCalBaseDefs.h"

#ifdef __cplusplus
extern "C" {
#endif

//! @brief Verifies the HDMT calibration base TIU is present.
//!
//! This function verifies the HDMT calibration base TIU is present.  It connects to and caches driver resources for use
//! by other \c tiuCalBaseXXXXX functions.  tiuCalBaseDisconnect() is its complementary function and frees any cached resources.
//!
//! Neither tiuCalBaseConnect() or tiuCalBaseDisconnect() are required to be called to use the other \c tiuCalBaseXXXXX functions.  All
//! \c tiuCalBaseXXXXX functions will allocate resources if they are not already allocated and free them if required to
//! perform their function.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS tiuCalBaseConnect(void);

//! @brief Frees any resources cached from using the calibration base TIU functions.
//!
//! This function frees any resources associated with using the calibration base TIU HIL functions.  tiuCalBaseConnect() is its
//! complementary function.
//!
//! Neither tiuCalBaseConnect() or tiuCalBaseDisconnect() are required to be called to use the other \c tiuCalBaseXXXXX functions.  All
//! \c tiuCalBaseXXXXX functions will allocate resources if they are not already allocated and free them if required to
//! perform their function.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS tiuCalBaseDisconnect(void);

//! @brief Programs the vendor and product IDs of all USB devices on the calibration base TIU.
//!
//! This function programs the vendor and product IDs (VID/PIDs) of the FT2232, FT4232, and Cypress
//! USB devices on the calibration base TIU.  Correct VID/PIDs are required to ensure Windows Device
//! Manager displays a proper description of the device, and HIL requires correct VID/PIDs to ensure
//! software can locate and communicate with the correct component.
//! @note FTDI devices have a limited number of updates, but this function is safe to call because it
//! only writes if the component programming is incorrect.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS tiuCalBaseVidPidsSet(void);

//! @brief Initializes the calibration base TIU to a known state.
//!
//! This function initializes the calibration base TIU to a known state, including setting all GPIOs to a known state of logic high.
//! This ensures that the active low relays attached to the GPIOs are off.  Note that this function can only be called after power
//! has been applied to the TIU using bpTiuAuxPowerEnable().
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS tiuCalBaseInit(void);

//! @brief Reads the calibration base TIU's CPLD version register.
//!
//! This function reads the version register of the calibration base TIU's CPLD.
//!
//! @param[out] pVersion Returns the version register value.  The pointer must not be NULL.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS tiuCalBaseCpldVersion(_Out_ LPDWORD pVersion);

//! @brief Reads the calibration base TIU's CPLD version string.
//!
//! This function reads the version register of the calibration base TIU's CPLD and converts it to an ASCII string representation.
//!
//! @param[out] pVersion Returns the version string.  The pointer must not be NULL.
//! @param[in]  length   The length of the \c pVersion buffer.  It should be at least five characters to hold a null-terminated version string.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS tiuCalBaseCpldVersionString(_Out_writes_z_(length) LPSTR pVersion, _In_ DWORD length);

//! @brief Executes a Xilinx .XSVF file over the calibration base TIU's JTAG interface.
//!
//! This function executes a Xilinx .XSVF file over the calibration base TIU's JTAG interface.  A Xilinx CPLD is the only device
//! in the scan chain.  An appropriate .XSVF file can be generated through Xilinx's iMPACT software to perform
//! functions such as programming and/or verifying the content of the CPLD.
//! @param[in] filename An ANSI string containing an absolute or relative (to the current directory) CPLD .XSVF binary filename.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS tiuCalBaseJtagExecute(_In_z_ LPCSTR filename);

//! @brief Execute a buffer containing Xilinx XSVF data over the calibration base TIU's JTAG interface.
//!
//! This function executes a Xilinx XSVF data buffer over the calibration base TIU's JTAG interface.  A Xilinx CPLD
//! s the only device in the scan chain.  XSVF data can be generated through Xilinx's iMPACT software to perform
//! functions such as programming and/or verifying the content of the CPLD.
//! @param[in] pData The XSVF image data.
//! @param[in] length The length of the XSVF image data in bytes.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS tiuCalBaseJtagBufferExecute(_In_reads_bytes_(length) LPCVOID pData, _In_ DWORD length);

//! @brief Sets the specified GPIO line to a logic high or low.
//!
//! This function sets the specified GPIO line to a logic high or low.
//! @param[in] gpio  Specifies which single GPIO on the calibration base TIU to set high or low.
//! @param[in] state Specifies whether the GPIO line is set to logic high (TRUE) or logic low (FALSE).
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS tiuCalBaseGpioWrite(_In_ TIU_CAL_BASE_GPIO gpio, _In_ BOOL state);

//! @brief Reads the value of the specified GPIO line.
//!
//! This function reads the logic value of the specified GPIO line.
//! @param[in]  gpio Specifies which single GPIO on the calibration base TIU to read.
//! @param[out] pState Contains the logic state of the GPIO where TRUE is logic high and FALSE is logic low.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS tiuCalBaseGpioRead(_In_ TIU_CAL_BASE_GPIO gpio, _Out_ LPBOOL pState);

//! @brief Directly writes an 8-bit register of a PCA9505 device on the calibration base TIU.
//!
//! This function writes an 8-bit register of a PCA9505 device on the calibration base TIU.  This function
//! is for debugging only.  Prefer to use other specialized \c tiuCalBaseXXXXX functions instead.
//!
//! | chip    | board designator     |
//! | :---:   | :------------------: |
//! | 0       | U7   (I2C Addr 0x40) |
//! | 1       | U101 (I2C Addr 0x44) |
//! | 2       | U77  (I2C Addr 0x48) |
//!
//! @param[in] chip There are three PCA9505 devices on an calibration base TIU.  Valid values are 0-2.
//! @param[in] reg A valid register address for the PCA9505 device.  Valid values are 0-39 (00h-27h).
//! @param[in] data The value to write.  Valid values are 0-255 (00h-FFh).
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS tiuCalBasePca9505Write(_In_ BYTE chip, _In_ BYTE reg, _In_ BYTE data);

//! @brief Directly reads an 8-bit register of a PCA9505 device on the calibration base TIU.
//!
//! This function reads an 8-bit register of a PCA9505 device on the calibration base TIU.  This function
//! is for debugging only.  Prefer to use other specialized \c tiuCalBaseXXXXX functions instead.
//! @param[in] chip There are three PCA9505 devices on an calibration base TIU.  Valid values are 0-2.
//!            (See tiuCalBasePca9505Write() for details.)
//! @param[in] reg A valid register address for the PCA9505 device.  Valid values are 0-39 (00h-27h).
//! @param[out] pData The register contents read from the device.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS tiuCalBasePca9505Read(_In_ BYTE chip, _In_ BYTE reg, _Out_ PBYTE pData);

//! @brief Writes a byte of data to a register of the PCA9555 (GPIO Expander) device on the calibration base TIU.
//!
//! This function writes a byte of data to a register of the PCA9555 (GPIO Expander) device on the calibration base TIU.
//! This function also calls rcRootI2cLock() and rcRootI2cUnlock() during its execution since it accesses
//! devices on the root I2C bus.  If additional I2C commands need to be atomically executed on the root I2C
//! bus, then manually call rcRootI2cLock() and rcRootI2cUnlock() around the entire root I2C bus access.
//! @note tiuCalBaseInit() is required to be called before using this function.
//! @param[in] reg  PCA9555 register address.
//! @param[in] data The data value to write.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS tiuCalBasePca9555Write(_In_ BYTE reg, _In_ BYTE data);

//! @brief Reads a byte of data from a register of the PCA9555 (GPIO Expander) device on the calibration base TIU.
//!
//! This function reads a byte of data from a register of the PCA9555 (GPIO Expander) device on the calibration base TIU.
//! This function also calls rcRootI2cLock() and rcRootI2cUnlock() during its execution since it accesses
//! devices on the root I2C bus.  If additional I2C commands need to be atomically executed on the root I2C
//! bus, then manually call rcRootI2cLock() and rcRootI2cUnlock() around the entire root I2C bus access.
//! @note tiuCalBaseInit() is required to be called before using this function.
//! @param[in]  reg   PCA9555 register address.
//! @param[out] pData The data value read back.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS tiuCalBasePca9555Read(_In_ BYTE reg, _Out_ PBYTE pData);

//! @brief Directly writes an 8-bit register on a MAX6651 fan controller of the calibration base TIU.
//!
//! This function writes an 8-bit register on a MAX6651 fan controller of the calibration base TIU.
//! @param[in] chip A zero-based chip index.  Consult the following table.
//!                 | Chip    | Ref Des |
//!                 | :-----: | :-----: |
//!                 |    0    |   U27   |
//!                 |    1    |   U28   |
//! @param[in] reg  A valid register address for the Maxim Integrated MAX6651.
//! @param[in] data The value to write.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS tiuCalBaseMax6651Write(_In_ INT chip, _In_ BYTE reg, _In_ BYTE data);

//! @brief Directly reads an 8-bit register on a MAX6651 fan controller of the calibration base TIU.
//!
//! This function reads an 8-bit register on a MAX6651 fan controller of the calibration base TIU.
//! @param[in] chip A zero-based chip index.  Consult the following table.
//!                 | Chip    | Ref Des |
//!                 | :-----: | :-----: |
//!                 |    0    |   U27   |
//!                 |    1    |   U28   |
//! @param[in]  reg   A valid register address for the Maxim Integrated MAX6651.
//! @param[out] pData The register contents.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS tiuCalBaseMax6651Read(_In_ INT chip, _In_ BYTE reg, _Out_ PBYTE pData);

//! @brief Reads one of eight fan tachometer channels on the calibration base TIU.
//!
//! This function reads one of eight fan tachometer channels on the calibration base TIU. The
//! channels and corresponding tachometers are (per Fab D schematic):
//!
//! | Channel | Tachometer  | Reference designator |
//! | :-----: | :---------: | :------------------: |
//! | 0       | TACH_FAN1   | JFAN_1               |
//! | 1       | TACH_FAN2   | JFAN_2FUT            |
//! | 2       | TACH_FAN3   | JFAN_3               |
//! | 3       | TACH_FAN4   | JFAN_4FUT            |
//! | 4       | TACH_FAN5   | JFAN_5               |
//! | 5       | TACH_FAN6   | JFAN_6FUT            |
//! | 6       | TACH_FAN7   | JFAN_7               |
//! | 7       | TACH_FAN8   | JFAN_8FUT            |
//! @note The fan controllers are set to power-up defaults.  tiuCalBaseInit() will reset those defaults if altered by tiuCalBaseMax6651Write().
//! @note After power-on the tachs will read zero until the initial integration time is past (about 1 second).
//! @param[in]  channel Selects the tachometer channel. Valid values are 0 to 7.
//! @param[out] pTach   The fan speed in revolutions per second (RPS).
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS tiuCalBaseFanRead(_In_ UINT channel, _Out_ double* pTach);

//! @brief Sets an LTC2485 (24-Bit ADC) device's operating mode.
//!
//! This function sets an LTC2485 (24-Bit ADC) device's operating mode.
//! @param[in] mode One of the valid modes of operation for LTC2485 (24-Bit ADC) devices. The mode can be one of:
//! | LTC2485 Mode | Description                                                  |
//! | :----------: | :----------------------------------------------------------- |
//! | 0            | External Input, 50Hz and 60Hz Rejection, Auto-Calibration    |
//! | 1            | External Input, 50Hz and 60Hz Rejection, 2x Speed            |
//! | 2            | External Input, 50Hz Rejection, Auto-Calibration             |
//! | 3            | External Input, 50Hz Rejection, 2x Speed                     |
//! | 4            | External Input, 60Hz Rejection, Auto-Calibration             |
//! | 5            | External Input, 60Hz Rejection, 2x Speed                     |
//! | 8            | Temperature Input, 50Hz and 60Hz Rejection, Auto-Calibration |
//! | 10           | Temperature Input, 50Hz Rejection, Auto-Calibration          |
//! | 11           | Temperature Input, 50Hz Rejection, Auto-Calibration          |
//! | 12           | Temperature Input, 60Hz Rejection, Auto-Calibration          |
//! | 13           | Temperature Input, 60Hz Rejection, Auto-Calibration          |
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS tiuCalBaseLtc2485ModeSet(_In_ BYTE mode);

//! @brief Reads a voltage value from an LTC2485 (24-Bit ADC) device on the calibration base TIU.
//!
//! This function reads a voltage value from an LTC2485 (24-Bit ADC) device on the calibration base TIU.
//! @param[out] pVoltage A pointer to double that receives the voltage in volts.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS tiuCalBaseLtc2485VoltageRead(_Out_ double* pVoltage);

//! @brief Reads the board-level traceability values from a TIU.
//!
//! This function reads the board-level traceability values from a TIU.  The values are defined in the #BLT structure.
//!
//! @param[in] eepromIndex Index of EEPROM to read.  Valid values are 0-7.  0 is required to be present.
//!                        Others are optional and will return #HS_I2C_NOACK if not present.
//! @param[out] pBlt The address of a #BLT structure that will be populated with the data.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS tiuBltBoardRead(_In_ UINT eepromIndex, _Out_ PBLT pBlt);

//! @brief Writes the board-level traceability values of a TIU.
//!
//! This function writes the board-level traceability values of a TIU.  The values are defined in the #BLT structure.
//!
//! The \c flags field allows this function to update individual values.  The flags can be ORed together.  Only the fields indicated by
//! \c flags need to be populated in the provided \c pBlt parameter.
//!
//! \par Updating the Full BLT - C
//! \code{.c}
//! #include <Windows.h>
//! #include "TiuCalBaseApi.h"
//! #include "string.h"
//!
//! int main()
//! {
//!     BLT blt;
//!     HIL_STATUS status;
//!     blt.Id = 0x01000020;
//!     strcpy(blt.DeviceName,"DeviceName");
//!     strcpy(blt.VendorName,"INTEL");
//!     strcpy(blt.PartNumberAsBuilt,"AAxxxxxx-xxx");
//!     strcpy(blt.PartNumberCurrent,blt.PartNumberAsBuilt);
//!     strcpy(blt.SerialNumber,"12345678");
//!     strcpy(blt.ManufactureDate,"YYMMDD");
//!     status = tiuBltBoardWrite(0, &blt, BF_ALL);
//!     if (status != HS_SUCCESS)
//!         return -1;
//!     return 0;
//! }
//! \endcode
//! \par Updating the Current Part Number - Python
//! \code{.py}
//! blt = hil.BLT()
//! blt.PartNumberCurrent = 'AAyyyyyy-yyy'
//! eepromIndex = 0
//! hil.tiuBltBoardWrite(eepromIndex, blt,hil.BF_PART_NUMBER_CURRENT)
//! # Note Python returns None for HIL_STATUS if successful and raises RuntimeError on failure.
//! \endcode
//!
//! \note HS_BLT_NOT_FOUND can be returned on an update (lack of #BF_ALL flag).
//!
//! @param[in] eepromIndex Index of EEPROM to read.  Valid values are 0-7.  0 is required to be present.
//!                        Others are optional and will return #HS_I2C_NOACK if not present.
//! @param[in] pBlt The address of a #BLT structure that contains the data to be written or updated.
//! @param[in] flags #BLT_FLAGS bits indicating what fields in \c pBlt are valid.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS tiuBltBoardWrite(_In_ UINT eepromIndex, _In_ PCBLT pBlt, _In_ BLT_FLAGS flags);

//! @brief Reads bytes from a TIU's EEPROM(s).
//!
//! This function reads bytes from a TIU's EEPROM(s).  Note that the standard calibration base TIU contains one
//! MC24LC64 EEPROM (8KB), but production TIUs can have 1-8 MC24LC512 EEPROMs (64KB).  If reading an 8KB EEPROM,
//! reading beyond 8KB will wrap around to the beginning of the device.
//!
//! @param[in] eepromIndex Index of EEPROM to read.  Valid values are 0-7.  0 is required to be present.
//!                        Others are optional and will return #HS_I2C_NOACK if not present.
//! @param[in]  address    The address to begin reading from the EEPROM.  Valid values are 0x0000-0xFFFF.  8KB EEPROMs
//!                        will wrap to the beginning of the device and continue reading.
//! @param[out] pData      The output buffer.  It cannot be NULL.
//! @param[in]  length     The amount of data to read.  \c address plus \c length cannot exceed 0x10000 (64KB).
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS tiuEepromRead(_In_ UINT eepromIndex, _In_ UINT address, _Out_writes_bytes_all_(length) LPVOID pData, _In_ DWORD length);

//! @brief Writes bytes to a TIU's EEPROM(s).
//!
//! This function writes bytes to a TIU's EEPROM(s).  Note that the standard calibration base TIU contains one
//! MC24LC64 EEPROM (8KB), but production TIUs can have 1-8 MC24LC512 EEPROMs (64KB).  If reading an 8KB EEPROM,
//! reading beyond 8KB will wrap around to the beginning of the device.
//!
//! @param[in] eepromIndex Index of EEPROM to read.  Valid values are 0-7.  0 is required to be present.
//!                        Others are optional and will return #HS_I2C_NOACK if not present.
//! @param[in]  address    The address to begin reading from the EEPROM.  Valid values are 0x0000-0xFFFF.  8KB EEPROMs will wrap
//!                        to the beginning of the device and continue writing.
//! @param[in]  pData      The input buffer.  It cannot be NULL.
//! @param[in]  length     The amount of data to write.  \c address plus \c length cannot exceed 0x10000 (64KB).
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS tiuEepromWrite(_In_ UINT eepromIndex, _In_ UINT address, _In_reads_bytes_(length) const void* pData, _In_ DWORD length);

//! @brief Reads the extended board-level traceability values from a TIU.
//!
//! This function reads the extended board-level traceability values from a TIU.  The values are defined in the #BLTEX structure.
//! @note In Python, extra methods are exposed in the #BLTEX structure for updating fields.  See #BLTEX for details.
//!
//! @param[in] eepromIndex Index of EEPROM to read.  Valid values are 0-7.  0 is required to be present.
//!                        Others are optional and will return #HS_I2C_NOACK if not present.
//! @param[out] pBltEx     The address of a #BLTEX structure that will be populated with the data.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS tiuBltExRead(_In_ UINT eepromIndex, _Out_ PBLTEX pBltEx);

//! @brief Writes the extended board-level traceability values of a TIU.
//!
//! This function writes the extended board-level traceability values of a TIU.  The values are defined in the #BLTEX structure.
//! @note In Python, extra methods are exposed in the #BLTEX structure for updating fields.  See #BLTEX for details.
//!
//! @param[in] eepromIndex Index of EEPROM to read.  Valid values are 0-7.  0 is required to be present.
//!                        Others are optional and will return #HS_I2C_NOACK if not present.
//! @param[in] pBltEx      The address of a #BLTEX structure that contains the data to be written.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS tiuBltExWrite(_In_ UINT eepromIndex, _In_ PCBLTEX pBltEx);

//! @brief Reads extended counter values from a TIU.
//!
//! This function reads extended counter values from a TIU.  The values are defined in the #BLTEX_COUNTERS structure.
//! @note In Python, extra methods are exposed in the #BLTEX_COUNTERS structure for updating fields.  See #BLTEX_COUNTERS for details.
//!
//! @param[in] eepromIndex     Index of EEPROM to read.  Valid values are 0-7.  0 is required to be present.
//!                            Others are optional and will return #HS_I2C_NOACK if not present.
//! @param[in] counterIndex    Index of the counter record to read.  There are 10 counter records available.  Valid indexes are 0-9.
//! @param[out] pBltExCounters The address of a #BLTEX_COUNTERS structure that will be populated with the data.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS tiuBltExCountersRead(_In_ UINT eepromIndex, _In_ UINT counterIndex, _Out_ PBLTEX_COUNTERS pBltExCounters);

//! @brief Writes extended counter values of a TIU.
//!
//! This function writes extended counter values of a TIU.  The values are defined in the #BLTEX_COUNTERS structure.
//! @note In Python, extra methods are exposed in the #BLTEX_COUNTERS structure for updating fields.  See #BLTEX_COUNTERS for details.
//!
//! @param[in] eepromIndex    Index of EEPROM to read.  Valid values are 0-7.  0 is required to be present.
//!                           Others are optional and will return #HS_I2C_NOACK if not present.
//! @param[in] counterIndex   Index of the counter record to write.  There are 10 counter records available.  Valid indexes are 0-9.
//! @param[in] pBltExCounters The address of a #BLTEX_COUNTERS structure that contains the data to be written.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS tiuBltExCountersWrite(_In_ UINT eepromIndex, _In_ UINT counterIndex, _In_ PCBLTEX_COUNTERS pBltExCounters);

//! @brief Reads cycle counter values from a TIU.
//!
//! This function reads cycle counter values from a TIU.  The values are defined in the #BLTCY_COUNTERS structure.
//!
//! @param[in] eepromIndex     Index of EEPROM to read.  Valid values are 0-7.  0 is required to be present.
//!                            Others are optional and will return #HS_I2C_NOACK if not present. By spec this
//!                            is only allowed on the primary EEPROM, but allowing multiples for consistency.
//! @param[out] pBltCyCounters The address of a #BLTCY_COUNTERS structure that will be populated with the data.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS tiuBltCyCountersRead(_In_ UINT eepromIndex, _Out_ PBLTCY_COUNTERS pBltCyCounters);

//! @brief Writes cycle counter values of a TIU.
//!
//! This function writes cycle counter values of a TIU.  The values are defined in the #BLTCY_COUNTERS structure.
//!
//! @param[in] eepromIndex    Index of EEPROM to read.  Valid values are 0-7.  0 is required to be present.
//!                           Others are optional and will return #HS_I2C_NOACK if not present. By spec this
//!                           is only allowed on the primary EEPROM, but allowing multiples for consistency.
//! @param[in] pBltCyCounters The address of a #BLTEX_COUNTERS structure that contains the data to be written.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS tiuBltCyCountersWrite(_In_ UINT eepromIndex, _In_ PCBLTCY_COUNTERS pBltCyCounters);

//! @brief Writes data to a register within the LMK04808 (Clock Conditioner) device on the calibration base TIU.
//!
//! This function writes data to a register within the LMK04808 (Clock Conditioner) device on the calibration base TIU.
//! @note At no time may registers be programmed to values other than the valid states defined in the datasheet.
//! @param[in] reg  LMK04808 register address.  Valid values are 0x00 to 0x1F.
//! @param[in] data The data value to write.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS tiuCalBaseLmk04808Write(_In_ BYTE reg, _In_ DWORD data);

//! @brief Reads data from a register within the LMK04808 (Clock Conditioner) device on the calibration base TIU.
//!
//! This function reads data from a register within the LMK04808 (Clock Conditioner) device on the calibration base TIU.
//! @param[in]  reg   LMK04808 register address.  Valid values are 0x00 to 0x1F.
//! @param[out] pData The data value read back.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS tiuCalBaseLmk04808Read(_In_ BYTE reg, _Out_ LPDWORD pData);

//! @brief Checks the PLL lock status of the LMK04808 clock device on the calibration base TIU.
//!
//! This function checks the PLL lock status of the LMK04808 clock device on the calibration base TIU.
//! It should be called after programming the clock to verify the clock is running properly.
//!
//! @param[out] pIsLocked TRUE if the PLL is locked, FALSE otherwise.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS tiuCalBaseLmk04808PllIsLocked(_Out_ BOOL* pIsLocked);

//! @brief Writes data to a register within a specified HMC987 (Fanout Buffer) device on the calibration base TIU.
//!
//! This function writes data to a register within a specified HMC987 (Fanout Buffer) device on the calibration base TIU.
//! @param[in] chip A zero-based chip index.  Consult the following table.
//!                 |  Chip   | Ref Des |
//!                 | :-----: | :-----: |
//!                 |    0    |   EU16  |
//!                 |    1    |   EU17  |
//! @param[in] reg HMC987 register address.  Valid values are 0x00 to 0x05.
//! @param[in] data The data value to write.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS tiuCalBaseHmc987Write(_In_ UINT chip, _In_ BYTE reg, _In_ DWORD data);

//! @brief Sets delay values for two channels within the SY89297U device on the calibration base TIU.
//!
//! This function sets delay values for two channels within the SY89297U device on the calibration base TIU.
//! @warning For calibration performance this function and those listed in **See also** below do not close device handles when the function completes.
//!          Users MUST call tiuCalBaseDisconnect() as soon these functions are no longer required.
//! @param[in] delayA Desired value of delay for channel A. Valid range is 0 - 5115ps (rounds to nearest 5ps increment).
//! @param[in] delayB Desired value of delay for channel B. Valid range is 0 - 5115ps (rounds to nearest 5ps increment).
//! @returns \ref HIL_STATUS
//! @see tiuCalBaseWindowCompRefSet\n
//!      tiuCalBaseWindowCompOutputEnable()\n
//!      tiuCalBaseWindowCompOutputSelect()\n
//!      tiuCalBaseWindowCompOutputRead()
HIL_API HIL_STATUS tiuCalBaseDelayLineSet(_In_ UINT delayA, _In_ UINT delayB);

//! @brief Sets the specified reference voltage level for the Window Comparator on the calibration base TIU.
//!
//! This function sets the specified reference voltage level for the Window Comparator on the calibration base TIU using LTC1592 DACs.
//! @note Due to implementation details, an input reference level is recommended to be within +/-2.0 volts.
//! @warning For calibration performance this function and those listed in **See also** below do not close device handles when the function completes.
//!          Users MUST call tiuCalBaseDisconnect() as soon these functions are no longer required.
//! @param[in]  chip   Selects the DAC associated with a reference level. Consult the following table.
//!                    | Chip    | Comment                         |
//!                    | :-----: | :------------------------------ |
//!                    |    0    | Reference Top (Input to RT)     |
//!                    |    1    | Reference Bottom (Input to RB)  |
//! @param[in] voltage Desired value of reference voltage for Window Comparator in volts.  Valid values are -2.5 to 2.5.
//! @returns \ref HIL_STATUS
//! @see tiuCalBaseWindowCompOutputEnable()\n
//!      tiuCalBaseWindowCompOutputSelect()\n
//!      tiuCalBaseWindowCompOutputRead()\n
//!      tiuCalBaseDelayLineSet()
HIL_API HIL_STATUS tiuCalBaseWindowCompRefSet(_In_ UINT chip, _In_ double voltage);

//! @brief Enables the selected data output from the CPLD for the window comparator (HMC974LC3C) on the calibration base TIU.
//!
//! This function enables sending the data selected by tiuCalBaseWindowCompOutputSelect() from the CPLD for the window comparator
//! (HMC974LC3C) on the calibration base TIU.
//! @note Calling tiuCalBaseInit() is required before using this function.
//! @warning For calibration performance this function and those listed in **See also** below do not close device handles when the function completes.
//!          Users MUST call tiuCalBaseDisconnect() as soon these functions are no longer required.
//! @param[in] enable Sends the data from the CPLD for the selected windows comparator output if TRUE or sends the ~CPLD_WOUTB if FALSE.
//! @returns \ref HIL_STATUS
//! @see tiuCalBaseWindowCompRefSet()\n
//!      tiuCalBaseWindowCompOutputSelect()\n
//!      tiuCalBaseWindowCompOutputRead()\n
//!      tiuCalBaseDelayLineSet()
HIL_API HIL_STATUS tiuCalBaseWindowCompOutputEnable(_In_ BOOL enable);

//! @brief Selects which output signal of the window comparator (HMC974LC3C) will be sent from the CPLD on the calibration base TIU.
//!
//! This function selects which output signal of the window comparator (HMC974LC3C) will be sent from the CPLD on the calibration base TIU.
//! @note Calling tiuCalBaseInit() is required before using this function.
//! @warning For calibration performance this function and those listed in **See also** below do not close device handles when the function completes.
//!          Users MUST call tiuCalBaseDisconnect() as soon these functions are no longer required.
//! @param[in] select The window comparator output for CPLD to send. Consult the following table:
//!                   | Select | Description |
//!                   | :----: | :---------: |
//!                   | 0      | WOUTB       |
//!                   | 1      | ORB         |
//!                   | 2      | URB         |
//! @returns \ref HIL_STATUS
//! @see tiuCalBaseWindowCompRefSet()\n
//!      tiuCalBaseWindowCompOutputEnable()\n
//!      tiuCalBaseWindowCompOutputRead()\n
//!      tiuCalBaseDelayLineSet()
HIL_API HIL_STATUS tiuCalBaseWindowCompOutputSelect(_In_ BYTE select);

//! @brief Reads bytes of selected data output from the CPLD for the window comparator (HMC974LC3C) on the calibration base TIU.
//!
//! This function reads bytes of selected data output from the CPLD for the window comparator (HMC974LC3C) on the calibration base TIU.
//! Each byte contains 8 1-bit samples.
//! @note Calling tiuCalBaseInit() is required before using this function.
//! @warning For calibration performance this function and those listed in **See also** below do not close device handles when the function completes.
//!          Users MUST call tiuCalBaseDisconnect() as soon these functions are no longer required.
//! @param[out] pData  The output buffer.  It cannot be NULL.
//! @param[in]  length The amount of data to read in bytes (minimum 1).
//! @returns \ref HIL_STATUS
//! @see tiuCalBaseWindowCompRefSet()\n
//!      tiuCalBaseWindowCompOutputEnable()\n
//!      tiuCalBaseWindowCompOutputSelect()\n
//!      tiuCalBaseDelayLineSet()
HIL_API HIL_STATUS tiuCalBaseWindowCompOutputRead(_Out_writes_bytes_all_(length) LPVOID pData, _In_ DWORD length);

#ifdef __cplusplus
}
#endif
