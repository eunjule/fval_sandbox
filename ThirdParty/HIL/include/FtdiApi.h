// INTEL CONFIDENTIAL
// Copyright 2014-2018 Intel Corporation All Rights Reserved.
//
// The source code contained or described herein and all documents related to
// the source code ("Material") are owned by Intel Corporation or its
// suppliers or licensors. Title to the Material remains with Intel
// Corporation or its suppliers and licensors. The Material contains trade
// secrets and proprietary and confidential information of Intel or its
// suppliers and licensors. The Material is protected by worldwide copyright
// and trade secret laws and treaty provisions. No part of the Material may be
// used, copied, reproduced, modified, published, uploaded, posted,
// transmitted, distributed, or disclosed in any way without Intel's prior
// express written permission.
//
// No license under any patent, copyright, trade secret or other intellectual
// property right is granted to or conferred upon you by disclosure or
// delivery of the Materials, either expressly, by implication, inducement,
// estoppel or otherwise. Any license under such intellectual property rights
// must be express and approved by Intel in writing.

//! @file
//! @brief Low-level APIs to directly control FTDI USB devices.
//!
//! The FTDI APIs implement functionality of FTDI USB devices including access to GPIO and the MPSSE engine supporting I2C and SPI communication protocols.
//! They are exposed to facilitate algorithm development for card-level APIs and should not be used unless a card-level API is not available.
#pragma once
#include "HilDefs.h"
#include "FtdiDefs.h"

#ifdef __cplusplus
extern "C" {
#endif

//! @brief Determines the number of supported FTDI devices installed.
//!
//! This function determines the number of supported FTDI devices available in the system.
//! This number is used to allocate the correct number of \ref FTDI_DEVICE_LIST_INFO structures
//! for ftdiDeviceInfoList().
//! @param[out] pNumDevices The number of FTDI devices found.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS ftdiDeviceInfoListCreate(_Out_ LPDWORD pNumDevices);

//! @brief Returns an information structure about each FTDI device installed.
//!
//! This function returns an \ref FTDI_DEVICE_LIST_INFO structure about each FTDI device installed.
//! @param[out]    pDevices    A user-allocated block of \ref FTDI_DEVICE_LIST_INFO structures to be filled out by this function.
//! @param[in,out] pNumDevices On input, a pointer to the number of structures in the user-allocated \c pNumDevices memory block. On output it
//!                            will point to the number of structures filled out by the call.  NOTE! The user-allocated memory block
//!                            \b must be large enough to contain at least the number of structures returned by ftdiDeviceInfoListCreate().
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS ftdiDeviceInfoList(_Out_writes_to_(*pNumDevices, *pNumDevices) FTDI_DEVICE_LIST_INFO* pDevices, _Inout_ LPDWORD pNumDevices);

//! @brief Opens an FTDI device handle given the location ID of the device.
//!
//! This function opens an FTDI device handle given the location ID of the device.  The valid location IDs can be listed by calling
//! ftdiDeviceInfoList() and using the \c LocId fields returned.
//!
//! @param[in]  location A valid location from ftdiDeviceInfoList().
//! @param[out] pHandle  An open handle to the device.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS ftdiOpenEx(_In_ DWORD location, _Out_ PHFTDI pHandle);

//! @brief Initializes default USB parameters for the FTDI device.
//!
//! This function initializes default USB parameters for the FTDI device.
//! @param[in] h An open handle from ftdiOpenEx().
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS ftdiUsbInit(_In_ HFTDI h);

//! @brief Closes an open FTDI device handle.
//!
//! This function closes an open FTDI device handle.
//! @param[in] h An open handle from ftdiOpenEx().
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS ftdiClose(_In_ HFTDI h);

//! @brief Sets the baud rate of an FTDI device port.
//!
//! This function sets the baud rate of an FTDI device port.  Since some devices
//! have different algorithms to set this value, the device type (see ftdiDeviceInfoList())
//! must be provided.
//! @param[in] h              An open handle from ftdiOpenEx().
//! @param[in] ftdiDeviceType The device type (see ftdiDeviceInfoList()).
//! @param[in] baudRate The baud rate required in bits/sec.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS ftdiBaudRateSet(_In_ HFTDI h, _In_ INT ftdiDeviceType, _In_ INT baudRate);

//! @brief Initializes the mode and initial state of an FTDI device port.
//!
//! This function sets the FTDI operating mode, initial direction, and initial state of each
//! bit on an FTDI port.  Note that \c mode can be \b one of the following values.  See the data sheet
//! for a particular FTDI device for which modes are supported.
//!
//! | Mode                         | Value |
//! | :--------------------------- | :---: |
//! | \ref FTDI_MODE_RESET         | 0x00  |
//! | \ref FTDI_MODE_ASYNC_BITBANG | 0x01  |
//! | \ref FTDI_MODE_MPSSE         | 0x02  |
//! | \ref FTDI_MODE_SYNC_BITBANG  | 0x04  |
//! | \ref FTDI_MODE_MCU_HOST      | 0x08  |
//! | \ref FTDI_MODE_FAST_SERIAL   | 0x10  |
//! | \ref FTDI_MODE_CBUS_BITBANG  | 0x20  |
//! | \ref FTDI_MODE_SYNC_FIFO     | 0x40  |
//!
//! @param[in] h                  An open handle from ftdiOpenEx().
//! @param[in] initPinsDirection  Each bit of this value sets the I/O direction of that bit on the port.
//!                               0 = input, 1 = output.
//! @param[in] initData           Each bit of this value sets the initial value of that bit on the port.
//! @param[in] mode               The operating mode of the port (see above table).
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS ftdiModeSet(_In_ HFTDI h, _In_ BYTE initPinsDirection, _In_ BYTE initData, _In_ BYTE mode);

//! @brief Determines the number of bytes available to read from an FTDI device port.
//!
//! This function determines the number of bytes available to read from an FTDI device port.
//! This can be used to read only data that is available.  Functions like ftdiMpsseSpiRead()
//! are synchronous and will wait for the data to become available if more data is read than
//! can be provided.
//! @param[in] h             An open handle from ftdiOpenEx().
//! @param[out] pRxQueueCount The number of bytes in the receive queue.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS ftdiQueueStatus(_In_ HFTDI h, _Out_ LPDWORD pRxQueueCount);

//! @brief Determines the status of current FTDI communication for an FTDI device.
//!
//! This function determines the status of current FTDI communication for an FTDI device
//! and is a direct passthrough to the FTDI FT_QueueStatus API.  See that documentation if you need it.
//! See ftdiQueueStatus() if you only need to see if data is ready to be read.
//! @param[in]  h             An open handle from ftdiOpenEx().
//! @param[out] pRxQueueCount The number of bytes in the receive queue.
//! @param[out] pTxQueueCount The number of bytes in the transmit queue.
//! @param[out] pEventStatus  The FTDI event status.  See the FTDI documentation.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS ftdiStatus(_In_ HFTDI h, _Out_ LPDWORD pRxQueueCount, _Out_ LPDWORD pTxQueueCount, _Out_ LPDWORD pEventStatus);

//! @brief Reads data directly from an FTDI device.
//!
//! This function reads data directly from an FTDI device and is a direct passthrough to the FTDI FT_Read API.
//! @param[in]  h          An open handle from ftdiOpenEx().
//! @param[in]  pData      A buffer of \c maxlength bytes to receive the data read.
//! @param[in]  maxLength  The length of the \c pData buffer.
//! @param[out] pBytesRead The number of data bytes returned.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS ftdiRead(_In_ HFTDI h, _Out_writes_bytes_all_(maxLength) LPVOID pData, _In_ DWORD maxLength, _Out_ LPDWORD pBytesRead);

//! @brief Writes data directly to an FTDI device.
//!
//! This function writes data directly to an FTDI device and is a direct passthrough to the FTDI FT_Write API.
//! @param[in]  h             An open handle from ftdiOpenEx().
//! @param[in]  pData         A buffer of at least \c byteLength bytes of data to write.
//! @param[in]  byteLength    The number of bytes to write from the \c pData buffer.
//! @param[out] pBytesWritten The actual number of bytes written after the call completes.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS ftdiWrite(_In_ HFTDI h, _In_reads_bytes_(byteLength) const void* pData, _In_ DWORD byteLength, _Out_ LPDWORD pBytesWritten);

//! @brief Reads the current value of GPIO pins on an FTDI device port.
//!
//! This function reads the current value of GPIO pins on an FTDI device port.
//!
//! \c mask is bitwise-ANDed with the value read so the returned data only
//! represents pins of interest.
//! @param[in] h      An open handle from ftdiOpenEx().
//! @param[in] mask   A value to bitwise-AND with the data read.
//! @param[out] pData The returned pin data bitwise-ANDed with \c mask.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS ftdiInstGpioRead(_In_ HFTDI h, _In_ BYTE mask, _Out_ PBYTE pData);

//! @brief Writes a series of GPIO pin values to an FTDI device port using bit-bang mode.
//!
//! This function writes a series of GPIO pin values to an FTDI device port configured
//! via ftdiModeSet() to one of the "bit bang" modes.  Refer to the data sheet of the
//! FTDI device for more details.
//!
//! \c mask indicates which bits of the port to write. This function maintains the
//! value of pins not indicated in \c mask.
//!
//! Use ftdiModeSet() and ftdiBaudRateSet() first to configure bit-bang mode and the
//! speed of transmission.
//!
//! @param[in] h           An open handle from ftdiOpenEx().
//! @param[in] mask        A value where each one bit indicates a port bit to change, leaving pins with a zero in the mask unchanged.
//! @param[in] pData       A data buffer whose bytes are to be streamed to the port pins.
//! @param[in] byteLength The number of bytes to write.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS ftdiBitBangGpioWrite(_In_ HFTDI h, _In_ BYTE mask, _In_reads_bytes_(byteLength) const void* pData, _In_ DWORD byteLength);

//! @brief Reads a series of GPIO pin values from an FTDI device port using bit-bang mode.
//!
//! This function reads a series of GPIO pin values from an FTDI device port configured
//! via ftdiModeSet() to one of the "bit bang" modes.  Refer to the data sheet of the
//! FTDI device for more details.
//!
//! \c mask is bitwise-ANDed with the values read so the returned data only
//! represents pins of interest.
//!
//! Use ftdiModeSet() and ftdiBaudRateSet() first to configure bit-bang mode and the
//! speed of transmission.
//!
//! @param[in]  h          An open handle from ftdiOpenEx().
//! @param[in]  mask       A value to bitwise-AND with the data read.
//! @param[out] pData      A data buffer whose will be filled by the port data qualified by \c mask.
//! @param[in]  byteLength The number of bytes to read.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS ftdiBitBangGpioRead(_In_ HFTDI h, _In_ BYTE mask, _Out_writes_bytes_all_(byteLength) LPVOID pData, _In_ DWORD byteLength);

//! @brief Writes GPIO 8-bit pin values to an FTDI device port using MPSSE mode.
//!
//! This function writes GPIO 8-bit pin values to an FTDI device port configured
//! via ftdiModeSet() to MPSSE mode.  Refer to the data sheet of the
//! FTDI device for more details.  Note that a devices such as FT2232H have a 16-bit
//! port requiring ftdiMpsseGpioHighWrite() to manipulate the upper 8-bits.
//!
//! MPSSE mode requires the direction of every pin on the port to be indicated for every write.
//!
//! \c pinsDirection is typically the same value used by ftdiModeSet(), but could
//! be different if the protocol requires a pin to change direction.
//!
//! \c mask indicates which bits of the port to write. This function maintains the
//! value of pins not indicated in \c mask.
//!
//! Use ftdiModeSet() first to configure MPSSE mode.
//!
//! @param[in] h              An open handle from ftdiOpenEx().
//! @param[in] pinsDirection  Each bit of this value sets the I/O direction of that bit on the port.
//!                           0 = input, 1 = output.
//! @param[in] mask           A value where each one bit indicates a port bit to change, leaving pins with a zero in the mask unchanged.
//! @param[in] data           The data to write to the port qualified by \c mask.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS ftdiMpsseGpioWrite(_In_ HFTDI h, _In_ BYTE pinsDirection, _In_ BYTE mask, _In_ BYTE data);

//! @brief Reads GPIO 8-bit pin values from an FTDI device port using MPSSE mode.
//!
//! This function reads GPIO pin values from an FTDI device port configured
//! via ftdiModeSet() to MPSSE mode.  Refer to the data sheet of the
//! FTDI device for more details.  Note that a devices such as FT2232H have a 16-bit
//! port requiring ftdiMpsseGpioHighRead() to read the upper 8-bits.
//!
//! \c mask is bitwise-ANDed with the values read so the returned data only
//! represents pins of interest.
//!
//! Use ftdiModeSet() first to configure MPSSE mode.
//!
//! @param[in] h      An open handle from ftdiOpenEx().
//! @param[in] mask   A value to bitwise-AND with the data read.
//! @param[out] pData The returned pin data bitwise-ANDed with \c mask.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS ftdiMpsseGpioRead(_In_ HFTDI h, _In_ BYTE mask, _Out_ PBYTE pData);

//! @brief Writes the high 8-bit GPIO pin values to a 16-bit FTDI device port using MPSSE mode.
//!
//! This function writes the high 8-bit GPIO pin values to a 16-bit FTDI device port configured
//! via ftdiModeSet() to MPSSE mode.  Refer to the data sheet of the
//! FTDI device for more details.
//!
//! MPSSE mode requires the direction of every pin on the port to be indicated for every write.
//!
//! \c pinsDirection is typically the same value used by ftdiModeSet(), but could
//! be different if the protocol requires a pin to change direction.
//!
//! \c mask indicates which bits of the port to write. This function maintains the
//! value of pins not indicated in \c mask.
//!
//! Use ftdiModeSet() first to configure MPSSE mode.
//!
//! @param[in] h              An open handle from ftdiOpenEx().
//! @param[in] pinsDirection  Each bit of this value sets the I/O direction of that bit on the port.
//!                           0 = input, 1 = output.
//! @param[in] mask           A value where each one bit indicates a port bit to change, leaving pins with a zero in the mask unchanged.
//! @param[in] data           The data to write to the port qualified by \c mask.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS ftdiMpsseGpioHighWrite(_In_ HFTDI h, _In_ BYTE pinsDirection, _In_ BYTE mask, _In_ BYTE data);

//! @brief Reads the high 8-bit GPIO pin values from a 16-bit FTDI device port using MPSSE mode.
//!
//! This function reads the high 8-bit GPIO pin values from a 16-bit FTDI device port configured
//! via ftdiModeSet() to MPSSE mode.  Refer to the data sheet of the
//! FTDI device for more details.
//!
//! \c mask is bitwise-ANDed with the values read so the returned data only
//! represents pins of interest.
//!
//! Use ftdiModeSet() first to configure MPSSE mode.
//!
//! @param[in] h      An open handle from ftdiOpenEx().
//! @param[in] mask   A value to bitwise-AND with the data read.
//! @param[out] pData The returned pin data bitwise-ANDed with \c mask.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS ftdiMpsseGpioHighRead(_In_ HFTDI h, _In_ BYTE mask, _Out_ PBYTE pData);

//! @brief Enables or disables loopback mode for an FTDI device port using MPSSE mode.
//!
//! This function enables loopback mode for an FTDI device port using MPSSE mode.
//!
//! Use ftdiModeSet() first to configure MPSSE mode.
//!
//! @param[in] h      An open handle from ftdiOpenEx().
//! @param[in] enable TRUE to enable, FALSE to disable.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS ftdiMpsseLoopbackEnable(_In_ HFTDI h, _In_ BOOL enable);

//! @brief Prepares for SPI transmission on an FTDI device port using MPSSE mode.
//!
//! This function prepares for SPI transmission on an FTDI device port using MPSSE mode.
//!
//! Use ftdiModeSet() first to configure MPSSE mode.
//!
//! @param[in] h            An open handle from ftdiOpenEx().
//! @param[in] clockDivisor The SPI transmission frequency is 60MHz / ((1 + divisor) * 2).
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS ftdiMpsseSpiInit(_In_ HFTDI h, _In_ INT clockDivisor);

//! @brief Sets the state of a single port bit on an FTDI device port using MPSSE mode.
//!
//! This function sets the state of a single port bit (typically used as a chip select) on an FTDI device port using MPSSE mode.
//! It requires as input the direction for all the bits on the port, which is typically
//! the same value used by ftdiModeSet() to initially configure the port.
//!
//! Use ftdiModeSet() first to configure MPSSE mode.
//!
//! @param[in] h              An open handle from ftdiOpenEx().
//! @param[in] pinsDirection  Each bit of this value sets the I/O direction of that bit on the port.
//!                           0 = input, 1 = output.
//! @param[in] csBitLocation  The bit to change on the port (0-7).
//! @param[in] csData         TRUE to drive a 1.  Note this \a disables an active low chip select.\n
//!                           FALSE to drive a 0.  Note this \a enables an active low chip select.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS ftdiMpsseSpiCsWrite(_In_ HFTDI h, _In_ BYTE pinsDirection, _In_ INT csBitLocation, _In_ BOOL csData);

//! @brief Writes a stream of bits via SPI to an FTDI device port using MPSSE mode.
//!
//! This function writes a stream of bits via SPI to an FTDI device port using MPSSE mode.
//! Note that the \c wrByteLength of the data buffer must be at least long enough to provide \c bitLength number of bits.
//! The data bytes are transmitted most significant bit (MSB) first.
//! @param[in] h            An open handle from ftdiOpenEx().
//! @param[in] edgeType     \ref FTDI_SPI_WR_POS or \ref FTDI_SPI_WR_NEG.
//! @param[in] pWrData      A data buffer of bytes to write via SPI.  Note that the bits within each byte of
//!                         \c pWrData are transmitted via SPI in the order of MSB down to LSB.
//! @param[in] wrByteLength The byte length of the \c pWrData buffer.
//! @param[in] bitLength    The specific number of bits to transmit.  \c wrByteLength*8 must be >= \c bitLength.  Bits in the data buffer exceeding
//!                         \c bitLength are not transmitted.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS ftdiMpsseSpiWrite(_In_ HFTDI h, _In_ INT edgeType, _In_reads_bytes_(wrByteLength) const void* pWrData, _In_ DWORD wrByteLength, _In_ DWORD bitLength);

//! @brief Reads a stream of bits via SPI from an FTDI device port using MPSSE mode.
//!
//! This function reads a stream of bits via SPI from an FTDI device port using MPSSE mode.
//! Note that the \c rdByteLength of the data buffer must be at least long enough to hold \c bitLength number of bits.
//! The data bytes are received most significant bit (MSB) first.
//! @param[in]  h             An open handle from ftdiOpenEx().
//! @param[in]  edgeType      \ref FTDI_SPI_RD_POS or \ref FTDI_SPI_RD_NEG.
//! @param[out] pRdData       A data buffer of bytes to hold the data read via SPI.  Note that the bits within each byte of
//!                           \c pRdData were received via SPI in the order of MSB down to LSB.
//! @param[in]  rdByteLength  The byte length of the \c pRdData buffer.
//! @param[in]  bitLength     The specific number of bits to receive.  \c rdByteLength*8 must be >= \c bitLength.  The final data byte with < 8 bits will have
//!                           the remaining least significant bits zeroed.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS ftdiMpsseSpiRead(_In_ HFTDI h, _In_ INT edgeType, _Out_writes_bytes_all_(rdByteLength) LPVOID pRdData, _In_ DWORD rdByteLength, _In_ DWORD bitLength);

//! @brief Writes and reads a stream of bits via SPI from an FTDI device port using MPSSE mode.
//!
//! This function writes and reads a stream of bits via SPI from an FTDI device port using MPSSE mode.
//! Depending on \c edgeType, data is clocked out on one edge of a clock and clocked in on the other edge.
//! Note that both \c wrByteLength and \c rdByteLength of the data buffers must be at least long enough to hold \c bitLength number of bits.
//! The data bytes are sent and received most significant bit (MSB) first.
//! @param[in]  h             An open handle from ftdiOpenEx().
//! @param[in]  edgeType      \ref FTDI_SPI_WR_NEG_RD_POS or \ref FTDI_SPI_WR_POS_RD_NEG.
//! @param[in]  pWrData       A data buffer of bytes to write via SPI.  Note that the bits within each byte of
//!                           \c pWrData are transmitted via SPI in the order of MSB down to LSB.
//! @param[in]  wrByteLength  The byte length of the \c pWrData buffer.
//! @param[out] pRdData       A data buffer of bytes to hold the data read via SPI.  Note that the bits within each byte of
//!                           \c pRdData were received via SPI in the order of MSB down to LSB.
//! @param[in]  rdByteLength  The byte length of the \c pRdData buffer.
//! @param[in]  bitLength     The specific number of bits to send and receive.  \c wrByteLength*8 and \c rdByteLength*8 must be >= \c bitLength.
//!                           Bits in \c pWrData exceeding \c bitLength are not transmitted.  A partial byte received will have
//!                           the remaining least significant bits zeroed.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS ftdiMpsseSpiParWriteRead(_In_ HFTDI h, _In_ INT edgeType, _In_reads_bytes_(wrByteLength) const void* pWrData, _In_ DWORD wrByteLength,
                                            _Out_writes_bytes_all_(rdByteLength) LPVOID pRdData, _In_ DWORD rdByteLength, _In_ DWORD bitLength);

//! @brief Returns the size of the user area of an FTDI's EEPROM.
//!
//! This function returns the size of the user area of an FTDI's EEPROM.
//!
//! @param[in]    h           An open handle from ftdiOpenEx().
//! @param[out]   pSize       The available size (in bytes) of the user area of the EEPROM.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS ftdiUserEepromSize(_In_ HFTDI h, _Out_ LPDWORD pSize);

//! @brief Writes a block of data to the user area of an FTDI's EEPROM.
//!
//! This function writes a block of data to the user area of an FTDI's EEPROM.
//!
//! @param[in]  h             An open handle from ftdiOpenEx().
//! @param[in]  pData         A buffer of bytes containing data to write to the user area of an FTDI's EEPROM.
//! @param[in]  length        The number of bytes to write to the EEPROM from the buffer pointed to by \c pData.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS ftdiUserEepromWrite(_In_ HFTDI h, _In_reads_bytes_(length) const void* pData, _In_ DWORD length);

//! @brief Reads a block of data from the user area of an FTDI's EEPROM.
//!
//! This function reads a block of data from the user area of an FTDI's EEPROM.
//!
//! @param[in]    h           An open handle from ftdiOpenEx().
//! @param[out]   pData       A pointer to the buffer into which the EEPROM data will be read.
//! @param[in,out] pLength    As an input, this indicates the available space of the buffer pointed to by pData. The function then
//!                           modifies the DWORD pointed to by \c pLength with the number of bytes read from the EEPROM. The expected
//!                           read length and the actual read length may differ due to EEPROM constraints.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS ftdiUserEepromRead(_In_ HFTDI h, _Out_writes_bytes_to_(_Old_(*pLength), *pLength) LPVOID pData, _Inout_ LPDWORD pLength);

// #########################################################################################
// ###################  Below this line is the I2C FTDI functions ##########################
// #########################################################################################

//! @brief Prepares for I2C transmission on an FTDI device port using MPSSE mode.
//!
//! This function prepares for I2C transmission on an FTDI device port using MPSSE mode.
//!
//! Use ftdiModeSet() first to configure MPSSE mode.
//!
//! @param[in]  h             An open handle from ftdiOpenEx().
//! @param[in]  i2cMode       The mode of operation for I2C bus, #FTDI_I2C_STANDARD_MODE or #FTDI_I2C_FAST_MODE.
//! @returns #HIL_STATUS
HIL_API HIL_STATUS ftdiMpsseI2cInit(_In_ HFTDI h, _In_ INT i2cMode);

//! @brief Performs an I2C Write transaction via an FTDI device port using MPSSE mode.
//!
//! This function performs an I2C Write transaction via an FTDI device port using MPSSE mode.
//! The data bytes are transmitted as described in the I2C specification.
//!
//! Use ftdiMpsseI2cInit() first to configure MPSSE I2C mode.
//!
//! @param[in]  h               An open handle from ftdiOpenEx().
//! @param[in]  i2cMode         The mode of operation for I2C bus, #FTDI_I2C_STANDARD_MODE or #FTDI_I2C_FAST_MODE.
//! @param[in]  portsDirection  This BYTE indicates the direction of each of the 8 pins on the FTDI port that relates to the handle used.
//!                             Where 1 = output and 0 = input.
//! @param[in]  address         This is the 7-bit address of the slave device being written to.
//! @param[out] pData           This is the BYTE array data to write to the slave device
//! @param[in]  length          This is the number of bytes to be written to the slave device
//! @returns #HIL_STATUS
HIL_API HIL_STATUS ftdiMpsseI2cWrite(_In_ HFTDI h, _In_ INT i2cMode, _In_ BYTE portsDirection, _In_ BYTE address, _In_reads_bytes_(length) const void* pData, _In_ DWORD length);

//! @brief Performs an I2C Read transaction via an FTDI device port using MPSSE mode.
//!
//! This function performs an I2C Read transaction via an FTDI device port using MPSSE mode.
//! The data bytes are received as described in the I2C specification.
//!
//! Use ftdiMpsseI2cInit() first to configure MPSSE I2C mode.
//!
//! @param[in]  h               An open handle from ftdiOpenEx().
//! @param[in]  i2cMode         The mode of operation for I2C bus, #FTDI_I2C_STANDARD_MODE or #FTDI_I2C_FAST_MODE.
//! @param[in]  portsDirection  This BYTE indicates the direction of each of the 8 pins on the FTDI port that relates to the handle used.
//!                             Where 1 = output and 0 = input.
//! @param[in]  address         This is the 7-bit address of the slave device being read from.
//! @param[out] pData           This is the BYTE array to contain the read data from the slave device
//! @param[in]  length          This is the number of bytes to be read from the slave device
//! @returns #HIL_STATUS
HIL_API HIL_STATUS ftdiMpsseI2cRead(_In_ HFTDI h, _In_ INT i2cMode, _In_ BYTE portsDirection, _In_ BYTE address, _Out_writes_bytes_all_(length) LPVOID pData, _In_ DWORD length);

//! @brief Performs an I2C combined Write then Read transaction via an FTDI device port using MPSSE mode.
//!
//! This function performs an I2C combined Write then Read transaction via an FTDI device port using MPSSE mode.
//! The data bytes are transmitted/received as described in the I2C specification.
//!
//! Use ftdiMpsseI2cInit() first to configure MPSSE I2C mode.
//!
//! @param[in]  h               An open handle from ftdiOpenEx().
//! @param[in]  i2cMode         The mode of operation for I2C bus, #FTDI_I2C_STANDARD_MODE or #FTDI_I2C_FAST_MODE.
//! @param[in]  portsDirection  This BYTE indicates the direction of each of the 8 pins on the FTDI port that relates to the handle used.
//!                             Where 1 = output and 0 = input.
//! @param[in]  address         This is the 7-bit address of the slave device being written to and read from.
//! @param[out] pWrData         This is the BYTE array data to write to the slave device
//! @param[in]  wrByteLength    This is the number of bytes to be written to the slave device
//! @param[out] pRdData         This is the BYTE array to contain the read data from the slave device
//! @param[in]  rdByteLength    This is the number of bytes to be read from the slave device
//! @returns #HIL_STATUS
HIL_API HIL_STATUS ftdiMpsseI2cWriteRead(_In_ HFTDI h, _In_ INT i2cMode, _In_ BYTE portsDirection, _In_ BYTE address,
                                         _In_reads_bytes_(wrByteLength) const void* pWrData, _In_ DWORD wrByteLength,
                                         _Out_writes_bytes_all_(rdByteLength) LPVOID pRdData, _In_ DWORD rdByteLength);

#ifdef __cplusplus
}
#endif
