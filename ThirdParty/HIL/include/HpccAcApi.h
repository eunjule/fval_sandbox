// INTEL CONFIDENTIAL
// Copyright 2014-2018 Intel Corporation All Rights Reserved.
//
// The source code contained or described herein and all documents related to
// the source code ("Material") are owned by Intel Corporation or its
// suppliers or licensors. Title to the Material remains with Intel
// Corporation or its suppliers and licensors. The Material contains trade
// secrets and proprietary and confidential information of Intel or its
// suppliers and licensors. The Material is protected by worldwide copyright
// and trade secret laws and treaty provisions. No part of the Material may be
// used, copied, reproduced, modified, published, uploaded, posted,
// transmitted, distributed, or disclosed in any way without Intel's prior
// express written permission.
//
// No license under any patent, copyright, trade secret or other intellectual
// property right is granted to or conferred upon you by disclosure or
// delivery of the Materials, either expressly, by implication, inducement,
// estoppel or otherwise. Any license under such intellectual property rights
// must be express and approved by Intel in writing.

//! @file
//! @brief APIs to control the HDMT HPCC-AC daughterboard.
#pragma once
#include "HilDefs.h"

#ifdef __cplusplus
extern "C" {
#endif

//! @brief Verifies the HDMT HPCC-AC daughterboard is present.
//!
//! This function verifies the HDMT Gen 2 digital channel card (HPCC-AC) is present.  It connects to and caches driver resources for use
//! by other \c hpccAcXXXXX functions.  hpccAcDisconnect() is its complementary function and frees any cached resources.
//!
//! Neither hpccAcConnect() or hpccAcDisconnect() are required to be called to use the other \c hpccAcXXXXX functions.  All
//! \c hpccAcXXXXX functions will allocate resources if they are not already allocated and free them if required to
//! perform their function.
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hpccAcConnect(_In_ INT slot);

//! @brief Frees any resources cached from using the HPCC-AC daughterboard functions.
//!
//! This function frees any resources associated with using the HPCC-AC daughterboard HIL functions. hpccAcConnect() is its
//! complementary function.
//!
//! Neither hpccAcConnect() or hpccAcDisconnect() are required to be called to use the other \c hpccAcXXXXX functions.  All
//! \c hpccAcXXXXX functions will allocate resources if they are not already allocated and free them if required to
//! perform their function.
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hpccAcDisconnect(_In_ INT slot);

//! @brief Programs the vendor and product IDs of all USB devices on an HPCC-AC daughterboard.
//!
//! This function programs the vendor and product IDs (VID/PIDs) of the FT4232 and FT240X
//! USB devices on an HPCC-AC daughterboard.  Correct VID/PIDs are required to ensure Windows Device
//! Manager displays a proper description of the device, and HIL requires correct VID/PIDs to ensure
//! software can locate and communicate with the correct component.
//! @note FTDI devices have a limited number of updates, but this function is safe to call because it
//! only writes if the component programming is incorrect.
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hpccAcVidPidsSet(_In_ INT slot);

//! @brief Reads the HPCC-AC daughterboard FPGA version numbers.
//!
//! This function reads the version number from one of the two FPGAs on the HPCC-AC daughterboard.  The returned 32-bit value is in the
//! format 0xMMMMmmHW where the upper 16 bits are the major version, next 8 bits are minor version and last 8 bits indicate
//! a hardware revision.  If HW bits are zero, it indicates an old format where the major version indicates hardware revision.
//!
//! | HW value | Meaning                | Example    | Version | Comment           |
//! | :------: | :--------------------- | :--------: | :-----: | :-----------------|
//! |    00    | See Major for HW value | 0x00014500 | 1.45.0  | Fab C (old style) |
//! |    00    | See Major for HW value | 0x00024500 | 2.45.0  | Fab D (old style) |
//! |    01    | HPCC AC Fab C          | 0x00030001 | 3.0.1   |                   |
//! |    02    | HPCC AC Fab D          | 0x00040002 | 4.0.2   |                   |
//!
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] fpgaIndex Selects one of the two FPGAs: 0 or 1.
//! @param[out] pVersion The address of a DWORD of memory.  It may not be NULL.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hpccAcFpgaVersion(_In_ INT slot, _In_ INT fpgaIndex, _Out_ LPDWORD pVersion);

//! @brief Reads an HPCC-AC daughterboard's CPLD version number.
//!
//! This function reads the CPLD version number from an HPCC-AC daughterboard.
//!
//! @param[in] slot A physical slot number.  Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[out] pVersion Returns the version register value.  The pointer must not be NULL.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hpccAcCpldVersion(_In_ INT slot, _Out_ LPDWORD pVersion);

//! @brief Reads an HPCC-AC daughterboard's CPLD version string.
//!
//! This function reads the version register of an HPCC-AC daughterboard's CPLD and converts it to an ASCII string representation.
//!
//! @param[in] slot A physical slot number.  Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[out] pVersion Returns the version string.  The pointer must not be NULL.
//! @param[in] length The length of the \c pVersion buffer.  It should be at least five characters to hold a null-terminated version string.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hpccAcCpldVersionString(_In_ INT slot, _Out_writes_z_(length) LPSTR pVersion, _In_ DWORD length);

//! @brief Disables PCI device drivers for the HPCC-AC FPGAs in a slot.
//!
//! This function disables PCI device drivers for the HPCC-AC FPGAs in a slot.  It should be called before using functions
//! such as hpccAcJtagExecute() and hpccAcFpgaLoad() that affect the hardware used by the drivers.
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] fpgaIndex Selects one of the two FPGAs: 0 or 1.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hpccAcDeviceDisable(_In_ INT slot, _In_ INT fpgaIndex);

//! @brief Enables PCI device drivers for the HPCC-AC FPGAs in a slot.
//!
//! This function enables PCI device drivers for the HPCC-AC FPGAs in a slot.  It should be called after using functions
//! such as hpccAcJtagExecute() and hpccAcFpgaLoad() that affect the hardware used by the drivers.
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] fpgaIndex Selects one of the two FPGAs: 0 or 1.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hpccAcDeviceEnable(_In_ INT slot, _In_ INT fpgaIndex);

//! @brief Loads an FPGA binary image file into an HPCC-AC daughterboard FPGA.
//!
//! This function loads an FPGA binary image file into an HPCC-AC daughterboard FPGA.  This image is volatile and the FPGA
//! will load its original base program from its SPI FLASH after a cold reboot.
//! @warning HPCC-AC device drivers are sensitive to FPGA changes.  If they are present, wrap this call in hpccAcDeviceDisable() and hpccAcDeviceEnable().
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] fpgaIndex Selects one of the two FPGAs: 0 or 1.
//! @param[in] filename An ANSI string containing an absolute or relative (to the current directory) FPGA binary filename.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hpccAcFpgaLoad(_In_ INT slot, _In_ INT fpgaIndex, _In_z_ LPCSTR filename);

//! @brief Programs an FPGA image file into the base SPI FLASH used to boot initialize an HPCC-AC daughterboard FPGA.
//!
//! This function programs an FPGA image file into the base SPI FLASH used to boot initialize an HPCC-AC daughterboard FPGA.
//! The base SPI FLASH data file can be a maximum of 32MB in size.
//!
//! The callback and context parameters are used to report progress back during a long operation.  See #HIL_BASE_LOAD_CALLBACK
//! for details.
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] fpgaIndex Selects the appropriate SPI FLASH for one of the two FPGAs: 0 or 1.
//! @param[in] filename An ANSI string containing an absolute or relative (to the current directory) FPGA binary filename.
//! @param[in] callback The callback function to be called with progress updates while programming the SPI FLASH.  Value can be NULL.
//! @param[in] context The context that will be passed back as an argument in the #HIL_BASE_LOAD_CALLBACK callback function.  Value can be NULL.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hpccAcFpgaBaseLoad(_In_ INT slot, _In_ INT fpgaIndex, _In_z_ LPCSTR filename, _In_opt_ HIL_BASE_LOAD_CALLBACK callback, _In_opt_ void* context);

//! @brief Compares an existing image within the base SPI FLASH on an HPCC-AC daughterboard against an FPGA binary image file.
//!
//! This function compares an existing image within the base SPI FLASH on an HPCC-AC daughterboard against an FPGA binary image file
//! and optionally dumps that image into a separate binary file.
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] fpgaIndex Selects one of the two FPGAs: 0 or 1.
//! @param[in] imageFilename An ANSI string containing an absolute or relative (to the current directory) FPGA binary filename to be compared against.
//! @param[in] dumpFilename An ANSI string containing an absolute or relative (to the current directory) binary dump filename.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hpccAcFpgaBaseVerify(_In_ INT slot, _In_ INT fpgaIndex, _In_z_ LPCSTR imageFilename, _In_opt_z_ LPCSTR dumpFilename);

//! @brief Executes a Xilinx .XSVF file over an HPCC-AC daughterboard's JTAG interface.
//!
//! This function executes a Xilinx .XSVF file over an HPCC-AC daughterboard's JTAG interface.  A Xilinx CPLD is the only device
//! in the scan chain.  An appropriate .XSVF file can be generated through Xilinx's iMPACT software to perform
//! functions such as programming and/or verifying the content of the CPLD.
//! @warning HPCC-AC FPGAs are sensitive to CPLD changes.  If the HPCC-AC drivers are present, use the following sequence:
//! 1. hpccAcDeviceDisable().
//! 2. hpccAcJtagExecute() to load the CPLD, which hangs the FPGAs.
//! 3. hpccAcFpgaLoad() on each FPGA to recover them.
//! 4. hpccAcDeviceEnable().
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] filename An ANSI string containing an absolute or relative (to the current directory) CPLD .XSVF binary filename.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hpccAcJtagExecute(_In_ INT slot, _In_z_ LPCSTR filename);

//! @brief Loads an FPGA binary image buffer into an HPCC-AC daughterboard FPGA.
//!
//! This function loads an FPGA binary image buffer into an HPCC-AC daughterboard FPGA.  This image is volatile and the FPGA
//! will load its original base program from its SPI FLASH after a cold reboot.
//! @warning HPCC-AC device drivers are sensitive to FPGA changes.  If they are present, wrap this call in hpccAcDeviceDisable() and hpccAcDeviceEnable().
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] fpgaIndex Selects one of the two FPGAs: 0 or 1.
//! @param[in] pData The FPGA image data.
//! @param[in] length The length of the FPGA image data in bytes.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hpccAcFpgaBufferLoad(_In_ INT slot, _In_ INT fpgaIndex, _In_reads_bytes_(length) LPCVOID pData, _In_ DWORD length);

//! @brief Programs an FPGA image buffer into the base SPI FLASH used to boot initialize an HPCC-AC daughterboard FPGA.
//!
//! This function programs an FPGA image buffer into the base SPI FLASH used to boot initialize an HPCC-AC daughterboard FPGA.
//! The base SPI FLASH data file can be a maximum of 32MB in size.
//!
//! The callback and context parameters are used to report progress back during a long operation.  See #HIL_BASE_LOAD_CALLBACK
//! for details.
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] fpgaIndex Selects the appropriate SPI FLASH for one of the two FPGAs: 0 or 1.
//! @param[in] pData The FPGA image data.
//! @param[in] length The length of the FPGA image data in bytes.
//! @param[in] callback The callback function to be called with progress updates while programming the SPI FLASH.  Value can be NULL.
//! @param[in] context The context that will be passed back as an argument in the #HIL_BASE_LOAD_CALLBACK callback function.  Value can be NULL.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hpccAcFpgaBaseBufferLoad(_In_ INT slot, _In_ INT fpgaIndex, _In_reads_bytes_(length) LPCVOID pData, _In_ DWORD length, _In_opt_ HIL_BASE_LOAD_CALLBACK callback, _In_opt_ void* context);

//! @brief Execute a buffer containing Xilinx XSVF data over an HPCC-AC daughterboard's JTAG interface.
//!
//! This function executes a Xilinx XSVF data buffer over an HPCC-AC daughterboard's JTAG interface.  A Xilinx CPLD
//! is the only device in the scan chain.  XSVF data can be generated through Xilinx's iMPACT software to perform
//! functions such as programming and/or verifying the content of the CPLD.
//! @warning HPCC-AC FPGAs are sensitive to CPLD changes.  If the HPCC-AC drivers are present, use the following sequence:
//! 1. hpccAcDeviceDisable().
//! 2. hpccAcJtagExecute() to load the CPLD, which hangs the FPGAs.
//! 3. hpccAcFpgaLoad() on each FPGA to recover them.
//! 4. hpccAcDeviceEnable().
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] pData The CPLD image data.
//! @param[in] length The length of the CPLD image data in bytes.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hpccAcJtagBufferExecute(_In_ INT slot, _In_reads_bytes_(length) LPCVOID pData, _In_ DWORD length);

//! @brief Reads a 32-bit register in one of the BAR regions of an HPCC-AC daughterboard.
//!
//! This function reads a 32-bit register in one of the BAR regions of an HPCC-AC daughterboard.
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] fpgaIndex Selects one of the two FPGAs: 0 or 1.
//! @param[in] bar The PCI base address register region to access.  Currently only 0 is supported.
//! @param[in] offset The DWORD-aligned offset address in BAR memory to read.
//! @param[out] pData The address of a DWORD to contain the result.  It cannot be NULL.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hpccAcBarRead(_In_ INT slot, _In_ INT fpgaIndex, _In_ UINT bar, _In_ DWORD offset, _Out_ LPDWORD pData);

//! @brief Writes a 32-bit register in one of the BAR regions of an HPCC-AC daughterboard.
//!
//! This function writes a 32-bit register in one the BAR regions of an HPCC-AC daughterboard.
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] fpgaIndex Selects one of the two FPGAs: 0 or 1.
//! @param[in] bar The PCI base address register region to access.  Currently only 0 is supported.
//! @param[in] offset The DWORD-aligned offset address in BAR memory to write.
//! @param[in] data The DWORD value to write.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hpccAcBarWrite(_In_ INT slot, _In_ INT fpgaIndex, _In_ UINT bar, _In_ DWORD offset, _In_ DWORD data);

//! @brief Reads from the DDR memory of an HPCC-AC daughterboard.
//!
//! This function reads from the DDR memory of an HPCC-AC daughterboard.
//!
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] fpgaIndex Selects one of the two FPGAs: 0 or 1.
//! @param[in] address The zero-based address within the DDR memory to read.
//! @param[out] pData The address of a buffer to hold the result.
//! @param[in] length The length of the \c pData buffer in bytes.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hpccAcDmaRead(_In_ INT slot, _In_ INT fpgaIndex, _In_ INT64 address, _Out_writes_bytes_all_(length) LPVOID pData, _In_ DWORD length);

//! @brief Writes to the DDR memory of an HPCC-AC daughterboard.
//!
//! This function writes to the DDR memory of an HPCC-AC daughterboard.
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] fpgaIndex Selects one of the two FPGAs: 0 or 1.
//! @param[in] address The zero-based address within the DDR memory to write.
//! @param[in] pData The address of a buffer containing the data to write.
//! @param[in] length The length of the \c pData buffer in bytes.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hpccAcDmaWrite(_In_ INT slot, _In_ INT fpgaIndex, _In_ INT64 address, _In_reads_bytes_(length) const void* pData, _In_ DWORD length);

//! @brief Reads the board-level traceability values from an HPCC-AC daughterboard.
//!
//! This function reads the board-level traceability values from a HPCC-AC daughterboard component.  The values are defined in the #BLT structure.
//! This data refers to the AC daughterboard component only, and not the assembled instrument.
//! Use hpccBltInstrumentRead() to read the BLT of the collective instrument.
//!
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[out] pBlt The address of a #BLT structure that will be populated with the data.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hpccAcBltBoardRead(_In_ INT slot, _Out_ PBLT pBlt);

//! @brief Writes the board-level traceability values for an HPCC-AC daughterboard.
//!
//! This function writes the board-level traceability values for an HPCC-AC daughterboard component.  The values are defined in the #BLT structure.
//! This data refers to the AC daughterboard component only, and not the assembled instrument.
//! Use hpccBltInstrumentWrite() to write the BLT of the collective instrument.
//!
//! The \c flags field allows this function to update individual values.  The flags can be ORed together.  Only the fields indicated by
//! \c flags need to be populated in the provided \c pBlt parameter.
//!
//! \par Updating the Full BLT - C
//! \code{.c}
//! #include <Windows.h>
//! #include "HpccAcApi.h"
//! #include "string.h"
//!
//! int main()
//! {
//!     int slot = 8;
//!     BLT blt;
//!     HIL_STATUS status;
//!     blt.Id = 0x10000037;
//!     strcpy(blt.DeviceName,"DeviceName");
//!     strcpy(blt.VendorName,"INTEL");
//!     strcpy(blt.PartNumberAsBuilt,"AAxxxxxx-xxx");
//!     strcpy(blt.PartNumberCurrent,blt.PartNumberAsBuilt);
//!     strcpy(blt.SerialNumber,"12345678");
//!     strcpy(blt.ManufactureDate,"YYMMDD");
//!     status = hpccAcBltBoardWrite(slot, &blt, BF_ALL);
//!     if (status != HS_SUCCESS)
//!         return -1;
//!     return 0;
//! }
//! \endcode
//! \par Updating the Current Part Number - Python
//! \code{.py}
//! slot = 8
//! blt = hil.BLT()
//! blt.PartNumberCurrent = 'AAyyyyyy-yyy'
//! hil.hpccAcBltBoardWrite(slot,blt,hil.BF_PART_NUMBER_CURRENT)
//! # Note Python returns None for HIL_STATUS if successful and raises RuntimeError on failure.
//! \endcode
//!
//! \note HS_BLT_NOT_FOUND can be returned on an update (lack of #BF_ALL flag).
//!
//! @param[in] slot A physical slot number. Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[in] pBlt The address of a #BLT structure that contains the data to be written or updated.
//! @param[in] flags #BLT_FLAGS bits indicating what fields in \c pBlt are valid.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hpccAcBltBoardWrite(_In_ INT slot, _In_ PCBLT pBlt, _In_ BLT_FLAGS flags);

//! @brief Reads the raw data from the specified HPCC-AC daughterboard's BLT EEPROM.
//!
//! This function reads the raw data from the specified HPCC-AC daughterboard's BLT EEPROM.
//!
//! @param[in] slot A physical slot number.  Valid values are 0 to \ref HIL_SLOTS - 1.
//! @param[out] pEepromData The output buffer.
//! @param[in,out] pLength On input, the size of the output buffer referenced by \c pData.  On output, the size of the EEPROM data.
//! @retval HS_INSUFFICIENT_BUFFER The buffer provided was too small.  \c pLength will indicate the minimum buffer size required.
//! @returns \ref HIL_STATUS
HIL_API HIL_STATUS hpccAcBltEepromRead(_In_ INT slot, _Out_writes_bytes_to_(_Old_(*pLength), *pLength) LPVOID pEepromData, _Inout_ LPDWORD pLength);

#ifdef __cplusplus
}
#endif
