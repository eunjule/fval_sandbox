
from contextlib import contextmanager
import getpass
import os
import shutil
import sys  


from Common import fval

if getpass.getuser() == 'SysC':
    dir = os.path.join("C:\\","FVAL_python3_6_client")
    if not os.path.exists(dir):
        os.makedirs(dir)
        current_path = os.getcwd()
        path_to_SASS_dir = current_path.replace("Tools","ThirdParty\SASS")
        shutil.copy(path_to_SASS_dir + '\\' + '_sass.pyd',dir)
        shutil.copy(path_to_SASS_dir + '\\' + 'sass.py',dir)
        shutil.copy(path_to_SASS_dir + '\\' + 'SassApi.dll',dir)
        shutil.copy(path_to_SASS_dir + '\\' + 'SassService.dll',dir)
        shutil.copy(path_to_SASS_dir + '\\' + 'SassToSassApi.dll',dir)
        shutil.copy(path_to_SASS_dir + '\\' + 'SassToSassService.dll',dir)
   
sys.path.append('C:/FVAL_python3_6_client')

try:
    import sass
except ModuleNotFoundError:
    from Common import sass_simulator as sass

_sass_version = None
def sass_version_string():
    global _sass_version
    if _sass_version is None:
        sass_api = sass.SassApi()
        sass_api.Connect('FVAL Get SASS Version')
        _sass_version = sass_api.GetServerVersion()
        sass_api.Disconnect()
    return f'{_sass_version.major}.{_sass_version.minor}.' \
           f'{_sass_version.revision}'


@contextmanager
def suppress_sensor(client_name, sensor_names):
    """A context manager for suppressing sensors

    Upon connecting the SassApi client, sensors for the sensor/sensor group are
    suppressed. On exiting the with-statement, the sensors are resumed.

    client_name -- the name to give this client (for logging)
    sensor_name -- the SASS sensor/sensor group name to suppress

    yield -- a handle to the SassApi client

    Example:

    with suppress_sensor("FVAL", "HBIMainBoard") as sass_api:
        hil.hbiMbFpgaLoad(1, "patgen_top.rbf")

    with suppress_sensor("FVAL", "HBIPSDB") as sass_api:
        hil.hbiPsdbFpgaLoad(0, 0, "pin_multiplier0_top.rbf")
    """

    if isinstance(sensor_names, str):
        sensor_names = [sensor_names]

    sass_api = sass.SassApi()

    sass_api.Connect(client_name)
    try:
        [sass_api.SuppressSensor(sensor_name) for sensor_name in sensor_names]
        yield sass_api
    finally:
        [sass_api.ResumeSensor(sensor_name) for sensor_name in sensor_names]
        try:
            sass_api.Disconnect()
        except RuntimeError as e:
            fval.ConfigLogger()
            fval.Log('warning', e)
