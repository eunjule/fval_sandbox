# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

from time import sleep

from Common.fval import Log, Object
from Common.hilmon import hil
from Rc3.instrument.rc3_register import LMK04832_COMMAND,\
    LMK04832_READ_DATA, LMK04832_RESET, LMK04832_STATUS

READ_TIMEOUT = 10


class Lmk04832(Object):
    ID_REGISTERS = {0x03: 0x06,  # ID_DEVICE_TYPE
                    0x0C: 0x51,  # ID_VNDR_UPPER
                    0x0D: 0x04}  # ID_VNDR_LOWER

    def __init__(self, rc):
        super().__init__()
        self.rc = rc

    def name(self):
        return self.__class__.__name__

    @staticmethod
    def ClocksInit(rc, force=False):
        if not force and rc.read_lmk04832_using_hil(0x148) == 0x33:
            Log('info', 'RCTC3 Clocks already initialized')
            return  # Already programmed
        else:
            rc.initialize_ad5064()
            sleep(0.1)

            if rc.get_fab_letter().upper() == 'A':
                Lmk04832._ClocksInitA(rc)
            elif rc.get_fab_letter().upper() == 'B':
                Lmk04832._ClocksInitB(rc)

    @staticmethod
    def _ClocksInitB(rc):
        Log('info', f'Initializing RCTC3 Fab_B clock...')
        rc.write_lmk04832_using_hil(0x0000, 0x90)
        sleep(.5)
        rc.write_lmk04832_using_hil(0x0000, 0x10)
        rc.write_lmk04832_using_hil(0x0002, 0x00)
        rc.write_lmk04832_using_hil(0x0003, 0x06)
        rc.write_lmk04832_using_hil(0x0004, 0xD1)
        rc.write_lmk04832_using_hil(0x0005, 0x63)
        rc.write_lmk04832_using_hil(0x0006, 0x50)
        rc.write_lmk04832_using_hil(0x000C, 0x51)
        rc.write_lmk04832_using_hil(0x000D, 0x04)
        rc.write_lmk04832_using_hil(0x0100, 0x18)  # 7:0 = DCLK0_1_DIV[7:0] (0x018 = 125MHz)
        rc.write_lmk04832_using_hil(0x0101, 0x0A)
        rc.write_lmk04832_using_hil(0x0102, 0x60)  # 1:0 = DCLK0_1_DIV[9:8]
        rc.write_lmk04832_using_hil(0x0103, 0x40)
        rc.write_lmk04832_using_hil(0x0104, 0x1C)
        rc.write_lmk04832_using_hil(0x0105, 0x00)
        rc.write_lmk04832_using_hil(0x0106, 0x01)
        rc.write_lmk04832_using_hil(0x0107, 0x05)
        rc.write_lmk04832_using_hil(0x0108, 0x60)  # 7:0 = DCLK2_3_DIV[7:0] (0x060 = 31.25MHz)
        rc.write_lmk04832_using_hil(0x0109, 0x0A)
        rc.write_lmk04832_using_hil(0x010A, 0x60)  # 1:0 = DCLK2_3_DIV[9:8]
        rc.write_lmk04832_using_hil(0x010B, 0x40)
        rc.write_lmk04832_using_hil(0x010C, 0x10)
        rc.write_lmk04832_using_hil(0x010D, 0x00)
        rc.write_lmk04832_using_hil(0x010E, 0x01)
        rc.write_lmk04832_using_hil(0x010F, 0x55)
        rc.write_lmk04832_using_hil(0x0110, 0x18)  # 7:0 = DCLK4_5_DIV[7:0] (0x018 = 125MHz)
        rc.write_lmk04832_using_hil(0x0111, 0x0A)
        rc.write_lmk04832_using_hil(0x0112, 0x60)  # 1:0 = DCLK4_5_DIV[9:8]
        rc.write_lmk04832_using_hil(0x0113, 0x44)
        rc.write_lmk04832_using_hil(0x0114, 0x10)
        rc.write_lmk04832_using_hil(0x0115, 0x00)
        rc.write_lmk04832_using_hil(0x0116, 0x01)
        rc.write_lmk04832_using_hil(0x0117, 0x55)
        rc.write_lmk04832_using_hil(0x0118, 0x18)  # 7:0 = DCLK6_7_DIV[7:0] (0x018 = 125MHz)
        rc.write_lmk04832_using_hil(0x0119, 0x0A)
        rc.write_lmk04832_using_hil(0x011A, 0x60)  # 1:0 = DCLK6_7_DIV[9:8]
        rc.write_lmk04832_using_hil(0x011B, 0x40)
        rc.write_lmk04832_using_hil(0x011C, 0x1C)
        rc.write_lmk04832_using_hil(0x011D, 0x00)
        rc.write_lmk04832_using_hil(0x011E, 0x01)
        rc.write_lmk04832_using_hil(0x011F, 0x05)
        rc.write_lmk04832_using_hil(0x0120, 0x2C)  # 7:0 = DCLK8_9_DIV[7:0] (0x12C = 10MHz)
        rc.write_lmk04832_using_hil(0x0121, 0x0A)
        rc.write_lmk04832_using_hil(0x0122, 0x61)  # 1:0 = DCLK8_9_DIV[9:8]
        rc.write_lmk04832_using_hil(0x0123, 0x40)
        rc.write_lmk04832_using_hil(0x0124, 0x10)
        rc.write_lmk04832_using_hil(0x0125, 0x00)
        rc.write_lmk04832_using_hil(0x0126, 0x01)
        rc.write_lmk04832_using_hil(0x0127, 0x55)
        rc.write_lmk04832_using_hil(0x0128, 0x58)  # 7:0 = DCLK10_11_DIV[7:0] (0x258 = 5MHz)
        rc.write_lmk04832_using_hil(0x0129, 0x0A)
        rc.write_lmk04832_using_hil(0x012A, 0xE2)  # 1:0 = DCLK10_11_DIV[9:8]
        rc.write_lmk04832_using_hil(0x012B, 0x40)
        rc.write_lmk04832_using_hil(0x012C, 0x10)
        rc.write_lmk04832_using_hil(0x012D, 0x00)
        rc.write_lmk04832_using_hil(0x012E, 0x01)
        rc.write_lmk04832_using_hil(0x012F, 0x50)
        rc.write_lmk04832_using_hil(0x0130, 0x1E)  # 7:0 = DCLK12_13_DIV[7:0] (0x01E = 100MHz)
        rc.write_lmk04832_using_hil(0x0131, 0x0A)
        rc.write_lmk04832_using_hil(0x0132, 0x60)  # 1:0 = DCLK12_13_DIV[9:8]
        rc.write_lmk04832_using_hil(0x0133, 0x44)
        rc.write_lmk04832_using_hil(0x0134, 0x10)
        rc.write_lmk04832_using_hil(0x0135, 0x00)
        rc.write_lmk04832_using_hil(0x0136, 0x01)
        rc.write_lmk04832_using_hil(0x0137, 0x55)
        rc.write_lmk04832_using_hil(0x0138, 0x30)
        rc.write_lmk04832_using_hil(0x0139, 0x00)
        rc.write_lmk04832_using_hil(0x013A, 0x0C)
        rc.write_lmk04832_using_hil(0x013B, 0x00)
        rc.write_lmk04832_using_hil(0x013C, 0x00)
        rc.write_lmk04832_using_hil(0x013D, 0x08)
        rc.write_lmk04832_using_hil(0x013E, 0x03)
        rc.write_lmk04832_using_hil(0x013F, 0x09)
        rc.write_lmk04832_using_hil(0x0140, 0x0F)
        rc.write_lmk04832_using_hil(0x0141, 0x00)
        rc.write_lmk04832_using_hil(0x0142, 0x00)
        rc.write_lmk04832_using_hil(0x0143, 0x11)
        rc.write_lmk04832_using_hil(0x0144, 0x00)
        rc.write_lmk04832_using_hil(0x0145, 0x00)
        rc.write_lmk04832_using_hil(0x0146, 0x38)
        rc.write_lmk04832_using_hil(0x0147, 0x06)  # 0x06 for CLKIN0 (10MHZ), 0x26 for CLKIN2 (5MHz HIPI)
        rc.write_lmk04832_using_hil(0x0148, 0x33)  # 0x33 selects READBACK
        rc.write_lmk04832_using_hil(0x0149, 0x42)
        rc.write_lmk04832_using_hil(0x014A, 0x03)
        rc.write_lmk04832_using_hil(0x014B, 0x06)
        rc.write_lmk04832_using_hil(0x014C, 0x00)
        rc.write_lmk04832_using_hil(0x014D, 0x00)
        rc.write_lmk04832_using_hil(0x014E, 0xC0)
        rc.write_lmk04832_using_hil(0x014F, 0x7F)
        rc.write_lmk04832_using_hil(0x0150, 0x01)
        rc.write_lmk04832_using_hil(0x0151, 0x02)
        rc.write_lmk04832_using_hil(0x0152, 0x00)
        rc.write_lmk04832_using_hil(0x0153, 0x00)
        rc.write_lmk04832_using_hil(0x0154, 0x02)
        rc.write_lmk04832_using_hil(0x0155, 0x00)
        rc.write_lmk04832_using_hil(0x0156, 0x78)
        rc.write_lmk04832_using_hil(0x0157, 0x00)
        rc.write_lmk04832_using_hil(0x0158, 0x96)
        rc.write_lmk04832_using_hil(0x0159, 0x00)
        rc.write_lmk04832_using_hil(0x015A, 0x19)
        rc.write_lmk04832_using_hil(0x015B, 0xD4)
        rc.write_lmk04832_using_hil(0x015C, 0x20)
        rc.write_lmk04832_using_hil(0x015D, 0x00)
        rc.write_lmk04832_using_hil(0x015E, 0x1E)
        rc.write_lmk04832_using_hil(0x015F, 0x0B)
        rc.write_lmk04832_using_hil(0x0160, 0x00)
        rc.write_lmk04832_using_hil(0x0161, 0x01)
        rc.write_lmk04832_using_hil(0x0162, 0x4D)
        rc.write_lmk04832_using_hil(0x0163, 0x00)
        rc.write_lmk04832_using_hil(0x0164, 0x00)
        rc.write_lmk04832_using_hil(0x0165, 0x06)
        rc.write_lmk04832_using_hil(0x0169, 0x58)
        rc.write_lmk04832_using_hil(0x016A, 0x20)
        rc.write_lmk04832_using_hil(0x016B, 0x00)
        rc.write_lmk04832_using_hil(0x016C, 0x00)
        rc.write_lmk04832_using_hil(0x016D, 0x00)
        rc.write_lmk04832_using_hil(0x016E, 0x13)
        rc.write_lmk04832_using_hil(0x0173, 0x10)
        rc.write_lmk04832_using_hil(0x0177, 0x00)
        rc.write_lmk04832_using_hil(0x0182, 0x00)
        rc.write_lmk04832_using_hil(0x0183, 0x00)
        rc.write_lmk04832_using_hil(0x0166, 0x00)
        rc.write_lmk04832_using_hil(0x0167, 0x00)
        rc.write_lmk04832_using_hil(0x0168, 0x06)
        rc.write_lmk04832_using_hil(0x0555, 0x00)
        Log('info', f'Rc3 Fab_B clock initialized!')

    @staticmethod
    def _ClocksInitA(rc):
        Log('info', f'Initializing Rc3 Fab_A clock...')
        rc.write_lmk04832_using_hil(0x0000, 0x90)
        sleep(.5)
        rc.write_lmk04832_using_hil(0x0000, 0x10)
        rc.write_lmk04832_using_hil(0x0002, 0x00)
        rc.write_lmk04832_using_hil(0x0003, 0x06)
        rc.write_lmk04832_using_hil(0x0004, 0xD1)
        rc.write_lmk04832_using_hil(0x0005, 0x63)
        rc.write_lmk04832_using_hil(0x0006, 0x50)
        rc.write_lmk04832_using_hil(0x000C, 0x51)
        rc.write_lmk04832_using_hil(0x000D, 0x04)
        rc.write_lmk04832_using_hil(0x0100, 0x18)  # 7:0 = DCLK0_1_DIV[7:0] (0x018 = 125MHz)
        rc.write_lmk04832_using_hil(0x0101, 0x0A)
        rc.write_lmk04832_using_hil(0x0102, 0x00)  # 1:0 = DCLK0_1_DIV[9:8]
        rc.write_lmk04832_using_hil(0x0103, 0x40)
        rc.write_lmk04832_using_hil(0x0104, 0x10)
        rc.write_lmk04832_using_hil(0x0105, 0x00)
        rc.write_lmk04832_using_hil(0x0106, 0x01)
        rc.write_lmk04832_using_hil(0x0107, 0x55)
        rc.write_lmk04832_using_hil(0x0108, 0x2C)  # 7:0 = DCLK2_3_DIV[7:0] (0x12C = 10MHz)
        rc.write_lmk04832_using_hil(0x0109, 0x0A)
        rc.write_lmk04832_using_hil(0x010A, 0x01)  # 1:0 = DCLK2_3_DIV[9:8]
        rc.write_lmk04832_using_hil(0x010B, 0x40)
        rc.write_lmk04832_using_hil(0x010C, 0x10)
        rc.write_lmk04832_using_hil(0x010D, 0x00)
        rc.write_lmk04832_using_hil(0x010E, 0x01)
        rc.write_lmk04832_using_hil(0x010F, 0x55)
        rc.write_lmk04832_using_hil(0x0110, 0x1E)  # 7:0 = DCLK4_5_DIV[7:0] (0x01E = 100MHz)
        rc.write_lmk04832_using_hil(0x0111, 0x0A)
        rc.write_lmk04832_using_hil(0x0112, 0x00)  # 1:0 = DCLK4_5_DIV[9:8]
        rc.write_lmk04832_using_hil(0x0113, 0x40)
        rc.write_lmk04832_using_hil(0x0114, 0x10)
        rc.write_lmk04832_using_hil(0x0115, 0x00)
        rc.write_lmk04832_using_hil(0x0116, 0x01)
        rc.write_lmk04832_using_hil(0x0117, 0x55)
        rc.write_lmk04832_using_hil(0x0118, 0x18)  # 7:0 = DCLK6_7_DIV[7:0] (0x018 = 125MHz)
        rc.write_lmk04832_using_hil(0x0119, 0x0A)
        rc.write_lmk04832_using_hil(0x011A, 0x00)  # 1:0 = DCLK6_7_DIV[9:8]
        rc.write_lmk04832_using_hil(0x011B, 0x40)
        rc.write_lmk04832_using_hil(0x011C, 0x10)
        rc.write_lmk04832_using_hil(0x011D, 0x00)
        rc.write_lmk04832_using_hil(0x011E, 0x01)
        rc.write_lmk04832_using_hil(0x011F, 0x55)
        rc.write_lmk04832_using_hil(0x0120, 0x60)  # 7:0 = DCLK8_9_DIV[7:0] (0x060 = 31.25MHz)
        rc.write_lmk04832_using_hil(0x0121, 0x0A)
        rc.write_lmk04832_using_hil(0x0122, 0x00)  # 1:0 = DCLK8_9_DIV[9:8]
        rc.write_lmk04832_using_hil(0x0123, 0x40)
        rc.write_lmk04832_using_hil(0x0124, 0x10)
        rc.write_lmk04832_using_hil(0x0125, 0x00)
        rc.write_lmk04832_using_hil(0x0126, 0x01)
        rc.write_lmk04832_using_hil(0x0127, 0x55)
        rc.write_lmk04832_using_hil(0x0128, 0x58)  # 7:0 = DCLK10_11_DIV[7:0] (0x258 = 5MHz)
        rc.write_lmk04832_using_hil(0x0129, 0x0A)
        rc.write_lmk04832_using_hil(0x012A, 0x02)  # 1:0 = DCLK10_11_DIV[9:8]
        rc.write_lmk04832_using_hil(0x012B, 0x40)
        rc.write_lmk04832_using_hil(0x012C, 0x10)
        rc.write_lmk04832_using_hil(0x012D, 0x00)
        rc.write_lmk04832_using_hil(0x012E, 0x01)
        rc.write_lmk04832_using_hil(0x012F, 0x55)
        rc.write_lmk04832_using_hil(0x0130, 0x1E)  # 7:0 = DCLK12_13_DIV[7:0] (0x01E = 100MHz)
        rc.write_lmk04832_using_hil(0x0131, 0x0A)
        rc.write_lmk04832_using_hil(0x0132, 0x00)  # 1:0 = DCLK12_13_DIV[9:8]
        rc.write_lmk04832_using_hil(0x0133, 0x40)
        rc.write_lmk04832_using_hil(0x0134, 0x10)
        rc.write_lmk04832_using_hil(0x0135, 0x00)
        rc.write_lmk04832_using_hil(0x0136, 0x01)
        rc.write_lmk04832_using_hil(0x0137, 0x55)
        rc.write_lmk04832_using_hil(0x0138, 0x30)
        rc.write_lmk04832_using_hil(0x0139, 0x00)
        rc.write_lmk04832_using_hil(0x013A, 0x0C)
        rc.write_lmk04832_using_hil(0x013B, 0x00)
        rc.write_lmk04832_using_hil(0x013C, 0x00)
        rc.write_lmk04832_using_hil(0x013D, 0x08)
        rc.write_lmk04832_using_hil(0x013E, 0x03)
        rc.write_lmk04832_using_hil(0x013F, 0x0F)
        rc.write_lmk04832_using_hil(0x0140, 0x0F)
        rc.write_lmk04832_using_hil(0x0141, 0x00)
        rc.write_lmk04832_using_hil(0x0142, 0x00)
        rc.write_lmk04832_using_hil(0x0143, 0x11)
        rc.write_lmk04832_using_hil(0x0144, 0x00)
        rc.write_lmk04832_using_hil(0x0145, 0x00)
        rc.write_lmk04832_using_hil(0x0146, 0x38)
        rc.write_lmk04832_using_hil(0x0147, 0x06)  # 0x06 for CLKIN0 (10MHZ), 0x26 for CLKIN2 (5MHz HIPI)
        rc.write_lmk04832_using_hil(0x0148, 0x33)  # 0x33 selects READBACK
        rc.write_lmk04832_using_hil(0x0149, 0x42)
        rc.write_lmk04832_using_hil(0x014A, 0x03)
        rc.write_lmk04832_using_hil(0x014B, 0x06)
        rc.write_lmk04832_using_hil(0x014C, 0x00)
        rc.write_lmk04832_using_hil(0x014D, 0x00)
        rc.write_lmk04832_using_hil(0x014E, 0xC0)
        rc.write_lmk04832_using_hil(0x014F, 0x7F)
        rc.write_lmk04832_using_hil(0x0150, 0x01)
        rc.write_lmk04832_using_hil(0x0151, 0x02)
        rc.write_lmk04832_using_hil(0x0152, 0x00)
        rc.write_lmk04832_using_hil(0x0153, 0x00)
        rc.write_lmk04832_using_hil(0x0154, 0x02)
        rc.write_lmk04832_using_hil(0x0155, 0x00)
        rc.write_lmk04832_using_hil(0x0156, 0x78)
        rc.write_lmk04832_using_hil(0x0157, 0x00)
        rc.write_lmk04832_using_hil(0x0158, 0x01)
        rc.write_lmk04832_using_hil(0x0159, 0x00)
        rc.write_lmk04832_using_hil(0x015A, 0x19)
        rc.write_lmk04832_using_hil(0x015B, 0xD4)
        rc.write_lmk04832_using_hil(0x015C, 0x20)
        rc.write_lmk04832_using_hil(0x015D, 0x00)
        rc.write_lmk04832_using_hil(0x015E, 0x1E)
        rc.write_lmk04832_using_hil(0x015F, 0x0B)
        rc.write_lmk04832_using_hil(0x0160, 0x00)
        rc.write_lmk04832_using_hil(0x0161, 0x01)
        rc.write_lmk04832_using_hil(0x0162, 0x4C)
        rc.write_lmk04832_using_hil(0x0163, 0x00)
        rc.write_lmk04832_using_hil(0x0164, 0x00)
        rc.write_lmk04832_using_hil(0x0165, 0x0C)
        rc.write_lmk04832_using_hil(0x0169, 0x58)
        rc.write_lmk04832_using_hil(0x016A, 0x20)
        rc.write_lmk04832_using_hil(0x016B, 0x00)
        rc.write_lmk04832_using_hil(0x016C, 0x00)
        rc.write_lmk04832_using_hil(0x016D, 0x00)
        rc.write_lmk04832_using_hil(0x016E, 0x13)
        rc.write_lmk04832_using_hil(0x0173, 0x10)
        rc.write_lmk04832_using_hil(0x0177, 0x00)
        rc.write_lmk04832_using_hil(0x0182, 0x00)
        rc.write_lmk04832_using_hil(0x0183, 0x00)
        rc.write_lmk04832_using_hil(0x0166, 0x00)
        rc.write_lmk04832_using_hil(0x0167, 0x00)
        rc.write_lmk04832_using_hil(0x0168, 0x0C)
        rc.write_lmk04832_using_hil(0x0555, 0x00)
        Log('info', f'Rc3 Fab_A clock initialized!')

    def reset_interface(self):
        self.rc.write_bar_register(LMK04832_RESET(controller_fifo_reset=1))
        self.rc.write_bar_register(LMK04832_RESET(controller_fifo_reset=0))
        reg = self.status()
        if not (reg.tx_fifo_count == reg.rx_fifo_count == 0):
            raise Lmk04832.ResetError(
                f'Unable to clear fifos (tx count, rx count) : '
                f'{reg.tx_fifo_count}, {reg.rx_fifo_count}')

    def status(self):
        return self.rc.read_bar_register(LMK04832_STATUS)

    def send_read_command(self, address):
        self._send_command(address=address, rw=1)

    def send_write_command(self, address, write_data):
        self._send_command(address=address, rw=0, write_data=write_data)

    def _send_command(self, address, rw, write_data=0):
        # rw: 0 for Write, 1 for Read

        self.rc.write_bar_register(LMK04832_COMMAND(register=address,
                                                    rw=rw,
                                                    write_data=write_data))

    def read(self, address):
        start_count = self.rx_fifo_count()

        self.send_read_command(address)

        for timeout in range(READ_TIMEOUT):
            current_count = self.rx_fifo_count()
            if start_count + 1 == current_count:
                break
        else:
            raise Lmk04832.ReadTimeout(
                f'Lmk04832 Timed out reading address {address}')

        return self.rc.read_bar_register(LMK04832_READ_DATA).data

    def rx_fifo_count(self):
        return self.rc.read_bar_register(LMK04832_STATUS).rx_fifo_count

    class ReadTimeout(Exception):
        pass

    class ResetError(Exception):
        pass
