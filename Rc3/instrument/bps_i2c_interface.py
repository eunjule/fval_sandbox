# INTEL CONFIDENTIAL

# Copyright 2018-2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

from enum import Enum

from Common.instruments.i2c_test_interface import I2cTestInterface
from Rc3.instrument.rc3_register import BPS_I2C_STATUS, BPS_I2C_CONTROL,\
    BPS_I2C_TX_FIFO, BPS_I2C_RX_FIFO, BPS_I2C_RESET

I2C_ADDR = [0xB0, 0xB2]
PMBUS_CAPABILITY_VALUE = 0x00
PMBUS_REVISION_VALUE = 0x22

MAX_NUM_BPS = 2
UNUSED_I2C_ADDR = 0x30


def create_interfaces(instrument):
    interfaces = [BpsInterface(instrument, bps_number) for bps_number in
                  range(MAX_NUM_BPS)]
    return interfaces


class PmbusCommands(Enum):
    CAPABILITY = 0x19
    REVISION = 0x98


class BpsInterface(I2cTestInterface):
    KNOWN_VALUE_REGISTERS = {
        PmbusCommands.CAPABILITY.value: PMBUS_CAPABILITY_VALUE,
        PmbusCommands.REVISION.value: PMBUS_REVISION_VALUE}

    def __init__(self, instrument, bps_number):
        super().__init__(instrument=instrument,
                         i2c_addr=I2C_ADDR[bps_number],
                         interface_index=0,
                         interface_name=f'BPS')
        self.bps_number = bps_number
        self.update_register_table({'STATUS': BPS_I2C_STATUS,
                                    'CONTROL': BPS_I2C_CONTROL,
                                    'TX_FIFO': BPS_I2C_TX_FIFO,
                                    'RX_FIFO': BPS_I2C_RX_FIFO,
                                    'RESET': BPS_I2C_RESET})

    def read_pmbus_revision(self):
        data = self.read(PmbusCommands.REVISION.value)
        return data

    def read_pmbus_capability(self):
        data = self.read(PmbusCommands.CAPABILITY.value)
        return data
