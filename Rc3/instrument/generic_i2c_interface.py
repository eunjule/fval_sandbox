# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

from Common.instruments.i2c_test_interface import I2cTestInterface
from Rc3.instrument.rc3_register import GENERIC_I2C_STATUS, GENERIC_I2C_CONTROL,\
    GENERIC_I2C_TX_FIFO, GENERIC_I2C_RX_FIFO, GENERIC_I2C_RESET

I2C_ADDR = 0x0
GENERIC_GROUPS = 10


def create_interfaces(instrument):
    interfaces = [GenericI2cInterface(instrument, generic_group) for generic_group in
                  range(GENERIC_GROUPS)]
    return interfaces


class GenericI2cInterface(I2cTestInterface):
    def __init__(self, instrument, generic_group):
        super().__init__(instrument=instrument,
                         i2c_addr=I2C_ADDR,
                         interface_index=generic_group,
                         interface_name=f'GenericI2cInterface')
        self.interface_index = generic_group
        self.update_register_table({'STATUS': GENERIC_I2C_STATUS,
                                    'CONTROL': GENERIC_I2C_CONTROL,
                                    'TX_FIFO': GENERIC_I2C_TX_FIFO,
                                    'RX_FIFO': GENERIC_I2C_RX_FIFO,
                                    'RESET': GENERIC_I2C_RESET})

    def wait_i2c_busy(self):
        self.Log('debug', f'Skipping I2C Busy Bit check because there is no '
                          f'device present.')
