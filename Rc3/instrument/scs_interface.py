# INTEL CONFIDENTIAL

# Copyright 2021 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

from random import getrandbits, randint
import sys
from time import sleep

from Common import triggers, get_device_class
from Common.fval import Object
from Common.instruments import hdmt_trigger_interface
from Common.triggers import SCSMemoryEncoding


class ScsScopeshotInterface(Object):
    DDR4_NUM_BYTES = int((1 << 30) * 1.5)  # 1.5GB DDR space
    DDR4_ENDING_ADDRESS_DEFAULT = DDR4_NUM_BYTES - 1
    DDR4_STARTING_ADDRESS_DEFAULT = 0
    DDR4_ENTRY_BYTE_SIZE = 8
    MAX_NUM_DATA_ENTRIES = int(DDR4_NUM_BYTES / DDR4_ENTRY_BYTE_SIZE)
    SAMPLE_COUNT_DELAY_TIME_US = [20, 20]
    SAMPLE_COUNT_DEFAULT = 0
    SAMPLE_COUNT_TOLERANCE = 0.20
    WAIT_ON_ACTIVE_RETRY_COUNT = 5000

    def __init__(self):
        super().__init__()
        self.rc = get_device_class.get_device_class('rc3')()

    def enable(self):
        reg = self.config_reg()
        reg.enable = 1
        self.rc.write_bar_register(reg)

    def disable(self):
        reg = self.config_reg()
        reg.enable = 0
        self.rc.write_bar_register(reg)

    def config_reg(self):
        return self.rc.read_bar_register(self.rc.registers.SCOPESHOT_CONFIG)

    def ddr4_latest_address(self):
        return self.rc.read_bar_register(
            self.rc.registers.SCOPESHOT_LATEST_DDR4_ADDRESS).ddr4_address

    def ddr4_starting_address(self):
        return self.rc.read_bar_register(
            self.rc.registers.SCOPESHOT_DDR4_STARTING_ADDRESS).ddr4_address

    def set_ddr4_starting_address(self, address):
        reg = self.rc.registers.SCOPESHOT_DDR4_STARTING_ADDRESS(address)
        self.rc.write_bar_register(reg)

    def ddr4_ending_address(self):
        return self.rc.read_bar_register(
            self.rc.registers.SCOPESHOT_DDR4_ENDING_ADDRESS).ddr4_address

    def set_ddr4_ending_address(self, address):
        reg = self.rc.registers.SCOPESHOT_DDR4_ENDING_ADDRESS(address)
        self.rc.write_bar_register(reg)

    def start_scopeshot(self, bypass=True, use_event_trigger=True,
                        force_wait=True):
        if use_event_trigger:
            hdmt_trigger_interface.empty_hddps_trigger_buffers()
            debug_card_link_num = 12  # Avoids trigger broadcast to instruments
            self.send_start_event(debug_card_link_num, bypass)
        else:
            self.assert_start_control()
        if force_wait:
            self.wait_on_active(asserted=True)

    def stop_scopeshot(self, bypass=True, use_event_trigger=True,
                       force_wait=True):
        if use_event_trigger:
            hdmt_trigger_interface.empty_hddps_trigger_buffers()
            debug_card_link_num = 12  # Avoids trigger broadcast to instruments
            self.send_stop_event(debug_card_link_num, bypass)
        else:
            self.assert_stop_control()
        if force_wait:
            self.wait_on_active(asserted=False)

    def send_start_event(self, slot, bypass=True):
        trigger = hdmt_trigger_interface.generate_scs_start_trigger()
        if bypass:
            self.rc.send_up_trigger_bypass(slot, trigger)
        else:
            self.rc.send_broadcast_trigger(trigger)

    def send_stop_event(self, slot, bypass=True):
        trigger = hdmt_trigger_interface.generate_scs_stop_trigger()
        if bypass:
            self.rc.send_up_trigger_bypass(slot, trigger)
        else:
            self.rc.send_broadcast_trigger(trigger)

    def assert_start_control(self):
        # start bit self clears upon a write
        reg = self.rc.registers.SCOPESHOT_CONTROL(start=1)
        self.rc.write_bar_register(reg)

    def assert_stop_control(self):
        # stop bit self clears upon a write
        reg = self.rc.registers.SCOPESHOT_CONTROL(stop=1)
        self.rc.write_bar_register(reg)

    def ddr4_next_address(self, current_address):
        ddr4_entry_byte_size = 8
        end_addr = self.ddr4_ending_address()
        start_addr = self.ddr4_starting_address()
        if current_address < (end_addr - ddr4_entry_byte_size):
            return current_address + ddr4_entry_byte_size
        else:
            return start_addr

    def read_scopeshot_entries_from_memory(self, address, num_entries=1):
        entry_byte_size = ScsScopeshotInterface.DDR4_ENTRY_BYTE_SIZE
        total_bytes = entry_byte_size * num_entries
        ddr4_data = self.rc.dma_read(address, total_bytes)

        encoding = []
        for i in range(num_entries):
            data_lsb = entry_byte_size * i
            stamp_lsb = data_lsb + int(entry_byte_size/2)
            stamp_msb = data_lsb + entry_byte_size
            sensor_data = int.from_bytes(ddr4_data[data_lsb:stamp_lsb],
                                         byteorder=sys.byteorder)
            timestamp = int.from_bytes(ddr4_data[stamp_lsb:stamp_msb],
                                       byteorder=sys.byteorder)
            encoding.append((triggers.SCSMemoryEncoding(value=sensor_data),
                             timestamp))

        if num_entries == 1:
            return encoding[0]
        else:
            return encoding

    def compare_sensor_data(self, a, b):
        return (a.sensor_data == b.sensor_data and a.user_id == b.user_id and
                a.pin_id == b.pin_id and a.ac_slice_id == b.ac_slice_id and
                a.link_num == b.link_num)

    def generate_random_scs_ddr4_data(self, num_entries, link_num=None):
        data = [SCSMemoryEncoding(
                sensor_data=getrandbits(10),
                user_id=getrandbits(10),
                pin_id=getrandbits(2),
                ac_slice_id=getrandbits(2),
                link_num=randint(0, 13) if link_num is None else link_num)
            for i in range(num_entries)]
        return data

    def convert_ddr4_data_to_trigger(self, entry):
        trigger = hdmt_trigger_interface.generate_scs_trigger(
            slice=entry.ac_slice_id, pin=entry.pin_id, user=entry.user_id,
            data=entry.sensor_data
        )
        return trigger

    def convert_trigger_to_ddr4_data(self, trigger, link_num):
        entry = SCSMemoryEncoding(
                    sensor_data=trigger.data,
                    user_id=trigger.user,
                    pin_id=trigger.pin,
                    ac_slice_id=trigger.slice,
                    link_num=link_num)
        return entry

    def initiate_scopeshot(self, use_event_trigger=True):
        self.enable()
        self.start_scopeshot(use_event_trigger=use_event_trigger)

    def terminate_scopeshot(self, use_event_trigger=True):
        self.stop_scopeshot(use_event_trigger=use_event_trigger)
        self.disable()

        self.set_ddr4_starting_address(
            self.DDR4_STARTING_ADDRESS_DEFAULT)
        self.set_ddr4_ending_address(
            self.DDR4_ENDING_ADDRESS_DEFAULT)
        self.set_sample_count(self.SAMPLE_COUNT_DEFAULT)

    def is_active(self):
        active = self.rc.read_bar_register(
            self.rc.registers.SCOPESHOT_STATUS).active
        return bool(active)

    def sample_count(self):
        count = self.rc.read_bar_register(
            self.rc.registers.SCOPESHOT_CONFIG).sample_count
        return count

    def set_sample_count(self, count):
        reg = self.rc.read_bar_register(self.rc.registers.SCOPESHOT_CONFIG)
        reg.sample_count = count
        self.rc.write_bar_register(reg)

    class DDR4Error(Exception):
        pass

    def wait_on_active(self, asserted=True):
        for retry in range(ScsScopeshotInterface.WAIT_ON_ACTIVE_RETRY_COUNT):
            if asserted == self.is_active():
                break
            sleep(0.0001)
        else:
            raise ScsScopeshotInterface.TimeoutError(
                f'Timed out waiting on active status of {asserted}')

    class TimeoutError(Exception):
        pass
