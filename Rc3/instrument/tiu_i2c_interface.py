# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.


from Common.hilmon import rc3PsPmbusRead
from Common.instruments.i2c_test_interface import I2cTestInterface
from Rc3.instrument.rc3_register import TIU_I2C_STATUS, TIU_I2C_CONTROL, \
    TIU_I2C_TX_FIFO, TIU_I2C_RX_FIFO, TIU_I2C_RESET

I2C_ADDR = 0x4E


MAX_NUM_TIU = 1
UNUSED_I2C_ADDR = 0x30


def create_interfaces(instrument):
    interfaces = [TiuInterface(instrument)]
    return interfaces


class TiuInterface(I2cTestInterface):
    def __init__(self, instrument):
        super().__init__(instrument=instrument,
                         i2c_addr=I2C_ADDR,
                         interface_index=0,
                         interface_name=f'TIU')
        self.update_register_table({'STATUS': TIU_I2C_STATUS,
                                    'CONTROL': TIU_I2C_CONTROL,
                                    'TX_FIFO': TIU_I2C_TX_FIFO,
                                    'RX_FIFO': TIU_I2C_RX_FIFO,
                                    'RESET': TIU_I2C_RESET})


