# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

from enum import Enum

from Common.instruments.i2c_test_interface import I2cTestInterface
from Rc3.instrument.rc3_register import FP_I2C_STATUS, FP_I2C_CONTROL,\
    FP_I2C_TX_FIFO, FP_I2C_RX_FIFO, FP_I2C_RESET


I2C_ADDR = 0x94

MANUFACTURER_ID_VALUE = 0x41
DEVICE_ID_VALUE = 0x02

UNUSED_I2C_ADDR = 0x30
MAX_NUM_FP = 1

def create_interfaces(instrument):
    interfaces = [FpInterface(instrument, I2C_ADDR)]
    return interfaces


class I2cCommands(Enum):
    MANUFACTURER_ID = 0x4E
    DEVICE_ID = 0x4D


class FpInterface(I2cTestInterface):
    KNOWN_VALUE_REGISTERS = {
        I2cCommands.MANUFACTURER_ID.value: MANUFACTURER_ID_VALUE,
        I2cCommands.DEVICE_ID.value: DEVICE_ID_VALUE}

    def __init__(self, instrument, i2caddr):
        super().__init__(instrument=instrument,
                         i2c_addr=i2caddr,
                         interface_index=0,
                         interface_name=f'FrontPanel')
        self.update_register_table({'STATUS': FP_I2C_STATUS,
                                    'CONTROL': FP_I2C_CONTROL,
                                    'TX_FIFO': FP_I2C_TX_FIFO,
                                    'RX_FIFO': FP_I2C_RX_FIFO,
                                    'RESET': FP_I2C_RESET})

    def device_id(self):
        return self.read(I2cCommands.DEVICE_ID.value)

    def manufacturer_id(self):
        return self.read(I2cCommands.MANUFACTURER_ID.value)
