# INTEL CONFIDENTIAL

# Copyright 2019-2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

from ctypes import c_uint

import Common.register as register


def get_register_types():
    d = globals().copy()
    register_types = []
    for attribute in d.values():
        try:
            if issubclass(attribute, Rc3Register):
                if attribute is not Rc3Register:
                    register_types.append(attribute)
        except:
            pass
    return register_types


def get_register_type_names():
    d = globals().copy()
    register_types = []
    for attribute in d.values():
        try:
            if issubclass(attribute, Rc3Register):
                if attribute is not Rc3Register:
                    register_types.append(attribute.__name__)
        except:
            pass
    return register_types


def get_struct_types():
    d = globals().copy()
    struct_types = []
    for attribute in d.values():
        try:
            if issubclass(attribute, Rc3Struct):
                if attribute is not Rc3Struct:
                    struct_types.append(attribute)
        except:
            pass
    return struct_types


def get_struct_names():
    d = globals().copy()
    struct_types = []
    for attribute in d.values():
        try:
            if issubclass(attribute, Rc3Struct):
                if attribute is not Rc3Struct:
                    struct_types.append(attribute.__name__)
        except:
            pass
    return struct_types


class Rc3Register(register.Register):
    BASEMUL = 4
    BAR = 1


class Rc3Struct(register.Register):
    ADDR = None

    def __str__(self):
        l = []
        l.append(type(self).__name__)
        for field in self._fields_:
            l.append('\n    {}={}'.format(field[0], getattr(self, field[0])))
        return ' '.join(l)


class SCRATCH_REG(Rc3Register):
    BAR = 1
    ADDR = 0x0
    REGCOUNT = 8
    _fields_ = [('data', c_uint, 32)]


class FPGA_VERSION(Rc3Register):
    BAR = 1
    ADDR = 0x20
    _fields_ = [('minor', c_uint, 16),  # 15:0
                ('major', c_uint, 16)]  # 31:16


class HG_ID_LOWER(Rc3Register):
    BAR = 1
    ADDR = 0x24
    _fields_ = [('data', c_uint, 32)]


class HG_ID_UPPER(Rc3Register):
    BAR = 1
    ADDR = 0x28
    _fields_ = [('data', c_uint, 16),  # 0
                ('reserved', c_uint, 15),
                ('hg_clean', c_uint, 1)]  # 31


class TARGET_HARDWARE(Rc3Register):
    BAR = 1
    ADDR = 0x2c
    _fields_ = [('hw_compatibility', c_uint, 4),  # 3:0
                ('reserved', c_uint, 28)]  # 31:4


class CHIP_ID_UPPER(Rc3Register):
    BAR = 1
    ADDR = 0x30
    _fields_ = [('data', c_uint, 32)]


class CHIP_ID_LOWER(Rc3Register):
    BAR = 1
    ADDR = 0x34
    _fields_ = [('data', c_uint, 32)]


class LMK04832_COMMAND(Rc3Register):
    BAR = 1
    ADDR = 0x40

    _fields_ = [('write_data', c_uint, 8),
                ('register', c_uint, 15),
                ('rw', c_uint, 1),
                ('reserved', c_uint, 8)]


class LMK04832_READ_DATA(Rc3Register):
    BAR = 1
    ADDR = 0x44

    _fields_ = [('data', c_uint, 8),
                ('Reserved', c_uint, 31)]


class LMK04832_STATUS(Rc3Register):
    BAR = 1
    ADDR = 0x48

    _fields_ = [('tx_fifo_count', c_uint, 10),
                ('reserved_0', c_uint, 6),
                ('rx_fifo_count', c_uint, 10),
                ('reserved_1', c_uint, 6)]


class LMK04832_RESET(Rc3Register):
    BAR = 1
    ADDR = 0x4C

    _fields_ = [('controller_fifo_reset', c_uint, 1),
                ('chip_reset', c_uint, 1),
                ('reserved', c_uint, 30)]


class BPS_I2C_STATUS(Rc3Register):
    BAR = 1
    ADDR = 0x520
    REGCOUNT = 1
    _fields_ = [('receive_fifo_count', c_uint, 11),
                ('reserved', c_uint, 1),
                ('transmit_fifo_count', c_uint, 11),
                ('reserved', c_uint, 4),
                ('address_nak', c_uint, 1),
                ('data_nak', c_uint, 1),
                ('transmit_fifo_count_error', c_uint, 1),
                ('i2c_busy_error', c_uint, 1),
                ('i2c_busy', c_uint, 1)]


class BPS_I2C_CONTROL(Rc3Register):
    BAR = 1
    ADDR = 0x524
    _fields_ = [('transmit', c_uint, 32)]


class BPS_I2C_TX_FIFO(Rc3Register):
    BAR = 1
    ADDR = 0x528
    _fields_ = [('data', c_uint, 8),
                ('reserved', c_uint, 24)]


class BPS_I2C_RX_FIFO(Rc3Register):
    BAR = 1
    ADDR = 0x52c
    _fields_ = [('data', c_uint, 8),
                ('reserved', c_uint, 24)]


class BPS_I2C_RESET(Rc3Register):
    BAR = 1
    ADDR = 0x530
    _fields_ = [('reset', c_uint, 1),
                ('reserved', c_uint, 31)]


class PMBUS_MONITOR_I2C_STATUS(Rc3Register):
    BAR = 1
    ADDR = 0x560
    REGCOUNT = 1
    BASEMUL = 32
    _fields_ = [('receive_fifo_count', c_uint, 11),
                ('reserved', c_uint, 1),
                ('transmit_fifo_count', c_uint, 11),
                ('reserved', c_uint, 4),
                ('address_nak', c_uint, 1),
                ('data_nak', c_uint, 1),
                ('transmit_fifo_count_error', c_uint, 1),
                ('i2c_busy_error', c_uint, 1),
                ('i2c_busy', c_uint, 1)]


class PMBUS_MONITOR_I2C_CONTROL(Rc3Register):
    BAR = 1
    ADDR = 0x564
    REGCOUNT = 1
    BASEMUL = 32
    _fields_ = [('transmit', c_uint, 32)]


class PMBUS_MONITOR_I2C_TX_FIFO(Rc3Register):
    BAR = 1
    ADDR = 0x568
    REGCOUNT = 1
    BASEMUL = 32
    _fields_ = [('data', c_uint, 8),
                ('reserved', c_uint, 24)]


class PMBUS_MONITOR_I2C_RX_FIFO(Rc3Register):
    BAR = 1
    ADDR = 0x56c
    REGCOUNT = 1
    BASEMUL = 32
    _fields_ = [('data', c_uint, 8),
                ('reserved', c_uint, 24)]


class PMBUS_MONITOR_I2C_RESET(Rc3Register):
    BAR = 1
    ADDR = 0x570
    REGCOUNT = 1
    BASEMUL = 32
    _fields_ = [('reset', c_uint, 1),
                ('reserved', c_uint, 31)]


class AURORA_CONTROL(Rc3Register):
    BAR = 1
    ADDR = 0x100
    REGCOUNT = 14
    BASEMUL = 32
    _fields_ = [('reset', c_uint, 1),
                ('clear_error_counts', c_uint, 1),
                ('clear_fifos', c_uint, 1),
                ('reserved', c_uint, 29)]


class AURORA_STATUS(Rc3Register):
    BAR = 1
    ADDR = 0x104
    REGCOUNT = 14
    BASEMUL = 32
    _fields_ = [('transceiver_pll_locked', c_uint, 1),
                ('lane_up', c_uint, 1),
                ('channel_up', c_uint, 1),
                ('soft_error', c_uint, 1),
                ('hard_error', c_uint, 1),
                ('tx_pll_locked', c_uint, 1),
                ('reserved', c_uint, 2),
                ('rx_fifo_count', c_uint, 11),
                ('rx_fifo_full', c_uint, 1),
                ('tx_fifo_count', c_uint, 11),
                ('tx_fifo_full', c_uint, 1)]


class AURORA_ERROR_COUNTS(Rc3Register):
    BAR = 1
    ADDR = 0x108
    REGCOUNT = 14
    BASEMUL = 32
    _fields_ = [('soft_error_counts', c_uint, 16),
                ('hard_error_counts', c_uint, 16)]


class AURORA_TRIGGER_UP(Rc3Register):
    BAR = 1
    ADDR = 0x10C
    REGCOUNT = 14
    BASEMUL = 32
    _fields_ = [('trigger', c_uint, 32)]


class AURORA_TRIGGER_DOWN(Rc3Register):
    BAR = 1
    ADDR = 0x110
    REGCOUNT = 14
    BASEMUL = 32
    _fields_ = [('trigger', c_uint, 32)]


class AURORA_TRIGGER_UP_SIM(Rc3Register):
    BAR = 1
    ADDR = 0x114
    REGCOUNT = 14
    BASEMUL = 32
    _fields_ = [('trigger', c_uint, 32)]


class TRIGGER_SYNC(Rc3Register):
    BAR = 1
    ADDR = 0x080
    _fields_ = [('trigger_sync_0', c_uint, 1),
                ('trigger_sync_1', c_uint, 1),
                ('trigger_sync_2', c_uint, 1),
                ('trigger_sync_3', c_uint, 1),
                ('trigger_sync_4', c_uint, 1),
                ('trigger_sync_5', c_uint, 1),
                ('trigger_sync_6', c_uint, 1),
                ('trigger_sync_7', c_uint, 1),
                ('trigger_sync_8', c_uint, 1),
                ('trigger_sync_9', c_uint, 1),
                ('trigger_sync_10', c_uint, 1),
                ('trigger_sync_11', c_uint, 1),
                ('trigger_sync_12', c_uint, 1),
                ('reserved', c_uint, 18),
                ('time_stamp_reset', c_uint, 1)]


class TRIGGER_SYNC_MODULUS(Rc3Register):
    BAR = 1
    ADDR = 0x084
    _fields_ = [('modulus', c_uint, 5),
                ('reserved', c_uint, 27)]


class TRIGGER_SYNC_CONTROL(Rc3Register):
    BAR = 1
    ADDR = 0x088
    _fields_ = [('modulus_counter_reset', c_uint, 1),
                ('reserved', c_uint, 31)]


class TIU_CLOCK_DIVIDER(Rc3Register):
    BAR = 1
    ADDR = 0x08C
    _fields_ = [('divider', c_uint, 6),
                ('reserved', c_uint, 26)]


class TEMPERATURE_SENSOR_DATA(Rc3Register):
    BAR = 1
    ADDR = 0xA04
    _fields_ = [('data', c_uint, 32)]


class TEMPERATURE_SENSOR_CONTROL(Rc3Register):
    BAR = 1
    ADDR = 0xA00
    _fields_ = [('data', c_uint, 32)]


class CLOCK_FREQUENCY_MEASUREMENT(Rc3Register):
    BAR = 1
    ADDR = 0xA60
    REGCOUNT = 8
    _fields_ = [('meas_counter', c_uint, 12),
                ('ref_window_size', c_uint, 12),
                ('divider_ratio', c_uint, 8)]


class BROADCAST_TRIGGER_DOWN(Rc3Register):
    BAR = 1
    ADDR = 0x0E0
    _fields_ = [('trigger', c_uint, 32)]


class LAST_BROADCASTED_TRIGGER(Rc3Register):
    BAR = 1
    ADDR = 0x0E4
    _fields_ = [('trigger', c_uint, 32)]


class SOFTWARE_TRIGGER_FIFO_CONTROL(Rc3Register):
    BAR = 1
    ADDR = 0xA20
    _fields_ = [('reset', c_uint, 1),
                ('reserved', c_uint, 31)]


class SOFTWARE_TRIGGER_FIFO_STATUS(Rc3Register):
    BAR = 1
    ADDR = 0xA24
    _fields_ = [('count', c_uint, 11),
                ('reserved', c_uint, 21)]


class SOFTWARE_TRIGGER_FIFO_READ_DATA(Rc3Register):
    BAR = 1
    ADDR = 0xA28
    _fields_ = [('data', c_uint, 32)]


class SOFTWARE_TRIGGER_FIFO_SOURCE(Rc3Register):
    BAR = 1
    ADDR = 0xA2C
    _fields_ = [('data', c_uint, 5),
                ('reserved', c_uint, 27)]


class AD5064_COMMAND_DATA(Rc3Register):
    BAR = 1
    ADDR = 0xA0
    _fields_ = [('reserved_1', c_uint, 4),
                ('data', c_uint, 16),
                ('address_cmd', c_uint, 4),
                ('command', c_uint, 4),
                ('reserved_2', c_uint, 4)]


class AD5064_STATUS(Rc3Register):
    BAR = 1
    ADDR = 0xA4
    _fields_ = [('controller_busy', c_uint, 1),
                ('reserved', c_uint, 31)]


class AD5064_CONTROL(Rc3Register):
    BAR = 1
    ADDR = 0xA8
    _fields_ = [('controller_reset', c_uint, 1),
                ('reserved', c_uint, 31)]


class MAX1230_CHANNEL_DATA(Rc3Register):
    BAR = 1
    ADDR = 0x460
    REGCOUNT = 32
    _fields_ = [('data', c_uint, 12),
                ('reserved', c_uint, 20)]


class MAX1230_INTERNAL_TEMPERATURE(Rc3Register):
    BAR = 1
    ADDR = 0x4E0
    REGCOUNT = 2
    _fields_ = [('data', c_uint, 12),
                ('reserved', c_uint, 20)]


class AUX_FPGA_ENABLE(Rc3Register):
    BAR = 1
    ADDR = 0x680
    _fields_ = [('enable', c_uint, 1),
                ('reserved', c_uint, 31)]


class AUX_FPGA_SIDE_DATA_OUTPUT(Rc3Register):
    BAR = 1
    ADDR = 0x684
    _fields_ = [('data', c_uint, 12),
                ('reserved', c_uint, 20)]


class AUX_FPGA_SIDE_DATA_INPUT(Rc3Register):
    BAR = 1
    ADDR = 0x68C
    _fields_ = [('data', c_uint, 12),
                ('reserved', c_uint, 20)]


class AUX_FPGA_SIDE_DIRECTION_CONTROL(Rc3Register):
    BAR = 1
    ADDR = 0x688
    _fields_ = [('data', c_uint, 12),
                ('reserved', c_uint, 20)]


class AUX_SIDE_DATA_INPUT(Rc3Register):
    BAR = 1
    ADDR = 0x698
    _fields_ = [('data', c_uint, 12),
                ('reserved', c_uint, 20)]


class FAILSAFE_CONTROL(Rc3Register):
    BAR = 1
    ADDR = 0xB00
    _fields_ = [('reset', c_uint, 1),
                ('reserved', c_uint, 31)]


class FAILSAFE_LIMIT(Rc3Register):
    BAR = 1
    ADDR = 0xB04
    _fields_ = [('data', c_uint, 32)]


class S2H_BD_PACKET_BYTE_COUNT(Rc3Register):
    BAR = 1
    ADDR = 0xA40
    _fields_ = [('count', c_uint, 16),
                ('reserved', c_uint, 16)]


class S2H_BD_VALID_PACKET_INDEX(Rc3Register):
    BAR = 1
    ADDR = 0xA44
    _fields_ = [('index', c_uint, 16),
                ('reserved', c_uint, 16)]


class S2H_BD_ADDRESS_LOWER(Rc3Register):
    BAR = 1
    ADDR = 0xA48
    _fields_ = [('address', c_uint, 32)]


class S2H_BD_ADDRESS_UPPER(Rc3Register):
    BAR = 1
    ADDR = 0xA4C
    _fields_ = [('address', c_uint, 32)]


class S2H_BD_PACKET_COUNT(Rc3Register):
    BAR = 1
    ADDR = 0xA50
    _fields_ = [('count', c_uint, 16),
                ('reserved', c_uint, 16)]


class S2H_BD_STATUS(Rc3Register):
    BAR = 1
    ADDR = 0xA54
    _fields_ = [('streaming_enabled', c_uint, 1),
                ('reserved', c_uint, 31)]


class S2H_BD_CONTROL(Rc3Register):
    BAR = 1
    ADDR = 0xA58
    _fields_ = [('streaming_start', c_uint, 1),
                ('streaming_stop', c_uint, 1),
                ('streaming_reset', c_uint, 1),
                ('reserved_1', c_uint, 1),
                ('controller_reset', c_uint, 1),
                ('reserved', c_uint, 27)]


class S2H_BD_CONFIG(Rc3Register):
    BAR = 1
    ADDR = 0xA5C
    _fields_ = [('done', c_uint, 1),
                ('reserved', c_uint, 31)]


class TIU_I2C_STATUS(Rc3Register):
    BAR = 1
    ADDR = 0x500
    _fields_ = [('receive_fifo_count', c_uint, 11),
                ('reserved', c_uint, 1),
                ('transmit_fifo_count', c_uint, 11),
                ('reserved', c_uint, 4),
                ('address_nak', c_uint, 1),
                ('data_nak', c_uint, 1),
                ('transmit_fifo_count_error', c_uint, 1),
                ('i2c_busy_error', c_uint, 1),
                ('i2c_busy', c_uint, 1)]


class TIU_I2C_CONTROL(Rc3Register):
    BAR = 1
    ADDR = 0x504
    _fields_ = [('transmit', c_uint, 1),
                ('reserved', c_uint, 31)]


class TIU_I2C_TX_FIFO(Rc3Register):
    BAR = 1
    ADDR = 0x508
    _fields_ = [('data', c_uint, 8),
                ('reserved', c_uint, 24)]


class TIU_I2C_RX_FIFO(Rc3Register):
    BAR = 1
    ADDR = 0x50c
    _fields_ = [('data', c_uint, 8),
                ('reserved', c_uint, 24)]


class TIU_I2C_RESET(Rc3Register):
    BAR = 1
    ADDR = 0x510
    _fields_ = [('reset', c_uint, 1),
                ('reserved', c_uint, 31)]


class FP_I2C_STATUS(Rc3Register):
    BAR = 1
    ADDR = 0x540
    _fields_ = [('receive_fifo_count', c_uint, 11),
                ('reserved', c_uint, 1),
                ('transmit_fifo_count', c_uint, 11),
                ('reserved', c_uint, 4),
                ('address_nak', c_uint, 1),
                ('data_nak', c_uint, 1),
                ('transmit_fifo_count_error', c_uint, 1),
                ('i2c_busy_error', c_uint, 1),
                ('i2c_busy', c_uint, 1)]


class FP_I2C_CONTROL(Rc3Register):
    BAR = 1
    ADDR = 0x544
    _fields_ = [('transmit', c_uint, 1),
                ('reserved', c_uint, 31)]


class FP_I2C_TX_FIFO(Rc3Register):
    BAR = 1
    ADDR = 0x548
    _fields_ = [('data', c_uint, 8),
                ('reserved', c_uint, 24)]


class FP_I2C_RX_FIFO(Rc3Register):
    BAR = 1
    ADDR = 0x54c
    _fields_ = [('data', c_uint, 8),
                ('reserved', c_uint, 24)]


class FP_I2C_RESET(Rc3Register):
    BAR = 1
    ADDR = 0x550
    _fields_ = [('reset', c_uint, 1),
                ('reserved', c_uint, 31)]


class GENERAL_PURPOSE_INPUT(Rc3Register):
    BAR = 1
    ADDR = 0x400
    _fields_ = [('io_relay_input', c_uint, 20),
                ('tiu_present', c_uint, 1),
                ('rctc_fpga_vccp_alert', c_uint, 1),
                ('pmic_reset', c_uint, 1), #(N)
                ('reserved', c_uint, 9)]


class FPGA_DEBUG(Rc3Register):
    BAR = 1
    ADDR = 0x404
    _fields_ = [('debug_bus_output', c_uint, 8),
                ('debug_bus_direction_control', c_uint, 8),
                ('debug_bus_input_values', c_uint, 8),
                ('reserved', c_uint, 8)]


class FPGA_LED(Rc3Register):
    BAR = 1
    ADDR = 0x408
    _fields_ = [('led_control', c_uint, 8),
                ('reserved', c_uint, 24)]


class GENERAL_PURPOSE_OUTPUT(Rc3Register):
    BAR = 1
    ADDR = 0x40C
    _fields_ = [('relay_select', c_uint, 20),
                ('stratix_10_fault', c_uint, 1),
                ('backplane_i2c_reset', c_uint, 1),
                ('gpio_fault_out_enable', c_uint, 1),
                ('power_supply_kill_control', c_uint, 1),
                ('reserved', c_uint, 8)]


class INTERRUPT_STATUS(Rc3Register):
    BAR = 1
    ADDR = 0x410
    _fields_ = [('trigger_interrupt_pending', c_uint, 1),
                ('safety_mode_interrupt_pending', c_uint, 1),
                ('reserved', c_uint, 30)]


class INTERRUPT_CONTROL(Rc3Register):
    BAR = 1
    ADDR = 0x414
    _fields_ = [('trigger_interrupt_enable', c_uint, 1),
                ('safety_mode_interrupt_enable', c_uint, 1),
                ('reserved', c_uint, 30)]


class POWER_ON_COUNT(Rc3Register):
    BAR = 1
    ADDR = 0x418
    _fields_ = [('data', c_uint, 32)]


class RC_FPGA_SPARE_GPIO_CONTROL(Rc3Register):
    BAR = 1
    ADDR = 0x420
    _fields_ = [('rc_fpga_spare_gpio_data_output', c_uint, 1),
                ('reserved0', c_uint, 15),
                ('rc_fpga_spare_gpio_direction_control', c_uint, 1),
                ('reserved1', c_uint, 15)]


class RC_FPGA_SPARE_GPIO_INPUT(Rc3Register):
    BAR = 1
    ADDR = 0x424
    _fields_ = [('rc_fpga_spare_gpio_data_input', c_uint, 1),
                ('reserved', c_uint, 31)]


class RC_FRONT_PANEL_GPIO_CONTROL(Rc3Register):
    BAR = 1
    ADDR = 0x428
    _fields_ = [('rc_front_panel_gpio_data_output', c_uint, 2),
                ('reserved0', c_uint, 14),
                ('rc_front_panel_gpio_direction_control', c_uint, 2),
                ('reserved1', c_uint, 14)]


class RC_FRONT_PANEL_SPARE_GPIO_INPUT(Rc3Register):
    BAR = 1
    ADDR = 0x42C
    _fields_ = [('rc_front_panel_gpio_data_input', c_uint, 2),
                ('reserved', c_uint, 30)]


class TIU_SPARE_GPIO_INPUT(Rc3Register):
    BAR = 1
    ADDR = 0x430
    _fields_ = [('tiu_spare_gpio_data_input', c_uint, 6),
                ('reserved', c_uint, 26)]


class DDR_CONTROLLER(Rc3Register):
    BAR = 1
    ADDR = 0x440
    _fields_ = [('reset', c_uint, 1),
                ('reserved', c_uint, 31)]


class DDR_STATUS(Rc3Register):
    BAR = 1
    ADDR = 0x444
    _fields_ = [('calibration_fail', c_uint, 1),
                ('calibration_success', c_uint, 1),
                ('reset_done', c_uint, 1),
                ('reserved', c_uint, 29)]


class COMPATIBILITY(Rc3Register):
    BAR = 1
    ADDR = 0x2C
    _fields_ = [('hardware_compatibility', c_uint, 4),
                ('data', c_uint, 28)]


class LIVE_TIME_STAMP(Rc3Register):
    BAR = 1
    ADDR = 0xA80
    _fields_ = [('data', c_uint, 32)]


class LTC2358_SOFTSPAN_CODE(Rc3Register):
    BAR = 1
    ADDR = 0x600
    REGCOUNT = 2
    BASEMUL = 4
    _fields_ = [('channel_0', c_uint, 3),
                ('channel_1', c_uint, 3),
                ('channel_2', c_uint, 3),
                ('channel_3', c_uint, 3),
                ('channel_4', c_uint, 3),
                ('channel_5', c_uint, 3),
                ('channel_6', c_uint, 3),
                ('channel_7', c_uint, 3),
                ('reserved', c_uint, 8)]


class LTC2358_CHANNEL_DATA_0(Rc3Register):
    BAR = 1
    ADDR = 0x620
    REGCOUNT = 8
    BASEMUL = 4
    _fields_ = [('softspan_code', c_uint, 3),
                ('channel_id', c_uint, 3),
                ('raw_adc_data', c_uint, 18),
                ('reserved', c_uint, 8)]


class LTC2358_CHANNEL_DATA_1(Rc3Register):
    BAR = 1
    ADDR = 0x640
    REGCOUNT = 8
    BASEMUL = 4
    _fields_ = [('softspan_code', c_uint, 3),
                ('channel_id', c_uint, 3),
                ('raw_adc_data', c_uint, 18),
                ('reserved', c_uint, 8)]


class ACCESSORY_CARD_IO_OUTPUT(Rc3Register):
    ADDR = 0x6A0
    _fields_ = [('data', c_uint, 32)]


class ACCESSORY_CARD_IO_CONTROL(Rc3Register):
    ADDR = 0x6A4
    _fields_ = [('control', c_uint, 32)]


class ACCESSORY_CARD_IO_INPUT(Rc3Register):
    ADDR = 0x6A8
    _fields_ = [('data', c_uint, 32)]


class ACCESSORY_CARD_OUT(Rc3Register):
    ADDR = 0x6AC
    _fields_ = [('data', c_uint, 32)]


class SCOPESHOT_LATEST_DDR4_ADDRESS(Rc3Register):
    ADDR = 0xAA0
    _fields_ = [('ddr4_address', c_uint, 31),
                ('reserved', c_uint, 1)]


class SCOPESHOT_DDR4_ENDING_ADDRESS(Rc3Register):
    ADDR = 0xAA4
    _fields_ = [('ddr4_address', c_uint, 31),
                ('reserved', c_uint, 1)]


class SCOPESHOT_DDR4_STARTING_ADDRESS(Rc3Register):
    ADDR = 0xAA8
    _fields_ = [('ddr4_address', c_uint, 31),
                ('reserved', c_uint, 1)]


class SCOPESHOT_CONTROL(Rc3Register):
    ADDR = 0xAAC
    _fields_ = [('start', c_uint, 1),
                ('stop', c_uint, 1),
                ('reserved', c_uint, 30)]


class SCOPESHOT_CONFIG(Rc3Register):
    ADDR = 0xAB0
    _fields_ = [('sample_count', c_uint, 16),
                ('reserved', c_uint, 15),
                ('enable', c_uint, 1)]


class SCOPESHOT_STATUS(Rc3Register):
    ADDR = 0xAB4
    _fields_ = [('active', c_uint, 1),
                ('reserved', c_uint, 31)]


class RELAYS_GPIO_I2C_CONFIGURATION(Rc3Register):
    ADDR = 0x434
    _fields_ = [('group_0', c_uint, 1),
                ('group_1', c_uint, 1),
                ('group_2', c_uint, 1),
                ('group_3', c_uint, 1),
                ('group_4', c_uint, 1),
                ('group_5', c_uint, 1),
                ('group_6', c_uint, 1),
                ('group_7', c_uint, 1),
                ('group_8', c_uint, 1),
                ('group_9', c_uint, 1),
                ('reserved', c_uint, 22)]


class GENERIC_I2C_STATUS(Rc3Register):
    ADDR = 0x700
    REGCOUNT = 10
    BASEMUL = 0x20
    _fields_ = [('receive_fifo_count', c_uint, 11),
                ('reserved', c_uint, 1),
                ('transmit_fifo_count', c_uint, 11),
                ('reserved', c_uint, 4),
                ('address_nak', c_uint, 1),
                ('data_nak', c_uint, 1),
                ('transmit_fifo_count_error', c_uint, 1),
                ('i2c_busy_error', c_uint, 1),
                ('i2c_busy', c_uint, 1)]


class GENERIC_I2C_CONTROL(Rc3Register):
    ADDR = 0x704
    REGCOUNT = 10
    BASEMUL = 0x20
    _fields_ = [('transmit', c_uint, 1),
                ('reserved', c_uint, 31)]


class GENERIC_I2C_TX_FIFO(Rc3Register):
    ADDR = 0x708
    REGCOUNT = 10
    BASEMUL = 0x20
    _fields_ = [('data', c_uint, 8),
                ('reserved', c_uint, 24)]


class GENERIC_I2C_RX_FIFO(Rc3Register):
    ADDR = 0x70C
    REGCOUNT = 10
    BASEMUL = 0x20
    _fields_ = [('data', c_uint, 8),
                ('reserved', c_uint, 24)]


class GENERIC_I2C_RESET(Rc3Register):
    ADDR = 0x710
    REGCOUNT = 10
    BASEMUL = 0x20
    _fields_ = [('reset', c_uint, 1),
                ('reserved', c_uint, 31)]


class GENERIC_I2C_FREQUENCY(Rc3Register):
    ADDR = 0x714
    REGCOUNT = 10
    BASEMUL = 0x20
    _fields_ = [('mode', c_uint, 3),
                ('reserved', c_uint, 29)]
				
				
