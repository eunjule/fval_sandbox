# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

from Common.fval import Object
from Common.utilities import compliment_bytes

"""
CODE | ANALOG_INPUT_RANGE        | FULL_SCALE_RANGE    | CONVERSION_RESULT
--------------------------------------------------------------------------
 111 | ±2.5 • VREFBUF            | 5 • VREFBUF         | Two’s Complement
 110 | ±2.5 • VREFBUF/1.024      | 5 • VREFBUF/1.024   | Two’s Complement
 101 | 0V to 2.5 • VREFBUF       | 2.5 • VREFBUF       | Straight Binary
 100 | 0V to 2.5 • VREFBUF/1.024 | 2.5 • VREFBUF/1.024 | Straight Binary
 011 | ±1.25 • VREFBUF           | 2.5 • VREFBUF       | Two’s Complement
 010 | ±1.25 • VREFBUF/1.024     | 2.5 • VREFBUF/1.024 | Two’s Complement
 001 | 0V to 1.25 • VREFBUF      | 1.25 • VREFBUF      | Straight Binary
 000 | Channel Disabled          | Channel Disabled    | All Zeros
"""


class Ltc2358(Object):
    MAX_NUM_LTC2358 = 2
    MAX_NUM_CHANNELS = 8
    CODE_BIT_SIZE = 3
    SOFTSPAN_CODE_DISBALE = 0b000
    SOFTSPAN_CODE_DEFAULT = 0b111

    def __init__(self, rc):
        super().__init__()
        self.rc = rc

    def read_softspan_code_register(self, adc_num):
        reg = self.rc.read_bar_register(
            self.rc.registers.LTC2358_SOFTSPAN_CODE, index=adc_num)
        return reg

    def softspan_code(self, adc_num, channel):
        reg = self.read_softspan_code_register(adc_num)
        code = reg.value >> (Ltc2358.CODE_BIT_SIZE * channel) & 0b111
        return code

    def write_softspan_code(self, adc_num, channel, softspan_code):
        reg = self.read_softspan_code_register(adc_num)
        bit_shift_factor = Ltc2358.CODE_BIT_SIZE * channel
        mask = compliment_bytes(0b111 << bit_shift_factor, num_bytes=3)
        reg.value &= mask  # Clear corresponding bits
        reg.value |= softspan_code << bit_shift_factor  # shift in new code
        self.rc.write_bar_register(reg, adc_num)

    def read_channel_data_register(self, adc_num, channel):
        reg = None
        if adc_num == 0:
            reg = self.rc.read_bar_register(
                self.rc.registers.LTC2358_CHANNEL_DATA_0, channel)
        elif adc_num == 1:
            reg = self.rc.read_bar_register(
                self.rc.registers.LTC2358_CHANNEL_DATA_1, channel)
        return reg

    def channel_data(self, adc_num, channel):
        reg = self.read_channel_data_register(adc_num, channel)
        return [reg.raw_adc_data, reg.channel_id, reg.softspan_code]

    def disable_channel(self, adc_num, channel):
        self.write_softspan_code(adc_num, channel,
                                 Ltc2358.SOFTSPAN_CODE_DISBALE)

    def reset_channel(self, adc_num, channel):
        self.write_softspan_code(adc_num, channel,
                                 Ltc2358.SOFTSPAN_CODE_DEFAULT)
