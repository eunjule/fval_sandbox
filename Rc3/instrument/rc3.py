# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

import os
import re
import time

from ThirdParty.SASS.suppress_sensor import suppress_sensor

from Common import configs, stratix10
from Common import hilmon as hil
from Common import hps_diskless_boot_wrapper as hps_diskless_boot
from Common.cache_blt_info import CachingBltInfo
from Common.instruments.base import Instrument
from Common.instruments.hdmt_trigger_interface import AVAIL_TRIGGER_INTERFACES
from Common.instruments.s2h_interface import S2hBufferIndex, S2hInterface
import Common.instruments.tester as tester
from Common.models.rc import Rc as RcModel
from Rc3.instrument import rc3_register
from Rc3.instrument.lmk04832 import Lmk04832
from Rc3.instrument.ltc2358 import Ltc2358


RC_ROOT_I2C_TIMEOUT_MS = 0xFFFFFFFF

# 1.5GB DDR space
SCOPESHOT_ADDRESS_LIMIT_DEFAULT = default_limit = int(1.5 * (1 << 30)) - 1


class Rc3(Instrument):

    def __init__(self, card_list=[], name=None):
        super().__init__(name)
        self.fpga_load = configs.RC3_FPGA_LOAD

        self.card_list = card_list
        self.fpga_index = 0
        self.registers = rc3_register
        self.under_test = False
        self.slot = None
        self.aurora_link_slots = []
        self._is_initialized = False

        self.rcModel = RcModel()
        self.instrumentblt = CachingBltInfo(
            callback_function=hil.rc3BltInstrumentRead,
            name='RC3 instrument')
        self.boardblt = CachingBltInfo(
            callback_function=hil.rc3BltBoardRead,
            name='RC3 board')

        self.bulk_data_s2h_stream = S2hInterface(self, S2hBufferIndex.BD)
        self.ltc2358 = Ltc2358(self)
        self.fab_letter = None
        self._failsafe_limit_default = None
        self._time_after_fpga_load = None

    def Initialize(self):
        self.update_aurora_link_slots()
        if configs.SKIP_INIT:
            self.fpga_load = False
            self.Log('info', f'Skipping initialization for {self.name()}')
        else:
            self.initialize_fpga()
        self.log_traceability()

    def initialize_fpga(self):
        Lmk04832.ClocksInit(self,
                            force=True if os.getenv('FORCE_RC3_CLOCK_INIT')
                            else False)
        self.load_fpga()
        hil.rc3Init()
        self.rc_root_i2c_init(RC_ROOT_I2C_TIMEOUT_MS)
        self._train_links()

        hps_diskless_boot.BootHps(self.name())
        self.bulk_data_s2h_stream.reset_stream()
        self.bulk_data_s2h_stream.enable_stream()

        self.enable_interrupts()
        if self.rc_loopback_exists():
            self.hold_accessory_card_in_reset()

        self._is_initialized = True

    def _train_links(self, num_retries=20):
        for slot in self.aurora_link_slots:
            for retry in range(num_retries):
                self.reset_aurora_bus(slot)
                self.clear_aurora_error_counts(slot)
                if self.aurora_link_is_stable(slot):
                    self.Log('info', f'Aurora link {slot} is stable.')
                    break
            else:
                self.log_aurora_status(slot, log_level='error')
                raise Rc3.AuroraBusError(f'Unable to stabilize aurora link '
                                         f'{slot}')

    class AuroraBusError(Exception):
        pass

    def ensure_initialized(self):
        if not self._is_initialized:
            self.Initialize()

    def log_traceability(self):
        self.log_fpga_version()
        self.log_build()
        self.log_cpld()
        self.log_chip_id()
        self.instrumentblt.write_to_log()
        self.boardblt.write_to_log()

    def load_fpga(self):
        if self.fpga_load:
            self.Log('info', f'Loading {self.name()} FPGA  with image '
                             f'"{self.fpga_image_path()}"')
            self.load_fpga_using_hil()
            self.Log('info', f'{self.name()} FPGA load succeeded')
        else:
            self.Log('warning', f'{self.name()} FPGA was not loaded due ' +
                     'to configs setting')

    def log_fpga_version(self):
        try:
            fpga_version = self.fpga_version()
            if self.is_non_zero_or_non_f(f'{fpga_version:08X}'):
                self.Log('info',
                         f'{self.name()} FPGA Version: '
                         f'{fpga_version >> 16}.{fpga_version & 0xFFFF}')
            else:
                self.Log('warning', f'{self.name()}: FPGA Version read all '
                                    f'0\' or all f\'s {fpga_version}')
        except RuntimeError:
            self.Log('warning', f'{self.name()} Version read was unsuccessful')
            return None

    def fpga_image_path(self):
        fab_letter = self.get_fab_letter()
        path = getattr(configs, f'RC3_FPGA_IMAGE_FAB_{fab_letter}')
        return path

    def get_fab_letter(self):
        if self.fab_letter is None:
            self.fab_letter = self.boardblt.fab_letter()
        return self.fab_letter

    def load_fpga_using_hil(self):
        with suppress_sensor('FVAL RC3 load_fpga_using_hil', 'RC3'):
            with Rc3.FpgaPcieDriverDisable():
                try:
                    hil.rc3FpgaLoad(self.fpga_image_path())
                    self._time_after_fpga_load = time.time()
                except RuntimeError as e:
                    raise Rc3.InstrumentError(
                        f'{self.name()} FPGA load was not successful '
                        f'with image "{self.fpga_image_path()}". {e}.')
            self._failsafe_limit_default = self.read_failsafe_limit()

        # Do not remove. Rc requires this every time a new image is loaded
        self.disable_fail_safe()

    class FpgaPcieDriverDisable():
        def __init__(self):
            pass

        def __enter__(self):
            hil.rc3DeviceDisable()
            return self

        def __exit__(self, exc_type, exc_val, exc_tb):
            hil.rc3DeviceEnable()

    def failsafe_limit_default(self):
        if self._failsafe_limit_default is None:
            self._failsafe_limit_default = self.read_failsafe_limit()
        return self._failsafe_limit_default

    def time_after_fpga_load(self):
        return self._time_after_fpga_load

    def enable_device(self):
        hil.rc3DeviceEnable()

    def disable_device(self):
        hil.rc3DeviceDisable()

    def read_hg_id(self):
        lower = self.read_bar_register(self.registers.HG_ID_LOWER).data
        upper = self.read_bar_register(self.registers.HG_ID_UPPER).data
        return upper, lower

    def read_chip_id(self):
        upper = self.read_bar_register(self.registers.CHIP_ID_UPPER).data
        lower = self.read_bar_register(self.registers.CHIP_ID_LOWER).data
        return upper, lower

    def log_chip_id(self):
        chip_id_upper, chip_id_lower = self.read_chip_id()
        chip_id = (chip_id_upper << 32) + chip_id_lower
        if chip_id != 0 and chip_id_lower != 0xffffffff and \
                chip_id_upper != 0xffffffff:
            self.Log('info', f'{self.name()} Chip ID: 0x{chip_id:016X}')
        else:
            self.Log('error', f'{self.name()} Chip ID read failed. Received '
                              f'0x{chip_id:016X} ')

    def log_build(self):
        try:
            hg_id_upper, hg_id_lower = self.read_hg_id()
            if hg_id_upper >> 31:
                hg_clean = ''
            else:
                hg_clean = '+'
            self.Log('info', f'{self.name()} FPGA Build: '
                             f'0x{hg_id_upper & 0xFFFF:04X}'
                             f'{hg_id_lower:08X}{hg_clean}')
        except RuntimeError:
            self.Log('warning', f'{self.name()} Build read was unsuccessful')
            return None

    def log_cpld(self):
        version_string = hil.rc3CpldVersionString()
        self.Log('info', f'{self.name()} CPLD Version: {version_string}')

    def disable_fail_safe(self):
        hil.rc3FailsafeDisable(1)

    def get_fpga_version_string(self):
        return hil.rc3FpgaVersionString()

    def read_bar_register(self, register_type, index=0):
        reg_value = self.bar_read(register_type.BAR,
                                  register_type.address(index))
        reg = register_type(value=reg_value)
        return reg

    def bar_read(self, bar, offset):
        data = hil.rc3BarRead(bar, offset)
        return data

    def write_bar_register(self, register, index=0):
        self.bar_write(register.BAR, register.address(index), register.value)

    def bar_write(self, bar, offset, data):
        return hil.rc3BarWrite(bar, offset, data)

    def get_hg_clean_bit(self):
        return self.read_bar_register(self.registers.HG_ID_UPPER).hg_clean

    def hg_id(self):
        upper = self.read_bar_register(self.registers.HG_ID_UPPER).data
        lower = self.read_bar_register(self.registers.HG_ID_LOWER).data
        return upper << 32 | lower

    def hg_id_from_file(self):
        per_build_params_file = self.fpga_per_build_param_file()
        return self.find_hg_id_in_param_build_file(per_build_params_file)

    def fpga_per_build_param_file(self):
        image_file = self.fpga_image_path()
        image_folder = os.path.dirname(image_file)
        per_build_params_file = os.path.join(image_folder,
                                             'per_build_params.txt')
        return per_build_params_file

    def find_hg_id_in_param_build_file(self, per_build_params_file):
        hg_id = 0
        with open(per_build_params_file, 'r') as build_params:
            lines = build_params.readlines()
            for line in lines:
                key_value = line.strip().split(' ')
                if 'hg_id' == key_value[0].lower():
                    hg_id = key_value[1]
                    break
            else:
                self.Log('error', f'Hg_ID could not be foud in '
                                  f'per_build_params.txt. Defaulting to 0x0')
        return int(hg_id, 16)

    def fpga_version(self):
        return self.read_bar_register(self.registers.FPGA_VERSION).value

    def is_tiu_physically_connected(self):
        return bool(self.read_bar_register(
            self.registers.GENERAL_PURPOSE_INPUT).tiu_present)

    def dma_read(self, address, length):
        return hil.rc3DmaRead(address, length)

    def dma_write(self, address, data):
        hil.rc3DmaWrite(address, data)

    def get_ddr_calibration_status(self):
        ddr_calibration_status = self.read_bar_register(
            self.registers.DDR_STATUS).value
        return ddr_calibration_status

    def ddr_is_ready(self):
        reg = self.read_bar_register(self.registers.DDR_STATUS)
        if reg.calibration_success and not reg.calibration_fail and \
                reg.reset_done:
            return True
        else:
            self.issue_ddr_reset()
            if self.is_status_calibration_after_wait():
                return True
            else:
                return False

    def read_trigger_up(self, slot):
        reg = self.read_bar_register(self.registers.AURORA_TRIGGER_UP, slot)
        return reg.value

    def read_trigger_down(self, slot=0):
        reg = self.read_bar_register(self.registers.AURORA_TRIGGER_DOWN, slot)
        return reg.value

    def read_clock_compensation_status(self):
        self.Log('info', 'Unable to read Clock Compensation Status. Not '
                         'available in RCTC3 FPGA')

    def read_clock_compensation_capability(self):
        self.Log('info', 'Capability Register not available in RCTC3 FPGA')
        return 0

    def disable_clock_compensation(self):
        self.Log('info', 'Unable to disable Clock Compensation. Not available '
                         'in RCTC3 FPGA')

    def enable_clock_compensation(self):
        self.Log('info', 'Unable to enable Clock Compensation. Not available '
                         'in RCTC3 FPGA')

    def wait_for_reset_to_clear(self):
        pass

    def send_sync_pulse(self, slots=[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]):
        if type(slots) is int:
            slots = [slots]

        reg = self.registers.TRIGGER_SYNC()
        for slot in slots:
            if 0 <= slot < 12:
                reg.value |= (1 << slot)
            else:
                raise Rc3.TriggerSyncPulseError(f'Invalid slot number: {slot}')
        self.write_bar_register(reg)
        self._wait_on_trigger_sync_pulse()

    def _wait_on_trigger_sync_pulse(self, num_retries=50):
        for retry in range(num_retries):
            sync = self.read_bar_register(self.registers.TRIGGER_SYNC).value
            if sync == 0:
                break
            time.sleep(0.001)
        else:
            raise Rc3.TriggerSyncPulseError(
                'Timed out waiting on trigger sync pulse to complete')

    class TriggerSyncPulseError(Exception):
        pass

    def reset_live_time_stamp_timer(self):
        reg = self.read_bar_register(self.registers.TRIGGER_SYNC)
        reg.time_stamp_reset = 0
        self.write_bar_register(reg)
        reg.time_stamp_reset = 1
        self.write_bar_register(reg)
        reg.time_stamp_reset = 0
        self.write_bar_register(reg)

    def live_time_stamp(self):
        reg = self.read_bar_register(self.registers.LIVE_TIME_STAMP)
        return reg.value

    def update_trigger_sync_modulus(self, modulus):
        reg = self.registers.TRIGGER_SYNC_MODULUS(modulus=modulus)
        self.write_bar_register(reg)

    def trigger_sync_modulus(self):
        return self.read_bar_register(
            self.registers.TRIGGER_SYNC_MODULUS).value

    def reset_trigger_sync_modulus_counter(self):
        reg = self.registers.TRIGGER_SYNC_CONTROL(modulus_counter_reset=0)
        self.write_bar_register(reg)
        reg.modulus_counter_reset = 1
        self.write_bar_register(reg)
        reg.modulus_counter_reset = 0
        self.write_bar_register(reg)

    def update_tiu_clock_divider(self, divider):
        reg = self.registers.TIU_CLOCK_DIVIDER(divider=divider)
        self.write_bar_register(reg)

    def tiu_clock_divider(self):
        return self.read_bar_register(self.registers.TIU_CLOCK_DIVIDER).value

    def enable_interrupts(self):
        reg = self.read_bar_register(self.registers.INTERRUPT_CONTROL)
        reg.trigger_interrupt_enable = 1
        reg.safety_mode_interrupt_enable = 1
        self.write_bar_register(reg)

    def fpga_core_temperature(self):
        return hil.rc3TmonRead(0)

    def temperature_using_bar_read(self, channel):
        self.write_bar_register(
            self.registers.TEMPERATURE_SENSOR_CONTROL(channel))
        raw_temperature = self.read_bar_register(
            self.registers.TEMPERATURE_SENSOR_DATA).value
        return stratix10.convert_raw_temp_to_degrees_celcius(raw_temperature)

    def read_clock_frequency_mhz(self, index):
        # See RCTC3_FPGA-HPS_HLD, Revision 2.1 (4/28/2020), Section 7.22
        reference_clock_period = 1 / 125.0  # 8ns

        reg = self.read_bar_register(
            self.registers.CLOCK_FREQUENCY_MEASUREMENT, index)

        # See RCTC3_FPGA-HPS_HLD, Revision 2.1 (4/28/2020), Section 7.22
        if reg.meas_counter == 0 or reg.meas_counter == 0:
            measured_clock_frequency = 0
        else:
            measured_clock_period = \
                ((reference_clock_period * reg.ref_window_size) /
                 (reg.meas_counter * reg.divider_ratio))
            measured_clock_frequency = 1.0 / measured_clock_period

        return measured_clock_frequency

    def hold_accessory_card_in_reset(self):
        """Prevent broadcast triggering from looping back"""
        self.hold_aurora_link_in_reset(links=[13])

    def hold_aurora_link_in_reset(self, links):
        if type(links) != list:
            links = [links]
        reg = self.registers.AURORA_CONTROL()
        reg.reset = 1
        reg.clear_error_counts = 1
        reg.clear_fifo = 1
        for link in links:
            self.write_bar_register(reg, link)

    def release_aurora_link_from_reset(self, links):
        if type(links) != list:
            links = [links]
        reg = self.registers.AURORA_CONTROL()
        reg.reset = 0
        reg.clear_error_counts = 0
        reg.clear_fifo = 0
        for link in links:
            self.write_bar_register(reg, link)

    def clear_rc_trigger(self):
        for slot in self.aurora_link_slots:
            self.clear_aurora_error_and_fifos(slot)

    def update_aurora_link_slots(self):
        self.aurora_link_slots = []
        t = tester.get_tester()

        self.aurora_link_slots = [
            device.slot for device in t.devices
            if (device.name() != self.name() and
                device.name().lower() in AVAIL_TRIGGER_INTERFACES)]

        slots = os.getenv('DBG_REMOVE_HDMT_SLOTS')
        if slots is not None:
            slots = slots.split(',')
            for slot in slots:
                slot = int(slot)
                try:
                    self.aurora_link_slots.remove(slot)
                except ValueError as e:
                    self.Log('warning',
                             f'{self.name()}: Unable to remove slot {slot} '
                             f'from aurora_link_list. {e}')

    def reset_trigger_interface(self, slot, enable_info_log=False):
        num_retries = 100
        for retry in range(num_retries):
            self.reset_aurora(slot)
            self.clear_aurora_error_and_fifos(slot, log_level='warning')

            if self.aurora_link_is_stable(slot):
                if enable_info_log:
                    self.Log('info',
                             f'{self.name()}: Aurora link {slot} successfully '
                             f'reset after {retry} retries')
                break
        else:
            self.Log('error',
                     f'{self.name()}: Aurora link {slot} unable to reset.')
            self.log_aurora_status(slot)

    def reset_aurora_bus(self, slots):
        if type(slots) is not list:
            slots = [slots]
        for slot in slots:
            self.reset_aurora(slot)

    def reset_aurora(self, slot):
        reg = self.registers.AURORA_CONTROL()
        reg.reset = 1
        self.write_bar_register(reg, slot)
        reg.reset = 0
        self.write_bar_register(reg, slot)
        time.sleep(0.5)

    def log_aurora_status(self, slot, log_level='info'):
        reg = self.read_bar_register(self.registers.AURORA_STATUS, slot)
        self.Log(log_level, f'{self.name()}_{slot} (aurora_status): '
                            f'0x{reg.value:08X}')

    def log_aurora_error_counts(self, slot, log_level='info'):
        reg = self.read_bar_register(self.registers.AURORA_ERROR_COUNTS, slot)
        self.Log(log_level, f'{self.name()}_{slot} (error_counts): '
                            f'0x{reg.value:08X}')

    def clear_aurora_error_and_fifos(self, slot, log_level='error'):
        error_reg = self.registers.AURORA_ERROR_COUNTS()

        num_retries = 10
        status_fail = False
        error_fail = False
        for retry in range(num_retries):
            self.clear_aurora_fifos(slot)
            self.clear_aurora_error_counts(slot)

            status_reg = self.read_bar_register(
                self.registers.AURORA_STATUS, slot)
            if status_reg.rx_fifo_count or status_reg.rx_fifo_full or \
                    status_reg.tx_fifo_count or status_reg.tx_fifo_full:
                status_fail = True

            error_reg = self.read_bar_register(
                self.registers.AURORA_ERROR_COUNTS, slot)
            if error_reg.value:
                error_fail = True

            if not status_fail and not error_fail:
                break
            else:
                self.Log(
                    'warning',
                    f'{self.name()}: Unable to clear aurora errors and fifos '
                    f'at slot {slot} ({retry+1} of {num_retries})')
                self.log_aurora_status(slot, log_level='warning')
                self.log_aurora_error_counts(slot, log_level='warning')
                status_fail = False
                error_fail = False
        else:
            if status_fail:
                self.log_aurora_status(slot, log_level)
            if error_fail:
                self.Log(log_level, self.create_table_from_register(error_reg))

    def clear_aurora_fifos(self, slot):
        control_reg = self.registers.AURORA_CONTROL()
        control_reg.clear_fifos = 1
        self.write_bar_register(control_reg, slot)
        control_reg.clear_fifos = 0
        self.write_bar_register(control_reg, slot)
        time.sleep(0.50)

    def clear_aurora_error_counts(self, slot):
        control_reg = self.registers.AURORA_CONTROL()
        control_reg.clear_error_counts = 1
        self.write_bar_register(control_reg, slot)
        control_reg.clear_error_counts = 0
        self.write_bar_register(control_reg, slot)
        time.sleep(0.50)

    def aurora_link_is_stable(self, slot):
        status_reg = self.read_bar_register(self.registers.AURORA_STATUS,
                                            slot)
        pll_lock_status = (status_reg.tx_pll_locked and
                           status_reg.transceiver_pll_locked)
        error_status = status_reg.hard_error or status_reg.soft_error
        up_status = status_reg.lane_up and status_reg.channel_up

        if not pll_lock_status or error_status or not up_status:
            return False
        else:
            return True

    def create_table_aurora_link_status(self, slot):
        status_reg = self.read_bar_register(self.registers.AURORA_STATUS, slot)
        return self.create_table_from_register(status_reg)

    def create_table_aurora_error_counts(self, slot):
        reg = self.read_bar_register(self.registers.AURORA_ERROR_COUNTS, slot)
        return self.create_table_from_register(reg)

    def send_trigger(self, trigger, slot=-1):
        if slot == -1:
            for slot in self.aurora_link_slots:
                self.write_bar_register(
                    self.registers.AURORA_TRIGGER_DOWN(value=trigger), slot)
        else:
            self.write_bar_register(
                self.registers.AURORA_TRIGGER_DOWN(value=trigger), slot)

    def send_up_trigger_bypass(self, slot, trigger):
        reg = self.registers.AURORA_TRIGGER_UP_SIM(trigger)
        self.write_bar_register(reg, slot)

    def next_trigger(self, slot):
        return self.read_trigger_up(slot)

    def software_trigger_reset(self, num_retries=10):
        for retry in range(num_retries):
            self.write_bar_register(
                self.registers.SOFTWARE_TRIGGER_FIFO_CONTROL(0))
            self.write_bar_register(
                self.registers.SOFTWARE_TRIGGER_FIFO_CONTROL(1))
            self.write_bar_register(
                self.registers.SOFTWARE_TRIGGER_FIFO_CONTROL(0))

            count = self.read_bar_register(
                self.registers.SOFTWARE_TRIGGER_FIFO_STATUS).count
            if count == 0:
                break
        else:
            raise Rc3.SoftwareTriggerError(
                'Unable to reset Software Trigger FIFO')

    class SoftwareTriggerError(Exception):
        pass

    def software_trigger_fifo_count(self):
        return self.read_bar_register(
            self.registers.SOFTWARE_TRIGGER_FIFO_STATUS).value

    def software_trigger_fifo_read_data(self):
        return self.read_bar_register(
            self.registers.SOFTWARE_TRIGGER_FIFO_READ_DATA).value

    def software_trigger_source(self):
        return self.read_bar_register(
            self.registers.SOFTWARE_TRIGGER_FIFO_SOURCE).value

    def read_ad5064_command_data(self):
        return self.read_bar_register(self.registers.AD5064_COMMAND_DATA)

    def write_ad5064_command_data(self, command, address, data):
        self._wait_until_controller_is_free()
        command_data = self.registers.AD5064_COMMAND_DATA()
        command_data.command = command
        command_data.address_cmd = address
        command_data.data = data
        self.write_bar_register(command_data)

    def write_ad5064_control(self, data):
        self._wait_until_controller_is_free()
        self.write_bar_register(self.registers.AD5064_CONTROL(data))

    def read_ad5064_control(self):
        return self.read_bar_register(self.registers.AD5064_CONTROL)

    def ad5064_wait_on_busy(self, num_retries=20):
        for retry in range(num_retries):
            busy = (self.read_bar_register(
                self.registers.AD5064_STATUS).controller_busy == 1)
            if not busy:
                break
        else:
            self.Log('error', f'AD5604 timed out waiting on busy signal')

    def ad5064_reset(self):
        reg = self.registers.AD5064_CONTROL(controller_reset=1)
        self.write_bar_register(reg)
        reg.controller_reset = 0
        self.write_bar_register(reg)
        time.sleep(0.5)

    def max1230_channel_temperature(self, channel):
        raw_temperature = self.read_bar_register(
            self.registers.MAX1230_INTERNAL_TEMPERATURE, index=channel).value
        return self.convert_raw_temperature(raw_temperature)

    def convert_raw_temperature(self, raw_temperature):
        return raw_temperature * 0.125

    def max1230_channel_temperature_using_hil(self, channel):
        return hil.rc3TmonRead(channel + 1)

    def voltage_using_hil(self, channel):
        return hil.rc3VmonRead(channel)

    def voltage(self, channel):
        raw_voltage = self.read_bar_register(
            self.registers.MAX1230_CHANNEL_DATA, index=channel)
        convert_raw_voltage = self.max1230_adc_to_voltage \
            if self.get_fab_letter().upper() == 'A' else \
            self.max1230_adc_to_voltage_FabB
        return convert_raw_voltage(raw_voltage.value, channel)

    def max1230_adc_to_voltage(self, data, channel):
        # MAX1230 12-bit binary value.
        # The range is from 0x000 (0.0V) to 0xFFF (4.095V),
        # LSB = 0.001V (4.096V / 0x1000).
        # Channels 0:15 belong to U187
        # Channels 16:31 belong to U186
        default = 1.0
        scales = {12: -2.49, 13: -2.49, 14: 5.99,
                  16: 2.0, 23: 5.99,
                  27: (1.5 + 0.499) / 0.499, 28: -2.74, 29: -2.74,
                  30: 5.99}

        return data * 0.001 * scales.get(channel, default)

    def max1230_adc_to_voltage_FabB(self, data, channel):
        scales = {
            0: 1.0, 1: 1.0, 2: 1.0, 3: 1.0, 4: 1.0, 5: 1.0, 6: 1.0, 7: 2.0,
            8: 1.0, 9: 1.0, 10: 1.0, 11: 1.0, 12: -2.49, 13: -2.49, 14: 5.99,
            15: 1.0,  # U187

            16: 2.0, 17: 1.0, 18: 1.0, 19: 1.0, 20: 1.0, 21: 1.0, 22: 1.0,
            23: 5.99, 24: 1.0, 25: 1.0, 26: 1.0, 27: (1.5 + 0.499) / 0.499,
            28: -2.74, 29: -2.74, 30: 5.99, 31: 1.0}  # U186

        return data * 0.001 * scales[channel]

    def _wait_until_controller_is_free(self):
        controller_status = self.read_bar_register(
            self.registers.AD5064_STATUS)
        for i in range(0xFFFFF):
            if controller_status.controller_busy == 0:
                break
        else:
            self.Log('error',
                     f'AD5064 Controller Busy Bit failed to deassert.')

    def rc_root_i2c_lock_bps(self, bps_num, ms_timeout=RC_ROOT_I2C_TIMEOUT_MS):
        backplane = 1
        channels = [1 << 5, 1 << 7][bps_num]
        self.rc_root_i2c_lock(ms_timeout)
        hil.rcRootI2cMuxSelect(selection=backplane)
        hil.bpRootI2cSwitchSelect(channels)

    def rc_root_i2c_init(self, ms_timeout=0xFFFFFFFF):
        try:
            hil.rcRootI2cInit(ms_timeout)
        except RuntimeError as e:
            self.Log('error',
                     f'{self.name()}: Unable to initialize I2C. \n{e}')

    def rc_root_i2c_lock(self, ms_timeout):
        try:
            hil.rcRootI2cLock(ms_timeout)
        except RuntimeError as e:
            self.Log('error', f'{self.name()}: Unable to Lock I2C. \n{e}')

    def rc_root_i2c_unlock(self):
        try:
            hil.rcRootI2cUnlock()
        except RuntimeError as e:
            self.Log('error', f'{self.name()}: Unable to Unlock I2C. \n{e}')

    def write_aux_fpga_side_data(self, value):
        self.write_bar_register(
            self.registers.AUX_FPGA_SIDE_DATA_OUTPUT(value))

    def aux_fpga_side_data(self):
        return self.read_bar_register(
            self.registers.AUX_FPGA_SIDE_DATA_OUTPUT).data

    def write_aux_fpga_side_control(self, direction):
        self.write_bar_register(
            self.registers.AUX_FPGA_SIDE_DIRECTION_CONTROL(direction))

    def aux_fpga_side_control(self):
        return self.read_bar_register(
            self.registers.AUX_FPGA_SIDE_DIRECTION_CONTROL).data

    def aux_fpga_side_input(self):
        return self.read_bar_register(
            self.registers.AUX_FPGA_SIDE_DATA_INPUT).data

    def enable_aux_fpga(self):
        self.write_bar_register(self.registers.AUX_FPGA_ENABLE(enable=1))

    def disable_aux_fpga(self):
        self.write_bar_register(self.registers.AUX_FPGA_ENABLE(enable=0))

    def aux_side_input(self):
        return self.read_bar_register(
            self.registers.AUX_SIDE_DATA_INPUT).data

    def write_failsafe_limit(self, limit_in_seconds):
        self.write_bar_register(
            self.registers.FAILSAFE_LIMIT(limit_in_seconds))

    def read_failsafe_limit(self):
        return self.read_bar_register(self.registers.FAILSAFE_LIMIT).value

    def write_failsafe_control(self, value):
        self.write_bar_register(self.registers.FAILSAFE_CONTROL(value))

    def read_failsafe_control(self):
        return self.read_bar_register(self.registers.FAILSAFE_CONTROL).value

    def reset_failsafe_limit(self):
        self.write_bar_register(self.registers.FAILSAFE_CONTROL(1))
        self.write_bar_register(self.registers.FAILSAFE_CONTROL(0))

    def rc_root_i2c_lock_tiu(self, ms_timeout=RC_ROOT_I2C_TIMEOUT_MS):
        tiu = 2
        self.rc_root_i2c_lock(ms_timeout)
        hil.rcRootI2cMuxSelect(selection=tiu)
        hil.bpRootI2cSwitchSelect(1)

    def rc_root_i2c_lock_fp(self, ms_timeout=0xFFFFFFFF):
        front_panel = 0
        self.rc_root_i2c_lock(ms_timeout)
        hil.rcRootI2cMuxSelect(selection=front_panel)

    def write_ddr_controller_reset(self, data):
        controller = self.registers.DDR_CONTROLLER()
        controller.reset = data
        self.write_bar_register(controller)

    def general_purpose_output_reg(self):
        return self.read_bar_register(
            self.registers.GENERAL_PURPOSE_OUTPUT)

    def general_purpose_input_reg(self):
        return self.read_bar_register(
            self.registers.GENERAL_PURPOSE_INPUT)

    def write_rc_fp_gpio_control(self, control):
        direction_control = self.read_bar_register(
            self.registers.RC_FRONT_PANEL_GPIO_CONTROL)
        direction_control.rc_front_panel_gpio_direction_control = control
        self.write_bar_register(direction_control)

    def write_rc_fp_gpio_data(self, data):
        register = self.read_bar_register(
            self.registers.RC_FRONT_PANEL_GPIO_CONTROL)
        register.rc_front_panel_gpio_data_output = data
        self.write_bar_register(register)

    def rc_fp_gpio_input(self):
        input = self.read_bar_register(
            self.registers.RC_FRONT_PANEL_SPARE_GPIO_INPUT)
        return input.rc_front_panel_gpio_data_input

    def write_rc_fpga_spare_control(self, control):
        direction_control = self.read_bar_register(
            self.registers.RC_FPGA_SPARE_GPIO_CONTROL)
        direction_control.rc_fpga_spare_gpio_direction_control = control
        self.write_bar_register(direction_control)

    def write_rc_fpga_spare_data(self, data):
        register = self.read_bar_register(
            self.registers.RC_FPGA_SPARE_GPIO_CONTROL)
        register.rc_fpga_spare_gpio_data_output = data
        self.write_bar_register(register)

    def rc_fpga_spare_input(self):
        input = self.read_bar_register(
            self.registers.RC_FPGA_SPARE_GPIO_INPUT)
        return input.rc_fpga_spare_gpio_data_input

    def ddr_controller_reset(self):
        return self.read_bar_register(self.registers.DDR_CONTROLLER).reset

    def ddr_status_register(self):
        return self.read_bar_register(self.registers.DDR_STATUS)

    def is_ddr_reset_done_after_wait(self, retries=0xFFFFFF):
        for retry in range(retries):
            if self.is_controller_reset_done():
                return True
        else:
            return False

    def is_controller_reset_done(self):
        reset_done = self.ddr_status_register().reset_done
        return True if reset_done else False

    def issue_ddr_reset(self):
        self.write_ddr_controller_reset(1)
        self.write_ddr_controller_reset(0)

    def is_status_calibration_after_wait(self, retries=0xFFFFFF):
        for retry in range(retries):
            if self.is_ddr_calibrated():
                return True
        else:
            return False

    def is_ddr_calibrated(self):
        calibration_success = self.ddr_status_register().calibration_success
        return True if calibration_success else False

    def chip_id(self):
        upper = self.read_bar_register(self.registers.CHIP_ID_UPPER).value
        lower = self.read_bar_register(self.registers.CHIP_ID_LOWER).value
        return upper, lower

    def compatibility(self):
        compatibility = self.read_bar_register(
            self.registers.COMPATIBILITY).hardware_compatibility
        try:
            return 'abcdef'[compatibility]
        except IndexError:
            raise CompatibilityIndexError(f'Invalid Compatibility. '
                                          f'Valid values are 0 to 5 (a:f)')

    def config_fab(self):
        fab_path = self.fpga_image_path()
        search = re.search('(?<=fab_)(.)', fab_path.lower())
        if search is None:
            return ' '
        else:
            return search.group(1)

    def send_broadcast_trigger(self, trigger):
        self.write_bar_register(self.registers.BROADCAST_TRIGGER_DOWN(
            value=trigger))

    def verify_last_broadcasted_trigger(self, expected_trigger):
        last_broadcasted_trigger = self.read_last_broadcasted_trigger()
        if last_broadcasted_trigger != expected_trigger:
            raise self.LastBroadcastedTriggerError(
                f'Last Broadcasted Trigger register did not receive trigger '
                f'(expected, actual): '
                f'0x{expected_trigger:08X}, 0x{last_broadcasted_trigger:08X}')

    def read_last_broadcasted_trigger(self):
        return self.read_bar_register(
            self.registers.LAST_BROADCASTED_TRIGGER).trigger

    def read_broadcast_trigger_down(self):
        return self.read_bar_register(
            self.registers.BROADCAST_TRIGGER_DOWN).trigger

    def power_on_count(self):
        return self.read_bar_register(self.registers.POWER_ON_COUNT).value

    def write_fpga_debug_control(self, control):
        fpga_debug = self.read_bar_register(self.registers.FPGA_DEBUG)
        fpga_debug.debug_bus_direction_control = control
        self.write_bar_register(fpga_debug)

    def fpga_debug_control(self):
        fpga_debug = self.read_bar_register(self.registers.FPGA_DEBUG)
        return fpga_debug.debug_bus_direction_control

    def write_fpga_debug_data(self, data):
        fpga_debug = self.read_bar_register(self.registers.FPGA_DEBUG)
        fpga_debug.debug_bus_output = data
        self.write_bar_register(fpga_debug)

    def fpga_debug_data(self):
        fpga_debug = self.read_bar_register(self.registers.FPGA_DEBUG)
        return fpga_debug.debug_bus_output

    def fpga_debug_input(self):
        fpga_debug = self.read_bar_register(self.registers.FPGA_DEBUG)
        return fpga_debug.debug_bus_input_values

    def interrupt_status_reg(self):
        return

    def clear_safety_mode_interrupt_status(self, num_retries=50):
        for retry in range(num_retries):
            status = self.read_bar_register(self.registers.INTERRUPT_STATUS)
            if status.safety_mode_interrupt_pending == 0:
                return True

            status.safety_mode_interrupt_pending = 1
            self.write_bar_register(status)
            time.sleep(0.1)

            status = self.read_bar_register(self.registers.INTERRUPT_STATUS)
            if status.safety_mode_interrupt_pending == 0:
                return True
        else:
            return False

    def enable_safety_mode_interrupt_bit(self, enable):
        control = self.read_bar_register(self.registers.INTERRUPT_CONTROL)
        control.safety_mode_interrupt_enable = int(enable)
        self.write_bar_register(control)

    def enable_trigger_interrupt_bit(self, enable):
        control = self.read_bar_register(self.registers.INTERRUPT_CONTROL)
        control.trigger_interrupt_enable = int(enable)
        self.write_bar_register(control)

    def tiu_interrupt_enable(self, enable):
        hil.rc3TiuAlarmEnable(enable)

    def read_interrupt_status(self):
        reg = self.read_bar_register(self.registers.INTERRUPT_STATUS)
        tigger_interrupt_status = reg.trigger_interrupt_pending
        safety_mode_interrupt_status = reg.safety_mode_interrupt_pending
        return safety_mode_interrupt_status, tigger_interrupt_status

    def rc_loopback_exists(self):
        try:
            hil.rc3LbConnect()
            return True
        except RuntimeError:
            return False

    class LastBroadcastedTriggerError(Exception):
        pass

    def read_lmk04832_using_hil(self, offset):
        return hil.rc3Lmk04832Read(offset)

    def write_lmk04832_using_hil(self, offset, data):
        hil.rc3Lmk04832Write(offset, data)

    def write_acc_card_io_control(self, control):
        reg = self.registers.ACCESSORY_CARD_IO_CONTROL(control=control)
        self.write_bar_register(reg)

    def acc_card_io_control(self):
        acc_card_io = self.read_bar_register(
            self.registers.ACCESSORY_CARD_IO_CONTROL)
        return acc_card_io.control

    def write_acc_card_io_output(self, data):
        reg = self.registers.ACCESSORY_CARD_IO_OUTPUT(data=data)
        self.write_bar_register(reg)

    def acc_card_io_output(self):
        reg = self.read_bar_register(self.registers.ACCESSORY_CARD_IO_OUTPUT)
        return reg.data

    def acc_card_io_input(self):
        reg = self.read_bar_register(self.registers.ACCESSORY_CARD_IO_INPUT)
        return reg.data

    def psBltBoardRead(self, slot):
        return hil.psBltBoardRead(slot)

    def write_relays_gpio_i2c_configuration_reg(self, data):
        self.write_bar_register(self.registers.RELAYS_GPIO_I2C_CONFIGURATION(data))

    def read_relays_gpio_i2c_configuration_reg(self):
        return self.read_bar_register(
            self.registers.RELAYS_GPIO_I2C_CONFIGURATION)

    def fill_up_generic_i2c_tx_fifo(self, tx_fifo_depth, index):
        for i in range(tx_fifo_depth):
            self.write_bar_register(self.registers.GENERIC_I2C_TX_FIFO(0xFF), index=index)

    def read_generic_i2c_tx_fifo_count(self, index):
        return self.read_bar_register(
            self.registers.GENERIC_I2C_STATUS, index=index).transmit_fifo_count

    def reset_generic_i2c(self, index):
        self.write_bar_register(self.registers.GENERIC_I2C_RESET(1), index=index)
        self.write_bar_register(self.registers.GENERIC_I2C_RESET(0), index=index)

    def initialize_ad5064(self):
        self.Log('info', f'Initializing VOUT-A of AD5064 to 2.5V')
        hil.rc3Ad5064Write(command=3, address=0, data=0x7FFF)


class CompatibilityIndexError(Exception):
    pass


