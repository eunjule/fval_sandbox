# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

import re
import time

from ThirdParty.HPS.hps_diskless_boot import BootHps

from Common import configs
from Common.fval import Object
from Common.utilities import compliment_bytes
from Common.triggers import is_software_trigger
from Common.testbench import s2h_simulator
from Common.testbench.dma_simulator import DmaSimulator
from Common.testbench.i2c_controller_simulator import I2CInterfaceSimulator
from Common.testbench.pmbus_simulator import\
    create_pmbus_devices as create_ltm4680_devices
from Rc3.instrument import rc3_register
from Rc3.instrument.bps_i2c_interface import I2C_ADDR as BPS_I2C_ADDR,\
    MAX_NUM_BPS
from Rc3.instrument.front_panel_i2c_interface import I2C_ADDR as FP_I2C_ADDR
from Rc3.instrument.generic_i2c_interface import I2C_ADDR as GENERIC_I2C_ADDR,\
    GENERIC_GROUPS
from Rc3.instrument.pmbus_monitor_i2c_interface import LTM4680_I2C_ADDR,\
    MAX_NUM_INTERFACES as MAX_NUM_PMBUS_MONITOR_INTERFACES,\
    PMBUS_MONITOR_I2C_STATUS,\
    PMBUS_MONITOR_I2C_CONTROL,\
    PMBUS_MONITOR_I2C_TX_FIFO,\
    PMBUS_MONITOR_I2C_RX_FIFO,\
    PMBUS_MONITOR_I2C_RESET
from Rc3.instrument.tiu_i2c_interface import I2C_ADDR as TIU_I2C_ADDR,\
    MAX_NUM_TIU
from Rc3.testbench.bps_pmbus_simulator import \
    create_pmbus_devices as create_bps_devices
from Rc3.testbench.front_panel_simulator import \
    create_fp_devices as create_fp_devices
from Rc3.testbench.generic_i2c_simulator import create_generic_devices
from Rc3.testbench.lmk04832_controller_simulator import Lmk04832
from Rc3.testbench.ltc2358_simulator import Ltc2358
from Rc3.testbench.max1230_simulator import Max1230
from Rc3.testbench.scopeshot_engine_simulator import ScopeShotEngine
from Rc3.testbench.tiu_simulator import create_tiu_devices
from Rc3.instrument.failsafe import FAILSAFE_LIMIT_DEFAULT


def simulate_from_file_method_calls(rc):
    rc.hg_id_from_file = lambda: Rc3.HG_ID_SIM
    rc.hg_clean_from_file = lambda: Rc3.HG_CLEAN_SIM


class Rc3(Object):
    POWER_ON_TIME = 0
    HG_ID_SIM = 0x3BA8_3D0158AE
    HG_CLEAN_SIM = 1

    def __init__(self, system):
        super().__init__()

        self.hdmt_simulator = system
        self.lmk04832 = Lmk04832()
        self.ltm4680 = \
            create_ltm4680_devices(MAX_NUM_PMBUS_MONITOR_INTERFACES)[0]
        self.power_supplies = create_bps_devices(MAX_NUM_BPS)
        self.tiu = create_tiu_devices(MAX_NUM_TIU)
        self.generic_i2c_devices = create_generic_devices(GENERIC_GROUPS)
        self.front_panel = create_fp_devices()
        self.memory_table = {}
        self.s2h = s2h_simulator.S2h(rc3_register)
        self.scopeshot_engine = ScopeShotEngine(self)

        self.registers = {
            0x24: Rc3.HG_ID_SIM & 0xFFFFFFFF,
            0x28: (Rc3.HG_CLEAN_SIM << 31) | (Rc3.HG_ID_SIM >> 32) & 0xFFFF,
            0x30: 0xA000_B000,
            0x34: 0xC000_D000,
            0x8C: 0x4,
            0xA0: 0x0,
            0xA4: 0x0,
            0x400: 0x3F_FFFE,
            0x0404: 0x0,
            0x410: 0x0,
            0x414: 0x3,
            0x40C: 0x20_0000,
            0x420: 0x0,
            0x440: 0x0,
            0x434: 0x0,
            0x444: 0x6,
            0x4000: BootHps.HPS2SYS_SYNC_0,
            0x4E0: 264,
            0x4E4: 264,
            0x688: 0x0,
            0xa04: 0x3400,
            0xA80: 0,
            0xB00: 0x0,
            0xB04: FAILSAFE_LIMIT_DEFAULT}

        self._initialize_clock_frequency_measurement_registers()
        self.read_actions = {
            0x44: self.lmk04832.read,
            0x48: self.lmk04832.read,
            0x404: self.read_fpga_debug,
            0x410: self.read_interrupt_pending_status,
            0x418: self.power_on_count_read,
            0x424: self.rc_fpga_spare_input,
            0x42c: self.rc_fp_gpio_input,
            0x444: self.ddr4_status,
            0x68C: self.read_data_input_register,
            0x698: self.read_data_input_register,
            0x6A8: self.read_acc_card_io_input,
            0xA80: self.read_live_time_stamp
        }
        self.write_actions = {
            0x40: self.lmk04832.write,
            0x4C: self.lmk04832.write,
            0x80: self.write_trigger_sync,
            0xA0: self.ad5064_command_data,
            0x410: self.interrupt_status,
            0x0E0: self.send_broadcast_trigger_down,
            0x4000: self.write_hps_msg_mailbox
        }
        self._setup_bps_interfaces()
        self._setup_generic_i2c_interfaces()
        self._setup_tiu_interfaces()
        self._setup_fp_interfaces()
        self._setup_ltm4680_interface()
        self._setup_s2h()
        self._set_compatibility()
        self._set_fpga_version()
        self._setup_software_trigger_interface()
        self._setup_aurora()
        self._setup_ltc2358_interface()
        self._setup_trigger_actions()
        self._setup_scopeshot_engine()
        self._setup_dma()

        self.max1230 = Max1230(self)

    def bar_write(self, bar, offset, data):
        self.write_actions.get(offset, self.default_write_action)(offset, data)

    def bar_read(self, bar, offset):
        data = self.read_actions.get(offset, self.default_read_action)(offset)
        return data

    def default_write_action(self, offset, data):
        self.registers[offset] = data

    def default_read_action(self, offset):
        if offset not in self.registers:
            self.registers[offset] = 0x0

        return self.registers[offset]

    def fpga_version(self):
        return self.registers[0x20]

    def fpga_version_string(self):
        version = self.fpga_version()
        return f'{version >> 16}.{version & 0xFFFF}'

    def cpld_version_string(self):
        return 'CPLD SIM'

    def _setup_ltm4680_interface(self):
        self.ltm4680_interface = I2CInterfaceSimulator(
            i2c_addr=LTM4680_I2C_ADDR,
            base_offset=PMBUS_MONITOR_I2C_STATUS.ADDR,
            write_cmd=self.ltm4680.pmbus_write,
            read_cmd=self.ltm4680.pmbus_read)
        self._setup_ltm4680_write_actions()
        self._setup_ltm4680_read_actions()

    def _setup_ltm4680_write_actions(self):
        self.write_actions[PMBUS_MONITOR_I2C_CONTROL.ADDR] =\
            self.ltm4680_interface.transmit
        self.write_actions[PMBUS_MONITOR_I2C_TX_FIFO.ADDR] =\
            self.ltm4680_interface.push_data
        self.write_actions[PMBUS_MONITOR_I2C_RESET.ADDR] =\
            self.ltm4680_interface.reset

    def _setup_ltm4680_read_actions(self):
        self.read_actions[PMBUS_MONITOR_I2C_STATUS.ADDR] =\
            self.ltm4680_interface.update_status_reg
        self.read_actions[PMBUS_MONITOR_I2C_RX_FIFO.ADDR] =\
            self.ltm4680_interface.pop_data

    def _setup_ltc2358_interface(self):
        self.ltc2358 = Ltc2358()
        self._setup_ltc2358_write_actions()
        self._setup_ltc2358_read_actions()

    def _setup_ltc2358_write_actions(self):
        reg_count = rc3_register.LTC2358_SOFTSPAN_CODE.REGCOUNT
        offsets = [rc3_register.LTC2358_SOFTSPAN_CODE.address(i)
                   for i in range(reg_count)]
        for offset in offsets:
            self.write_actions[offset] = self.ltc2358.write_softspan_code


    def _setup_ltc2358_read_actions(self):
        count = rc3_register.LTC2358_CHANNEL_DATA_0.REGCOUNT
        offsets_0 = [rc3_register.LTC2358_CHANNEL_DATA_0.address(i)
                     for i in range(count)]
        offsets_1 = [rc3_register.LTC2358_CHANNEL_DATA_1.address(i)
                     for i in range(count)]

        for channel in range(count):
            self.read_actions[offsets_0[channel]] = self.ltc2358.channel_data_0
            self.read_actions[offsets_1[channel]] = self.ltc2358.channel_data_1

        count = rc3_register.LTC2358_SOFTSPAN_CODE.REGCOUNT
        offsets = [rc3_register.LTC2358_SOFTSPAN_CODE.address(i)
                   for i in range(count)]
        for offset in offsets:
            self.read_actions[offset] = self.ltc2358.softspan_code

    def _setup_bps_interfaces(self):
        self.bps_interfaces = [I2CInterfaceSimulator(
            i2c_addr=BPS_I2C_ADDR,
            base_offset=rc3_register.BPS_I2C_STATUS.ADDR,
            write_cmd=self.power_supplies[i].pmbus_write,
            read_cmd=self.power_supplies[i].pmbus_read)
            for i in range(MAX_NUM_BPS)]
        self._setup_bps_write_actions()
        self._setup_bps_read_actions()

    def _setup_bps_write_actions(self):
        for i in range(MAX_NUM_BPS):
            self.write_actions[0x524] = \
                self.bps_interfaces[i].transmit
            self.write_actions[0x528] = \
                self.bps_interfaces[i].push_data
            self.write_actions[0x530] = \
                self.bps_interfaces[i].reset

    def _setup_bps_read_actions(self):
        for i in range(MAX_NUM_BPS):
            self.read_actions[0x520] = \
                self.bps_interfaces[i].update_status_reg
            self.read_actions[0x52c] = \
                self.bps_interfaces[i].pop_data

    def _setup_tiu_interfaces(self):
        self.tiu_interfaces = [I2CInterfaceSimulator(
            i2c_addr=TIU_I2C_ADDR,
            base_offset=rc3_register.TIU_I2C_STATUS.ADDR,
            write_cmd=self.tiu[i].write,
            read_cmd=self.tiu[i].read)
            for i in range(MAX_NUM_TIU)]
        self._setup_tiu_write_actions()
        self._setup_tiu_read_actions()

    def _setup_tiu_write_actions(self):
        for i in range(MAX_NUM_TIU):
            self.write_actions[0x504] = \
                self.tiu_interfaces[i].transmit
            self.write_actions[0x508] = \
                self.tiu_interfaces[i].push_data
            self.write_actions[0x510] = \
                self.tiu_interfaces[i].reset

    def _setup_tiu_read_actions(self):
        for i in range(MAX_NUM_TIU):
            self.read_actions[0x500] = \
                self.tiu_interfaces[i].update_status_reg
            self.read_actions[0x50c] = \
                self.tiu_interfaces[i].pop_data

    def _setup_generic_i2c_interfaces(self):
        self.generic_interfaces = [I2CInterfaceSimulator(
            i2c_addr=GENERIC_I2C_ADDR,
            base_offset=rc3_register.GENERIC_I2C_STATUS.ADDR,
            write_cmd=self.generic_i2c_devices[i].write,
            read_cmd=self.generic_i2c_devices[i].read)
            for i in range(GENERIC_GROUPS)]
        self._setup_generic_i2c_write_actions()
        self._setup_generic_i2c_read_actions()

    def _setup_generic_i2c_write_actions(self):
        for i in range(GENERIC_GROUPS):
            self.write_actions[0x704 + 0X20 * i] = self.generic_transmit
            self.write_actions[0x708 + 0X20 * i] = self.generic_push_data
            self.write_actions[0x710 + 0X20 * i] = self.generic_reset

    def _setup_generic_i2c_read_actions(self):
        for i in range(GENERIC_GROUPS):
            self.read_actions[0x700 + 0X20 * i] = \
                self.generic_interfaces[i].update_status_reg
            self.read_actions[0x70c + 0X20 * i] = \
                self.generic_interfaces[i].pop_data

    def _setup_fp_interfaces(self):
        self.fp_interfaces = I2CInterfaceSimulator(
            i2c_addr=FP_I2C_ADDR, base_offset=rc3_register.FP_I2C_STATUS.ADDR,
            write_cmd=self.front_panel.write,
            read_cmd=self.front_panel.read)
        self._setup_fp_write_actions()
        self._setup_fp_read_actions()

    def _setup_fp_write_actions(self):
        self.write_actions[0x544] = self.fp_interfaces.transmit
        self.write_actions[0x548] = self.fp_interfaces.push_data
        self.write_actions[0x550] = self.fp_interfaces.reset

    def _setup_fp_read_actions(self):
        self.read_actions[0x540] = self.fp_interfaces.update_status_reg
        self.read_actions[0x54c] = self.fp_interfaces.pop_data

    def _setup_software_trigger_interface(self):
        self.sw_trigger_fifo = []
        self.sw_trigger_source = []
        self.read_actions.update(
            {0xA28: self.read_sw_trigger_data,
             0xA2C: self.read_sw_trigger_source,
             0xA24: self.read_sw_trigger_count})
        self.write_actions.update(
            {0xA20: self.write_sw_trigger_control})

    def _setup_s2h(self):
        self.read_actions.update(self.s2h.read_actions)
        self.write_actions.update(self.s2h.write_actions)

    def _setup_trigger_actions(self):
        num_links = rc3_register.AURORA_TRIGGER_DOWN.REGCOUNT

        actions = {rc3_register.AURORA_TRIGGER_DOWN.address(link):
                       self.write_trigger_down_by_offset
                   for link in range(num_links)}
        self.write_actions.update(actions)

        actions = {rc3_register.AURORA_TRIGGER_DOWN.address(link):
                       self.read_trigger_down_by_offset
                   for link in range(num_links)}
        self.read_actions.update(actions)

        actions = {rc3_register.AURORA_TRIGGER_UP.address(link):
                       self.write_trigger_up_by_offset
                   for link in range(num_links)}
        self.write_actions.update(actions)

        actions = {rc3_register.AURORA_TRIGGER_UP.address(link):
                       self.read_trigger_up_by_offset
                   for link in range(num_links)}
        self.read_actions.update(actions)

        actions = {rc3_register.AURORA_CONTROL.address(link):
                       self.write_aurora_control
                   for link in range(num_links)}
        self.write_actions.update(actions)

        actions = {rc3_register.AURORA_TRIGGER_UP_SIM.address(link):
                       self.write_simulated_up_trigger
                   for link in range(num_links)}
        self.write_actions.update(actions)

        self.hddps_down_trigger_buffer = {i: [] for i in range(num_links)}

        action = {0xE4: self.read_last_broadcasted_trigger}
        self.read_actions.update(action)

    def _setup_scopeshot_engine(self):
        self.read_actions.update(self.scopeshot_engine.read_actions)
        self.write_actions.update(self.scopeshot_engine.write_actions)

    def _setup_dma(self):
        self.dma = DmaSimulator()
        self.dma_write = self.dma.dma_write
        self.dma_read = self.dma.dma_read

    def hps_dma_write(self, address, data):
        self.hps_ram = data

    def write_hps_msg_mailbox(self, offset, data):
        if data == BootHps.SYS2HPS_SYNC_1:
            self.registers[offset] = BootHps.HPS2SYS_UBOOT_WAIT
        elif data == BootHps.SYS2HPS_UBOOT_DONE:
            self.registers[offset] = BootHps.HPS2SYS_LINUX_WAIT
        else:
            self.registers[offset] = data

    def ad5064_command_data(self, offset, data):
        ad5064_vref = 5
        channel_7_addr =  0x47C
        command_data_register = rc3_register.AD5064_COMMAND_DATA(data)
        if command_data_register.command == 3:
            voltage = (ad5064_vref/ 2 ** 16) * command_data_register.data
            self.registers.update({
                channel_7_addr: self.voltage_to_raw(7, voltage)})
        elif command_data_register.command == 7:
            self.registers.update({
                channel_7_addr: self.voltage_to_raw(7, 2.5)})
        else:
            self.default_write_action(offset, data)

    def _initialize_clock_frequency_measurement_registers(self):
        base_offset = rc3_register.CLOCK_FREQUENCY_MEASUREMENT.ADDR

        self.registers[base_offset + 0x00] = 0x1fffff6
        self.registers[base_offset + 0x04] = 0x1fffff6
        self.registers[base_offset + 0x08] = 0x1fff444
        self.registers[base_offset + 0x0C] = 0x1fff444
        self.registers[base_offset + 0x10] = 0x1fffff6
        self.registers[base_offset + 0x14] = 0x1fffff6
        self.registers[base_offset + 0x18] = 0x1fffff6
        self.registers[base_offset + 0x1c] = 0x1fffff6

    def generic_transmit(self, offset, data):
        i = (offset - 0x700) // 0x20
        self.registers[offset] = data
        self.generic_interfaces[i].transmit(offset, data)

    def generic_push_data(self, offset, data):
        i = (offset - 0x700) // 0x20
        self.registers[offset] = data
        self.generic_interfaces[i].push_data(offset, data)

    def generic_reset(self, offset, data):
        i = (offset - 0x700) // 0x20
        self.registers[offset] = data
        self.generic_interfaces[i].reset(offset, data)

    def rc3_tmon_read(self, channel):
        if channel == 0:
            rc_temperature_register = 0xA04
            return self.registers[rc_temperature_register]/256
        elif channel == 1:
            max1230_temperature_register = 0x4E0
            return self.registers[max1230_temperature_register] * 0.125
        elif channel == 2:
            max1230_temperature_register = 0x4E4
            return self.registers[max1230_temperature_register] * 0.125
        else:
            return None

    def rc3_vmon_read(self, channel):
        return self.max1230.read_channel(channel)

    def read_data_input_register(self, offset):
        aux_control = 0x688
        output_data = 0x684
        return self.registers[aux_control] & \
            self.registers[output_data] & 0xFFF

    def rc3_ad5064_write(self, command, address, data):
        ad5064_vref = 5
        if command == 3:
            data = ((ad5064_vref / 2 ** 16) * data) / 0.001
            self.bar_write(bar=1, offset=0x47C, data=data)
        elif command == 7:
            default_2p5V = int(2.5/0.001)
            self.bar_write(bar=1, offset=0x47C, data=default_2p5V)
        else:
            pass

    def voltage_to_raw(self, channel, voltage):
        default = 1.0
        scales = {12: -2.49, 13: -2.49, 14: 5.99, 16: 2.0, 23: 5.99,
                  27: (1.5 + 0.499) / 0.499, 28: -2.74, 29: -2.74,
                  30: 5.99}
        return int(voltage / (0.001 * scales.get(channel, default)))

    def update_aurora_slots(self, slots):
        if type(slots) != list:
            slots = [slots]
        for slot in slots:
            if slot is not None:
                status_reg = rc3_register.AURORA_STATUS()
                status_reg.transceiver_pll_locked = 1
                status_reg.tx_pll_locked = 1
                status_reg.channel_up = 1
                status_reg.lane_up = 1
                status_reg.soft_error = 0
                status_reg.hard_error = 0

                self.registers[rc3_register.AURORA_STATUS.address(slot)] = \
                    status_reg.value

    def ddr4_status(self, offset):
        return 0x0 if self.registers[0x440] & 0b1 else\
            0b110

    def _set_compatibility(self):
        fab_num = self.config_fab()
        self.registers[0x2c] = 'abcdef'.find(fab_num) & 0xFFFF_FFFF

    def config_fab(self):
        fab_path = configs.RC3_FPGA_IMAGE_FAB_A
        search = re.search('(?<=fab_)(.)', fab_path.lower())
        if search is None:
            return ' '
        else:
            return search.group(1)

    def _set_fpga_version(self):
        fpga_version = self.get_version_number_from_config_file_name()
        self.registers[0x20] = fpga_version

    def get_version_number_from_config_file_name(self):
        config_file_name = configs.RC3_FPGA_IMAGE_FAB_A
        if config_file_name.rfind('_v') == -1:
            self.log_error_invalid_config_file_name(config_file_name)
            return -1

        temp = config_file_name.rsplit('_v')
        version_substring = temp[len(temp)-1]
        if version_substring.rfind('.') == -1:
            self.log_error_invalid_config_file_name(config_file_name)
            return -1

        temp = version_substring.rsplit('.')
        if len(temp) < 3:
            self.log_error_invalid_config_file_name(config_file_name)
            return -1

        major = int(temp[0]) << 16
        minor = int(temp[1]) & 0xFFFF
        return major | minor

    def log_error_invalid_config_file_name(self, config_file_name):
        self.Log('error',
                 f'Invalid file name format \"{config_file_name}\". '
                 f'Expecting image name to contain '
                 f'\"_v<major>.<minor>.<hw compatibility>.rbf\"')

    def read_trigger_down_by_offset(self, offset):
        return self.registers.get(offset, 0)

    def read_trigger_down(self, slot):
        offset = rc3_register.AURORA_TRIGGER_DOWN.address(slot)
        return self.read_trigger_up_by_offset(offset)

    def write_trigger_down_by_offset(self, offset, data):
        self.registers[offset] = data

        if not is_software_trigger(data):
            self.write_trigger_up_by_offset(offset - 4, data)

    def write_trigger_down(self, slot, trigger):
        offset = rc3_register.AURORA_TRIGGER_DOWN.address(slot)
        self.write_trigger_down_by_offset(offset, trigger)

    def read_trigger_up_by_offset(self, offset):
        return self.registers.get(offset, 0)

    def read_trigger_up(self, slot):
        offset = rc3_register.AURORA_TRIGGER_UP.address(slot)
        return self.read_trigger_up_by_offset(offset)

    def read_hddps_down_trigger_buffer(self, slot):
        if len(self.hddps_down_trigger_buffer[slot]):
            return self.hddps_down_trigger_buffer[slot].pop(0)
        else:
            return 0

    def write_trigger_up_by_offset(self, offset, data):
        self.registers[offset] = data
        hddps_fifo_depth = 256  # HDDPS HLD V0.15 "Trigger Down"

        # If software data, update software trigger fifo
        if is_software_trigger(data):
            self.update_software_trigger_fifo(offset, data)
        else:  # if not software data, redirect to down trigger for broadcast
            self.registers[offset + 4] = data
            slot = rc3_register.AURORA_TRIGGER_DOWN.index(offset + 4)
            if len(self.hddps_down_trigger_buffer[slot]) < hddps_fifo_depth:
                self.hddps_down_trigger_buffer[slot].append(data)
            self.registers[0xE4] = data  # last broadcast trigger register

    def write_trigger_up(self, slot, trigger):
        offset = rc3_register.AURORA_TRIGGER_UP.address(slot)
        self.write_trigger_up_by_offset(offset, trigger)
        self.scopeshot_engine.process_trigger(link_num=slot, trigger=trigger)

    def write_aurora_control(self, offset, data):
        self.registers[offset] = data

    def write_simulated_up_trigger(self, offset, trigger):
        link_num = rc3_register.AURORA_TRIGGER_UP_SIM.index(offset)
        self.write_trigger_up(link_num, trigger)

    def receive_up_trigger(self, slot, trigger):
        rc_offset = rc3_register.AURORA_TRIGGER_UP.address(slot)
        self.bar_write(rc3_register.AURORA_TRIGGER_UP.BAR, rc_offset, trigger)

        if not is_software_trigger(trigger):
            rc_offset = rc3_register.AURORA_TRIGGER_DOWN.address(slot)
            self.bar_write(rc3_register.AURORA_TRIGGER_DOWN.BAR, rc_offset,
                           trigger)

    def update_software_trigger_fifo(self, offset, trigger):
        fifo_depth = 1024
        if len(self.sw_trigger_fifo) < fifo_depth:
            slot = rc3_register.AURORA_TRIGGER_UP.index(offset)
            self.sw_trigger_fifo.append(trigger)
            self.sw_trigger_source.append(slot)

    def send_broadcast_trigger_down(self, offset, data):
        self.registers[offset] = data

        # Update LAST_BROADCASTED_TRIGGER register
        self.registers[rc3_register.LAST_BROADCASTED_TRIGGER.ADDR] = data

        # Update individual trigger down links so that sim instruments can read
        # it
        for slot in range(rc3_register.AURORA_TRIGGER_DOWN.REGCOUNT):
            self.write_trigger_down(slot, data)

        # process if trigger is a Star/Stop scopeshot event
        self.scopeshot_engine.process_trigger(link_num=None, trigger=data)

    def read_last_broadcasted_trigger(self, offset):
        return self.registers[0xE4]

    def rc_fp_gpio_input(self, offset):
        rc_fp_gpio_control = (self.registers[0x428] >> 16) & 0b11
        rc_fp_gpio_output = self.registers[0x428] & 0b11
        return rc_fp_gpio_control & rc_fp_gpio_output

    def rc_fpga_spare_input(self, offset):
        rc_fpga_spare_control = (self.registers[0x420] >> 16) & 0b1
        rc_fpga_spare_output = self.registers[0x420] & 0b1
        return rc_fpga_spare_control & rc_fpga_spare_output

    def rc3_fpga_load(self, image):
        Rc3.POWER_ON_TIME = int(time.clock())

    def read_fpga_debug(self, offset):
        output_direction = 1
        fpga_debug = self.registers.get(0x404, 0)
        control = (fpga_debug >> 8) & 0xFF
        output = fpga_debug & 0xFF

        input_reg = 0
        bit_size = 8
        for bit_num in range(bit_size):
            control_bit = (control >> bit_num) & 1
            output_bit = (output >> bit_num) & 1
            clear_bit_mask = compliment_bytes(num_to_convert=1 << bit_num,
                                              num_bytes=1)

            if control_bit == output_direction:
                if output_bit:
                    input_reg |= (1 << bit_num)
                else:
                    input_reg &= clear_bit_mask
            else:
                input_reg |= (1 << bit_num)

        return input_reg << 16 | control << 8 | output

    def read_acc_card_io_input(self, offset):
        output_direction = 1
        control = self.registers.get(0x6A4, 0)
        output = self.registers.get(0x6A0, 0)

        input_reg = 0
        bit_size = 32
        for bit_num in range(bit_size):
            control_bit = (control >> bit_num) & 1
            output_bit = (output >> bit_num) & 1
            clear_bit_mask = compliment_bytes(num_to_convert=1 << bit_num,
                                              num_bytes=4)

            if control_bit == output_direction:
                if output_bit:
                    input_reg |= (1 << bit_num)
                else:
                    input_reg &= clear_bit_mask
            else:
                if bit_num >= (bit_size-8):  #MSB zero @ acc_card_io_input
                    input_reg &= clear_bit_mask
                else:
                    input_reg |= (1 << bit_num)
        return input_reg

    def convert_int_to_binary_string(self, bit_size, data):
        bin_string = bin(data)

        # Remove first to letters '0b'
        return bin_string[2: len(bin_string)].zfill(bit_size)

    def tiu_spare_gpio(self, gpio, state):
        if gpio == 69 and state:
            if self.is_safety_mode_interrupt_enabled():
                self.registers[0x410] |= 0b10

    def is_safety_mode_interrupt_enabled(self):
        if self.registers[0x414] & 0b10:
            return True
        return False

    def is_trigger_interrupt_enabled(self):
        if self.registers[0x414] & 0b1:
            return True
        return False

    def read_interrupt_pending_status(self, offset):
        if bool(len(self.sw_trigger_fifo)) & \
                self.is_trigger_interrupt_enabled():
            self.registers[0x410] |= 0b01
        else:
            self.registers[0x410] &= 0xFFFFFFFE
        return self.registers[0x410]

    def interrupt_status(self, offset, data):
        if self.registers[offset] & 0b1:
            self.registers[offset] ^= (data & 0b1)

        if self.registers[offset] & 0b10:
            self.registers[offset] ^= (data & 0b10)

    def write_sw_trigger_control(self, offset, data):
        self.registers[offset] = data
        if data == 1:
            self.sw_trigger_fifo.clear()
            self.sw_trigger_source.clear()

    def read_sw_trigger_count(self, offset):
        return len(self.sw_trigger_fifo)

    def read_sw_trigger_source(self, offset):
        return 0 if not self.sw_trigger_source else\
            self.sw_trigger_source[0]

    def read_sw_trigger_data(self, offset):
        sw_trigger_reset_bit = self.registers[0xA20]
        if not self.sw_trigger_fifo or sw_trigger_reset_bit:
            return 0
        else:
            self.sw_trigger_source.pop(0)
            return self.sw_trigger_fifo.pop(0)

    def _setup_aurora(self):
        # Add any necessary aurora interface attributes here
        for link in range(rc3_register.AURORA_STATUS.REGCOUNT):
            self.registers[rc3_register.AURORA_STATUS.address(link)] = \
                0x0000_0027

    def power_on_count_read(self, offset):
        current_time = int(time.clock())
        total_time_seconds = current_time - Rc3.POWER_ON_TIME
        number_of_10ms_increments = int(total_time_seconds / 0.01)
        return number_of_10ms_increments

    def write_trigger_sync(self, offset, data):
        for i in range(12):  # there are 12 possible hdmt slots
            if (data >> i) & 1:
                try:
                    hpcc = self.get_hpcc_sim(i)
                except Rc3.GetHpccError:
                    pass
                else:
                    hpcc.receive_sync_pulse()
        time_stamp_reset_bit = (data >> 31) & 1
        if time_stamp_reset_bit:
            self.reset_live_time_stamp()

    def reset_live_time_stamp(self):
        self.registers[0xA80] = 0

    def live_time_stamp(self):
        return self.registers[0xA80]

    def get_hpcc_sim(self, slot):
        for key, instrument in self.hdmt_simulator.hpcc.items():
            if instrument.slot == slot:
                if 'hpcc' not in instrument.__class__.__name__.lower():
                    raise Rc3.GetHpccError(f'Instrument at slot {slot} not of '
                                       f'Hpcc type')
                else:
                    return instrument
        else:
            raise Rc3.GetHpccError(f'No instrument found at slot {slot}')

    class GetHpccError(Exception):
        pass

    def read_live_time_stamp(self, offset=None):
        current_time = self.registers[0xA80]
        self.registers[0xA80] += 100
        return current_time

    class BLT():
        DeviceName = 'RCTC3Sim'
        VendorName = 'INTEL'
        PartNumberAsBuilt = 'AAK50927-101'
        PartNumberCurrent = 'AAK50927-101'
        SerialNumber = 'INV100250155'
        ManufactureDate = '2020JAN24'
		
