# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

"""Purpose of this module is to use it to debug Rc3

You can add imports as needed and create instances of objects.
All local variables will be available for use. For example, rc3 will be avail.
If you want to leave the interactive session, press Ctl+Z

Here's an example run:

D:\FVAL>py
Python 3.6.8 (tags/v3.6.8:3c6b436a57, Dec 24 2018, 00:16:47) [MSC v.1916 64
bit (AMD64)] on win32
Type "help", "copyright", "credits" or "license" for more information.
>>> import Rc3.testbench.command_line_debugger
20-Jun-11 19:33:29 000|I| Rc3 FPGA version: 3.0-0
Start...
Entering the interactive debugger..
>>> rc3.log_fpga_version()
20-Jun-11 19:34:06 000|I| Rc3 FPGA Version: 3.0
>>> ^Z

Exiting the interactive debugger..
End!
>>> exit()

D:\FVAL>

"""

import logging

from Common import hilmon as hil
from Common.fval import ConfigLogger, Log, SetLoggerLevel, TimeElapsedLogger
from Common.instruments.tester import get_tester
from Rc3.instrument.rc3 import Rc3

def prog_fpga():
    rc.load_fpga_using_hil()
    Log('info', f'Rc3 FPGA Version String: {hil.rc3FpgaVersionString()}')


def init_rc():
    rc.ensure_initialized()


def scopeshot_tests(start_addr=None, end_addr=None, num_entries=None):
    from Rc3.instrument.scs_interface import ScsScopeshotInterface

    rc.ensure_initialized()
    scs = ScsScopeshotInterface(rc)

    try:
        assert scs.ddr4_ending_address() == scs.DDR4_ENDING_ADDRESS_DEFAULT
        assert scs.ddr4_starting_address() == scs.DDR4_STARTING_ADDRESS_DEFAULT

        max_address = scs.DDR4_ENDING_ADDRESS_DEFAULT
        address_step = 10

        for i in range(0, address_step):
            scs.set_ddr4_ending_address(i)
            end = scs.ddr4_ending_address()
            scs.set_ddr4_starting_address(i)
            start = scs.ddr4_starting_address()
            # Log('info', f'(expected, start, end): '
            #             f'0x{i:08X}, 0x{start:08X}, 0x{end:08X}')
            assert start == end == i

        for i in range(max_address-address_step, max_address + 1):
            scs.set_ddr4_ending_address(i)
            end = scs.ddr4_ending_address()
            scs.set_ddr4_starting_address(i)
            start = scs.ddr4_starting_address()
            # Log('info', f'(expected, start, end): '
            #             f'0x{i:08X}, 0x{start:08X}, 0x{end:08X}')
            assert start == end == i

        scs.set_ddr4_starting_address(scs.DDR4_STARTING_ADDRESS_DEFAULT)
        scs.set_ddr4_ending_address(scs.DDR4_ENDING_ADDRESS_DEFAULT)

        if start_addr is not None:
            scs.set_ddr4_starting_address(start_addr)
        if end_addr is not None:
            scs.set_ddr4_ending_address(end_addr)
        if num_entries is None:
            num_entries = 10

        zero_bytes = bytes(num_entries * 8)
        start_address = scs.ddr4_starting_address()
        rc.dma_write(start_address, zero_bytes)
        read_data = rc.dma_read(start_address, num_entries * 8)
        assert zero_bytes == read_data

        scs_ddr4_data = generate_random_scs_ddr4_data(num_entries)
        time_stamps = []
        populate_ddr4_with_scopeshot_data(scs_ddr4_data, time_stamps)
        validate_ddr4_scopeshot_data(scs_ddr4_data, time_stamps)
    finally:
        scs.set_ddr4_starting_address(scs.DDR4_STARTING_ADDRESS_DEFAULT)
        scs.set_ddr4_ending_address(scs.DDR4_ENDING_ADDRESS_DEFAULT)


def populate_ddr4_with_scopeshot_data(scs_ddr4_data, time_stamps=None):
    from Common.instruments import hdmt_trigger_interface
    from Rc3.instrument.scs_interface import ScsScopeshotInterface

    scs = ScsScopeshotInterface(rc)

    scs.enable()
    scs.start_scopeshot()

    if time_stamps is None or not isinstance(time_stamps, list):
        time_stamps = []
    for data in scs_ddr4_data:
        link = data.link_num
        scs_trigger = hdmt_trigger_interface.generate_scs_trigger(
            slice=data.ac_slice_id,
            pin=data.pin_id,
            user=data.user_id,
            data=data.sensor_data)
        rc.send_up_trigger_bypass(link, scs_trigger)
        time_stamps.append(rc.live_time_stamp())

    scs.stop_scopeshot()
    scs.disable()


def validate_ddr4_scopeshot_data(scs_ddr4_data, time_stamps):
    from Common.fval import TimeElapsedLogger
    from Rc3.instrument.scs_interface import ScsScopeshotInterface

    scs = ScsScopeshotInterface(rc)
    start_address = scs.ddr4_starting_address()
    num_entries = len(scs_ddr4_data)

    assert num_entries == len(time_stamps)

    diffs = []
    with TimeElapsedLogger(f'DMA Bulk Read'):
        ddr4_entries = scs.read_scopeshot_entries_from_memory(start_address,
                                                              num_entries)
        for i in range(len(scs_ddr4_data)):
            expected_data = scs_ddr4_data[i]
            actual_data, timestamp = ddr4_entries[i]
            assert scs.compare_sensor_data(expected_data, actual_data)
            diffs.append(abs(timestamp - time_stamps[i]))
            Log('info', f'Timestamp (expected, actual, diff): '
                        f'{time_stamps[i]}, {timestamp}, {diffs[i]:2d}')

    diffs.sort()
    Log('info', f'Timestamp (min, max): '
                f'{diffs[0]:2d}, {diffs[len(diffs)-1]:2d}')


def generate_random_scs_ddr4_data(num_data):
    from random import getrandbits, randint
    from Common.triggers import SCSMemoryEncoding

    data = []
    for i in range(num_data):
        data.append(SCSMemoryEncoding(
            sensor_data=getrandbits(10),
            user_id=getrandbits(10),
            pin_id=getrandbits(2),
            ac_slice_id=getrandbits(2),
            link_num=randint(0, 13)))

    return data


def validate_at_each_trigger_sent(sa=None, ea=None, num_entries=1):
    from Common.instruments import hdmt_trigger_interface
    from Rc3.instrument.scs_interface import ScsScopeshotInterface

    scs = ScsScopeshotInterface(rc)
    scs_ddr4_data = generate_random_scs_ddr4_data(num_entries)

    assert scs.ddr4_starting_address() == scs.DDR4_STARTING_ADDRESS_DEFAULT
    assert scs.ddr4_ending_address() == scs.DDR4_ENDING_ADDRESS_DEFAULT

    if sa is not None:
        scs.set_ddr4_starting_address(sa)
    if ea is not None:
        scs.set_ddr4_ending_address(ea)

    scs.enable()
    scs.start_scopeshot()

    try:
        address = scs.ddr4_starting_address()
        diffs = []
        for data in scs_ddr4_data:
            link = data.link_num
            scs_trigger = hdmt_trigger_interface.generate_scs_trigger(
                slice=data.ac_slice_id,
                pin=data.pin_id,
                user=data.user_id,
                data=data.sensor_data)
            rc.send_up_trigger_bypass(link, scs_trigger)
            expected_stamp = rc.live_time_stamp()

            entry = scs.read_scopeshot_entries_from_memory(address, num_entries=1)
            actual, actual_stamp = entry

            latest = scs.ddr4_latest_address()
            diffs.append(abs(expected_stamp - actual_stamp))
            i = len(diffs) - 1
            Log('info', f'Timestamp (expected, actual, diff): '
                        f'{expected_stamp}, {actual_stamp}, {diffs[i]:2d}. '
                        f'Address (expected, actual): '
                        f'0x{address:08X}, 0x{latest:08X}')

            assert address == latest
            expected = data
            assert scs.compare_sensor_data(expected, actual)

            address = scs.ddr4_next_address(address)
        diffs.sort()
        Log('info', f'Timestamp (min, max): '
                    f'{diffs[0]:2d}, {diffs[len(diffs) - 1]:2d}')
    finally:
        scs.stop_scopeshot()
        scs.disable()
        scs.set_ddr4_starting_address(scs.DDR4_STARTING_ADDRESS_DEFAULT)
        scs.set_ddr4_ending_address(scs.DDR4_ENDING_ADDRESS_DEFAULT)


def DirectedCircularBufferWriteReadExhaustiveTest():
    from Rc3.instrument import rc3, scs_interface
    from Rc3.Tests.SerialCaptureStream import ScopeshotEngineDiagnostics

    rc = rc3.Rc3()
    test_class = ScopeshotEngineDiagnostics()
    test_class.rc = rc
    test_class.scs = scs_interface.ScsScopeshotInterface(rc)
    test_class.DirectedCircularBufferWriteReadExhaustiveTest()


def start_stop_triggers(num_triggers, repeat):
    from Common.register import create_field_dictionary
    from Rc3.instrument.scs_interface import ScsScopeshotInterface

    scs = ScsScopeshotInterface()
    scs_ddr4_data = generate_random_scs_ddr4_data(num_triggers)

    assert scs.ddr4_starting_address() == scs.DDR4_STARTING_ADDRESS_DEFAULT
    assert scs.ddr4_ending_address() == scs.DDR4_ENDING_ADDRESS_DEFAULT

    staring_address = scs.ddr4_starting_address()

    for i in range(repeat):
        scs.initiate_scopeshot()
        Log('info', f'After initiate latest_address: {scs.ddr4_latest_address()}')
        zero_bytes = bytes(scs.DDR4_ENTRY_BYTE_SIZE * num_triggers)
        rc.dma_write(scs.DDR4_STARTING_ADDRESS_DEFAULT, zero_bytes)
        for i in range(num_triggers):
            address = staring_address + (i * scs.DDR4_ENTRY_BYTE_SIZE)
            if i == num_triggers // 2:
                scs.stop_scopeshot()
            scs_trigger = scs.convert_ddr4_data_to_trigger(scs_ddr4_data[i])
            rc.send_up_trigger_bypass(0, scs_trigger)
            data, stamp = scs.read_scopeshot_entries_from_memory(address)
            Log('info', f'{i}) latest_address: {scs.ddr4_latest_address():2d},'
                        f' {create_field_dictionary(data)}')
        scs.terminate_scopeshot()
        Log('info', f'After terminate latest_address: {scs.ddr4_latest_address()}')

def enable_disable_triggers(num_triggers, repeat):
    from Common import triggers
    from Common.register import create_field_dictionary
    from Rc3.instrument.scs_interface import ScsScopeshotInterface

    scs = ScsScopeshotInterface()
    scs_ddr4_data = scs.generate_random_scs_ddr4_data(num_triggers)

    assert scs.ddr4_starting_address() == scs.DDR4_STARTING_ADDRESS_DEFAULT
    assert scs.ddr4_ending_address() == scs.DDR4_ENDING_ADDRESS_DEFAULT

    staring_address = scs.ddr4_starting_address()

    for i in range(repeat):
        scs.initiate_scopeshot()

        zero_bytes = bytes(scs.DDR4_ENTRY_BYTE_SIZE * num_triggers)
        rc.dma_write(scs.DDR4_STARTING_ADDRESS_DEFAULT, zero_bytes)

        for i in range(num_triggers):
            address = staring_address + (i * scs.DDR4_ENTRY_BYTE_SIZE)
            if i == num_triggers // 2:
                scs.disable()

            expected_data = scs_ddr4_data[i]
            scs_trigger = scs.convert_ddr4_data_to_trigger(expected_data)
            link_num = expected_data.link_num
            rc.send_up_trigger_bypass(link_num, scs_trigger)
            actual_data, stamp = scs.read_scopeshot_entries_from_memory(address)

            if i < num_triggers // 2:
                assert scs.is_active()
                assert scs.compare_sensor_data(expected_data, actual_data)
            else:
                assert not scs.is_active()
                zero_data = triggers.SCSMemoryEncoding(0)
                assert scs.compare_sensor_data(zero_data, actual_data)

            # print('\n--------------------------------------------------------')
            # print(f'{i}) {scs.is_active()}')
            # print(create_field_dictionary(expected_data))
            # print(create_field_dictionary(actual_data))

def enable_disable_start_stop(num_triggers, repeat):
    from Common import triggers
    from Common.register import create_field_dictionary
    from Rc3.instrument.scs_interface import ScsScopeshotInterface

    scs = ScsScopeshotInterface()

    assert scs.ddr4_starting_address() == scs.DDR4_STARTING_ADDRESS_DEFAULT
    assert scs.ddr4_ending_address() == scs.DDR4_ENDING_ADDRESS_DEFAULT

    staring_address = scs.ddr4_starting_address()

    try:
        scs.initiate_scopeshot()

        for i in range(repeat):
            scs_ddr4_data = scs.generate_random_scs_ddr4_data(num_triggers)

            zero_bytes = bytes(scs.DDR4_ENTRY_BYTE_SIZE)

            for i in range(num_triggers):
                is_even = (i % 2) == 0

                expected_data = scs_ddr4_data[i]
                scs_trigger = scs.convert_ddr4_data_to_trigger(
                    expected_data)
                link_num = expected_data.link_num

                rc.dma_write(staring_address, zero_bytes)

                if is_even:
                    scs.enable()
                    scs.start_scopeshot(use_event_trigger=False)
                    # print('\n------------------------------------------------')
                    # print(f'{i}) {scs.is_active()}')
                    assert scs.is_active()
                    rc.send_up_trigger_bypass(link_num, scs_trigger)
                    scs.stop_scopeshot(use_event_trigger=False)
                else:
                    scs.disable()
                    scs.start_scopeshot(use_event_trigger=False)
                    # print('\n------------------------------------------------')
                    # print(f'{i}) {scs.is_active()}')
                    assert not scs.is_active()
                    rc.send_up_trigger_bypass(link_num, scs_trigger)
                    scs.stop_scopeshot(use_event_trigger=False)

                actual_data, stamp = scs.read_scopeshot_entries_from_memory(
                    staring_address)
                if is_even:
                    assert scs.compare_sensor_data(expected_data, actual_data)
                else:
                    zero_data = triggers.SCSMemoryEncoding(0)
                    assert scs.compare_sensor_data(zero_data, actual_data)
                # print(create_field_dictionary(expected_data))
                # print(create_field_dictionary(actual_data))

    finally:
        scs.terminate_scopeshot()


def sample_count(num_triggers, count, adjust):
    from time import perf_counter
    from Common import triggers
    from Common.register import create_field_dictionary
    from Rc3.instrument.scs_interface import ScsScopeshotInterface

    approximate_send_trigger_time_us = 9
    scs = ScsScopeshotInterface()

    assert scs.ddr4_starting_address() == scs.DDR4_STARTING_ADDRESS_DEFAULT
    assert scs.ddr4_ending_address() == scs.DDR4_ENDING_ADDRESS_DEFAULT

    staring_address = scs.ddr4_starting_address()
    print(staring_address)
    zero_bytes = bytes(scs.DDR4_ENTRY_BYTE_SIZE * (num_triggers + (count*adjust)))
    rc.dma_write(staring_address, zero_bytes)

    expected_data = scs.generate_random_scs_ddr4_data(num_triggers + (count*adjust))

    if count == 1:
        temp = count * 15
    else:
        temp = 15 + ((count-1) * adjust)
    scs.set_sample_count(temp)
    assert temp == scs.sample_count()

    try:
        print(scs.sample_count())
        scs.initiate_scopeshot(use_event_trigger=False)

        for i in range(num_triggers):
            trigger = scs.convert_ddr4_data_to_trigger(expected_data[i])
            rc.send_up_trigger_bypass(expected_data[i].link_num, trigger)

        scs.stop_scopeshot(use_event_trigger=False)

        for i in range(num_triggers, len(expected_data)):
            # start_time = perf_counter()
            trigger = scs.convert_ddr4_data_to_trigger(expected_data[i])
            rc.send_up_trigger_bypass(expected_data[i].link_num, trigger)
            # print((perf_counter() - start_time) * 1e6)

        entries = scs.read_scopeshot_entries_from_memory(staring_address,
                                                         len(expected_data))


        for i in range(len(entries)):
            print(f'{i}) {create_field_dictionary(entries[i][0])}')
            if scs.compare_sensor_data(triggers.SCSMemoryEncoding(0),
                                       entries[i][0]):
                print(i)
                break

    finally:
        scs.terminate_scopeshot(use_event_trigger=False)

"""-------------------------------------------------------------------------"""

ConfigLogger()
# _levelToName = {
#     CRITICAL: 'CRITICAL',
#     ERROR: 'ERROR',
#     WARNING: 'WARNING',
#     INFO: 'INFO',
#     DEBUG: 'DEBUG',
#     NOTSET: 'NOTSET',
# }
SetLoggerLevel(logging.INFO)  # Use logging.DEBUG for D, I and W

tester = get_tester()
rc = Rc3()