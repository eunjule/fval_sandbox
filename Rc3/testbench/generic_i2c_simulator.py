# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

from Common.fval import Object


def create_generic_devices(num_devices):
    return [GenericI2c() for i in range(num_devices)]


class GenericI2c(Object):
    def __init__(self):
        super().__init__()

        self.registers = {}

    def write(self, command, data):
        command &= 0xFF
        if command in self.registers.keys():
            self.registers[command] = data

    def read(self, command):
        return self.registers[command & 0xFF]
