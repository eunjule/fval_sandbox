# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

from Common import fval

VOLTAGES = {0: 3.6, 1: 1.2, 2: 3.3, 3: 3.3, 4: 3.3, 5: 3.3, 6: 3.3, 7: 2.5,
            8: 0.0, 12: -9.5, 13: -9.5,
            14: 19.5, 15: 0.02, 16: 5.0, 17: 3.3, 18: 2.5, 19: 1.2, 20: 3.0,
            21: 1.03, 22: 0.9, 23: 20.1,
            24: 0.85, 25: 0.94, 26: 2.4, 27: 12.0, 28: -10.1, 29: -10.1,
            30: 19.5, 31: 1.8}


class Max1230(fval.Object):
    def __init__(self, rc):
        super().__init__()
        self.rc = rc
        self.registers = {}
        self._initialize_voltage_registers()

    def read_channel(self, channel):
        return VOLTAGES.get(channel, None)

    def _initialize_voltage_registers(self):
        base = 0x460
        for channel, voltage in VOLTAGES.items():
            raw_value = self.voltage_to_raw(channel, voltage)
            self.rc.registers.update({base + (channel * 4): raw_value})

    def voltage_to_raw(self, channel, voltage):
        default = 1.0
        scales = {12: -2.49, 13: -2.49, 14: 5.99, 16: 2.0, 23: 5.99, 27: (1.5 + 0.499) / 0.499, 28: -2.74, 29: -2.74,
                  30: 5.99}
        return int(voltage / (0.001 * scales.get(channel, default)))
