# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

from Common.fval import Object
from Rc3.instrument.rc3_register import LTC2358_CHANNEL_DATA_0,\
    LTC2358_CHANNEL_DATA_1, LTC2358_SOFTSPAN_CODE


class Ltc2358(Object):
    DEFAULT_CODE = 0b111
    DEFAULT_RAW_DATA = 0xBbEF

    def __init__(self):
        super().__init__()
        self.registers = \
            {LTC2358_SOFTSPAN_CODE.address(0): LTC2358_SOFTSPAN_CODE(),
             LTC2358_SOFTSPAN_CODE.address(1): LTC2358_SOFTSPAN_CODE()}

    def write_softspan_code(self, offset, data):
        self.registers[offset].value = data

    def softspan_code(self, offset):
        return self.registers.get(offset, LTC2358_SOFTSPAN_CODE()).value

    def channel_data_0(self, offset):
        channel = LTC2358_CHANNEL_DATA_0.index(offset)
        value = self.registers[LTC2358_SOFTSPAN_CODE.address(0)].value
        softspan_code = (value >> (3*channel)) & 0b111
        raw_adc_data = 0 if softspan_code == 0 else Ltc2358.DEFAULT_RAW_DATA
        value = LTC2358_CHANNEL_DATA_0(
            raw_adc_data=raw_adc_data,
            channel_id = channel,
            softspan_code = softspan_code).value
        return value

    def channel_data_1(self, offset):
        channel = LTC2358_CHANNEL_DATA_1.index(offset)
        value = self.registers[LTC2358_SOFTSPAN_CODE.address(1)].value
        softspan_code = (value >> (3*channel)) & 0b111
        raw_adc_data = 0 if softspan_code == 0 else Ltc2358.DEFAULT_RAW_DATA
        value = LTC2358_CHANNEL_DATA_1(
            raw_adc_data=raw_adc_data,
            channel_id = channel,
            softspan_code = softspan_code).value
        return value
