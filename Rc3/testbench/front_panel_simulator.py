# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

from Common.fval import Object
from Rc3.instrument.front_panel_i2c_interface import I2cCommands, \
    MANUFACTURER_ID_VALUE, DEVICE_ID_VALUE

def create_fp_devices():
    return FrontPanel()


class FrontPanel(Object):
    KNOWN_VALUE_REGISTERS = {
        I2cCommands.MANUFACTURER_ID.value: MANUFACTURER_ID_VALUE,
        I2cCommands.DEVICE_ID.value: DEVICE_ID_VALUE}

    def __init__(self):
        super().__init__()

        self.registers = self.KNOWN_VALUE_REGISTERS

    def write(self, command, data):
        command &= 0xFF
        if command in self.registers.keys():
            self.registers[command] = data

    def read(self, command):
        return self.registers[command & 0xFF]
