# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

from Common import fval
from Rc3.instrument.lmk04832 import Lmk04832 as ClockChip
from Rc3.instrument.rc3_register import LMK04832_COMMAND, LMK04832_STATUS


class Lmk04832(fval.Object):
    def __init__(self):
        super().__init__()

        self.registers = ClockChip.ID_REGISTERS
        self.tx_fifo = []
        self.rx_fifo = []
        self.is_fifo_in_reset = True

        self.write_actions = {0x40: self.send_command,
                              0x4C: self.reset_fifos}
        self.read_actions = {0x44: self.read_data,
                             0x48: self.status}

    def write(self, offset, data):
        self.write_actions.get(offset)(data)

    def read(self, offset):
        return self.read_actions.get(offset, 0)()

    def reset_fifos(self, reset):
        """This is a hold in reset feature.

        :param reset: If 1, fifos are cleared and held in reset.
        :return: None
        """
        self.is_fifo_in_reset = reset
        if self.is_fifo_in_reset:
            self.tx_fifo = []
            self.rx_fifo = []

    def push_rx_data(self, address):
        if not self.is_fifo_in_reset:
            self.rx_fifo.insert(0, self.registers.get(address))

    def pop_rx_data(self):
        if not self.is_fifo_in_reset:
            return self.rx_fifo.pop()

    def send_command(self, command):
        reg = LMK04832_COMMAND(value=command)
        if reg.rw:
            self.read_command(reg.register)
        else:
            pass  # currently ignoring writes

    def read_command(self, address):
        self.push_rx_data(address)

    def read_data(self):
        if self.rx_fifo:
            return self.pop_rx_data()
        else:
            return 0

    def status(self):
        return LMK04832_STATUS(tx_fifo_count=len(self.tx_fifo),
                               rx_fifo_count=len(self.rx_fifo)).value
