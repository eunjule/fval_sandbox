# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

from sys import byteorder

from Common import triggers
from Common.fval import Object
from Rc3.instrument import rc3_register, scs_interface


class ScopeShotEngine(Object):
    def __init__(self, rc):
        super().__init__()
        self.rc = rc
        self._new_entry_available = False
        self.init_registers()
        self.init_actions()
        self.next_latest_address = self.latest_address_reg.value
        self.sample_count = 0

    def init_registers(self):
        self.latest_address_reg = rc3_register.SCOPESHOT_LATEST_DDR4_ADDRESS(0)
        self.ending_address_reg = \
            rc3_register.SCOPESHOT_DDR4_ENDING_ADDRESS(0x5FFF_FFFF)
        self.starting_address_reg = \
            rc3_register.SCOPESHOT_DDR4_STARTING_ADDRESS(0)
        self.control_reg = rc3_register.SCOPESHOT_CONTROL(0)
        self.config_reg = rc3_register.SCOPESHOT_CONFIG(0)
        self.status_reg = rc3_register.SCOPESHOT_STATUS(0)

    def init_actions(self):
        self.read_actions = {
            0xAA0: self.latest_address,
            0xAA4: self.ending_address,
            0xAA8: self.starting_address,
            0xAAC: self.control,
            0xAB0: self.config,
            0xAB4: self.status
        }

        self.write_actions = {
            0xAA4: self.set_ending_address,
            0xAA8: self.set_starting_address,
            0xAAC: self.set_control,
            0xAB0: self.set_config,
        }

    def latest_address(self, offset=None):
        return self.latest_address_reg.value

    def ending_address(self, offset=None):
        return self.ending_address_reg.value

    def starting_address(self, offset=None):
        return self.starting_address_reg.value

    def control(self, offset=None):
        return self.control_reg.value

    def config(self, offset=None):
        return self.config_reg.value

    def status(self, offset=None):
        return self.status_reg.value

    def set_ending_address(self, offset=None, data=None):
        self.ending_address_reg.value = data

    def set_starting_address(self, offset=None, data=None):
        self.starting_address_reg.value = data
        self.reset_latest_address()

    def reset_latest_address(self):
        self.latest_address_reg.value = self.starting_address()
        self.next_latest_address = self.latest_address_reg.value

    def set_control(self, offset=None, data=None):
        self.control_reg.value = data
        if self.scopeshot_enable():
            if self.control_reg.start:
                self.status_reg.active = 1
                self.reset_latest_address()
            elif self.control_reg.stop:
                self.status_reg.active = 0
        self.control_reg.value = 0

    def set_config(self, offset=None, data=None):
        self.config_reg.value = data

        sample_count_adjust = 4
        self.sample_count = self.config_reg.sample_count // sample_count_adjust

        if not self.config_reg.enable:
            self.status_reg.active = 0

    # def latest_address(self):
    def process_trigger(self, link_num, trigger):
        card_type = triggers.CardType(trigger >> 26)

        if card_type == triggers.CardType.EVENT:
            self.process_event(trigger)
        elif card_type == triggers.CardType.SCS:
            self.process_sensor_data(link_num, trigger)

    def process_event(self, trigger):
        trigger = triggers.EventTriggerEncoding(trigger)
        if trigger.event_type == triggers.EventType.SCOPESHOT_START.value:
            self.set_control(data=rc3_register.SCOPESHOT_CONTROL(start=1).value)
        elif trigger.event_type == triggers.EventType.SCOPESHOT_STOP.value:
            self.set_control(data=rc3_register.SCOPESHOT_CONTROL(stop=1).value)
            self._address_pointer = 0
        elif trigger.event_type == triggers.EventType.TIME_STAMP_RESET.value:
            self.rc.reset_live_time_stamp()

    def process_sensor_data(self, link_num, trigger):
        if (self.is_active() or self.sample_count)  and self.scopeshot_enable():
            trigger = triggers.SCSTriggerEncoding(trigger)
            memory_data = triggers.SCSMemoryEncoding(
                sensor_data=trigger.sensor_data,
                user_id=trigger.user_id,
                pin_id=trigger.pin_id,
                ac_slice_id=trigger.ac_slice_id,
                link_num=link_num)

            if self.sample_count:
                self.sample_count -= 1

            self.update_latest_address()
            current_address = self.latest_address()

            dword_byte_size = 4
            memory_data_bytes = memory_data.value.to_bytes(
                length=dword_byte_size, byteorder=byteorder)
            time_stamp = self.rc.live_time_stamp()
            time_stamp_bytes = time_stamp.to_bytes(length=dword_byte_size,
                                                   byteorder=byteorder)
            ddr4_data = memory_data_bytes + time_stamp_bytes
            self.rc.dma_write(current_address, data=ddr4_data)

    def update_latest_address(self):
        ddr4_entry_byte_size = \
            scs_interface.ScsScopeshotInterface.DDR4_ENTRY_BYTE_SIZE

        self.latest_address_reg.value = self.next_latest_address
        address_limit = self.ending_address() - ddr4_entry_byte_size
        if self.next_latest_address < address_limit:
            self.next_latest_address += ddr4_entry_byte_size
        else:
            self.next_latest_address = self.starting_address()

    def scopeshot_enable(self):
        return bool(self.config_reg.enable)

    def is_active(self):
        return bool(self.status_reg.active)
