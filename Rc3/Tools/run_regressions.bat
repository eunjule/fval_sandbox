@echo off
setlocal enabledelayedexpansion

if %1==-h (
    echo Two arguments required:
    echo 1: any name for these regressions
    echo 2: number of iterations to repeat
    goto end
)

set argCount=0
for %%x in (%*) do (
   set /A argCount+=1
   set "argVec[!argCount!]=%%~x"
)

set REGRESSION_NAME=!argVec[1]!
set NUM_ITERATIONS=!argVec[2]!
echo Number of iterations to run: %NUM_ITERATIONS%

set DBG_REMOVE_HDMT_SLOTS=4
REM set DBG_SKIP_HPCC_FPGA_LOAD=
REM set DBG_RC3_TEST_UP_TRIGGER_MINI_TESTS=1
REM set FORCE_RC3_CLOCK_INIT=1

REM echo Re-image all FPGAs, re-init all clocks, re-train
py ./Tools/regress.py rc3 -o !REGRESSION_NAME!_rc3_full_1 -skm

REM set FORCE_RC3_CLOCK_INIT=
REM REM REM set DBG_SKIP_HPCC_FPGA_LOAD=1
REM REM set DBG_RC3_TEST_UP_TRIGGER_MINI_TESTS=1

REM echo Run Ad5064 tests only with repeat of 10
REM py ./Tools/regress.py rc3 -o !REGRESSION_NAME!_rc3_AD5064_repeat10 -skm -tn Ad5064.*.* -r 10

set LOOP_START=2
for /L %%i in (%LOOP_START%, 1, %NUM_ITERATIONS%) do (
    echo ##################################### Iteration %%i #####################################
    py ./Tools/regress.py rc3 -o !REGRESSION_NAME!_rc3_full_%%i -skm
    echo -----------------------------------------------------------------------------------------
    echo.
)

EXIT /B %ERRORLEVEL%

