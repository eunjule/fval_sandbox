# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

"""RCTC3 has an LMK04832 clock conditioner chip

FPGA has a SPI interface to the LMK04832
"""

from random import randrange
from time import sleep

from Rc3.instrument.lmk04832 import Lmk04832
from Rc3.Tests.Rc3Test import Rc3Test


class Diagnostics(Rc3Test):
    """Test communication robustness"""

    def setUp(self, tester=None):
        super().setUp(tester)
        self.test_iterations = 100
        self.max_fail_count = 10
        self.lmk04832 = Lmk04832(self.rc)
        self.lmk04832.reset_interface()

    def DirectedRegisterReadExhaustiveTest(self):
        """Read ID registers

        Read device IDs and compare them to expected values
        1) Read ID register and compare to expected value
        2) Repeat step 1 until all ID registers have been verified
        3) Repeat above steps a total of 100 times
        """
        for iteration in range(self.test_iterations):
            all_pass = True
            registers = Lmk04832.ID_REGISTERS

            for command, expected_data in registers.items():
                actual_data = self.lmk04832.read(command)

                if expected_data != actual_data:
                    self.Log(
                        'error',
                        self.exhaustive_read_error_msg(
                            iteration,
                            self.lmk04832.name(),
                            expected_data,
                            actual_data))
                    all_pass = False

            self.update_failed_iterations(all_pass)
            if self.fail_count >= self.max_fail_count:
                break

        self.validate_iterations()

    def exhaustive_read_error_msg(self, iteration, name, expected_data,
                                  actual_data):
        return f'Iteration_{iteration}: {name} failed read test ' \
               f'(Expected, Actual): 0x{expected_data:02X}, ' \
               f'0x{actual_data:02X}'


class Functional(Rc3Test):
    """Test operation of the i2c master"""

    def setUp(self, tester=None):
        super().setUp(tester)
        self.test_iterations = 10
        self.max_fail_count = 10
        self.lmk04832 = Lmk04832(self.rc)
        self.lmk04832.reset_interface()

    def DirectedRxFifoCountTest(self):
        """Verify the Receive FIFO Count of the status register

        The counter denotes the number of items in the fifo
        - Perform a random number of reads
        - Verify Receive FIFO Count matches total reads
        - Reset FIFO using Control register and verify RX FIFO count is zero
        - Perform a total of max fifo size (2^10) reads
        - Verify Receive FIFO Count matches total reads
        - Reset FIFO using Control register and verify RX FIFO count is zero
        - Repeat above steps a total of 10 times
        """
        count_list = self.rx_fifo_count_list()

        for iteration in range(len(count_list)):
            actual_count = 0
            expected_count = count_list[iteration]

            with Lmk04832Tester(self.lmk04832) as rx_count_tester:
                for i in range(expected_count):
                    rx_count_tester.lmk04832.send_read_command(address=0)
                sleep(0.1)
                actual_count = rx_count_tester.lmk04832.rx_fifo_count()

            if actual_count != expected_count:
                self.Log('error',
                         self.invalid_rx_count_msg(iteration, expected_count,
                                                   actual_count))
                self.update_failed_iterations(success=False)

            if self.fail_count >= self.max_fail_count:
                break

        self.validate_iterations()

    def invalid_rx_count_msg(self, iteration, expected, actual):
        return f'Iteration_{iteration}: Lmk04832 failed rx count test ' \
               f'(Expected, Actual): {expected}, {actual}'

    def rx_fifo_count_list(self):
        max_count = 2 ** 10 - 1  # depth of fifo is 1024
        count_list = [randrange(1, max_count)
                      for i in range(self.test_iterations - 2)]
        count_list += [0, max_count]
        return count_list


class Lmk04832Tester():
    def __init__(self, lmk04832):
        self.lmk04832 = lmk04832

    def __enter__(self):
        self.lmk04832.reset_interface()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.lmk04832.reset_interface()
