# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.


"""RCTC3 communicates with an AD5064.

This DAC has four channels and has 16-bit accuracy.
It uses SPI communication with clock speeds up to 50 MHz.
Resets to known output voltage (zero scale or midscale)

DAC mainly provides voltage reference to OSC_VC_5P oscillator
"""
import math
import time

from Rc3.Tests.Rc3Test import Rc3Test


class Diagnostics(Rc3Test):
    """Test operation of the interface"""
    V_CNTL_VOLATGE = 2.5
    V_CNTL_STEP = 0.050
    AD5064_VREF = 5  # 5V output from MAX5250A
    DAC_TO_ADC_CONVERSION_WAIT = 0.01

    # Actual voltage reading si 0.036 above expected, use 0.04 cpmpare delta to
    #  compensate
    COMPARE_DELTA = 0.04

    def setUp(self, tester=None):
        super().setUp(tester)
        self.test_iterations = 10
        self.max_fail_count = 10

    def DirectedVoutAReadExhaustiveTest(self):
        """ Verify the AD5064 VOUT_A output voltage

        The VOUT_A output provides input to the LMK04832
        oscillator (Y9 from FabA schematic) and is measured by channel 7 of the
        MAX1230 (U187 from FabA RCTC3 schematic). The test will validate proper
        voltage to the oscillator by reading channel 7 of the MAX1230. At power
        on, the voltage at VOUT_A is 0V, and it gets set to 2.5V. We are testing
        for 2.5V.
        1) Read channel 7 voltage of the MAX1230
        2) Verify voltage is within expected range
        3) Repeat above steps 100 times
        """
        expected = Diagnostics.V_CNTL_VOLATGE
        for iteration in range(self.test_iterations):
            actual = self.rc.voltage(channel=7)

            if not math.isclose(a=expected, b=actual,
                                abs_tol=Diagnostics.COMPARE_DELTA):
                self.Log('error', self.error_msg_voltage_read(
                    iteration, expected, actual))
                self.update_failed_iterations(False)

            if self.fail_count >= self.max_fail_count:
                break

        self.validate_iterations()

    def error_msg_voltage_read(self, iteration, expected, actual):
        return f'Iteration {iteration}: Invalid voltage read ' \
               f'(expected, actual): {expected}, {actual}'

    # @skip('This test should only be used for debug purposes and not for full '
    #       'regressions. The test affects the reference clock of the RCTC3\'s '
    #       'LMK04832 and will cause Aurora Triggers to fail if running full '
    #       'regressions')
    # def DirectedWriteToDACChannelTest(self):
    #     """Tests FPGA to AD5064 Command Execution
    #
    #     1. Make sure current voltage control is at default
    #     2. Change AD5064 voltage output to default - minimum step
    #     3. Verify voltage has updated
    #     4. Change AD5064 voltage output to default + minimum step
    #     5. Verify voltage has updated
    #     6. Repeat step 2-5 10 times.
    #     """
    #
    #     if not self.validate_default_voltage_control():
    #         self.update_failed_iterations(success=False)
    #
    #     ch07_voltage_control = Diagnostics.V_CNTL_VOLATGE
    #     max_voltage_step = Diagnostics.V_CNTL_STEP
    #     voltage_step = [ch07_voltage_control-max_voltage_step,
    #                     ch07_voltage_control+max_voltage_step]
    #     for iteration in range(self.test_iterations):
    #         success = True
    #         with self.VoltageControlMgr(self.rc):
    #             for step in voltage_step:
    #                 data = Diagnostics.voltage_to_encoding(step)
    #                 self.rc.write_ad5064_command_data(command=3,
    #                                                   address=0,
    #                                                   data=data)
    #                 time.sleep(Diagnostics.DAC_TO_ADC_CONVERSION_WAIT)
    #                 observed = self.rc.voltage(channel=7)
    #
    #                 if not math.isclose(observed, step,
    #                                     abs_tol=Diagnostics.COMPARE_DELTA):
    #                     self.Log('error',
    #                              self.error_msg_voltage_step(iteration,
    #                                                          step,
    #                                                          observed))
    #                     success = False
    #         self.update_failed_iterations(success)
    #         if self.fail_count >= self.max_fail_count:
    #             break
    #
    #     self.validate_iterations()
    #
    # def error_msg_voltage_step(self, iteration, expected, actual):
    #     return f'Iteration {iteration}: Read invalid control voltage ' \
    #            f'(expected, actual): {expected}, {actual}'

    @staticmethod
    def voltage_to_encoding(voltage):
        return int(voltage * 2 ** 16 / Diagnostics.AD5064_VREF)

    @staticmethod
    def restore_voltage_control(rc):
        data = Diagnostics.voltage_to_encoding(Diagnostics.V_CNTL_VOLATGE)
        rc.write_ad5064_command_data(command=3,
                                     address=0,
                                     data=data)
        time.sleep(0.5)

    def validate_default_voltage_control(self):
        ch07_voltage_control = Diagnostics.V_CNTL_VOLATGE
        observed = self.rc.voltage(channel=7)
        if not self.check_almost_equal(observed, ch07_voltage_control,
                                       delta=.075):
            self.Log('warning',
                     f'AD5064 has invalid default control voltage '
                     f'(expected, observed): '
                     f'{ch07_voltage_control}, {observed}. Setting to default')
            Diagnostics.restore_voltage_control(self.rc)
            return False
        else:
            return True

    class VoltageControlMgr():
        def __init__(self, rc):
            self.default_data = Diagnostics.voltage_to_encoding(
                Diagnostics.V_CNTL_VOLATGE)
            self.rc = rc
        def __enter__(self):
            Diagnostics.restore_voltage_control(self.rc)
            return self
        def __exit__(self, exc_type, exc_val, exc_tb):
            Diagnostics.restore_voltage_control(self.rc)
