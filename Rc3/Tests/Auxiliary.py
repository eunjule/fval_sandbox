# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.


"""RCTC3 has a bi-directional GPIO interface via Auxillary registers.

Twelve GPIOs are available to send/receive data through the 'aux_fpga_side' bus and
snooped by the aux input register.

NOTES:
7.17.2 AUX FPGA Enable Register willnot be validated by FVAL because there is
no way to test for it in software.
7.17.6 AUX Side Data Input Register will not be validated by FVAL because there
no output data and direction control registers.

These AUX signals go to the backplane 2 and route across to the various
card slots. They do not appear on the accessory card or loopback card that
is native to the RCTC3.

To my knowledge, we have never defined a use for them so they don’t go
anywhere on any of our instruments.I do see that they appear on SMPM jacks
on the fab E pcie_Translator board (schematic attached, see page 5, J48,53).
It would be possible to add a short SMPM cable to create a loopback,
if FVAL wanted to confirm they work. Based on that, FVAL will not be validating
it with a loopback.

"""
import random

from Common.fval import skip, expected_failure
from Rc3.Tests.Rc3Test import Rc3Test
from Rc3.instrument import rc3_register

BUS_SIZE = 12
ALL_OUTPUTS = 0xFFF
ALL_INPUTS = 0x0


class Diagnostics(Rc3Test):
    """Test Read/Write Robustness"""

    def setUp(self, tester=None):
        super().setUp(tester)
        self.test_iterations = 100
        self.max_fail_count = 1
        self.rc.disable_aux_fpga()

    def DirectedAuxFpgaSideBusOutputTest(self):
        """Write Data to Aux FPGA Side Bus

        1. Write random 12-bit value to Aux FPGA Side Data Output Register
        2. Set Aux FPGA Direction Control Register 0xFFF to make them
            all outputs
        3. Read Aux FPGA Side Data Input Register and compare to value
            created in step 1.
        4. Repeat step 1-3 100 times.
        """
        for iteration in range(self.test_iterations):
            expected = random.getrandbits(BUS_SIZE)
            self.rc.write_aux_fpga_side_control(ALL_OUTPUTS)
            self.rc.write_aux_fpga_side_data(expected)
            observed = self.rc.aux_fpga_side_input()
            if observed != expected:
                self.Log('error', self.error_msg_output_test(
                    iteration, expected, observed))
                self.update_failed_iterations(False)

            if self.fail_count >= self.max_fail_count:
                break

        self.validate_iterations()

    def error_msg_output_test(self, iteration, expected, actual):
        return f'Iteration {iteration}: Aux FPGA Side Bus failed write '\
               f'(expected, actual): 0b{expected:012b}, 0b{actual:012b}'

    def DirectedBidirectionalControlAllBitsTest(self):
        """Verify directional control of all bits together

        1. Set the AUX FPGA Side Direction Control Register to all Output
        2. Set the AUX FPGA Side Data Output Register to a randomized
            12-bit value
        3. Verify AUX FPGA Side Data Input and AUX Side Data Input Register
            matches randomized 12-bit value
        4. Set the AUX FPGA Side Direction Control Register to all Input
        6. Record the AUX FPGA Side Data Input and AUX Side Data Input
            Register value
        7. Set AUX FPGA Side Data Output Register to compliment of AUX FPGA
            Side Data Input Register
        8. Verify AUX FPGA Side Data Input and AUX Side Data Input Register
            does not change
        9. Repeat above steps a total of 100 times
        """
        for iteration in range(self.test_iterations):
            success = True
            expected = random.getrandbits(BUS_SIZE)

            self.rc.write_aux_fpga_side_control(ALL_OUTPUTS)
            self.rc.write_aux_fpga_side_data(expected)

            self.set_bus_input_value()

            if not self.is_aux_out_reflected_in_input(
                    expected, msg=f'Iteration {iteration}:'):
                success = False

            self.rc.write_aux_fpga_side_control(ALL_INPUTS)
            self.rc.write_aux_fpga_side_data(self.fpga_side_input)

            self.set_bus_input_value()

            if not self.is_aux_out_reflected_in_input(expected=0x0):
                self.Log('warning',
                         f'Iteration {iteration}: Possible Stale Data: '
                         f'Previous value: 0b{expected:012b}. Compare it to '
                         f'Observed 0b{self.fpga_side_input:012b}')
                success = False

            self.update_failed_iterations(success)

            if self.fail_count >= self.max_fail_count:
                break

        self.validate_iterations()

    def DirectedBidirectionalControlWalkingOnesTest(self):
        """Verify directional control using a walking 1s patter

        1. Set the AUX FPGA Side Direction Control Register to all Input
        2. Set AUX FPGA Side Direction Control Register to Output direction
            one bit at a time
        3. Toggle AUX FPGA Side Data Output Register bit from step 2
        4. Verify toggle received at AUX FPGA Side Data Input and AUX Side
            Data Input Register bit. All other bits should remain unchanged.
        5. Set the AUX FPGA Side Direction Control Register to all Output
        6. Set AUX FPGA Side Direction Control Register to input direction one
            bit at a time
        7. Update AUX FPGA Side Data Output Register to all Fs and all 0s
        8. Verify AUX FPGA Side Data Input and AUX Side Data Input Register
            reflects changes of step 6 except the bit
        from step 5 remains unchanged
        9. Repeat above steps a total of 10 times
        """
        for iteration in range(self.test_iterations):
            success = True
            success &= self.walk_inputs_to_outputs()
            success &= self.walk_output_to_input()
            self.update_failed_iterations(success)

            if self.fail_count >= self.max_fail_count:
                break

        self.validate_iterations()

    def walk_output_to_input(self):
        success = True
        msg = f'Walking Output -> Inputs'
        for index in range(BUS_SIZE):
            self.rc.write_aux_fpga_side_control(ALL_OUTPUTS)
            self.rc.write_aux_fpga_side_data(0x0)

            input = ALL_OUTPUTS ^ (1 << index)
            self.rc.write_aux_fpga_side_control(input)
            self.rc.write_aux_fpga_side_data(0xFFF)
            self.set_bus_input_value()

            if not self.is_aux_out_reflected_in_input(expected=input, msg=msg):
                success = False
        return success

    def walk_inputs_to_outputs(self):
        success = True
        msg = f'Walking Inputs -> Output'
        for index in range(BUS_SIZE):
            self.rc.write_aux_fpga_side_control(ALL_OUTPUTS)
            self.rc.write_aux_fpga_side_data(0x0)

            output = 1 << index
            self.rc.write_aux_fpga_side_control(output)
            self.rc.write_aux_fpga_side_data(0xFFF)
            self.set_bus_input_value()

            if not self.is_aux_out_reflected_in_input(output, msg):
                success = False
        return success

    def set_bus_input_value(self):
        self.fpga_side_input = self.rc.aux_fpga_side_input()

    def is_aux_out_reflected_in_input(self, expected, msg=''):
        if self.fpga_side_input != expected:
            self.Log('error', f'{msg} '
                              f'Input Values. Expected: 0b{expected:012b} '
                              f'FPGA Side: 0b{self.fpga_side_input:012b}')
            return False
        return True

    @expected_failure('No defined use case for aux side signals until Fab E '
                      'PCIE translator card comes on board')
    def DirectedAuxSideBidirectionalControlWalkingOnesTest(self):
        """Verify directional control using a walking 1s pattern

        1. Set the AUX FPGA Side Direction Control Register to all Input
        2. Set AUX FPGA Side Direction Control Register to Output direction
            one bit at a time
        3. Toggle AUX FPGA Side Data Output Register bit from step 2
        4. Verify toggle received at AUX Side
            Data Input Register bit. All other bits should remain unchanged.
        5. Set the AUX FPGA Side Direction Control Register to all Output
        6. Set AUX FPGA Side Direction Control Register to input direction one
            bit at a time
        7. Update AUX FPGA Side Data Output Register to all Fs and all 0s
        8. Verify AUX Side Data Input Register
            reflects changes of step 6 except the bit
        from step 5 remains unchanged
        9. Repeat above steps a total of 10 times
        """
        for iteration in range(self.test_iterations):
            success = True
            success &= self.aux_side_walk_inputs_to_outputs()
            success &= self.aux_side_walk_output_to_input()
            self.update_failed_iterations(success)

            if self.fail_count >= self.max_fail_count:
                break

        self.validate_iterations()

    def aux_side_walk_output_to_input(self):
        success = True
        msg = f'Walking Output -> Inputs'
        for index in range(BUS_SIZE):
            self.rc.write_aux_fpga_side_control(ALL_OUTPUTS)
            self.rc.write_aux_fpga_side_data(0x0)

            input = ALL_OUTPUTS ^ (1 << index)
            self.rc.write_aux_fpga_side_control(input)
            self.rc.write_aux_fpga_side_data(0xFFF)
            self.aux_side_input = self.rc.aux_side_input()

            if not self.is_aux_side_out_reflected_in_input(input, msg):
                success = False
        return success

    def aux_side_walk_inputs_to_outputs(self):
        success = True
        msg = f'Walking Inputs -> Output'
        for index in range(BUS_SIZE):
            self.rc.write_aux_fpga_side_control(ALL_OUTPUTS)
            self.rc.write_aux_fpga_side_data(0x0)

            output = 1 << index
            self.rc.write_aux_fpga_side_control(output)
            self.rc.write_aux_fpga_side_data(0xFFF)
            self.aux_side_input = self.rc.aux_side_input()

            if not self.is_aux_side_out_reflected_in_input(output, msg):
                success = False
        return success

    def is_aux_side_out_reflected_in_input(self, expected, msg=''):
        if self.aux_side_input != expected:
            self.Log('error', f'{msg} '
                              f'Input Values. Expected: 0b{expected:012b} '
                              f'FPGA Side: 0b{self.aux_side_input:012b}')
            return False
        return True


class Functional(Rc3Test):
    """Test operation of the interface"""

    def setUp(self, tester=None):
        super().setUp(tester)
        self.test_iterations = 10000
        self.max_fail_count = 1

    def DirectedAuxRegistersWriteReadExhaustiveTest(self):
        """Write to and then read from Aux Registers
            (Enable/Data Output/Direction Control)

        1. Write a random 32-bit value to the Aux Registers
        2. Verify data written to the registers matches its read data
        3. Repeat step 1-2 10,000 times
        """
        registers = [rc3_register.AUX_FPGA_ENABLE,
                     rc3_register.AUX_FPGA_SIDE_DATA_OUTPUT,
                     rc3_register.AUX_FPGA_SIDE_DIRECTION_CONTROL]
        enable_default = self.rc.read_bar_register(
            rc3_register.AUX_FPGA_ENABLE)

        for register in registers:
            name = register().name()
            with self.subTest(REG=f'{name}(0x{register.ADDR:X})'):
                self.fail_count = 0
                for iteration in range(self.test_iterations):
                    if name == 'AUX_FPGA_ENABLE':
                        expected = random.getrandbits(1)
                    else:
                        expected = random.getrandbits(12)

                    success = self.compare_data(iteration, register, expected)
                    self.update_failed_iterations(success)

                    if self.fail_count >= self.max_fail_count:
                        break
                self.validate_iterations()
        self.rc.write_bar_register(enable_default)

    def compare_data(self, iteration, register, expected):
        self.rc.write_bar_register(register(expected))
        observed = self.rc.read_bar_register(register).value
        if expected != observed:
            self.Log('error',
                     f'Iteration {iteration}: {register.name()} '
                     f'Expected, Actual - 0x{expected:08X}, 0x{observed:08X}')
            return False
        else:
            return True

    def error_msg(self, iteration, expected, actual):
        return f'Iteration {iteration}: (expected, actual) 0x{expected:08X}, '\
               f'0x{actual:08X}'
