# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

"""The RCTC3 FPGA provides 8 scratch registers.

The PCIe hard IP in the FPGA has a 256-bit data bus. Registers are accessed
by DWORD (32-bits). Based on the offset of the PCIe register, different
ranges of wires in the 256-bit bus will be used for the 32-bit data.
"""

import random

from Rc3.Tests.Rc3Test import Rc3Test
from Rc3.instrument.rc3_register import SCRATCH_REG


class Diagnostics(Rc3Test):
    """Test communication robustness"""

    def setUp(self, tester=None):
        super().setUp(tester)
        self.test_iterations = 100
        self.max_fail_count = 1

    def DirectedWriteReadExhaustiveTest(self):
        """Write and then read each of eight scratch registers

        - Write random data to all registers
        - Read all registers
        - Verify data written to each register matches its corresponding read
          data
        - Repeat 100 times
        """

        for iteration in range(self.test_iterations):
            success = True
            written_list = [random.randrange(0, 0xFFFFFFFF) for i in
                            range(SCRATCH_REG.REGCOUNT)]

            for index in range(SCRATCH_REG.REGCOUNT):
                self.rc.write_bar_register(self.rc.registers.SCRATCH_REG(
                    value=written_list[index]), index)

            read_list = [self.rc.read_bar_register(
                self.rc.registers.SCRATCH_REG, index).value for index in
                         range(SCRATCH_REG.REGCOUNT)]

            for index in range(SCRATCH_REG.REGCOUNT):
                if written_list[index] != read_list[index]:
                    self.Log('error',
                             self.scratch_write_read_error_msg(
                                 iteration, index, written_list[index],
                                 read_list[index]))
                    success = False

            self.update_failed_iterations(success)
            if self.fail_count >= self.max_fail_count:
                break

        self.validate_iterations()

    def scratch_write_read_error_msg(self, iteration, index, expected_value,
                                     actual_value):
        return f'Iteration {iteration}. Scratch_{index} (expected, actual): ' \
               f'0x{expected_value:08X}, 0x{actual_value:08X}'
