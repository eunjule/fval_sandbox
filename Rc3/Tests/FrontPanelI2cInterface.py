# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

"""RCTC has a Front Panel I2C master

The FPGA can communicate with the Front Panel via I2C. Some of the I2C slave
devices on the Front Panel accessible by this bus are:
- MAX6651 (I2C Address 0x36)
- BLT EEPROM (I2C Address 0xAE)
- LTC2453 (I2C Address 0x28)
- ADT7411 (I2C Address 0x94)
- Humidity sensor (I2C Address 0x80)
"""

from Common.fval import skip
from Common.testbench.i2c_tester import address_nak_test, tx_fifo_count_test, \
    tx_fifo_count_error_bit_test
from Rc3.Tests.Rc3Test import Rc3Test
from Rc3.instrument.front_panel_i2c_interface import UNUSED_I2C_ADDR, create_interfaces, \
    MANUFACTURER_ID_VALUE, DEVICE_ID_VALUE


class Functional(Rc3Test):
    """Test communication robustness"""

    def setUp(self, tester=None):
        super().setUp(tester)
        self.test_iterations = 10
        self.max_fail_count = 1
        self.fp = create_interfaces(self.rc)

    def DirectedReadExhaustiveTest(self):
        """Read Registers from ADT7411

        1) Read the register and compare with datasheet exptected value
        2) Repeat steps 1 and 2 for all registers
        3) Repeat steps 1 to 3 a total of 10 times
        """
        expected_manufacturer_id = MANUFACTURER_ID_VALUE
        expected_device_id = DEVICE_ID_VALUE

        for iteration in range(self.test_iterations):
            success = True
            for fp in self.fp:

                with RcRootI2cLockFp(self.rc):
                    device_id = fp.device_id()
                    manufacturer = fp.manufacturer_id()

                if device_id != expected_device_id:
                    self.Log('error', self.error_message(iteration,
                                                         expected_device_id,
                                                         device_id))
                    success = False
                if manufacturer != expected_manufacturer_id:
                    self.Log('error', self.error_message(iteration,
                                                         expected_manufacturer_id,
                                                         manufacturer))
                    success = False

            self.update_failed_iterations(success)
            if self.fail_count >= self.max_fail_count:
                break

        self.validate_iterations()

    def error_message(self, iteration, expected_value,
                      actual_value):
        return f'Iteration {iteration}'\
               f' (expected, actual): 0x{expected_value:02X}, ' \
               f'0x{actual_value:02X}'


class Diagnostics(Rc3Test):
    """Test operation of the i2c master"""

    def setUp(self, tester=None):
        super().setUp(tester)
        self.test_iterations = 10
        self.max_fail_count = 1
        self.fp = create_interfaces(self.rc)

    def DirectedTxFifoCountErrorBitCheckingTest(self):
        """Verify the Transmit FIFO Count Error bit of the status register

        The error bit is set if a message is sent to a chip using less than two
        bytes
        - Create a message with an invalid number of bytes and verify error bit
          is set
        - Create a message with an valid number of bytes and verify error bit
          is clear
        - Repeat above steps a total of 10 times
        """
        for iteration in range(self.test_iterations):
            success = True

            for fp in self.fp:
                with RcRootI2cLockFp(self.rc):
                    error_msgs = tx_fifo_count_error_bit_test(fp)
                if error_msgs:
                    [self.Log('error', msg) for msg in error_msgs]
                    success = False

            self.update_failed_iterations(success)
            if self.fail_count >= self.max_fail_count:
                break

        self.validate_iterations()

    def DirectedAddressNakTest(self):
        """Verify the Address NAK Error bit of the status register

        The error bit is set if a message is sent to a chip using an invalid
        i2c address
        - Create a message with an invalid address and verify error bit is set
        - Create a message with an valid address and verify error bit is clear
        - Repeat above steps a total of 10 times
        """
        for iteration in range(self.test_iterations):
            success = True
            for fp in self.fp:
                with RcRootI2cLockFp(self.rc):
                    error_msgs = address_nak_test(
                        fp, unused_i2c_addr=UNUSED_I2C_ADDR)
                if error_msgs:
                    [self.Log('error', msg) for msg in error_msgs]
                    success = False

            self.update_failed_iterations(success)

            if self.fail_count >= self.max_fail_count:
                break

        self.validate_iterations()

    def DirectedTxFifoCountTest(self):
        """Verify the Transmit FIFO Count of the status register

        The counter denotes the number of items in the fifo
        - Perform 20 random number of data pushes onto the FIFO of each FP
        - Perform a push of corner cases 0 and max fifo size.
        - Verify Transmit FIFO Count matches total data sent at each push
        """
        self.test_iterations = 1
        for iteration in range(self.test_iterations):
            success = True
            for fp in self.fp:
                with RcRootI2cLockFp(self.rc):
                    error_msgs = tx_fifo_count_test(fp)
                if error_msgs:
                    [self.Log('error', msg) for msg in error_msgs]
                    success = False

            self.update_failed_iterations(success)
            if self.fail_count >= self.max_fail_count:
                break

        self.validate_iterations()


class RcRootI2cLockFp():
    def __init__(self, rc):
        self.rc = rc

    def __enter__(self):
        self.rc.rc_root_i2c_lock_fp()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.rc.rc_root_i2c_unlock()

