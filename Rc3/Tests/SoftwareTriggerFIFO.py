# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.


"""RCTC3 has a Software Trigger FIFO Interface

Trigger link partners can send the FPGA software type triggers. These triggers
are received at the Software Trigger FIFO interface.
"""

from random import choice, getrandbits

from Common.fval import Log
from Common.instruments.tester import HDMT_NUM_SLOTS
from Common.instruments.hdmt_trigger_interface import HdmtTriggerInterface, \
    validate_aurora_statuses
from Common.triggers import CardType, generate_trigger
from Rc3.Tests.Rc3Test import Rc3Test

SW_TRIGGER_FIFO_DEPTH = 1024

class Diagnostics(Rc3Test):
    """Test communication robustness"""

    def setUp(self, tester=None):
        super().setUp(tester)
        self.interfaces = HdmtTriggerInterface.get_interfaces()
        self.test_iterations = 100
        self.max_fail_count = 2

        self.Log('info', f'Checking Trigger Status before test...')
        validate_aurora_statuses(self.rc.aurora_link_slots)

    def DirectedSendSoftwareTriggerReadTest(self):
        """Verify receipt of Software Triggers

        1) Create a list of triggers for each available instrument made up of
        RTOS type or software trigger enable. List size will be 170 to prevent
        overflow if all 12 slots have instruments. Size of FIFO is
        1024, 1024/12 = 85.33. Will use 85
        2) Send all instrument triggers up to the Rc
        3) Read all available triggers stored in the Software Trigger FIFO and
        compare the trigger to what was sent by the instruments by first
        reading the slot source from the Source register, and then the Data
        register for the trigger itself.
        4) Verify trigger read matches instrument's trigger from its list.
        5) Verfiy any non RTOS type or software trigger disabled triggers are
        not in the FIFO
        6) Repeat above steps a total of 1000
        """
        trigger_list_size = SW_TRIGGER_FIFO_DEPTH // HDMT_NUM_SLOTS

        for iteration in range(self.test_iterations):
            with ResetFifoManager(self.rc):
                trigger_dict = create_random_trigger_dictionary(
                    self.rc.aurora_link_slots, trigger_list_size)
                self.send_up_triggers(trigger_dict)
                success = self.validate_triggers(iteration, trigger_dict)
            self.update_failed_iterations(success)
            if self.fail_count >= self.max_fail_count:
                break

        self.validate_iterations()

    def send_up_triggers(self, trigger_dict):
        for slot, triggers in trigger_dict.items():
            interface = self.interfaces.get(slot)[0]
            for trigger in triggers:
                interface.send_up_trigger(trigger)

    def validate_triggers(self, iteration, trigger_dict):
        return validate_triggers(self.rc, iteration, trigger_dict)

class SimulatedUpTriggerDiagnostics(Rc3Test):
    """Test communication robustness"""

    def setUp(self, tester=None):
        super().setUp(tester)
        self.test_iterations = 100
        self.max_fail_count = 2

    def DirectedSimulatedToSoftwareFifoTest(self):
        """Verify receipt of Software Triggers using Rc3 simulated up triggers

        1) Create a list of triggers for each available instrument made up of
        RTOS type or software trigger enable. List size will be 170 to prevent
        overflow if all 12 slots have instruments. Size of FIFO is
        1024, 1024/12 = 85.33. Will use 85
        2) Send simulated up triggers from the Rc
        3) Read all available triggers stored in the Software Trigger FIFO and
        compare the trigger to what was sent by the instruments by first
        reading the slot source from the Source register, and then the Data
        register for the trigger itself.
        4) Verify trigger read matches instrument's trigger from its list.
        5) Verfiy any non RTOS type or software trigger disabled triggers are
        not in the FIFO
        6) Repeat above steps a total of 1000
        """
        num_simulated_links = self.rc.registers.AURORA_TRIGGER_UP_SIM.REGCOUNT
        trigger_list_size = SW_TRIGGER_FIFO_DEPTH // num_simulated_links
        simulated_links = [link for link in range(num_simulated_links)]

        for iteration in range(self.test_iterations):
            with ResetFifoManager(self.rc):
                trigger_dict = create_random_trigger_dictionary(
                    simulated_links, trigger_list_size)
                self.send_up_triggers(trigger_dict)
                success = self.validate_triggers(iteration, trigger_dict)
            self.update_failed_iterations(success)
            if self.fail_count >= self.max_fail_count:
                break

        self.validate_iterations()

    def send_up_triggers(self, trigger_dict):
        for slot, triggers in trigger_dict.items():
            for trigger in triggers:
                self.rc.send_up_trigger_bypass(slot, trigger)

    def validate_triggers(self, iteration, trigger_dict):
        return validate_triggers(self.rc, iteration, trigger_dict)


class ResetFifoManager():
    def __init__(self, rc):
        self.rc = rc

    def __enter__(self):
        self.reset_fifo_and_verify()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.reset_fifo_and_verify()

    def reset_fifo_and_verify(self):
        self.rc.software_trigger_reset()
        fifo_count = self.rc.software_trigger_fifo_count()
        if fifo_count:
            raise self.SoftwareFifoError(
                f'FIFO did not reset. FIFO count: '
                f'{fifo_count}')

    class SoftwareFifoError(Exception):
        pass


def create_random_trigger_dictionary(available_slots, entry_size):
    dict = {}
    if type(available_slots) != list:
        available_slots = [available_slots]
    for slot in available_slots:
        triggers = []
        for i in range(entry_size):
            card_type = choice([CardType.HPCC,
                                CardType.DPS,
                                CardType.RTOS])
            sw_trigger_enable = 1 if card_type != CardType.RTOS else 0
            trigger = generate_trigger(
                card_type=card_type.name,
                payload=getrandbits(19),
                is_software_trigger=sw_trigger_enable)
            triggers.append(trigger)
        dict[slot] = triggers
    return dict


def validate_triggers(rc, iteration, trigger_dict):
    actual_count = rc.software_trigger_fifo_count()
    expected_count = trigger_dict_size(trigger_dict)
    success = True

    if actual_count != expected_count:
        success = False
        Log('error', error_msg_fifo_count(actual_count, expected_count))
    for i in range(actual_count):
        slot = rc.software_trigger_source()
        actual_trigger = rc.software_trigger_fifo_read_data()
        expected_trigger = trigger_dict[slot].pop(0)

        if actual_trigger != expected_trigger:
            success = False
            Log(
                'error',
                error_msg(
                    iteration=iteration,
                    slot=slot,
                    expected=expected_trigger,
                    actual=actual_trigger))
            validate_aurora_statuses(slot)

    return success


def trigger_dict_size(trigger_dict):
    num_triggers = 0
    for slot in trigger_dict:
        num_triggers += len(trigger_dict[slot])
    return num_triggers


def error_msg_fifo_count(actual_count, expected_count):
    return f'Invalid number of triggers in Software Trigger FIFO ' \
           f'(expected, actual): {expected_count}, {actual_count}'


def error_msg(iteration, slot, expected, actual):
    return f'Iteration {iteration}: Software Trigger from slot {slot} ' \
           f'was not received (expected, actual): ' \
           f'{expected}, {actual}'
