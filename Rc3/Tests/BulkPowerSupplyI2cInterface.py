# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

"""RCTC3 draws power from Bulk Power Supply

The Bulk Power Supply (BPS) is located on the Backplane
"""

from Common.fval import SkipTest
from Common.testbench.i2c_tester import address_nak_test, \
    tx_fifo_count_error_bit_test, tx_fifo_count_test
from Rc3.instrument.bps_i2c_interface import create_interfaces,\
    PMBUS_CAPABILITY_VALUE, PMBUS_REVISION_VALUE, UNUSED_I2C_ADDR
from Rc3.Tests.Rc3Test import Rc3Test


class Functional(Rc3Test):
    """Test communication robustness"""

    def setUp(self, tester=None):
        super().setUp(tester)
        self.test_iterations = 10
        self.max_fail_count = 1
        self.bps = create_interfaces(self.rc)

    def DirectedReadExhaustiveTest(self):
        """Read BPS PMBUS_REVISION value

        - Read BPS PMBUS_REVISION register
        - Verify value read is valid
        - Repeat 10 times
        """
        skip_if_not_delta_power_supply(self.rc)

        expected_revision = PMBUS_REVISION_VALUE
        expected_capability = PMBUS_CAPABILITY_VALUE

        for iteration in range(self.test_iterations):
            success = True

            for bps in self.bps:

                with RcRootI2cLockBps(self.rc, bps.bps_number):
                    revision = bps.read_pmbus_revision()
                    capability = bps.read_pmbus_capability()

                if revision != expected_revision:
                    self.Log('error', self.error_message(iteration,
                                                         bps.bps_number,
                                                         expected_revision,
                                                         revision))
                    success = False
                if capability != expected_capability:
                    self.Log('error', self.error_message(iteration,
                                                         bps.bps_number,
                                                         expected_capability,
                                                         capability))
                    success = False

            self.update_failed_iterations(success)
            if self.fail_count >= self.max_fail_count:
                break

        self.validate_iterations()

    def error_message(self, iteration, interface_num, expected_value,
                      actual_value):
        return f'Iteration {iteration}, BPS_{interface_num}' \
               f' (expected, actual): 0x{expected_value:02X}, ' \
               f'0x{actual_value:02X}'


class Diagnostics(Rc3Test):
    """Test operation of the i2c master"""

    def setUp(self, tester=None):
        super().setUp(tester)
        self.test_iterations = 10
        self.max_fail_count = 1
        self.bps = create_interfaces(self.rc)

    def DirectedTxFifoCountErrorBitCheckingTest(self):
        """Verify the Transmit FIFO Count Error bit of the status register

        The error bit is set if a message is sent to a chip using less than two
        bytes
        - Create a message with an invalid number of bytes and verify error bit
          is set
        - Create a message with an valid number of bytes and verify error bit
          is clear
        - Repeat above steps a total of 10 times
        """
        for iteration in range(self.test_iterations):
            success = True

            for bps in self.bps:
                with RcRootI2cLockBps(self.rc, bps.bps_number):
                    error_msgs = tx_fifo_count_error_bit_test(bps)
                if error_msgs:
                    [self.Log('error', msg) for msg in error_msgs]
                    success = False

            self.update_failed_iterations(success)
            if self.fail_count >= self.max_fail_count:
                break

        self.validate_iterations()

    def DirectedAddressNakTest(self):
        """Verify the Address NAK Error bit of the status register

        The error bit is set if a message is sent to a ch ip using an invalid
        i2c address
        - Create a message with an invalid address and verify error bit is set
        - Create a message with an valid address and verify error bit is clear
        - Repeat above steps a total of 10 times
        """
        skip_if_not_delta_power_supply(self.rc)

        for iteration in range(self.test_iterations):
            success = True

            for bps in self.bps:
                with RcRootI2cLockBps(self.rc, bps.bps_number):
                    error_msgs = address_nak_test(
                        bps, unused_i2c_addr=UNUSED_I2C_ADDR)
                if error_msgs:
                    [self.Log('error', msg) for msg in error_msgs]
                    success = False

            self.update_failed_iterations(success)
            if self.fail_count >= self.max_fail_count:
                break

        self.validate_iterations()

    def DirectedTxFifoCountTest(self):
        """Verify the Transmit FIFO Count of the status register

        The counter denotes the number of items in the fifo
        - Perform 20 random number of data pushes onto the FIFO of each BPS
        - Perform a push of corner cases 0 and max fifo size.
        - Verify Transmit FIFO Count matches total data sent at each push
        - Repeat above steps a total of 1 times
        """
        self.test_itertaions = 1
        for iteration in range(self.test_iterations):
            success = True

            for bps in self.bps:
                with RcRootI2cLockBps(self.rc, bps.bps_number):
                    error_msgs = tx_fifo_count_test(bps)
                if error_msgs:
                    [self.Log('error', msg) for msg in error_msgs]
                    success = False

            self.update_failed_iterations(success)
            if self.fail_count >= self.max_fail_count:
                break

        self.validate_iterations()


class RcRootI2cLockBps():
    def __init__(self, rc, bps_num):
        self.rc = rc
        self.bps_num = bps_num

    def __enter__(self):
        self.rc.rc_root_i2c_lock_bps(self.bps_num)
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.rc.rc_root_i2c_unlock()


def skip_if_not_delta_power_supply(rc):
    letf_internal_power_supply = 0
    blt = rc.psBltBoardRead(letf_internal_power_supply)
    vendor_name = blt.VendorName
    if 'DELTA' not in vendor_name.upper():
        raise SkipTest(f'Tester is not using valid power supply '
                       f'(expected, actual): DELTA, {vendor_name}')
