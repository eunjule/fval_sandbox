# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.


"""RCTC3 controls Failsafe Reset and Failsafe Limits

Users can set a specific failsafe limit in seconds and when
timer reaches zero, the RCTC3 shuts down the tester via the failsafe
control register.

NOTE:
    FVAL can not test the execution of failsafe. In order to prevent
    the tester from shutting down during a failsafe event, the user must
    use the failsafe bypass switch. It's not feasible for FVAL to do this
    therefore will not be testing the execution of a failsafe.

    The validation of the actual failsafe and subsequent tester shutdown
    will be tested elsewhere.
"""

import random

from Common.fval import SkipTest
from Common.utilities import format_docstring
from Rc3.Tests.Rc3Test import Rc3Test
from Rc3.instrument.failsafe import FAILSAFE_LIMIT_DEFAULT

TEST_ITERATIONS = 10000


class Functional(Rc3Test):
    """Test Read/Write Robustness"""

    def setUp(self, tester=None):
        super().setUp(tester)
        self.test_iterations = TEST_ITERATIONS
        self.max_fail_count = 10

    @format_docstring(times=TEST_ITERATIONS)
    def DirectedWriteReadFailsafeLimitExhaustiveTest(self):
        """Failsafe Limit Register Robustness

        1. Write random 32-bit value greater than 100 in the failsafe
            limit register.
        2. Verify data written to the register matches its expected read data
        3. Repeat step 1-2 {times} times
        """

        try:
            self.verify_default_value()

            for iteration in range(self.test_iterations):
                expected = random.getrandbits(32)
                self.rc.write_failsafe_limit(limit_in_seconds=expected)
                observed = self.rc.read_failsafe_limit()

                if observed != expected:
                    self.Log('error',
                             f'Failsafe Limit Register: '
                             f'Expected: {expected} Observed: {observed}')
                    self.update_failed_iterations(False)

                if self.fail_count >= self.max_fail_count:
                    break
        finally:
            self.reset_failsafe()

        self.validate_iterations()

    def verify_default_value(self):
        limit = self.rc.failsafe_limit_default()
        if limit != FAILSAFE_LIMIT_DEFAULT:
            self.Log('error', self.failsafe_default_limit_error_msg(limit))

    def failsafe_default_limit_error_msg(self, limit):
        return f'Failsafe limit default value (expected, actual): ' \
               f'{FAILSAFE_LIMIT_DEFAULT}, {limit}'

    @format_docstring(times=TEST_ITERATIONS)
    def DirectedWriteReadFailsafeControlExhaustiveTest(self):
        """Failsafe Control Register Robustness

        1. Write random 32-bit value greater than 100 in the failsafe
            control register.
        2. Verify data written to the register matches its expected read data
        3. Repeat step 1-2 {times} times
        """
        try:
            for iteration in range(self.test_iterations):
                expected = iteration & 0b1
                self.rc.write_failsafe_control(value=expected)
                observed = self.rc.read_failsafe_control()

                if observed != expected:
                    self.Log('error',
                             f'Failsafe Control Register:'
                             f'Expected: {expected} Observed: {observed}')
                    self.update_failed_iterations(False)

                if self.fail_count >= self.max_fail_count:
                    break
        finally:
            self.reset_failsafe()

        self.validate_iterations()

    def reset_failsafe(self):
        self.rc.write_failsafe_limit(FAILSAFE_LIMIT_DEFAULT)
        self.rc.reset_failsafe_limit()
