# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

"""RC3 uses PCIe DMA into DDR4

RC3 has a 2GB DDR4 memory

The Fpga has a set of registers to reset the DDR4 and report the status of
calibration.

DDR4 Control Register:
- Controller Reset: Writing a 1 followed by a zero resets memory
DDR4 Status Register:
- Reset Done field: logic 1, reset is complete
- Calibration success: logic 1, calibration successful after a reset
-Calibration fail: logic 1, calibration failed
"""

import os

from Rc3.Tests.Rc3Test import Rc3Test


class Diagnostics(Rc3Test):
    """Test communication robustness"""

    def setUp(self, tester=None):
        super().setUp(tester)
        self.test_iterations = 5
        self.max_fail_count = 1
        self.chunk_size = 1 << 30

    def DirectedDMAWriteReadDDRTest(self):
        """Write random data to the DDR and read it back

        1) Verify DDR has passed calibration. If failed, fail test
        2) DMA write 1GB of random data at address 0
        3) DMA read 1GB from address 0
        4) Verify written data matches read data
        5) Repeat steps 1 to 4 using upper half of DDR
        6) Repeat steps 1 to 5 a total of 5 times
        """
        if not self.rc.ddr_is_ready():
            self.Log('error', self.ddr_calibration_fail_msg())
            self.update_failed_iterations(success=False)
        else:
            memory_chunk_count = 2
            chunk_size = 1 << 30

            for iteration in range(self.test_iterations):
                last_memory_address = 0
                success = True

                for total_memory in range(memory_chunk_count):
                    memory_start_address = last_memory_address
                    random_test_data = os.urandom(chunk_size)

                    read_back_data = self.dma_write_read_transaction(
                        memory_start_address, random_test_data)

                    if random_test_data != read_back_data:
                        self.Log(
                            'error',
                            self.dma_mismatch_msg(
                                iteration, memory_start_address,
                                memory_start_address + chunk_size))
                        success = False

                    last_memory_address = last_memory_address + chunk_size

                self.update_failed_iterations(success)
                if self.fail_count >= self.max_fail_count:
                    break

        self.validate_iterations()

    def DirectedInitiateSuccessfulCalibrationTest(self):
        """Re-calibrate memory and verify success

        1) Trigger a calibration by performing a reset from control register
        2) Verify Reset Done and Calibration Success in status register is zero
        3) Wait on Reset Done logic-1
        4) Verify Calibration Success is logic 1
        5) Repeat above steps a total of 5 times
        """
        for iteration in range(self.test_iterations):
            success = True

            self.rc.issue_ddr_reset()

            if not self.rc.is_ddr_reset_done_after_wait():
                self.Log('error', f'Reset sequence for DDR4 Controller did '
                                  f'NOT complete.')
                success = False

            if not self.rc.is_status_calibration_after_wait():
                self.Log('error', f'DDR Calibration NOT Successful')
                success = False

            self.update_failed_iterations(success)

            if self.fail_count >= self.max_fail_count:
                break

        self.validate_iterations()

    def DirectedResetMemoryTest(self):
        """Verify reset of memory contents

        1) Create two sets of randomized data, each 1 GB
        2) Write each set to top and bottom half of the 2GB memory
        3) Perform dma reads of both half and compare with written data
        4) Verify write and read data match of both half
        5) Trigger a reset pulse using Controller Reset, wait for Reset Done
        6) Repeat above steps a total of 5 times
        """
        self.partitions = 2

        for iteration in range(self.test_iterations):
            success = True
            data = [os.urandom(self.chunk_size) for i in range(self.partitions)]

            self.write_data(data)

            success &= self.check_data(data)
            success &= self.check_ddr_reset()

            self.update_failed_iterations(success)

            if self.fail_count >= self.max_fail_count:
                break

        self.validate_iterations()

    def check_data_after_reset(self, data, success=True):
        for partition in range(self.partitions):
            observed = self.rc.dma_read(partition * self.chunk_size, self.chunk_size)
            if data[partition] == observed:
                self.Log('error', f'Write/Read Memory Failure after Reset')
                success = False
        return success

    def check_ddr_reset(self, success=True):
        self.rc.issue_ddr_reset()
        if not self.rc.is_ddr_reset_done_after_wait:
            self.Log('error', f'Reset sequence for DDR4 Controller did '
                              f'NOT complete.')
            success = False
        return success

    def check_data(self, data, success=True):
        for partition in range(self.partitions):
            observed = self.rc.dma_read(partition * self.chunk_size, self.chunk_size)
            if data[partition] != observed:
                self.Log('error', f'Write/Read Memory Failure before Reset')
                success = False
        return success

    def write_data(self, data):
        for partition in range(self.partitions):
            self.rc.dma_write(partition * self.chunk_size, data[partition])

    def ddr_calibration_fail_msg(self):
        register_type = self.rc.registers.DDR_STATUS
        return f'DDR is not ready. DDR_STATUS status: ' \
               f'0x{self.rc.read_bar_register(register_type)}'

    def dma_write_read_transaction(self, start_address, write_data):
        self.rc.dma_write(start_address, write_data)
        return self.rc.dma_read(start_address, len(write_data))

    def dma_mismatch_msg(self, iteration, start_address, end_address):
        return f'Iteration {iteration}. Data compare mismatch for memory ' \
               f'range: 0x{start_address:08X} - 0x{end_address:08X}'


class Functional(Rc3Test):
    """Test operation of the interface"""

    def setUp(self, tester=None):
        super().setUp(tester)
        self.test_iterations = 100
        self.max_fail_count = 1

    def DirectedWriteReadControlRegisterExhaustiveTest(self):
        """Verify Control register updates when written to

        Verify Controller Reset bit can be updated
        1) Write 0x1 to control register
        2) Verify read of register equals 1
        3) Write 0x0 to control register
        4) Verify read of register equals 0
        5) Repeat above steps 100 times
        6) Wait on Calibration Success
        """
        for iteration in range(self.test_iterations):
            success = self.set_ddr4_controller(expected=1)
            success &= self.set_ddr4_controller(expected=0)

            self.update_failed_iterations(success)
            if self.fail_count >= self.max_fail_count:
                break

        self.validate_iterations()

        if not self.rc.is_status_calibration_after_wait():
            self.Log('error', f'DDR Calibration NOT Successful')

    def set_ddr4_controller(self, expected):
        success =  True
        self.rc.write_ddr_controller_reset(expected)
        observed = self.rc.ddr_controller_reset()
        if observed != expected:
            self.Log('error',
                     f'DDR4 Controller Register write/read: '
                     f'Expected: {expected} Observed: {observed}')
            success = False
        return success
