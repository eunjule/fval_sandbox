# INTEL CONFIDENTIAL

# Copyright 2018-2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

"""Instrument uses Sensor to Host (S2H) Streaming

The FPGA uses S2H for transferring sensor data from its Mailbox (MB) Ram to
the SiteC Ram. The HPS Firmware loads bulk data into the MB which is eventually
streamed to the SiteC using S2H. HPS is a hard processor within the FPGA.

S2H register definitions:
- S2H Packet Byte Count: Size of a single packet
- S2H Valid Packet Index: Index of last packet written
- S2H Packet Count: Number of packets in stream
- S2H Status: Check if stream is enabled
- S2H Control: Reset controller, reset stream, stop and start stream
"""

from Common.Tests.Sensor2HostStreaming import Functional as S2hFunctional
from Common.utilities import format_docstring
from Rc3.Tests.Rc3Test import Rc3Test


class Functional(Rc3Test):
    """Verify operation of the S2H interface"""

    def setUp(self, tester=None):
        super().setUp(tester)
        self.test_iterations = S2hFunctional.TEST_ITERATIONS
        self.max_fail_count = S2hFunctional.MAX_FAIL_COUNT
        self.test_case = S2hFunctional(stream=self.rc.bulk_data_s2h_stream,
                                       rc_test=self)

    @format_docstring(num_iterations=S2hFunctional.TEST_ITERATIONS)
    def DirectedValidatePacketHeadersTest(self):
        """Verify stream packet header

        S2H stream is comprised of packets where each packet as a header. This
        test will verify that the header's signature, bulk packet version,
        packet counter and valid index are correct. The test will also verify
        the circular buffer by checking the valid_packet_index value and making
        sure it wraps around back to 0 and continues incrementing

        Test steps:
        1) Reset stream
        2) Enable stream
        3) Read packet at valid_index and validate header
        4) Wait on new packet
        5) Repeat steps 3 and 4 until all packets in stream have been verified
        6) Repeat steps 3 to 5 a total of {num_iterations} times
        """
        self.test_case.DirectedValidatePacketHeadersTest()

    @format_docstring(num_iterations=S2hFunctional.TEST_ITERATIONS)
    def DirectedEnableStreamTest(self):
        """Validate S2H stream enable feature

        If stream is enabled, new data is inserted into circular buffer every
        4ms. Verify the S2H valid index register increments by 1 every 4ms
        1) Enable stream and get valid index register
        2) Wait for 4ms new packet insertion
        3) Verify S2H has a valid and new valid index
        4) Repeat steps 2 and 3 a total of {num_iterations} times
        """
        self.test_case.DirectedEnableStreamTest()

    @format_docstring(num_iterations=S2hFunctional.TEST_ITERATIONS)
    def DirectedDisableStreamTest(self):
        """Validate S2H stream disable feature

        If stream is disabled, the S2H valid index shall not change every 4ms
        1) Disable stream and get valid index register
        2) Wait for new packet insertion
        3) Current valid index register must be the same as initial
        4) Repeat steps 2 and 3 a total of {num_iterations} times
        """
        self.test_case.DirectedDisableStreamTest()

    @format_docstring(num_iterations=S2hFunctional.TEST_ITERATIONS)
    def DirectedResetStreamTest(self):
        """Validate S2H stream reset feature

        If stream is reset, valid_index register becomes invalid
        1) Reset and stop stream
        2) Wait for 12ms new packet insertion
        3) Verify valid index register is S2H_INVALID_PACKET_INDEX and S2H
           steam enabled status bit is False
        4) Repeat steps 1 to 3 a total of {num_iterations} times
        """
        self.test_case.DirectedResetStreamTest()
