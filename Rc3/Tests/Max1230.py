# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.


"""The RCTC3 FPGA communicates with two MAX1230 devices with 17 channels each.

There are 34 MAX1230 Registers, 17 per MAX1230.

MAX1230 has 16 Channels that monitor voltage readings
MAX1230 has a sensor that reports internal temperature
"""

from enum import Enum

from Rc3.Tests.Rc3Test import Rc3Test

MAX1230 = 33.0
MIN_MULTIPLIER = 0.60
MAX_MULTIPLIER = 2.40
TEMPERATURE = {'min': MAX1230 * MIN_MULTIPLIER, 'mean': MAX1230, 'max': MAX1230 * MAX_MULTIPLIER}
MAX_DIFF = .10


class Diagnostics(Rc3Test):
    """Test Read/Write Robustness"""

    def setUp(self, tester=None):
        super().setUp(tester)
        self.test_iterations = 1000
        self.max_fail_count = 1

    def DirectedReadTemperatureExhaustiveTest(self):
        """Read MAX1230 A/B Temperature

        1. Read chip temperature by reading Internal Temperature Register value
        2. Compare observed temperature with expected characterized temperature
        3. Repeat step 1-2 1000 times
        """
        for channel in range(2):
            with self.subTest(Max1230=f'Channel {channel}'):
                self.failures = []
                self.hil_samples = []
                self.bar_samples = []
                self.fail_count = 0

                for iteration in range(self.test_iterations):
                    self.read_temperatures(channel)
                    success = self.are_temps_within_limits(iteration, channel)

                    self.update_failed_iterations(success)
                    if self.fail_count >= self.max_fail_count:
                        break

                if self.failures:
                    table = self.rc.contruct_text_table(data=self.failures,
                                                        title_in=f'Max1230 Temperature Channel {channel} Failed')
                    self.Log('error', f'{table}')

                self.report_sample_average(self.hil_samples, 'HIL')
                self.report_sample_average(self.bar_samples, 'BAR')
                
                self.validate_iterations()

    def read_temperatures(self, channel):
        self.bar_temp = self.rc.max1230_channel_temperature(channel)
        self.hil_temp = self.rc.max1230_channel_temperature_using_hil(channel)
        self.bar_samples.append(self.bar_temp)
        self.hil_samples.append(self.hil_temp)


    class Voltages(Enum):
        ch00_3P6V_8A_MON = 0
        ch01_1P2V_2P5A_MON = 1
        ch02_3P3V_VX_501_600MA_MON = 2
        ch03_3P3V_LMK04832_PLL_600MA_MON = 3
        ch04_3P3V_LMK04832_CLK_MON = 4
        ch05_3P3V_RCTC_2P5A_MON = 5
        ch06_3P3V_HPS_SD_MON = 6
        ch07_V_CNTL_Y9_R = 7
        ch08_VREFP_ADC_RC = 8
        ch12_N9P5V_A_1A_DIV2P5_INV = 12
        ch13_N9P5V_B_1A_DIV2P5_INV = 13
        ch14_19P5V_B_1A_DIV6 = 14
        ch15_IMON_R_V19 = 15
        ch16_5P0V_8A_DIV2 = 16
        ch17_3P3V_16A_MON = 17
        ch18_2P5V_VPP_DDR4_RC_1A_MON = 18
        ch19_1P2V_VDD_DDR4_RC_5A_MON = 19
        ch20_3P0V_3A_MON = 20
        ch21_1P03V_VCC_GXB_RC_4A_MON = 21
        ch22_0P9V_VCCERAM_RC_4A_MON = 22
        ch23_20P1V_4A_DIV6_MON = 23
        ch24_0P85V_VCCP_RC_8A_MON = 24
        ch25_0P94V_HPS_RC_8A_MON = 25
        ch26_2P4V_VCCFUSE_RC_1A_MON = 26
        ch27_12V_15A_DIV4 = 27
        ch28_N10P1V_A_1A_DIV2P74_INV = 28
        ch29_N10P1V_B_1A_DIV2P74_INV = 29
        ch30_19P5V_A_1A_DIV6 = 30
        ch31_1P8V_8A_MON = 31


    EXPECTED_VOLTAGES = {
        Voltages.ch00_3P6V_8A_MON.value: 3.6,
        Voltages.ch01_1P2V_2P5A_MON.value: 1.2,
        Voltages.ch02_3P3V_VX_501_600MA_MON.value: 3.3,
        Voltages.ch03_3P3V_LMK04832_PLL_600MA_MON.value: 3.3,
        Voltages.ch04_3P3V_LMK04832_CLK_MON.value: 3.3,
        Voltages.ch05_3P3V_RCTC_2P5A_MON.value: 3.3,
        Voltages.ch06_3P3V_HPS_SD_MON.value: 3.3,  # reads .148 in hil
        Voltages.ch07_V_CNTL_Y9_R.value: 2.5,
        Voltages.ch08_VREFP_ADC_RC.value: 0.0,
        Voltages.ch12_N9P5V_A_1A_DIV2P5_INV.value: -9.5,
        Voltages.ch13_N9P5V_B_1A_DIV2P5_INV.value: -9.5,
        Voltages.ch14_19P5V_B_1A_DIV6.value: 19.5,
        Voltages.ch15_IMON_R_V19.value: 0.02,  #current monitor. Refer schematic Page 65 for the formula
        Voltages.ch16_5P0V_8A_DIV2.value: 5.0,
        Voltages.ch17_3P3V_16A_MON.value: 3.3,
        Voltages.ch18_2P5V_VPP_DDR4_RC_1A_MON.value: 2.5,
        Voltages.ch19_1P2V_VDD_DDR4_RC_5A_MON.value: 1.2,
        Voltages.ch20_3P0V_3A_MON.value: 3.0,
        Voltages.ch21_1P03V_VCC_GXB_RC_4A_MON.value: 1.03,
        Voltages.ch22_0P9V_VCCERAM_RC_4A_MON.value: 0.9,
        Voltages.ch23_20P1V_4A_DIV6_MON.value: 20.1,
        Voltages.ch24_0P85V_VCCP_RC_8A_MON.value: 0.85,
        Voltages.ch25_0P94V_HPS_RC_8A_MON.value: 0.94,
        Voltages.ch26_2P4V_VCCFUSE_RC_1A_MON.value: 2.4,
        Voltages.ch27_12V_15A_DIV4.value: 12.0,
        Voltages.ch28_N10P1V_A_1A_DIV2P74_INV.value: -10.1,
        Voltages.ch29_N10P1V_B_1A_DIV2P74_INV.value: -10.1,
        Voltages.ch30_19P5V_A_1A_DIV6.value: 19.5,
        Voltages.ch31_1P8V_8A_MON.value: 1.8
    }

    def DirectedReadVoltageExhaustiveTest(self):
        """Read Max1230 Voltage Monitors

        1. Read MAX1230 Channel Data Register for channel 0
        2. Using hil.rc3VmonRead, read the corresponding channel to step 1
        3. Compare values from step 1 and 2. Allow 2% difference
        4. Repeat step 1-3 for channel 0-15.
        5. Repeat step 1-4 1,000 times
        6. Using subtest, repeat step 1-5 on the two Max1230
        """
        
        self.log_unmonitored_voltages()

        self.failures = []
        for iteration in range(self.test_iterations):
            success = True

            for channel in Diagnostics.Voltages:

                self.expected_voltage = \
                    Diagnostics.EXPECTED_VOLTAGES[channel.value]
                self.bar_voltage = self.rc.voltage(channel.value)
                self.hil_voltage = self.rc.voltage_using_hil(channel.value)

                if channel not in self.unmonitored_voltages():
                    success &=self.are_voltages_within_limits(iteration,
                                                              channel)
            self.update_failed_iterations(success)
            if self.fail_count >= self.max_fail_count:
                break

        if self.failures:
            table = self.rc.contruct_text_table(
                data=self.failures,
                title_in=f'Max1230 Voltage Monitor Failures')
            self.Log('error', f'{table}')
        else:
            self.Log('info', f'Max1230 Voltages Passed')

        self.validate_iterations()

    def log_unmonitored_voltages(self):
        for channel in self.unmonitored_voltages():
            expected_voltage = \
                Diagnostics.EXPECTED_VOLTAGES[channel.value]
            hil_voltage = self.rc.voltage_using_hil(channel.value)

            self.Log('warning',
                     f'{channel.name} is unmonitored (expected, actual): '
                     f'{expected_voltage, hil_voltage}')

    def unmonitored_voltages(self):
        return [Diagnostics.Voltages.ch15_IMON_R_V19,
                Diagnostics.Voltages.ch06_3P3V_HPS_SD_MON]

    def are_voltages_within_limits(self, iteration, channel):
        max, min = self.get_voltage_limits(channel)

        hil_voltage = min <= self.hil_voltage and self.hil_voltage <= max
        bar_voltage = min <= self.bar_voltage and self.bar_voltage <= max
        observed_diff = abs(self.hil_voltage - self.bar_voltage)
        expected_diff = self.get_max_diff()
        diff = observed_diff <= abs(expected_diff)

        if not all([hil_voltage, bar_voltage, diff]):
            entry = {'Iteration': iteration, 'Channel': channel,
                     'HIL': self.hil_voltage, 'BAR': self.bar_voltage,
                     'Expected': self.expected_voltage, 'Diff': observed_diff,
                     f'Max Diff({MAX_DIFF * 100}%)':
                         self.expected_voltage * MAX_DIFF}
            self.failures.append(entry)
            return False
        else:
            return True

    def get_max_diff(self):
        if self.expected_voltage == 0:
            return 0.1
        return self.expected_voltage * MAX_DIFF

    def get_voltage_limits(self, channel):
        multiplier = 0.2
        if self.expected_voltage == 0:
            max, min = 0.4, -0.4
        elif self._is_hps_sd_mon(channel):
            self.Log('debug', f'{channel.name} is near zero. Setting min '
                                f'and max to default of +/- 0.5')
            max, min = 0.5, -0.5
        else:
            min = self.expected_voltage - (abs(self.expected_voltage) *
                                           multiplier)
            max = self.expected_voltage + (abs(self.expected_voltage) *
                                           multiplier)
        return max, min


    def are_temps_within_limits(self, iteration, channel):
        min, mean, max = TEMPERATURE.values()
        sample_mean = abs(self.hil_temp + self.bar_temp) / 2

        hil_temp = min < self.hil_temp < max
        bar_temp = min < self.bar_temp < max
        max_diff = abs(self.hil_temp - self.bar_temp) < sample_mean * MAX_DIFF

        if not all([hil_temp, bar_temp, max_diff]):
            entry = {'Iteration': iteration, 'Channel': channel,
                     'HIL': self.hil_temp, 'BAR': self.bar_temp}
            self.failures.append(entry)
            return False
        else:
            return True

    def report_sample_average(self, samples, method):
        addition = sum(samples)
        size = len(samples)
        average = addition / size
        message = f'{method} Average temperature is {average}.'
        self.rc.log_device_message(message, 'info')

    def _is_hps_sd_mon(self, channel):
        if channel.name == 'ch06_3P3V_HPS_SD_MON':
            hil = self.check_almost_equal(
                self.hil_voltage, expected=0, delta=.5, log_type='debug')
            bar = self.check_almost_equal(
                self.bar_voltage, expected=0, delta=.5, log_type='debug')
            return all([hil, bar])
        return False
