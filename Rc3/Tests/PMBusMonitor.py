# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

"""RCTC3 has one LTM4680 Power Regulator

The RCTC3 FPGA has an i2c master for the LTM4680
"""

from ThirdParty.SASS.suppress_sensor import suppress_sensor

from Common.testbench.i2c_tester import\
    address_nak_test,\
    tx_fifo_count_error_bit_test,\
    tx_fifo_count_test,\
    validate_i2c_address_test
from Rc3.instrument.pmbus_monitor_i2c_interface import\
    create_interfaces,\
    PmBusMonitorInterface
from Rc3.Tests.Rc3Test import Rc3Test


class Diagnostics(Rc3Test):
    """Test communication robustness"""

    def setUp(self, tester=None):
        super().setUp(tester)
        self.test_iterations = 10
        self.max_fail_count = 1
        self.interfaces = create_interfaces(self.rc)

    def DirectedReadExhaustiveTest(self):
        """Read a subset of known value registers

        Read value of read-only PMBus device registers like Capability and
        Revision and compare them to expected value
        1) Read first read-only register and compare to expected value
        2) Repeat step one for remaining read-only registers
        3) Repeat a total of 100 times
        """
        for iteration in range(self.test_iterations):
            success = True
            registers = PmBusMonitorInterface.KNOWN_VALUE_REGISTERS

            for interface in self.interfaces:
                with suppress_sensor(
                        'FVAL DirectedPmbusMonitorWriteReadExhaustiveTest',
                        'RC3'):
                    for command, expected_data in registers.items():
                        actual_data = interface.read(command)

                        if expected_data != actual_data:
                            self.Log(
                                'error',
                                self.exhaustive_read_error_msg(
                                    iteration,
                                    interface.interface_name,
                                    expected_data,
                                    actual_data))
                            success = False

            self.update_failed_iterations(success)
            if self.fail_count >= self.max_fail_count:
                break

        self.validate_iterations()

    def exhaustive_read_error_msg(self, iteration, name, expected_data,
                                  actual_data):
        return f'Iteration_{iteration}: {name} failed read test ' \
               f'(Expected, Actual): 0x{expected_data:02X}, ' \
               f'0x{actual_data:02X}'


class Functional(Rc3Test):
    """Test operation of the i2c master"""

    def setUp(self, tester=None):
        super().setUp(tester)
        self.test_iterations = 10
        self.max_fail_count = 1
        self.interfaces = create_interfaces(self.rc)

    def DirectedTxFifoCountErrorBitCheckingTest(self):
        """Verify the Transmit FIFO Count Error bit of the status register

        The error bit is set if a message is sent to a chip using less than
        two bytes
        - Create a message with an invalid number of bytes and verify error bit
          is set
        - Create a message with an valid number of bytes and verify error bit
          is clear
        - Repeat above steps for all interfaces a total of 10 times
        """

        for iteration in range(self.test_iterations):
            success = True

            for interface in self.interfaces:
                with suppress_sensor('FVAL DirectedPmbusMonitor'
                                     'TxFifoCountErrorBitCheckingTest', 'RC3'):
                    error_msg = tx_fifo_count_error_bit_test(interface)
                    if error_msg:
                        [self.Log('error', msg) for msg in error_msg]
                        success = False

            self.update_failed_iterations(success)
            if self.fail_count >= self.max_fail_count:
                break

        self.validate_iterations()

    def DirectedAddressNakTest(self):
        """Verify the Address NAK Error bit of the status register

        The error bit is set if a message is sent to a chip using an invalid
        i2c address
        - Create a message with an invalid address and verify error bit is set
        - Create a message with an valid address and verify error bit is clear
        - Repeat above steps for all interfaces a total of 10 times
        """

        for iteration in range(self.test_iterations):
            success = True

            for interface in self.interfaces:
                with suppress_sensor('FVAL DirectedPmbusMonitorAddressNakTest',
                                     'RC3'):
                    error_msgs = address_nak_test(interface)
                    if error_msgs:
                        [self.Log('error', msg) for msg in error_msgs]
                        success = False

            self.update_failed_iterations(success)

            if self.fail_count >= self.max_fail_count:
                break

        self.validate_iterations()

    def DirectedTxFifoCountTest(self):
        """Verify the Transmit FIFO Count of the status register

        The counter denotes the number of items in the fifo
        - Perform 20 random number of data pushes onto the FIFO of each BPS
        - Perform a push of corner cases 0 and max fifo size.
        - Verify Transmit FIFO Count matches total data sent at each push
        - Repeat above steps for all interfaces a total of 10 times
        """
        self.test_iterations = 1  # test takes too long. once is fine

        for iteration in range(self.test_iterations):
            success = True

            with suppress_sensor('FVAL DirectedPmbusMonitorTxFifoCountTest',
                                 sensor_names=['HBIMainBoard', 'HBIDTB']):
                for interface in self.interfaces:
                    error_msgs = tx_fifo_count_test(interface)
                    if error_msgs:
                        [self.Log('error', msg) for msg in error_msgs]
                        success = False

            self.update_failed_iterations(success)
            if self.fail_count >= self.max_fail_count:
                break

        self.validate_iterations()

    def DirectedValidateI2cAddressTest(self):
        """ Verify interface i2c addresses are valid

        1) Send an i2c packet using all possible i2c address (0x00 to 0xFF)
        2) Make a list of addresses that do not return an address nak
        3) Verify interface i2c address is in list of valid addresses
        4) Run above steps once
        """
        self.test_iterations = 1
        success = True

        with suppress_sensor('FVAL DirectedPmbusMonitorValidateI2cAddressTest',
                             'RC3'):
            for interface in self.interfaces:
                error_msgs = validate_i2c_address_test(interface)
                if error_msgs:
                    [self.Log('error', msg) for msg in error_msgs]
                    success = False

        self.update_failed_iterations(success)
        self.validate_iterations()
