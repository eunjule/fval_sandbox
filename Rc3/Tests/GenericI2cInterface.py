# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

"""RCTC3 FPGA has Generic I2C Registers used to communicate to I2C devices

Currently, RC3 POR does not have an I2C devices on this lines. Therefore,
the validation will be limited to register write/read exhaustive testing and
FIFO Count checks.
"""
import random

from Common.testbench.i2c_tester import tx_fifo_count_error_bit_test, \
    tx_fifo_count_test, tx_fifo_reset_test, tx_fifo_error_msg
from Rc3.instrument import rc3_register
from Rc3.instrument.generic_i2c_interface import create_interfaces, GENERIC_GROUPS
from Rc3.instrument.rc3_register import RELAYS_GPIO_I2C_CONFIGURATION
from Rc3.Tests.Rc3Test import Rc3Test


DEFAULT_ITERATIONS = 10000


class Functional(Rc3Test):
    """Test for robustness of write/read for registers and
        test operation of the i2c master

        No I2C device is present on the generic GPIO/I2C Bus
        therefore the tests are focused more on the functionality of
        registers
    """

    def setUp(self, tester=None):
        super().setUp(tester)
        self.test_iterations = DEFAULT_ITERATIONS
        self.max_fail_count = 1
        self.tx_fifo_depth = 2047
        self.emtpy_tx_fifo_depth = 0
        self.generic_interfaces = create_interfaces(self.rc)

    def DirectedRelayGpioI2cConfigurationRegisterTest(self):
        """Perform write/read exhaustive test on Relay GPIO/I2C register

        1) Randomize a 10 bit value
        2) Write random value to Relay GPIO/I2C register
        3) Read the value back and compare to expected
        4) Repeat 1-3 1000 times"""
        with ResetRelayConfigurationRegister(self.rc):
            for iteration in range(self.test_iterations):
                expected = self.generate_random_value(10)
                self.write_relays_gpio_i2c_configuration_reg(expected)
                register = self.read_relays_gpio_i2c_configuration_reg()
                observed = register.value
                if observed != expected:
                    self.log_register_error(iteration,
                                            RELAYS_GPIO_I2C_CONFIGURATION,
                                            observed, expected)
                    self.update_failed_iterations(False)

                if self.fail_count >= self.max_fail_count:
                    break

        self.validate_iterations()

    def DirectedExhaustiveWriteReadGenericRegistersTest(self):
        """Write to and then read from Generic I2C Registers

        1. Set the Relay GPIO/I2C register to 0x0 to disable I2C
        2. Write a random value to the Generic Registers
        3. Verify data written to the registers matches its read data
        4. Repeat step 2-3 10,000 times for each of the 10 groups
        """
        registers = {rc3_register.GENERIC_I2C_CONTROL: 1,
                     rc3_register.GENERIC_I2C_TX_FIFO: 8,
                     rc3_register.GENERIC_I2C_RESET: 1,
                     rc3_register.GENERIC_I2C_FREQUENCY: 3}

        self.rc.write_relays_gpio_i2c_configuration_reg(0x0)
        self.reset_all_generic_i2c()
        default_modes = self.read_generic_i2c_frequency_modes()
        try:
            for register, bit_length in registers.items():
                for index in range(register.REGCOUNT):
                    address = register.ADDR + index * register.BASEMUL
                    reg_id = f'{register().name()}(0x{address:X})'
                    with self.subTest(REG=reg_id):
                        self.fail_count = 0
                        for iteration in range(self.test_iterations):
                            expected = self.generate_random_value(bit_length)
                            self.rc.write_bar_register(register(expected),
                                                       index=index)
                            observed = self.rc.read_bar_register(
                                register, index=index).value
                            success = self.compare_data(iteration, register,
                                                        expected, observed)
                            self.update_failed_iterations(success)

                            if self.fail_count >= self.max_fail_count:
                                break
                        self.validate_iterations()
        finally:
            self.reset_all_generic_i2c()
            self.write_generic_i2c_frequency_modes(default_modes)

    def reset_all_generic_i2c(self):
        for index in range(rc3_register.GENERIC_I2C_RESET.REGCOUNT):
            self.rc.reset_generic_i2c(index)

    def read_generic_i2c_frequency_modes(self):
        reg = rc3_register.GENERIC_I2C_FREQUENCY
        modes = [self.rc.read_bar_register(reg, i).mode
                 for i in range(reg.REGCOUNT)]
        return modes

    def write_generic_i2c_frequency_modes(self, modes):
        for index in range(rc3_register.GENERIC_I2C_FREQUENCY.REGCOUNT):
            reg = rc3_register.GENERIC_I2C_FREQUENCY(mode=modes[index])
            self.rc.write_bar_register(reg, index)

    def DirectedTxFifoCountErrorBitCheckingTest(self):
        """Verify the Transmit FIFO Count Error bit of the status register

        The error bit is set if a message is sent to a chip using less than two
        bytes
        - Set relay gpio/i2c configuration register to all ones to select
        I2c mode
        - Create a message with an invalid number of bytes and verify error bit
          is set
        - Create a message with an valid number of bytes and verify error bit
          is clear
        - Repeat above steps a total of 10 times
        """
        self.rc.write_relays_gpio_i2c_configuration_reg(0x3FF)

        self.resolve_test_iterations()
        for iteration in range(self.test_iterations):
            success = True

            for interface in self.generic_interfaces:
                error_msgs = tx_fifo_count_error_bit_test(interface)
                if error_msgs:
                    [self.Log('error', msg) for msg in error_msgs]
                    success = False

            self.update_failed_iterations(success)
            if self.fail_count >= self.max_fail_count:
                break

        self.validate_iterations()

    def DirectedTxFifoCountTest(self):
        """Verify the Transmit FIFO Count of the status register

        The counter denotes the number of items in the fifo
        - Set relay gpio/i2c configuration register to all ones to select
        I2c mode
        - Perform 20 random number of data pushes onto the FIFO of each Generic
        Interface
        - Perform a push of corner cases 0 and max fifo size.
        - Verify Transmit FIFO Count matches total data sent at each push
        - Repeat above steps a total of 1 times
        """
        self.rc.write_relays_gpio_i2c_configuration_reg(0x3FF)

        self.resolve_test_iterations()
        for iteration in range(self.test_iterations):
            success = True

            for interface in self.generic_interfaces:
                error_msgs = tx_fifo_count_test(interface)
                if error_msgs:
                    [self.Log('error', msg) for msg in error_msgs]
                    success = False

            self.update_failed_iterations(success)
            if self.fail_count >= self.max_fail_count:
                break

        self.validate_iterations()

    def resolve_test_iterations(self):
        if self.test_iterations == DEFAULT_ITERATIONS:
            self.test_iterations = 5

    def DirectedResetTxFifoTest(self):
        """Fill Generic I2C Tx FIFO then Reset

        1. Send 2047 8-bit values to tx fifo
        2. Check that the tx fifo count matches step 1 count
        3. Reset generic I2C
        4. Check that tx fifo now reads 0
        5. Repeat 1-4 for all 10 Generic groups
        6. Repeat 1-5 5 times.
        """
        self.resolve_test_iterations()
        self.clear_all_tx_fifos()

        for interface in self.generic_interfaces:
            with self.subTest(GenericGroup=f'{interface.interface_index}'):
                for self.iteration in range(self.test_iterations):
                    success = True
                    error_msgs = tx_fifo_reset_test(self, interface)
                    if error_msgs:
                        [self.Log('error', msg) for msg in error_msgs]
                        success = False

                    self.update_failed_iterations(success)
                    if self.fail_count >= self.max_fail_count:
                        break
                self.validate_iterations()

    def fill_up_generic_i2c_tx_fifo(self, fifo_depth, index):
        self.rc.fill_up_generic_i2c_tx_fifo(fifo_depth, index)

    def check_tx_fifo_count(self, iteration, index, expected):
        observed = self.rc.read_generic_i2c_tx_fifo_count(index)
        if observed != expected:
            self.Log('error', tx_fifo_error_msg(iteration, index, expected,
                                                observed))
            return False
        return True

    def read_register(self, register, index):
        return self.rc.read_bar_register(register, index)

    def generate_random_value(self, bit_length):
        expected = random.getrandbits(bit_length)
        return expected

    def read_relays_gpio_i2c_configuration_reg(self):
        return self.rc.read_relays_gpio_i2c_configuration_reg()

    def write_relays_gpio_i2c_configuration_reg(self, expected):
        self.rc.write_relays_gpio_i2c_configuration_reg(expected)

    def log_register_error(self, iteration, register, observed, expected):
        self.Log('error', error_msg(iteration, register, observed, expected))

    def compare_data(self, iteration, register, expected, observed):
        if expected != observed:
            print(iteration, register, observed, expected)
            self.Log('error', error_msg(iteration, register, observed,
                                        expected))
            return False
        else:
            return True

    def clear_all_tx_fifos(self):
        for index in range(GENERIC_GROUPS):
            self.rc.reset_generic_i2c(index)

def tx_fifo_error_msg(iteration, group, expected, observed):
    return f'Iteration: {iteration} Generic {group} I2C Tx FIFO Count ' \
           f'Failure. Expected {expected} Observed {observed}.'

def error_msg(iteration, register, observed, expected):
    return f'Iteration: {iteration} - {register().name()}: ' \
           f'Observed 0x{observed:X} does NOT match expected 0x{expected:X}'


class ResetRelayConfigurationRegister():
    def __init__(self, rc):
        self.rc = rc

    def __enter__(self):
        pass

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.rc.write_relays_gpio_i2c_configuration_reg(0x0)
