# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

"""FPGA has 7 Clock Frequency Measurement registers

FPGA has measurement logic for calculating observed frequencies.
"""

from enum import Enum

from Rc3.Tests.Rc3Test import Rc3Test

FREQ_VARIANCE_FACTOR = 2 / 100.0

class FrequencyMeasurement(Rc3Test):
    """Test clock frequencies

    The following clocks are measured:
    Clock Name........| Expected MHz
    ------------------------------
    Measuring Clk.....| 125
    PCIe User Clk.....| 125
    FPGA DDR Ref Clk..| 33.33
    HPS DDR Ref Clk...| 33.33
    MS Clk Transceiver| 125
    MS Clk Fabric.....| 125
    HPS User Clk......| 125
    """

    class Clocks(Enum):
        MEASURING_CLK = 0
        PCIE_USER_CLK = 1
        FPGA_DDR4_REF_CLK = 2
        HPS_DDR4_REF_CLK = 3
        MS_CLK_XCVR = 5
        MS_CLK_FABRIC = 6
        HPS_USER_CLK = 7

    EXPECTED_FREQS = {Clocks.MEASURING_CLK.value: 125.0,
                      Clocks.PCIE_USER_CLK.value: 125.0,
                      Clocks.FPGA_DDR4_REF_CLK.value: 33.33,
                      Clocks.HPS_DDR4_REF_CLK.value: 33.33,
                      Clocks.MS_CLK_XCVR.value: 125.0,
                      Clocks.MS_CLK_FABRIC.value: 125.0,
                      Clocks.HPS_USER_CLK.value: 125.0}

    def setUp(self, tester=None):
        super().setUp(tester)
        self.test_iterations = 10000
        self.max_fail_count = 10

    def DirectedFrequencyReadTest(self):
        """Verify each clock measurement has a valid frequency

        1) Read Clock Frequency Measurement register
        2) Calculate the measured frequency from data read
        3) Compare measured frequency with expected within +/- 2%
        4) Repeat steps 1 to 3 a total of 10000 times
        """

        for clock in FrequencyMeasurement.Clocks:
            self.fail_count = 0
            with self.subTest(ClockFrequencyMeasruemtn=f'Clock {clock.name}'):
                for iteration in range(self.test_iterations):
                    success = True

                    measured_freq = \
                        self.rc.read_clock_frequency_mhz(clock.value)
                    expected_freq = FrequencyMeasurement.EXPECTED_FREQS[
                        clock.value]
                    variance_factor = measured_freq * FREQ_VARIANCE_FACTOR
                    if not (expected_freq + variance_factor) >= measured_freq \
                           >= (expected_freq - variance_factor):
                        self.Log(
                            'error',
                            self.frequency_mismtach_msg(iteration,
                                                        clock.name,
                                                        expected_freq,
                                                        measured_freq))
                        success = False

                    self.update_failed_iterations(success)
                    if self.fail_count >= self.max_fail_count:
                        break

                self.validate_iterations()

    def frequency_mismtach_msg(self, iteration, clock_name, expected, actual):
        factor = f'{int(FREQ_VARIANCE_FACTOR * 100)}'
        return f'Iteration {iteration}: {clock_name} MHz expected within ' \
               f'{factor}% (expected, actual): {expected:0.02f}, {actual:0.02f}'
