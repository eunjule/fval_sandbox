# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

"""RCTC has a bi-directional IO interface to the Accessory Card
"""

from random import getrandbits
from time import sleep

from Common.fval import SkipTest
from Common.utilities import compliment_bytes, negative_one
from Rc3.Tests.Rc3Test import Rc3Test


class Diagnostics(Rc3Test):
    """Test communication robustness"""

    def setUp(self, tester=None):
        super().setUp(tester)
        self.test_iterations = 10
        self.max_fail_count = 1

    # @skip('This register bit is tied to the acc_card_out input signal, which '
    #       'is tied to the acc_card_clk signal. Cannnot test this becuase it '
    #       'is basically a clock input signal with no logic tied to it')
    # def DirectedAccesoryCardOutValueTest(self):
    #     """Verify value of the "acc_card_out" pins from the Accessory Card
    #
    #     Pin value is determined by the Accessory Card
    #     1) Read bit 0 of the Accessory Card Out register
    #     2) Verify value is as expected.
    #     3) Repeat above steps a total of 10 times
    #     """


class Functional(Rc3Test):
    """Test operation of the interface"""

    def setUp(self, tester=None):
        super().setUp(tester)
        self.test_iterations = 50
        self.max_fail_count = 2

    def tearDown(self):
        self.rc.write_acc_card_io_output(0)
        self.rc.write_acc_card_io_control(0)
        self.test_wait_seconds()
        super().tearDown()

    def DirectedAccCardIoFlippingBitsTest(self):
        """ Verify operation of control field

        NOTE: Only bits 0 to 23 are available for bi-directional testing. Bits
        24 to 31 are tied to DUT Thermal data/Clk on the RCTC3 Loopback
        schematic as of FabA

        The control field sets the direction of the fpga_debug bus one bit at a
        time.
        Control bit is 1: Output field bit is reflected on input field bit
        Control bit is 0: Input field bit reflects external connection
        Test steps:
        1) Get the default input field value by setting control to all input
        2) Set output field to inverse of default input
        3) Perform a walking one's on the control field. Sets bit to output one
        bit at a time
        4) Verify corresponding input field bits toggle
        5) Repeat steps 3 and 4 a total of 50 times
        """
        fab_letter = self.rc.get_fab_letter().upper()
        if fab_letter != 'A':
            raise SkipTest(f'This test is only compatible with a FabA board. '
                           f'Current board is a Fab{fab_letter}')

        acc_card_io_byte_size = 3

        # get default input field's value when control is set to all input
        self.rc.write_acc_card_io_control(0)
        self.test_wait_seconds()
        original_input = self.rc.acc_card_io_input()

        # set the output field's value to compliment of default input
        output_value = compliment_bytes(
            original_input, num_bytes=acc_card_io_byte_size)
        self.rc.write_acc_card_io_output(output_value)

        # calculate expected input field values
        expected_values = self.flipping_bits_expected_input_values(
            original_input, acc_card_io_byte_size)

        # send control field to output one bit at a time
        for iteration in range(self.test_iterations):
            success = True

            for bit_num in range(8 * acc_card_io_byte_size):
                self.rc.write_acc_card_io_control(1 << bit_num)
                self.test_wait_seconds()

                actual = self.rc.acc_card_io_input()
                expected = expected_values[bit_num]
                if actual != expected:
                    msg = self.error_msg_flipping_bits(
                        iteration, expected, actual, acc_card_io_byte_size)
                    self.Log('error', msg)
                    success = False

            self.update_failed_iterations(success)
            if self.fail_count >= self.max_fail_count:
                break

        self.validate_iterations()
    
    def flipping_bits_expected_input_values(self, original_input, num_bytes):
        out_compliment = compliment_bytes(original_input, num_bytes)

        expected = []
        num_bits = num_bytes * 8
        for i in range(0, num_bits):
            if ((out_compliment >> i) & 0x1) == 1:
                expected.append(original_input | out_compliment)
            else:
                f_s = negative_one(num_bytes)
                expected.append(original_input & (f_s ^ 1 << i))
        return expected

    def error_msg_flipping_bits(self, iteration, expected, actual, num_bytes):
        expected = f'{expected:0b}'.zfill(8 * num_bytes)
        actual = f'{actual:0b}'.zfill(8 * num_bytes)
        return f'Iteration {iteration}: Invalid input field ' \
               f'(expected, actual) 0b{expected}, 0b{actual}'

    def DirectedAccCardIoOutputToInputTest(self):
        """ Verify transfer of data from output to input

        1) Set direction control to all output
        2) Write randomized data to output data
        3) Verify input data matches output data
        4) Repeat steps 2 and 3 50 times
        """
        for iteration in range(self.test_iterations):
            success = self.output_control_enabled_scenarion(iteration)
            success &= self.output_control_disabled_scenario(iteration)
            self.update_failed_iterations(success)
            if self.fail_count >= self.max_fail_count:
                break

        self.validate_iterations()

    def output_control_enabled_scenarion(self, iteration):
        self.rc.write_acc_card_io_control(0xFFFF_FFFF)
        self.test_wait_seconds()
        expected_data = self.generate_random_bytes(exclude=0xFFFF_FFFF,
                                                   num_bytes=4)
        self.rc.write_acc_card_io_output(expected_data)
        self.test_wait_seconds()
        actual = self.rc.acc_card_io_input()
        if actual != expected_data:
            self.Log('error', self.error_msg_output_enabled(
                iteration, expected_data, actual))
            return False
        else:
            return True

    def error_msg_output_enabled(self, iteration, expected, actual):
        return f'Iteration {iteration}: Output to Input should match ' \
               f'(expected, actual): 0x{expected:02X}, 0x{actual:02X}'

    def output_control_disabled_scenario(self, iteration):
        self.rc.write_acc_card_io_control(0x0000_0000)
        self.test_wait_seconds()
        expected_data = self.generate_random_bytes(
            exclude=self.rc.acc_card_io_input(), num_bytes=4)
        self.rc.write_acc_card_io_output(expected_data)
        self.test_wait_seconds()
        actual = self.rc.acc_card_io_input()
        if actual == expected_data:
            self.Log('error', self.error_msg_output_disabled(
                iteration, expected_data, actual))
            return False
        else:
            return True

    def error_msg_output_disabled(self, iteration, expected, actual):
        return f'Iteration {iteration}: Output to Input should not match ' \
               f'(expected, actual): 0x{expected:02X}, 0x{actual:02X}'

    def test_wait_seconds(self):
        sleep(0.025)

    def generate_random_bytes(self, exclude, num_bytes):
        data = getrandbits(8 * num_bytes)
        while data == exclude:
            data = getrandbits(8 * num_bytes)
        return data

    def DirectedAccCardIoOutputWriteReadTest(self):
        """Verify Write/Read access of Accessory Card IO Data Output register

        1) Perform randomized 32-bit write to register and use corner case
        of all 0s or all Fs
        2) Read register and make sure data matches written data
        3) Repeat above steps a total of 50 times
        """
        writes = self.generate_writes()

        for iteration in range(self.test_iterations):
            write = writes[iteration]
            self.rc.write_acc_card_io_output(write)
            read = self.rc.acc_card_io_output()
            if read != write:
                self.Log('error',
                         self.error_msg_write_read(iteration, write, read))
                self.update_failed_iterations(False)
            if self.fail_count >= self.max_fail_count:
                break

        self.validate_iterations()

    def DirectedAccCardIoControlReadWriteAccessTest(self):
        """Verify Write/Read access of Accessory Card IO Data Control register

        1) Perform randomized 32-bit write to register and use corner case
        of all 0s or all Fs
        2) Read register and make sure data matches written data
        3) Repeat above steps a total of 50 times
        4) Reset control to all input
        """
        writes = self.generate_writes()

        for iteration in range(self.test_iterations):
            write = writes[iteration]
            self.rc.write_acc_card_io_control(write)
            read = self.rc.acc_card_io_control()
            if read != write:
                self.Log('error',
                         self.error_msg_write_read(iteration, write, read))
                self.update_failed_iterations(False)
            if self.fail_count >= self.max_fail_count:
                break

        self.validate_iterations()

    def generate_writes(self):
        writes = [getrandbits(32) for i in range(self.test_iterations - 2)]
        writes.insert(0, 0)
        writes.insert(0, 0xFFFF_FFFF)
        return writes

    def error_msg_write_read(self, iteration, write, read):
        return f'Iteration {iteration}: Failed Write-Read register test ' \
               f'(write, read): 0x{write:08X}, 0x{read:08X}'
