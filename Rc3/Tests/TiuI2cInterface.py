# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

"""RCTC has a TIU I2c master

The Fpga can communicate with a Calbase PCA9555 chip that controls fan function
and it's I2C address is 0x4E.
"""
import random

from Common.testbench.i2c_tester import address_nak_test, \
    tx_fifo_count_error_bit_test, tx_fifo_count_test
from Rc3.instrument.tiu_i2c_interface import UNUSED_I2C_ADDR, create_interfaces
from Rc3.Tests.Rc3Test import Rc3Test


class Functional(Rc3Test):
    """Test communication robustness"""

    def setUp(self, tester=None):
        super().setUp(tester)
        self.test_iterations = 10
        self.max_fail_count = 1
        self.tiu = create_interfaces(self.rc)

    def DirectedWriteReadExhaustiveTest(self):
        """Write data and read it back to register 4, 5

            These are inversion polarity registers for port 0 and 1.
            They can be used as scratch registers since it woudl take a
            second command for those values to take effect.
        1) Write random data to PCA955 register 4, 5.
        2) Read data back and verify match with data written
        3) Repeat above steps 10 times
        """
        registers = [0x4, 0x5]
        for iteration in range(self.test_iterations):
            success = True
            for tiu in self.tiu:
                with RcRootI2cLockTiu(self.rc):
                    for register in registers:
                        expected = random.getrandbits(8)
                        tiu.write(register, data=expected)
                        observed = tiu.read(register)

                        if expected != observed:
                            self.Log('error',
                                     f'Write/Read: Expected {expected} '
                                     f'Observed: {observed}')
                            success = False
            self.update_failed_iterations(success)

            if self.fail_count >= self.max_fail_count:
                break

        self.validate_iterations()


class Diagnostics(Rc3Test):
    """Test operation of the i2c master"""

    def setUp(self, tester=None):
        super().setUp(tester)
        self.test_iterations = 10
        self.max_fail_count = 1
        self.tiu = create_interfaces(self.rc)

    def DirectedTxFifoCountErrorBitCheckingTest(self):
        """Verify the Transmit FIFO Count Error bit of the status register

        The error bit is set if a message is sent to a chip using less than two
        bytes
        - Create a message with an invalid number of bytes and verify error bit
          is set
        - Create a message with an valid number of bytes and verify error bit
          is clear
        - Repeat above steps a total of 10 times
        """
        for iteration in range(self.test_iterations):
            success = True

            for tiu in self.tiu:
                with RcRootI2cLockTiu(self.rc):
                    error_msgs = tx_fifo_count_error_bit_test(tiu)
                if error_msgs:
                    [self.Log('error', msg) for msg in error_msgs]
                    success = False

            self.update_failed_iterations(success)
            if self.fail_count >= self.max_fail_count:
                break

        self.validate_iterations()

    def DirectedAddressNakTest(self):
        """Verify the Address NAK Error bit of the status register

        The error bit is set if a message is sent to a chip using an invalid
        i2c address
        - Create a message with an invalid address and verify error bit is set
        - Create a message with an valid address and verify error bit is clear
        - Repeat above steps a total of 10 times
        """
        for iteration in range(self.test_iterations):
            success = True
            for tiu in self.tiu:
                with RcRootI2cLockTiu(self.rc):
                    error_msgs = address_nak_test(
                        tiu, unused_i2c_addr=UNUSED_I2C_ADDR)
                    if error_msgs:
                        [self.Log('error', msg) for msg in error_msgs]
                        success = False

            self.update_failed_iterations(success)

            if self.fail_count >= self.max_fail_count:
                break

        self.validate_iterations()

    def DirectedTxFifoCountTest(self):
        """Verify the Transmit FIFO Count of the status register

        The counter denotes the number of items in the fifo
        - Perform 20 random number of data pushes onto the FIFO of each BPS
        - Perform a push of corner cases 0 and max fifo size.
        - Verify Transmit FIFO Count matches total data sent at each push
        - Repeat above steps a total of 10 times
        """
        self.test_iterations = 1
        for iteration in range(self.test_iterations):
            success = True
            for tiu in self.tiu:
                with RcRootI2cLockTiu(self.rc):
                    error_msgs = tx_fifo_count_test(tiu)
                if error_msgs:
                    [self.Log('error', msg) for msg in error_msgs]
                    success = False

            self.update_failed_iterations(success)
            if self.fail_count >= self.max_fail_count:
                break

        self.validate_iterations()


class RcRootI2cLockTiu():
    def __init__(self, rc):
        self.rc = rc

    def __enter__(self):
        self.rc.rc_root_i2c_lock_tiu()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.rc.rc_root_i2c_unlock()
