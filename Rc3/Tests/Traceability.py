# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

"""Traceability tests

FPGA bitstream and the FPGA itself have unique identifiers. The following
sources will be tested:
- FPGA Version
- HG ID
- Compatibility (Fab Version)
- Chip ID
"""

from Common.utilities import format_docstring
from Rc3.Tests.Rc3Test import Rc3Test


TEST_ITERATIONS = 1000
MAX_FAIL_COUNT = 10


class Image(Rc3Test):
    """Verify image related traceability"""

    def setUp(self, tester=None):
        super().setUp(tester)
        self.test_iterations = TEST_ITERATIONS
        self.max_fail_count = MAX_FAIL_COUNT

    def DirectedHgCleanBitTest(self):
        """Determine if FPGA source code is committed

        Test steps:
        1) Read HG Clean bit located at bit 31 of the HG ID Upper register
        2) Verify bit == 1
        3) Do not repeat test
        """

        for iteration in range(self.test_iterations):
            actual = self.rc.get_hg_clean_bit()
            expected = 1
            if expected != actual:
                self.Log('error', self.error_msg(iteration, expected, actual))
                self.update_failed_iterations(success=False)

            if self.fail_count >= self.max_fail_count:
                break

        self.validate_iterations()

    @format_docstring(num_iterations=TEST_ITERATIONS)
    def DirectedHgIdTest(self):
        """Determine if HG identification is a valid number

        HG ID value:
        Bits    | Register
        ------------------
        47:32   | HG ID Upper (15:0)
        31:0    | HG ID Lower (31:0)

        Test steps:
        1) Read HG ID from the Upper and Lower registers
        2) Verify ID matches the ID found in the per_build_params file in the
        FPGA image release directory
        3) Re-read HG ID BAR register and verify stable value
        4) Repeat step 3 {num_iterations} times
        """
        fpga_hg_id = self.rc.hg_id()
        file_hg_id = self.rc.hg_id_from_file()
        if fpga_hg_id != file_hg_id:
            self.Log('error', self.hg_id_error_msg(file_hg_id, fpga_hg_id))

        for iteration in range(self.test_iterations):
            temp = self.rc.hg_id()
            if fpga_hg_id != temp:
                self.Log('error', self.error_msg(iteration,
                                                 expected=fpga_hg_id,
                                                 actual=temp))
                self.update_failed_iterations(success=False)
            if self.fail_count >= self.max_fail_count:
                break
        self.validate_iterations()

    def hg_id_error_msg(self, per_build_params, bar_register):
        return f'HG ID does not match (from per_build_params file, ' \
               f'from bar register): 0x{per_build_params:016X}, ' \
               f'0x{bar_register:016X}'

    def DirectedFpgaVersionTest(self):
        """Verify FPGA Version matches image file name

        1) Read FPGA Version register
        2) Extract version from the image file name found in FVAL configs.py
        3) Compare versions obtained from step 1 and 2
        3) Do not repeat test
        """

        for iteration in range(self.test_iterations):
            version = self.rc.fpga_version()
            file_name_version = self.get_version_number_from_config_file_name()

            if file_name_version != version:
                self.Log('error', self.error_msg(iteration,
                                                 expected=file_name_version,
                                                 actual=version))
                self.update_failed_iterations(success=False)

            if self.fail_count >= self.max_fail_count:
                break

        self.validate_iterations()

    def DirectedFabCompatibilityTest(self):
        """Verify FPGA Fab version

        FPGA Compatibility Register:
        Bits    | Description
        -----------------------
        31:4    | Reserved
        3:0     | Fab D, C, B, A: 3, 2, 1, 0

        Test steps:
        1) Read Fab field from FPGA Compatibility BAR register
        2) Extract Fab version from the image file name found in FVAL
           configs.py <major.minor.fab>
        3) Compare the two reads from step 1 and 2
        3) Do not repeat test
        """

        for iteration in range(self.test_iterations):
            fpga_fab = self.rc.compatibility()
            config_fab = self.rc.config_fab()
            if fpga_fab != config_fab:
                self.Log('error', self.error_msg(iteration,
                                                 expected=config_fab,
                                                 actual=fpga_fab))
                self.update_failed_iterations(False)

            if self.fail_count >= self.max_fail_count:
                break

        self.validate_iterations()

    def get_version_number_from_config_file_name(self):
        config_file_name = self.rc.fpga_image_path()
        if config_file_name.rfind('_v') == -1:
            self.log_error_invalid_config_file_name(config_file_name)
            return -1

        temp = config_file_name.rsplit('_v')
        version_substring = temp[len(temp) - 1]
        if version_substring.rfind('.') == -1:
            self.log_error_invalid_config_file_name(config_file_name)
            return -1

        temp = version_substring.rsplit('.')
        if len(temp) < 3:
            self.log_error_invalid_config_file_name(config_file_name)
            return -1

        try:
            major = int(temp[0]) << 16
            minor = int(temp[1]) & 0xFFFF
        except ValueError:
            self.log_error_invalid_config_file_name(config_file_name)
            return -1

        return major | minor

    def log_error_invalid_config_file_name(self, config_file_name):
        self.Log('error',
                 f'Invalid file name format \"{config_file_name}\". '
                 f'Expecting image name to contain '
                 f'\"_v<major>.<minor>.<hw compatibility>.rbf\"')

    def error_msg(self, iteration, expected, actual):
        return f'Iteration_{iteration}: expected={expected}, actual={actual}'


class Chip(Rc3Test):
    """Verify chip related traceability (Chip ID)"""

    def setUp(self, tester=None):
        super().setUp(tester)
        self.test_iterations = TEST_ITERATIONS
        self.max_fail_count = MAX_FAIL_COUNT

    @format_docstring(num_iterations=TEST_ITERATIONS)
    def DirectedChipIdTest(self):
        """Determine if Chip identification is a valid number

        Chip ID value:
        Bits    | Register
        ------------------
        63:32   | Chip ID Upper (31:0)
        31:0    | Chip ID Lower (31:0)

        Test steps:
        1) Read Chip ID Upper and Upper registers
        2) Concatenate both values from step one and verify value is not 0 or
           0xFFFF_FFFF_FFFF_FFFF
        3) Repeat test {num_iterations} times
        """

        for iteration in range(self.test_iterations):
            success = True
            upper, lower = self.rc.chip_id()
            chip_id = upper << 32 | lower
            if chip_id == 0:
                self.Log('error', f'Iteration_{iteration}: '
                                  f'Invalid Chip ID value of 0x0')
                success = False

            if upper == 0xFFFFFFFF:
                self.Log('error', f'Iteration_{iteration}: '
                                  f'Invalid Chip ID Upper of 0xFFFFFFFF')
                success = False

            if lower == 0xFFFFFFFF:
                self.Log('error', f'Iteration_{iteration}: '
                                  f'Invalid Chip ID Lower of 0xFFFFFFFF')
                success = False

            self.update_failed_iterations(success)
            if self.fail_count >= self.max_fail_count:
                break

        self.validate_iterations()
