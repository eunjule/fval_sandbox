# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.


"""RCTC3 has a Trigger Sync Interface

FPGA can generate synchronized pulses to trigger link partners 0 to 12. The
pulse is generated on common clock MS_CLK. The pulse is sent on a configured
number of MS_CLK pulses.
"""

from time import sleep

from Common.instruments.hdmt_trigger_interface import HdmtTriggerInterface
from Hpcc.instrument.hpccAcRegs import PatternControl
from Rc3.Tests.Rc3Test import Rc3Test


class Functional(Rc3Test):
    """Test operation of the interface"""

    def setUp(self, tester=None):
        super().setUp(tester)
        self.test_iterations = 10
        self.max_fail_count = 1
        self.hpccs = self.get_hpccs()

    def get_hpccs(self):
        # HdmtTriggerInterface will gracefully initialize the instrument if it
        # can. It will disregard it if it can't without causing the test to fail
        self.rc.update_aurora_link_slots()
        interfaces = HdmtTriggerInterface.get_interfaces()
        hpccs = []
        for slot, interface in interfaces.items():
            instrument = self.tester.get_hdmt_instrument(slot)[0]
            if 'hpcc' in instrument.name().lower():
                hpccs.append(instrument)
        return hpccs

    def DirectedTriggerSyncPulseTest(self):
        """Send sync pulses to trigger link partners

        1) Send a sync pulse to one partner at a time and verify receipt at the
           partner
        2) Send a sync pulse to a randomized number of partners in one transac-
           tion and verify receipt at the partners
        3) Send a sync pulses to the remaining number of partners from step 3
           and verify receipt
        4) Repeat above steps a total of 10 times
        """

        self.fail_if_no_hpcc()

        for iteration in range(self.test_iterations):
            success = True

            for hpcc in self.hpccs:
                for hpcc_ac in hpcc.ac:
                    success &= self.run_trigger_sync_scenario(hpcc_ac,
                                                              iteration)

            self.update_failed_iterations(success)
            if self.fail_count >= self.max_fail_count:
                break
        self.validate_iterations()

    def DirectedTriggerSyncModulusTest(self):
        """Adjust Trigger Sync Modulus while sending sync pulses

        1) Set Modulus register to 0
        2) Send sync pulse to multiple partners
        3) Verify receipt at partner and reset modulus
        4) Repeat steps 1 to 3 using a Modulus of 15
        5) Repeat steps 1 to 3 using max value Modulus of 31
        6) Repeat above steps a total of 10 times
        """
        max_modulus = 2**5 - 1
        modulus_list = [0, max_modulus // 2, max_modulus]
        initial_modulus = self.rc.trigger_sync_modulus()

        self.fail_if_no_hpcc()

        for iteration in range(self.test_iterations):
            success = True

            for hpcc in self.hpccs:
                hpcc_ac = hpcc.ac[0]

                for modulus in modulus_list:
                    self.rc.update_trigger_sync_modulus(modulus)
                    if not self.run_trigger_sync_scenario(hpcc_ac, iteration):
                        success = False
                        self.Log(
                            'error',
                            f'Iteration {iteration}) {hpcc_ac.name()}: '
                            f'Failed using modulus {modulus}')

            self.update_failed_iterations(success)
            if self.fail_count >= self.max_fail_count:
                break

        self.rc.update_trigger_sync_modulus(initial_modulus)
        self.validate_iterations()

    def fail_if_no_hpcc(self):
        if len(self.hpccs) == 0:
            self.Log('error', f'No HPCC available')
            return

    def run_trigger_sync_scenario(self, hpcc_ac, iteration):
        success = self.verify_pattern_status(hpcc_ac, iteration, True)

        self.setup_hpcc_pattern(hpcc_ac)

        success &= self.verify_pattern_status(hpcc_ac, iteration, False)
        sleep(1)
        success &= self.verify_pattern_status(hpcc_ac, iteration, False)

        self.send_trigger_sync(hpcc_ac.slot)

        if not self.wait_on_pattern_complete(hpcc_ac):
            success = False
            self.Log('error',
                     self.error_msg_wait_on_pattern_complete(iteration,
                                                             hpcc_ac.name()))
        return success

    def error_msg_wait_on_pattern_complete(self, iteration, name):
        return f'Iteration {iteration}) {name}: Timed out waiting on pattern '\
               f'to complete after sending trigger sync pulse'

    def setup_hpcc_pattern(self, hpcc_ac):
        data = hpcc_ac.read_register(PatternControl)
        data.PreStagePattern = 0
        data.StartOnNextRCSyncPulse = 1
        data.EnableDisableOutputs = 0
        data.AbortPattern = 0
        data.SyncStartBypass = 0
        hpcc_ac.write_register(data)

        hpcc_ac.PrestagePattern()

    def wait_on_pattern_complete(self, hpcc_ac, num_retries=10):
        for retry in range(num_retries):
            sleep(0.0001)
            if self.is_pattern_complete(hpcc_ac):
                return True
        else:
            return False

    def is_pattern_complete(self, hpcc_ac):
        return hpcc_ac.IsPatternComplete()

    def send_trigger_sync(self, slot):
        self.tester.rc.send_sync_pulse(slot)

    def verify_pattern_status(self, hpcc_ac, iteration, expected):
        actual = self.is_pattern_complete(hpcc_ac)
        if actual != expected:
            self.Log('error',
                     f'Iteration {iteration}) {hpcc_ac.name()}: Pattern '
                     f'complete status (expected, actual): '
                     f'{expected}, {actual}')
            return False
        else:
            return True

    def DirectedTimeStampResetTest(self):
        """Reset the Live Stamp Timer

        1) Reset Live Stamp Timer
        2) Wait for Live Stamp Timer to be greater than 1 second
        3) Send a reset via the Trigger Sync register
        4) Verify the Live Stamp Timer register has a value < 100 us
        5) Repeat above steps a total of 1000 times
        """
        initial_timer_value_us = 1000
        timer_value_after_reset_us = 200

        for iteration in range(self.test_iterations):
            success = True

            self.rc.reset_live_time_stamp_timer()

            if not self._wait_on_timer_value(initial_timer_value_us):
                success = False
                self.Log('error',
                         self.error_msg_time_stamp_increment(iteration))

            self.rc.reset_live_time_stamp_timer()
            time_stamp = self.rc.live_time_stamp()
            if not time_stamp < timer_value_after_reset_us:
                success = False
                self.Log('error',
                         self.error_msg_time_stamp_clear(iteration,
                                                         time_stamp))

            self.update_failed_iterations(success)
            if self.fail_count >= self.max_fail_count:
                break

        self.validate_iterations()

    def error_msg_time_stamp_increment(self, iteration):
        return f'Iteration {iteration}: Time stamp value is not incrementing'

    def error_msg_time_stamp_clear(self, iteration, time_stamp):
        return f'Iteration {iteration}: Time stamp did not clear. {time_stamp}'

    def _wait_on_timer_value(self, value, num_retries=1000):
        for retry in range(num_retries):
            time_stamp = self.rc.live_time_stamp()
            if time_stamp >= value:
                return True
        else:
            return False

    def DirectedTriggerSyncModuluslWriteReadTest(self):
        """Verify write/read capability of the Trigger Sync Modulus register

        1) Read register and verify Power On value of 0
        2) Write all possible values (2^5) and read it back
        3) Reset register back to Power On value of 0
        """
        power_on_value = 0
        actual_value = self.rc.trigger_sync_modulus()
        if actual_value != power_on_value:
            self.Log('error',
                     self.error_msg_modulus_wr_power_on(power_on_value,
                                                        actual_value))
            self.update_failed_iterations(success=False)
        else:
            max_modulus_value = 2**5
            self.test_iterations = max_modulus_value
            for modulus in range(self.test_iterations):
                actual_value = self.read_updated_trigger_sync_modulus(modulus)
                if actual_value != modulus:
                    self.Log('error',
                             self.error_msg_modulus_wr(modulus, actual_value))
                    self.update_failed_iterations(success=False)
                if self.fail_count >= self.max_fail_count:
                    break

            self.rc.update_trigger_sync_modulus(power_on_value)
        self.validate_iterations()

    def read_updated_trigger_sync_modulus(self, update_value):
        self.rc.update_trigger_sync_modulus(update_value)
        return self.rc.trigger_sync_modulus()

    def error_msg_modulus_wr_power_on(self, expected, actual):
        self.Log('error', f'Trigger Sync Modulus default value '
                          f'(expected, actual): {expected}, {actual}')

    def error_msg_modulus_wr(self, expected, actual):
        return f'Trigger Sync Modulus write-read (expected, actual): ' \
               f'{expected}, {actual}'

    def DirectedTiuClockDividerlWriteReadTest(self):
        """Verify write/read capability of the TIU Clock Divider register

        1) Read register and verify initial value of 4
        2) Write all possible values (2^6) and read it back
        3) Reset register back to Power On value of 0
        """
        initial_value = 4
        actual_value = self.rc.tiu_clock_divider()
        if actual_value != initial_value:
            self.Log('error',
                      self.error_msg_tiu_clock_divider_wr_initial(
                          initial_value, actual_value))
            self.update_failed_iterations(success=False)
        else:
            max_divider_value = 2**6
            self.test_iterations = max_divider_value
            for divider in range(self.test_iterations):
                actual_value = self.read_updated_tiu_clock_divider(divider)
                if actual_value != divider:
                    self.Log('error',
                             self.error_msg_tiu_clock_divider_wr(divider,
                                                                 actual_value))
                    self.update_failed_iterations(success=False)
                    if self.fail_count >= self.max_fail_count:
                        break

            self.rc.update_tiu_clock_divider(initial_value)
        self.validate_iterations()

    def read_updated_tiu_clock_divider(self, update_value):
        self.rc.update_tiu_clock_divider(update_value)
        return self.rc.tiu_clock_divider()

    def error_msg_tiu_clock_divider_wr_initial(self, expected, actual):
        return f'TIU Clock Divider default value (expected, actual): ' \
               f'{expected}, {actual}'

    def error_msg_tiu_clock_divider_wr(self, expected, actual):
        return f'TIU Clock Divider write-read (expected, actual): ' \
               f'{expected}, {actual}'
