# INTEL CONFIDENTIAL

# Copyright 2019-2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

import os

from Common.fval import TestCase
from Common.instruments import tester as t
from Common.instruments.hdmt_trigger_interface import run_up_triggger_mini_test
from Rc3.testbench.env import Rc3Env


def get_pass_msg(method_name, test_iterations):
    """Returns a message of to be used for tests that pass
        - method_name: name of the test method
        - test_iterations: total run of repeated tests for method_name"""
    return f'{method_name} was successful for iterations: {test_iterations}'


def get_fail_msg(method_name, fail_count, test_iterations):
    """Returns a message of to be used for tests that fail
        - method_name: name of the test method
        - fail_count: number of failed test runs for method_name
        - test_iterations: total run of repeated tests for method_name"""
    return f'{method_name} was unsuccessful for {fail_count} out of ' \
           f'{test_iterations} iterations'


class Rc3Test(TestCase):
    dutName = 'rc3'

    def __init__(self, methodName='runTest'):
        super().__init__(methodName)
        self.test_iterations = 1000
        self.max_fail_count = 10

    def setUp(self, tester=None):
        self.LogStart()
        if tester is None:
            self.tester = t.get_tester()
        else:
            self.tester = tester
        self.env = Rc3Env(self, tester=self.tester)
        self.rc = self.env.rc
        self.fail_count = 0

    def tearDown(self):
        num_triggers = os.getenv('DBG_RC3_TEST_UP_TRIGGER_MINI_TESTS')
        if num_triggers is not None:
            try:
                run_up_triggger_mini_test(int(num_triggers))
            except ValueError as e:
                self.Log('warning', f'Unable to run mini test. {e}')
        self.LogEnd()

    def validate_iterations(self):
        if self.fail_count == 0:
            self.Log('info', get_pass_msg(self.test_name(),
                                          self.test_iterations))
        else:
            self.Log('error', get_fail_msg(self.test_name(), self.fail_count,
                                           self.test_iterations))

    def update_failed_iterations(self, success):
        if not success:
            self.fail_count += 1

    def update_test_name(self, name):
        self._testMethodName = name
