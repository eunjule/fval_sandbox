# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

"""RCTC3 processes Serial Capture Stream (SCS) triggers from up to 14 trigger

The RCTC3 FPGA accepts UpTriggers from up to 14 trigger link partners. If the
trigger is of SCS type (containing sensor information from a DUT), it uses a
Data Stream and a Scopeshot engine to store the sensor information. The sensor
information stored by the FPGA consists of:
    Name..................| Bit size
    --------------------------------
    Slot ID/Trigger Link #| 4
    HPCC AC FPGA Index....| 2
    HPCC AC FPGA Pin ID...| 2
    User ID...............| 10
    SCS Data..............| 10

"""
from math import isclose

from Common.instruments.hdmt_trigger_interface import HDMT_NUM_SLOTS
from Common.register import create_field_dictionary
from Common.triggers import SCSMemoryEncoding
from Common.utilities import relative_tolerance
from Rc3.instrument.scs_interface import ScsScopeshotInterface
from Rc3.Tests.Rc3Test import Rc3Test


# class DataStreamEngineDiagnostics(Rc3Test):
#     """Test communication robustness
#
#     Data Stream engine:
#     - Take 32-bit formatted SCS trigger information and store in FPGA RAM
#         - RAM is 32-bit wide and 2^18 bit deep
#         - There are 14 RAMs, one for each trigger link partner
#     - RAM is accessible via HPS Serial Capture Stream RAM registers.
#     """
#
#     def setUp(self, tester=None):
#         super().setUp(tester)
#         self.test_iterations = TEST_ITERATIONS
#         self.max_fail_count = MAX_FAIL_COUNT
#
#     @skip('Validation to be performed at the TOS level via the HPS')
#     def DirectedMailboxWriteReadExhaustiveTest(self):
#         """Write random data to 8KB Mailbox (MB) and read it back
#
#         Data Stream uses the top 2KB of the MB to store sensor data before it
#         is periodically (4ms) sent to the SiteC memory. This data is retrieved
#         from Data Stream RAM, formatted by TC FW, and then placed into MB.
#
#         1) Write 8KB of random data to the MB
#         2) Read 8KB of data from the MB and compare to data written
#         3) Repeat above steps a total of 100 times
#         """
#
#     @skip('Validation to be performed at the TOS level via the HPS')
#     def DirectedDataStreamRamWriteReadExhaustiveTest(self):
#         """Write random data to Data Stream (DS) RAM and read it back
#
#         1) Create a 2^18 array of randomized 10-bit SCS data
#         2) Send the SCS data via an available HPCC
#         3) Verify receipt at the corresponding RCTC3 UpTrigger register
#         4) Continue steps 2 and 3 until entire data array has been sent
#         5) Perform read of the DS RAM using Serial Capture Stream RAM interface
#            and compare all UpTrigger data has been captured
#         6) Verify RAM Read Address increments by 4
#         7) Repeat steps 1 to 6 for all available HPCC
#         8) Repeat all steps a total of 100 times
#         """


class ScopeshotEngineDiagnostics(Rc3Test):
    """Test communication robustness

    Scopeshot engine:
    - Takes SCS trigger information and adds a 64-bit entry into external DDR4
        - Lower 32-bits consists of formatted SCS trigger information
        - Upper 32-bits consists of a microsecond timestamp
        - First 1.5 GB of the 2GB DDR4 is treated as a circular buffer and is
          reserved for Scopeshot
          - 1.5 GB equals (2^30*1.5) 1,610,612,736 B
          - Byte addressing from 0 to 0x5FFF_FFFF
          - 64-bit, 8 byte address from 0 to 0x5FFF_FFF8
          - Scopeshot memory region can hold a total of 1.5GB / 8 = 201,326,592
            entries
    - Scopeshot entries can be read using PCIe DMA reads
    """

    def setUp(self, tester=None):
        super().setUp(tester)
        self.scs = ScsScopeshotInterface()

    def DirectedCircularBufferWriteReadExhaustiveTest(self):
        """ Validate circular buffer aspect of the scopeshot DDR

        Send a fixed amount of triggers to the DDR within an address range
        that will allow the data to wrap around the range. Adjust the starting
        and ending address registers to test the limits of the 1.5GB of the DDR.

        The number of triggers used will be 16.
        A test will consist of sending 16 triggers with the following address
        combinations:
        starting_address    ending_address
        ----------------------------------
        0                   0
        0                   8
        0                   0x20
        0x3000_0000         0x3000_0020
        0x5FFF_FFE0         0x6000_0000

        Scopeshot interface features tested:
        - Starting and ending address: Setting ranges of 0 to 0x20 address
        ranges to test circular buffer and verify if data insertion wraps from
        end to start address. At start of each test, the default values of the
        addresses are also verified.
        - Latest memory address; verifies that the latest address bar register
        properly reads the expected address as a trigger is inserted into
        memory
        - Sensor data and time stamp: When a trigger is sent, a reading ot the
        live time stamp and the sensor data is taken. This is then compared to
        the data read in memory and its expected to have an exact match of the
        data, and an absolute tolerance of the time stamp of 30 us
        """

        self.test_iterations = 2
        self.max_fail_count = 1
        num_triggers = 16
        num_buffer_wrap_cycles = 4
        starting_addresses = [
            0,
            int(self.scs.DDR4_NUM_BYTES / 2),
            self.scs.DDR4_NUM_BYTES -
            (self.scs.DDR4_ENTRY_BYTE_SIZE * num_buffer_wrap_cycles)]

        for iteration in range(self.test_iterations):
            success = True

            for starting_address in starting_addresses:
                ending_address = int(starting_address +
                                     (self.scs.DDR4_ENTRY_BYTE_SIZE *
                                      (num_triggers / num_buffer_wrap_cycles)))

                with self.subTest(ITERATION=f'{iteration:2d}',
                                  ADDRESS_RANGE=f'0x{starting_address:08X} to '
                                                f'0x{ending_address:08X}',
                                  NUM_TRIGGERS=num_triggers):
                    success &= self.validate_address_defaults(iteration)
                    success &= self.validate_ddr4_at_each_trigger_sent(
                        iteration=iteration,
                        start_addr=starting_address,
                        end_addr=ending_address,
                        num_triggers=num_triggers
                    )

            for address_range in [[0,0], [0, 8]]:
                starting_address = address_range[0]
                ending_address = address_range[1]

                with self.subTest(ITERATION=f'{iteration:2d}',
                                  ADDRESS_RANGE=f'0x{starting_address:08X} to '
                                                f'0x{ending_address:08X}',
                                  NUM_TRIGGERS=num_triggers):
                    success &= self.validate_address_defaults(iteration)
                    success &= self.validate_ddr4_at_each_trigger_sent(
                        iteration=iteration,
                        start_addr=starting_address,
                        end_addr=ending_address,
                        num_triggers=num_triggers
                    )

            self.update_failed_iterations(success)
            if self.fail_count >= self.max_fail_count:
                break

        self.validate_iterations()

    def validate_address_defaults(self, iteration):
        success = True

        starting_address = self.scs.ddr4_starting_address()
        expected = self.scs.DDR4_STARTING_ADDRESS_DEFAULT
        if  starting_address != expected:
            success = False
            self.Log('error', self.error_msg_at_default_address(
                iteration, expected, starting_address=starting_address))

        ending_address = self.scs.ddr4_ending_address()
        expected = self.scs.DDR4_ENDING_ADDRESS_DEFAULT
        if ending_address != expected:
            success = False
            self.Log('error', self.error_msg_at_default_address(
                iteration, expected, ending_address=ending_address))

        return success

    def validate_ddr4_at_each_trigger_sent(self, iteration, start_addr,
                                           end_addr, num_triggers):
        scs_ddr4_data = self.scs.generate_random_scs_ddr4_data(num_triggers)

        self.scs.set_ddr4_starting_address(start_addr)
        self.scs.set_ddr4_ending_address(end_addr)
        address = self.scs.ddr4_starting_address()
        fail_count = 0

        try:
            self.scs.initiate_scopeshot(use_event_trigger=False)

            for data in scs_ddr4_data:
                success = True
                scs_trigger = self.scs.convert_ddr4_data_to_trigger(data)
                expected_stamp_before = self.rc.live_time_stamp()
                self.rc.send_up_trigger_bypass(data.link_num, scs_trigger)
                expected_stamp_after = self.rc.live_time_stamp()

                actual_data, actual_stamp = \
                    self.scs.read_scopeshot_entries_from_memory(address,
                                                                num_entries=1)
                latest = self.scs.ddr4_latest_address()

                if address != latest:
                    success = False
                    self.Log('error', self.error_msg_latest_address(
                        iteration, address, latest))

                if not self.scs.compare_sensor_data(data, actual_data):
                    success = False
                    self.Log('error', self.error_msg_sensor_data(
                        iteration, address, data, actual_data))

                success &= self.is_time_stamp_within_range(
                    address, expected_stamp_before,
                    expected_stamp_after, actual_stamp, iteration=iteration)

                address = self.scs.ddr4_next_address(address)

                if not success:
                    fail_count += 1
                if fail_count >= self.max_fail_count:
                    break
        finally:
            self.scs.terminate_scopeshot(use_event_trigger=False)

        return (fail_count == 0)

    def DirectedScopeshotTriggerDataFlowByLinkTest(self):
        """Test SCS trigger scopeshot path into the RCTC3 and out to its DDR

        The test consists of sending a considerable number, approximately 196K,
        of SCS triggers with the same link number and verify its data and
        time stamp at the DDR4. The triggers are to be sent all at once and
        read from memory in bulk once done. The test is repeated once for each
        link number, HDMT slots 0 to 11. Starting address is set to default of
        zero and data is to be stores incrementally throughout.
        """

        try:
            self.scs.initiate_scopeshot(use_event_trigger=False)

            self.validate_address_defaults(iteration=0)

            num_entries = self.number_of_triggers()

            address = self.scs.ddr4_starting_address()
            for link_num in range(HDMT_NUM_SLOTS):
                scs_ddr4_data = self.scs.generate_random_scs_ddr4_data(
                    num_entries, link_num)

                expected_address = \
                    int(address + (self.scs.DDR4_ENTRY_BYTE_SIZE *
                                   num_entries))
                expected_address -= self.scs.DDR4_ENTRY_BYTE_SIZE

                with self.subTest(LINK_NUM=f'{link_num:2d}',
                                  ADDRESS_RANGE=f'0x{address:08X} to '
                                                f'0x{expected_address:08X}',
                                  NUM_TRIGGERS=num_entries):
                    expected_stamps = []
                    self.send_scs_triggers(scs_ddr4_data, expected_stamps)

                    actual_entries = \
                        self.scs.read_scopeshot_entries_from_memory(
                            address, num_entries)

                    actual_address = self.scs.ddr4_latest_address()
                    if actual_address != expected_address:
                        self.Log('error', self.error_msg_latest_address(
                            link_num, expected_address, actual_address))

                    self.validate_data_flow(address,
                                            expected_stamps, scs_ddr4_data,
                                            actual_entries)

                address = self.scs.ddr4_latest_address() + \
                          self.scs.DDR4_ENTRY_BYTE_SIZE

        finally:
            self.scs.terminate_scopeshot(use_event_trigger=False)

    def number_of_triggers(self):
        from Common import hilmon, hdmt_simulator
        if isinstance(hilmon.hil, hdmt_simulator.HdmtSimulator):
            return 10
        else:
            # 1.5 GB of memory can store 201,325,592 scs entries (1.5GB / 8)
            # 1/4 of total entries is 50,331,648
            # 1/16 is 12,582,912
            # 1/1024 is 196,608
            # 1/4096 is 49,151
            return self.scs.MAX_NUM_DATA_ENTRIES // 4096

    def send_scs_triggers(self, scs_ddr4_data, expected_stamps):
        for data in scs_ddr4_data:
            scs_trigger = \
                self.scs.convert_ddr4_data_to_trigger(data)
            before = self.rc.live_time_stamp()
            self.rc.send_up_trigger_bypass(data.link_num,
                                           scs_trigger)
            after = self.rc.live_time_stamp()
            expected_stamps.append((before, after))

    def validate_data_flow(self, start_address, expected_stamps,
                           expected_data, actual_entries):
        num_entries = len(expected_data)
        entry_fail_count = 0
        entry_max_fail_count = 3
        address = start_address
        for i in range(num_entries):
            link_num = expected_data[i].link_num
            expected_stamp_before, expected_stamp_after = expected_stamps[i]
            stamp_from_ddr4 = actual_entries[i][1]

            entry_success = self.is_time_stamp_within_range(
                address, expected_stamp_before, expected_stamp_after,
                stamp_from_ddr4, link_num=link_num)

            data_from_ddr4 = actual_entries[i][0]
            if not self.scs.compare_sensor_data(expected_data[i],
                                                data_from_ddr4):
                entry_success = False
                self.Log('error', self.error_msg_sensor_data(
                    link_num, address, expected_data[i], data_from_ddr4))

            address = self.scs.ddr4_next_address(address)

            if not entry_success:
                entry_fail_count += 1
            if entry_fail_count >= entry_max_fail_count:
                break

    def is_time_stamp_within_range(self, address,
                                min_expected_stamp, max_expected_stamp,
                                actual_stamp, link_num=None, iteration=None):
        if min_expected_stamp > actual_stamp or \
                actual_stamp > max_expected_stamp:
            self.Log('error', self.error_msg_time_stamp(
                iteration,
                link_num,
                address,
                min_expected_stamp,
                max_expected_stamp,
                actual_stamp))
            return False
        else:
            return True

    def error_msg_time_stamp(self, iteration, link_num, address, min_stamp,
                             max_stamp, actual_stamp):
        id_msg = f'Iteration {iteration})' if link_num is None \
            else f'Link {link_num})'
        return f'{id_msg} Invalid time stamp of {actual_stamp}us recorded at ' \
               f'memory address {hex(address)}. Range expected is ' \
               f'{min_stamp}us to {max_stamp}us.'

    def error_msg_latest_address(self, iteration, expected, actual):
        return f'Iteration {iteration}) Invalid latest address ' \
               f'(expected, actual): 0x{expected:08X}, 0x{actual:08X}'

    def error_msg_at_default_address(self, iteration, expected, **kwargs):
        name, value = list(kwargs.items())[0]
        return f'Iteration {iteration}) Invalid default {name} ' \
               f'(expected, actual): 0x{expected:08X}, 0x{value:08X}'

    def error_msg_sensor_data(self, iteration, address, sent, received):
        return f'Iteration {iteration}, Address: 0x{address:08X}) Sensor data ' \
               f'mismatch (sent, received):' \
               f'\n\tsent - {create_field_dictionary(sent)}' \
               f'\n\trcvd - {create_field_dictionary(received)}'


class ScopeshotEngineFunctional(Rc3Test):
    """Test engine operation"""

    def setUp(self, tester=None):
        super().setUp(tester)
        self.test_iterations = 10
        self.max_fail_count = 2
        self.scs = ScsScopeshotInterface()

    def DirectedScopeshotStartStopUsingEventTriggerTest(self):
        """Test SCS Scopeshot Start and Stop Trigger Event reception

        Using a fixed number of triggers, initialize corresponding DDR4 entries
        to all zero. To the first half of the DDR4, send a random number of
        SCS triggers sandwhiched by a start and stop event trigger. To the next
        half of the DDR4 do the same except do not send start and stop triggers
        before and after. Validate the contents of the DDR4 to match the first
        half of the triggers sent, but the second half to have zero value
        entries.
        """
        self.num_triggers = 10

        try:
            self.scs.initiate_scopeshot()

            for iteration in range(self.test_iterations):
                expected_scs_memory = self.scs.generate_random_scs_ddr4_data(
                    self.num_triggers, link_num=0)
                scs_triggers = [
                    self.scs.convert_ddr4_data_to_trigger(expected_scs_memory[i])
                    for i in range(self.num_triggers)
                ]

                success = self.validate_address_defaults(iteration)
                success &= self.run_start_stop_test(iteration, scs_triggers,
                                                    expected_scs_memory)

                self.update_failed_iterations(success)
                if self.fail_count >= self.max_fail_count:
                    break

        finally:
            self.scs.terminate_scopeshot()

        self.validate_iterations()

    def DirectedScopeshotStartStopUsingBarRegisterTest(self):
        """Test SCS Scopeshot Start and Stop Bar register

        Using a fixed number of triggers, initialize corresponding DDR4 entries
        to all zero. To the first half of the DDR4, send a random number of
        SCS triggers sandwhiched by a start and stop control assertion. To the
        next half of the DDR4 do the same except do not assert start and stop
        control before and after. Validate the contents of the DDR4 to match
        the first half of the triggers sent, but the second half to have zero
        value entries.
        """
        self.num_triggers = 10

        try:
            self.scs.initiate_scopeshot(use_event_trigger=False)

            for iteration in range(self.test_iterations):
                expected_scs_memory = self.scs.generate_random_scs_ddr4_data(
                    self.num_triggers, link_num=0)
                scs_triggers = [
                    self.scs.convert_ddr4_data_to_trigger(expected_scs_memory[i])
                    for i in range(self.num_triggers)
                ]

                success = self.validate_address_defaults(iteration)
                success &= self.run_start_stop_test(iteration, scs_triggers,
                                                    expected_scs_memory,
                                                    use_event_triggers=False)

                self.update_failed_iterations(success)
                if self.fail_count >= self.max_fail_count:
                    break

        finally:
            self.scs.terminate_scopeshot(use_event_trigger=False)

        self.validate_iterations()

    def run_start_stop_test(self, iteration, scs_triggers, expected_scs_memory,
                            use_event_triggers=True):
        success = True

        self.num_triggers = len(expected_scs_memory)
        starting_address = self.scs.ddr4_starting_address()

        self.clear_ddr4_entries(self.num_triggers)

        self.scs.start_scopeshot(use_event_trigger=use_event_triggers)
        for i in range(self.num_triggers // 2):
            trigger_link = expected_scs_memory[i].link_num
            self.rc.send_up_trigger_bypass(slot=trigger_link,
                                           trigger=scs_triggers[i])
        self.scs.stop_scopeshot(use_event_trigger=use_event_triggers)

        for i in range(self.num_triggers // 2, self.num_triggers):
            trigger_link = expected_scs_memory[i].link_num
            self.rc.send_up_trigger_bypass(slot=trigger_link,
                                           trigger=scs_triggers[i])

        actual_entries =  self.get_start_stop_ddr4_entries()

        for i in range(self.num_triggers):
            address = starting_address + (i * self.scs.DDR4_ENTRY_BYTE_SIZE)
            actual_scs_memory = actual_entries[i][0]

            if i < (self.num_triggers // 2):
                if not self.scs.compare_sensor_data(expected_scs_memory[i],
                                                    actual_scs_memory):
                    success = False
                    self.Log('error', self.error_msg_sensor_data(
                        iteration=iteration,
                        address=address,
                        index=i,
                        sent=expected_scs_memory[i],
                        received=actual_scs_memory))
            else:
                if not self.scs.compare_sensor_data(SCSMemoryEncoding(value=0),
                                                    actual_scs_memory):
                    success = False
                    self.Log('error', self.error_msg_sensor_data(
                        iteration=iteration,
                        address=address,
                        index=i,
                        sent=SCSMemoryEncoding(value=0),
                        received=actual_scs_memory))

        return success

    def get_start_stop_ddr4_entries(self):
        from Common import hdmt_simulator, hilmon
        address = self.scs.ddr4_starting_address()
        if isinstance(hilmon.hil, hdmt_simulator.HdmtSimulator):
            entries = []
            for i in range(self.num_triggers):
                entry = self.scs.read_scopeshot_entries_from_memory(address)
                entries.append(entry)
                address = self.scs.ddr4_next_address(address)
        else:
            entries = self.scs.read_scopeshot_entries_from_memory(
                address, self.num_triggers)
        return entries

    def validate_address_defaults(self, iteration):
        success = True

        starting_address = self.scs.ddr4_starting_address()
        expected = self.scs.DDR4_STARTING_ADDRESS_DEFAULT
        if  starting_address != expected:
            success = False
            self.Log('error', self.error_msg_at_default_address(
                iteration, expected, starting_address=starting_address))

        ending_address = self.scs.ddr4_ending_address()
        expected = self.scs.DDR4_ENDING_ADDRESS_DEFAULT
        if ending_address != expected:
            success = False
            self.Log('error', self.error_msg_at_default_address(
                iteration, expected, ending_address=ending_address))

        return success

    def error_msg_at_default_address(self, iteration, expected, **kwargs):
        name, value = list(kwargs.items())[0]
        return f'Iteration {iteration}) Invalid default {name} ' \
               f'(expected, actual): 0x{expected:08X}, 0x{value:08X}'

    def DirectedScopeshotEnableAndActiveTest(self):
        """ Test Scopeshot Enable control and Active status features

        Will test ability of enabling/disabling Scopeshot using hte following
        two approaches:

        1. Test title: Sending SCS triggers while Scopeshot disabled.

        Using a fixed number of triggers, initialize corresponding DDR4
        entries to all zero. Enable Scopeshot and send a start event trigger.
        To the first half of the allocated DDR4 space, send a random SCS
        trigger, verify the trigger sent matches the entry stored in
        DDR4, and verify the Scopeshot Active status is asserted. To the second
        half of the allocated DDR4 space, disable Scopeshot first. Then send a
        random SCS triggers one at a time, while verifying the corresponding
        DDR4 entry contains zeroed out data, and the Scopeshot Active status is
        de-asserted.

        2. Test title: Asserting Start\Stop control while Scopeshot disabled.

        Send a fixed number of random SCS trigger to DDR4. A trigger will be
        sent an iteration at a time, and the DDR4 entry at starting address
        will be zeroed out by writing zeros to its contents. At every even
        iteration, enable Scopeshot, assert the Start control, send the
        trigger, and assert the Stop control. Verify the Active status was
        asserted and the trigger matches the DDR4 entry at starting address. At
        every odd iteration, disable Scopeshot, assert the Start control, send
        the trigger, and assert the Stop control. Verify the Active status was
        de-asserted and DDR4 entry at starting address has zeroed out data.
        """

        try:
            self.scs.initiate_scopeshot(use_event_trigger=False)

            with self.subTest(
                    DESCRIPTION=f'Sending SCS triggers while Scopeshot '
                                f'disabled'):
                self.run_sending_scs_triggers_while_disabled_test()

            with self.subTest(
                    DESCRIPTION=f'Asserting Start and Stop control while '
                                f'Scopeshot disabled'):
                self.run_sending_event_triggers_while_disabled_test()
        finally:
            self.scs.terminate_scopeshot(use_event_trigger=False)

    def run_sending_scs_triggers_while_disabled_test(self):
        self.fail_count = 0
        self.num_triggers = 16
        starting_address = self.scs.ddr4_starting_address()

        for iteration in range(self.test_iterations):
            success = True

            expected_data = self.scs.generate_random_scs_ddr4_data(
                self.num_triggers)

            self.clear_ddr4_entries(self.num_triggers)

            self.scs.initiate_scopeshot(use_event_trigger=False)

            for i in range(self.num_triggers):
                address = starting_address + \
                          (i * self.scs.DDR4_ENTRY_BYTE_SIZE)

                if i == self.num_triggers // 2:
                    self.scs.disable()

                scs_trigger = self.scs.convert_ddr4_data_to_trigger(
                    expected_data[i])
                link_num = expected_data[i].link_num
                self.rc.send_up_trigger_bypass(link_num, scs_trigger)
                actual_data, stamp = \
                    self.scs.read_scopeshot_entries_from_memory(address)

                if i < self.num_triggers // 2:
                    if not self.scs.is_active():
                        success = False
                        self.Log('error', self.error_msg_is_active(
                            iteration, i, True, False))
                    if not self.scs.compare_sensor_data(expected_data[i],
                                                        actual_data):
                        success = False
                        self.Log('error', self.error_msg_sensor_data(
                            iteration, address, i, expected_data[i],
                            actual_data))
                else:
                    zero_data = SCSMemoryEncoding(0)
                    if self.scs.is_active():
                        success = False
                        self.Log('error', self.error_msg_is_active(
                            iteration, i, False, True))
                    if not self.scs.compare_sensor_data(zero_data, actual_data):
                        success = False
                        self.Log('error', self.error_msg_sensor_data(
                            iteration, address, i, zero_data, actual_data))

            self.update_failed_iterations(success)
            if self.fail_count >= self.max_fail_count:
                break

        self.validate_iterations()

    def run_sending_event_triggers_while_disabled_test(self):
        self.fail_count = 0
        self.num_triggers = 16
        starting_address = self.scs.ddr4_starting_address()
        zero_bytes = bytes(self.scs.DDR4_ENTRY_BYTE_SIZE)

        for iteration in range(self.test_iterations):
            success = True

            expected_data = self.scs.generate_random_scs_ddr4_data(
                self.num_triggers)

            for i in range(self.num_triggers):
                address = starting_address + \
                          (i * self.scs.DDR4_ENTRY_BYTE_SIZE)
                is_even = (i % 2) == 0

                scs_trigger = self.scs.convert_ddr4_data_to_trigger(
                    expected_data[i])
                link_num = expected_data[i].link_num

                self.rc.dma_write(starting_address, zero_bytes)

                if is_even:
                    self.scs.enable()
                    self.scs.start_scopeshot(use_event_trigger=False)

                    if not self.scs.is_active():
                        success = False
                        self.Log('error', self.error_msg_is_active(
                            iteration, i, True, False))

                    self.rc.send_up_trigger_bypass(link_num, scs_trigger)
                    self.scs.stop_scopeshot(use_event_trigger=False)
                else:
                    self.scs.disable()
                    self.scs.start_scopeshot(use_event_trigger=False,
                                             force_wait=False)

                    if self.scs.is_active():
                        success = False
                        self.Log('error', self.error_msg_is_active(
                            iteration, i, False, True))

                    self.rc.send_up_trigger_bypass(link_num, scs_trigger)
                    self.scs.stop_scopeshot(use_event_trigger=False)

                actual_data, s = self.scs.read_scopeshot_entries_from_memory(
                    starting_address)
                if is_even:
                    if not self.scs.compare_sensor_data(expected_data[i],
                                                        actual_data):
                        success = False
                        self.Log('error', self.error_msg_sensor_data(
                            iteration, address, i, expected_data[i],
                            actual_data))
                else:
                    zero_data = SCSMemoryEncoding(0)
                    if not self.scs.compare_sensor_data(zero_data,
                                                        actual_data):
                        success = False
                        self.Log('error', self.error_msg_sensor_data(
                            iteration, address, i, zero_data, actual_data))

            self.update_failed_iterations(success)
            if self.fail_count >= self.max_fail_count:
                break

        self.validate_iterations()

    def error_msg_is_active(self, iteration, trigger_num, expected, actual):
        return f'Iteration {iteration}) Scopeshot active status invalid at ' \
               f'trigger number {trigger_num} out of {self.num_triggers} ' \
               f'(expected, actual): {expected}, {actual}'

    def DirectedSampleCountTest(self):
        """ Test the sample count feature of the Scopeshot interface

        The sample count determines how many microseconds to accept SCS
        triggers after a Stop command. The goal here is to calculate how many
        triggers are expected after stopping using different sample counts.
        It is estimated that it takes approximately 9us to transmit a trigger
        using the bar register bypass. Using this value, we can roughly
        estimate that for every 9 sample counts, we will have one trigger.
        In this test, we will send 10 valid triggers, meaning 10 triggers to be
        sent before a stop command. Then we will attempt to count the number
        of extra triggers after the stop command dependent on the applied
        sample count. By reading the entry data from DDR4, once we reach an
        entry with zeroed out data, we calculate how many extra triggers past
        the stop command were sent, and verify this against the sample count
        times 9.
        For example, if we send 10 valid triggers using a sample count of
        90 (10 expected triggers times 9use delay per trigger), we should cycle
        through the DDR4 and find approximately 20 matching triggers. The DDR4
        entry immediately after will be zeroed out providing a marker as to
        when to stop counting.
        """
        self.num_triggers = 10
        expected_trigger_counts = self.total_triggers_with_sample_count()
        delay_times_us = self.scs.SAMPLE_COUNT_DELAY_TIME_US

        for iteration in range(self.test_iterations):
            success = True

            for expected_trigger_count in expected_trigger_counts:
                extra_count = expected_trigger_count - self.num_triggers
                num_extra_entries = self.num_triggers + \
                                    (extra_count * delay_times_us[1]) + 1
                expected_data = self.scs.generate_random_scs_ddr4_data(
                    num_extra_entries)

                success &= self.setup_sample_count_test(
                    iteration, extra_count, num_extra_entries, delay_times_us)

                success &= self.run_sample_count_test(iteration, expected_data,
                                                      extra_count)

            self.update_failed_iterations(success)
            if self.fail_count >= self.max_fail_count:
                break

        self.validate_iterations()

    def total_triggers_with_sample_count(self):
        return [10, 11, 20, 30]

    def error_msg_sample_count(self, iteration, extra_count, expected, actual):
        return f'Iteration {iteration}) Invalid sample count ' \
               f'(extra_count, expected, actual): {extra_count}, {expected}, ' \
               f'{actual}'

    def error_msg_extra_count(self, iteration, expected, actual):
        return f'Iteration {iteration}) Invalid number of triggers after stop ' \
               f'command, during sample count(expected, actual): ' \
               f'{expected}, {actual}. Criteria must be expected <= actual'

    def warn_msg_extra_count(self, iteration, expected, actual):
        actual_tolerance = relative_tolerance(expected, actual)

        return f'Iteration {iteration}) Number of triggers during ' \
               f'sample count where below expected, but within a relative ' \
               f'tolerance of {self.scs.SAMPLE_COUNT_TOLERANCE*100:.01f}%. ' \
               f'(expected count, actual, count, actual tolerance): ' \
               f'{expected}, {actual}, {actual_tolerance*100:.01f}%'

    def run_sample_count_test(self, iteration, expected_data, extra_count):
        starting_address = self.scs.ddr4_starting_address()
        success = True
        scs_triggers = [self.scs.convert_ddr4_data_to_trigger(data)
                        for data in expected_data]
        try:
            self.scs.initiate_scopeshot(use_event_trigger=False)

            for i in range(self.num_triggers):
                self.rc.send_up_trigger_bypass(expected_data[i].link_num,
                                               scs_triggers[i])

            self.scs.stop_scopeshot(use_event_trigger=False, force_wait=False)

            for i in range(self.num_triggers, len(expected_data)):
                self.rc.send_up_trigger_bypass(expected_data[i].link_num,
                                               scs_triggers[i])

            entries = self.scs.read_scopeshot_entries_from_memory(
                starting_address, len(expected_data))

            entry_fail_count = 0
            for i in range(len(entries)):
                actual_data = entries[i][0]

                if self.scs.compare_sensor_data(SCSMemoryEncoding(0),
                                                actual_data):
                    actual_count = (i + 1) - self.num_triggers
                    if extra_count > actual_count:
                        if isclose(extra_count, actual_count,
                                   rel_tol=self.scs.SAMPLE_COUNT_TOLERANCE):
                            self.Log('warning', self.warn_msg_extra_count(
                                iteration, extra_count, actual_count))
                        else:
                            self.Log('error', self.error_msg_extra_count(
                                iteration, extra_count, actual_count))
                    break
                elif not self.scs.compare_sensor_data(expected_data[i],
                                                      actual_data):
                    success = False
                    entry_fail_count += 1
                    address = i * self.scs.DDR4_ENTRY_BYTE_SIZE
                    self.Log('error', self.error_msg_sensor_data(
                        iteration, address, i, expected_data[i], actual_data,
                        extra_count))

                if entry_fail_count >= self.max_fail_count:
                    break
        finally:
            self.scs.terminate_scopeshot(use_event_trigger=False)
            return success

    def setup_sample_count_test(self, iteration, extra_count,
                                num_extra_entries, delay_times_us):
        self.clear_ddr4_entries(num_extra_entries)

        if extra_count == 1:
            sample_count = extra_count * 15
        elif extra_count == 0:
            sample_count = 0
        else:
            sample_count = delay_times_us[0] + ((extra_count - 1) *
                                                delay_times_us[1])

        self.scs.set_sample_count(sample_count)
        actual_sample_count = self.scs.sample_count()
        if sample_count != actual_sample_count:
            self.Log('error', self.error_msg_sample_count(
                iteration, extra_count, sample_count, actual_sample_count))
            return False
        else:
            return True

    def clear_ddr4_entries(self, num_entries):
        from Common import hdmt_simulator, hilmon
        starting_address = self.scs.ddr4_starting_address()
        if isinstance(hilmon.hil, hdmt_simulator.HdmtSimulator):
            zero_bytes = bytes(self.scs.DDR4_ENTRY_BYTE_SIZE)
            address = starting_address
            for i in range(num_entries):
                self.rc.dma_write(address, zero_bytes)
                address = self.scs.ddr4_next_address(address)
        else:
            zero_bytes = bytes(self.scs.DDR4_ENTRY_BYTE_SIZE * num_entries)
            self.rc.dma_write(starting_address, zero_bytes)

    def error_msg_sensor_data(self, iteration, address, index, sent, received,
                              extra_count=None):
        num_triggers_msg = '' if extra_count is None else \
            f'Number of triggers expected (before and after stop): ' \
            f'{self.num_triggers}, {extra_count}'
        return f'------------------------------------------------------' \
               f'\nIteration {iteration}, Index {index})) Sensor data '\
               f'mismatch' \
               f'\n\tAddress: {hex(address)}' \
               f'\n\t{num_triggers_msg}' \
               f'\n\tExpected data - {create_field_dictionary(sent)}' \
               f'\n\t  Actual data - {create_field_dictionary(received)}'
