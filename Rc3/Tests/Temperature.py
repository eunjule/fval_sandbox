# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

"""FPGA Temperature Sensor interface

Perform reads of each of the nine internal temperature sensor on the FPGA
- CH0: Core fabric
- CH1: Transceiver Tile Bank 1C, 1D, 1E, 1F or 8A. Though available, according to FPGA developer
the temperature reading for non-zero channels can not be relied upon.

Channels 2 to 8 are not supported in current FPGA package
- CH2: Transceiver Tile Bank 1G, 1H, 1I, 1J or 8B
- CH3: Transceiver Tile Bank 1K, 1L, 1M, 1N or 8C
- CH4: Transceiver Tile Bank 4C, 4D, 4E, 4F or 9A
- CH5: Transceiver Tile Bank 4G, 4H, 4I, 4J or 9B
- CH6: Transceiver Tile Bank 4K, 4L, 4M, 4N or 9C
- CH7: HBM2 Top
- CH8: HBM2 Bottom
"""

from enum import Enum
from math import isclose

from Common.utilities import format_docstring
from Rc3.Tests.Rc3Test import Rc3Test


TEST_ITERATIONS = 1000
MAX_FAIL_COUNT = 10
TEMPERATURE_RANGE = [10, 80]
DELTA = 1


class Channels(Enum):
    CH0_CORE = 0
    CH1_TRANSCEIVER = 1


class Diagnostics(Rc3Test):
    """Test communication robustness

    Read all available FPGA sensors
    """

    def setUp(self, tester=None):
        super().setUp(tester)
        self.test_iterations = TEST_ITERATIONS
        self.max_fail_count = MAX_FAIL_COUNT

    @format_docstring(num_iterations=TEST_ITERATIONS, t=TEMPERATURE_RANGE,
                      delta=DELTA)
    def DirectedTemperatureTest(self):
        """ Verify temperature is within {t[0]} to {t[1]} degrees Celsius +/-1

        1) Read temperature data for BAR register
        2) Verify data is within range
        3) Re-read temperature data and verify value is within {delta} Celsius
        4) Repeat step 3 {num_iterations} times
        5) Repeat for all available temperature channels
        """
        for channel in Channels:
            with self.subTest(TEMPERATURE_CHANNEL=channel.name):
                self.fail_count = 0
                value = self.rc.temperature_using_bar_read(channel.value)
                self.Log('info', f'{channel.name} temperature: {value:.02f} '
                                 f'degrees Celsius')
                if int(value) not in range(TEMPERATURE_RANGE[0],
                                           TEMPERATURE_RANGE[1]):
                    self.Log('error', self.error_msg_out_of_range(value))

                for iteration in range(self.test_iterations):
                    temp = self.rc.temperature_using_bar_read(
                        channel.value)
                    if not isclose(a=value, b=temp, abs_tol=DELTA):
                        self.Log('error', self.error_msg(iteration=iteration,
                                                         expected=value,
                                                         actual=temp))
                        self.update_failed_iterations(success=False)
                    if self.fail_count >= self.max_fail_count:
                        break
                self.validate_iterations()

    def error_msg_out_of_range(self, actual):
        return f'{actual:0.2f} is not within ' \
               f'{TEMPERATURE_RANGE[0]} to {TEMPERATURE_RANGE[1]} degrees ' \
               f'Celsius'

    def error_msg(self, iteration, expected, actual):
        return f'Iteration {iteration}): (expected, actual, delta): ' \
               f'{expected:.02f}, {actual:.02f}, {DELTA}'
