# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

"""RCTC has two LTC2358 ADC devices, each with 8 channels

The LTC2358 is configurable using softspan codes.

SoftSpan CODE|ANALOG INPUT RANGE|FULL SCALE RANGE|OUTPUT FORMAT
111|+/-2.5 * VREFBUF|5 * VREFBUF|Two's Complement
110|+/-2.5 * VREFBUF/1.024|5 * VREFBUF/1.024|Two's Complement
101|0V to 2.5 * VREFBUF|2.5 * VREFBUF|Straight Binary
100|0V to 2.5 * VREFBUF/1.024|2.5 * VREFBUF/1.024|Straight Binary
011|+/-1.25 * VREFBUF|2.5 * VREFBUF|Two's Complement
010|+/-1.25 * VREFBUF/1.024|2.5 * VREFBUF/1.024|Two's Complement
001|0V to 1.25 * VREFBUF|1.25 * VREFBUF|Straight Binary
000|Channel Disabled|Channel Disabled|All Zeros

"""

from Rc3.Tests.Rc3Test import Rc3Test


class Diagnostics(Rc3Test):
    """Test communication robustness"""

    def setUp(self, tester=None):
        super().setUp(tester)
        self.test_iterations = 100
        self.max_fail_count = 1
        self.ltc2358 = self.rc.ltc2358

    def DirectedChannelDisableTest(self):
        """Disable channels and verify ADC data is zero

        1) Disable odd channels using softspan code 000
        2) Verify odd channels have Raw ADC value of 0
        3) Verify even channels Raw ADC is still default
        4) Repeat steps 1 and 4 reversing odd and even channel disabling
        5) Repeat above steps a total of 10 times
        6) Restore default softspan values and verify Raw ADC default values
        7) Repeat above steps 100 times
        """
        odd_channels = [i for i in range(1, self.ltc2358.MAX_NUM_CHANNELS, 2)]
        even_channels = [i for i in range(0, self.ltc2358.MAX_NUM_CHANNELS, 2)]

        for iteration in range(self.test_iterations):
            with Ltc2358ResetManager(self.ltc2358):
                success = self.run_test(iteration,
                                        channels_disabled=odd_channels,
                                        channels_enabled=even_channels)
                for adc_num in range(self.ltc2358.MAX_NUM_LTC2358):
                    reset_channels(self.ltc2358, adc_num, odd_channels)

                success &= self.run_test(iteration,
                                         channels_disabled=even_channels,
                                         channels_enabled=odd_channels)
                for adc_num in range(self.ltc2358.MAX_NUM_LTC2358):
                    reset_channels(self.ltc2358, adc_num, even_channels)

                success &= self.verify_all_channels_at_default(iteration)

            self.update_failed_iterations(success)
            if self.fail_count >= self.max_fail_count:
                break

        self.validate_iterations()

    def disable_channels(self, adc_num, channels):
        if type(channels) is int:
            channels = [channels]
        for channel in channels:
            self.ltc2358.disable_channel(adc_num, channel)

    def run_test(self, iteration, channels_disabled, channels_enabled):
        success = True

        for adc_num in range(self.ltc2358.MAX_NUM_LTC2358):
            self.disable_channels(adc_num, channels_disabled)
            success &= self.verify_disabled_channels(
                iteration, adc_num, channels_disabled)
            success &= self.verify_enabled_channels(
                iteration, adc_num, channels_enabled)

        return success

    def verify_disabled_channels(self, iteration, adc_num, channels):
        success = True
        disable_code = self.ltc2358.SOFTSPAN_CODE_DISBALE
        for channel in channels:
            data, id, code = self.ltc2358.channel_data(adc_num, channel)
            if data != 0 or id != channel or code != disable_code:
                self.Log(
                    'error',
                    self.error_msg_at_disable(iteration,
                                              adc_num,
                                              [0, channel, disable_code],
                                              [data, id, code]))
                success = False
        return success

    def verify_enabled_channels(self, iteration, adc_num, channels):
        success = True
        disable_code = self.ltc2358.SOFTSPAN_CODE_DISBALE
        for channel in channels:
            data, id, code = self.ltc2358.channel_data(adc_num, channel)
            if data == 0 or id != channel or code == disable_code:
                self.Log(
                    'error',
                    self.error_msg_at_enable(
                        iteration, adc_num, channel, [data, id, code]))
                success = False
        return success

    def verify_all_channels_at_default(self, iteration):
        channels = [i for i in range(self.ltc2358.MAX_NUM_CHANNELS)]
        success = True

        for adc_num in range(self.ltc2358.MAX_NUM_LTC2358):
            if not self.verify_enabled_channels(iteration, adc_num, channels):
                self.Log('error', f'LTC2358_{adc_num}: Failed default verify')
                success = False

        return success

    def error_msg_at_disable(self, iteration, adc_num, expected, actual):
        return f'Iteration {iteration}: LTC2358_{adc_num} (expected, actual) '\
               f'{expected}, {actual}'

    def error_msg_at_enable(self, iteration, adc_num, channel, actual):
        expected = self.ltc2358.channel_data(adc_num, channel)
        return f'Iteration {iteration}: LTC2358_{adc_num} (expected, actual) '\
               f'{expected}, {actual}'


class Functional(Rc3Test):
    """Test operation interface"""

    def setUp(self, tester=None):
        super().setUp(tester)
        self.test_iterations = 100
        self.max_fail_count = 1
        self.ltc2358 = self.rc.ltc2358

    def DirectedSoftspanCodeRegisterWriteReadTest(self):
        """Verify writes to the Softspan Code register

        1) Write different 3 digit values to each channel's softspan code
        2) Verify read of the Softspan code register matches step 1
        3) Verify 3 digit code and channel using ADC Channel Data register
        4) Repeat above steps until for all 3 digit combinations
        5) Restore default values from step 1
        6) Repeat above steps 100 times
        """
        num_channels = self.ltc2358.MAX_NUM_CHANNELS
        codes = [i for i in range(num_channels)]

        with Ltc2358ResetManager(self.ltc2358):
            for iteration in range(self.test_iterations):
                success = True

                for ch in range(num_channels):
                    expected_code = codes[ch]

                    for adc_num in range(self.ltc2358.MAX_NUM_LTC2358):
                        self.ltc2358.write_softspan_code(
                            adc_num, ch, expected_code)
                        actual_code = self.ltc2358.softspan_code(adc_num, ch)
                        if actual_code != expected_code:
                            self.Log('error',
                                     self.error_msg_softspan_read(
                                         iteration, adc_num, ch,
                                         expected_code, actual_code))
                            success = False

                        success &= self.wait_on_channel_data_code(
                            iteration, adc_num, ch, expected_code)
                self.update_failed_iterations(success)
                if self.fail_count >= self.max_fail_count:
                    break

        self.validate_iterations()

    def wait_on_channel_data_code(self, iteration, adc_num, channel,
                                  expected_code, num_retries=50):
        data, ch, code = self.ltc2358.channel_data(adc_num, channel)
        if ch == channel and code == expected_code:
            return True

        for retry in range(num_retries):
            data, ch, code = self.ltc2358.channel_data(adc_num, channel)
            if ch == channel and code == expected_code:
                self.Log('warning', f'Channel data expected code found '
                                    f'({retry} ouf of {num_retries} retries')
                return True
        else:
            self.Log('error',
                     self.error_msg_data_read(
                         iteration, adc_num, channel,
                         expected=[channel, expected_code],
                         actual=[ch, code]))
            return False

    def error_msg_softspan_read(self, iteration, adc_num, channel,
                                expected, actual):
        return f'Iteration {iteration}: LTC2358_{adc_num}_{channel} - ' \
               f'Invalid Softspan Code (expected, actual) ' \
               f'0b{expected:03b}, 0b{actual:03b}'

    def error_msg_data_read(self, iteration, adc_num, channel,
                            expected, actual):
        return f'Iteration {iteration}: LTC2358_{adc_num}_{channel} - ' \
               f'Invalid channel_data [channel_id, softspan_code] ' \
               f'(expected, actual) [{expected[0]}, 0b{expected[1]:03b}], ' \
               f'[{actual[0]}, 0b{actual[1]:03b}]'


def reset_channels(ltc2358, adc_num, channels):
    if type(channels) is int:
        channels = [channels]
    for channel in channels:
        ltc2358.reset_channel(adc_num, channel)


def reset_all_channels(ltc2358):
    channels = [i for i in range(ltc2358.MAX_NUM_CHANNELS)]
    for adc_num in range(ltc2358.MAX_NUM_LTC2358):
        reset_channels(ltc2358, adc_num, channels)


class Ltc2358ResetManager:
    def __init__(self, ltc2358):
        self.ltc2358 = ltc2358

    def __enter__(self):
        reset_all_channels(self.ltc2358)
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        reset_all_channels(self.ltc2358)
