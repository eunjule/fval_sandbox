# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.


"""RCTC3 has a Broadcast Trigger Interface

FPGA can generate Aurora Trigger Down packets to all connected trigger link
partners simultaneously
"""

from random import getrandbits

from Common.fval import TimeElapsedLogger
from Common.instruments.hdmt_trigger_interface import \
    generate_random_payload_with_exclude, \
    HdmtTriggerInterface,\
    validate_aurora_statuses
from Common.triggers import CardType, generate_trigger
from Rc3.Tests.Rc3Test import Rc3Test


class Diagnostics(Rc3Test):
    """Test communication of the interface"""

    def setUp(self, tester=None):
        super().setUp(tester)
        self.interfaces = HdmtTriggerInterface.get_interfaces()
        self.test_iterations = 100000
        self.max_fail_count = 2

        self.Log('info', f'Checking Trigger Status before test...')
        validate_aurora_statuses(self.rc.aurora_link_slots)

        for slot, sub_interfaces in self.interfaces.items():
            if slot is not None:
                for sub_interface in sub_interfaces:
                    sub_interface.setup_interface()

    def DirectedSendBroadcastTriggerTest(self):
        """Send a trigger to all connected trigger link partners

        1) Set the Broadcast Trigger Down register with a 32-bit random number
        2) Verify receipt on all connected partners
        3) Repeat above steps a total of 100000 times
        """

        with TimeElapsedLogger(
                f'Time elapsed for DirectedSendBroadcastTriggerTest'):
            for iteration in range(self.test_iterations):
                success = True

                # Keep DPS card type as is. DPS instrument only updates its
                # down trigger register with a DPS card type, while others
                # accept any allowable type.
                trigger = generate_trigger(
                    card_type=CardType.DPS.name,
                    payload=generate_random_payload_with_exclude(0))

                self.rc.send_broadcast_trigger(trigger)
                self.rc.verify_last_broadcasted_trigger(trigger)

                for slot, sub_interfaces in self.interfaces.items():
                    if slot is not None:  # skip Rc
                        sub_index = \
                            self.randomize_sub_interfaces_index(sub_interfaces)
                        actual_trigger = \
                            sub_interfaces[sub_index].check_for_down_trigger(
                            trigger)
                        for sub_interface in sub_interfaces:
                            sub_interface.setup_interface()
                        if actual_trigger != trigger:
                            success = False
                            self.Log(
                                'error',
                                self.error_msg(
                                    iteration=iteration,
                                    name=sub_interfaces[sub_index].name(),
                                    expected=trigger,
                                    actual=actual_trigger))
                            validate_aurora_statuses(self.rc.aurora_link_slots)

                self.update_failed_iterations(success)
                if self.fail_count >= self.max_fail_count:
                    break

            self.validate_iterations()

    def randomize_sub_interfaces_index(self, sub_interfaces):
        return getrandbits(len(sub_interfaces)-1)

    def generate_random_number_with_exclude(self, num_bits, exclude):
        value = exclude
        while value == exclude:
            value = getrandbits(num_bits)
        return value

    def error_msg(self, iteration, name, expected, actual):
        return f'Iteration {iteration}: {name} did not receive broadcast ' \
               f'trigger (expected, actual): 0x{expected:08X}, 0x{actual:08X}'
