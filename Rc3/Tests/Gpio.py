# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.

"""RCTC3 FPGA has General Purpose Input/Output pins

FPGA has Bar registers to provide status or control of GPIO pins
"""

from math import ceil, isclose
from os import getenv, system
from random import getrandbits, randrange
from time import clock, sleep, time

from Common.fval import SkipTest
from Common.utilities import compliment_bytes, format_docstring, negative_one
from Common.instruments import backplane, cb2
from Common.triggers import CardType, generate_trigger
from Rc3.instrument.rc3_trigger_interface import MAX_NUM_LINKS
from Rc3.Tests.Rc3Test import Rc3Test


FUNCTIONAL_TEST_ITERATIONS = 100
DIAGNOSTICS_TEST_ITERATIONS = 1000


class Diagnostics(Rc3Test):
    """Test pin status"""

    def setUp(self, tester=None):
        super().setUp(tester)
        self.test_iterations = DIAGNOSTICS_TEST_ITERATIONS
        self.max_fail_count = 10

    def DirectedGeneralPurposeInputRegisterTest(self):
        """Read register and confirm stable value

        Note: This is a read only register
        1) read initial value of register
        2) Read register 1000 times and make sure value does not change
        """
        expected = self.general_purpose_input_reg().value
        for iteration in range(self.test_iterations):
            register = self.general_purpose_input_reg()
            actual = register.value
            if expected != actual:
                self.Log('error',
                         self.error_msg(iteration, register, actual, expected))
                self.update_failed_iterations(False)

            if self.fail_count >= self.max_fail_count:
                break

        self.validate_iterations()

    def general_purpose_input_reg(self):
        return self.rc.general_purpose_input_reg()

    def DirectedGeneralPurposeOutputRegisterTest(self):
        """Verify default value of register fields

        Field, default value, pin name:
        - Power Supply Kill Control, 0, ps_kill_l_ctrl
        - GPIO Fault Output Enable, 0, gpio_fault_oe_l_n
        - Backplane I2C Reset, 1, i2c_rst_l_bp
        - Stratix 10 Fault, 0, s10_fault
        - Relay Select, 0, selrly_1p8v

        Test steps:
        - Read register and verify default value of bits 23:0 equals 0x20_0000
        - Repeat above steps 100 times
        """
        expected = 0x20_0000
        for iteration in range(self.test_iterations):
            register = self.general_purpose_output_reg()
            actual = register.value & 0x00FFFFFF
            if actual != expected:
                self.Log('error',
                         self.error_msg(iteration, register, actual, expected))
                self.update_failed_iterations(False)

            if self.fail_count >= self.max_fail_count:
                break

        self.validate_iterations()

    def general_purpose_output_reg(self):
        return self.rc.general_purpose_output_reg()

    def DirectedTiuSpareGpioInputRegisterTest(self):
        """Read TIU_RC_FPGA_SPARE_X_1P8V_IO pins where X is 0 to 5

        1) Place random value on TIU bus from connected TIU
        2) Read register and compare with value from step 1
        3) Repeat a total of 100 times
        """

    def error_msg(self, iteration, register, observed, expected):
        return f'Iteration: {iteration} - {register.name()}: ' \
               f'Observed 0x{observed:08X} does NOT match ' \
               f'expected 0x{expected:08X}'


class Functional(Rc3Test):
    """Test operation of registers"""
    TRIGGER_INTERFACE = None

    def setUp(self, tester=None):
        super().setUp(tester)
        self.test_iterations = FUNCTIONAL_TEST_ITERATIONS
        self.max_fail_count = 10

    def DirectedFpgaDebugFlippingBitsTes(self):
        """ Verify operation of bidrectional interface

        The control field sets the direction of the fpga_debug bus one bit at a
        time.
        Control bit is 1: Output field bit is reflected on input field bit
        Control bit is 0: Input field bit reflects external connection
        Test steps:
        1) Get the default input field value by setting control to all input
        2) Set output field to inverse of default input
        3) Perform a walking one's on the control field. Sets bit to output one
        bit at a time
        4) Verify corresponding input field bits toggle
        5) Repeat steps 3 and 4 a total of 100 times
        """
        fpga_debug_byte_size = 1

        # get default input field's value when control is set to all input
        self.rc.write_fpga_debug_control(0x00)
        self.fpga_debug_test_wait_seconds()
        default_input = self.rc.fpga_debug_input()

        # set the output field's value to compliment of default input
        output_value = compliment_bytes(default_input,
                                        num_bytes=fpga_debug_byte_size)
        self.rc.write_fpga_debug_data(output_value)

        # calculate expected input field values
        expected_values = self.flipping_bits_expected_input_values(
            default_input, fpga_debug_byte_size)

        # send control field to output one bit at a time
        for iteration in range(self.test_iterations):
            success = True

            for bit_num in range(8 * fpga_debug_byte_size):
                self.rc.write_fpga_debug_control(1 << bit_num)
                self.fpga_debug_test_wait_seconds()

                actual = self.rc.fpga_debug_input()
                expected = expected_values[bit_num]
                if actual != expected:
                    msg = self.error_msg_flipping_bits(
                        iteration, expected, actual, fpga_debug_byte_size)
                    self.Log('error', msg)
                    success = False

            self.update_failed_iterations(success)
            if self.fail_count >= self.max_fail_count:
                break

        self.validate_iterations()

    def flipping_bits_expected_input_values(self, default_input, num_bytes):
        out_compliment = compliment_bytes(default_input, num_bytes)
        out_list = [i for i in f'{out_compliment:b}'.zfill(8 * num_bytes)]
        out_list.reverse()

        expected = []
        for i in range(len(out_list)):
            if out_list[i] == '1':
                expected.append(default_input | out_compliment)
            else:
                f_s = negative_one(num_bytes)
                expected.append(default_input & (f_s ^ 1 << i))
        return expected

    def error_msg_flipping_bits(self, iteration, expected, actual, num_bytes):
        expected = f'0b{expected:0b}'.zfill(8 * num_bytes)
        actual = f'0b{actual:0b}'.zfill(8 * num_bytes)
        return f'Iteration {iteration}: Invalid input field ' \
               f'(expected, actual) {expected}, {actual}'

    def DirectedFpgaDebugOutputToInputTest(self):
        """ Verify transfer of data from output to input

        1) Set direction control to all output
        2) Write randomized data to output data
        3) Verify input data matches output data
        4) Repeat steps 2 and 3 100 times
        """
        for iteration in range(self.test_iterations):
            success = True
            success &= self.output_control_enabled_scenarion(iteration)
            success &= self.output_control_disabled_scenario(iteration)
            self.update_failed_iterations(success)
            if self.fail_count >= self.max_fail_count:
                break

        self.validate_iterations()

    def output_control_enabled_scenarion(self, iteration):
        self.rc.write_fpga_debug_control(0xFF)
        self.fpga_debug_test_wait_seconds()
        expected_data = self.generate_random_byte(exclude=0xFF)
        self.rc.write_fpga_debug_data(expected_data)
        self.fpga_debug_test_wait_seconds()
        actual = self.rc.fpga_debug_input()
        if actual != expected_data:
            self.Log('error', self.error_msg_output_enabled(
                iteration, expected_data, actual))
            return False
        else:
            return True

    def error_msg_output_enabled(self, iteration, expected, actual):
        return f'Iteration {iteration}: Output to Input should match ' \
               f'(expected, actual): 0x{expected:02X}, 0x{actual:02X}'

    def output_control_disabled_scenario(self, iteration):
        self.rc.write_fpga_debug_control(0x00)
        self.fpga_debug_test_wait_seconds()
        expected_data = self.generate_random_byte(
            exclude=self.rc.fpga_debug_input())
        self.rc.write_fpga_debug_data(expected_data)
        self.fpga_debug_test_wait_seconds()
        actual = self.rc.fpga_debug_input()
        if actual == expected_data:
            self.Log('error', self.error_msg_output_disabled(
                iteration, expected_data, actual))
            return False
        else:
            return True

    def error_msg_output_disabled(self, iteration, expected, actual):
        return f'Iteration {iteration}: Output to Input should not match ' \
               f'(expected, actual): 0x{expected:02X}, 0x{actual:02X}'

    def fpga_debug_test_wait_seconds(self):
        sleep(0.025)

    def generate_random_byte(self, exclude):
        data = getrandbits(8)
        while data == exclude:
            data = getrandbits(8)
        return data

    def DirectedSafetyModeInterruptEnableStatusRegisterTest(self):
        """Verify Safety Mode interrupts

        NOTE: Enable Safety Mode Interrupts from Interrupt Control Register
        1) Generate an assertion on bit 5 of the TIU spare GPIO bus
        2) Verify safety mode interrupt pending register bit is 1 and software
           trigger 0
        3) Write a 1 to the bit of step 2 to clear the alarm
        4) Verify safety mode interrupt pending register bit is 0 and software
           trigger 0
        5) Repeat above steps a total of 100 times
        """
        for iteration in range(self.test_iterations):
            with InterruptManager(rc=self.rc, is_safety_mode_test=True) as mgr:

                self.rc.enable_safety_mode_interrupt_bit(enable=True)

                interrupt_not_set_pass = self.check_interrupt_status(
                    iteration=iteration,
                    expected_safety_mode_status=0, expected_trigger_status=0)

                mgr.send_interrupt_via_tiu_spare_gpio_bus()

                interrupt_set_pass = self.check_interrupt_status(
                    iteration=iteration,
                    expected_safety_mode_status=1, expected_trigger_status=0)

                interrupt_clear_pass = \
                    self.rc.clear_safety_mode_interrupt_status()
                if not interrupt_clear_pass:
                    self.Log(
                        'warning',
                        f'Iteration {iteration}: Unable to clear '
                        f'{self.rc.name()} safety mode interrupt bit. '
                        f'Attempitng to correct by power cycling TIU')
                    self.rc.enable_safety_mode_interrupt_bit(enable=False)
                    mgr.power_cycle_tiu()
                    mgr.initialize_calbase()
                    self.rc.enable_safety_mode_interrupt_bit(enable=True)
                    interrupt_clear_pass =\
                        self.rc.clear_safety_mode_interrupt_status()
                    if not interrupt_clear_pass:
                        self.Log('error',
                                 f'Iteration {iteration}: Unable to clear '
                                 f'{self.rc.name()} safety mode interrupt bit')

                success = interrupt_set_pass & interrupt_clear_pass & \
                          interrupt_not_set_pass
                if getenv('DBG_GPIO_INTERRUPT'):
                    if not success:
                        system('pause')
                self.update_failed_iterations(success)

            if self.fail_count >= self.max_fail_count:
                break
        self.validate_iterations()

    def DirectedSafetyModeInterruptDisableStatusRegisterTest(self):
        """Verify Safety Mode interrupts

        NOTE: Disable Safety Mode Interrupts from Interrupt Control Register
        1) Generate an assertion on bit 5 of the TIU spare GPIO bus
        2) Verify safety mode interrupt pending register bit is 0 and software
           trigger 0
        4) Repeat above steps a total of 100 times
        """
        for iteration in range(self.test_iterations):
            with InterruptManager(rc=self.rc, is_safety_mode_test=True) as mgr:

                self.rc.enable_safety_mode_interrupt_bit(enable=False)
                mgr.send_interrupt_via_tiu_spare_gpio_bus()

                success = self.check_interrupt_status(
                    iteration=iteration,
                    expected_safety_mode_status=0, expected_trigger_status=0)
                if getenv('DBG_GPIO_INTERRUPT'):
                    if not success:
                        system('pause')
                self.update_failed_iterations(success)

            if self.fail_count >= self.max_fail_count:
                break
        self.validate_iterations()

    def DirectedTriggerInterruptEnableStatusMaxFifoCountRegisterTest(self):
        """Verify Trigger interrupts

        NOTE: Enable Trigger Interrupts from Interrupt Control Register
        1) Send software trigger to RC3
        2) Verify software interrupt pending register bit is 1 and
           safety mode 0
        3) Empty software trigger FIFO to clear the alarm
        4) Verify software interrupt pending register bit is 0 and
           safety mode 0
        5) Repeat above steps a total of 100 times
        """
        max_fifo_size = 1024

        for iteration in range(self.test_iterations):
            success = True

            with InterruptManager(rc=self.rc, is_trigger_test=True):
                self.rc.enable_trigger_interrupt_bit(enable=True)
                success &= self.check_interrupt_status(
                    iteration=iteration,
                    expected_safety_mode_status=0,
                    expected_trigger_status=0)

                self.send_software_triggers(max_fifo_size)
                success &= self.check_interrupt_status(
                    iteration=iteration,
                    expected_safety_mode_status=0,
                    expected_trigger_status=1)

                self.rc.software_trigger_reset()
                success &= self.check_interrupt_status(
                    iteration=iteration,
                    expected_safety_mode_status=0,
                    expected_trigger_status=0)

            self.update_failed_iterations(success)
            if self.fail_count >= self.max_fail_count:
                break

        self.validate_iterations()

    def DirectedTriggerInterruptEnableStatusZeroFifoCountRegisterTest(self):
        """Verify Trigger interrupts

        NOTE: Enable Trigger Interrupts from Interrupt Control Register
        1) Send software trigger to RC3
        2) Verify software interrupt pending register bit is 1 and
           safety mode 0
        3) Empty software trigger FIFO to clear the alarm
        4) Verify software interrupt pending register bit is 0 and
           safety mode 0
        5) Repeat above steps a total of 100 times
        """
        fifo_size = 0

        for iteration in range(self.test_iterations):
            success = True

            with InterruptManager(rc=self.rc, is_trigger_test=True):
                self.rc.enable_trigger_interrupt_bit(enable=True)
                success &= self.check_interrupt_status(
                    iteration=iteration,
                    expected_safety_mode_status=0,
                    expected_trigger_status=0)

                self.send_software_triggers(fifo_size)
                success &= self.check_interrupt_status(
                    iteration=iteration,
                    expected_safety_mode_status=0,
                    expected_trigger_status=0)
                self.update_failed_iterations(success)

            if self.fail_count >= self.max_fail_count:
                break

        self.validate_iterations()

    def DirectedTriggerInterruptEnableStatusSingleFifoCountRegisterTest(self):
        """Verify Trigger interrupts

        NOTE: Enable Trigger Interrupts from Interrupt Control Register
        1) Send software trigger to RC3
        2) Verify software interrupt pending register bit is 1 and
           safety mode 0
        3) Empty software trigger FIFO to clear the alarm
        4) Verify software interrupt pending register bit is 0 and
           safety mode 0
        5) Repeat above steps a total of 100 times
        """
        fifo_size = 1

        for iteration in range(self.test_iterations):
            success = True

            with InterruptManager(rc=self.rc, is_trigger_test=True):
                self.rc.enable_trigger_interrupt_bit(enable=True)
                success &= self.check_interrupt_status(
                    iteration=iteration,
                    expected_safety_mode_status=0,
                    expected_trigger_status=0)

                self.send_software_triggers(fifo_size)
                success &= self.check_interrupt_status(
                    iteration=iteration,
                    expected_safety_mode_status=0,
                    expected_trigger_status=1)

                self.rc.software_trigger_reset()
                success &= self.check_interrupt_status(
                    iteration=iteration,
                    expected_safety_mode_status=0,
                    expected_trigger_status=0)

            self.update_failed_iterations(success)
            if self.fail_count >= self.max_fail_count:
                break

        self.validate_iterations()

    def DirectedTriggerInterruptDisableStatusRegisterTest(self):
        """Verify Trigger interrupts

        NOTE: Disable Trigger Interrupts from Interrupt Control Register
        1) Send software trigger to RC3
        2) Verify trigger interrupt pending register bit is 0 and
           safety mode 0
        4) Repeat above steps a total of 100 times
        """

        for iteration in range(self.test_iterations):
            with InterruptManager(rc=self.rc, is_trigger_test=True):
                self.rc.enable_trigger_interrupt_bit(enable=False)

                self.send_software_triggers(num_triggers=1)

                if not self.check_interrupt_status(
                    iteration=iteration,
                    expected_safety_mode_status=0, expected_trigger_status=0):
                    self.update_failed_iterations(success=False)

            if self.fail_count >= self.max_fail_count:
                break
        self.validate_iterations()

    def DirectedInterruptControlRegisterTest(self):
        """Verify Enable/Disable of Safety Mode and Trigger interrupts

        Test interrupt control using all four possible control combinations:
        - Enabled, Enabled, - Disabled, Enabled, - Enabled, Disabled,
        - Disabled, Disabled
        For each combination, generate both interrupts from their corresponding
        sources, and verify that only the Enabled combination has its bit set
        in the Interrupt Status register.
        Repeat test a total of 100 times
        """
        self.test_iterations = 10

        for iteration in range(self.test_iterations):
            success = True
            with InterruptManager(rc=self.rc,
                                  is_safety_mode_test=True,
                                  is_trigger_test=True) as mgr:
                for [safety_mode_enable, trigger_enable] in [[False, False],
                    [False, True], [True, False], [True, True]]:
                        success &= self.interrupt_combination_scenario(
                            iteration, mgr, safety_mode_enable, trigger_enable)
                self.update_failed_iterations(success)
                if self.fail_count >= self.max_fail_count:
                    break
        self.validate_iterations()

    def interrupt_combination_scenario(self, iteration, interrupt_manager,
                                       safety_mode_enable, trigger_enable):
        self.rc.enable_safety_mode_interrupt_bit(safety_mode_enable)
        self.rc.enable_trigger_interrupt_bit(trigger_enable)
        interrupt_manager.send_interrupt_via_tiu_spare_gpio_bus()
        self.send_software_triggers(num_triggers=10)
        interrupt_set_pass = self.check_interrupt_status(
            iteration=iteration,
            expected_safety_mode_status=int(safety_mode_enable),
            expected_trigger_status=int(trigger_enable))
        self.rc.clear_safety_mode_interrupt_status()
        self.rc.software_trigger_reset()
        interrupt_clear_pass = self.check_interrupt_status(
            iteration=iteration,
            expected_safety_mode_status=0, expected_trigger_status=0)
        return interrupt_set_pass & interrupt_clear_pass

    @format_docstring(times=FUNCTIONAL_TEST_ITERATIONS)
    def DirectedPowerOnCountRegisterTest(self):
        """Validate Power On Count field increments by one every 10 ms

        1) Read the Power On Count register
        2) Calculate time since calling Fpga image load and convert to a count
        3) Validate counts from above steps are within acceptable range
        4) Wait 10 ms
        5) Repeat above steps a total of {times} times
        """
        if self.rc.time_after_fpga_load() is None:
            raise SkipTest(f'Unable to run test. FPGA image load was skipped. '
                           f'Power On count starts at FPGA image load.')

        time_2_count_factor = 0.01
        self.count_delta = 150  # about 1.5 seconds of delta

        for iteration in range(self.test_iterations):
            actual_count = self.rc.power_on_count()
            expected_count = self.expected_power_on_count()
            self.sleep_seconds(time_2_count_factor)
            if not isclose(a=expected_count, b=actual_count,
                           abs_tol=self.count_delta):
                self.Log('error',
                         self.power_on_error_msg(iteration=iteration,
                                                 expected=expected_count,
                                                 actual=actual_count))
                self.update_failed_iterations(success=False)
            if self.fail_count >= self.max_fail_count:
                break
        self.validate_iterations()

    def expected_power_on_count(self):
        time_2_count_factor = 0.01
        time_after_fpga_load = self.rc.time_after_fpga_load()
        current_time = time()
        expected_count = ceil(abs(time_after_fpga_load - current_time) /
                              time_2_count_factor)
        return expected_count

    def sleep_seconds(self, seconds):
        start_time = clock()
        while (clock() - start_time) < seconds:
            pass

    def power_on_error_msg(self, iteration, expected, actual):
        return f'Iteration {iteration}) Power On Counts (expected, actual): ' \
               f'{expected}, {actual}. Absolute tolerance (expected, actual): '\
               f'{self.count_delta}, {abs(expected-actual)}'

    def DirectedRcFpgaSpareGpioInterfaceBidirectionalControlTest(self):
        """Verify directional control of the “rc_fpga_lv_spare8" pin

        1) Set the direction control to Output
        2) Set the data output bit to 0
        3) Verify data input bit is 0
        4) Repeat steps 2 and 3 but using 1
        5) Set the direction control to Input
        6) Record the data input bit value
        7) Set data output bit to 1 and 0 and verify data input does not change
        8) Repeat above steps a total of 100 times
        """
        output = 0b1
        input = 0b0
        msg = f'RC FPGA SPARE GPIO'
        for iteration in range(self.test_iterations):
            success = True

            expected = 0b1
            self.rc.write_rc_fpga_spare_control(control=output)
            self.rc.write_rc_fpga_spare_data(data=expected)
            observed = self.rc.rc_fpga_spare_input()
            success &= self.check_results(f'{msg}: Control: {output}', expected, observed)

            expected = 0b0
            self.rc.write_rc_fpga_spare_data(data=expected)
            observed = self.rc.rc_fpga_spare_input()
            success  &= self.check_results(f'{msg}: Control: {output}', expected, observed)

            self.rc.write_rc_fpga_spare_control(control=input)
            expected = self.rc.rc_fpga_spare_input()
            self.rc.write_rc_fpga_spare_data(data=0b1)
            self.rc.write_rc_fpga_spare_data(data=0b0)
            observed = self.rc.rc_fpga_spare_input()
            success &= self.check_results(f'{msg}: Control: {input}', expected, observed)

            self.update_failed_iterations(success)

            if self.fail_count >= self.max_fail_count:
                break

        self.validate_iterations()

    def DirectedRcFrontPanelGpioInterfaceBidirectionalControlTest(self):
        """Verify directional control of the “rc_fp_l_fpga" pin

        1) Set the direction control to Output
        2) Set the data output bit to 0b0
        3) Verify data input bit is 0b0
        4) Repeat steps 2 and 3 but using 0b11
        5) Set the direction control to Input
        6) Record the data input bit value
        7) Set data output bit to 1 and 0 and verify data input does not change
        8) Repeat above steps a total of 100 times
        """
        output = 0b11
        input = 0b00
        msg = f'RC FRONT PANEL GPIO'
        for iteration in range(self.test_iterations):
            success = True

            expected = 0b00
            self.rc.write_rc_fp_gpio_control(control=output)
            self.rc.write_rc_fp_gpio_data(data=expected)
            observed = self.rc.rc_fp_gpio_input()
            success  &= self.check_results(msg, expected, observed)

            expected = 0b11
            self.rc.write_rc_fp_gpio_data(data=expected)
            observed = self.rc.rc_fp_gpio_input()
            success &= self.check_results(msg, expected, observed)

            self.rc.write_rc_fp_gpio_control(control=input)
            expected = self.rc.rc_fp_gpio_input()
            self.rc.write_rc_fp_gpio_data(data=0b01)
            self.rc.write_rc_fp_gpio_data(data=0b00)
            observed = self.rc.rc_fp_gpio_input()
            success &= self.check_results(msg, expected, observed)

            self.update_failed_iterations(success)

            if self.fail_count >= self.max_fail_count:
                break

        self.validate_iterations()

    def check_results(self, msg, expected, observed):
        success = True
        if observed != expected:
            self.Log('error', f'{msg} Failed: '
                              f'Expected, Observed: 0x{expected:02X}, '
                              f'{observed:02X}')
            success = False
        return success

    def send_software_triggers(self, num_triggers):
        for i in range(num_triggers):
            trigger = self.generate_random_software_trigger()
            slot = randrange(0, MAX_NUM_LINKS)
            self.rc.send_up_trigger_bypass(slot, trigger)

    def generate_random_software_trigger(self):
        return generate_trigger(card_type=CardType.RTOS.name,
                                payload=getrandbits(19),
                                is_software_trigger=True)

    def check_interrupt_status(self, iteration, expected_safety_mode_status,
                               expected_trigger_status):
        check_pass = True
        observed_safety_mode_status, observed_trigger_status = \
            self.rc.read_interrupt_status()
        if observed_safety_mode_status != expected_safety_mode_status:
            check_pass = False
            self.Log('error', f'Iteration {iteration}: '
                              f'safty_mode_status Failed '
                              f'expected: {expected_safety_mode_status}, '
                              f'observed: {observed_safety_mode_status}')
        if observed_trigger_status != expected_trigger_status:
            check_pass = False
            self.Log('error', f'Iteration {iteration}: '
                              f'trigger_status Failed '
                              f'expected: {expected_trigger_status}, '
                              f'observed: {observed_trigger_status}')
        return check_pass


class InterruptManager():
    def __init__(self, rc, is_safety_mode_test=False, is_trigger_test=False):
        if not rc.is_tiu_physically_connected() and is_safety_mode_test:
            raise SkipTest(f'TIU is not present')
        self.rc = rc
        self.is_safety_mode_test = bool(is_safety_mode_test)
        self.is_trigger_test = bool(is_trigger_test)
        self.cb2 = cb2.Cb2()
        self.bp = backplane.Backplane()
        self.initialize_calbase()
        self.rc.software_trigger_reset()

    def __enter__(self):
        if self.is_safety_mode_test:
            self.initialize_calbase()
            self.rc.enable_safety_mode_interrupt_bit(enable=False)
        if self.is_trigger_test:
            self.rc.software_trigger_reset()
            self.rc.enable_trigger_interrupt_bit(enable=False)
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        if self.is_safety_mode_test:
            self.rc.enable_safety_mode_interrupt_bit(enable=True)
        if self.is_trigger_test:
            self.rc.enable_trigger_interrupt_bit(enable=True)
            self.rc.software_trigger_reset()

    def initialize_calbase(self):
        if not self.cb2.Connect():
            self.ensure_tiu_is_powered()

        # This method has to be called before each safety mode interrupt test
        self.cb2.Initialize()

    def ensure_tiu_is_powered(self):
        if self.bp.PowerState() != [1, 1]:
            self.bp.PowerEnable()

            aux_5v, aux_12v = self.bp.PowerState()
            if [aux_5v, aux_5v] != [1, 1]:
                aux_5v = 'On' if bool(aux_5v) else 'Off'
                aux_12v = 'On' if bool(aux_12v) else 'Off'
                raise InterruptManager.Error(
                    f'Unable to power up TIU CalBase (aux 5v, aux 12V):'
                    f'{aux_5v}, {aux_12v}')

    def send_interrupt_via_tiu_spare_gpio_bus(self):
        TCBG_TIU_5V_RC_FPGA_SPARE_6 = 69
        self.cb2.toggle_negedge_tiu_spare_gpio_bit(TCBG_TIU_5V_RC_FPGA_SPARE_6)

    def power_cycle_tiu(self):
        self.bp.PowerCycle()
