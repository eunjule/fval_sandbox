# INTEL CONFIDENTIAL

# Copyright 2020 Intel Corporation.

# This software and the related documents are Intel copyrighted materials,
# and your use of them is governed by the express license under which they
# were provided to you ("License"). Unless the License provides otherwise,
# you may not use, modify, copy, publish, distribute, disclose or transmit
# this software or the related documents without Intel's prior written
# permission.

# This software and the related documents are provided as is, with no express
# or implied warranties, other than those that are expressly stated in the
# License.
"""Build script for Windows

Builds C++ sources.
"""

import argparse
import os
import shutil
import subprocess


checkout_dir = os.path.abspath(os.path.dirname(__file__))


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('target', nargs='?', default='Release')
    return parser.parse_args()


def build(target):
    add_swig_to_path()
    enter_build_directory()
    run_cmake()
    run_msbuild(target)


def run_msbuild(target):
    subprocess.run(f'{msbuild()} /nologo /p:Configuration={target} /t:Build /m FpgaValidation.sln')


def run_cmake():
    try:
        subprocess.run(f'{cmake()} -G"Visual Studio 16 2019" ../')
    except FileNotFoundError:
        print(f'ERROR: {cmake()} not found')
        exit(1)


def msbuild():
    if os.path.exists(msbuild_professional()):
        return msbuild_professional()
    if os.path.exists(msbuild_enterprise()):
        return msbuild_enterprise()
    raise FileNotFoundError('Did not find MSBuild')


def msbuild_professional():
    return 'C:\\Program Files (x86)\\Microsoft Visual Studio\\2019\\Professional\\MSBuild\\Current\\Bin\\MSBuild.exe'


def msbuild_enterprise():
    return 'C:\\Program Files (x86)\\Microsoft Visual Studio\\2019\\Enterprise\\MSBuild\\Current\\Bin\\MSBuild.exe'


def cmake():
    return os.path.join(checkout_dir, 'ThirdParty', 'cmake', 'bin', 'cmake.exe')


def add_swig_to_path():
    swig_path = os.path.join(checkout_dir, 'ThirdParty', 'swig')
    os.environ['PATH'] += os.pathsep + swig_path


def enter_build_directory():
    try:
        os.mkdir('_build')
    except FileExistsError:
        pass
    os.chdir('_build')


def clean():
    shutil.rmtree('_build', ignore_errors=True)


if __name__ == '__main__':
    args = parse_args()
    if args.target.lower() == 'clean':
        clean()
    else:
        build(args.target)


